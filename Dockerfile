# vi:syntax=dockerfile

# Frontend build
FROM node:8-alpine

RUN mkdir -p /tmp/build-javascript
WORKDIR /tmp/build-javascript
RUN apk update && apk add curl make bash gettext openjdk7-jre unzip

COPY yarn.lock .
COPY package.json .
RUN yarn install --pure-lockfile

# Translations
COPY frontend frontend
COPY Makefile Makefile
RUN make makemessages
RUN make translations

# Build comment system, checkout
ARG CROWDIN_API_KEY
ARG CROWDIN_PROJECT_ID
ARG CHECKOUT_INTERACTIVE_TRANSLATIONS

COPY apps/b3_core/static/js/button3d/common apps/b3_core/static/js/button3d/common
COPY apps/b3_core/static/js/utils apps/b3_core/static/js/utils
COPY frontend/webpack.config.js frontend/webpack.config.js
COPY frontend/webpack.dev.config.js frontend/webpack.dev.config.js
COPY frontend/webpack.prod.config.js frontend/webpack.prod.config.js
COPY .babelrc .babelrc
COPY .eslintrc .eslintrc
RUN mkdir -p apps/b3_core/static/js/dist
RUN mkdir -p apps/b3_user_panel/templates/dist
RUN if [ "$CHECKOUT_INTERACTIVE_TRANSLATIONS" == "true" ] ; then yarn run containers:buildCrowdin; else yarn run build; fi
RUN  ./node_modules/.bin/jest --ci --testResultsProcessor="jest-bamboo-formatter" || echo "test results are examined later"

FROM python:3.6.4-alpine3.7

WORKDIR /var/www/django/

RUN apk update && apk add --no-cache \
    bash \
    build-base \
    cairo \
    curl \
    freetds \
    freetds-dev \
    gcc \
    gdk-pixbuf \
    gettext \
    git \
    libffi-dev \
    libjpeg-turbo-dev \
    libxml2 \
    linux-headers \
    make \
    mariadb-client \
    musl-dev \
    netcat-openbsd \
    nginx \
    nodejs \
    postgresql-client \
    postgresql-dev \
    pango \
    ruby \
    ruby-dev \
    shared-mime-info \
    ttf-opensans \
    unixodbc-dev \
    unzip \
    xmlsec-dev \
    yarn

COPY docker_scripts/fix-libs.sh /bin/
RUN chmod a+x /bin/fix-libs.sh
RUN /bin/fix-libs.sh /usr/lib/libpangocairo-1.0.so.0 \
    /usr/lib/libpangoxft-1.0.so.0 \
    /usr/lib/libpangoft2-1.0.so.0 \
    /usr/lib/libpango-1.0.so.0 \
    /usr/lib/libcairo.so.2 \
    /usr/lib/libgobject-2.0.so.0

# Configure MS-SQL driver
COPY docker_scripts/odbcinst.ini /etc/
RUN odbcinst -i -d -f /etc/odbcinst.ini

# Link python/pip command to python3/pip3 binary
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN ln -s /usr/bin/pip3 /usr/bin/pip

# install some python dependencies that are not direct dependencies of button3d
RUN pip3 install \
    j2cli-3==0.0.1 \
    coverage2clover==1.3.2 \
    gunicorn==19.8.1 \
    psycopg2-binary==2.7.4 \
    unittest-xml-reporting==2.1.1 \
    requests-mock==1.5.0 \
    django-pyodbc-azure==1.11.12.1 \
    tblib==1.3.2 \
    awscli==1.15.85 \
    newrelic==4.2.0.100

# the versions are of enormous importance here!
RUN echo 'gem: --no-document' > /etc/gemrc && gem install sass
RUN npm config set unsafe-perm true
RUN npm install -g autoprefixer@5.1.1 yarn yuglify@0.1.4

# install button3d dependencies
RUN mkdir -p /var/log/django/
COPY ./requirements.txt /var/www/django/requirements.txt
COPY materials.json .
RUN pip3 install -r requirements.txt

# We add source files after all dependencies have been installed.
# This way, the installation steps are cached even if the source is changed.

# add run scripts
COPY ./docker_scripts/server.conf.j2 /var/www/django/docker_scripts/server.conf.j2
COPY ./docker_scripts/start.sh ./docker_scripts/test.sh ./docker_scripts/sync_static.sh ./docker_scripts/prepare_dev.sh ./docker_scripts/start_celery.sh /bin/
RUN chmod +x /bin/start.sh /bin/test.sh /bin/sync_static.sh /bin/prepare_dev.sh /bin/start_celery.sh

# add the django source to the container
COPY ./button3d /var/www/django/button3d
COPY ./apps /var/www/django/apps
COPY ./locale /var/www/django/locale
COPY ./frontend/src/locale/de_DE/LC_MESSAGES/app.po /var/www/django/locale/de/LC_MESSAGES/app.po
COPY manage.py version sample_data.json newrelic.conf.j2 wait_for_db.py /var/www/django/

# add the built frontend files from stage 0 to the container
COPY --from=0 /tmp/build-javascript/apps/b3_core/static/js/dist apps/b3_core/static/js/dist
COPY --from=0 /tmp/build-javascript/apps/b3_user_panel/templates/dist apps/b3_user_panel/templates/dist
COPY --from=0 /tmp/build-javascript/jest.json jest.json

# build translations and compress static files/templates
ENV SQL_ENGINE=sqlite
RUN python manage.py compilemessages && \
    python manage.py compilejsi18n && \
    python manage.py collectstatic --no-input -v 0 && \
    python manage.py compress2 -v 0

EXPOSE 8000
CMD ["/bin/start.sh"]
