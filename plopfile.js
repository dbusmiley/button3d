const path = require('path');

module.exports = function (plop) {
    // setGenerator creates a generator that can be run with "plop generatorName"
    plop.setGenerator('Vue Django Container', {
        description: 'Creating a Vue.js Django Container based on 3YOURMINDs Web Application',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'Name of the Container? (Please use Uppercase and spaces for names like "Example Container")',
                validate: function (value) {
                    if (/.+/.test(value)) {
                        return true;
                    }
                    return 'name is required';
                }
            }, {
                type: 'input',
                name: 'scope',
                message: 'Scope of the Container. Could be "apps/b3_user_panel" for example. Please use this option' +
                ', because its important for the webpack output. Anyway you can find the final path inside' +
                './button3d/webpack.config.js file under Plugins.',
                validate: function (value) {
                    if (/.+/.test(value)) {
                        return true;
                    }
                    return 'Scope is required';
                }
            }
        ],
        actions: [{
            type: 'add',
            templateFile: 'frontend/templates/plop/module_name.txt',
            path: 'frontend/templates/{{snakeCase name}}.html',
            abortOnFail: true
        }, {
            type: 'add',
            templateFile: 'frontend/templates/plop/moduleNameContainerJavaScript.txt',
            path: 'frontend/src/{{camelCase name}}.js',
            abortOnFail: true
        }, {
            type: 'add',
            templateFile: 'frontend/templates/plop/ModuleNameContainerVue.txt',
            path: 'frontend/src/{{properCase name}}.vue',
            abortOnFail: true
        }, {
            type: 'modify',
            path: 'button3d/webpack.config.js',
            pattern: /(\/\/ AppendEntries \/\/)/gi,
            template: '{{ camelCase name }}: path.resolve(__dirname, \'..\/frontend\/src\/{{ camelCase name}}.js\'),' +
            '\n        \/\/ AppendEntries \/\/'
        }, {
            type: 'modify',
            path: 'button3d/webpack.config.js',
            pattern: /(\/\/ AppendHtmlWebpackPlugin \/\/)/gi,
            template: `new HtmlWebpackPlugin({
            hash: true,
            title: '{{ sentenceCase name }}',
            scriptName: '{{ camelCase name }}.bundle.js',
            commonFileName: 'common',
            chunks: ['{{ camelCase name }}'],
            inject: false,
            template: path.resolve(__dirname, '../frontend/templates/{{ snakeCase name }}.html'),
            filename: path.resolve(__dirname, '../{{ scope }}/templates/dist/{{ snakeCase name }}.html')
        }),
        // AppendHtmlWebpackPlugin //`
        }]
    });
};
