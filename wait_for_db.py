#!/usr/bin/env python3
# Based on: https://stackoverflow.com/q/52621819/10000573
import os
import sys
import time

import django

os.environ['DJANGO_SETTINGS_MODULE'] = 'button3d.settings'
sys.path.insert(0, os.path.dirname(__file__))
WAIT_INTERVAL = 1


def run():
    from django.db import connections
    from django.db.utils import OperationalError

    db_connection = None
    while not db_connection:
        try:
            db_connection = connections['default']
        except OperationalError:
            print(f'Waiting {WAIT_INTERVAL} more second(s) for database')
            time.sleep(WAIT_INTERVAL)

    print('Database available.')
    sys.exit(0)


if __name__ == '__main__':
    django.setup()
    run()
