# The Button3d Docker Image

This file aims to give full description of the Application inside the docker
container.


## Configuring the Container

Configuration of the container is done through environment variables. These
cannot be build variables as they are not guaranteed to be available during
the build stage, so things like `CROWDIN_API_KEY`, still need to be in the shell
environment. Below is a list of all environment variables that change the behavior
of the container at runtime:

Variable Name                         | Possible Values                      | Default Value                            | Required   | Description
:------------------------------------ | :----------------------------------- | :--------------------------------------- | :--------: | :----------
`SQL_ENGINE`                          | `mysql/psql/mssql`                   | `mysql`                                  | no         | The SQL Adapter to use for connecting to a database, mysql, postgresql or Microsoft SQL
`MIGRATE_DB`                          | `true/false`                         | `false`                                  | no         | Whether or not the database should be migrated upon startup
`LOAD_SAMPLE_DATA`                    | `true/false`                         | `false`                                  | no         | Whether or not the Sample data should be loaded upon startup. This will do nothing, if there is already some data in the database
`DB_HOST`                             | IP or Hostname                       | None                                     | YES        | The Hostname/IP Address of the Database server
`DB_PORT`                             | Any Port Number                      | None                                     | YES        | The Port, on which the Database is listening
`DB_USER`                             | Any String                           | None                                     | YES        | The Username with which the app should authenticate towards the db
`DB_PASSWORD`                         | Any String                           | None                                     | YES        | The password with which the app should authenticate towards the db
`DB_NAME`                             | Any String                           | None                                     | YES        | The Name of the Database the app should connect to
`DB_SCHEMA`                           | Any String                           | `public`                                 | no         | When connecting with a Postgresql-DB, you can specify the schema to use
`DB_SSL`                              | `true/false`                         | `false`                                  | no         | When connecting to a mysql-db, you can turn on TLS Encrypted db-communication. See [Here](#tls-encrypted-mysql-connection) for more information
`BACKEND_API_URL`                     | Any Hostname/Port or Relative Path   | `""`                                     | no         | (DEPRECATED) The Path/Host under which the 3D-Backend is reachable for the user. Should only be set, if not run together with the 3yd-nginx Reverse Proxy
`BACKEND_3D_HOST`                     | Any Hostname                         | `""`                                     | no         | The Host under which the 3D-Backend is reachable from within the button3d docker container. Uses User-facing Host if unset.
`MEDIA_STORAGE`                       | `swift/s3/file`                      | `file`                                   | no         | Which Storage engine for User-Uploads ( Not 3D-Files ) should be used.
`SWIFT_AUTH_URL`                      | Any URL                              | None                                     | no         | The URL of the Openstack Authentication Server
`SWIFT_USERNAME`                      | Any String                           | None                                     | no         | The Username with which to autheticate against the Openstack Auth Server
`SWIFT_PASSWORD`                      | Any String                           | None                                     | no         | The Username with which to autheticate against the Openstack Auth Server
`SWIFT_TENANT_NAME`                   | Any String                           | None                                     | no         | The Tenant to store the files in. Use either this setting XOR `SWIFT_TENANT_ID`
`SWIFT_TENANT_ID`                     | Any String                           | None                                     | no         | The Tenant ID to store the files in Use Either this settings XOR `SWIFT_TENANT_NAME`
`SWIFT_CONTAINER_NAME`                | Any String                           | None                                     | no         | The Name of the Swift container to store the files in
`SWIFT_NAME_PREFIX`                   | Any String                           | None                                     | no         | Specify a prefix ( Folder ) for all files saved ( for example `mediafiles/` if the files should be in that folder
`SWIFT_AUTH_TOKEN_DURATION`           | Time in Seconds                      | `60*59`                                  | no         | The Time between auth token refreshes against the swift auth server. If the uploads throw Unauthenticated errors after an hour, you might want to lower that value.
`S3_REGION`                           | Any AWS Region                       | `eu-central-1`                           | no         | The Region, in which the s3 bucket is in
`S3_BUCKET`                           | Any String                           | None                                     | no         | The Bucket, to which the uploads should eb saved
`S3_PRIVATE_BUCKET`                   | Any String                           | `S3_BUCKET`                              | no         | Set this to use another bucket for private media files.
`S3_ACCESS_KEY`                       | Any String                           | None                                     | no         | The IAM Access Key with which you have write, list and read access to the specified bucket
`S3_SECRET_KEY`                       | Any String                           | None                                     | no         | The IAM Secret Key
`S3_MEDIAFILES_LOCATION`              | Any Path                             | None                                     | no         | Specify a subfolder in the bucket where the mediafiles should be saved
`S3_MEDIA_LINK_EXPIRATION_SECONDS`    | A Number                             | 60                                       | no         | The Duration for which the MEDAI File download links should be valid
`STATICFILE_STORAGE`                  | `file/s3`                            | `file`                                   | no         | Whether the staticfiles should be served from s3 or locally.
`PRIVATE_FILE_STORAGE`                | A Store engine class (string)        | Depends on `STATICFILE_STORAGE`          | no         | Set this equal to the `DEFAULT_FILE_STORAGE` if secure file serving should be disabled. CSV export will include links to attachments if `PRIVATE_FILE_STORAGE == DEFAULT_FILE_STORAGE`.
`S3_STATIC_SYNC`                      | `true/false`                         | `false`                                  | no         | Whether or Not, staticfiles should be synced to s3 before startup
`S3_STATIC_REGION`                    | Any AWS Region                       | None                                     | no         | The Region of the Staticbucket. Needs to be set, when `STATICFILE_STORAGE=s3`
`S3_STATIC_BUCKET`                    | String                               | None                                     | no         | The Bucket, where the Staticfiles are pushed to and served from. Needs to be set, when `STATICFILE_STORAGE=s3`
`S3_STATIC_ACCESS_KEY`                | String                               | None                                     | no         | The IAM Access key used when pushing the staticfiles to s3. Needs to be set, when `STATICFILE_STORAGE=s3`
`S3_STATIC_SECRET_KEY`                | String                               | None                                     | no         | The IAM Secret key used when pushing the staticfiles to s3. Needs to be set, when `STATICFILE_STORAGE=s3`
`S3_STATIC_CUSTOM_DOMAIN`             | Domain                               | None                                     | no         | When you want the Staticfiles to be served via cloudfront or a revproxy in front of s3, specify its domain here.
`RECAPTCHA_ENABLED`                   | Bool                                 | False                                    | no         | Enable reCAPTCHA on request form?
`RECAPTCHA_PRIVATE_KEY`               | String                               | ''                                       | YES        | Go to https://www.google.com/recaptcha/admin and get keys. In domain list add top-level domain, i.e, for *.3yourmind.com, add 3yourmind.com
`RECAPTCHA_PUBLIC_KEY`                | String                               | ''                                       | YES        | Go to https://www.google.com/recaptcha/admin and get keys. In domain list add top-level domain, i.e, for *.3yourmind.com, add 3yourmind.com
`DEBUG`                               | `true/false`                         | `false`                                  | no         | Enables the django debug mode
`ENABLE_SQL_DEBUG`                    | `true/false`                         | `false`                                  | no         | Enable the debug sql toolbar in django
`SECRET_KEY`                          | Any String                           | None                                     | YES        | The Secret key, the app should use to encrypt sessions. The 3D-Backend needs to be configured to use the same key
`DEMO_UUID`                           | Any UUID                             | `00000000-0000-0000-0000-000000000000`   | no         | The UUID of the demo file
`LOG_LEVEL`                           | `debug/info/warn/error`              | `info`                                   | no         | The Log level of the django application
`EMAIL_BACKEND`                       | `file/console/smtp/smtpssl`          | `file`                                   | no         | The Email backend to use.
`EMAIL_HOST`                          | A Valid Hostname                     | None                                     | no         | The Hostname of the smtp server to use
`EMAIL_PORT`                          | A Valid Port number                  | 465                                      | no         | The Port under which the smtp server is reachable
`EMAIL_HOST_USER`                     | Any String                           | None                                     | no         | The Username with which to authenticate against the smtp server
`EMAIL_HOST_PASSWORD`                 | Any String                           | None                                     | no         | The Password with which to authenticate against the smtp server
`FROM_EMAIL_ADDRESS`                  | An Email address                     | `3YOURMIND <localhost@3yourmind.com>`    | no         | The Default email address that should be used when sending email
`EMAIL_USE_TLS`                       | `true/false`                         | `true`                                   | no         | Whether to use an explicitly TLS encrypted Connection to the smtp server
`EMAIL_USE_SSL`                       | `true/false`                         | `false`                                  | no         | Whether or not to use and implicitly TLS encrypted connection to the smtp server
`USE_CELERY`                          | `true/false`                         | `false`                                  | no         | Whether or not Emails should be sent asynchronously with celery and redis
`CELERY_BROKER_HOST`                  | A Hostname                           | `redis`                                  | no         | The Hostname of the redis server celery uses
`CELERY_BROKER_PORT`                  | Integer Port                         | `6379`                                   | no         | The Port of the redis server celery uses
`ENABLE_IPINFO_API`                   | `true/false`                         | `true`                                   | no         | Whether or not, the IPInfo api should be used for geoip. Requires `IP_INFO_TOKEN` to be set
`IP_INFO_TOKEN`                       | Any String                           | None                                     | no         | The Token with which to authorize against the IPInfor API
`DEFAULT_COUNTRY`                     | Two Letter Country code              | `DE`                                     | no         | The Default country set, when no ipinfo could be retrieved
`ENABLE_CURRENCY_API`                 | `true/false`                         | `true`                                   | no         | Whether the currency api for getting exchange rate should be enabled
`COMPRESS_OFFLINE`                    | `true/false`                         | `true`                                   | no         | Whether templates should be compiled on the fly (Online) or precompiled should be used (Offline). Don't change this if you don't know what you're doing
`JSON_WEB_TOKEN_EXPIRATION_SECONDS`   | Time in Seconds                      | `30`                                     | no         | The Duration for which the JWTs for downloading 3D-Data from the 3D-Backend should be valid
`STRIPE_SECRET_KEY`                   | Any String                           | None                                     | no         | If used, the secret key for connecting to stripe payment provider
`STRIPE_PUBLISHABLE_KEY`              | Any String                           | None                                     | no         | The Publshable key for the Stripe integration
`STRIPE_CLIENT_ID`                    | Any String                           | None                                     | no         | The Stripe Client id for connecting to stripe
`B3_CACHE_TIMEOUT`                    | Any Number                           | `5`                                      | no         | The Timeout after which cache entries are invalidated again
`NETS_BASE_URL`                       | Any URL                              | None                                     | no         | The URL to use for the NETS-Payment Provider
`NETS_CLIENT_ID`                      | Any String                           | None                                     | no         | The Client id used for nets-payment integration
`NETS_ORDER_ID_PREFIX`                | Any String                           | None                                     | no         | The Order Prefix for Orders via nets-payments
`ENABLE_SENTRY`                       | `true` or `false`                    | `false`                                  | no         | Whether or Not error reporting to sentry should be enabled. If true, `SENTRY_DSN` and `SENTRY_JS_DSN` are required to be set
`SENTRY_DSN`                          | Any String                           | None                                     | no         | The Sentry DSN for reporting errors for django
`SENTRY_JS_DSN`                       | Any String                           | None                                     | no         | The Public DSN for Sentry to use with the Frontend
`TEST_BASE_DOMAIN`                    | Any Domain                           | `my.3yd`                                 | no         | The Domain Name used for running the tests. No need to change this.
`LOAD_DEMO_USERS`                     | `true/false`                         | `false`                                  | no         | If true, creates an admin, service and user account on `LOAD_DEMO_USERS_DOMAIN`
`LOAD_DEMO_USERS_DOMAIN`              | Any Site Domain present in the DB    | None                                     | no         | The Domain of the Organization, the Demo users should be added
`SAMPLE_DATA_MULTI_DOMAIN`            | Any Domain Name                      | `multi.my.3yd`                           | no         | the Domain name of the Multi Organization for  the Sample Data
`SAMPLE_DATA_SINGLE_DOMAIN`           | Any Domain Name                      | `single.my.3yd`                          | no         | the Domain name of the Single Organization for  the Sample Data
`ENABLE_NEWRELIC`                     | `true` or `false`                    | `false`                                  | no         | Set to true, to enable reporting to newrelic. You will also need to set `NEWRELIC_KEY` and `NEWRELIC_APP`
`NEWRELIC_KEY`                        | String                               | None                                     | no         | The Newrelic api key
`NEWRELIC_APP`                        | String                               | None                                     | no         | the Newrelic Application name
`REDOC_DYNAMIC_SPEC`                  | Filename (String)                    | `openapi.yaml`                           | YES        | For the API Documentation: which specification to render dynamically
`USE_SECURE_COOKIES`                  | `true` or `false`                    | `true`                                   | NO         | Set this to false if HTTP and not HTTPS is used.

### Using Celery

To Start the celery and redis container with docker compose, specify the main docker-compose.yml and the celery one when running docker-compose up:
```
docker-compose -f ./docker-compose.yml -f ./docker-compose-celery.yml up
```

### TLS-encrypted Mysql connection
