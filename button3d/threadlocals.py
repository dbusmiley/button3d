import threading
import logging


logger = logging.getLogger(__name__)


class ThreadLocal(threading.local):
    def __init__(self):
        super(ThreadLocal, self).__init__()
        logger.debug('Initializing %r', self)


_threadlocals = ThreadLocal()
