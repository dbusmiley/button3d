from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.views.generic.base import RedirectView
from django.views.i18n import javascript_catalog
from filebrowser.sites import site

import apps.b3_compare.api_v1_views as v1
from apps.b3_checkout.views.payment_redirect_target_user_panel import \
    payment_redirect_target_view
from apps.b3_compare import views as compare_views
from apps.b3_core import views, ajax_views
from apps.b3_manual_request import views as manual_request_views
from apps.b3_misc import views as views_pages
from apps.b3_organization import admin_views
from apps.b3_signup.forms import CustomSetPasswordForm, ResetPasswordForm
from apps.b3_signup.views import (
    LoginView, extended_registration_verification_pending_view,
    EmailSignupView, EmailSignupSetPasswordView, EmailSignupConfirmView,
    popup_success, EmailSignupExtendedRegistrationView,
    extended_registration_addresses_view,
    extended_registration_addresses_delete_view,
    PrintingServiceWelcomeView, PrintingServiceWelcomeConfirmView
)
from apps.b3_user_panel.views import change_password

admin.autodiscover()

js_info_dict = {
    'packages': ('pages',
                 'uploads',)
}

handler500 = 'apps.b3_core.views.handler500'


urlpatterns = [
    url(r'^u/(?P<uuid>[a-f0-9\-]+)/$',
        lambda request, uuid: redirect('short_uploads_views:upload',
                                       uuid=uuid),
        name='short_uploads'),
    # Auth Plugins
    url(
        r'^auth/',
        include('apps.b3_auth_plugins.urls', namespace="b3_auth_plugins")
    ),
    url(
        r'^api-auth/', include(
            'rest_framework.urls', namespace='rest_framework'
        )
    ),
    url(
        r'^api/v2.0/', include(
            'apps.b3_api.urls', namespace="api_urls"
        )
    ),
]

uploads_patterns = [
    url(r'^(?P<uuid>[a-f0-9\-]+)/$',
        compare_views.compare, name='upload'),

    # Ajax callbacks
    url(r'^(?P<uuid>[a-f0-9\-]+)/generic_upload/$',
        compare_views.generic_upload, name='generic_upload'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_optimized/$',
        compare_views.download_optimized, name='download_optimized'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_original/$',
        compare_views.download_original, name='download_original'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_thumbnail/$',
        compare_views.download_thumbnail, name='download_thumbnail'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_wta/$',
        compare_views.download_wta, name='download_wta'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_support/$',
        compare_views.download_support, name='download_support'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_viewer_original/$',
        compare_views.download_viewer_original,
        name='download_viewer_original'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_viewer_optimized/$',
        compare_views.download_viewer_optimized,
        name='download_viewer_optimized'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_texture_original/$',
        compare_views.download_texture_original,
        name='download_texture_original'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_texture_optimized/$',
        compare_views.download_texture_optimized,
        name='download_texture_optimized'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_viewer_simple/$',
        compare_views.download_viewer_simple, name='download_viewer_simple'),
    url(r'^(?P<uuid>[a-f0-9\-]+)/download_repaired_simple/$',
        compare_views.download_repaired_simple,
        name='download_repaired_simple'),

    # Workaround for Sketchup for Mac.
    # Sketchup will open this url because of some wired CLRF ist appended
    url(r'^(?P<uuid>[a-f0-9\-]+)/\r\n\r\n/$',
        RedirectView.as_view(pattern_name='upload',
                             permanent=False)),  # %0D%0A%0D%0A
]

urlpatterns += i18n_patterns(
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^imprint/',
        RedirectView.as_view(url='https://www.3yourmind.com', permanent=False),
        name='imprint'),
    url(r'^jobs/',
        RedirectView.as_view(url='https://www.3yourmind.com', permanent=False),
        name='jobs'),
    url(r'^terms/',
        RedirectView.as_view(url='https://www.3yourmind.com', permanent=False),
        name='terms'),
    url(r'^contact/',
        RedirectView.as_view(url='https://www.3yourmind.com', permanent=False),
        name='contact'),
    url(r'^uploads/', include(uploads_patterns)),
    url(r'^u/', include(uploads_patterns, namespace='short_uploads_views')),

    # demo
    url(r'^demo/$',
        compare_views.compare,
        name='demo'),

    # misc
    url(r'^error/(?P<error>[a-z0-9\-]+)', views_pages.error, name='error'),
    url(r'^jsi18n/$', javascript_catalog, js_info_dict),
    url(r'^i18n/', include('django.conf.urls.i18n')),


    # Override admin login/logout
    url(r'^admin/login/', admin_views.login, name='login'),
    url(r'^admin/logout/', admin_views.logout, name='logout'),

    # Admin
    url(r'^admin/', include(admin.site.urls)),

    # Status Page
    url(r'^status/', include('apps.status_page.urls')),

    # Allauth
    url(r'^accounts/login/$',
        LoginView.as_view(), name='account_login'),
    url(r'^accounts/logout/$',
        auth_views.logout, {'next_page': 'home'}, name='account_logout'),
    url(r'^accounts/signup/$',
        EmailSignupView.as_view(),
        name='account_email_signup'),
    url(r'^accounts/signup/confirm/(?P<key>\w+)/?$',
        EmailSignupConfirmView.as_view(),
        name='account_email_signup_confirm'),
    url(r'^accounts/signup/set-password/$',
        EmailSignupSetPasswordView.as_view(),
        name='account_email_signup_set_password'),
    url(r'^accounts/signup/extended-registration/$',
        EmailSignupExtendedRegistrationView.as_view(),
        name='account_email_extended_registration'),
    url(r'^accounts/signup/extended-registration-addresses/$',
        extended_registration_addresses_view,
        name='account_email_extended_registration_addresses'),
    url((r'^accounts/signup/extended-registration-addresses'
         '/delete/(?P<user_address_id>[0-9]+)/$'),
        extended_registration_addresses_delete_view,
        name='account_email_extended_registration_addresses_delete'),
    url(r'^accounts/popup-success/$',
        popup_success, name='account_popup_success'),
    url(r'^accounts/password/change/$',
        change_password, name='account_change_password'),
    url(r'^accounts/password/reset/$',
        auth_views.password_reset,
        {'template_name': "account/password_reset.html",
         'password_reset_form': ResetPasswordForm},
        name='account_reset_password'),
    url(r'^accounts/password/reset/done/$',
        auth_views.password_reset_done,
        {'template_name': "account/password_reset_done.html"},
        name='password_reset_done'),
    url((r'^accounts/password/reset/key/'
         '(?P<uidb64>[0-9A-Za-z]+)/(?P<token>.+)/$'),
        auth_views.password_reset_confirm,
        {'template_name': "account/password_reset_from_key.html",
         'set_password_form': CustomSetPasswordForm},
        name='account_reset_password_from_key'),
    url(r'^accounts/password/reset/key/done/$',
        auth_views.password_reset_complete,
        {'template_name': "account/password_reset_from_key_done.html"},
        name='password_reset_complete'),
    url(r'^accounts/verification-pending/$',
        extended_registration_verification_pending_view,
        name='verification_pending'),
    # Printing Service onboarding
    url(r'^accounts/ps_signup/$',
        EmailSignupView.as_view(),
        {"is_ps": True},
        name='account_ps_email_signup'),
    url(r'^accounts/ps_welcome/$',
        PrintingServiceWelcomeView.as_view(),
        name='account_ps_welcome'),
    url(r'^accounts/ps_welcome_confirm/$',
        PrintingServiceWelcomeConfirmView.as_view(),
        name='account_ps_welcome_confirm'),

    # User Panel
    url(r'^user/',
        include('apps.b3_user_panel.urls', namespace="b3_user_panel")),

    # Dashboard - Pro accounts
    url(r'^dashboard/',
        include('apps.b3_organization.urls',
                namespace="b3_organization")),


    # Manual requests
    url(r'^request/$',
        manual_request_views.manual_request,
        name='manual-request'),
    url(r'^request/attachment/ajax$',
        manual_request_views.ManualRequestAtachmentAjaxView.as_view(),
        name='manual-request-attachments'),

    # Used in several places to get all countries
    url(r'^all-countries-ajax/',
        views.get_all_countries_view, name='all-countries-ajax'),
)

urlpatterns += [
    url(r'^admin/filebrowser/',
        include(site.urls)),
    url(r'^grappelli/',
        include('grappelli.urls')),
    url(r'^ckeditor/',
        include('ckeditor_uploader.urls')),
    url(r'^compatibility/$',
        compare_views.upload_file, name='upload-file'),

    # Basket ajax calls
    url(r'^basket-ajax/',
        include('apps.basket.urls', namespace='basket-ajax')),
    url(r'^change_site_configuration/',
        ajax_views.change_site_configuration,
        name='change_site_configuration'),

    # NETS Payment callback redirection target
    url(r'^payment-redirect-target/',
        payment_redirect_target_view, name='payment-redirect-target')
]

# api v1
urlpatterns += [
    url(r'^v1/uploads/(?P<uuid>[a-f0-9\-]+)/$',
        v1.UploadView.as_view(), name='v1_uploads_uuid'),
    url(r'^v1/material-prices/$',
        v1.materials.MaterialPricesView.as_view(),
        name='v1_material_prices'
        ),
    url(r'^v1/supplier-prices/$',
        v1.suppliers.SupplierPricesView.as_view(),
        name='v1_supplier_prices'
        ),
    url(r'^v1/finishes-prices/$',
        v1.finishes.FinishesPricesView.as_view(),
        name='v1_finishes_prices'
        ),
    url(r'^v1/colors/$',
        v1.ColorsView.as_view(), name='v1_colors'),
    url(r'^v1/suppliers/$',
        v1.suppliers.SuppliersView.as_view(), name='v1_suppliers'),
    url(r'^v1/materials/$',
        v1.materials.MaterialsView.as_view(), name='v1_materials'),
    url(r'^v1/finishes/$',
        v1.finishes.FinishesView.as_view(), name='v1_finishes'),
    url(r'^v1/add-to-basket/$',
        v1.add_to_basket_view, name='v1_add_to_basket'),
    url(r'^v1/custom-price/$',
        v1.get_custom_price_view, name='v1_custom_price'),
]

# Always throws exception
urlpatterns += [
    url(r'^exception/$',
        views.exception_view, name='exception'),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

    urlpatterns += [
        url(r'^impersonate/',
            include('apps.b3_impersonate.urls',
                    namespace='impersonate')),
    ]

    if settings.ENABLE_DEBUG_TOOL and not settings.ENABLE_SQL_DEBUG:
        import debug_toolbar
        urlpatterns += [
            url(r'^__debug__/', include(debug_toolbar.urls)),
        ]

    if settings.ENABLE_PROXY:
        # Activate Backend/Yoda proxy in development mode
        from button3d.proxy import proxy_patterns
        urlpatterns += proxy_patterns
