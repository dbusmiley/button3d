# flake8: noqa
import sys
import logging

from button3d.settings import env, is_true

# # # # # # #
# DATABASE  #
# # # # # # #

_SQL_ENGINE = env('SQL_ENGINE') or 'mysql'
if _SQL_ENGINE == 'mssql':
    DATABASES = {
        'default': {
            'ENGINE': 'sql_server.pyodbc',
            'NAME': env('DB_NAME') or 'threeyd',
            'USER': env('DB_USER') or 'threeyd',
            'PASSWORD': env('DB_PASSWORD') or 'threeyd',
            'HOST': env('DB_HOST') or 'db',
            'PORT': env('DB_PORT') or '1433',
            'OPTIONS': {
                'host_is_server': True,
                'unicode_results': True,
                'extra_params': 'tds_version=8.0',
            },
        },
    }

elif _SQL_ENGINE == 'psql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': env('DB_NAME') or 'threeyd',
            'USER': env('DB_USER') or 'threeyd',
            'PASSWORD': env('DB_PASSWORD') or 'threeyd',
            'HOST': env('DB_HOST') or 'db',
            'PORT': env('DB_PORT') or '5432',
            'OPTIONS':  {
                'options': f"-c search_path={env('DB_SCHEMA') or 'public'}"
            }
        },
    }

elif _SQL_ENGINE == 'sqlite':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': env('DB_NAME'),
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
            'OPTIONS': {}
        }
    }

elif _SQL_ENGINE == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': env('DB_NAME') or 'threeyd',
            'USER': env('DB_USER') or 'threeyd',
            'PASSWORD': env('DB_PASSWORD') or 'threeyd',
            'HOST': env('DB_HOST') or 'db',
            'PORT': env('DB_PORT') or '3306',
            'OPTIONS': {
                'ssl': {
                    'ca': '/etc/ssl/db/ca.pem',
                    'cert': '/etc/ssl/db/cert.pem',
                    'key': '/etc/ssl/db/key.pem'
                }
            } if is_true('DB_SSL') else {}
        }
    }


# # # # #
# DEBUG #
# # # # #

DEBUG = is_true('DEBUG')
ENABLE_SQL_DEBUG = is_true('ENABLE_SQL_DEBUG')
ENABLE_DEBUG_TOOL = is_true('ENABLE_DEBUG_TOOL')
ENABLE_PROFILER = is_true('ENABLE_PROFILER')


# # # # # #
# E-MAIL  #
# # # # # #

_EMAIL_BACKEND = env('EMAIL_BACKEND') or 'file'
if _EMAIL_BACKEND == 'smtpssl':
    EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'
elif _EMAIL_BACKEND == 'smtp':
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
elif _EMAIL_BACKEND == 'console':
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
elif _EMAIL_BACKEND == 'file':
    EMAIL_BACKEND = 'button3d.email_backends.HtmlFileBackend'

if is_true('USE_CELERY'):
    CELERY_BROKER_URL = (f"redis://{env('CELERY_BROKER_HOST') or 'redis'}:"
                         f"{env('CELERY_BROKER_PORT') or '6379'}/0")
    CELERY_ACCEPT_CONTENT = {'pickle'}
    CELERY_TASK_SERIALIZER = 'pickle'

    if 'worker' not in sys.argv:
        EMAIL_BACKEND = 'button3d.email_backends.AsyncCeleryEmailBackend'

EMAIL_HOST = env('EMAIL_HOST') or 'email-smtp.eu-west-1.amazonaws.com'
EMAIL_PORT = int(env('EMAIL_PORT') or 465)
EMAIL_HOST_USER = env('EMAIL_HOST_USER') or 'AKIAJX6H2EWHEW3SKXHA'
EMAIL_HOST_PASSWORD = env(
    'EMAIL_HOST_PASSWORD') or 'AtljKJQyPxrSRFqeA5fKYlqPyto4meD3cFuTVUgsnunU'
EMAIL_USE_TLS = is_true('EMAIL_USE_TLS', default=True)
EMAIL_USE_SSL = is_true('EMAIL_USE_SSL')
# The Address from where emails should be sent
SERVER_EMAIL = OSCAR_FROM_EMAIL = DEFAULT_FROM_EMAIL = env(
    'FROM_EMAIL_ADDRESS') or '3YOURMIND <localhost@3yourmind.com>'


# # # # # #
# LOGGING #
# # # # # #

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s|| %(message)s'
        },
    },
    'handlers': {
        'file_debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/django/debug.log',
            'formatter': 'verbose',
        },
        'file_info': {
            'level': 'INFO',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/django/info.log',
            'formatter': 'verbose',
        },
        'file_error': {
            'level': 'ERROR',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/var/log/django/error.log',
            'formatter': 'verbose',
        },
        'console': {
            'level': env('LOG_LEVEL', 'INFO').upper(),
            'class': 'logging.StreamHandler',
        },
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['file_debug', 'file_info', 'file_error', 'console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ['null'],
            'level': 'DEBUG',
            'propagate': False,
        },
    },
}

ENABLE_SENTRY = is_true('ENABLE_SENTRY')
if ENABLE_SENTRY:
    LOGGING['handlers']['sentry'] = {
        'level': 'WARNING',
        'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler'
    }
    LOGGING['loggers']['']['handlers'].append('sentry')

    RAVEN_CONFIG = {
        'dsn': env('SENTRY_DSN'),
        'release': env('B3_VERSION')

    }
    SENTRY_JS_DSN = env('SENTRY_JS_DSN') or (
        'https://90eb0294ccea4158bfa7d5d419778071@sentry.3yourmind.com/5'
    )

logging.getLogger('urllib3').setLevel(logging.CRITICAL)


# # # # # # # # # # #
# EXTERNAL SERVICES #
# # # # # # # # # # #

# ipinfo API config
ENABLE_IPINFO_API = is_true('ENABLE_IPINFO_API', default=True)
IP_INFO_TOKEN = env('IP_INFO_TOKEN')
DEFAULT_COUNTRY = env('DEFAULT_COUNTRY') or 'DE'

# currencylayer API config
ENABLE_CURRENCY_API = is_true('ENABLE_CURRENCY_API', default=True)
CURRENCY_API_KEY = \
    env('CURRENCY_API_KEY') or 'fafd9ff1306bba49b43af75a1c3ff8df'

# stripe API config
STRIPE_SECRET_KEY = \
    env('STRIPE_SECRET_KEY') or 'sk_test_PQN3K2zENOF2Do090ZynbsMQ'
STRIPE_PUBLISHABLE_KEY = \
    env('STRIPE_PUBLISHABLE_KEY') or 'pk_test_PfZvqoZ8cnTPG3HJ6L1WkLqj'
STRIPE_CLIENT_ID = \
    env('STRIPE_CLIENT_ID') or 'ca_7ue4vO1H3seAB26ldjU98aiMd9ML1keV'

# nets API config
NETS_BASE_URL = \
    env('NETS_BASE_URL') or 'http://atportal.postnord.com/api/payment/v1/'
NETS_CLIENT_ID = env('NETS_CLIENT_ID') or '3D-PRINT'
NETS_ORDER_ID_PREFIX = env('NETS_ORDER_ID_PREFIX') or '3D'

# reCAPTCHA API config
RECAPTCHA_ENABLED = is_true('RECAPTCHA_ENABLED')
RECAPTCHA_PUBLIC_KEY = env('RECAPTCHA_PUBLIC_KEY')
RECAPTCHA_PRIVATE_KEY = env('RECAPTCHA_PRIVATE_KEY')

# 3dbackend API config
BACKEND_3D_HOST = env('BACKEND_3D_HOST') or 'http://3dbackend:8080'


# # # # # # # #
# MEDIA FILES #
# # # # # # # #
MEDIA_LINK_EXPIRATION_SECONDS = env('MEDIA_LINK_EXPIRATION_SECONDS') or '600'
_MEDIA_STORAGE = env('MEDIA_STORAGE') or 'file'
if _MEDIA_STORAGE == 'swift':
    DEFAULT_FILE_STORAGE = 'swift.storage.SwiftStorage'
    PRIVATE_FILE_STORAGE = 'swift.storage.SwiftStorage'
    SWIFT_AUTH_URL = env('SWIFT_AUTH_URL')
    SWIFT_USERNAME = env('SWIFT_USERNAME')
    SWIFT_PASSWORD = env('SWIFT_PASSWORD')
    SWIFT_TENANT_NAME = env('SWIFT_TENANT_NAME')
    SWIFT_TENANT_ID = env('SWIFT_TENANT_ID')
    SWIFT_CONTAINER_NAME = env('SWIFT_CONTAINER_NAME')
    SWIFT_NAME_PREFIX = env('SWIFT_NAME_PREFIX')
    SWIFT_AUTH_TOKEN_DURATION = env('SWIFT_AUTH_TOKEN_DURATION') or '60*59'

elif _MEDIA_STORAGE == 's3':
    AWS_S3_REGION_NAME = env('S3_REGION') or 'eu-central-1'
    S3_USE_SIGV4 = AWS_S3_REGION_NAME == 'eu-central-1'

    AWS_STORAGE_BUCKET_NAME = env('S3_BUCKET')
    AWS_PRIVATE_STORAGE_BUCKET_NAME = env('S3_PRIVATE_BUCKET') or \
        AWS_STORAGE_BUCKET_NAME
    # set Access Keys to None if they're empty
    if env('S3_ACCESS_KEY') == '':
        AWS_ACCESS_KEY_ID = None
        AWS_SECRET_ACCESS_KEY = None
    else:
        AWS_ACCESS_KEY_ID = env('S3_ACCESS_KEY')
        AWS_SECRET_ACCESS_KEY = env('S3_SECRET_KEY')

    MEDIAFILES_LOCATION = env('S3_MEDIAFILES_LOCATION')
    DEFAULT_FILE_STORAGE = 'button3d.custom_storages.PublicS3Boto3Storage'
    PRIVATE_FILE_STORAGE = env('PRIVATE_FILE_STORAGE') or \
        'button3d.custom_storages.PrivateS3Boto3Storage'

    AWS_HEADERS = {
        # see http://developer.yahoo.com/performance/rules.html#expires
        'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
        'Cache-Control': 'max-age=94608000',
    }
    MEDIA_URL = (f"https://s3.{env('S3_REGION')}.amazonaws.com/"
                 f"{env('S3_BUCKET')}/{env('S3_MEDIAFILES_LOCATION')}/")
else:
    PRIVATE_FILE_STORAGE = env('PRIVATE_FILE_STORAGE') or \
        'button3d.custom_storages.PrivateFileSystemStorage'


# # # # # # # # #
# STATIC FILES  #
# # # # # # # # #

if env('STATICFILE_STORAGE') == 's3':
    _S3_STATIC_DEFAULT_DOMAIN = (f"s3.{env('S3_STATIC_REGION')}.amazonaws.com/"
                                 f"{env('S3_STATIC_BUCKET')}")
    _S3_STATIC_CUSTOM_DOMAIN = \
        env('S3_STATIC_CUSTOM_DOMAIN') or _S3_STATIC_DEFAULT_DOMAIN
    _STATICFILES_LOCATION = env('S3_STATICFILES_LOCATION') or 'static'
    STATIC_URL = f'https://{_S3_STATIC_CUSTOM_DOMAIN}/{_STATICFILES_LOCATION}/'
    COMPRESS_URL = STATIC_URL
else:
    STATIC_URL = env('STATIC_URL') or '/assets/'

STATIC_ROOT = env('STATIC_ROOT') or '/var/www/assets'
COMPRESS_OFFLINE = is_true('COMPRESS_OFFLINE', default=True)
COMPRESS_ENABLED = is_true('COMPRESS_ENABLED', default=not DEBUG)


# # # # # #
# TESTING #
# # # # # #

TEST_BASE_DOMAIN = env('TEST_BASE_DOMAIN') or 'my.3yd'
TEST_SITE_DOMAIN = 'org.' + TEST_BASE_DOMAIN
TEST_PORT_RANGE = '8081-8082,9000-9200'
TEST_ADDRESSES = f'{TEST_BASE_DOMAIN}:{TEST_PORT_RANGE}'

if {'test', 'testserver'} & set(sys.argv):
    TEST_OUTPUT_VERBOSE = 2  # xmlrunner
    TEST_OUTPUT_DIR = '/var/www/django/test-reports/'  # xmlrunner
    COMPRESS_PRECOMPILERS = ()
    INTERNAL_IPS = ['127.0.0.1']


# # # # #
# MISC  #
# # # # #

SECRET_KEY = \
    env('SECRET_KEY') or '4e%669-tb4_#!t7$*^25ifzw!j%aohn+3h+@o5n_c%==#dy0c9'
JSON_WEB_TOKEN_EXPIRATION_SECONDS = \
    int(env('JSON_WEB_TOKEN_EXPIRATION_SECONDS') or 30)
B3_CACHE_TIMEOUT = int(env('B3_CACHE_TIMEOUT') or 5)
DEMO_UUID = env('DEMO_UUID') or '00000000-0000-0000-0000-000000000000'

# # # # # # #
# SECURITY  #
# # # # # # #

USE_SECURE_COOKIES = is_true('USE_SECURE_COOKIES', True)
if USE_SECURE_COOKIES:
    SESSION_COOKIE_PATH = '/;secure;HttpOnly;SameSite=Lax'
    CSRF_COOKIE_PATH = '/;secure;SameSite=Strict'
    # TODO: remove with oscar
    BASKET_COOKIE_SECURE = True
else:
    SESSION_COOKIE_PATH = '/;HttpOnly;SameSite=Lax'
    CSRF_COOKIE_PATH = '/;SameSite=Strict'
    # TODO: remove with oscar
    BASKET_COOKIE_SECURE = False
