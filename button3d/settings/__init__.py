# flake8: noqa
import sys
from .utils import env, is_true, pymysql_patch


if sys.version_info < (3, 6):
    raise RuntimeError(
        f'This application only supports python>=3.6. '
        f'Your python version is {sys.version}'
    )

pymysql_patch()

from .base import *  # noqa
from .env import *  # noqa

try:
    from .local import *  # noqa
except ImportError:
    pass

if DEBUG:
    if ENABLE_PROXY:
        INSTALLED_APPS += (
            'revproxy',
        )

    if ENABLE_DEBUG_TOOL:
        MIDDLEWARE += (
            'debug_toolbar.middleware.DebugToolbarMiddleware',
        )
        if not ENABLE_SQL_DEBUG:
            DEBUG_TOOLBAR_CONFIG = {
                'SHOW_TOOLBAR_CALLBACK': lambda x: True,
            }

            INSTALLED_APPS += (
                'debug_toolbar',
            )
