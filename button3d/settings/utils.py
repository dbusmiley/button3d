import os


# Fix for pymysql.install_as_MySQLdb()
# https://github.com/PyMySQL/PyMySQL/issues/273
def pymysql_patch():
    from pymysql import install_as_MySQLdb
    install_as_MySQLdb()

    def mysqldb_escape(value, conv_dict):
        from pymysql.converters import encoders
        vtype = type(value)
        # note: you could provide a default:
        # PY2: encoder = encoders.get(vtype, escape_str)
        # PY3: encoder = encoders.get(vtype, escape_unicode)
        encoder = encoders.get(vtype)
        return encoder(value)

    import pymysql
    setattr(pymysql, 'escape', mysqldb_escape)
    del pymysql


def env(key, default=None):
    value = os.getenv(key, default)
    if value is None:
        return ''
    return value


def is_true(key, default=False):
    env_value = env(key).lower()
    if env_value:
        return env_value in ['true', 'yes', 'on', '1']
    return default
