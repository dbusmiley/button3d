# flake8: noqa
import os
from decimal import Decimal

from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(__file__)
    )
)

ALLOWED_HOSTS = ['*']


INSTALLED_APPS = [
    # 3yd Apps
    'apps.b3_compare',
    'apps.b3_attachement',
    'apps.b3_core',
    'apps.b3_user_panel',
    'apps.b3_misc',
    'apps.b3_organization',
    'apps.b3_signup',
    'apps.b3_tests',
    'apps.b3_modal',
    'apps.b3_manual_request',
    'apps.b3_auth_plugins',
    'apps.b3_mes',
    'apps.b3_api',
    'apps.b3_address',
    'apps.b3_migration',  # used only for migration. Deletable afterwards
    'apps.b3_checkout',
    'apps.b3_voucher',
    'apps.b3_shipping',
    'apps.b3_order',
    'apps.status_page',
    'apps.b3_impersonate',
    'modeltranslation',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'compressor',
    'widget_tweaks',  # Oscar dependency
    'statici18n',
    'raven.contrib.django.raven_compat',
    'colorfield',
    'django_truncate',
    'test_without_migrations',
    'django_jenkins',
    'storages',  # See button3d/custom_storages
    'ckeditor',
    'ckeditor_uploader',
    'adminsortable2',
    'stdimage',
    'guardian',
    'mailer',
    'rest_framework',
    'rest_framework.authtoken',
    'actstream',
    'django_filters',
    'snowpenguin.django.recaptcha2',
    'apps.b3_pdf',
    'apps.catalogue',
    'apps.partner',
    'apps.basket',
    'apps.order',  # To be deleted after 3.0.0 roll-out
    'apps.customer',  # To be deleted after 3.0.0 roll-out
    'apps.shipping',  # To be deleted after 3.0.0 roll-out
    'apps.address',  # To be deleted after 3.0.0 roll-out
    'apps.voucher',  # To be deleted after 3.0.0 roll-out
    'apps.offer',  # To be deleted after 3.0.0 roll-out
    'apps.payment',  # To be deleted after 3.0.0 roll-out
]

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'button3d.middlewares.B3CheckHost',
    'apps.b3_organization.middleware.iframe_header.IframeHeaderMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'apps.b3_core.middleware.GlobalRequestMiddleware',
    'button3d.middlewares.LangMiddleware',
    'apps.b3_core.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'apps.basket.middleware.BasketMiddleware',
    'apps.b3_core.middleware.ExceptionsMiddleware',
    'apps.b3_organization.middleware.CurrentSiteMiddleware',
    'apps.b3_user_panel.middleware.ForceVerifyCustomerAddressMiddleware',
    'apps.b3_organization.middleware.http_auth.BasicAuthProtectionMiddleware',
    'apps.b3_core.middleware.UserInteractionTrackingMiddleware',
    'apps.b3_core.middleware.SQLLogToConsoleMiddleware',
    'apps.b3_core.middleware.ProfileMiddleware',
    'apps.b3_logging.middleware.RequestLoggingMiddleware',
    'apps.b3_logging.middleware.RequestUUIDMiddleware',
)

ROOT_URLCONF = 'button3d.urls'

WSGI_APPLICATION = 'button3d.wsgi.application'

# Internationalization
USE_I18N = True
USE_L10N = True

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _('English')),
    ('de', _('German'))
)

DEFAULT_COUNTRY = 'DE'


def gettext_noop(s):
    return s


PAGE_LANGUAGES = (
    ('en', gettext_noop('English')),
    ('de', gettext_noop('German')),
)

LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'),)

TIME_ZONE = 'UTC'
USE_TZ = True

CACHE_BACKEND = "locmem:///?max_entries=5000"

# Precompressors / Minifier for CSS/JS
COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'sass --scss --sourcemap=none {infile} | autoprefixer'),
)

COMPRESS_JS_FILTERS = ['compressor.filters.yuglify.YUglifyJSFilter']
COMPRESS_CSS_FILTERS = ['compressor.filters.yuglify.YUglifyCSSFilter']

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)
OPENAPI_SPEC_FILE = os.path.join(
    BASE_DIR, 'apidocs', 'openapi', 'openapi.yaml'
)

CURRENCY_FIELD_DIGITS = 12
CURRENCY_FIELD_DECIMAL_PLACES = 2

CURRENCY_DECIMAL_PLACES = Decimal('0.01')

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}

THUMBNAIL_DEBUG = False

OSCAR_SHOP_NAME = '3yourmind.com'
BASKET_COOKIE_NAME = 'oscar_open_basket'
BASKET_COOKIE_LIFETIME = 604800

CHOICE_CURRENCY = (
    ('EUR', 'EUR'),
    ('USD', 'USD'),
    ('GBP', 'GBP'),
    ('PLN', 'PLN'),
    ('NOK', 'NOK'),
    ('SEK', 'SEK'),
    ('HUF', 'HUF'),
    ('DKK', 'DKK'),
    ('IDR', 'IDR'),
)

SUPPORTED_CURRENCY_SET = {
    currency for currency, _ in CHOICE_CURRENCY
}

DEFAULT_EXCHANGE_RATES = {
    u'USDIDR': 13672.5,
    u'USDGBP': 0.684833,
    u'USDEUR': 0.894334,
    u'USDNOK': 8.364701,
    u'USDDKK': 6.650398,
    u'USDHUF': 282.570007,
    u'USDSEK': 8.32835,
    u'USDUSD': 1,
    u'USDPLN': 3.97125
}

DEFAULT_CURRENCY = 'EUR'

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin
    'django.contrib.auth.backends.ModelBackend',
    # Our Custom Auth Backend to support Login by E-Mail Address and per Domain
    'apps.b3_organization.auth_backend.DomainEmailBackend',
    'guardian.backends.ObjectPermissionBackend',
    # Custom authentication backends used by auth plugins (use email address,
    # and a bool to check if the request was from an SSO provider)
    'apps.b3_auth_plugins.methods.auth_backend.SSOLoginBackend',
)
ACCOUNT_PASSWORD_MIN_LENGTH = 8
EMAIL_SUBJECT_PREFIX = ACCOUNT_EMAIL_SUBJECT_PREFIX = ""

LOGIN_REDIRECT_URL = "/dashboard/"
LOGIN_URL = 'account_login'

SESSION_REMEMBER = None
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 3

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
]

# override in settings_local.py
# to get keys, go to https://www.google.com/recaptcha/admin
RECAPTCHA_ENABLED = False
RECAPTCHA_PUBLIC_KEY = ''  # aka Site key
RECAPTCHA_PRIVATE_KEY = ''

# Testing fixture
FIXTURE_DIRS = (
    '/apps/b3_tests/fixtures/',
)

# Rich text editor - Config
CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_CONFIG_GENERIC = [
    ['FontSize'],
    ['Bold', 'Italic', 'Underline', '-', 'RemoveFormat'],
    ['NumberedList', 'BulletedList', '-', 'Outdent',
     'Indent', '-', 'JustifyLeft', 'JustifyCenter',
     'JustifyRight', 'JustifyBlock'],
    ['Link', 'Unlink'],
    ['Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe'],
    ['Source'],
    ['Image'],
]

no_image_config = CKEDITOR_CONFIG_GENERIC[:]
no_image_config.remove(['Image'])
no_image_config.remove(['FontSize'])

no_font_size_config = CKEDITOR_CONFIG_GENERIC[:]
no_font_size_config.remove(['FontSize'])

CKEDITOR_CONFIGS = {
    'ckeditor_custom': {
        'skin': 'moono',
        'toolbar': 'Custom',
        'toolbar_Custom': CKEDITOR_CONFIG_GENERIC
    },
    'ckeditor_custom_no_image': {
        'autoParagraph': False,
        'skin': 'moono',
        'toolbar': 'Custom',
        'toolbar_Custom': no_image_config
    },
    'ckeditor_custom_no_font_size': {
        'autoParagraph': False,
        'skin': 'moono',
        'toolbar': 'Custom',
        'toolbar_Custom': no_font_size_config
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': False,
        'DIRS': [],
        'OPTIONS': {
            'builtins': [],
            'loaders': (
                'apps.b3_organization.template.loaders.SiteLoader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.filesystem.Loader'
            ),
            'context_processors': (
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'apps.b3_core.context_processors.core_context_processor',
                'button3d.context_processors.debug',
                'django.template.context_processors.i18n',
                'apps.b3_organization.context_processors.site_organizations',
                'apps.b3_auth_plugins.context_processors.'
                'auth_context_processor'
            ),
            'string_if_invalid': 'TEMPLATE ERROR: Undefined variable: [%s]'
        },
    }
]

ACTSTREAM_SETTINGS = {
    'USE_JSONFIELD': True,
}

ACCOUNT_ADAPTER = 'apps.b3_signup.adapter.B3AccountAdapter'

CSRF_FAILURE_VIEW = 'apps.b3_core.views.csrf_failure'

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'EXCEPTION_HANDLER':
        'apps.b3_api.errors.custom_exceptions.custom_exception_handler',
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
        'apps.b3_core.filters.FieldMappingOrderingFilter',
    ),
}

DATA_UPLOAD_MAX_MEMORY_SIZE = 134217728

SERVICE_PANEL_PATH = '/service-panel/#/'
STRIPE_REDIRECT_PATH = f'{SERVICE_PANEL_PATH}settings/payment-methods/'

STATUS_PAGE_THREADPOOL_SIZE = 10


# Each tuple contains up to 3 items:
# (<monitor class path>, (<args>), {<kwargs>}),
# (<monitor class path>, (<args>)),
# (<monitor class path>, {<kwargs>}),
# (<monitor class path>, ),
STATUS_PAGE_MONITORS = (
    ('apps.status_page.monitors.app_version.ApplicationVersion',),
    ('apps.status_page.monitors.app_settings.ApplicationSettings',
     ('INSTALLED_APPS',)),
    ('apps.status_page.monitors.currency_api.CurrencyAPI',),
    ('apps.status_page.monitors.domain_expiry.DomainExpiryDate',
     ('3yourmind.com', ),
     {'expiry_period_days': 30}),
    ('apps.status_page.monitors.free_mem.FreeMemory',),
    ('apps.status_page.monitors.free_space.FreeSpace',),
    ('apps.status_page.monitors.ipinfo.IPInfoConfig',),
    ('apps.status_page.monitors.remote_server_is_accessible'
     '.RemoteServerIsAccessible',
     {'server_url': 'http://127.0.0.1:8000'}),
    ('apps.status_page.monitors.server_ip.ServerIp',),
    ('apps.status_page.monitors.sites.Sites',),
    ('apps.status_page.monitors.uptime.Uptime',),
    ('apps.status_page.monitors.app_tables.ApplicationTables',),
    ('apps.status_page.monitors.migrations.Migrations',),
    ('apps.status_page.monitors.python_packages.PythonPackages',),
    ('apps.status_page.monitors.threed_backend_upload.ThreeDBackendUpload',),
)

ENVIRONMENT = 'local'
USE_X_FORWARDED_HOST = True
EMAIL_CONTACT = ''
MEDIA_ROOT = '/var/www/media/'
PRIVATE_MEDIA_ROOT = '/var/www/private-media/'
MEDIA_URL = '/media/'
PRIVATE_MEDIA_URL = '/private-media/'
EMAIL_FILE_PATH = '/var/www/mails/'
ENABLE_OPENAPI_SPEC_VALIDATION = False
ENABLE_PROXY = False
