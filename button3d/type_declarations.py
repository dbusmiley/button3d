# flake8: noqa
"""
All (non-builtin or 3rd-party lib) types for current project should be
defined/imported here.
"""
import typing as t
import sys


class Exporter:
    """
    This class can replace a module with itself and this way let that module
    export any identifier. The value of the identifier will be the name
    of the identifier (as a string). Hack was approved by Guido:
    https://mail.python.org/pipermail/python-ideas/2012-May/014969.html
    """

    def __getattr__(self, item: str) -> str:
        return item

    @classmethod
    def export_anything(cls, module_name: str) -> None:
        # We are exploiting duck-typing here, so we need to add an exception
        # to the type checker here.
        sys.modules[module_name] = cls()  # type: ignore


# noinspection PyUnresolvedReferences
if not t.TYPE_CHECKING:
    # if we are running the code, let this module export any identifier.
    # The value of the identifier will always be the name of the identifier.
    # In this example, it would be equivalent to Foo, Bar = 'Foo', 'Bar'
    Exporter.export_anything(__name__)

else:
    # if we are type checking, import/define the actual classes/types
    # We don't have to worry about circular imports here, since
    # the type checker does not actually run the code.

    TupleOfTwoInts = t.Tuple[int, int]
    TupleOfTwoNones = t.Tuple[None, None]
    from django.utils.functional import SimpleLazyObject

    # noinspection PyUnresolvedReferences
    from django.contrib.sites.models import Site

    from django.contrib.sessions.backends.base import SessionBase
    # noinspection PyUnresolvedReferences
    from apps.b3_organization.models import Organization

    from apps.b3_checkout.payment_methods.base import BasePaymentMethod

    # noinspection PyUnresolvedReferences
    from apps.b3_checkout.serializers import \
        CreatePartnerPaymentMethodSerializer

    # noinspection PyUnresolvedReferences
    from apps.b3_address.models import Address

    # noinspection PyUnresolvedReferences
    from apps.b3_checkout.models import Payment, PartnerPaymentMethod, \
        PaymentAttachment, SupportedPaymentMethod
    # noinspection PyUnresolvedReferences
    from django.contrib.auth.models import User

    # noinspection PyUnresolvedReferences
    from apps.partner.models import StripeAccount, Partner, \
        CustomPaymentMethod, StockRecord, PostProcessing

    # noinspection PyUnresolvedReferences
    from apps.catalogue.models.product import Product

    CustomPaymentMethodClass = t.Type[CustomPaymentMethod]

    # noinspection PyUnresolvedReferences
    from apps.b3_migration.models import Switch

    PaymentMethodType = t.Type[BasePaymentMethod]

    PaymentMethodClasses = t.Sequence[t.Tuple[str, PaymentMethodType]]

    PaymentMethodClassesDict = t.Dict[str, PaymentMethodType]

    # noinspection PyUnresolvedReferences
    from apps.b3_core.models import StlFile
    # noinspection PyUnresolvedReferences
    PostProcessings = t.Sequence[PostProcessing]
    # noinspection PyUnresolvedReferences
    from apps.partner.pricing.price import Price
    # noinspection PyUnresolvedReferences
    from apps.b3_voucher.models import Voucher

    from decimal import Decimal
    Number = t.Union[int, float, Decimal]
    NumberOptional = t.Optional[Number]
    StringKeyDict = t.Dict[str, t.Optional[t.Any]]

    from django.http import (
        HttpRequest,
        HttpResponse,
        StreamingHttpResponse,
        JsonResponse,
    )
    MiddlewareOrView = t.Callable[[HttpRequest], HttpResponse]
    DjangoResponse = t.Union[HttpResponse, StreamingHttpResponse, JsonResponse]

    class DjangoAuthenticatedRequest(HttpRequest):
        user = ...

    StrKeyDict = t.Dict[str, t.Any]
    StrKeyValueDict = t.Dict[str, str]

    from django.db.models.query import QuerySet
    from django.http import HttpRequest
    from django.test import RequestFactory
    from django.db.models import QuerySet

    DjangoQuerySet = QuerySet

    Request = t.Union[HttpRequest, RequestFactory]

    from rest_framework.serializers import Serializer
    SerializerClass = t.Type[Serializer]

    # noinspection PyUnresolvedReferences
    from rest_framework.views import Request as DRFRequest

    # noinspection PyUnresolvedReferences
    from apps.partner.pricing.calculators import PartPriceCalculator, \
        ProjectPriceCalculator

    # noinspection PyUnresolvedReferences
    from apps.basket.models import Basket, Comment
    # noinspection PyUnresolvedReferences
    from apps.basket.models import (
        Line,
        Line as BasketLine,
    )
    # noinspection PyUnresolvedReferences
    from apps.basket.managers import BasketManager

    class RequestWithBasket(HttpRequest):
        #: Cookies that should be deleted in this response
        cookies_to_delete: t.List[str] = ...
        #: There should always be a session
        session: SessionBase = ...
        #: BasketMiddleware wants access to request.user
        user: User = ...
        #: BasketMiddleware sets the basket
        basket: t.Union[Basket, SimpleLazyObject] = ...

    # noinspection PyUnresolvedReferences
    from apps.partner.strategy import PurchaseInfo

    # noinspection PyUnresolvedReferences
    from apps.b3_shipping.models import (
        ShippingMethod,
        ShippingMethod as ShippingMethodNew,
        PickupLocation,
    )
    # noinspection PyUnresolvedReferences
    from apps.b3_address.models import Country as CountryNew

    # noinspection PyUnresolvedReferences
    from apps.b3_order.models import (
        Order as OrderNew,
        Order,
        OrderAttachment,
        OrderFee,
        OrderLine,
        OrderLine as OrderLineNew,
        OrderLinePostProcessingOption,
        OrderStatus,
    )

    OrderLinePostProcessingOptionClass = t.Type[OrderLinePostProcessingOption]

    # noinspection PyUnresolvedReferences
    from apps.b3_address.models import Country, Address

    # used in apps.b3_migration
    ModelDescriptorType = t.Dict[
        str,
        t.Union[
            str,
            t.Dict[str, str],
            t.Sequence[str],
            t.Sequence[t.Tuple[str, t.Callable, bool]]
        ]
    ]

    # noinspection PyUnresolvedReferences
    from apps.catalogue.models.colors import Color

    # noinspection PyUnresolvedReferences
    from apps.b3_checkout.order_placement.order_builder import \
        OrderFromProjectBuilder

    # noinspection PyUnresolvedReferences
    from apps.b3_core.mail import EmailMultiAlternatives

    # noinspection PyUnresolvedReferences
    from apps.b3_auth_plugins.models import SiteAuthPluginConfig

    #
    # Order Notes
    #
    # noinspection PyUnresolvedReferences
    from apps.b3_order.models.order_notes import BaseOrderNote, \
        InvoiceSequence, InvoiceNote

    TwoStrTuple = t.Tuple[str, str]

    #
    # Django Rest Framework generic interfaces
    #
    # noinspection PyUnresolvedReferences
    from rest_framework.generics import GenericAPIView

    class FilterBackend:
        def filter_queryset(self,
                            request: HttpRequest,
                            queryset: DjangoQuerySet,
                            view: GenericAPIView) -> DjangoQuerySet: ...

    # noinspection PyUnresolvedReferences
    from django_filters.rest_framework import FilterSet

    # noinspection PyUnresolvedReferences
    from apps.b3_mes.models import (
        AbstractWorkflow,
        AbstractWorkflowStatus,
        Job as MESJob,
        Workflow,
        WorkflowStatus,
        WorkflowTemplate,
        WorkflowTemplateStatus,
        Sequence,
        Workstation,
        JobAttachment,
        PartAttachment
    )

    # noinspection PyUnresolvedReferences
    from apps.b3_api.catalogue.serializers import \
        ProductWithTechnologySerializer

    DjangoView = t.Callable[[HttpRequest, ...], DjangoResponse]
