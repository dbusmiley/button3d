from django.conf import settings
from django.conf.urls import url
from django.views.static import serve
from revproxy.views import ProxyView

"""
This script is deprecated and soon to be removed.
BACKEND_API_URL is removed from the sample settings already.
Use the docker-compose setup instead.
"""


backend_url = getattr(settings, 'BACKEND_API_URL', None)

backend_patterns = [
    url(
        r'^bulk-download(?P<path>.*)',
        ProxyView.as_view(upstream=backend_url + '/bulk-download')
    ),
    url(
        r'^download(?P<path>.*)',
        ProxyView.as_view(upstream=backend_url + '/download')
    ),
    url(
        r'^upload(?P<path>.*)',
        ProxyView.as_view(upstream=backend_url + '/upload')
    ),
    url(
        r'^job(?P<path>.*)',
        ProxyView.as_view(upstream=backend_url + '/job')
    ),
    url(
        r'^v1/uploads/$',
        ProxyView.as_view(upstream=backend_url + '/upload'),
        {'path': ''}
    )
] if backend_url else []


yoda_path = getattr(settings, 'YODA_PATH', None)

yoda_patterns = [
    url(
        r'^service-panel/$',
        serve,
        {
            'document_root': yoda_path + 'psconfig.html',
            'path': ''
        }
    ),
    url(
        r'^service-panel(?P<path>.*)$',
        serve,
        {'document_root': yoda_path}
    ),
    url(
        r'^organization-panel/$',
        serve,
        {
            'document_root': yoda_path + 'organizationPanel.html',
            'path': ''
        }
    ),
    url(
        r'^organization-panel(?P<path>.*)$',
        serve,
        {'document_root': yoda_path}
    ),
    url(
        r'^static(?P<path>.*)$',
        serve,
        {'document_root': yoda_path + 'static/'}
    )
] if yoda_path else []


user_panel_path = getattr(settings, 'USER_PANEL_PATH', None)

user_panel_patterns = [
    url(
        r'^user-panel/$',
        serve,
        {
            'document_root': user_panel_path + 'index.html',
            'path': ''
        }
    ),
    url(
        r'^user-panel(?P<path>.*)$',
        serve,
        {'document_root': user_panel_path}
    ),
] if user_panel_path else []

proxy_patterns = backend_patterns + yoda_patterns + user_panel_patterns

__all__ = ['proxy_patterns']
