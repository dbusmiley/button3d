import logging
import os
import mimetypes
import time

from django.core.mail.backends.base import BaseEmailBackend
from django.conf import settings
from django.core.mail.message import DEFAULT_ATTACHMENT_MIME_TYPE

from apps.b3_core.tasks import send_email


logger = logging.getLogger(__name__)


class AsyncCeleryEmailBackend(BaseEmailBackend):
    """
    This backend forwards all EmailMessage objects to celery task
    `send_messages`, which actually sends them in the background.

    `message.connection` is set to None, because initially it points to the
    current email backend, which is `self`, and leaving it as-is will lead to
    recursive task creation
    """

    def send_messages(self, email_messages):
        for message in email_messages:
            logger.debug(f'Sending email to {message.to}')
            message.connection = None
            send_email.delay(message)


class HtmlFileBackend(BaseEmailBackend):

    def send_messages(self, messages):
        timestamp = time.time()
        msg_dir = os.path.join(settings.EMAIL_FILE_PATH, f'{timestamp}')
        os.makedirs(msg_dir)

        for idx, message in enumerate(messages):

            with open(os.path.join(msg_dir, f'{idx}.raw.log'), 'w') as f:
                print(message.message().as_string(), file=f)

            alternatives = getattr(message, 'alternatives', ())
            for alt_idx, alternative in enumerate(alternatives):
                content, mimetype = alternative
                alt_extension = mimetypes.guess_extension(mimetype)
                if alt_extension is None:
                    alt_extension = f'{DEFAULT_ATTACHMENT_MIME_TYPE}'
                filename = os.path.join(
                    msg_dir,
                    f'{idx}.alternative-{alt_idx}{alt_extension}')

                with open(filename, 'wb') as f:
                    f.write(content.encode('utf8'))

            # Write out attachments
            for att_idx, attachment in enumerate(message.attachments):
                _, content, mimetype = attachment
                if mimetype is None:
                    mimetype = DEFAULT_ATTACHMENT_MIME_TYPE
                extension = mimetypes.guess_extension(mimetype)
                if extension is None:
                    extension = f'{DEFAULT_ATTACHMENT_MIME_TYPE}'
                filename = os.path.join(
                    msg_dir,
                    f'{idx}.attachment-{att_idx}{extension}')
                with open(filename, 'wb') as f:
                    f.write(content)


class PreviewEmailBackend(BaseEmailBackend):
    """
    This backend does not send emails, but opens them locally in default
    application for html or txt files.

    Useful for local testing only for quick email contents preview most
    probably will not work in Docker.

    Linux users: install `xdg-open` before use: `apt install xdg-open`
    Mac users: install `open` before use
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        import sys
        platform_name = sys.platform
        if platform_name.startswith('linux'):
            self._open_cmd = 'xdg-open'
        elif platform_name == 'darwin':
            self._open_cmd = 'open'
        else:
            raise Exception(
                f'Your platform {platform_name} is not supported by '
                f'PreviewEmailBackend')

    def send_messages(self, email_messages):
        from tempfile import NamedTemporaryFile
        import subprocess  # nosec

        for message in email_messages:
            with NamedTemporaryFile(delete=False) as f:
                f.write(message.body.encode())
                subprocess.Popen([self._open_cmd, f.name])  # nosec
            if message.alternatives:
                body = message.alternatives[0][0]
                with NamedTemporaryFile(delete=False) as f:
                    f.write(body.encode())
                    subprocess.Popen([self._open_cmd, f.name])  # nosec
