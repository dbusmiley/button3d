from django.conf import settings
from django.contrib.sites.models import Site
from django.core.exceptions import (DisallowedHost, ObjectDoesNotExist)
from django.shortcuts import redirect
from django.utils import translation

from apps.b3_core.base import BaseMiddleware

from apps.b3_impersonate.utils import get_impersonate_page_url


# Saves Language in Session on every request.
# (But only if the language really changes to
# prevent modifying the session on every request)
class LangMiddleware(BaseMiddleware):
    def __call__(self, request):
        if translation.LANGUAGE_SESSION_KEY in request.session:
            if request.session[translation.LANGUAGE_SESSION_KEY] == \
                    request.LANGUAGE_CODE:
                return self.get_response(request)

        request.session[translation.LANGUAGE_SESSION_KEY] = \
            request.LANGUAGE_CODE
        return self.get_response(request)


class B3CheckHost(BaseMiddleware):
    """
    Checks if the host specified in the request is a valid one.
    This protects agains HTTP Host header attacks.

    This also ensures that get_current_site() will never return None.
    """

    def __call__(self, request):
        try:
            site = Site.objects.get(domain=request.get_host().split(":")[0])
        except Site.DoesNotExist:
            if settings.DEBUG:
                url = get_impersonate_page_url(request)
                if url is not None:
                    return redirect(url)
            raise DisallowedHost('Invalid Host')

        try:
            curr_organization = site.organization
            if curr_organization.is_deleted:
                raise DisallowedHost('Organization is deleted')
        except ObjectDoesNotExist:
            raise DisallowedHost('Site has no organization connected')

        return self.get_response(request)
