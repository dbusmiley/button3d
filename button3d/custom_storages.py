import base64
import hashlib
import calendar
import datetime
import urllib

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from storages.backends.s3boto3 import S3Boto3Storage


class PublicS3Boto3Storage(S3Boto3Storage):
    def __init__(self, *args, **kwargs):
        self.location = settings.MEDIAFILES_LOCATION
        self.default_acl = 'public-read'
        self.querystring_auth = False
        super().__init__(*args, **kwargs)


class PrivateS3Boto3Storage(PublicS3Boto3Storage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default_acl = 'private'
        self.querystring_auth = True
        self.querystring_expire = settings.MEDIA_LINK_EXPIRATION_SECONDS
        self.bucket_name = settings.AWS_PRIVATE_STORAGE_BUCKET_NAME


class PrivateFileSystemStorage(FileSystemStorage):
    """
    Generates a one-time-url for private media files.
    See https://stackoverflow.com/questions/11612032/how-to-generate-a-nginx-secure-link-in-python  # noqa
    """

    def url(self, name):
        secret = urllib.parse.quote_plus(settings.SECRET_KEY)
        url = super().url(name)
        exp = int(settings.MEDIA_LINK_EXPIRATION_SECONDS)
        future = datetime.datetime.utcnow() + datetime.timedelta(seconds=exp)
        expiry = calendar.timegm(future.timetuple())
        secure_link = f"{secret}{url}{expiry}".encode('utf-8')
        hash = hashlib.md5(secure_link).digest()  # nosec
        base64_hash = base64.urlsafe_b64encode(hash)
        str_hash = base64_hash.decode('utf-8').rstrip('=')
        return f'{url}?md5={str_hash}&expires={str(expiry)}'

    def __init__(self, *args, **kwargs):
        location = settings.PRIVATE_MEDIA_ROOT
        base_url = settings.PRIVATE_MEDIA_URL
        super().__init__(location=location, base_url=base_url, *args, **kwargs)
