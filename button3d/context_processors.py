from django.conf import settings
from apps.b3_organization.utils import get_base_url


def debug(request):
    sentry_js_dsn = settings.SENTRY_JS_DSN if settings.ENABLE_SENTRY else None
    email_contact = ''
    if hasattr(settings, 'EMAIL_CONTACT'):
        email_contact = settings.EMAIL_CONTACT
    return {
        'DEBUG': settings.DEBUG,
        'ENVIRONMENT': settings.ENVIRONMENT,
        'SENTRY_JS_DSN': sentry_js_dsn,
        'base_url': get_base_url(),
        'user_is_active': request.user.is_active,
        'EMAIL_CONTACT': email_contact,
    }
