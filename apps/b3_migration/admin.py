from django.conf.urls import url
from django.contrib import admin
from django.http import HttpResponseRedirect

from apps.b3_migration.models import Switch
from apps.b3_organization.models import Organization


def switch_on(modeladmin, request, queryset):
    queryset.update(active=True)


def switch_off(modeladmin, request, queryset):
    queryset.update(active=False)


switch_on.short_description = "Turn on all selected Switches"
switch_off.short_description = "Turn off all selected Switches"


@admin.register(Switch)
class SwitchAdmin(admin.ModelAdmin):
    change_list_template = "b3_migration/switch_changelist.html"
    list_display = (
        'feature',
        'organization',
        'active',
    )

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            url('create-all/', self.create_all_switches),
        ]
        return my_urls + urls

    def create_all_switches(self, request):
        for feature in Switch.FEATURE_CHOICES:
            for organization in Organization.objects.all():
                Switch.objects.get_or_create(
                    feature=feature[0], organization=organization
                )
        return HttpResponseRedirect("../")

    actions = [switch_on, switch_off]
