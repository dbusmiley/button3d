from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_organization.utils import get_current_org


class Switch(models.Model):
    """A feature switch.
    Switches are active, or inactive, per organization.
    """
    NEW_USER_PANEL = 'new_user_panel'
    LEFT_NAVIGATION = 'left_navigation'

    FEATURE_CHOICES = (
        (NEW_USER_PANEL, 'New UserPanel'),
        (LEFT_NAVIGATION, 'Left Navigation'),
    )

    organization = models.ForeignKey(
        'b3_organization.Organization',
        related_name='switches',
        verbose_name=_('Organization'),
    )
    feature = models.CharField(
        choices=FEATURE_CHOICES,
        max_length=32,
        help_text=_('Feature being switched/toggled'),
        verbose_name=_('Feature')
    )
    active = models.BooleanField(
        default=False,
        help_text=_('Is this switch active?'),
        verbose_name=_('Active'),
    )
    note = models.TextField(
        blank=True,
        help_text=_('Switch description'),
        verbose_name=_('Note'),
    )
    creation_date = models.DateTimeField(
        auto_now_add=True,
        help_text=_('Date when this Switch was created.'),
        verbose_name=_('Created'),
    )
    last_modified = models.DateTimeField(
        auto_now=True,
        help_text=_('Date when this Switch was last modified.'),
        verbose_name=_('Last Modified'),
    )

    class Meta:
        verbose_name = _('Switch')
        verbose_name_plural = _('Switches')
        unique_together = ('organization', 'feature')

    def __str__(self):
        """
        String representation of a switch
        :return: Organization name and feature name
        """
        active = 'Active' if self.active else 'Inactive'
        feature = self.get_feature_display()
        return f'{self.organization.showname}: {feature} - {active}'

    @classmethod
    def is_active(cls, feature, organization=None):
        organization = organization or get_current_org()
        try:
            switch = Switch.objects.get(
                feature=feature,
                organization=organization
            )
            return switch.active
        except Switch.DoesNotExist:
            return False
