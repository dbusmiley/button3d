from apps.b3_migration.sync.initial_sync.initial_model_synchronization_base \
    import InitialModelSynchronizationBase
from apps.b3_migration.model_descriptors import (
    country_descriptor,
    oscar_country_descriptor,
)


class InitialCountrySynchronization(InitialModelSynchronizationBase):
    help = 'Sync old countries to new countries'

    new_model_descriptor = country_descriptor
    old_model_to_descriptor_mapping = {
        'Country': oscar_country_descriptor
    }
