from apps.b3_migration.sync.initial_sync.initial_model_synchronization_base \
    import InitialModelSynchronizationBase
from apps.b3_migration.sync.utils import \
    address_old_model_to_descriptor_mapping
from apps.b3_migration.model_descriptors import address_descriptor


class InitialAddressSynchronization(InitialModelSynchronizationBase):
    help = 'Sync old addresses to new addresses'

    new_model_descriptor = address_descriptor
    old_model_to_descriptor_mapping = address_old_model_to_descriptor_mapping
