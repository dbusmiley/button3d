from apps.b3_migration.sync.initial_sync.initial_model_synchronization_base \
    import InitialModelSynchronizationBase
from apps.b3_migration.model_descriptors import (
    shipping_method_new_descriptor,
    shipping_method_old_descriptor,
)


class InitialShippingMethodSynchronization(InitialModelSynchronizationBase):
    help = 'Sync old shipping methods to new ones'

    new_model_descriptor = shipping_method_new_descriptor
    old_model_to_descriptor_mapping = {
        'ShippingMethod': shipping_method_old_descriptor
    }
