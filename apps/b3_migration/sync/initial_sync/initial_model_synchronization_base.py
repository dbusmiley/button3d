import typing as t
from django.db import models
import logging

from apps.b3_migration.sync.utils import \
    sync_source_and_target_models

logger = logging.getLogger(__name__)


class InitialModelSynchronizationBase:
    """
    Model Synchronization Base Class. Used for synchronizing models when
    migrating from old to new models.

    Attributes:

        pre_run_funcs:  Array of Functions
            List of functions to run before sync
            Takes optional kwargs:
                :param buddy_model_class: class
                    Buddy model class e.g. AddressBuddy
                :param new_model_class: class
                        New model class e.g. Address
                :param old_models_classes: list of class
                        List of old model classes that are being unified
                        into a single new class
                        e.g. [OrganizationShippingAddress, UserAddress]


        new_model_descriptor:   dict
            Descriptor dictionary for new model

        old_model_to_descriptor_mapping:    dict
            Dictionary of old model names (keys) and their respective
            descriptor dictionaries (values)
            e.g.
            {
                'Country': oscar_country_descriptor,
            }
            This is needed for the case of multiple old models being converted
            to a single new model.

        PRECAUTION: check :func: `__init__()` docstring for initialization
        arguments needed.
    """
    pre_run_funcs = []

    new_model_descriptor = {}
    old_model_to_descriptor_mapping = {}

    def __init__(
        self,
        buddy_model_class: t.Type[models.Model],
        new_model_class: t.Type[models.Model],
        old_model_classes: t.Sequence[t.Type[models.Model]],
    ):
        """
        Initialization. Historical model classes are passed here from the
        migrations script

        :param buddy_model_class: class
                Buddy model class e.g. AddressBuddy
        :param new_model_class:   class
                New model class e.g. Address
        :param old_model_classes:  list of class
                List of old model classes that are being unified
                into a single new class
                e.g. [OrganizationShippingAddress, UserAddress]
        """
        logger.debug('INIT-SYNC: Starting instantiating initial model sync')
        self.buddy_model_class = buddy_model_class
        self.new_model_class = new_model_class
        self.old_model_classes = old_model_classes

        self.buddy_model_new_field_name = self.new_model_descriptor[
            'field_name_in_buddy']

        self.fields_mapping = self.new_model_descriptor['fields_mapping']
        self.fields_delete = self.new_model_descriptor.get('fields_delete', [])
        self.fields_funcs = self.new_model_descriptor.get('fields_funcs', {})
        logger.debug('INIT-SYNC: Completed instantiating')

    def run(self) -> None:
        """
        1. Runs :list of functions: `pre_run_funcs`
        2. For each old model:
            a. retrieves all instances without corresponding
                new model instances (to avoid duplications on multiple runs)
            b. creates new model instances for instances from previous step
        """
        logger.debug(f'INIT-SYNC: Starting running pre-run functions, number'
                     f' of functions to run {len(self.pre_run_funcs)}')
        for func in self.pre_run_funcs:
            func(
                buddy_model_class=self.buddy_model_class,
                new_model_class=self.new_model_class,
                old_models_class=self.old_model_classes,
            )
        logger.debug('INIT-SYNC: Completed running pre-run functions')

        for old_model in self.old_model_classes:

            old_model_descriptor = self.old_model_to_descriptor_mapping[
                old_model.__name__]
            old_model_related_name_in_buddy = old_model_descriptor[
                'related_name_in_buddy']

            model_manager = old_model_descriptor.get(
                'model_manager', 'objects')
            old_qs = getattr(old_model, model_manager).filter(
                **{old_model_related_name_in_buddy: None})

            logger.debug(f'INIT-SYNC: Starting syncing model '
                         f'{old_model.__name__}. Number of unsynced instances'
                         f'is {old_qs.count()}')
            for old_model_instance in old_qs:
                sync_source_and_target_models(
                    old_model_instance,
                    old_model_descriptor,
                    self.new_model_descriptor,
                    'INIT-SYNC',
                    update=False
                )
