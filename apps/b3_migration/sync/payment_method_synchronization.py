import logging
import typing as t

from django.conf import settings
from django.db import models
from django.db.models import Manager

from apps.b3_checkout.utils \
    import get_next_payment_method_priority_for_partner
import button3d.type_declarations as td

logger = logging.getLogger(__name__)

PAYMENT_METHOD_INVOICE = 'invoice'
PAYMENT_METHOD_PAY_IN_STORE = 'pay-in-store'
PAYMENT_METHOD_VW = 'volkswagen'
PAYMENT_METHOD_PO_UPLOAD = 'po-upload'
PAYMENT_METHOD_STRIPE = 'stripe'
PAYMENT_METHOD_NETS = 'nets'
PAYMENT_METHOD_CUSTOM = 'custom'


SIMPLE_PAYMENT_METHOD_ATTRIBUTE_MAPPING: t.Sequence[t.Tuple[str, str]] = \
    (
        ('invoice_enabled', PAYMENT_METHOD_INVOICE),
        ('pay_in_store_enabled', PAYMENT_METHOD_PAY_IN_STORE),
        ('po_payment_enabled', PAYMENT_METHOD_PO_UPLOAD),
        ('volkswagen_payment_enabled', PAYMENT_METHOD_VW),
        ('nets_payment_enabled', PAYMENT_METHOD_NETS),
)

SIMPLE_PAYMENT_METHOD_TYPE_NAMES: t.Tuple[str] = tuple(
    type_name for _, type_name in SIMPLE_PAYMENT_METHOD_ATTRIBUTE_MAPPING
)

SIMPLE_PAYMENT_METHOD_TYPE_NAME_TO_ATTRIBUTE_NAME_DICT: t.Dict[str, str] = {
    type_name: attr_name
    for attr_name, type_name
    in SIMPLE_PAYMENT_METHOD_ATTRIBUTE_MAPPING
}


class PaymentMethodsOld2NewSync:
    def __init__(
            self,
            partner_model_class: t.Type[td.Partner],
            partner_payment_method_model_class: t.Type[
                td.PartnerPaymentMethod],
            custom_payment_method_class: t.Type[td.CustomPaymentMethod],
            custom_payment_method_buddy_class: t.Type[
                td.CustomPaymentMethodBuddy],
            stripe_account_class):

        self.Partner = self._add_default_manager_to_model_class(
            partner_model_class)
        self.PartnerPaymentMethod = partner_payment_method_model_class
        self.CustomPaymentMethod = \
            self._add_default_manager_to_model_class(
                custom_payment_method_class)
        self.CustomPaymentMethodBuddy = \
            self._add_default_manager_to_model_class(
                custom_payment_method_buddy_class)
        self.StripeAccount = stripe_account_class

    def old2new_create_all_partners_payment_methods(self) -> None:
        """
        Creates records of new implementation of partner payment methods.

        Gets Partner, PartnerPaymentMethod, StripeAccount, CustomPaymentMethod
        model classes as arguments to be able to use this function in Django
        migrations, that use very special model classes
        """

        logger.debug('Started migrating payment methods')

        # for every partner create or delete PPM according to boolean
        # `*_enabled`
        for partner in self.Partner.objects_manager.all():
            logger.debug(
                f'Migrating payment methods for partner {partner.id}')
            self.old2new_sync_all_simple_payment_methods(
                partner=partner,
            )

            self.old2new_sync_stripe_account(
                partner=partner,
            )

            self.old2new_sync_all_custom_payment_methods(
                partner=partner,
            )

        logger.debug('Done migrating payment methods')

    def old2new_sync_all_simple_payment_methods(
            self, partner: td.Partner) -> None:
        """
        Creates payment methods, defined in old-style Partner model. Iterates
        over boolean '*_enabled' boolean attributes and type_name pairs from
        new-style PartnerPaymentMethod and calls
        `old2new_sync_simple_payment_methods` for each.
        """

        attribute_mapping = self._get_attribute_value_to_type_name_mapping(
            partner)

        for source_payment_method_is_enabled, target_payment_type_name \
                in attribute_mapping:
            self.old2new_sync_simple_payment_method(
                partner=partner,
                target_payment_type_name=target_payment_type_name,
                is_enabled=source_payment_method_is_enabled
            )

    def old2new_sync_stripe_account(self, partner: td.Partner) -> None:
        """
        Either creates or deletes stripe payment method for
        """

        if partner.stripe_enabled:
            self.old2new_create_or_update_stripe_payment_method(
                partner=partner,
            )
        else:
            self.old2new_delete_stripe_payment_method(
                partner=partner,
            )

    def old2new_create_or_update_stripe_payment_method(
            self, partner: td.Partner) -> None:
        """
        Creates PartnerPaymentMethod with type 'stripe' for partner, based on
        old-style data

        1. create PartnerPaymentMethod with type_name='stripe'
        2. copy details of StripeAccount into
          PartnerPaymentMethod.config_args
        """

        logger.debug(f'Creating stripe PPM for partner {partner.id}')

        if not partner.stripe_connect_id:
            logger.warning(
                f'Partner {partner.id} has stripe enabled, but does not '
                f'have stripe_connect_id')
            return

        stripe_account = partner.stripe_connect_id

        try:
            payment_method = self.PartnerPaymentMethod.objects.get(
                partner=partner,
                type_name=PAYMENT_METHOD_STRIPE
            )
        except self.PartnerPaymentMethod.DoesNotExist:
            payment_method = self.PartnerPaymentMethod(
                type_name=PAYMENT_METHOD_STRIPE,
                partner=partner,
                is_enabled=True,
                priority=get_next_payment_method_priority_for_partner(
                    partner,
                    self.PartnerPaymentMethod
                )
            )

        logger.debug(
            f'Setting public_config_arguments for PPM '
            f'for partner {partner.id}'
        )
        payment_method.public_config_arguments = {
            'stripe_user_id': stripe_account.stripe_user_id,
            'stripe_publishable_key': stripe_account.stripe_publishable_key,
        }

        logger.debug(
            f'Setting private_config_arguments for PartnerPaymentMethod '
            f'for partner {partner.id}')
        payment_method.private_config_arguments = {
            'access_token': stripe_account.access_token,
            'refresh_token': stripe_account.refresh_token,
            'livemode': stripe_account.livemode,
            'token_type': stripe_account.token_type,
            'scope': stripe_account.scope
        }
        try:
            payment_method.save(target=True)
        except TypeError:
            payment_method.save()
        logger.debug(
            f'Saved stripe PartnerPaymentMethod for partner {partner.id}')

    def old2new_delete_stripe_payment_method(
            self,
            partner: td.Partner,
    ) -> None:
        """
        Deletes new-style stripe PartnerPaymentMethod for `partner`
        """

        logger.debug(
            f'Deleting stripe PartnerPaymentMethod for partner '
            f'{partner.id}')

        try:
            stripe_payment_method = self.PartnerPaymentMethod.objects.get(
                partner=partner,
                type_name=PAYMENT_METHOD_STRIPE
            )
        except self.PartnerPaymentMethod.DoesNotExist:
            pass
        else:
            try:
                stripe_payment_method.delete(target=True)
            except TypeError:
                stripe_payment_method.delete()
            logger.debug(
                f'Done deleting stripe PartnerPaymentMethod for partner '
                f'{partner.id}')

    def old2new_sync_simple_payment_method(
            self,
            partner: td.Partner,
            target_payment_type_name: str,
            is_enabled: bool,
    ) -> None:
        """
        Syncs PartnerPaymentMethod of given `target_payment_type_name`.
        If `is_enabled` is False, PartnerPaymentMethod is deleted
        """

        if is_enabled:
            logger.debug(
                f'Partner {partner.id} {target_payment_type_name} is '
                f'enabled, creating PartnerPaymentMethod')
            try:
                partner_payment_method = self.PartnerPaymentMethod.objects.get(
                    type_name=target_payment_type_name,
                    partner=partner
                )
            except self.PartnerPaymentMethod.DoesNotExist:
                partner_payment_method = self.PartnerPaymentMethod(
                    type_name=target_payment_type_name,
                    partner=partner
                )
            partner_payment_method.is_enabled = True
            partner_payment_method.priority = \
                get_next_payment_method_priority_for_partner(
                    partner,
                    self.PartnerPaymentMethod
                )
            if target_payment_type_name == PAYMENT_METHOD_NETS:
                partner_payment_method.private_config_arguments = {
                    'client_id': settings.NETS_CLIENT_ID,
                    'order_id_prefix': settings.NETS_ORDER_ID_PREFIX,
                    'base_url': settings.NETS_BASE_URL
                }
            try:
                partner_payment_method.save(target=True)
            except TypeError:
                partner_payment_method.save()

        else:
            logger.debug(
                f'Partner {partner.id} {target_payment_type_name} is '
                f'disabled deleting PartnerPaymentMethods:')
            for payment_method in self.PartnerPaymentMethod.objects.filter(
                    type_name=target_payment_type_name,
                    partner=partner
            ):
                logger.debug(f'Deleting {payment_method}')
                try:
                    payment_method.delete(target=True)
                except TypeError:
                    payment_method.delete()

    def old2new_sync_all_custom_payment_methods(
            self, partner: td.Partner) -> None:
        """
        Creates PartnerPaymentMethods from old-style CustomPaymentMethod
        """

        for custom_payment_method in self.CustomPaymentMethod. \
                objects_manager.filter(partner=partner):
            self.old2new_create_or_update_custom_payment_method(
                custom_payment_method,
            )

    def old2new_create_or_update_custom_payment_method(
            self,
            custom_payment_method: td.CustomPaymentMethod,
    ):
        """
        Creates/updates PartnerPaymentMethod

        1. Looks for buddy record, if:
        1.1 exists, update related new-style PartnerPaymentMethod
        1.2 otherwise create both buddy and new-style PartnerPaymentMethod
        """

        kwargs = {
            'type_name': PAYMENT_METHOD_CUSTOM,
            'partner': custom_payment_method.partner,
            'is_enabled': custom_payment_method.is_active,
            'public_config_arguments': {
                'name': custom_payment_method.name,
                'description': custom_payment_method.description,
            }
        }

        try:
            buddy_record = custom_payment_method.payment_method_buddy
        except self.CustomPaymentMethodBuddy.DoesNotExist:
            partner_payment_method = self.PartnerPaymentMethod(
                priority=get_next_payment_method_priority_for_partner(
                    custom_payment_method.partner,
                    self.PartnerPaymentMethod
                ),
                **kwargs
            )
            try:
                partner_payment_method.save(target=True)
            except TypeError:
                partner_payment_method.save()

            self.CustomPaymentMethodBuddy.objects_manager.create(
                old_model=custom_payment_method,
                new_model=partner_payment_method,
            )

        else:
            partner_payment_method = buddy_record.new_model
            for name, value in kwargs.items():
                setattr(partner_payment_method, name, value)
            try:
                partner_payment_method.save(target=True)
            except TypeError:
                partner_payment_method.save()

    def old2new_delete_custom_payment_method(
            self,
            custom_payment_method: td.CustomPaymentMethod,
    ) -> None:
        """
        Deletes new-style implementation of custom payment method
        """

        try:
            buddy_record = custom_payment_method.payment_method_buddy
        except self.CustomPaymentMethodBuddy.DoesNotExist:
            pass
        else:
            try:
                buddy_record.new_model.delete(target=True)
            except TypeError:
                buddy_record.new_model.delete()

    def _get_attribute_value_to_type_name_mapping(
            self,
            partner: td.Partner
    ) -> t.Sequence[t.Tuple[bool, str]]:
        """
        Function returns tuple of tuples-pairs of `partner.*_enabled` values
        and `PartnerPaymentMethod.type_name`s.
        """
        return tuple(
            (getattr(partner, attr_name), type_name)
            for attr_name, type_name
            in SIMPLE_PAYMENT_METHOD_ATTRIBUTE_MAPPING
        )

    def _add_default_manager_to_model_class(
            self,
            model_class: t.Type[models.Model]
    ) -> t.Type[t.Type[models.Model]]:
        """
        Utility function, is used in DB migration context to make sure, that
        all objects get returned my Model.objects_manager.get_queryset(). Takes
        model class and adds custom attribute `objects_manager`=Manager(), that
        has default implementation for .get_queryset() and returns unfiltered
        queryset
        """

        if not hasattr(model_class, 'objects_manager'):
            model_class.add_to_class('objects_manager', Manager())
        return model_class


class PaymentMethodsNew2OldSync:
    def new2old_sync_all_partner_payment_methods(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Syncs creation of PartnerPaymentMethod with old implementation.

        Is called when saving PartnerPaymentMethod.
        """

        if partner_payment_method.type_name \
                in SIMPLE_PAYMENT_METHOD_TYPE_NAMES:
            self._new2old_sync_simple_payment_method(
                partner_payment_method=partner_payment_method,
                is_enabled=partner_payment_method.is_enabled
            )

        elif partner_payment_method.type_name == PAYMENT_METHOD_CUSTOM:
            self._new2old_custom_payment_method_create_or_update_sync(
                partner_payment_method)

        elif partner_payment_method.type_name == PAYMENT_METHOD_STRIPE:
            self._new2old_create_or_update_stripe_payment_method(
                partner_payment_method)

        else:
            raise ValueError(
                f'PartnerPaymentMethod.type_name has '
                f'unexpected value: {partner_payment_method.type_name}')

    def _new2old_create_or_update_stripe_payment_method(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Updates (including enable/disable) Stripe payment details at old model.
        If stripe was disabled at old model, creates missing StripeAccount
        model and updates Partner.
        """

        partner = partner_payment_method.partner
        public_config_arguments = \
            partner_payment_method.public_config_arguments
        private_config_arguments = \
            partner_payment_method.private_config_arguments
        partner.stripe_enabled = partner_payment_method.is_enabled
        # update old stripe account
        stripe_account_attributes = {
            'access_token': private_config_arguments['access_token'],
            'livemode': private_config_arguments['livemode'],
            'refresh_token': private_config_arguments['refresh_token'],
            'token_type': private_config_arguments['token_type'],
            'stripe_publishable_key': public_config_arguments[
                'stripe_publishable_key'],
            'stripe_user_id': public_config_arguments['stripe_user_id'],
            'scope': private_config_arguments['scope']
        }
        if not partner.stripe_connect_id:
            stripe_account = self.StripeAccount(**stripe_account_attributes)
        else:
            stripe_account = partner.stripe_connect_id
            for attr_name, value in stripe_account_attributes.items():
                setattr(stripe_account, attr_name, value)
        try:
            stripe_account.save(target=True)
        except TypeError:
            stripe_account.save()
        partner.stripe_connect_id = stripe_account
        try:
            partner.save(target=True)
        except TypeError:
            partner.save()

    def _new2old_custom_payment_method_create_or_update_sync(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Creates or updates old CustomPaymentMethod model. Enables/sisables it
        accordingly.
        """

        partner = partner_payment_method.partner
        config_arguments = partner_payment_method.public_config_arguments

        try:
            buddy_model = partner_payment_method.payment_method_buddy
        except self.CustomPaymentMethodBuddy.DoesNotExist:
            custom_payment_method = self.CustomPaymentMethod(
                partner=partner,
                name=config_arguments['name'],
                description=config_arguments.get('description', ''),
                is_active=partner_payment_method.is_enabled,
            )
            try:
                custom_payment_method.save(target=True)
            except TypeError:
                custom_payment_method.save()
            self.CustomPaymentMethodBuddy.objects.create(
                old_model=custom_payment_method,
                new_model=partner_payment_method
            )
        else:
            custom_payment_method = buddy_model.old_model
            custom_payment_method.name = config_arguments['name']
            custom_payment_method.description = \
                config_arguments.get('description', '')
            custom_payment_method.is_active = partner_payment_method.is_enabled
            try:
                custom_payment_method.save(target=True)
            except TypeError:
                custom_payment_method.save()

    def new2old_delete_partner_payment_method(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Syncs deletion of `PartnerPaymentMethod` with old implementation.

        Is called when deleting `PartnerPaymentMethod`.
        """

        if partner_payment_method.type_name \
                in SIMPLE_PAYMENT_METHOD_TYPE_NAMES:
            self._new2old_sync_simple_payment_method(
                partner_payment_method=partner_payment_method,
                is_enabled=False
            )

        elif partner_payment_method.type_name == PAYMENT_METHOD_CUSTOM:
            self._new2old_delete_custom_payment_method(partner_payment_method)

        elif partner_payment_method.type_name == PAYMENT_METHOD_STRIPE:
            self._new2old_delete_stripe_payment_method(partner_payment_method)

    def _new2old_sync_simple_payment_method(
            self,
            partner_payment_method: td.PartnerPaymentMethod,
            is_enabled: bool
    ) -> None:
        """
        Enables/disables payment method for `Partner`. Sets `*_enabled`
        attribute to `is_enabled` argument value. Used to sync enable/disable
        of payment method and also when it is deleted.
        """

        attr_name = SIMPLE_PAYMENT_METHOD_TYPE_NAME_TO_ATTRIBUTE_NAME_DICT[
            partner_payment_method.type_name]
        partner = partner_payment_method.partner
        setattr(
            partner,
            attr_name,
            is_enabled
        )
        try:
            partner.save(target=True)
        except TypeError:
            partner.save()

    def _new2old_delete_custom_payment_method(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Synchronizes deletion of new `PartnerPaymentMethod` with old
        `CustomPaymentMethod`
        """

        try:
            buddy_model = partner_payment_method.payment_method_buddy
        except self.CustomPaymentMethodBuddy.DoesNotExist:
            pass
        else:
            custom_payment_method = buddy_model.old_model
            try:
                custom_payment_method.delete(target=True)
            except TypeError:
                custom_payment_method.delete()

    def _new2old_delete_stripe_payment_method(
            self,
            partner_payment_method: td.PartnerPaymentMethod
    ) -> None:
        """
        Synchronizes deletion of new `PartnerPaymentMethod.type_name`=`stripe`
        with old `Partner.stripe_enabled` and `StripeAccount`
        """

        partner = partner_payment_method.partner

        stripe_account = partner.stripe_connect_id

        partner.stripe_enabled = False
        partner.stripe_account_id = ''
        partner.stripe_connect_id = None
        try:
            partner.save(target=True)
        except TypeError:
            partner.save()

        if stripe_account:
            try:
                stripe_account.delete(target=True)
            except TypeError:
                stripe_account.delete()
