import button3d.type_declarations as td
import typing as t


class ShippingMethodCountrySync:
    def __init__(
            self,
            shipping_method_old_model_class: t.Type[td.ShippingMethodOld],
            country_model_old_class: t.Type[td.CountryOld],
            shipping_method_new_model_class: t.Type[td.ShippingMethod],
            country_new_model_class: t.Type[td.Country],
            buddy_model_class: t.Type[td.ShippingMethodBuddy],
    ):
        self.ShippingMethodOld = shipping_method_old_model_class
        self.CountryOld = country_model_old_class
        self.ShippingMethodNew = shipping_method_new_model_class
        self.CountryNew = country_new_model_class
        self.ShippingMethodBuddy = buddy_model_class

    def old_to_new_all_shipping_method_countries_create(self):
        """
        This method is executed from DB migration to populate new Shipping
        Methods country lists
        """

        for shipping_method_old in self.ShippingMethodOld.objects.all():
            country_list = shipping_method_old.countries.values_list(
                'iso_3166_1_a2', flat=True)
            self.old_to_new_shipping_method_countries_create(
                shipping_method_old,
                country_list,
            )

    def old_to_new_shipping_method_countries_create(
            self,
            old_shipping_method: td.ShippingMethodOld,
            countries: t.Sequence[str],
    ):
        buddy_instance: td.ShippingMethodBuddy = \
            self.ShippingMethodBuddy.objects.get(old_model=old_shipping_method)

        new_instance: td.ShippingMethod = buddy_instance.new_model

        existing_countries = new_instance.countries.all()
        for country_name in countries:
            country, _ = self.CountryNew.objects.get_or_create(
                alpha2=country_name
            )
            if country not in existing_countries:
                new_instance.countries.add(country)

    def old_to_new_shipping_method_countries_delete(
            self,
            old_shipping_method: td.ShippingMethodOld,
            countries: t.Sequence[str],
    ):
        buddy_instance: td.ShippingMethodBuddy = \
            self.ShippingMethodBuddy.objects.get(old_model=old_shipping_method)

        new_instance: td.ShippingMethod = buddy_instance.new_model

        countries_list = list(
            self.CountryNew.objects.filter(alpha2__in=countries)
        )

        new_instance.countries.remove(*countries_list)

    def new_to_old_shipping_method_countries_create(
            self,
            new_shipping_method: td.ShippingMethod,
            countries: t.Sequence[str],
    ):
        buddy_instance: td.ShippingMethodBuddy = \
            self.ShippingMethodBuddy.objects.get(new_model=new_shipping_method)

        old_instance: td.ShippingMethodOld = buddy_instance.old_model

        existing_countries = old_instance.countries.all()
        for country_name in countries:
            country, _ = self.CountryOld.objects.get_or_create(
                iso_3166_1_a2=country_name
            )
            if country not in existing_countries:
                old_instance.countries.add(country)

    def new_to_old_shipping_method_countries_delete(
            self,
            new_shipping_method: td.ShippingMethod,
            countries: t.Sequence[str],
    ):
        buddy_instance: td.ShippingMethodBuddy = \
            self.ShippingMethodBuddy.objects.get(new_model=new_shipping_method)

        old_instance: td.ShippingMethodOld = buddy_instance.old_model

        countries_list = list(
            self.CountryOld.objects.filter(iso_3166_1_a2__in=countries)
        )

        old_instance.countries.remove(*countries_list)
