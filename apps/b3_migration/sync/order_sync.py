import json
import logging
import typing as t
from argparse import Action
from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db import models
from django.utils.timezone import now

import button3d.type_declarations as td
from apps.b3_core.utils import cache_result_on_instance
from apps.b3_migration.sync.payment_method_synchronization import \
    PAYMENT_METHOD_INVOICE

logger = logging.getLogger(__name__)


class SkipOrder(Exception):
    pass


class OrderSync:

    STRIPE, INVOICE, PAY_IN_STORE, FREE, PO_UPLOAD, VOLKSWAGEN, NETS = \
        'stripe', 'invoice', 'pay-in-store', \
        'free', 'po-upload', 'volkswagen', 'nets'

    # slug/code as key, tuple of (name, _(verbose_name)) as value
    TYPES = {
        STRIPE: ('Stripe', 'Credit Card'),
        INVOICE: ('Invoice', 'Invoice'),
        PAY_IN_STORE: ('Pay in store', 'Pay in Store'),
        FREE: ('Free', 'Free'),
        PO_UPLOAD: ('PO-Upload', 'Purchase Order'),
        VOLKSWAGEN: ('Volkswagen', 'Diensleistungsvereinbarung (DLV)'),
        NETS: ('Nets', 'Credit Card via NETS'),
    }

    def __init__(
            self,
            order_old_model_class: t.Type[td.OrderOld],
            order_new_model_class: t.Type[td.OrderNew],
            buddy_model_class: t.Type[td.OrderBuddy],
            payment_method_new_model_class: t.Type[td.PartnerPaymentMethod],
            custom_payment_method_old_model_class: td.CustomPaymentMethodClass,
            payment_new_model_class: t.Type[td.Payment],
            shipping_method_old_class: t.Type[td.ShippingMethodOld],
            order_line_new_model_class: t.Type[td.OrderLineNew],
            # flake8: noqa
            order_line_post_processing_option_class: td.OrderLinePostProcessingOptionClass,
            order_attachment_class: t.Type[td.OrderAttachment],
            order_status_model_class: t.Type[td.OrderStatus],
            payment_attachment_class: t.Type[td.PaymentAttachment],
            order_fee_new_class: t.Type[td.OrderFee],
            action_model_class: t.Type[Action],
            address_model_class: t.Type[td.Address],
            user_model_class: t.Type[td.User],
    ):
        self.OrderOld = order_old_model_class
        self.OrderNew = order_new_model_class
        self.OrderBuddy = buddy_model_class
        self.PartnerPaymentMethod = payment_method_new_model_class
        self.CustomPaymentMethodOld = custom_payment_method_old_model_class
        self.PaymentNew = payment_new_model_class
        self.ShippingMethodOld = shipping_method_old_class
        self.OrderLineNew = order_line_new_model_class
        self.OrderLinePostProcessingOption = \
            order_line_post_processing_option_class
        self.OrderAttachment = order_attachment_class
        self.PaymentAttachment = payment_attachment_class
        self.OrderFeeNew = order_fee_new_class
        self.Action = action_model_class
        self.Address = address_model_class
        self.User = user_model_class
        self.OrderStatus = order_status_model_class

    def migrate_orders(self) -> None:
        if hasattr(self.OrderOld, 'all_objects'):
            all_objects_manager = self.OrderOld.all_objects
        else:
            all_objects_manager = self.OrderOld.objects
        for order in all_objects_manager.all():
            try:
                self.sync_order_old_to_new(order_old=order)
            except SkipOrder as exc:
                self._report_skipped_order(order, reason=exc.args[0])

    def sync_order_old_to_new(self, order_old: td.OrderOld) -> None:
        basket = order_old.basket
        order_has_lines = order_old.lines.exists()
        billing_address_old = order_old.billing_address
        payment_old = order_old.sources.first()  # Source

        if not order_has_lines:
            raise SkipOrder('has no lines')

        if not basket:
            raise SkipOrder('has no basket')

        if not billing_address_old:
            raise SkipOrder('has no billing address')

        if not payment_old:
            raise SkipOrder('has no payment')

        if not payment_old.source_type:
            raise SkipOrder('payment has no type')

        if order_old.user:
            user = order_old.user
        else:
            user = self._get_anonymous_user()

        shipping_address_old = order_old.shipping_address

        billing_address_new = billing_address_old.address_buddy.new_address
        if billing_address_new.vat_id != order_old.vat_number:
            self.Address.objects.filter(id=billing_address_new.id).update(
                vat_id=order_old.vat_number
            )

        shipping_address_new = shipping_address_old.address_buddy.new_address

        partner = order_old.lines.first().stockrecord.partner

        # ShippingMethod
        shipping_method_code_old: str = order_old.shipping_code

        if hasattr(self.ShippingMethodOld, 'all_objects'):
            shipping_method_manager = self.ShippingMethodOld.all_objects
        else:
            shipping_method_manager = self.ShippingMethodOld.objects

        shipping_method_old: td.ShippingMethodOld = \
            shipping_method_manager.get(
                code=shipping_method_code_old)

        try:
            shipping_method_new = \
                shipping_method_old.shipping_method_buddy.new_model
        except models.ObjectDoesNotExist:
            raise SkipOrder('has no shipping method')

        min_price_diff, min_price_diff_tax = self._get_min_price_diff(
            order_old
        )

        fees, fees_tax = self._calculate_order_fees(order_old)

        shipping_tax = order_old.shipping_incl_tax \
            - order_old.shipping_excl_tax

        total_tax = order_old.total_incl_tax - order_old.total_excl_tax

        min_price = (order_old.total_excl_tax - order_old.shipping_excl_tax) \
            if min_price_diff else 0
        min_price_tax = (total_tax - shipping_tax) if min_price_diff else 0

        subtotal_value = order_old.total_excl_tax \
            - order_old.shipping_excl_tax \
            - min_price_diff \
            - fees

        subtotal_tax = total_tax \
            - shipping_tax \
            - min_price_diff_tax \
            - fees_tax

        if hasattr(self.OrderNew, 'all_objects'):
            order_manager = self.OrderNew.all_objects
        else:
            order_manager = self.OrderNew.objects

        order_new, order_created = order_manager.get_or_create(
            number=order_old.number,
            defaults={
                'title': basket.title,
                'site': order_old.site,
                'datetime_placed': order_old.date_placed,
                'project': basket,
                'partner': partner,
                'tax_rate': partner.vat_rate,
                'currency': order_old.currency,
                'min_price_diff_value': min_price_diff,
                'min_price_diff_tax': min_price_diff_tax,
                'min_price_value': min_price,
                'min_price_tax': min_price_tax,
                'subtotal_value': subtotal_value,
                'subtotal_tax': subtotal_tax,
                'shipping_value': order_old.shipping_excl_tax,
                'shipping_tax': shipping_tax,
                'fees_value': fees,
                'fees_tax': fees_tax,
                'total_value': order_old.total_excl_tax,
                'total_tax': total_tax,
                'shipping_method': shipping_method_new,
                'tracking_information': order_old.tracking_info or '',
                'delivery_instructions': shipping_address_old.notes,
                'purchased_by_id': user.id,
                'customer_language': order_old.language or 'de',
                'customer_reference': basket.reference or '',
                'billing_address': billing_address_new,
                'shipping_address': shipping_address_new,
                'customer_ip': order_old.customer_ip,
            }
        )

        if order_created:
            logger.info(f'Order {order_new.id} created')
        else:
            logger.info(f'Order {order_new.id} exists')

        self._create_payment(
            payment_old,
            order_old,
            order_new,
            user,
            partner
        )

        self._copy_order_state_changes(order_new, order_old)

        self._create_order_new_lines(order_new, order_old)

        self._create_order_new_attachments(order_old, order_new)

        self._create_order_new_fees(order_old, order_new)

        self._create_order_status(order_old, order_new)

    def _create_payment(
            self,
            payment_old,
            order_old,
            order_new,
            user,
            partner
    ):
        payment_method_new, payment_method_created = \
            self._get_payment_method_new(partner, payment_old)

        payment_information = payment_old.payment_information.first()
        metadata = {}
        if payment_information:
            try:
                metadata = json.loads(payment_information.content)
            except json.JSONDecodeError:
                pass

        payment_new, payment_created = self.PaymentNew.objects.get_or_create(
            order=order_new,
            user_id=user.id,
            currency=order_old.currency,
            amount=payment_old.amount_debited,
            metadata=metadata,
            date_time=order_old.date_placed,
            defaults={
                'payment_method': payment_method_new,
            }
        )

        if payment_created:
            logger.info(f'Payment {payment_new.id} created')
        else:
            logger.info(f'Payment {payment_new.id} exists')

        self._create_po_attachments(payment_new, payment_old)

        if payment_method_created:
            self.PartnerPaymentMethod.objects \
                .filter(id=payment_method_new.id) \
                .update(deleted_date=now())

    def _get_min_price_diff(
            self,
            order: td.OrderOld,
    ) -> t.Tuple[td.Decimal, td.Decimal]:
        try:
            minimal_order_price_fee = order.applied_fees.get(
                type='Minimum Order'
            )
        except models.ObjectDoesNotExist:
            return Decimal('0'), Decimal('0')
        else:
            return minimal_order_price_fee.value, minimal_order_price_fee.tax

    def _create_order_new_lines(
            self,
            order_new: td.OrderNew,
            order_old: td.OrderOld,
    ) -> None:
        for order_line_old in order_old.lines.all():
            item_discount_value = order_line_old.line_price_before_discounts_excl_tax \
                - order_line_old.line_price_excl_tax
            item_discount_tax = (
                order_line_old.line_price_before_discounts_incl_tax
                - order_line_old.line_price_incl_tax) \
                - item_discount_value
            unit_price_tax = order_line_old.unit_price_incl_tax \
                - order_line_old.unit_price_excl_tax
            item_total_tax = order_line_old.line_price_incl_tax \
                - order_line_old.line_price_excl_tax
            line_name = self._get_basket_line_name_for_order_line_old(
                order_line_old,
            )

            order_line_new, order_line_created = \
                self.OrderLineNew.objects.get_or_create(
                    order=order_new,
                    name=line_name,
                    stl_file=order_line_old.stl_file,
                    stock_record=order_line_old.stockrecord,
                    configuration=order_line_old.configuration,
                    unit_price_value=order_line_old.unit_price_excl_tax,
                    unit_price_tax=unit_price_tax,
                    quantity=order_line_old.quantity,
                    item_discount_value=item_discount_value,
                    item_discount_tax=item_discount_tax,
                    item_total_value=order_line_old.line_price_excl_tax,
                    item_total_tax=item_total_tax,
                    instructions='',
                )
            if order_line_created:
                logger.info(f'OrderLine {order_line_new.id} created')
            else:
                logger.info(f'OrderLine {order_line_new.id} exists')

            self._create_post_processings_new(order_line_new, order_line_old)

    def _get_basket_line_name_for_order_line_old(
            self,
            order_line_old: td.OrderLineOld,
    ):
        order_old = order_line_old.order
        basket = order_old.basket
        try:
            basket_line = basket.lines.get(
                stl_file=order_line_old.stl_file,
                stockrecord=order_line_old.stockrecord,
                quantity=order_line_old.quantity,
                deleted_date__isnull=True,
            )
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            return order_line_old.title

        return basket_line.name or order_line_old.title

    def _create_post_processings_new(
            self,
            order_line_new: td.OrderLineNew,
            order_line_old: td.OrderLineOld,
    ) -> None:
        for post_processing_old in order_line_old.post_processing_options.all():
            post_processing_option, post_processing_option_created = \
                self.OrderLinePostProcessingOption.objects.get_or_create(
                    line=order_line_new,
                    post_processing=post_processing_old.post_processing,
                    color=post_processing_old.color,
                )
            if post_processing_option_created:
                logger.info(
                    f'OrderLinePostProcessingOption '
                    f'{post_processing_option.id} created')
            else:
                logger.info(
                    f'OrderLinePostProcessingOption '
                    f'{post_processing_option.id} exists')

    def _copy_order_state_changes(
            self,
            order_new: td.OrderNew,
            order_old: td.OrderOld,
    ) -> None:
        new_order_content_type = ContentType.objects.get_for_model(
            self.OrderNew
        )
        old_order_content_type = ContentType.objects.get_for_model(
            self.OrderOld
        )
        # todo:
        for action_old in self.Action.objects.filter(
                actor_content_type=old_order_content_type.id,
                actor_object_id=order_old.id
        ):
            action, action_created = self.Action.objects.get_or_create(
                actor_object_id=order_new.id,
                actor_content_type_id=new_order_content_type.id,
                verb=action_old.verb,
                description=action_old.description,
                target_content_type=action_old.target_content_type,
                target_object_id=action_old.target_object_id,
                action_object_content_type=action_old.action_object_content_type,
                action_object_object_id=action_old.action_object_object_id,
                timestamp=action_old.timestamp,
                public=action_old.public,
                data=action_old.data,
            )
            if action_created:
                logger.info(f'Action {action.id} created')
            else:
                logger.info(f'Action {action.id} exists')

    def sync_order_state_changes(self):
        new_order_content_type = ContentType.objects.get_for_model(
            self.OrderNew
        )
        old_order_content_type = ContentType.objects.get_for_model(
            self.OrderOld
        )

        actions_old = self.Action.objects.filter(
            actor_content_type=old_order_content_type.id,
        )

        for action_old in actions_old:
            order_old_number = self.OrderOld.objects \
                .only('number') \
                .get(id=action_old.actor_object_id) \
                .number
            try:
                order_new_id = self.OrderNew.objects \
                    .only('id') \
                    .get(number=order_old_number) \
                    .id
            except self.OrderNew.DoesNotExist:
                continue

            action_new, created = self.Action.objects.get_or_create(
                actor_object_id=order_new_id,
                actor_content_type_id=new_order_content_type.id,
                timestamp=action_old.timestamp,
                defaults={
                    'verb': action_old.verb,
                    'description': action_old.description,
                    'target_content_type': action_old.target_content_type,
                    'target_object_id': action_old.target_object_id,
                    'action_object_content_type':
                    action_old.action_objtent_type,
                    'action_object_object_id':
                    action_old.action_object_object_id,
                    'public': action_old.public,
                    'data': action_old.data,
                }
            )
            if not action_new.data:
                action_new.data = action_old.data
                action_new.save()

    def _get_payment_method_new(
            self,
            partner: td.Partner,
            payment_old: td.Source,
    ) -> t.Tuple[td.PartnerPaymentMethod, bool]:
        payment_method_type_old = payment_old.source_type
        payment_method_type_old_slug = payment_method_type_old.code
        if payment_method_type_old_slug in self.TYPES:
            # it is one of pre-defined PM and there is no buddy model. Just
            # look for new PM with type_name=payment_method_type_old_slug

            payment_method, created = \
                self.PartnerPaymentMethod.objects.filter(
                    deleted_date__isnull=True,
                    partner=partner,
                    type_name=payment_method_type_old.code
                ).first(), False
            if not payment_method:
                # this means, payment method does not exist
                # select Invoice payment method
                logger.warning(
                    f'Payment "{payment_method_type_old_slug}" '
                    f'was replaced with "invoice"')
                payment_method, created = \
                    self.PartnerPaymentMethod.objects.get_or_create(
                        partner=partner,
                        type_name=PAYMENT_METHOD_INVOICE,
                        priority=99,
                    )
        else:
            # has buddy model, but not necessarily existing
            try:
                if hasattr(self.CustomPaymentMethodOld, 'all_objects'):
                    manager = self.CustomPaymentMethodOld.all_objects
                else:
                    manager = self.CustomPaymentMethodOld.objects

                custom_payment_method_old = \
                    manager.get(
                        source_type_slug=payment_method_type_old_slug,
                        partner=partner,
                    )
            except self.CustomPaymentMethodOld.DoesNotExist:
                # does not exist anymore
                logger.warning(
                    f'Payment "{payment_method_type_old_slug}" '
                    f'was replaced with "invoice"')
                payment_method, created = \
                    self.PartnerPaymentMethod.objects.get_or_create(
                        partner=partner,
                        type_name=PAYMENT_METHOD_INVOICE,
                        priority=99,
                    )
            else:
                payment_method, created = \
                    custom_payment_method_old.payment_method_buddy.new_model, \
                    False

        return payment_method, created

    @cache_result_on_instance
    def _get_anonymous_user(self) -> td.User:
        return self.User.objects.get(username='AnonymousUser')

    def _create_order_new_attachments(
            self,
            order_old: td.OrderOld,
            order_new: td.OrderNew,
    ) -> None:
        for attachment_old in order_old.basket.attachments.all():
            logger.info(f'Creating attachment for {attachment_old}')

            basket_line = attachment_old.basket_line
            if basket_line:
                # look for corresponding line in new OrderLine
                # and assign it, otherwise just leave blank

                order_line_new = order_new.lines.filter(
                    stock_record=basket_line.stockrecord,
                    stl_file=basket_line.stl_file,
                    quantity=basket_line.quantity,
                ).first()
            else:
                order_line_new = None

            attachment_new, attachment_created = \
                self.OrderAttachment.objects.get_or_create(
                    creation_date=attachment_old.creation_date,
                    filename=attachment_old.filename,
                    filesize=attachment_old.filesize,
                    order=order_new,
                    uploader=attachment_old.uploader,
                    file=attachment_old.file,
                    order_line=order_line_new,
                )
            if attachment_created:
                logger.info(f'OrderAttachment {attachment_new.id} created')
            else:
                logger.info(f'OrderAttachment {attachment_new.id} exists')

    def _calculate_order_fees(self, order_old: td.OrderOld):
        result = order_old.applied_fees \
            .exclude(type='Minimum Order') \
            .aggregate(fees=models.Sum('value'), tax=models.Sum('tax'))
        return result['fees'] or Decimal('0'), result['tax'] or Decimal('0')

    def _create_order_new_fees(
            self,
            order_old: td.OrderOld,
            order_new: td.OrderNew,
    ) -> None:
        for applied_fee_old in order_old.applied_fees.exclude(
                type='Minimum Order',
        ):
            order_fee_new, order_fee_created = \
                self.OrderFeeNew.objects.get_or_create(
                    order=order_new,
                    excl_tax=applied_fee_old.value,
                    tax=applied_fee_old.tax,
                    name=applied_fee_old.type,
                )
            if order_fee_created:
                logger.info(f'OrderFeeNew {order_fee_new.id} created')
            else:
                logger.info(f'OrderFeeNew {order_fee_new.id} exists')

    def _create_po_attachments(
            self,
            payment_new: td.Payment,
            payment_old: td.PaymentOld,
    ) -> None:
        for po_attachment_old in payment_old.attachments.all():
            logger.info(f'Creating payment attachment for {po_attachment_old}')
            attachment_uploader = po_attachment_old.uploader
            order_user = po_attachment_old.payment_source.order.user
            basket_owner = po_attachment_old.payment_source.order.basket.owner
            uploader = attachment_uploader or order_user or basket_owner
            if not uploader:
                # no way to know user
                logger.warning(
                    f'Can not create new PaymentAttachment for '
                    f'{po_attachment_old}, uploader is NULL')
                continue

            po_attachment_new, attachment_created = \
                self.PaymentAttachment.objects.get_or_create(
                    filename=po_attachment_old.filename,
                    filesize=po_attachment_old.filesize,
                    uploader=uploader,
                    file=po_attachment_old.file,
                    creation_date=po_attachment_old.creation_date,
                    defaults={'payment': payment_new}
                )
            if attachment_created:
                logger.info(
                    f'PaymentAttachment {po_attachment_new.id} created')
            else:
                logger.info(f'PaymentAttachment {po_attachment_new.id} exists')

    def _report_skipped_order(
            self,
            order_old: td.OrderOld,
            reason: str,
    ):
        date_placed = order_old.date_placed.strftime('%Y-%m-%d')
        payment_methods_enabled = []
        try:
            partner = order_old.partner
        except AttributeError:
            payment_methods_enabled.append('basket is None')
        else:
            if partner:
                if partner.stripe_enabled:
                    payment_methods_enabled.append('s')
                if partner.invoice_enabled:
                    payment_methods_enabled.append('i')
            else:
                payment_methods_enabled.append('partner is None')

        payment_methods_enabled_str = ' '.join(payment_methods_enabled)

        logger.warning(
            f'Order skipped: {order_old.id};{date_placed} '
            f'({payment_methods_enabled_str}) {reason}'
        )

    def _create_order_status(
            self,
            order_old: td.OrderOld,
            order_new: td.OrderNew,
    ):
        status_new, status_created = self.OrderStatus.objects.get_or_create(
            order_id=order_new.id,
            type=order_old.status,
            created=order_old.date_placed,
        )

        if status_created:
            logger.info(f'OrderStatus {status_new.id} created')
        else:
            logger.info(f'OrderStatus {status_new.id} exists')
