# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def forwards(apps, schema_editor):
    from apps.b3_migration.sync.initial_sync import \
        InitialCountrySynchronization

    buddy_model_class = apps.get_model('b3_migration', 'CountryBuddy')
    new_model_class = apps.get_model('b3_address', 'Country')
    old_models_classes = [apps.get_model('address', 'Country')]
    InitialCountrySynchronization(
        buddy_model_class,
        new_model_class,
        old_models_classes,
    ).run()


class Migration(migrations.Migration):

    initial = False

    dependencies = [
        ('b3_migration', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards, migrations.RunPython.noop),
    ]
