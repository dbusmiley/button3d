# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def forwards(apps, schema_editor):
    from apps.b3_migration.sync.initial_sync import \
        InitialAddressSynchronization

    buddy_model_class = apps.get_model('b3_migration', 'AddressBuddy')
    new_model_class = apps.get_model('b3_address', 'Address')
    old_models_classes = [
        apps.get_model('address', 'UserAddress'),
        apps.get_model('partner', 'PartnerAddress'),
        apps.get_model('order', 'ShippingAddress'),
        apps.get_model('order', 'BillingAddress'),
    ]
    InitialAddressSynchronization(
        buddy_model_class,
        new_model_class,
        old_models_classes,
    ).run()


class Migration(migrations.Migration):

    initial = False

    dependencies = [
        ('b3_migration', '0002_sync_country'),
    ]

    operations = [
        migrations.RunPython(forwards, migrations.RunPython.noop),
    ]
