country_descriptor = {
    'app_name': 'b3_address',
    'model_name': 'Country',
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'CountryBuddy',
    'field_name_in_buddy': 'new_country',
    'related_name_in_buddy': 'country_buddy',

    'fields_mapping': {
        'iso_3166_1_a2': 'alpha2',
        'iso_3166_1_a3': 'alpha3',
        'iso_3166_1_numeric': 'numeric',
        'printable_name': 'name',
        'name': 'official_name'
    },
}
