import copy

from apps.b3_migration.model_descriptors.address_base_descriptor import \
    address_base_descriptor

user_address_descriptor = copy.deepcopy(address_base_descriptor)
user_address_descriptor.update({
    'app_name': 'address',
    'model_name': 'UserAddress',
    'field_name_in_buddy': 'user_address',
})
user_address_descriptor['fields_mapping'].update(
    {
        'verified_status': 'verified_status',
        'user_id': 'user_id',
    })
