oscar_country_descriptor = {
    'app_name': 'address',
    'model_name': 'Country',
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'CountryBuddy',
    'field_name_in_buddy': 'old_country',
    'related_name_in_buddy': 'country_buddy',

    'fields_mapping': {
        'alpha2': 'iso_3166_1_a2',
        'alpha3': 'iso_3166_1_a3',
        'numeric': 'iso_3166_1_numeric',
        'name': 'printable_name',
        'official_name': 'name',
    },
}
