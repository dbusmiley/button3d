# flake8: noqa
from apps.b3_migration.model_descriptors.address_descriptor import \
    address_descriptor
from apps.b3_migration.model_descriptors.billing_address_descriptor \
    import billing_address_descriptor
from apps.b3_migration.model_descriptors.country_descriptor import \
    country_descriptor
from apps.b3_migration.model_descriptors.oscar_country_descriptor import \
    oscar_country_descriptor
from apps.b3_migration.model_descriptors.partner_address_descriptor \
    import partner_address_descriptor
from apps.b3_migration.model_descriptors.shipping_address_descriptor \
    import shipping_address_descriptor
from apps.b3_migration.model_descriptors.user_address_descriptor import \
    user_address_descriptor
from apps.b3_migration.model_descriptors.shipping_method_new_descriptor \
    import shipping_method_new_descriptor
from apps.b3_migration.model_descriptors.shipping_method_old_descriptor \
    import shipping_method_old_descriptor
