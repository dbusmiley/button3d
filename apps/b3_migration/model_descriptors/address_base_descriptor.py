from django.apps import apps


def get_country_pk(source_instance):
    """
    Get corresponding country
    from new model :model: `apps.b3_address.models.Country`

    :param source_instance: instance of the new model
    """
    new_country_alpha2 = source_instance.country.alpha2
    oscar_country_model = apps.get_model('address', 'Country')
    return oscar_country_model.objects.get(iso_3166_1_a2=new_country_alpha2).pk


address_base_descriptor = {
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'AddressBuddy',
    'related_name_in_buddy': 'address_buddy',
    'fields_optional': [
        'same_as_shipping',
        'guest_email_address',
        'notes',
        'vat_id',
    ],
    'fields_mapping': {
        'title': 'title',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'department': 'department',
        'line1': 'line1',
        'line2': 'line2',

        'company_name': 'line3',
        'city': 'line4',
        'zip_code': 'postcode',
    },
    'fields_funcs': [
        ('country_id', get_country_pk, False),
    ],

}
