import copy

from apps.b3_migration.model_descriptors.address_base_descriptor import \
    address_base_descriptor

billing_address_descriptor = copy.deepcopy(address_base_descriptor)
billing_address_descriptor.update({
    'app_name': 'order',
    'model_name': 'BillingAddress',
    'field_name_in_buddy': 'billing_address',
    'model_manager': 'all_objects',
    'read_only': True,
})
