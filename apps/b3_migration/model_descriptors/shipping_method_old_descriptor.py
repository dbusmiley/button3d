from decimal import Decimal

import button3d.type_declarations as td


def sync_price_incl_tax(source_instance: td.ShippingMethod) -> Decimal:
    return source_instance.price_incl_tax


shipping_method_old_descriptor: td.ModelDescriptorType = {
    'app_name': 'shipping',
    'model_name': 'ShippingMethod',
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'ShippingMethodBuddy',
    'field_name_in_buddy': 'old_model',
    'related_name_in_buddy': 'shipping_method_buddy',

    'fields_optional': [
        'description',
        'code',
    ],

    'fields_mapping': {
        'partner_id': 'partner_id',
        'name': 'name',
        'description': 'description',
        'price_excl_tax': 'price_excl_tax',
        'shipping_days_min': 'min_shipping_days',
        'shipping_days_max': 'max_shipping_days',
        'deleted_date': 'deleted_date',
    },

    'fields_funcs': [
        ('price_incl_tax', sync_price_incl_tax, False),
    ]
}
