import copy

from django.apps import apps


def get_country_pk(source_instance):
    """
    Get corresponding country
    from new model :model: `apps.b3_address.models.Country`

    :param source_instance: instance of the old model
    :return corresponding pk of new country model
    """
    old_country_alpha2 = source_instance.country.iso_3166_1_a2
    country_model = apps.get_model('b3_address', 'Country')
    country = country_model.objects.get(alpha2=old_country_alpha2)
    return country.pk


def get_phone_number(source_instance):
    """
    Oscar uses a custom field for phone number that has an underlying class
    :class: `PhoneNumber` so we need to use deepcopy

    :param source_instance: instance of the old model
    :return deepcopy of the phone number which is :object: PhoneNumber
    """
    return copy.deepcopy(source_instance.phone_number)


def get_partner(source_instance):
    """
    If the old model instance is an instance of :model: `PartnerAddress`
    set :field: `partner` to the partner instance related to
    :model instance: `old_model_instance`
    :param source_instance: instance of the old model
    :return partner instance pk
    """
    if (
        source_instance.__class__.__name__ == 'PartnerAddress' and
        hasattr(source_instance, 'partner')
    ):
        return source_instance.partner.pk
    else:
        return None


address_descriptor = {
    'app_name': 'b3_address',
    'model_name': 'Address',
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'AddressBuddy',
    'field_name_in_buddy': 'new_address',
    'related_name_in_buddy': 'address_buddy',
    'fields_optional': [
        'user_id',
        'phone_number',
        'vat_id',
        'verified_status',
    ],
    'fields_mapping': {
        'user_id': 'user_id',
        'title': 'title',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'department': 'department',
        'line1': 'line1',
        'line2': 'line2',
        'verified_status': 'verified_status',

        'line3': 'company_name',
        'line4': 'city',
        'postcode': 'zip_code',
    },

    'fields_funcs': [
        ('country_id', get_country_pk, False),
        ('phone_number', get_phone_number, True),
        ('partner_id', get_partner, False)
    ]
}
