import copy

from apps.b3_migration.model_descriptors.address_base_descriptor import \
    address_base_descriptor

shipping_address_descriptor = copy.deepcopy(address_base_descriptor)
shipping_address_descriptor.update({
    'app_name': 'order',
    'model_name': 'ShippingAddress',
    'field_name_in_buddy': 'shipping_address',
    'model_manager': 'all_objects',
    'read_only': True,
})
