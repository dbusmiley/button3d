def wrap_field(source_field_name):
    """
    Factory function returns another function, that returns value of
    `model_instance` object `source_field_name` attribute
    """

    def zero_if_none(model_instance):
        source_value = getattr(model_instance, source_field_name)
        return source_value or 0
    return zero_if_none


shipping_method_new_descriptor = {
    'app_name': 'b3_shipping',
    'model_name': 'ShippingMethod',
    'buddy_app_name': 'b3_migration',
    'buddy_model_name': 'ShippingMethodBuddy',
    'field_name_in_buddy': 'new_model',
    'related_name_in_buddy': 'shipping_method_buddy',

    'fields_optional': [
        'description',
    ],

    'fields_mapping': {
        'partner_id': 'partner_id',
        'name': 'name',
        'description': 'description',
        'price_excl_tax': 'price_excl_tax',
        'deleted_date': 'deleted_date',
    },

    'fields_funcs': [
        ('shipping_days_min', wrap_field('min_shipping_days'), False),
        ('shipping_days_max', wrap_field('max_shipping_days'), False),
    ]
}
