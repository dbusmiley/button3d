import copy

from apps.b3_migration.model_descriptors.address_base_descriptor import \
    address_base_descriptor

partner_address_descriptor = copy.deepcopy(address_base_descriptor)
partner_address_descriptor.update({
    'app_name': 'partner',
    'model_name': 'PartnerAddress',
    'field_name_in_buddy': 'partner_address',
    'read_only': True,
})
