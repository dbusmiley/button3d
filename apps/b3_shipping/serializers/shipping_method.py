import typing as t

from rest_framework import serializers

from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_shipping.models import ShippingMethod
from apps.basket.models import Basket as Project


class BaseShippingMethodSerializer(serializers.ModelSerializer):
    currency = serializers.CharField(
        source='partner.price_currency',
        read_only=True,
    )

    shippingDaysMin = serializers.IntegerField(source='shipping_days_min')
    shippingDaysMax = serializers.IntegerField(source='shipping_days_max')

    priceExclusiveTax = CurrencyField(
        source='price_excl_tax',
        coerce_to_string=True
    )

    deliveryDays = serializers.SerializerMethodField()

    def get_deliveryDays(
        self, obj: ShippingMethod
    ) -> t.Union[t.Tuple[int, int], None]:
        if 'project_id' in self.context and self.context['project_id']:
            project = Project.all_objects.get(id=self.context['project_id'])
            return project.delivery_time_all_lines_time(obj)
        return None


class ShippingMethodServicePanelSerializer(BaseShippingMethodSerializer):
    class Meta:
        model = ShippingMethod
        fields = (
            'id',
            'name',
            'description',
            'deliveryDays',
            'shippingDaysMin',
            'shippingDaysMax',
            'priceExclusiveTax',
            'currency',
            'countries',
        )


class ShippingMethodUserPanelSerializer(BaseShippingMethodSerializer):
    priceInclusiveTax = CurrencyField(
        source='price_incl_tax',
        coerce_to_string=True,
        read_only=True
    )

    class Meta:
        model = ShippingMethod
        fields = (
            'id',
            'name',
            'description',
            'deliveryDays',
            'shippingDaysMin',
            'shippingDaysMax',
            'priceExclusiveTax',
            'priceInclusiveTax',
            'currency',
        )
