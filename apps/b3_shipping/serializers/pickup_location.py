from rest_framework import serializers

from apps.b3_shipping.models import PickupLocation


class PickupLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = PickupLocation
        fields = (
            'id',
            'location',
            'instructions',
        )
