from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestPickupLocationsUserPanelList(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        self.partner1 = PartnerFactory()
        self.pickup_location1 = PickupLocationFactory(
            partner=self.partner1,
            instructions='After 18:00',
            location='Berlin Hbf'
        )
        self.pickup_location2 = PickupLocationFactory(
            partner=self.partner1,
            instructions='Knock twice',
            location='Dresden Hbf'
        )

        self.partner2 = PartnerFactory()
        self.pickup_location3 = PickupLocationFactory(
            partner=self.partner2,
            instructions='Key word: "Bahamas"',
            location='Hamburg Hbf'
        )

    def test_get_location_list_with_two_items(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/services/'
            f'{self.partner1.id}/pickup-locations/')

        berlin = {
            'id': self.pickup_location1.id,
            'instructions': 'After 18:00',
            'location': 'Berlin Hbf'
        }
        dresden = {
            'id': self.pickup_location2.id,
            'instructions': 'Knock twice',
            'location': 'Dresden Hbf'
        }
        actual = response.json()
        self.assertEqual(200, response.status_code)
        self.assertMembersIn(actual, berlin, dresden)

    def test_get_location_list_with_one_item(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/services/'
            f'{self.partner2.id}/pickup-locations/')

        expected = [
            {'id': self.pickup_location3.id,
             'instructions': 'Key word: "Bahamas"',
             'location': 'Hamburg Hbf'}
        ]
        self.assertEqual(200, response.status_code)
        self.assertEqual(expected, response.json())
