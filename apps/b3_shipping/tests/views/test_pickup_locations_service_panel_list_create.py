import json

from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_shipping.models import PickupLocation
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestPickupLocationsServicePanelListCreate(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(site=self.site)
        self.partner.users.add(self.user)

    def test_create_pickup_location_with_location_only(self):
        data = {
            'location': 'Bismarkstraße 10-12, Berlin'
        }

        response = self.client.post(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/pickup-locations/',
            content_type='application/json',
            data=json.dumps(data),
        )

        expected = {
            'id': PickupLocation.objects.first().id,
            'instructions': '',
            'location': 'Bismarkstraße 10-12, Berlin'
        }

        self.assertEqual(expected, response.json())

    def test_create_pickup_location(self):
        data = {
            'location': 'Bismarkstraße 10-12, Berlin',
            'instructions': '2nd Floor'
        }

        response = self.client.post(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/pickup-locations/',
            content_type='application/json',
            data=json.dumps(data),
        )

        expected = {
            'id': PickupLocation.objects.first().id,
            'location': 'Bismarkstraße 10-12, Berlin',
            'instructions': '2nd Floor',
        }
        self.assertEqual(expected, response.json())

    def test_list_pickup_location(self):
        pickup_location1 = PickupLocationFactory(
            partner=self.partner,
            instructions='After 18:00',
            location='Berlin Hbf'
        )
        pickup_location2 = PickupLocationFactory(
            partner=self.partner,
            instructions='Knock twice',
            location='Dresden Hbf'
        )

        response = self.client.get(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/pickup-locations/',
        )

        berlin = {
            'id': pickup_location1.id,
            'instructions': 'After 18:00',
            'location': 'Berlin Hbf'
        }
        dresden = {
            'id': pickup_location2.id,
            'instructions': 'Knock twice',
            'location': 'Dresden Hbf'
        }
        actual = response.json()
        self.assertMembersIn(actual, berlin, dresden)
