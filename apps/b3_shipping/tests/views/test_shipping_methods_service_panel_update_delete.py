import json
from decimal import Decimal

from apps.b3_address.factories import CountryFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_shipping.models import ShippingMethod
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestShippingMethodsServicePanelUpdateDelete(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(site=self.site)
        self.partner.users.add(self.user)
        self.country_de = CountryFactory(alpha2='DE')
        self.country_it = CountryFactory(alpha2='IT')
        self.country_et = CountryFactory(alpha2='ET')
        self.country_nl = CountryFactory(alpha2='NL')

        self.shipping_method1 = ShippingMethodFactory(
            name='USPS',
            partner=self.partner,
        )
        self.shipping_method2 = ShippingMethodFactory(
            name='DHL',
            partner=self.partner,
        )

        self.shipping_method1.countries.add(self.country_de)
        self.shipping_method1.countries.add(self.country_it)

        self.shipping_method2.countries.add(self.country_de)
        self.shipping_method2.countries.add(self.country_et)

    def test_update_shipping_method(self):
        data = {
            'name': 'Default',
            'countries': ['DE'],
            'shippingDaysMin': 1,
            'shippingDaysMax': 6,
            'priceExclusiveTax': 20
        }

        response = self.client.put(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/shipping-methods/{self.shipping_method1.id}/',
            content_type='application/json',
            data=json.dumps(data),
        )

        expected = {
            'countries': ['DE'],
            'currency': 'EUR',
            'deliveryDays': None,
            'id': self.shipping_method1.id,
            'name': 'Default',
            'description': '',
            'priceExclusiveTax': '20.00',
            'shippingDaysMax': 6,
            'shippingDaysMin': 1
        }

        self.assertEqual(200, response.status_code)
        self.assertEqual(expected, response.json())

        self.shipping_method1.refresh_from_db()
        self.assertEqual('Default', self.shipping_method1.name)
        self.assertEqual(
            Decimal('20.00'), self.shipping_method1.price_excl_tax)
        self.assertEqual(6, self.shipping_method1.shipping_days_max)
        self.assertEqual(1, self.shipping_method1.shipping_days_min)

    def test_delete_shipping_method(self):
        self.assertEqual(2, ShippingMethod.objects.count())

        response = self.client.delete(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/shipping-methods/{self.shipping_method1.id}/',
        )

        self.assertEqual(204, response.status_code)
        self.assertEqual(1, ShippingMethod.objects.count())
