from apps.b3_address.factories import CountryFactory, AddressFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestShippingMethodsList(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        self.partner1 = PartnerFactory()
        self.shipping_method1 = ShippingMethodFactory(
            name='USPS',
            partner=self.partner1,
        )
        self.shipping_method2 = ShippingMethodFactory(
            name='DHL',
            partner=self.partner1,
        )
        self.country_de = CountryFactory(alpha2='DE')
        self.country_it = CountryFactory(alpha2='IT')
        self.country_et = CountryFactory(alpha2='ET')
        self.country_nl = CountryFactory(alpha2='NL')

        self.shipping_method1.countries.add(self.country_de)
        self.shipping_method1.countries.add(self.country_it)

        self.shipping_method2.countries.add(self.country_de)
        self.shipping_method2.countries.add(self.country_et)

    def test_get_all_location_list(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/services/'
            f'{self.partner1.id}/shipping-methods/')

        self.assertEqual(200, response.status_code)
        expected = [
            {'currency': 'EUR',
             'deliveryDays': None,
             'description': '',
             'id': self.shipping_method1.id,
             'name': 'USPS',
             'priceExclusiveTax': '10.00',
             'priceInclusiveTax': '11.90',
             'shippingDaysMax': 4,
             'shippingDaysMin': 2},
            {'currency': 'EUR',
             'deliveryDays': None,
             'description': '',
             'id': self.shipping_method2.id,
             'name': 'DHL',
             'priceExclusiveTax': '10.00',
             'priceInclusiveTax': '11.90',
             'shippingDaysMax': 4,
             'shippingDaysMin': 2}
        ]

        self.assertEqual(expected, response.json())

    def test_get_filtered_shipping_methods_list1(self):
        address = AddressFactory(
            country=self.country_it,
            user=self.user
        )

        response = self.client.get(
            f'/api/v2.0/user-panel/services/{self.partner1.id}/'
            f'shipping-methods/?shippingAddressId={address.id}')

        self.assertEqual(200, response.status_code)
        expected = [
            {'currency': 'EUR',
             'deliveryDays': None,
             'description': '',
             'id': self.shipping_method1.id,
             'name': 'USPS',
             'priceExclusiveTax': '10.00',
             'priceInclusiveTax': '11.90',
             'shippingDaysMax': 4,
             'shippingDaysMin': 2}
        ]

        self.assertEqual(expected, response.json())

    def test_get_filtered_shipping_methods_list2(self):
        address = AddressFactory(
            country=self.country_et,
            user=self.user
        )

        response = self.client.get(
            f'/api/v2.0/user-panel/services/{self.partner1.id}/'
            f'shipping-methods/?shippingAddressId={address.id}')

        self.assertEqual(200, response.status_code)
        expected = [
            {'currency': 'EUR',
             'deliveryDays': None,
             'description': '',
             'id': self.shipping_method2.id,
             'name': 'DHL',
             'priceExclusiveTax': '10.00',
             'priceInclusiveTax': '11.90',
             'shippingDaysMax': 4,
             'shippingDaysMin': 2}
        ]

        self.assertEqual(expected, response.json())

    def test_get_filtered_shipping_methods_empty_list(self):
        address = AddressFactory(
            country=self.country_nl,
            user=self.user
        )

        response = self.client.get(
            f'/api/v2.0/user-panel/services/{self.partner1.id}/'
            f'shipping-methods/?shippingAddressId={address.id}')

        self.assertEqual(200, response.status_code)
        expected = []

        self.assertEqual(expected, response.json())
