import json

from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_shipping.models import PickupLocation
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestPickupLocationsServicePanelUpdateDelete(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(site=self.site)
        self.partner.users.add(self.user)

        self.pickup_location1 = PickupLocationFactory(
            partner=self.partner,
            instructions='After 18:00',
            location='Berlin Hbf'
        )
        self.pickup_location2 = PickupLocationFactory(
            partner=self.partner,
            instructions='Knock twice',
            location='Dresden Hbf'
        )

    def test_update_pickup_location(self):
        data = {
            'location': 'Alexanderplatz',
            'instructions': '-'
        }

        response = self.client.put(
            f'/api/v2.0/service-panel/services/{self.partner.id}'
            f'/pickup-locations/{self.pickup_location1.id}/',
            content_type='application/json',
            data=json.dumps(data),
        )

        expected = {
            'id': PickupLocation.objects.first().id,
            'instructions': '-',
            'location': 'Alexanderplatz'
        }

        self.assertEqual(expected, response.json())

    def test_delete_pickup_location(self):
        self.assertEqual(2, PickupLocation.objects.count())

        response = self.client.delete(
            f'/api/v2.0/service-panel/services/{self.partner.id}'
            f'/pickup-locations/{self.pickup_location1.id}/'
        )
        self.assertEqual(204, response.status_code)

        self.assertEqual(1, PickupLocation.objects.count())
