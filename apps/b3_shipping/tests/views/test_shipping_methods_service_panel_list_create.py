import json

from apps.b3_address.factories import CountryFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_shipping.models import ShippingMethod
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class TestShippingMethodsServicePanelListCreate(AuthenticatedTestCase):

    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(site=self.site)
        self.partner.users.add(self.user)
        self.country_de = CountryFactory(alpha2='DE')
        self.country_it = CountryFactory(alpha2='IT')
        self.country_et = CountryFactory(alpha2='ET')
        self.country_nl = CountryFactory(alpha2='NL')

    def test_create_shipping_method(self):
        self.assertEqual(0, ShippingMethod.objects.count())

        data = {
            'name': 'Default',
            'description': '',
            'countries': ['DE', 'ET'],
            'shippingDaysMin': 3,
            'shippingDaysMax': 5,
            'priceExclusiveTax': 15
        }

        response = self.client.post(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/shipping-methods/',
            content_type='application/json',
            data=json.dumps(data),
        )

        new_shipping_method = ShippingMethod.objects.first()

        expected = {
            'countries': ['DE', 'ET'],
            'currency': 'EUR',
            'deliveryDays': None,
            'id': new_shipping_method.id,
            'name': 'Default',
            'description': '',
            'priceExclusiveTax': '15.00',
            'shippingDaysMax': 5,
            'shippingDaysMin': 3
        }

        self.assertEqual(expected, response.json())

    def test_list_shipping_methods(self):
        self.shipping_method1 = ShippingMethodFactory(
            name='USPS',
            partner=self.partner,
        )
        self.shipping_method2 = ShippingMethodFactory(
            name='DHL',
            partner=self.partner,
        )

        self.shipping_method1.countries.add(self.country_de)
        self.shipping_method1.countries.add(self.country_it)

        self.shipping_method2.countries.add(self.country_de)
        self.shipping_method2.countries.add(self.country_et)

        response = self.client.get(
            f'/api/v2.0/service-panel/services/'
            f'{self.partner.id}/shipping-methods/',
        )

        dhl = {
            'countries': ['DE', 'IT'],
            'currency': 'EUR',
            'deliveryDays': None,
            'id': self.shipping_method1.id,
            'name': 'USPS',
            'description': '',
            'priceExclusiveTax': '10.00',
            'shippingDaysMax': 4,
            'shippingDaysMin': 2
        }
        usps = {
            'countries': ['DE', 'ET'],
            'currency': 'EUR',
            'deliveryDays': None,
            'id': self.shipping_method2.id,
            'name': 'DHL',
            'description': '',
            'priceExclusiveTax': '10.00',
            'shippingDaysMax': 4,
            'shippingDaysMin': 2
        }

        self.assertMembersIn(response.json(), dhl, usps)
