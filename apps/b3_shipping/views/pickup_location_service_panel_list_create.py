from rest_framework.generics import ListCreateAPIView, get_object_or_404

from apps.b3_shipping.views.pickup_location_service_panel_base import \
    PickupLocationServicePanelBase
from apps.partner.models import Partner


class PickupLocationServicePanelListCreate(
    PickupLocationServicePanelBase,
    ListCreateAPIView
):
    def perform_create(self, serializer):
        partner = get_object_or_404(Partner, id=self.kwargs['service_id'])
        serializer.save(partner=partner)
