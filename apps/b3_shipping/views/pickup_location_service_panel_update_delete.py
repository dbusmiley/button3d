from rest_framework.generics import RetrieveUpdateDestroyAPIView, \
    get_object_or_404

from apps.b3_shipping.models import PickupLocation
from apps.b3_shipping.views.pickup_location_service_panel_base import \
    PickupLocationServicePanelBase


class PickupLocationServicePanelRetrieveUpdateDestroy(
    PickupLocationServicePanelBase,
    RetrieveUpdateDestroyAPIView
):
    def get_object(self) -> PickupLocation:
        return get_object_or_404(
            PickupLocation,
            pk=self.kwargs['pickup_location_id'],
            partner=self.kwargs['service_id'],
        )
