from django.db.models import QuerySet
from rest_framework.generics import ListAPIView

from apps.b3_checkout.views.base_service_user_panel import \
    BaseUserPanelServiceView
from apps.b3_shipping.models import PickupLocation
from apps.b3_shipping.serializers.pickup_location \
    import PickupLocationSerializer


class PickupLocationUserPanelList(BaseUserPanelServiceView, ListAPIView):
    serializer_class = PickupLocationSerializer

    def get_queryset(self) -> QuerySet:
        return PickupLocation.objects.filter(
            partner=self.get_service()
        )
