from django.db.models import QuerySet

from apps.b3_api.services.views import BaseServiceView
from apps.b3_shipping.models import PickupLocation
from apps.b3_shipping.serializers.pickup_location \
    import PickupLocationSerializer


class PickupLocationServicePanelBase(BaseServiceView):
    serializer_class = PickupLocationSerializer

    def get_queryset(self) -> QuerySet:
        pickup_locations = PickupLocation.objects.filter(
            partner=self.kwargs['service_id']
        )
        return pickup_locations
