from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.services.permissions import IsServiceRelated
from apps.b3_shipping.models import ShippingMethod
from apps.b3_shipping.serializers.shipping_method \
    import ShippingMethodServicePanelSerializer


class ShippingMethodServicePanelUpdateDelete(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsServiceRelated, IsServiceUser)
    serializer_class = ShippingMethodServicePanelSerializer
    lookup_url_kwarg = 'shipping_method_id'

    def get_queryset(self):
        return ShippingMethod.objects.filter(
            partner=self.kwargs['service_id']
        )
