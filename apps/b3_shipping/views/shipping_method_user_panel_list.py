from django.db.models import QuerySet
from rest_framework.generics import ListAPIView, get_object_or_404

from apps.b3_address.models import Address
from apps.b3_checkout.views.base_service_user_panel \
    import BaseUserPanelServiceView
from apps.b3_shipping.models import ShippingMethod
from apps.b3_shipping.serializers.shipping_method \
    import ShippingMethodUserPanelSerializer


class ShippingMethodUserPanelList(BaseUserPanelServiceView, ListAPIView):
    serializer_class = ShippingMethodUserPanelSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['project_id'] = self.request.query_params.get(
            'projectId', None)
        return context

    def get_queryset(self) -> QuerySet:
        """
        Returns shipping methods for given `service_id`

        Additionally, if `shippingAddressId` is passed, shipping methods are
        filtered by country: only shipping methods, having Address.country are
        selected.
        """

        shipping_methods = ShippingMethod.objects.filter(
            partner=self.get_service()
        )

        address_id = self.request.query_params.get('shippingAddressId')
        if address_id is not None:
            # todo: get_object_or_404() will be replaced with
            # special function, that produces more detailed error message
            # this functionality is in `tech-day-404-handler-refactoring`
            # branch
            address = get_object_or_404(
                Address,
                id=address_id,
                user=self.request.user
            )

            shipping_methods = shipping_methods.filter(
                countries=address.country
            )

        return shipping_methods
