from rest_framework.permissions import IsAuthenticated

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.services.permissions import IsServiceRelated
from apps.b3_shipping.models import ShippingMethod
from apps.b3_shipping.serializers.shipping_method \
    import ShippingMethodServicePanelSerializer

from rest_framework.generics import ListCreateAPIView, get_object_or_404

from apps.partner.models import Partner


class ShippingMethodServicePanelListCreate(ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsServiceRelated, IsServiceUser)
    serializer_class = ShippingMethodServicePanelSerializer

    def get_queryset(self):
        return ShippingMethod.objects.filter(
            partner=self.kwargs['service_id']
        )

    def perform_create(self, serializer):
        partner = get_object_or_404(Partner, id=self.kwargs['service_id'])
        serializer.save(partner=partner)
