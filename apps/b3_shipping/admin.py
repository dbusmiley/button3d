from django.contrib import admin

from apps.b3_shipping.models import PickupLocation, ShippingMethod


@admin.register(ShippingMethod)
class ShippingMethodAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'partner', 'price_excl_tax',
        'shipping_days_min', 'shipping_days_max'
    )
    raw_id_fields = ('partner', )
    list_filter = ('partner', )


@admin.register(PickupLocation)
class PickupLocationAdmin(admin.ModelAdmin):
    list_display = (
        'partner', 'location', 'instructions'
    )
    raw_id_fields = ('partner', )
    list_filter = ('partner', )
