import typing as t

import button3d.type_declarations as td
from apps.b3_core.beautify import pretty_times


def get_delivery_range(
        lines: t.Sequence[td.OrderLine],
        method: td.ShippingMethod,
) -> t.Union[td.TupleOfTwoInts, td.TupleOfTwoNones]:

    min_delivery_days = []
    max_delivery_days = []

    if not lines:
        return None, None

    for line in lines:
        if method:
            delivery_time = method.delivery_time(
                line.stockrecord, line.post_processings
            )
        else:
            delivery_time = 0, 0
        min_delivery_days.append(delivery_time[0])
        max_delivery_days.append(delivery_time[1])

    min_delivery_day = max(min_delivery_days)
    max_delivery_day = max(max_delivery_days)

    return min_delivery_day, max_delivery_day


def get_delivery_range_pretty_printed(
        lines: t.Sequence[td.OrderLine],
        method: td.ShippingMethod,
) -> str:
    """
    Returns '{min} - {max} days' or '{min==max} days'
    """

    min_delivery_day, max_delivery_day = get_delivery_range(lines, method)
    return pretty_times((min_delivery_day, max_delivery_day))
