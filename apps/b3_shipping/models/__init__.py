# flake8: noqa
from apps.b3_shipping.models.pickup_location import PickupLocation
from apps.b3_shipping.models.shipping_method import ShippingMethod
