import typing as t
from decimal import Decimal

from django.db import models
from django.utils.translation import ugettext_lazy as _

import button3d.type_declarations as td
from apps.b3_core.fields import CurrencyField
from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_migration.model_descriptors.shipping_method_new_descriptor \
    import shipping_method_new_descriptor
from apps.b3_migration.model_descriptors.shipping_method_old_descriptor \
    import shipping_method_old_descriptor


class ShippingMethod(AbstractDeletedDateModel):
    partner = models.ForeignKey(
        'partner.Partner',
        related_name='shipping_methods',
    )
    name = models.CharField(_("Name"), max_length=128)
    description = models.TextField(blank=True)

    price_excl_tax = CurrencyField(
        _("Shipping Cost (excluding Tax)"),
    )

    shipping_days_min = models.PositiveIntegerField(
        _('Minimum days for shipping'),
    )

    shipping_days_max = models.PositiveIntegerField(
        _('Maximum days for shipping'),
    )

    countries = models.ManyToManyField(
        'b3_address.Country',
        blank=True,
        verbose_name=_('Countries')
    )

    def __str__(self) -> str:
        return self.name

    @property
    def price(self) -> td.Price:
        """
        Returns Price object based on `self.price_excl_tax`, that contains tax
        and price inclusive tax
        """

        return self.partner.create_price(self.price_excl_tax)

    @property
    def price_incl_tax(self) -> Decimal:
        return self.price.incl_tax

    def get_source_and_target_descriptors(self) -> \
            t.Tuple[td.ModelDescriptorType, td.ModelDescriptorType]:
        return shipping_method_new_descriptor, shipping_method_old_descriptor

    @property
    def shipping_time(self) -> t.Tuple[int, int]:
        return self.shipping_days_min, self.shipping_days_max

    def delivery_time(
            self,
            stockrecord: td.StockRecord,
            post_processings: t.Optional[td.PostProcessings] = None
    ) -> t.Tuple[int, int]:

        _post_processings = post_processings or ()

        min_prod_time, max_prod_time = stockrecord.production_time
        for post_processing in _post_processings:
            min_prod_time += post_processing.production_days_min
            max_prod_time += post_processing.production_days_max
        min_delivery_days = min_prod_time + self.shipping_days_min
        max_delivery_days = max_prod_time + self.shipping_days_max
        return min_delivery_days, max_delivery_days

    class Meta:
        ordering = ('pk',)
