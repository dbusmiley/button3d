from django.db import models

from apps.b3_core.models import AbstractDeletedDateModel


class PickupLocation(AbstractDeletedDateModel):
    partner = models.ForeignKey('partner.Partner')
    location = models.CharField(max_length=200)
    instructions = models.CharField(max_length=200, blank=True, default='')

    def __str__(self):
        return self.location

    class Meta:
        ordering = ('pk',)
