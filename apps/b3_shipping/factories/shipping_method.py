from decimal import Decimal

import factory

from apps.b3_address.factories import CountryFactory
from apps.b3_shipping.models import ShippingMethod
from apps.partner.models import Partner


class ShippingMethodFactory(factory.django.DjangoModelFactory):
    partner = factory.LazyFunction(Partner.objects.order_by('?').first)
    name = factory.Iterator((
        'DHL',
        'UPS',
        'USPS',
        'PostNord',
    ))

    price_excl_tax = Decimal('10')
    shipping_days_min = 2
    shipping_days_max = 4

    class Meta:
        model = ShippingMethod

    @factory.post_generation
    def countries(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for country in extracted:
                self.countries.add(country)
        else:
            self.countries.add(CountryFactory(alpha2='DE'))
