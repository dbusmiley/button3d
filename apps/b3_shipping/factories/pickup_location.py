import factory

from apps.b3_shipping.models import PickupLocation
from apps.partner.models import Partner


class PickupLocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PickupLocation

    partner = factory.LazyFunction(Partner.objects.order_by('?').first)
    location = factory.Iterator((
        'Berlin Hbf',
        'Dresden Hbf',
        'Hamburg Hbf',
    ))
    instructions = factory.Iterator((
        'After 18:00',
        'Knock twice',
        'Key word: "Bahamas"',
        'Any time',
    ))
