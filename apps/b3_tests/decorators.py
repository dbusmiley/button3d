import requests
from unittest import skip


def skipIfOffline():
    if is_internet_online():
        return lambda func: func
    return skip('Skipping because offline')


def is_internet_online(url='http://www.google.com/', timeout=3):
    try:
        requests.get(url, timeout=timeout)
        return True
    except requests.ConnectionError:
        pass
    return False
