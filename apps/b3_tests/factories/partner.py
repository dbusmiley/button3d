from decimal import Decimal
import factory

from apps.b3_organization.utils import get_current_org
from apps.b3_address.factories import AddressFactory
from apps.b3_tests.factories.catalogue import ProductFactory
from apps.b3_voucher.models import Voucher
from apps.partner.models import StockRecord, StockRecordRule, PostProcessing, \
    ExtraFeePerOrder


class PartnerFactory(factory.django.DjangoModelFactory):
    code = factory.Sequence(lambda n: 'test-partner-{}'.format(n))
    name = 'Test Partner'
    description = 'Test Partner Description'
    payment_method = factory.RelatedFactory(
        'apps.b3_checkout.factories.PartnerPaymentMethodFactory',
        'partner'
    )

    price_currency = 'EUR'
    vat_rate = Decimal('19.0')
    minimum_order_price_enabled = False
    minimum_order_price_value = Decimal('0.00')
    rating_quality = 5.0
    rating_reliability = 2.0
    rating_service = 3.0
    logo_temp = 'partners/temp/logo_sy_2018-01-19_13-35-22.jpg'

    class Meta:
        model = 'partner.Partner'
        django_get_or_create = ('code',)

    @factory.post_generation
    def add_org_and_address(self, created, extracted, **kwargs):
        self.save()
        if extracted and 'country' in extracted:
            self.address = AddressFactory(
                partner=self,
                user=None,
                country=extracted['country']
            )
        else:
            self.address = AddressFactory(
                partner=self,
                user=None,
            )
        if created and get_current_org():
            get_current_org().partners_enabled.add(self)


class StockRecordFactory(factory.DjangoModelFactory):
    partner = factory.SubFactory(PartnerFactory)
    product = factory.SubFactory(ProductFactory)

    price_custom = ("max(10.5, 1.25+root(volume)+"
                    "pow(box, 0.33)+min(pow(box, 0.2), area)*shells)")

    max_x_mm = 300
    max_y_mm = 300
    max_z_mm = 300
    min_production_days = 4
    max_production_days = 8
    is_default = True
    edit_status = StockRecord.PUBLISHED

    @factory.post_generation
    def colors(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for color in extracted:
                self.colors.add(color)

    class Meta:
        model = StockRecord


class PostProcessingFactory(factory.DjangoModelFactory):
    class Meta:
        model = PostProcessing

    stock_record = factory.SubFactory(StockRecordFactory)

    title = factory.Sequence(lambda n: 'PostProcessing-{}'.format(n))
    slug = factory.Sequence(lambda n: 'post-processing-{}'.format(n))

    description = 'Test PostProcessing'
    production_days_min = 1
    production_days_max = 3

    bounds_x_max = 200
    bounds_y_max = 200
    bounds_z_max = 200

    price_formula = 'root(volume)+pow(box, 0.33)'
    always_priced_manually = False
    published = True

    @factory.post_generation
    def colors(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for color in extracted:
                self.colors.add(color)


class StockRecordRuleFactory(factory.DjangoModelFactory):
    class Meta:
        model = StockRecordRule

    stockrecord = factory.SubFactory(StockRecordFactory)
    conclusion = 'manual_pricing'
    conditions = '[]'


class PartnerExtraFeeFactory(factory.DjangoModelFactory):
    partner = factory.SubFactory(PartnerFactory)

    type = 'Setup'
    value = Decimal('10.00')

    class Meta:
        model = ExtraFeePerOrder


class VoucherFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Voucher

    partner = factory.SubFactory(PartnerFactory)
    code = 'VOUCHER'
    action_type = Voucher.PERCENTAGE
    action_value = Decimal('10.00')
