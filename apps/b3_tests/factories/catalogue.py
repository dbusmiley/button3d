import factory

from apps.catalogue.models.product import Product
from apps.catalogue.models.category import Category


class ColorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'catalogue.Color'

    rgb = '#FFFFFF'
    title = factory.Sequence(lambda n: 'Test Color {0}'.format(n))


class TechnologyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'catalogue.Technology'
    title = 'Test product technology'


class CategoryFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'Category %d' % n)

    depth = 2
    path = factory.Sequence(lambda n: '0001%04d' % n)

    class Meta:
        model = Category

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Create a root category (one time)
        if not Category.objects.exists() and kwargs['depth'] != 1:
            CategoryFactory(name='Root Category', depth=1, path='0001')
        return super(CategoryFactory, cls)._create(
            model_class, *args, **kwargs
        )


class ProductFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: f'Test product {n}')
    description = 'Test product description'
    slug = factory.Sequence(lambda n: f'test-product-{n}')
    technology = factory.SubFactory(TechnologyFactory)
    category = factory.SubFactory(CategoryFactory)
    attr_wall_min = 1
    attr_wall_opt = 1.2

    class Meta:
        model = Product
