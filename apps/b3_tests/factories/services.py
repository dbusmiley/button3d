import factory
import datetime
import pytz

from apps.b3_mes.models import PartAttachment, Job
from apps.b3_mes.models.job_attachment import JobAttachment
from apps.b3_mes.models.workstation import Workstation
from apps.b3_mes.models.workflow_status import WorkflowStatus
from apps.b3_mes.models.workflow_template import WorkflowTemplate
from apps.b3_mes.models.workflow import Workflow
from apps.b3_mes.models.workflow_template_status import \
    WorkflowTemplateStatus
from apps.b3_mes.models.sequence import Sequence
from apps.b3_order.factories import OrderLineFactory, OrderFactory
from apps.b3_tests.factories import UserFactory
from apps.b3_tests.factories.partner import \
    PartnerFactory, PostProcessingFactory
from apps.b3_tests.factories.catalogue import (
    ProductFactory,
)
from apps.b3_tests.factories.checkout import (
    BasketLineFactory, AbstractAttachmentFactory)


class WorkstationFactory(factory.django.DjangoModelFactory):
    name = factory.Iterator([
        'EOS 201',
        'EOS 401',
        'EOS 701',
    ])
    partner = factory.SubFactory(PartnerFactory)
    description = 'lorem impsum'
    dimension_x = 20
    dimension_y = 20
    dimension_z = 20
    load_factor = 100
    bounding_box3d = factory.Iterator([True, False])

    @factory.post_generation
    def generate_many_to_many_fields(self, create, extracted, **kwargs):
        if not create:
            return
        material = ProductFactory()
        self.materials.add(material)

        post_processing = PostProcessingFactory()
        self.post_processings.add(post_processing)

    class Meta:
        model = Workstation


class BaseWorkflowFactory(factory.django.DjangoModelFactory):
    partner = factory.SubFactory(PartnerFactory)
    name = factory.Iterator([
        'General Workflow',
        'Metal Workflow',
        'Plastic Workflow',
        'Paint Job Workflow'
    ])
    description = 'Best workflow'


class WorkflowTemplateFactory(BaseWorkflowFactory):
    class Meta:
        model = WorkflowTemplate


class WorkflowTemplateWithStatusesFactory(BaseWorkflowFactory):

    @factory.post_generation
    def generate_statuses(self, create, extracted, **kwargs):
        if not create:
            return
        status = WorkflowTemplateStatusFactory(workflow=self, next=None)
        status = WorkflowTemplateStatusFactory(workflow=self, next=status)
        WorkflowTemplateStatusFactory(workflow=self, next=status)

    class Meta:
        model = WorkflowTemplate


class WorkflowFactory(BaseWorkflowFactory):
    part = factory.SubFactory(OrderLineFactory)

    class Meta:
        model = Workflow


class AbstractWorkflowStatusFactory(factory.django.DjangoModelFactory):
    workstation = factory.SubFactory(WorkstationFactory)

    name = factory.Iterator([
        'Finished',
        'Post processing',
        'Print',
        'Data prep',
    ])


class WorkflowTemplateStatusFactory(AbstractWorkflowStatusFactory):
    workflow = factory.SubFactory(WorkflowTemplateFactory)

    class Meta:
        model = WorkflowTemplateStatus


class WorkflowStatusFactory(AbstractWorkflowStatusFactory):
    workflow = factory.SubFactory(WorkflowFactory)

    class Meta:
        model = WorkflowStatus


class SequenceFactory(factory.django.DjangoModelFactory):
    order = factory.SubFactory(OrderFactory)
    part = factory.SubFactory(BasketLineFactory)
    status = factory.SubFactory(WorkflowStatusFactory)

    class Meta:
        model = Sequence


class PartAttachmentFactory(AbstractAttachmentFactory):
    part = factory.SubFactory(OrderLineFactory)

    class Meta:
        model = PartAttachment


class JobFactory(factory.django.DjangoModelFactory):
    """
    :field: `Job.workstation` and :field: `Job.material` must be supplied
    as a params to assure that the :model: `Workstation` can print that
    material -> :model: `Product`
    """
    created_by = factory.SubFactory(UserFactory)
    start_time = pytz.utc.localize(datetime.datetime(2018, 12, 20, 9, 30))
    finish_time = pytz.utc.localize(datetime.datetime(2018, 12, 20, 11, 30))
    process_parameters = 'Foo bar'
    material_batch_number = 'B-12-10'
    name = 'job name'
    workstation = factory.SubFactory(WorkstationFactory)
    material = factory.LazyAttribute(
        lambda self: self.workstation.materials.first()
    )

    class Meta:
        model = Job


class JobAttachmentFactory(AbstractAttachmentFactory):
    job = factory.SubFactory(JobFactory)

    class Meta:
        model = JobAttachment
