import sys
from io import BytesIO

import factory

from apps.b3_core.models import StlFile, Parameter, Configuration
from apps.b3_tests.factories.b3_signup import UserFactory


class ParameterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Parameter
    # Repaired parameters of LittleShip
    max_scale = 3.907
    area = 17672.93
    volume = 23286.07
    h = 97.856
    w = 29.878
    d = 144.6
    faces = 2308
    shells = 1
    holes = 0


class ConfigurationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Configuration


class StlFileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = StlFile

    # Last twelve digits are for different testing instances
    uuid = factory.Sequence(lambda n: '13a25b02-794b-4ce6-bc27-%012d' % n)
    showname = factory.Sequence(lambda n: 'StlFile%d' % n)

    o_parameter = factory.SubFactory(ParameterFactory)
    r_parameter = factory.SubFactory(ParameterFactory)
    parameter = factory.SubFactory(ParameterFactory)


class AbstractAttachmentFactory(factory.DjangoModelFactory):
    filename = 'test.file'
    content = b'test_data'
    uploader = factory.SubFactory(UserFactory)

    @classmethod
    def _create(cls, model_class, *args,  **kwargs):
        content = BytesIO(kwargs.pop('content'))
        if 'filesize' not in kwargs:
            kwargs['filesize'] = sys.getsizeof(content)

        attachment = super()._create(model_class, *args, **kwargs)
        attachment.file.save(
            name=kwargs['filename'],
            content=content,
            save=True
        )

        return attachment
