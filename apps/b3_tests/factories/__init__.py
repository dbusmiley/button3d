# flake8: noqa
from apps.b3_tests.factories.b3_core import *
from apps.b3_tests.factories.b3_manual_request import *
from apps.b3_tests.factories.b3_organization import *
from apps.b3_tests.factories.b3_signup import *
from apps.b3_tests.factories.catalogue import *
from apps.b3_tests.factories.checkout import *
from apps.b3_tests.factories.partner import *
from apps.b3_tests.factories.helpers import *

from sys import modules
from inspect import isclass, getmembers
import factory


def iterate_factories():
    for name, obj in getmembers(modules[__name__]):
        if isclass(obj)\
            and isinstance(obj, factory.base.FactoryMetaClass) \
            and obj.__name__ not in ["BasketLineFactory",
                                     "OrderFactory",
                                     "UserFactory"]:
            yield obj
