import factory

from apps.b3_address.factories import CountryFactory
from apps.b3_tests.factories.partner import PostProcessingFactory


class ManualRequestFactory(factory.django.DjangoModelFactory):
    ip_address = factory.Sequence(lambda n: '192.168.1.{0}'.format(n))
    country = factory.SubFactory(CountryFactory)

    class Meta:
        model = 'b3_manual_request.ManualRequest'


class ManualRequestPostProcessingOptionFactory(
    factory.django.DjangoModelFactory
):
    post_processing = factory.SubFactory(PostProcessingFactory)
    manual_request = factory.SubFactory(ManualRequestFactory)

    class Meta:
        model = 'partner.ManualRequestPostProcessingOption'


class ManualRequestAttachmentFactory(factory.django.DjangoModelFactory):
    manual_request = factory.SubFactory(ManualRequestFactory)
    filesize = 10
    file = factory.django.FileField(filename='Test file')

    class Meta:
        model = 'b3_manual_request.Attachment'
