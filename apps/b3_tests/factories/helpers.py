from apps.b3_tests.factories import ProductFactory, ColorFactory, \
    StockRecordFactory, PostProcessingFactory, CategoryFactory


def create_parent_product(technology=None):
    parent_product = ProductFactory(
        attr_detail=5,
        attr_strength=4,
        is_multicolor=False,
        attr_wall_min=0.7,
        attr_wall_opt=1.2,
        popularity=500,
        attr_density_min=1.3,
        attr_density_max=1.3,
        score_detail=30,
    )
    if technology:
        parent_product.technology = technology
        parent_product.save()
    return parent_product


def create_product(technology=None):
    product = ProductFactory(
        attr_detail=5,
        attr_strength=4,
        is_multicolor=False,
        attr_wall_min=0.7,
        attr_wall_opt=1.2,
        popularity=500,
        attr_density_min=1.3,
        attr_density_max=1.3,
        score_detail=30,
    )
    if technology:
        product.technology = technology
        product.save()
    return product


def create_child_product(
    parent_product=None, is_colorable=False, technology=None
):
    child_product = ProductFactory(
        title=parent_product.title + ' Finishing',
        is_colorable=is_colorable,
        attr_elongation_min=15.0,
        attr_elongation_max=25.0,
        attr_modulus_min=1200.0,
        attr_modulus_max=1500.0,
    )
    if technology:
        child_product.technology = technology
        child_product.save()
    return child_product


def create_stockrecord(child_product, partner, is_default):
    return StockRecordFactory(product=child_product, partner=partner)


def create_post_processing(stock_record, has_colors=False, **kwargs):
    if has_colors:
        color_1 = ColorFactory(
            id=1,
            title="Color 1",
            rgb="FFFFFF"
        )
        color_2 = ColorFactory(
            id=2,
            title="Color 2",
            rgb="0000FF"
        )
        colors = [color_1, color_2]
    else:
        colors = []

    return PostProcessingFactory(
        stock_record=stock_record, colors=colors, **kwargs
    )


def create_category():
    return CategoryFactory()
