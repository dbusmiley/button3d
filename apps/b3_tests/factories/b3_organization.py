import factory

from django.conf import settings
from django.core.handlers.base import BaseHandler
from django.test.client import RequestFactory


class RequestMock(RequestFactory):
    def request(self, **request):
        # Construct a generic request object.
        request['SERVER_NAME'] = settings.TEST_BASE_DOMAIN
        request = RequestFactory.request(self, **request)
        handler = BaseHandler()
        handler.load_middleware()
        for middleware_method in handler._request_middleware:
            if middleware_method(request):
                raise Exception("Couldn't create request mock object - "
                                "request middleware returned a response")
        return request


class OrganizationProductOptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'b3_organization.OrganizationProductOption'


class OrganizationPartnerOptionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'b3_organization.OrganizationPartnerOption'

    @factory.post_generation
    def partners(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return
        if extracted:
            for partner in extracted:
                self.partners.add(partner)


class SiteFactory(factory.django.DjangoModelFactory):
    name = factory.Sequence(
        lambda n: f'{n}org.my.3yd')
    domain = 'org.my.3yd'

    class Meta:
        model = 'sites.Site'
        django_get_or_create = ('domain',)


class OrganizationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'b3_organization.Organization'

    showname = '3YOURMIND'
    site = factory.SubFactory(SiteFactory)

    @factory.post_generation
    def partners_enabled(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for partner in extracted:
                self.partners_enabled.add(partner)


class KeyManagerFactory(factory.django.DjangoModelFactory):
    title = 'Test Key Account Manager'
    name = 'Stephan Testing'
    role = 'CEO'
    email = 'test@3yourmind.com'
    telephone = '0123456789'

    class Meta:
        model = 'b3_organization.KeyManager'


class OrganizationCustomTemplateFactory(factory.django.DjangoModelFactory):
    site = factory.SubFactory(SiteFactory)
    template_name = 'b3_core/nav.html'

    class Meta:
        model = 'b3_organization.OrganizationCustomTemplate'
