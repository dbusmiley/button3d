import factory
from actstream.models import Action
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.crypto import get_random_string

from apps.b3_organization.tracking.utils import VERBS, REGISTRATION
from apps.b3_organization.utils import get_current_site
from apps.b3_signup.models import Signup


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    is_staff = False
    is_active = True
    username = factory.Sequence(lambda n: 'tester{0}'.format(n))
    email = factory.LazyAttribute(lambda o: '%s@3yourmind.com' % o.username)
    password = factory.PostGenerationMethodCall('set_password', 'testpassword')


class UserAnyEmailFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    is_staff = True
    is_active = True
    username = 'tester'
    email = 'tester@3yourmind.com'
    password = factory.PostGenerationMethodCall('set_password', 'testpassword')


class SignupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Signup

    email = factory.Sequence(
        lambda n: 'tester{}@3yourmind.com'.format(
            '-{}'.format(n) if n else ''
        )
    )
    confirmed = True
    key = factory.LazyAttribute(lambda o: get_random_string(64).lower())
    site = factory.LazyAttribute(lambda o: get_current_site())


class UserInteractionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Action

    verb = VERBS.INTERACTS
    timestamp = factory.LazyAttribute(lambda o: timezone.now())

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        kwargs['actor'] = kwargs.pop('user')
        return super(UserInteractionFactory, cls)._create(
            model_class, *args, **kwargs
        )


class SignupFirstStepStateChangeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Action

    verb = VERBS.STATE_CHANGE
    timestamp = factory.LazyAttribute(lambda o: timezone.now())

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        kwargs['actor'] = kwargs.pop('signup')
        action = super(SignupFirstStepStateChangeFactory, cls)._create(
            model_class, *args, **kwargs
        )
        action.data = {'new_state': REGISTRATION.FIRST_STEP}
        action.save()
        return action


class SignupRegisteredStateChangeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Action

    verb = VERBS.STATE_CHANGE
    timestamp = factory.LazyAttribute(lambda o: timezone.now())

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        kwargs['actor'] = kwargs.pop('signup')
        action = super(SignupRegisteredStateChangeFactory, cls)._create(
            model_class, *args, **kwargs
        )
        action.data = {
            'old_state': REGISTRATION.FIRST_STEP,
            'new_state': REGISTRATION.REGISTERED
        }
        action.save()
        return action
