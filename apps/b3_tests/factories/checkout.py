import factory

from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import StlFileFactory, UserFactory, \
    AbstractAttachmentFactory
from apps.b3_tests.factories.partner import StockRecordFactory
from apps.basket.models import Line, BasketAttachment, Basket


class BasketFactory(factory.DjangoModelFactory):
    site_id = get_current_site().id if get_current_site() else None

    @staticmethod
    def save_basket(basket):
        basket.pricing_status = basket.NO_REQUEST
        basket.status = basket.SUBMITTED
        basket.save()
        return basket

    class Meta:
        model = Basket


class BasketLineFactory(factory.DjangoModelFactory):
    basket = factory.SubFactory(BasketFactory)
    configuration = factory.SubFactory(
        'apps.b3_tests.factories.b3_core.ConfigurationFactory'
    )
    stl_file = factory.SubFactory(StlFileFactory)
    stockrecord = factory.SubFactory(StockRecordFactory)
    quantity = 1

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        line = super(BasketLineFactory, cls) \
            ._create(model_class, *args, **kwargs)

        line.product = line.stockrecord.product if line.stockrecord else None
        line.save()
        line.basket.price_line(line, created=False)
        if line.basket.owner is None:
            line.basket.owner = UserFactory()
        return line

    class Meta:
        model = Line


class BasketAttachmentFactory(AbstractAttachmentFactory):
    class Meta:
        model = BasketAttachment

    basket = factory.LazyFunction(lambda: BasketLineFactory().basket)
