import os
from contextlib import contextmanager

from django.conf import settings
from django.test import TestCase as DjangoTestCase

from apps.b3_organization import utils
from apps.b3_organization.utils import CurrentRequestFactory, \
    set_current_request
from apps.b3_tests.factories import SiteFactory, \
    OrganizationFactory, UserFactory
from apps.b3_tests.clients import ThreeydAjaxClient, ThreeydClient


class TestCase(DjangoTestCase):
    client_class = ThreeydClient
    maxDiff = None

    def setUp(self):
        self.site = SiteFactory(
            domain=settings.TEST_BASE_DOMAIN,
            name=settings.TEST_BASE_DOMAIN
        )
        OrganizationFactory(site=self.site)
        utils.set_current_site(self.site)
        self.organization = self.site.organization
        # Set up anonymous user
        self.site.refresh_from_db()
        super(TestCase, self).setUp()

    def populate_current_request(self):
        set_current_request(CurrentRequestFactory(self.site).get('/'))

    def tearDown(self):
        utils.set_current_site(None)
        utils.SITE_CACHE = {}
        super(TestCase, self).tearDown()

    def assertMembersIn(self, container, *members):
        """Assert that container contains given members"""
        for member in members:
            self.assertIn(member, container)

    @contextmanager
    def override_system_environment(self, name, value):
        """
        Allows to run code with temporarily overridden system environment
        variable value.

        Cleans after itself. If `name` previously had value, it is restored.
        Otherwise variable name is deleted.

        Usage:
        with self.override_system_environment('SEAT_BELT', 'OFF'):
            do('crazy', 'things')
        """
        original_value_exists = name in os.environ
        if original_value_exists:
            original_value = os.environ[name]
        else:
            original_value = None

        os.environ[name] = value

        yield

        if original_value_exists:
            os.environ[name] = original_value
        else:
            del os.environ[name]

    def preview_content(self, content):
        """
        Saves content into temp file and opens with the default application
        """
        from tempfile import NamedTemporaryFile
        import subprocess  # nosec

        with NamedTemporaryFile(delete=False) as f:  # nosec
            f.write(content.encode())
            subprocess.Popen(['xdg-open', f.name])  # nosec


class AjaxTestCase(TestCase):
    client_class = ThreeydAjaxClient


class AuthenticatedTestCase(TestCase):
    test_password = 'TestPass'

    def setUp(self):
        super(AuthenticatedTestCase, self).setUp()
        self.user = UserFactory(password=self.test_password)
        logged_in = self.client.login(
            email=self.user.email, password=self.test_password)
        self.assertTrue(logged_in, 'should be logged in')


class AuthenticatedAjaxTestCase(AjaxTestCase, AuthenticatedTestCase):
    pass
