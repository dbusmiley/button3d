from django.test import TestCase as DjangoTestCase

from apps.b3_organization import utils
from apps.b3_organization.utils import set_current_request, \
    CurrentRequestFactory
from apps.b3_tests.factories import SiteFactory, \
    OrganizationFactory, UserFactory
from apps.b3_tests.clients import ProAccountClient, ProAccountAjaxClient


class OrganizationTestCase(DjangoTestCase):
    client_class = ProAccountClient

    def setUp(self):
        self.site = SiteFactory()
        utils.set_current_site(self.site)
        self.organization = OrganizationFactory(site=self.site)
        super(OrganizationTestCase, self).setUp()

    def tearDown(self):
        super(OrganizationTestCase, self).tearDown()
        utils.set_current_site(None)
        utils.SITE_CACHE = {}

    def populate_current_request(self):
        set_current_request(CurrentRequestFactory(self.site).get('/'))


class AuthenticatedOrganizationTestCase(OrganizationTestCase):
    test_password = 'TestPass'

    def setUp(self):
        super(AuthenticatedOrganizationTestCase, self).setUp()
        self.user = UserFactory(password=self.test_password)
        self.user.userprofile.site = self.site
        self.user.userprofile.save()

        logged_in = self.client.login(
            email=self.user.email, password=self.test_password)
        self.assertTrue(logged_in, 'should be logged in')


class AuthenticatedAjaxOrganizationTestCase(AuthenticatedOrganizationTestCase):
    client_class = ProAccountAjaxClient
