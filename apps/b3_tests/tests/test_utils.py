import copy

from django.test import TestCase as DjangoTestCase

from apps.b3_tests.utils import compare_dict_keys


class UtilsTest(DjangoTestCase):
    def setUp(self):
        self.dict_1 = {
            "name": "string",
            "file": {
                "name": "string",
                "created": "string",
            },
            "postProcessings": [
                0
            ],
            "attachments": [
                {
                    "uploader": {
                        "company": "string"
                    }
                }
            ],

        }

        self.dict_2 = {
            "name": "Hello world",
            "file": {
                "name": "Turbine.stl",
                "created": "2018-09-12",
            },
            "postProcessings": [
                12, 14, 15
            ],
            "attachments": [
                {
                    "uploader": {
                        "company": "3YD"
                    }
                }
            ],

        }

    def test_compare_dict_keys_equal(self):
        """
        Asserts that :func: `compare_dict_keys`
        works as intended when param keys are equal
        """
        self.assertTrue(compare_dict_keys(self.dict_1, self.dict_2))

    def test_compare_dict_keys_not_equal(self):
        """
        Asserts that :func: `compare_dict_keys`
        works as intended when param keys are not equal
        """
        dict_2_copy = copy.deepcopy(self.dict_2)
        dict_2_copy['file']['NOT_name'] = dict_2_copy['file']['name']
        del dict_2_copy['file']['name']

        self.assertFalse(compare_dict_keys(self.dict_1, dict_2_copy))

    def test_compare_dict_keys_not_same_structure(self):
        """
        Asserts that :func: `compare_dict_keys`
        works as intended when param keys are not equal
        """
        dict_2_copy = copy.deepcopy(self.dict_2)
        dict_2_copy['NEW_KEY'] = 'hello world'

        self.assertFalse(compare_dict_keys(self.dict_1, dict_2_copy))
