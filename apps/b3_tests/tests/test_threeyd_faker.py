from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.management.threeyd_faker import get_threeyd_faker


class ThreeydCustomFakerTest(TestCase):
    def setUp(self):
        self.faker = get_threeyd_faker()

    def test_basket_status(self):
        status = self.faker.basket_status()
        possible_states = (
            "Open", "Merged", "Saved", "Submitted", "Editable")
        self.assertIn(status, possible_states)

    def test_order_status(self):
        status = self.faker.order_status()
        possible_states = (
            "pending", "printing", "shipped", "cancelled")
        self.assertIn(status, possible_states)

    def test_pricing_status(self):
        status = self.faker.pricing_status()
        possible_states = (
            "No Request", "Waiting For Pricing", "Manually Priced")
        self.assertIn(status, possible_states)

    def test_category_type(self):
        type = self.faker.category_type()
        possible_types = ("Basket", "Shipping", "Deferred")
        self.assertIn(type, possible_types)

    def test_unit(self):
        unit = self.faker.unit()
        possible_units = ("mm", "in")
        self.assertIn(unit, possible_units)

    def pdf_file(self):
        pdf = self.faker.pdf_file()
        expected_content_type = "application/pdf"
        self.assertTrue(pdf.content_type, expected_content_type)
        self.assertEqual(pdf.name[-4:], ".pdf")

    def image_file(self):
        jpg = self.faker.image_file()
        expected_content_type = "image/jpeg"
        self.assertTrue(jpg.content_type, expected_content_type)
        self.assertEqual(jpg.name[-4:], ".pdf")
