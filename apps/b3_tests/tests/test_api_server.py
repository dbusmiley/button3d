from django.test import RequestFactory
from apps.b3_core.exceptions import JSONBadRequest
from apps.b3_tests.testcases.common_testcases import TestCase


class APIBasicServerTest(TestCase):
    def test_django_local_api(self):
        from apps.b3_compare.api_v1_views import UploadView
        from apps.b3_tests.factories import OrganizationFactory, SiteFactory
        view = UploadView.as_view()

        request = RequestFactory().get('/v1/colors')
        with self.assertRaisesRegex(JSONBadRequest, "Unable to authorize."):
            view(request)

        site = SiteFactory()
        organization = OrganizationFactory(site=site)
        request = RequestFactory().get(
            '/v1/colors?organization={0}'.format(organization.slug))
        with self.assertRaisesRegex(JSONBadRequest, "Unable to authorize."):
            view(request)

        request = RequestFactory().get(
            '/v1/uploads/b5fd966a-349e-4538-9514-a4183976b793/')
        with self.assertRaisesRegex(JSONBadRequest, "Unable to authorize."):
            view(request)
