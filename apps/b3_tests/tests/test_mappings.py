from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.management.mappings import FieldFakerMapping, \
     FieldValidationMapping
from apps.b3_tests.management.settings import FIELD_MAP, MODEL_FAKER_MAP
from django.contrib.auth.models import User


class FakerMappingTest(TestCase):
    def test_get_default_field(self):
        user_faker = FieldFakerMapping(User)
        expected = FIELD_MAP["BooleanField"]
        self.assertEqual(user_faker["is_staff"], expected)

    def test_fields_are_correctly_initialized(self):
        user_faker = FieldFakerMapping(User)
        expected_fields = frozenset(
            [field.name for field in User._meta.get_fields()
             if FieldFakerMapping._is_field_to_be_mapped(field)]
        )
        self.assertTrue(user_faker.get_keys() == expected_fields)

    def test_get_custom_field(self):
        user_faker = FieldFakerMapping(User)
        expected = MODEL_FAKER_MAP["User"]["username"]
        self.assertEqual(user_faker["username"], expected)

    def test_is_field_to_be_mapped(self):
        username = User._meta.get_field("username")
        is_true = FieldFakerMapping._is_field_to_be_mapped(username)
        self.assertTrue(is_true)


class ValidationMappingTest(TestCase):
    def test_fields_are_correctly_initialized(self):
        user_faker = FieldValidationMapping(User)
        expected_fields = frozenset(
            [field.name for field in User._meta.get_fields()
             if FieldValidationMapping._is_field_to_be_mapped(field)]
        )
        self.assertTrue(user_faker.get_keys() == expected_fields)

    def test_get_correct_validation_method(self):
        user_validator_map = FieldValidationMapping(User)
        expected = User._meta.get_field("email").clean
        self.assertEqual(user_validator_map["email"], expected)

    def test_is_field_to_be_mapped(self):
        username = User._meta.get_field("username")
        is_true = FieldValidationMapping._is_field_to_be_mapped(username)
        self.assertTrue(is_true)
