import unittest
import json

from urllib.parse import urlsplit, parse_qsl


def decode_url(url):
    split_result = urlsplit(url)
    query_dict = dict(parse_qsl(split_result.query))
    return (
        split_result.scheme,
        split_result.netloc,
        split_result.path,
        query_dict
    )


def assert_serializer_structure(test_client, serializer, structure_expected):
    """
    Asserts that :serializer: `serializer` 's structure
    matches :dict: `structure_expected` 's structure
    :param test_client: TestCase instance
    :param serializer: Serializer instance
    :param structure_expected: dictionary
    """
    indent = 2

    # handle the case of bytes in `serializer.data`
    try:
        structure_actual_str = json.dumps(serializer.data, indent=indent)
    except TypeError:
        structure_actual_str = str(serializer.data)
    structure_expected_str = json.dumps(structure_expected, indent=indent)

    test_client.assertTrue(
        compare_dict_keys(structure_expected, serializer.data),
        f'Serializer data structure is incorrect.'
        f'\n\nExpected:\n{structure_expected_str}\n\n'
        f'Actual:\n{structure_actual_str}'
    )


def compare_dict_keys(dict_1, dict_2):
    """
    Checks if the keys in :dict: `dict_1` match the keys in :dict: `dict_2`

    :param dict_1: dictionary
    :param dict_2: dictionary
    :return: boolean indicating whether or not the params have the same keys
    """
    keys_1 = dict_1.keys()
    keys_2 = dict_2.keys()
    equal = True
    test_case = unittest.TestCase()
    try:
        test_case.assertListEqual(list(keys_1), list(keys_2))
    except AssertionError:
        return False
    else:
        for key_1 in keys_1:
            if type(dict_1[key_1]) == dict:
                equal = equal and compare_dict_keys(
                    dict_1[key_1], dict_2[key_1])
            elif isinstance(dict_1[key_1], list):
                if dict_1[key_1] and dict_2[key_1]:
                    if isinstance(dict_1[key_1][0], dict):
                        equal = equal and compare_dict_keys(
                            dict_1[key_1][0], dict_2[key_1][0])
        return equal
