import json

from django.conf import settings
from django.test.client import Client


class JsonResponseClient(Client):
    def get(self, url, data=None,
            return_json=True, follow=False, secure=False, **extra):
        response = super(JsonResponseClient, self).get(
            url, data, follow, secure, **extra)
        if return_json:
            return self._parse_json_response(response)
        else:
            return response

    def post(self, url, data=None,
             return_json=True, content_type='application/json',
             follow=False, secure=False, **extra):
        response = super(JsonResponseClient, self).post(
            url, data, content_type, follow, secure, **extra)
        if return_json:
            return self._parse_json_response(response)
        else:
            return response

    def put(self, url, data='',
            return_json=True, content_type='application/json',
            follow=False, secure=False, **extra):
        response = super(JsonResponseClient, self).put(
            url, data, content_type, follow, secure, **extra)
        if return_json:
            return self._parse_json_response(response)
        else:
            return response

    def delete(self, url, data='',
               return_json=True, content_type='application/octet-stream',
               follow=False, secure=False, **extra):
        response = super(JsonResponseClient, self).delete(
            url, data, content_type, follow, secure, **extra)
        if return_json:
            return self._parse_json_response(response)
        else:
            return response

    @staticmethod
    def _parse_json_response(response):
        json_resp = response.json()
        if isinstance(json_resp, dict):
            return json_resp

        try:
            return json.loads(
                response.content.decode('string-escape').strip('"'))
        except BaseException:
            return json.loads(response.content)


class ProAccountClient(Client):
    def request(self, **request):
        kwargs = {
            'HTTP_HOST': settings.TEST_SITE_DOMAIN,
        }
        kwargs.update(request)
        return super(ProAccountClient, self).request(**kwargs)


class ThreeydClient(Client):
    def request(self, **request):
        kwargs = {
            'HTTP_HOST': settings.TEST_BASE_DOMAIN,
            'SERVER_NAME': settings.TEST_BASE_DOMAIN
        }
        kwargs.update(request)
        return super(ThreeydClient, self).request(**kwargs)


class AjaxClient(JsonResponseClient):
    def request(self, **request):
        request['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        return super(AjaxClient, self).request(**request)


class ProAccountAjaxClient(ProAccountClient, AjaxClient):
    pass


class ThreeydAjaxClient(ThreeydClient, AjaxClient):
    pass


class ApiKeyAuthorizedClient(ThreeydClient):
    def __init__(self, api_key, *args, **kwargs):
        super(ApiKeyAuthorizedClient, self).__init__(*args, **kwargs)
        self.api_key = str(api_key)

    def request(self, **request):
        if self.api_key:
            request['HTTP_AUTHORIZATION'] = 'ApiKey {0}'.format(self.api_key)
        return super(ApiKeyAuthorizedClient, self).request(**request)
