from string import ascii_uppercase
from apps.b3_tests.factories import iterate_factories
from pytz import UTC


LANGUAGES = {
    "ger": "de_DE",
    "german": "de_DE",
    "pol": "pl_PL",
    "polish": "pl_PL",
    "eng": "en_US",
    "english": "en_US",
    "arb": "ar_EG",
    "arabic": "ar_EG",
    "jap": "ja_JP",
    "japanese": "ja_JP",
    "chn": "zh_CN",
    "chinese": "zh_CN",
    "ind": "hi_IN",
    "hindi": "hi_IN",
    "kor": "ko_KR",
    "korean": "ko_KR",
    "rus": "ru_RU",
    "russian": "ru_RU",
    "grk": "el_GR",
    "greek": "el_GR",
}


"""
Maps a model Name to the Name of its Factory
Examples:
    "Action" ---> "UserInteractionFactory"
    "Basket" ---> "BasketFactory"
    etc.
"""
FACTORY_MAP = {
    factory._meta.model.__name__: factory.__name__
    for factory in iterate_factories()
    if not factory._meta.abstract
}


FIELD_MAP = {
    "AutoSlugField": "slug",
    "UniqueAutoSlugField": "slug",
    "ColorField": "hex_color",
    "BigIntegerField": "pyint",
    "BooleanField": "pybool",
    "CharField": "word",
    "DateField": (
        "past_date",
        {"tzinfo": UTC, "start_date": "-30d"}
    ),
    "DateTimeField": (
        "past_datetime",
        {"tzinfo": UTC, "start_date": "-30d"}
    ),
    "DecimalField": (
        "pydecimal",
        {"left_digits": 2, "right_digits": 2, "positive": True}
    ),
    "EmailField": "email",
    "ExtendedURLField": "uri",
    "FloatField": (
        "pydecimal",
        {"left_digits": 2, "right_digits": 2, "positive": True}
    ),
    "GenericIPAddressField": "ipv4",
    "IntegerField": "pyint",
    "JSONField": "json",
    "NullBooleanField": "pybool",
    "NullCharField": "first_name",
    "PositiveDecimalField": (
        "pydecimal",
        {"left_digits": 2, "right_digits": 2, "positive": True}
    ),
    "PositiveIntegerField": "random_int",
    "PositiveSmallIntegerField": "randomize_nb_elements",
    "SlugField": "slug",
    "SmallIntegerField": "randomize_nb_elements",
    "TextField": "paragraph",
    "UppercaseCharField": "cryptocurrency_code",
    "URLField": "uri",
    "UUIDField": "uuid4",
    "FileField": "pdf_file",
    "ImageField": "image_file",
    "TranslationCharField": "word",
    "TranslationFileField": "pdf_file",
    "TranslationTextField": "paragraphs",
    "TranslationURLField": "uri",
    "PhoneNumberField": "phone_number",
    "StdImageField": "image_file"
}

MODEL_FAKER_MAP = {
    "Country": {
        "iso_3166_1_a2":
            ("lexify", {"text": "??", "letters": ascii_uppercase}),
        "iso_3166_1_a3":
            ("lexify", {"text": "???", "letters": ascii_uppercase}),
        "iso_3166_1_numeric":
            ("numerify", {"text": "###"}),
        "printable_name": "country",
        "name": "country"
    },
    "OrganizationCustomTemplate": {
        "site": "db_site"
    },
    "OrganizationBillingAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "vat_number": "ean8",
        "company": "company",
        "country": "db_country",
        "site": "db_site",
        "organization": "db_organization"
    },
    "OrganizationShippingAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "hash": "sha256",
        "country": "db_country",
        "site": "db_site"
    },
    "Line": {
        "price_incl_tax": (
            "pydecimal",
            {"left_digits": 3, "right_digits": 2, "positive": True}
        ),
        "price_excl_tax": (
            "pydecimal",
            {"left_digits": 3, "right_digits": 2, "positive": True}
        ),
        "stl_file": "stl_file",
        "basket": "db_basket",
        "configuration": "db_configuration"
    },
    "Action": {
        "user": "db_user",
        "actor_content_type": "none"
    },
    "UserAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "hash": "sha256",
        "country": "db_country"
    },
    "Configuration": {
        "unit": "unit",
    },
    "StlFile": {
        "uuid": "uuid4",
        "filetype": "file3D",
        "ip_address": "ipv4",
    },
    "ManualRequest": {
        "first_name": "first_name",
        "last_name": "last_name",
        "company": "company",
        "message": "sentences",
        "language": "language",
        "ip_address": "ipv4",
        "referer_url": "uri",
        "hubspot_data": "word",
        "type": "manual_request_type",
        "site": "db_site",
        "country": "db_country"
    },
    "StockRecord": {
        "product": "db_product",
        "partner": "db_partner"
    },
    "StockRecordRule": {
        "stockrecord": "db_stockrecord"
    },
    "KeyManager": {
        "title": "prefix",
        "title_en": "prefix",
        "title_de": "prefix",
        "name": "name",
        "role": "job",
        "role_en": "job",
        "role_de": "job",
        "email": "email",
        "telephone": "phone_number",
    },
    "OrganizationPartnerOption": {
        "site": "db_site",
        "partners": "db_partners"
    },
    "OrganizationProductOption": {
        "site": "db_site",
        "product": "db_product"
    },
    "Organization": {
        "showname": "company",
        "site": "db_site",
    },
    "ProductCategory": {
        "product": "db_product",
        "category": "db_category"
    },
    "Basket": {
        "status": "basket_status",
        "site": "db_site",
    },
    "Color": {
        "rgb": "hex_color",
    },
    "ConditionalOffer": {
        "slug": "slug",
    },
    "BillingAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "same_as_shipping": "pybool",
        "country": "db_country",
        "site": "db_site"
    },
    "ShippingMethod": {
        "partner": "db_partner"
    },
    "Order": {
        "currency": "currency_code",
        "status": "order_status",
        "vat_number": "ean8",
        "company": "company",
        "customer_ip": "ipv4",
        "language": "language",
        "site": "db_site"
    },
    "OrderDiscount": {
        "category": "category_type",
    },
    "ShippingAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "country": "db_country"
    },
    "Partner": {
        "code": "slug",
        "vat_id": "ean8",
        "website": "uri",
        "email": "email",
        "price_currency": "currency_code",
        "webhook": "uri",
        "stripe_enabled": "false",
        "invoice_enabled": "true"
    },
    "PartnerAddress": {
        "first_name": "first_name",
        "last_name": "last_name",
        "line1": "street_address",
        "line2": "zipcode",
        "line3": "company",
        "line4": "city",
        "state": "state",
        "postcode": "zipcode",
        "country": "db_country"
    },
    "User": {
        "username": "user_name",
        "first_name": "first_name",
        "last_name": "last_name",
        "email": "email",
    },
    "UserProfile": {
        "user": "db_user_without_profile"
    },
    "Site": {
        "domain": "domain_name",
        "name": "domain_word"
    },
    "Voucher": {
        "code": "slug"
    },
    'PartnerPaymentMethod': {
        'type_name': 'partner_payment_method_type_name',
        'public_config_arguments': 'get_empty_dict',
        'private_config_arguments': 'get_empty_dict',
        'is_enabled': 'true',
    },
}
