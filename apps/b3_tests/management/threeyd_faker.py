import faker
from django.core.files.uploadedfile import SimpleUploadedFile

from apps.b3_checkout.constants import PAYMENT_METHODS

from apps.b3_tests.factories import CountryFactory, UserFactory, \
    StockRecordFactory, SiteFactory, ProductFactory, OrganizationFactory, \
    CategoryFactory, BasketFactory, ConfigurationFactory, PartnerFactory, \
    StlFileFactory
from json import dumps


class ThreeydProvider(faker.providers.BaseProvider):
    """
    Extends faker with some methods customized for 3yd needs
    """

    def basket_status(self):
        basket_states = (
            "Open", "Merged", "Saved", "Submitted", "Editable")
        return self.random_element(elements=basket_states)

    def order_status(self):
        order_states = (
            "pending", "printing", "shipped", "cancelled")
        return self.random_element(elements=order_states)

    def pricing_status(self):
        pricing_states = (
            "No Request", "Waiting For Pricing", "Manually Priced")
        return self.random_element(elements=pricing_states)

    def category_type(self):
        categories = ("Basket", "Shipping", "Deferred")
        return self.random_element(elements=categories)

    def manual_request_type(self):
        types = ('question', 'offer', 'printable', 'other')
        return self.random_element(elements=types)

    @staticmethod
    def file3D():
        return "stl"

    def language(self):
        languages = ("EN", "DE")
        return self.random_element(elements=languages)

    def unit(self):
        units = ("mm", "in")
        return self.random_element(elements=units)

    def pdf_file(self):
        return SimpleUploadedFile(
            "3dFile.pdf",
            bytes([1, 2, 3, 4]),
            content_type="application/pdf"
        )

    def image_file(self):
        return SimpleUploadedFile(
            "image.png",
            bytes([1, 2, 3, 4]),
            content_type="image/jpeg"
        )

    @staticmethod
    def db_country():
        return CountryFactory()

    def db_site(self):
        return SiteFactory(domain=self.lexify("???????"))

    @staticmethod
    def db_product():
        return ProductFactory()

    @staticmethod
    def db_organization():
        return OrganizationFactory()

    @staticmethod
    def db_user():
        return UserFactory()

    @staticmethod
    def db_user_without_profile():
        return UserFactory(userprofile=None)

    @staticmethod
    def db_partner():
        return PartnerFactory()

    @staticmethod
    def db_partners():
        return [PartnerFactory(), PartnerFactory()]

    @staticmethod
    def db_stockrecord():
        return StockRecordFactory()

    @staticmethod
    def db_category():
        return CategoryFactory()

    @staticmethod
    def db_basket():
        return BasketFactory()

    @staticmethod
    def db_configuration():
        return ConfigurationFactory()

    @staticmethod
    def true():
        return True

    @staticmethod
    def false():
        return False

    @staticmethod
    def none():
        return None

    @staticmethod
    def stl_file():
        return StlFileFactory()

    def json(self):
        return dumps({"test": 1, "random": 2})

    def partner_payment_method_type_name(self):
        return PAYMENT_METHODS.INVOICE

    def get_empty_dict(self):
        return {}


def get_threeyd_faker(language="en_US"):
    threeyd_faker = faker.Faker(language)
    threeyd_faker.add_provider(ThreeydProvider)
    return threeyd_faker
