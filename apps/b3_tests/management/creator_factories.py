from django.core.exceptions import ValidationError
from apps.b3_tests.management.mappings import FieldFakerMapping, \
    FieldValidationMapping
from apps.b3_tests.management.threeyd_faker import get_threeyd_faker
from apps.b3_tests import factories
from apps.b3_tests.management.settings import FACTORY_MAP


class MicroFactoryManager:
    def __init__(self, model_name):
        factory_name = FACTORY_MAP[model_name]
        self.factory = getattr(factories, factory_name)
        self.validator = FactoryInputValidator(self.factory)
        self.supplementer = FactoryInputSupplementer(self.factory)

    def peek(self):
        for field in self.factory._meta.model._meta.fields:
            yield (field.name, field.__class__.__name__)

    def validate(self, input_data):
        return self.validator.validate(input_data)

    def cast(self, input_data):
        return self.supplementer.create_single(input_data)


class FactoryInputValidator:
    """
    Takes a factory and checks for valid user input to
    avoid errors later when the model should be created
    in the database
    """

    def __init__(self, factory):
        self.model = factory._meta.model
        self.model_name = self.model.__name__
        self.field_validator_map = FieldValidationMapping(self.model)

    def validate(self, input_data={}):
        """
        Checks all input for mistakes and provides a helpful
        error message if that's not the case.
        :param input_data:
        """
        for field, data in input_data.items():
            try:
                validator = self.field_validator_map[field]
            except KeyError:
                raise ValidationError(
                    """
                    Invalid input for model {}:
                    Field < {} > doesn't exist for this model
                    """.format(self.model_name, field)
                )
            try:
                validator(data, self.model)
            except ValidationError:
                raise ValidationError(
                    """
                    Invalid input for model {}:
                    input for field < {} > can not be < {} >
                    """.format(self.model_name, field, data))


class FactoryInputSupplementer:
    """
    A Wrapper Class for django factories. Takes a factory
    and adds functionality that automaticallyfills missing
    input values with faker data according to
    settings.MODEL_FIELD_MAP
    """

    def __init__(self, model_factory):
        self.factory = model_factory
        model = model_factory._meta.model

        self.faker_mapping = FieldFakerMapping(model)
        self.model_fields = self.faker_mapping.get_keys()
        self.f = get_threeyd_faker()
        self.created_object_ids = []

    def __call__(self, single_data):
        return self.create_single(single_data)

    def create_single(self, single_data):
        """
        Heart of this class. Takes a dictionary, supplements
        missing data, and creates a new model with the partly
        supplemented input in the database.
        :return: Object created with supplemented data
        """
        supplemented_data = self._supplement_data(single_data)
        try:
            new_object = self.factory(**supplemented_data)
        except Exception as e:
            raise TypeError("""
            Model was not created by {}
            Supplemented data: {}
            Error: {}
            """.format(self.factory.__name__,
                       supplemented_data,
                       str(e)))
        try:
            self.created_object_ids += [new_object.id]
        except AttributeError:
            self.created_object_ids += [new_object.alpha2]
        return new_object

    def _supplement_data(self, single_data):
        """
        Finds which fields lack data and provides it
        :return: Supplemented Data
        """
        to_be_faked = list(self.model_fields.difference(single_data.keys()))
        to_be_faked.sort()
        for field in to_be_faked:
            single_data[field] = self._fake_this(field)
        return single_data

    def _fake_this(self, field):
        """
        Provides faker results for each field, either without
        or with kwargs
        :return: Supplement for one field
        """
        faker_method = self.faker_mapping[field]
        if isinstance(faker_method, str):
            return getattr(self.f, faker_method)()
        else:
            faker_method, kwargs = faker_method
            return getattr(self.f, faker_method)(**kwargs)
