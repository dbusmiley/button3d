from django.db.models import Field, AutoField
from apps.b3_tests.management import settings as s


class FieldMapping:
    """
    Basically a dict, used to make code more readable.
    Takes the fields of a model and maps ~Something~ onto them.
    Behaviour is specified by subclassing and defining
    _find_map_for_field and _is_field_to_be_mapped
    """
    def __init__(self, model):
        """
        First filters model's field according to
        self._is_field_to_be_mapped then finds its mapping
        :param model: Django model
        """
        self.field_mapping = {
            field.name: self._find_map_for_field(field)
            for field in model._meta.get_fields()
            if self._is_field_to_be_mapped(field)}

    def __getitem__(self, key):
        return self.field_mapping[key]

    def __str__(self):
        repr_string = ""
        for key, value in self.field_mapping.items():
            repr_string += "{} ---> {}\n".format(key, value)
        return repr_string

    def get_keys(self):
        field_list = self.field_mapping.keys()
        return frozenset(field_list)

    def _find_map_for_field(self, field):
        """
        Set what should be mapped onto field
        :param field: subclass of django.db.models.Field
        :return: Mapping for field
        """
        return ""

    @staticmethod
    def _is_field_to_be_mapped(field):
        """
        Set conditions which fields should be considered for this mapping
        Default is
        :param field: subclass of django.db.models.Field
        :return: Boolean, True if fields should have a mapping
        """
        return True


class FieldFakerMapping(FieldMapping):
    """
    Maps Fields unto faker methods that are suited for them.
    """
    def __init__(self, model):
        self.model_name = model.__name__
        self.model_config = s.MODEL_FAKER_MAP.get(model.__name__, {}).copy()
        FieldMapping.__init__(self, model)
        self._override()

    def _find_map_for_field(self, field):
        """
        :return: faker method according to configuration in
        MODEL_FAKER_MAP, or as default according to FIELD_MAP
        """
        try:
            return self.model_config.pop(
                field.name,
                s.FIELD_MAP.get(field.__class__.__name__))
        except KeyError:
            raise KeyError("""
            Didn't find a match for field {}
            with class {} in model {}
            """.format(field.name,
                       field.__class__.__name__,
                       self.model_name))

    @staticmethod
    def _is_field_to_be_mapped(field):
        """
        True if field is an "attribute", ForeignKey or similar
        and no AutoField
        """
        return isinstance(field, Field) \
            and not isinstance(field, AutoField) \
            and not hasattr(field, "rel_class")

    def _override(self):
        for key, value in self.model_config.items():
            self.field_mapping[key] = value


class FieldValidationMapping(FieldMapping):
    """
    Maps fields unto their clean method.
    """
    def _find_map_for_field(self, field):
        return field.clean

    @staticmethod
    def _is_field_to_be_mapped(field):
        return hasattr(field, "clean")
