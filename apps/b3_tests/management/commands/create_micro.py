from django.core.exceptions import ValidationError
from apps.b3_tests.management.creator_factories import \
    MicroFactoryManager
from django.core.management import BaseCommand, CommandError


class Command(BaseCommand):
    help = """
    Creates single entries in the database.\n\n\n\n"""

    def add_arguments(self, parser):
        parser.add_argument(
            "model",
            type=str,
            help="Model that you want to work with"
        )
        parser.add_argument(
            "--peek",
            dest="peek_mode",
            help="Print models fields to the terminal",
            action="store_true"
        )
        parser.add_argument(
            "--cast",
            dest="attributes",
            nargs="+",
            default=[],
            type=str,
            help="Set values for fields in this model instance"
        )

    def handle(self, *args, **options):
        manager = MicroFactoryManager(options.pop("model"))

        if options.pop("peek_mode"):
            self.peek(manager)

        else:
            attributes = dict(map(lambda x: x.split(":"),
                                  options["attributes"]))
            self.create(manager, attributes)

    def peek(self, manager):
        for output in manager.peek():
            self.stdout.write("{} ---> {}\n".format(*output))

    @staticmethod
    def create(manager, attributes):
        try:
            manager.validate(attributes)
            manager.cast(attributes)
        except ValidationError as e:
            raise CommandError(e.message)
