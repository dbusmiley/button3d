from factory.django import DjangoModelFactory
import factory

from apps.b3_checkout.constants import PAYMENT_METHODS
from apps.b3_checkout.models import Payment, PaymentAttachment,\
    PartnerPaymentMethod
from apps.b3_tests.factories import UserFactory


class PartnerPaymentMethodFactory(DjangoModelFactory):
    class Meta:
        model = PartnerPaymentMethod

    type_name = PAYMENT_METHODS.INVOICE
    priority = 1


class PaymentFactory(DjangoModelFactory):
    class Meta:
        model = Payment

    user = factory.SubFactory(UserFactory)
    payment_method = factory.SubFactory(PartnerPaymentMethodFactory)


class PaymentAttachmentFactory(DjangoModelFactory):
    class Meta:
        model = PaymentAttachment

    uploader = factory.SubFactory(UserFactory)
    payment = factory.SubFactory(
        PaymentFactory,
        user=factory.SelfAttribute('uploader')
    )
    filesize = 10
    file = factory.django.FileField(filename='Test file')
