# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-15 13:09
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('b3_checkout', '0003_auto_20180813_1138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paymentattachment',
            name='uploader',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_attachments', to=settings.AUTH_USER_MODEL),
        ),
    ]
