# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-09-06 13:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('b3_order', '0001_initial'),
        ('b3_checkout', '0004_auto_20180815_1309'),
    ]

    operations = [
        migrations.AddField(
            model_name='payment',
            name='order',
            field=models.OneToOneField(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='payment', to='b3_order.Order'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='payment',
            name='date_time',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False),
        ),
        migrations.AlterField(
            model_name='partnerpaymentmethod',
            name='partner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payment_methods', to='partner.Partner'),
        ),
    ]
