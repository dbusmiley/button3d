import json
import logging

import requests
from django.core.serializers.json import DjangoJSONEncoder

import button3d.type_declarations as td

logger = logging.getLogger(__name__)


class OrderWebhookSender:
    def __init__(
            self,
            order: td.OrderNew,
            serializer_class: td.SerializerClass,
    ):
        self.order = order
        self.serializer_class = serializer_class

    def send_webhook(self) -> None:
        partner = self.order.partner
        webhook_url = partner.webhook
        if webhook_url:
            data = self.serializer_class(instance=self.order).data

            order_data = json.dumps(data, cls=DjangoJSONEncoder)
            headers = {'Content-type': 'application/json'}
            response = requests.post(
                webhook_url, data=order_data, headers=headers
            )
            response.raise_for_status()
