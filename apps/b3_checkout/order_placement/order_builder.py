import logging
import typing as t

from apps.b3_checkout.order_placement.email_sender import OrderEmailSender
from apps.b3_checkout.order_placement.webhook_sender import OrderWebhookSender
from apps.b3_order.webhook_serializers import OrderWebhookSerializer
from button3d import type_declarations as td
from apps.b3_checkout.exceptions import PaymentNotAuthorizedError, PaymentError
from apps.b3_checkout.validators import errors
from apps.b3_order.models import Order, OrderAttachment
from apps.b3_organization.utils import get_current_site

logger = logging.getLogger(__name__)


class OrderFromProjectBuilder:
    def __init__(
        self,
        project: td.Project,
        user: td.User,
        customer_language: str,
        customer_ip: str,
        customer_reference: str,
        billing_address: td.Address,
        pickup_location: t.Optional[td.PickupLocation],
        shipping_method: t.Optional[td.ShippingMethod],
        shipping_address: t.Optional[td.Address],
        delivery_instructions: str,
        payment_method: td.PartnerPaymentMethod,
        payment_arguments: td.StrKeyValueDict,
        price_calculator: td.ProjectPriceCalculator,
        voucher: t.Optional[td.Voucher]
    ):
        self.project = project
        self.user = user
        self.customer_language = customer_language
        self.customer_ip = customer_ip
        self.customer_reference = customer_reference
        self.pickup_location = pickup_location
        self.shipping_method = shipping_method
        self.delivery_instructions = delivery_instructions
        self.payment_method = payment_method
        self.payment_arguments = payment_arguments
        self.price_calculator = price_calculator
        self.voucher = voucher

        self.user_shipping_address = shipping_address
        self.user_billing_address = billing_address

        self.order: t.Optional[td.Order] = None
        self.payment: t.Optional[td.Payment] = None

    def create_order_object(self) -> td.Order:
        price_calculator = self.price_calculator
        project = self.project
        order_number = self._generate_order_number()
        site = get_current_site()

        self._clone_addresses()

        self.order = Order.objects.create(
            site=site,
            number=order_number,
            title=project.title or '',
            partner=project.partner,
            project=project,

            purchased_by=self.user,
            customer_language=self.customer_language,
            customer_ip=self.customer_ip,
            customer_reference=self.customer_reference,
            billing_address=self.billing_address,

            pickup_location=self.pickup_location,
            shipping_method=self.shipping_method,
            shipping_address=self.shipping_address,
            delivery_instructions=self.delivery_instructions,

            currency=price_calculator.currency,
            tax_rate=price_calculator.tax_rate,
            subtotal_value=price_calculator.subtotal.excl_tax,
            subtotal_tax=price_calculator.subtotal.tax,
            shipping_value=price_calculator.shipping.excl_tax,
            shipping_tax=price_calculator.shipping.tax,
            min_price_value=price_calculator.min_price.excl_tax,
            min_price_tax=price_calculator.min_price.tax,
            min_price_diff_value=price_calculator.min_price_diff.excl_tax,
            min_price_diff_tax=price_calculator.min_price_diff.tax,
            voucher=self.voucher,
            voucher_discount_value=price_calculator.voucher_discount.excl_tax,
            voucher_discount_tax=price_calculator.voucher_discount.tax,
            fees_value=price_calculator.fees.excl_tax,
            fees_tax=price_calculator.fees.tax,
            total_value=price_calculator.total.excl_tax,
            total_tax=price_calculator.total.tax
        )

        for line_price_calculator in price_calculator.lines:
            self._create_order_line(line_price_calculator)

        for attachment in project.attachments.filter(basket_line__isnull=True):
            self._create_attachment(attachment)

        for fee in price_calculator.fee_objects:
            self.order.applied_fees.create(
                name=fee.name,
                excl_tax=fee.amount.excl_tax,
                tax=fee.amount.tax
            )

        return self.order

    def retrieve_payment(self) -> td.Payment:
        try:
            self.payment = self.payment_method.retrieve_payment(
                self.order,
                self.payment_arguments
            )
        except PaymentNotAuthorizedError as exc:
            logger.warning(exc, stack_info=True, exc_info=exc)
            raise errors.OrderPlacementError(
                errors.ERROR_PAYMENT_NOT_AUTHORIZED,
                'The payment was not authorized with the selected '
                'the payment provider.'
            ) from exc
        except PaymentError as exc:
            logger.error(exc, stack_info=True, exc_info=exc)
            raise errors.OrderPlacementError(
                errors.ERROR_PAYMENT_FAILED,
                'The payment was not successful. '
                'Please try again or choose a different payment method.'
            ) from exc

        return self.payment

    def _clone_addresses(self) -> None:
        self.shipping_address, self.billing_address = None, None
        if self.user_shipping_address:
            self.shipping_address = self.user_shipping_address.clone()
        if self.user_billing_address:
            self.billing_address = self.user_billing_address.clone()

    def _create_order_line(self, line_price_calculator) -> None:
        line = line_price_calculator.line
        order_line = self.order.lines.create(
            name=line.name or '',
            stl_file=line.stl_file,
            stock_record=line.stockrecord,
            configuration=line.configuration,
            quantity=line.quantity,
            unit_price_value=line_price_calculator.unit_price.excl_tax,
            unit_price_tax=line_price_calculator.unit_price.tax,
            item_discount_value=line_price_calculator.item_discount.excl_tax,
            item_discount_tax=line_price_calculator.item_discount.tax,
            item_total_value=line_price_calculator.item_total.excl_tax,
            item_total_tax=line_price_calculator.item_total.tax

        )
        for option in line.post_processing_options.all():
            order_line.post_processing_options.create(
                post_processing=option.post_processing,
                color=option.color
            )
        for attachment in line.attachments.all():
            self._create_attachment(attachment, order_line)

    def _create_attachment(self, project_attachment, order_line=None) -> None:
        order_attachment = OrderAttachment.objects.create(
            order=self.order,
            order_line=order_line,
            filename=project_attachment.filename,
            filesize=project_attachment.filesize,
            uploader=project_attachment.uploader,
            creation_date=project_attachment.creation_date
        )
        order_attachment.file.save(
            name=project_attachment.filename,
            content=project_attachment.file,
            save=True
        )

    def _generate_order_number(self) -> str:
        return str(100000 + self.project.id)

    @property
    def emails(self):
        return OrderEmailSender(self.order)

    @property
    def webhook(self):
        return OrderWebhookSender(
            order=self.order,
            serializer_class=OrderWebhookSerializer
        )
