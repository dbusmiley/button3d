import button3d.type_declarations as td


class OrderEmailSender:
    def __init__(self, order: td.OrderNew):
        self.order = order

    def send(self) -> None:
        self.order.send_order_placed_email()
