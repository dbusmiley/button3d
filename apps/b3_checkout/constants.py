from types import SimpleNamespace


class PaymentMethodSlugs(SimpleNamespace):
    INVOICE = 'invoice'
    PAY_IN_STORE = 'pay-in-store'
    VW = 'volkswagen'
    PO_UPLOAD = 'po-upload'
    STRIPE = 'stripe'
    NETS = 'nets'
    CUSTOM = 'custom'


PAYMENT_METHODS = PaymentMethodSlugs()

PAYMENT_METHOD_CHOICES = [
    (PAYMENT_METHODS.INVOICE, 'Invoice'),
    (PAYMENT_METHODS.PAY_IN_STORE, 'Pay in Store'),
    (PAYMENT_METHODS.VW, 'Dienstleistungsvereinbarung (VW)'),
    (PAYMENT_METHODS.PO_UPLOAD, 'Purchase Order Upload'),
    (PAYMENT_METHODS.STRIPE, 'Credit Card via Stripe'),
    (PAYMENT_METHODS.NETS, 'Credit Card via Nets'),
    (PAYMENT_METHODS.CUSTOM, 'Custom'),
]
