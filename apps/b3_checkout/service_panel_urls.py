from django.conf.urls import url, include

from apps.b3_checkout.views.payment_methods import \
    PaymentMethodListCreate, PaymentMethodTypes, \
    PaymentMethodRetrieveUpdateDelete
from apps.b3_checkout.views.payment_methods.connect_stripe_account import \
    ConnectStripeAccount
from apps.b3_checkout.views.payment_methods.fetch_stripe_callback_url import \
    FetchStripeCallbackURL
from apps.b3_shipping.views.pickup_location_service_panel_list_create import \
    PickupLocationServicePanelListCreate
from apps.b3_shipping.views.pickup_location_service_panel_update_delete \
    import PickupLocationServicePanelRetrieveUpdateDestroy
from apps.b3_shipping.views.shipping_method_service_panel_list_create import \
    ShippingMethodServicePanelListCreate
from apps.b3_shipping.views.shipping_method_service_panel_update_delete \
    import ShippingMethodServicePanelUpdateDelete

urlpatterns = [
    url(r'^services/(?P<service_id>\d+)/payment-method-types/$',
        PaymentMethodTypes.as_view()),
    url(r'^services/(?P<service_id>\d+)/payment-methods/$',
        PaymentMethodListCreate.as_view()),
    url(r'^services/(?P<service_id>\d+)/payment-methods/'
        r'(?P<payment_method_id>\d+)/$',
        PaymentMethodRetrieveUpdateDelete.as_view()),

    url(r'^services/(?P<service_id>\d+)/pickup-locations/$',
        PickupLocationServicePanelListCreate.as_view()),
    url(r'^services/(?P<service_id>\d+)/pickup-locations/'
        r'(?P<pickup_location_id>\d+)/$',
        PickupLocationServicePanelRetrieveUpdateDestroy.as_view()),

    url(r'^services/(?P<service_id>\d+)/shipping-methods/$',
        ShippingMethodServicePanelListCreate.as_view()),
    url(r'^services/(?P<service_id>\d+)/shipping-methods/'
        r'(?P<shipping_method_id>\d+)/$',
        ShippingMethodServicePanelUpdateDelete.as_view()),

    url(r'^services/(?P<service_id>\d+)/vouchers/',
        include('apps.b3_voucher.urls')),

    url(r'^services/(?P<service_id>\d+)/payment-methods/stripe/callbackurl/$',
        FetchStripeCallbackURL.as_view()),
    url(r'^services/(?P<service_id>\d+)/payment-methods/stripe/connect/$',
        ConnectStripeAccount.as_view())
]
