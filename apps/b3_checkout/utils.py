import typing as t

from django.db.models import Max

import button3d.type_declarations as td


def get_max_payment_method_priority_for_partner(
        partner: td.Partner,
        partner_payment_method_class: t.Type[
            td.PartnerPaymentMethod]
):
    """
    Gets the biggest priority from list of partner's PartnerPaymentMethods.

    The reason to pass `partner_payment_method_class` instead of referencing
    class via related_name, is because this function is used in migration,
    where related objects are unavailable.
    """
    max_priority_aggregate = partner_payment_method_class.objects \
        .filter(partner=partner) \
        .aggregate(max_priority=Max('priority'))
    max_priority = max_priority_aggregate['max_priority'] or 0
    return max_priority


def get_next_payment_method_priority_for_partner(
        partner: td.Partner,
        partner_payment_method_class: t.Type[
            td.PartnerPaymentMethod]
):
    max_priority = get_max_payment_method_priority_for_partner(
        partner,
        partner_payment_method_class
    )
    return max_priority + 1
