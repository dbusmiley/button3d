from rest_framework.fields import CharField
from rest_framework.serializers import Serializer

from apps.b3_checkout.payment_methods.base import BasePaymentMethod


class VolkswagenPaymentMethod(BasePaymentMethod):
    """
    VW-specific payment method. Payment is accomplished by submitting form
    data with details of user that made the order.
    """
    visible_at_service_panel = False
    name = 'Dienstleistungsvereinbarung (DLV)'

    class PaymentArgumentsSerializer(Serializer):
        creator_name = CharField()
        creator_email = CharField()
        provider = CharField()
        proposal_number = CharField()
        proposal = CharField()
        description = CharField()
        delivery_date = CharField()
        charge_from = CharField()
        charge_to = CharField()
