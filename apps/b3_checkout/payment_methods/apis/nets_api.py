import logging
from decimal import Decimal as D
import requests
import typing as t

import button3d.type_declarations as td

from django.utils.http import urlencode

from apps.b3_checkout.constants import PAYMENT_METHODS
from apps.b3_core.utils import quantize
from apps.b3_organization.utils import get_base_url


logger = logging.getLogger(__name__)


class NetsAPIError(Exception):
    pass


class NetsApi(object):
    CART_ENDPOINT = 'carts'
    PAYMENT_ENDPOINT = 'carts/{cartId}/payments'
    VERIFICATION_ENDPOINT = 'carts/{cartId}/payments/latest/verifications'
    FINALIZATION_ENDPOINT = 'carts/{cartId}/payments/latest/finalizations'
    COMPLETION_ENDPOINT = 'carts/{cartId}/items/{itemId}/completions'

    def __init__(self, client_id: str, order_id_prefix: str, base_url: str):
        self.client_id = client_id
        self.order_id_prefix = order_id_prefix
        self.base_url = base_url

    def create_cart(self, total: td.Price) -> t.Tuple[str, str]:
        cart_url = '{0}{1}'.format(self.base_url, self.CART_ENDPOINT)

        # Derive the vat rate from the tax incl/excl prices.
        # This is necessary since the vat rate of the total price
        # depends on the vat rates of the product and of the shipping
        excl_tax_converted = int(total.excl_tax.shift(2))
        tax_converted = int(total.tax.shift(2)) if total.tax else 0

        cart_payload = {
            'items': [
                {
                    'orderIdPrefix': self.order_id_prefix,
                    'baseArticleId': '0',
                    'amount': excl_tax_converted,
                    'currencyCode': total.currency,
                    'vat': 0,
                    'vatAmount': tax_converted
                }
            ]
        }

        logger.debug(
            f'NetsAPI: creating cart. URL {cart_url}, '
            f'payload {cart_payload}')
        cart_response = requests.post(cart_url, json=cart_payload)

        response_json = cart_response.json()
        logger.debug(f'NetsAPI: cart creation response: {response_json}')

        cart_id = response_json['cartId']
        item_id = response_json['itemIds'][0]
        return cart_id, item_id

    def create_payment(self, cart_id: str, item_id: str, email: str) -> str:
        redirect_url = '{0}/payment-redirect-target/?{1}'.format(
            get_base_url(),
            urlencode(
                dict(
                    cart_id=cart_id,
                    item_id=item_id,
                    payment_method=PAYMENT_METHODS.NETS
                )
            )
        )
        logger.debug(f'NETS Redirect URL: {redirect_url}')

        payment_url = '{0}{1}'.format(
            self.base_url,
            self.PAYMENT_ENDPOINT.format(cartId=cart_id)
        )

        payment_payload = {
            'clientId': self.client_id,
            'paymentMethodId': 'CARD',
            'countryCode': 'GLOBAL',
            'invoiceInfo': {
                'email': email
            },
            'locale': 'en_GB',
            'redirectUrl': redirect_url
        }

        logger.debug(
            f'NetsAPI: creating payment. URL {payment_url}, '
            f'payload {payment_payload}')

        payment_response = requests.post(payment_url, json=payment_payload)
        response_json = payment_response.json()
        if 200 > payment_response.status_code > 300:
            msg = f'NetsAPI: payment creation status' \
                f' {payment_response.status_code} and response {response_json}'
            logger.warning(msg)
            raise NetsAPIError(msg)
        else:
            logger.debug(
                f'NetsAPI: payment creation response: {response_json}')

            return response_json['terminalUrl']

    def get_verification_info(self, cart_id: str) -> t.Tuple[str, td.Decimal]:
        verification_url = '{0}{1}'.format(
            self.base_url,
            self.VERIFICATION_ENDPOINT.format(cartId=cart_id)
        )
        logger.debug(
            f'NetsAPI: getting verification info. URL {verification_url}')

        verification_response = requests.post(verification_url)
        response_json = verification_response.json()
        logger.debug(
            f'NetsAPI: verification response: {response_json}')

        status = response_json['paymentStatus']['cartStatus']
        amount = response_json['totalAmount']
        amount_converted = quantize(D(amount)/D(100))

        return status, amount_converted

    def finalize_payment(self, cart_id: str) -> str:
        finalization_url = '{0}{1}'.format(
            self.base_url,
            self.FINALIZATION_ENDPOINT.format(cartId=cart_id)
        )
        logger.debug(f'NetsAPI: finalizing payment. URL {finalization_url}')

        finalization_response = requests.post(finalization_url)
        response_json = finalization_response.json()
        logger.debug(f'NetsAPI: finalization response: {response_json}')

        return response_json['paymentStatus']['cartStatus']

    def complete_payment(self, cart_id: str, item_id: str) -> None:
        completion_url = '{0}{1}'.format(
            self.base_url,
            self.COMPLETION_ENDPOINT.format(cartId=cart_id, itemId=item_id)
        )
        logger.debug(f'NetsAPI: completing payment. URL {completion_url}')

        response = requests.post(completion_url)
        response_json = response.text

        logger.debug(
            f'NetsAPI: payment completion response: {response_json}')
