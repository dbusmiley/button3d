import logging

import stripe
from django.conf import settings

import button3d.type_declarations as td

logger = logging.getLogger(__name__)


class StripeApi:
    CardError = stripe.error.CardError
    StripeError = stripe.error.StripeError

    def __init__(self):
        self.api_key = settings.STRIPE_SECRET_KEY

    def charge(
        self,
        amount: td.Decimal,
        currency: str,
        destination: str,
        application_fee: td.Decimal,
        source: str,
        description: str,
        metadata: td.StrKeyDict
    ):
        if destination == self._get_3yd_account():
            return stripe.Charge.create(
                api_key=self.api_key,
                amount=int(amount.shift(2)),
                currency=currency,
                source=source,
                description=description,
                metadata=metadata
            )

        destination_amount = amount - application_fee
        return stripe.Charge.create(
            api_key=self.api_key,
            amount=int(amount.shift(2)),
            currency=currency,
            destination={
                'account': destination,
                'amount': int(destination_amount.shift(2))
            },
            source=source,
            description=description,
            metadata=metadata
        )

    def _get_3yd_account(self):
        return stripe.Account.retrieve(api_key=self.api_key).id
