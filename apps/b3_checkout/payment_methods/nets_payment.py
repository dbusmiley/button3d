import logging

from django.conf import settings
from rest_framework import fields
from rest_framework.serializers import Serializer

import button3d.type_declarations as td
from apps.b3_checkout.exceptions import PaymentNotAuthorizedError, \
    PaymentGatewayError
from apps.b3_checkout.payment_methods.apis.nets_api import NetsApi
from apps.b3_checkout.payment_methods.base import BasePaymentMethod


logger = logging.getLogger(__name__)


class NetsPaymentMethod(BasePaymentMethod):
    visible_at_service_panel = False
    name = 'Credit Card via NETS'

    class PaymentArgumentsSerializer(Serializer):
        cartId = fields.CharField()
        itemId = fields.CharField()

    def __init__(self) -> None:
        self.api = NetsApi(
            client_id=settings.NETS_CLIENT_ID,
            order_id_prefix=settings.NETS_ORDER_ID_PREFIX,
            base_url=settings.NETS_BASE_URL,
        )

    def retrieve_payment(
        self,
        order: td.Order,
        partner_payment_method: td.PartnerPaymentMethod,
        payment_arguments: td.StrKeyDict
    ) -> td.Payment:
        cart_id = payment_arguments['cartId']
        item_id = payment_arguments['itemId']

        try:
            verification_status, amount = self.api.get_verification_info(
                cart_id
            )
        except Exception as e:
            raise PaymentGatewayError from e
        if verification_status != 'COMMITTED':
            raise PaymentNotAuthorizedError

        expected_amount = order.total.incl_tax
        if amount != expected_amount:
            logger.error(
                f'Price calculated by NETS API differs from internally '
                f'calculated price: received = {amount}, '
                f'expected = {expected_amount}'
            )
            raise PaymentGatewayError

        try:
            finalization_status = self.api.finalize_payment(cart_id)
        except Exception as e:
            raise PaymentGatewayError from e
        if finalization_status != 'PAID':
            raise PaymentNotAuthorizedError

        try:
            self.api.complete_payment(cart_id, item_id)
        except Exception as e:
            raise PaymentGatewayError from e

        return super().retrieve_payment(
            order=order,
            partner_payment_method=partner_payment_method,
            payment_arguments=payment_arguments
        )

    def initialize_payment(
        self,
        price: td.Price,
        user: td.User
    ) -> td.StrKeyDict:
        try:
            cart_id, item_id = self.api.create_cart(price)
            terminal_url = self.api.create_payment(
                cart_id, item_id, user.email
            )
        except Exception as e:
            raise PaymentGatewayError from e

        return {
            'cart_id': cart_id,
            'item_id': item_id,
            'terminal_url': terminal_url
        }
