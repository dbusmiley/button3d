import typing as t
from rest_framework import serializers

import button3d.type_declarations as td
from apps.b3_checkout.payment_methods.base import BasePaymentMethod


class POUploadPaymentArgumentsSerializer(serializers.Serializer):
    attachments = serializers.ListField(
        child=serializers.IntegerField(), allow_empty=False
    )

    def validate_attachments(self, attachment_ids: t.List[int]) -> t.List[int]:
        """
        Attachments must exist in DB and have payment=NULL, i.e. not used
        for payments before
        """
        from apps.b3_checkout.models import PaymentAttachment
        present_attachment_ids = PaymentAttachment.objects.filter(
            id__in=attachment_ids,
            payment__isnull=True
        ).values_list('id', flat=True)

        missing_attachment_ids = [
            str(attachment_id)
            for attachment_id in attachment_ids
            if attachment_id not in present_attachment_ids
        ]
        if missing_attachment_ids:
            raise serializers.ValidationError(
                f'Not all attachments exist: '
                f'{", ".join(missing_attachment_ids)}'
            )
        return attachment_ids


class POUploadPaymentMethod(BasePaymentMethod):
    visible_at_service_panel = False
    name = 'Purchase Order'
    PaymentArgumentsSerializer = POUploadPaymentArgumentsSerializer

    def retrieve_payment(
        self,
        order: td.Order,
        partner_payment_method: td.PartnerPaymentMethod,
        payment_arguments: td.StrKeyDict
    ) -> td.Payment:
        """
        Accepts payment metadata in `payment_arguments` in form of file
        attachment id list. Uses serializer to validate it
        """
        from apps.b3_checkout.models import PaymentAttachment

        payment = super().retrieve_payment(
            order=order,
            partner_payment_method=partner_payment_method,
            payment_arguments=payment_arguments
        )

        attachments = payment_arguments['attachments']
        for po_upload in PaymentAttachment.objects.filter(pk__in=attachments):
            po_upload.payment = payment
            po_upload.save(update_fields=['payment'])

        return payment
