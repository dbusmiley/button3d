import typing as t
from decimal import Decimal as D

from rest_framework import serializers
from rest_framework.fields import CharField
from rest_framework.serializers import Serializer

import button3d.type_declarations as td
from apps.b3_checkout.exceptions import PaymentNotAuthorizedError, \
    PaymentGatewayError
from apps.b3_checkout.payment_methods.apis.stripe_api import StripeApi
from apps.b3_checkout.payment_methods.base import BasePaymentMethod
from apps.b3_core.utils import quantize


class StripePaymentMethod(BasePaymentMethod):
    name = 'Credit Card via Stripe'
    visible_at_service_panel = True

    class PublicConfigArgumentsSerializer(Serializer):
        stripe_publishable_key = serializers.CharField()
        stripe_user_id = serializers.CharField()

    class PaymentArgumentsSerializer(Serializer):
        token = CharField()

    def __init__(self, stripe_user_id: str, **kwargs: t.Any):
        """
        Initializes payment method instance, accepts required arguments and
        validates their correctness.
        """
        self.stripe_user_id = stripe_user_id
        self.api = StripeApi()

    def retrieve_payment(
        self,
        order: td.Order,
        partner_payment_method: td.PartnerPaymentMethod,
        payment_arguments: td.StrKeyDict
    ) -> td.Payment:
        partner = partner_payment_method.partner
        amount = order.total.incl_tax
        currency = order.total.currency
        application_fee = self._calculate_application_fee(order.total, partner)
        token = payment_arguments['token']

        try:
            charge = self.api.charge(
                destination=self.stripe_user_id,
                amount=amount,
                currency=currency,
                application_fee=application_fee,
                source=token,
                description=self._get_description(order),
                metadata=self._get_metadata(order),
            )
        except StripeApi.CardError:
            raise PaymentNotAuthorizedError
        except StripeApi.StripeError as e:
            raise PaymentGatewayError from e

        return self.save_payment(
            order=order,
            payment_method=partner_payment_method,
            metadata={
                'application_fee': str(application_fee),
                'stripe_account': self.stripe_user_id,
                'charge_id': charge.id
            }
        )

    @staticmethod
    def _get_description(order: td.Order) -> str:
        return f'Order #{order.number}'

    @staticmethod
    def _get_metadata(order: td.Order) -> td.StrKeyDict:
        user = order.purchased_by
        return {
            'order_user_email': user.email,
            'order_user_id': user.pk,
            'order_number': f'#{order.number}',
        }

    @staticmethod
    def _calculate_application_fee(price: td.Price, partner: td.Partner) \
            -> td.Decimal:
        """
        If partner is in Germany: Base fee on gross price
        If partner is outside Germany: Base fee on net price
        """
        fee_percentage = partner.org_options.stripe_3yd_fee or D('0.00')

        if partner.address.country.alpha2 == 'DE':
            fee = price.incl_tax * (fee_percentage / D('100'))
        else:
            fee = price.excl_tax * (fee_percentage / D('100'))

        return quantize(fee)
