import typing as t

from rest_framework.fields import CharField
from rest_framework.serializers import Serializer

from apps.b3_checkout.payment_methods.base import BasePaymentMethod


class CustomPaymentMethod(BasePaymentMethod):
    visible_at_service_panel = True
    arguments_visible_to_user = ['name', 'description']

    class PublicConfigArgumentsSerializer(Serializer):
        """
        Validates, that arguments contain at least `name` value
        """
        name = CharField(max_length=128)
        description = CharField(required=False)

    def __init__(
        self,
        name: str,
        description: t.Optional[str] = None
    ) -> None:
        self.name = name
        self.description = description
