import typing as t

import button3d.type_declarations as td
from apps.b3_checkout.constants import PAYMENT_METHODS

from .base import BasePaymentMethod
from .custom_payment import CustomPaymentMethod
from .pay_in_store import PayInStorePayment
from .invoice import InvoicePaymentMethod
from .nets_payment import NetsPaymentMethod
from .po_upload import POUploadPaymentMethod
from .stripe_payment import StripePaymentMethod
from .volkswagen_payment import VolkswagenPaymentMethod

PAYMENT_METHOD_CLASSES: td.PaymentMethodClasses = (
    (PAYMENT_METHODS.INVOICE, InvoicePaymentMethod),
    (PAYMENT_METHODS.PAY_IN_STORE, PayInStorePayment),
    (PAYMENT_METHODS.VW, VolkswagenPaymentMethod),
    (PAYMENT_METHODS.PO_UPLOAD, POUploadPaymentMethod),
    (PAYMENT_METHODS.STRIPE, StripePaymentMethod),
    (PAYMENT_METHODS.NETS, NetsPaymentMethod),
    (PAYMENT_METHODS.CUSTOM, CustomPaymentMethod),
)


def get_payment_method_class(slug: str) -> t.Type[BasePaymentMethod]:
    return dict(PAYMENT_METHOD_CLASSES)[slug]
