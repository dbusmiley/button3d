from apps.b3_checkout.payment_methods.base import BasePaymentMethod


class PayInStorePayment(BasePaymentMethod):
    """
    This payment method does no processing at the moment of payment. Buyer
    pays for his order at the moment of pickup.
    """
    visible_at_service_panel = False
    name = 'Pay in store'
