import typing as t

from rest_framework.serializers import Serializer

import button3d.type_declarations as td


class BasePaymentMethod:
    """
    Base class for PaymentMethod classes. Defines abstract fields and methods
    to be implemented in subclasses
    """
    # defines wherer this payment method will be available for creation by
    # default for all partners or will require to be explicitly enabled by
    # admin
    visible_at_service_panel: bool = False

    # human-readable name and description for payment method.
    # May be overridden with property, that will somehow calculate this value
    name: t.Optional[str] = None
    description: t.Optional[str] = None

    # List of argument names from PartnerPaymentMethod.public_config_arguments
    # that are visible to the user when using the payment method
    arguments_visible_to_user = []

    # serializer class to initially validate arguments passed to payment class
    # __init__ method
    PublicConfigArgumentsSerializer = Serializer

    # Serializer class that validates payment_arguments passed to
    # handle_payment (such as tokens etc)
    PaymentArgumentsSerializer = Serializer

    def retrieve_payment(
        self,
        order: td.Order,
        partner_payment_method: td.PartnerPaymentMethod,
        payment_arguments: td.StrKeyDict
    ) -> td.Payment:
        return self.save_payment(
            order=order,
            payment_method=partner_payment_method,
            metadata=payment_arguments
        )

    def initialize_payment(
        self,
        price: td.Price,
        user: td.User
    ) -> td.StrKeyDict:
        """
        Can  be overridden by subclasses to implement logic that has to happen
        on the backend before the frontend can collect all payment arguments.
        This method should return all information that the frontend needs to
        proceed with the payment authorization.
        """
        return {}

    # noinspection PyMethodMayBeStatic
    def save_payment(
        self,
        order: td.Order,
        payment_method: td.PartnerPaymentMethod,
        metadata: td.StrKeyDict
    ) -> td.Payment:
        """
        Generic method-wrapper for creating Payment object. Can be overridden
        to add custom behavior
        """
        from apps.b3_checkout.models import Payment
        return Payment.objects.create(
            order=order,
            user=order.purchased_by,
            payment_method=payment_method,
            currency=order.total.currency,
            amount=order.total.incl_tax,
            metadata=metadata,
        )

    def __str__(self) -> str:
        return self.__class__.__name__
