from apps.b3_checkout.payment_methods.base import BasePaymentMethod


class InvoicePaymentMethod(BasePaymentMethod):
    """
    This payment method does no processing at the moment of payment. Partner is
    responsible for issuing invoice to buyer.
    """
    name = 'Invoice'
    visible_at_service_panel = True
