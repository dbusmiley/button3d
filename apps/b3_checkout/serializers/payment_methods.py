import inflection
from drf_payload_customizer.mixins import PayloadConverterMixin
from rest_framework import serializers

from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_checkout.payment_methods import get_payment_method_class


class CreatePartnerPaymentMethodSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type_name')
    configArguments = serializers.DictField(source='public_config_arguments')
    isEnabled = serializers.BooleanField(source='is_enabled')
    isBillingAddressRequired = serializers.BooleanField(
        source='is_billing_address_required', required=False
    )

    def validate_type(self, value: str) -> str:
        partner = self.context['partner']
        payment_method_types = \
            partner.get_available_for_creation_payment_method_types()
        if value not in payment_method_types:
            raise serializers.ValidationError(f'invalid payment type: {value}')
        return value

    def validate(self, attrs: dict) -> dict:
        """
        1. get payment method by its type name
        2. if it has serializer class, validate configArguments
        3. if validation fails, raise error
        :param attrs:
        :return:
        """
        payment_type_name = attrs['type_name']
        payment_method_class = get_payment_method_class(payment_type_name)

        snake_case_dict = {
            inflection.underscore(key): value
            for key, value in attrs['public_config_arguments'].items()
        }
        serializer = payment_method_class.PublicConfigArgumentsSerializer(
            data=snake_case_dict
        )
        if not serializer.is_valid():
            raise serializers.ValidationError({
                'configArguments': serializer.errors
            })

        attrs['public_config_arguments'] = serializer.validated_data
        return attrs

    class Meta:
        model = PartnerPaymentMethod
        fields = [
            'id', 'type', 'configArguments', 'isEnabled',
            'isBillingAddressRequired'
        ]


class UpdatePartnerPaymentMethodSerializer(serializers.ModelSerializer):
    """
    Does not allow (ignores) payment method type_name change.
    This serializer is used in Update view.
    """
    type = serializers.CharField(source='type_name', read_only=True)
    configArguments = serializers.DictField(source='public_config_arguments')
    isEnabled = serializers.BooleanField(source='is_enabled')
    isBillingAddressRequired = serializers.BooleanField(
        source='is_billing_address_required', required=False
    )

    def validate_isEnabled(self, is_enabled):
        """
        Check, that if is_enabled=False, then there is at least one other
        Payment Method, that is enabled
        """
        if is_enabled:
            return is_enabled

        partner = self.instance.partner
        if not partner.has_other_enabled_payment_method_left_except(
            self.instance
        ):
            raise serializers.ValidationError(
                'At least one payment method must be enabled'
            )
        return is_enabled

    def validate_configArguments(self, camel_case_dict):
        payment_method_class = self.instance.payment_method_class
        snake_case_dict = {
            inflection.underscore(key): value
            for key, value in camel_case_dict.items()
        }
        serializer = payment_method_class.PublicConfigArgumentsSerializer(
            data=snake_case_dict
        )
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data

    class Meta:
        model = PartnerPaymentMethod
        fields = [
            'id', 'type', 'configArguments', 'isEnabled',
            'isBillingAddressRequired'
        ]


class PaymentMethodUserPanelSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    type = serializers.CharField(source='type_name', read_only=True)
    additional_information = serializers.SerializerMethodField()
    is_billing_address_required = serializers.BooleanField(read_only=True)

    def get_additional_information(self, obj):
        return {
            inflection.camelize(key, uppercase_first_letter=False):
                obj.public_config_arguments.get(key)
            for key in obj.payment_method_class.arguments_visible_to_user
        }

    class Meta:
        model = PartnerPaymentMethod
        fields = [
            'id',
            'type',
            'additional_information',
            'is_billing_address_required'
        ]
