from django.conf import settings
from drf_payload_customizer.mixins import PayloadConverterMixin

from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField, DecimalField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import Serializer

from apps.b3_checkout.models import PartnerPaymentMethod
from apps.partner.pricing.price import Price


class PaymentInitializationSerializer(PayloadConverterMixin, Serializer):
    currency = CharField()
    price_exclusive_tax = DecimalField(
        max_digits=settings.CURRENCY_FIELD_DIGITS,
        decimal_places=settings.CURRENCY_FIELD_DECIMAL_PLACES,
    )
    price_inclusive_tax = DecimalField(
        max_digits=settings.CURRENCY_FIELD_DIGITS,
        decimal_places=settings.CURRENCY_FIELD_DECIMAL_PLACES,
    )
    method_id = PrimaryKeyRelatedField(
        queryset=PartnerPaymentMethod.objects.all()
    )

    def validate_currency(self, currency):
        if currency not in self.context['service'].supported_currencies:
            raise ValidationError(
                f'Currency {currency} is not supported by the printing service'
            )
        return currency

    def validate_method_id(self, partner_payment_method):
        if partner_payment_method.partner != self.context['service']:
            raise ValidationError(
                'Invalid methodId: does not belong to service.'
            )
        return partner_payment_method

    def validate(self, attrs):
        currency = attrs['currency']
        price_excl_tax = attrs['price_exclusive_tax']
        price_incl_tax = attrs['price_inclusive_tax']

        tax = price_incl_tax - price_excl_tax
        price = Price(excl_tax=price_excl_tax, tax=tax, currency=currency)

        attrs['price'] = price
        return attrs
