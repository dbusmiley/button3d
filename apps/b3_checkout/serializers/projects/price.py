from rest_framework import serializers
from rest_framework.fields import CharField, IntegerField, BooleanField

from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_api.utils.serializers import NestingMixin


class PriceSerializer(serializers.Serializer):
    exclusiveTax = CurrencyField(
        source='excl_tax',
        coerce_to_string=True
    )
    inclusiveTax = CurrencyField(
        source='incl_tax',
        coerce_to_string=True,
        read_only=True
    )


class FloatPriceSerializer(serializers.Serializer):
    """
    Should be removed if/when API switches over to strings for numerical values
    """
    exclusiveTax = CurrencyField(
        source='excl_tax',
        coerce_to_string=False
    )
    inclusiveTax = CurrencyField(
        source='incl_tax',
        coerce_to_string=False,
        read_only=True
    )


class OrderFeeApplicationSerializer(serializers.Serializer):
    name = serializers.CharField()
    amount = PriceSerializer()


class LinePriceUserPanelSerializer(NestingMixin, serializers.Serializer):
    lineId = IntegerField(source='line.id')
    quantity = IntegerField(source='line.quantity')
    unitPrice = PriceSerializer(source='unit_price')
    itemDiscount = PriceSerializer(source='item_discount')
    itemTotal = PriceSerializer(source='item_total')

    class Meta:
        nesting_options = [{
            'name': 'price',
            'translations': [
                ('unitPrice', 'unitPrice'),
                ('itemDiscount', 'itemDiscount'),
                ('itemTotal', 'itemTotal')
            ]
        }]


class ProjectPriceUserPanelSerializer(NestingMixin, serializers.Serializer):
    currency = CharField()
    currency_supported = BooleanField()
    taxRate = CharField(source='tax_rate')
    taxType = CharField(source='partner.tax_type')
    lines = LinePriceUserPanelSerializer(many=True)
    fees = OrderFeeApplicationSerializer(source='fee_objects', many=True)
    minimumPrice = PriceSerializer(source='min_price')
    summarySubTotal = PriceSerializer(source='subtotal')
    summaryShippingCost = PriceSerializer(source='shipping')
    summaryVoucherDiscount = PriceSerializer(source='voucher_discount')
    summaryFees = PriceSerializer(source='fees')
    summaryMinimumPriceDifference = PriceSerializer(source='min_price_diff')
    summaryTotal = PriceSerializer(source='total')

    def get_currency_supported(self):
        partner = self.instance.partner
        return self.instance.currency in partner.payment_currencies

    class Meta:
        nesting_options = [{
            'name': 'priceSummary',
            'translations': [
                ('summarySubTotal', 'subTotal'),
                ('summaryShippingCost', 'shippingCost'),
                ('summaryVoucherDiscount', 'voucherDiscount'),
                ('summaryFees', 'fees'),
                ('summaryMinimumPriceDifference',
                 'minimumPriceDifference'),
                ('summaryTotal', 'total')
            ]
        }]
