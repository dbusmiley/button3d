from rest_framework import serializers
from rest_framework.fields import CharField
from drf_payload_customizer.mixins import PayloadConverterMixin


from apps.b3_address.models import Address
from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_checkout.validators import errors
from apps.b3_checkout.validators.order_from_project_validator \
    import OrderFromProjectValidator
from apps.b3_organization.utils import get_current_org
from apps.b3_shipping.models import PickupLocation, ShippingMethod


class OrderFromProjectShippingSerializer(
    PayloadConverterMixin,
    serializers.Serializer
):
    method_id = serializers.PrimaryKeyRelatedField(
        queryset=ShippingMethod.objects.all(),
        allow_null=True,
        source='shipping_method'
    )
    address_id = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        allow_null=True,
        source='shipping_address'
    )
    delivery_instructions = serializers.CharField(allow_blank=True)

    def get_value(self, data):
        # To make it possible for the frontend to pass `None` for the whole
        # `shipping` object (instead of an object with empty values),
        # we override this method, supplying the expected data.
        value = super().get_value(data)
        if self.allow_null and value is None:
            return {
                'method_id': None,
                'address_id': None,
                'delivery_instructions': ''
            }
        return value


class OrderFromProjectPaymentSerializer(
    PayloadConverterMixin,
    serializers.Serializer
):
    method_id = serializers.PrimaryKeyRelatedField(
        queryset=PartnerPaymentMethod.objects.all(),
        source='payment_method'
    )
    authorized_amount = CurrencyField()
    currency = CharField()
    details = serializers.DictField(source='payment_arguments')


class OrderFromProjectAdditionalInformationSerializer(
    PayloadConverterMixin,
    serializers.Serializer
):
    reference = serializers.CharField(
        max_length=254,
        source='customer_reference',
        allow_blank=True
    )


class OrderFromProjectSerializer(
    PayloadConverterMixin,
    serializers.Serializer
):
    billing_address_id = serializers.PrimaryKeyRelatedField(
        queryset=Address.objects.all(),
        allow_null=True,
        source='billing_address'
    )
    pickup_location_id = serializers.PrimaryKeyRelatedField(
        queryset=PickupLocation.objects.all(),
        allow_null=True,
        source='pickup_location'
    )
    shipping = OrderFromProjectShippingSerializer(source='*', allow_null=True)
    payment = OrderFromProjectPaymentSerializer(source='*')
    voucher_code = serializers.CharField(allow_blank=True)
    additional_information = OrderFromProjectAdditionalInformationSerializer(
        source='*'
    )

    def validate(self, attrs):
        validator = OrderFromProjectValidator(
            project=self.project,
            user=self.user,
            organization=get_current_org(),
            pickup_location=attrs['pickup_location'],
            shipping_method=attrs['shipping_method'],
            shipping_address=attrs['shipping_address'],
            billing_address=attrs['billing_address'],
            payment_method=attrs['payment_method'],
            payment_arguments=attrs['payment_arguments'],
            authorized_amount=attrs['authorized_amount'],
            currency=attrs['currency'],
            voucher_code=attrs['voucher_code']
        )
        if not validator.is_valid():
            raise errors.OrderValidationError(validator.errors)

        attrs['project'] = self.project
        attrs['user'] = self.user
        attrs['price_calculator'] = validator.price_calculator
        attrs['voucher'] = validator.voucher

        del attrs['voucher_code']
        del attrs['authorized_amount']
        del attrs['currency']

        return attrs

    @property
    def project(self):
        return self.context['view'].project

    @property
    def user(self):
        return self.context['request'].user
