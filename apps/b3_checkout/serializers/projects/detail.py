from rest_framework.fields import CharField, SerializerMethodField
from rest_framework.serializers import ModelSerializer
from apps.basket.models import Basket, Line
from apps.partner.models import BasketLinePostProcessingOption


class ProjectItemPostProcessingUserPanelSerializer(ModelSerializer):
    name = CharField(source='post_processing.title')
    colorId = SerializerMethodField()

    def get_colorId(self, post_processing_option):
        if post_processing_option.color:
            return post_processing_option.color.id
        return None

    class Meta:
        model = BasketLinePostProcessingOption
        fields = ('name', 'colorId')


class ProjectItemUserPanelSerializer(ModelSerializer):
    thumbnailUrl = CharField(source='thumbnail_url')
    materialName = SerializerMethodField()
    postProcessings = ProjectItemPostProcessingUserPanelSerializer(
        many=True, source='post_processing_options'
    )

    def get_materialName(self, line):
        material = line.stockrecord.product
        return {
            'en': material.title_en,
            'de': material.title_de
        }

    class Meta:
        model = Line
        fields = (
            'id',
            'quantity',
            'name',
            'thumbnailUrl',
            'materialName',
            'postProcessings'
        )


class ProjectUserPanelSerializer(ModelSerializer):
    lines = ProjectItemUserPanelSerializer(many=True)
    number = CharField(source='quotation_number')

    class Meta:
        model = Basket
        fields = ('id', 'lines', 'number')
