from rest_framework import serializers

from apps.b3_checkout.models import PartnerPaymentMethod, Payment


class PartnerPaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerPaymentMethod
        fields = (
            'name',
        )


class PaymentSerializer(serializers.ModelSerializer):
    payment_method = PartnerPaymentMethodSerializer()

    class Meta:
        model = Payment
        fields = (
            'id',
            'payment_method',
            'currency',
            'amount',
            'date_time',
        )
