from drf_payload_customizer.mixins import PayloadConverterMixin
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField, ListField

from apps.partner.models import Partner


class ServiceUserPanelSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    phone_number = serializers.CharField(source='address.phone_number')
    country = serializers.CharField(source='address.country')
    city = serializers.CharField(source='address.city')
    disclaimer = SerializerMethodField()
    terms_and_conditions = SerializerMethodField()
    supported_currencies = ListField(read_only=True)

    def get_disclaimer(self, service):
        information_en = service.bluebox_preview_step_en
        information_de = service.bluebox_preview_step_de
        return {
            'en': information_en,
            'de': information_de
        }

    def get_terms_and_conditions(self, service):
        condition_en = service.terms_conditions_en
        condition_de = service.terms_conditions_de
        return {
            'en': condition_en,
            'de': condition_de
        }

    class Meta:
        model = Partner
        fields = (
            'id',
            'city',
            'country',
            'description',
            'email',
            'logo',
            'name',
            'phone_number',
            'website',
            'tax_type',
            'terms_and_conditions',
            'disclaimer',
            'supported_currencies'
        )
