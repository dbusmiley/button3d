import typing as t
import itertools
import logging

import button3d.type_declarations as td
from apps.b3_checkout.validators import errors
from apps.b3_checkout.validators.base_checkout_validator \
    import AbstractCheckoutValidator

logger = logging.getLogger(__name__)


class ProjectCheckoutValidator(AbstractCheckoutValidator):
    def __init__(self, project: td.Project):
        super().__init__()
        self.project = project

    @property
    def validation_functions(self) -> t.Union[t.Iterator[t.Callable],
                                              t.Sequence[t.Callable]]:
        _validation_functions = (
            self.check_project_is_valid,
            self.check_project_can_be_priced,
        )
        return itertools.chain(
            super().validation_functions,
            _validation_functions,
        )

    def check_project_is_valid(self) -> None:
        if self.project.is_deleted:
            msg = 'Project has been deleted.'
            logger.warning(msg)
            raise errors.OrderValidationError(
                errors.ERROR_PROJECT_DELETED,
                msg)

        if not self.project.all_lines():
            msg = 'Project has no lines.'
            logger.warning(msg)
            raise errors.OrderValidationError(
                errors.ERROR_NO_PARTS,
                msg
            )

        if self.project.is_submitted:
            msg = 'Project has already been ordered.'
            logger.warning(msg)
            raise errors.OrderValidationError(
                errors.ERROR_PROJECT_ALREADY_ORDERED,
                msg
            )

        currency = self.project.currency
        service = self.project.partner

        self._validate_service(service)

        for part in self.project.all_lines():
            if part.price_currency != currency:
                raise errors.OrderValidationError(
                    errors.ERROR_MULTIPLE_CURRENCIES,
                    'Parts have different currencies.')

            self._validate_stock_record(part.stockrecord, service)
            self._validate_stl_file(part.stl_file)

            for option in part.post_processing_options.all():
                self._validate_post_processing(
                    option.post_processing,
                    option.color,
                    service
                )

    def check_project_can_be_priced(self) -> None:
        if self.project.is_no_pricing_possible:
            raise errors.OrderValidationError(
                errors.ERROR_NO_PRICING_POSSIBLE,
                'Order cannot be priced.')

        if self.project.is_manual_pricing_required:
            if not self.project.are_all_lines_priced:
                raise errors.OrderValidationError(
                    errors.ERROR_MANUAL_PRICING_REQUIRED,
                    'Order needs manual pricing.')

            if not self.project.is_manually_priced:
                raise errors.OrderValidationError(
                    errors.ERROR_MANUAL_PRICING_REQUIRED,
                    'Order needs manual pricing.')
