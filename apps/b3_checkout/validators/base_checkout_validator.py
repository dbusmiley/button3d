import logging
import typing as t

from button3d import type_declarations as td

from apps.b3_checkout.validators import errors
from apps.b3_checkout.validators.errors import OrderValidationError
from apps.b3_voucher.models import Voucher

logger = logging.getLogger(__name__)


class AbstractCheckoutValidator:
    def __init__(self):
        self._errors: t.List[td.TwoStrTuple] = []
        self._validation_done = False

    @property
    def validation_functions(self) -> t.Union[t.Iterator[t.Callable],
                                              t.Sequence[t.Callable]]:
        """
        Override in subclasses
        """
        return ()

    def is_valid(self) -> bool:
        if not self._validation_done:
            for validation_function in self.validation_functions:
                try:
                    validation_function()
                except OrderValidationError as exc:
                    logger.warning(exc, exc_info=exc, stack_info=True)
                    code, message = exc.args
                    self._errors.append((code, message))
            self._validation_done = True

        return not self._errors

    @property
    def errors(self) -> t.Sequence[td.StringKeyDict]:
        return [errors.error_details_to_dict(code, message)
                for code, message
                in self._errors]

    @staticmethod
    def _validate_service(service: td.Partner) -> None:
        if service is None:
            msg = 'No service has been selected.'
            raise OrderValidationError(
                errors.ERROR_NO_SERVICE,
                msg
            )
        if service.is_deleted:
            msg = f'The selected service has been deleted.'
            raise OrderValidationError(
                errors.ERROR_SERVICE_DELETED,
                msg
            )

    @staticmethod
    def _validate_stl_file(stl_file: td.StlFile) -> None:
        if stl_file is None:
            msg = 'Not all parts have a stl file assigned.'
            raise OrderValidationError(
                errors.ERROR_NO_STL_FILE,
                msg
            )
        if stl_file.is_deleted:
            msg = 'The stl file assigned to some part has been deleted.'
            raise OrderValidationError(
                errors.ERROR_STL_FILE_DELETED,
                msg
            )

        if not stl_file.all_jobs_finished:
            msg = 'The stl file assigned to some part has not been fully '\
                  'analyzed yet.'

            raise OrderValidationError(
                errors.ERROR_STL_FILE_ANALYZE_PENDING,
                msg
            )

        if stl_file.parameter is None:
            msg = 'The parameter of an stl file assigned to some part ' \
                  'is missing.'
            raise OrderValidationError(
                errors.ERROR_STL_FILE_NO_PARAMETER,
                msg
            )

    @staticmethod
    def _validate_stock_record(
            stock_record: t.Optional[td.StockRecord],
            service: t.Optional[td.Partner]
    ) -> None:
        if stock_record is None:
            msg = 'Not all parts have a stock record assigned.'
            raise OrderValidationError(
                errors.ERROR_NO_STOCK_RECORD,
                msg
            )

        if stock_record.is_deleted or not stock_record.is_published:
            msg = 'The stock record assigned to some part has been deleted.'
            raise OrderValidationError(
                errors.ERROR_STOCK_RECORD_DELETED,
                msg
            )

        if stock_record.partner != service:
            msg = 'The stock record assigned to some part does not belong ' \
                  'to the selected service.'
            raise OrderValidationError(
                errors.ERROR_STOCK_RECORD_WRONG_SERVICE,
                msg
            )

    @staticmethod
    def _validate_post_processing(
            post_processing: td.PostProcessing,
            color: t.Optional[td.Color],
            service: td.Partner,
    ) -> None:
        if post_processing.is_deleted or not post_processing.published:
            msg = 'A post processing assigned to some part has ' \
                  'been deleted.'
            raise OrderValidationError(
                errors.ERROR_POST_PROCESSING_DELETED,
                msg
            )

        if post_processing.partner != service:
            msg = 'The post processing assigned to some part does not ' \
                  'belong to the selected service.'
            raise OrderValidationError(
                errors.ERROR_POST_PROCESSING_WRONG_SERVICE,
                msg
            )

        allowed_colors = post_processing.colors.all()
        if color and color not in allowed_colors:
            msg = 'A post processing assigned to some part has invalid ' \
                  'color assigned.'
            raise OrderValidationError(
                errors.ERROR_POST_PROCESSING_INVALID_COLOR,
                msg
            )

    @staticmethod
    def _validate_pickup_location(
            pickup_location: td.PickupLocation,
            service: td.Partner,
    ) -> None:
        if pickup_location.is_deleted:
            msg = 'The selected pickup location has been deleted.'
            raise OrderValidationError(
                errors.ERROR_PICKUP_DELETED,
                msg
            )

        if pickup_location.partner != service:
            msg = 'The selected pickup location does not belong to the ' \
                  'selected service.'
            raise OrderValidationError(
                errors.ERROR_PICKUP_WRONG_SERVICE,
                msg
            )

    @staticmethod
    def _validate_shipping(
            shipping_method: td.ShippingMethod,
            shipping_address: t.Optional[td.Address],
            service: t.Optional[td.Partner],
            user: td.User
    ) -> None:
        if shipping_method.is_deleted:
            msg = 'The selected shipping method has been deleted.'
            raise OrderValidationError(
                errors.ERROR_SHIPPING_METHOD_DELETED,
                msg
            )

        elif shipping_method.partner != service:
            msg = 'The selected shipping method does not belong to the ' \
                  'selected service.'
            raise OrderValidationError(
                errors.ERROR_SHIPPING_METHOD_WRONG_SERVICE,
                msg
            )

        if shipping_address is None:
            msg = 'No shipping address has been selected.'
            raise OrderValidationError(
                errors.ERROR_NO_SHIPPING_ADDRESS,
                msg
            )

        elif shipping_address.user != user:
            msg = 'The selected address can not be used as a shipping ' \
                  'address.'
            raise OrderValidationError(
                errors.ERROR_SHIPPING_ADDRESS_INVALID,
                msg
            )

        if shipping_address.country not in shipping_method.countries.all():
            msg = 'The selected shipping method does not ship to the ' \
                  'shipping address.'
            raise OrderValidationError(
                errors.ERROR_SHIPPING_METHOD_WRONG_COUNTRY,
                msg
            )

    @staticmethod
    def _validate_billing_address(
            payment_method: t.Optional[td.PartnerPaymentMethod],
            billing_address: t.Optional[td.Address],
            user: td.User,
            org: td.Organization
    ) -> None:
        if payment_method.is_billing_address_required:
            if billing_address is None:
                msg = 'No billing address has been selected.'
                raise OrderValidationError(
                    errors.ERROR_NO_BILLING_ADDRESS,
                    msg
                )

            if billing_address.user != user:
                msg = 'The selected address can not be used as a billing ' \
                      'address.'
                raise OrderValidationError(
                    errors.ERROR_BILLING_ADDRESS_INVALID,
                    msg
                )

        if org.company_information_visibility == org.REQUIRED and \
                not (billing_address.company_name and billing_address.vat_id):
            msg = 'Company name and VAT ID are required'
            raise OrderValidationError(
                errors.ERROR_BILLING_ADDRESS_INVALID,
                msg
            )

    @staticmethod
    def _validate_voucher(
            voucher_code: str,
            service: t.Optional[td.Partner],
            user: td.User
    ) -> t.Optional[Voucher]:
        try:
            voucher = Voucher.objects.get(code=voucher_code, partner=service)
        except Voucher.DoesNotExist:
            msg = 'The selected voucher code not found'
            raise OrderValidationError(
                errors.ERROR_VOUCHER_CODE_INVALID,
                msg
            )

        if not voucher.is_applicable(user):
            msg = 'The selected voucher code is not valid for the ' \
                  'selected service.'
            raise OrderValidationError(
                errors.ERROR_VOUCHER_CODE_INVALID,
                msg
            )
        return voucher

    @staticmethod
    def _validate_payment_method(
            payment_method: t.Optional[td.PartnerPaymentMethod],
            payment_arguments: t.Optional[td.StrKeyValueDict],
            service: t.Optional[td.Partner]
    ) -> None:
        if payment_method is None:
            msg = 'No payment method has been selected.'
            raise OrderValidationError(
                errors.ERROR_NO_PAYMENT_METHOD,
                msg
            )

        if payment_method.is_deleted:
            msg = 'The selected payment method has been deleted.'
            raise OrderValidationError(
                errors.ERROR_PAYMENT_METHOD_DELETED,
                msg
            )

        if payment_method.partner != service:
            msg = 'The selected payment method does not belong to the ' \
                  'selected service'
            raise OrderValidationError(
                errors.ERROR_PAYMENT_METHOD_WRONG_SERVICE,
                msg
            )

        arguments_serializer = payment_method.arguments_serializer_class(
            data=payment_arguments
        )
        if payment_arguments is None or not arguments_serializer.is_valid():
            logger.warning(f'Payment method config is wrong:'
                           f' {arguments_serializer.errors}')
            raise OrderValidationError(
                errors.WRONG_PAYMENT_ARGUMENTS,
                f'Payment method config is wrong.')
