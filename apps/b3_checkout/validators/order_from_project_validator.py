import itertools
import logging
import typing as t
from button3d import type_declarations as td

from apps.b3_checkout.validators import errors
from apps.b3_checkout.validators.project_checkout_validator \
    import ProjectCheckoutValidator
from apps.partner.pricing.calculators.base import get_is_b2b_transaction, \
    get_billing_and_shipping_country
from apps.partner.pricing.calculators.project import ProjectPriceCalculator

logger = logging.getLogger(__name__)


class OrderFromProjectValidator(ProjectCheckoutValidator):
    def __init__(
        self,
        project: td.Project,
        user: td.User,
        organization: td.Organization,
        pickup_location: t.Optional[td.PickupLocation],
        shipping_method: t.Optional[td.ShippingMethod],
        shipping_address: t.Optional[td.Address],
        billing_address: t.Optional[td.Address],
        payment_method: t.Optional[td.PartnerPaymentMethod],
        payment_arguments: td.StrKeyValueDict,
        currency: str,
        authorized_amount: t.Optional[td.Decimal] = None,
        voucher_code: t.Optional[str] = None
    ):
        super().__init__(project)
        self.user = user
        self.organization = organization
        self.pickup_location = pickup_location
        self.shipping_method = shipping_method
        self.shipping_address = shipping_address
        self.billing_address = billing_address
        self.payment_method = payment_method
        self.payment_arguments = payment_arguments
        self.authorized_amount = authorized_amount
        self.currency = currency
        self.voucher_code = voucher_code
        self.voucher: t.Optional[td.Voucher] = None
        self.price_calculator: t.Optional[td.ProjectPriceCalculator] = None
        self.service: td.Partner = self.project.partner

    @property
    def validation_functions(self) -> t.Union[t.Iterator[t.Callable],
                                              t.Sequence[t.Callable]]:
        _validation_functions = (
            self.check_delivery_is_valid,
            self.check_payment_is_valid,
            self.check_currency_is_supported,
            self.check_project_price_is_correct,
        )

        return itertools.chain(
            super().validation_functions,
            _validation_functions,
        )

    def check_delivery_is_valid(self) -> None:
        both_shipping_and_pickup = self.shipping_method is not None \
            and self.pickup_location is not None
        neither_shipping_nor_pickup = not self.shipping_method \
            and not self.pickup_location

        if both_shipping_and_pickup:
            msg = 'Both shipping method and pickup location are specified'
            logger.warning(msg)
            raise errors.OrderValidationError(
                errors.ERROR_EITHER_SHIPPING_OR_PICKUP,
                msg
            )

        if neither_shipping_nor_pickup:
            msg = 'Neither shipping method or a pickup location ' \
                  'are specified'
            logger.warning(msg)
            raise errors.OrderValidationError(
                errors.ERROR_EITHER_SHIPPING_OR_PICKUP,
                msg
            )

        if self.pickup_location:
            self._validate_pickup_location(
                self.pickup_location,
                self.service,
            )
        elif self.shipping_method:
            self._validate_shipping(
                self.shipping_method,
                self.shipping_address,
                self.service,
                self.user
            )

    def check_payment_is_valid(self) -> None:
        self._validate_payment_method(
            self.payment_method,
            self.payment_arguments,
            self.service
        )

        self._validate_billing_address(
            self.payment_method,
            self.billing_address,
            self.user,
            self.organization
        )

        if self.voucher_code:
            self.voucher = self._validate_voucher(
                self.voucher_code,
                self.service,
                self.user
            )

    def check_currency_is_supported(self):
        if self.currency not in self.service.supported_currencies:
            raise errors.OrderValidationError(
                errors.ERROR_PAYMENT_WRONG_PRICE,
                f'The selected currency {self.currency} is not supported by '
                f'the service.'
            )

    def check_project_price_is_correct(self) -> None:
        """
        This validation function must be the last in execution chain, because
        this check is only done if all previous checks were successful, since
        the order must be valid to allow a proper price calculation.
        """
        if self.errors:
            return

        is_b2b_transaction = get_is_b2b_transaction(
            self.billing_address
        )
        shipping_country, billing_country = get_billing_and_shipping_country(
            self.billing_address,
            self.shipping_address
        )
        price_calculator = ProjectPriceCalculator(
            project=self.project,
            currency=self.currency,
            shipping_method=self.shipping_method,
            voucher=self.voucher,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )
        if self.authorized_amount is not None:
            if price_calculator.total.incl_tax != self.authorized_amount:
                logger.warning(
                    f'PAYMENT_WRONG_PRICE: '
                    f'total.incl_tax: {price_calculator.total.incl_tax} '
                    f'authorized_amount: {self.authorized_amount} '
                )
                raise errors.OrderValidationError(
                    errors.ERROR_PAYMENT_WRONG_PRICE,
                    f'The total price {price_calculator.total.incl_tax} '
                    f'is not equal to the amount authorized'
                    f' {self.authorized_amount} for payment.'
                )
        self.price_calculator = price_calculator
