class PaymentError(Exception):
    pass


class PaymentGatewayError(PaymentError):
    """
    Raised if problems with an external gateway occur (e.g. stripe, nets)
    """


class PaymentMethodWrongConfigError(PaymentError):
    """
    Raised when an instance of PartnerPaymentMethod is incorrectly configured,
    such that the config arguments can not be used to instantiate the
    PaymentMethod object
    """


class InvalidPaymentArgumentsError(PaymentError):
    """
    Raised when the arguments submitted with a payment are not valid.
    e.detail holds a dictionary that explains the error
    """

    def __init__(self, detail=None):
        self.detail = detail


class PaymentNotAuthorizedError(PaymentError):
    """
    Raised when the payment was unsuccessful because the payment provider
    refused the allocation.
    """
