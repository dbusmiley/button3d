from django.shortcuts import render


def payment_redirect_target_view(request):
    """
    NETS Payment callback redirection target.
    Used as the callback URL for NETS
    """
    # This is in place to stop the redirect sending back a new sessionid
    # to the calling django session
    request.session = {}
    return render(request, 'b3_checkout/payment-redirect-target.html')
