from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.partner.models import Partner


class BaseUserPanelServiceView(APIView):
    permission_classes = (IsAuthenticated, )
    queryset = Partner.objects.all()

    def get_service(self):
        try:
            return Partner.objects.get(id=self.kwargs['service_id'])
        except Partner.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='PARTNER', object_id=self.kwargs['service_id']
                )
            )

    def get_object(self):
        return self.get_service()
