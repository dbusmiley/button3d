from rest_framework.generics import RetrieveAPIView
from apps.b3_checkout.serializers.projects import ProjectUserPanelSerializer
from apps.b3_checkout.views.projects.base_project_user_panel \
    import ProjectUserPanelBaseView


class ProjectDetailUserPanelView(ProjectUserPanelBaseView, RetrieveAPIView):
    serializer_class = ProjectUserPanelSerializer
