import typing as t

from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

import button3d.type_declarations as td
from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.errors.custom_exceptions import ErrorResponse
from apps.b3_checkout.validators.project_checkout_validator \
    import ProjectCheckoutValidator
from apps.b3_core.utils import cache_result_on_instance
from apps.basket.models import Basket


class ProjectUserPanelBaseView(APIView):
    permission_classes = (IsAuthenticated,)

    @property
    @cache_result_on_instance
    def project(self) -> Basket:
        project_id = self.kwargs['project_id']
        try:
            project = Basket.objects.get(
                id=project_id,
            )
        except Basket.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='BASKET',
                    object_id=project_id
                )
            )
        if not project.can_user_view(
                user=self.request.user,
                request=self.request,
        ):
            raise Http404(
                error_object_not_found(
                    object_name='BASKET',
                    object_id=project_id
                )
            )

        self._validate_project(project)
        return project

    def get_object(self) -> Basket:
        return self.project

    def _validate_project(self, project: Basket) -> None:
        validator = ProjectCheckoutValidator(project)
        if not validator.is_valid():
            self._raise_error_response(project, validator.errors)

    # noinspection PyMethodMayBeStatic
    def _raise_error_response(
            self,
            project: Basket,
            errors: t.Union[td.StringKeyDict, t.Sequence[td.StringKeyDict]],
    ) -> None:
        if not isinstance(errors, (list, tuple)):
            errors = [errors]

        raise ErrorResponse(
            code='PROJECT_CAN_NOT_BE_ORDERED',
            message=f'This project (id={project.id}) can not be ordered.',
            more_info={
                'errors': errors
            }
        )
