import logging

from django.db import transaction
from django.utils.translation import get_language_from_request
from rest_framework.exceptions import ValidationError
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED

import button3d.type_declarations as td
from apps.b3_checkout.order_placement.order_builder import \
    OrderFromProjectBuilder
from apps.b3_checkout.serializers.projects.order import \
    OrderFromProjectSerializer
from apps.b3_checkout.validators import errors
from apps.b3_checkout.views.projects.base_project_user_panel \
    import ProjectUserPanelBaseView
from apps.b3_core.utils import get_client_ip

logger = logging.getLogger(__name__)


class CreateOrderFromProjectView(ProjectUserPanelBaseView, CreateAPIView):
    serializer_class = OrderFromProjectSerializer

    def create(self, request, *args, **kwargs):
        serializer: OrderFromProjectSerializer = self.get_serializer(
            data=request.data
        )

        # validate
        try:
            serializer.is_valid(raise_exception=True)
        except (errors.OrderValidationError, ValidationError) \
                as order_placement_exc:
            self._raise_error_response(
                self.project,
                order_placement_exc.args[0]
            )

        # make order
        try:
            order = self.place_order(serializer.validated_data)
        except errors.OrderPlacementError as order_placement_exc:
            code, message = order_placement_exc.args
            return self._raise_error_response(
                self.project,
                errors.error_details_to_dict(code, message)
            )

        except Exception as other_exc:
            logger.error(other_exc, stack_info=True, exc_info=other_exc)
            return self._raise_error_response(
                self.project,
                errors.error_details_to_dict(
                    errors.ERROR_ORDER_PLACEMENT_FAILED,
                    'The order could not be placed. Please try again later.'
                )
            )

        return Response({'id': order.id}, status=HTTP_201_CREATED)

    def place_order(self, order_data: td.StrKeyDict) -> td.Order:
        customer_language = get_language_from_request(self.request)
        customer_ip = get_client_ip(self.request)

        order_builder = OrderFromProjectBuilder(
            customer_language=customer_language,
            customer_ip=customer_ip,
            **order_data
        )

        with transaction.atomic():
            order_builder.create_order_object()
            order_builder.retrieve_payment()
            self.project.submit()

        try:
            order_builder.emails.send()
        except Exception as exc:
            logger.warning(
                'order_builder.emails.send() failed',
                stack_info=True,
                exc_info=exc,
            )

        try:
            order_builder.webhook.send_webhook()
        except Exception as exc:
            logger.warning(
                'order_builder.webhook.send_webhook() failed',
                stack_info=True,
                exc_info=exc,
            )

        return order_builder.order

    def _validate_project(self, project: td.Basket) -> None:
        # Project is validated through the serializer, so we can skip
        # validation here.
        pass
