import typing as t

from django.http import Http404
from rest_framework.exceptions import ValidationError
from rest_framework.generics import RetrieveAPIView

import button3d.type_declarations as td
from apps.b3_address.models import Address
from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_checkout.serializers.projects import \
    ProjectPriceUserPanelSerializer
from apps.b3_checkout.views.projects.base_project_user_panel \
    import ProjectUserPanelBaseView
from apps.b3_core.utils import cache_result_on_instance
from apps.b3_shipping.models import ShippingMethod
from apps.b3_voucher.models import Voucher
from apps.partner.pricing.calculators.base import get_is_b2b_transaction, \
    get_billing_and_shipping_country
from apps.partner.pricing.calculators.project import ProjectPriceCalculator


class ProjectPriceUserPanelView(ProjectUserPanelBaseView, RetrieveAPIView):
    serializer_class = ProjectPriceUserPanelSerializer

    def get_object(self) -> ProjectPriceCalculator:
        is_b2b_transaction = get_is_b2b_transaction(
            billing_address=self.billing_address
        )
        shipping_country, billing_country = get_billing_and_shipping_country(
            billing_address=self.billing_address,
            shipping_address=self.shipping_address
        )

        return ProjectPriceCalculator(
            project=self.project,
            shipping_method=self.shipping_method,
            voucher=self.voucher,
            currency=self.currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

    @property
    def currency(self) -> t.Optional[str]:
        return self.request.query_params.get('currency', None)

    @property
    def voucher(self) -> t.Optional[td.Voucher]:
        voucher_code = self.request.query_params.get('voucherCode', '')
        voucher_code_sanitized = voucher_code.upper().strip()
        if not voucher_code_sanitized:
            return None

        try:
            voucher = Voucher.objects.get(
                partner=self.project.partner,
                code=voucher_code_sanitized
            )
        except Voucher.DoesNotExist:
            raise ValidationError(
                {'voucherCode': 'The voucher does not exist.'}
            )

        if not voucher.is_applicable(self.request.user):
            raise ValidationError(
                {'voucherCode': 'The voucher is not valid.'}
            )
        return voucher

    @property
    def shipping_method(self) -> t.Optional[td.ShippingMethod]:
        shipping_method_id = self.request.query_params.get('shippingMethodId')
        if not shipping_method_id:
            return None

        try:
            return ShippingMethod.objects.get(
                id=shipping_method_id,
                partner=self.project.partner
            )
        except ShippingMethod.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='SHIPPING_METHOD',
                    object_id=shipping_method_id
                )
            )

    @property
    @cache_result_on_instance
    def shipping_address(self) -> t.Optional[td.Address]:
        query_params = self.request.query_params
        try:
            shipping_address_id = query_params['shippingAddressId']
        except KeyError:
            return None

        try:
            return Address.objects.get(id=shipping_address_id)
        except Address.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='ADDRESS',
                    object_id=shipping_address_id
                )
            )

    @property
    @cache_result_on_instance
    def billing_address(self) -> t.Optional[td.Address]:
        query_params = self.request.query_params
        try:
            billing_address_id = query_params['billingAddressId']
        except KeyError:
            return None

        try:
            return Address.objects.get(id=billing_address_id)
        except Address.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='ADDRESS',
                    object_id=billing_address_id
                )
            )
