from rest_framework.response import Response

from apps.b3_api.services.views import BaseServiceView


class PaymentMethodTypes(BaseServiceView):
    """
    Returns list of payment method types, available for creation by selected
    `partner`
    """

    def get(self, *args, **kwargs) -> Response:
        partner = self.get_object()
        return Response(
            partner.get_available_for_creation_payment_method_types()
        )
