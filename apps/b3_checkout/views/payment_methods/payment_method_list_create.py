import typing as t

from django.db.models import QuerySet
from rest_framework import generics

from apps.b3_api.services.views import BaseServiceView
from apps.b3_checkout.serializers.payment_methods \
    import CreatePartnerPaymentMethodSerializer
from apps.b3_checkout.models import PartnerPaymentMethod


class PaymentMethodListCreate(BaseServiceView, generics.ListCreateAPIView):
    serializer_class = CreatePartnerPaymentMethodSerializer

    def get_queryset(self) -> t.Type[QuerySet]:
        """
        Filters PartnerPaymentMethod by current `partner` instance
        :return:
        """
        partner = self.get_object()
        return PartnerPaymentMethod.objects.filter(partner=partner)

    def get_serializer_context(self):
        """
        Passes `partner` to serializer's context, where it is used for
        validation
        :return:
        """
        ctx = super().get_serializer_context()
        ctx['partner'] = self.get_object()
        return ctx

    def perform_create(
            self,
            serializer: CreatePartnerPaymentMethodSerializer
    ) -> None:
        """
        Before saving new Partner Payment Method, sets its `partner` property
        :param serializer:
        :return:
        """
        serializer.save(partner=self.get_object())
