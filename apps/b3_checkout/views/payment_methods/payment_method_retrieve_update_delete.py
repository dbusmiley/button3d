from rest_framework import generics
from rest_framework.exceptions import ValidationError

import button3d.type_declarations as td
from apps.b3_api.services.views import BaseServiceView
from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_checkout.serializers.payment_methods \
    import UpdatePartnerPaymentMethodSerializer


class PaymentMethodRetrieveUpdateDelete(
        BaseServiceView, generics.RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'payment_method_id'
    serializer_class = UpdatePartnerPaymentMethodSerializer

    def get_serializer_context(self):
        """
        Passes `partner` to serializer's context, where it is used for
        validation
        :return:
        """
        ctx = super().get_serializer_context()
        ctx['partner'] = super().get_object()
        return ctx

    def get_object(self) -> td.PartnerPaymentMethod:
        """
        Looks up PartnerPaymentMethod not only by its PK, but by `partner`,
        that makes access control possible
        :return:
        """
        partner = super().get_object()
        payment_method = generics.get_object_or_404(
            PartnerPaymentMethod,
            partner=partner,
            id=self.kwargs['payment_method_id'])
        return payment_method

    def perform_destroy(self, instance: PartnerPaymentMethod):
        """
        Before deleting checks if this payment method is not the last enabled
        """

        if not instance.partner.has_other_enabled_payment_method_left_except(
                instance):
            raise ValidationError(
                {'__all__': 'At least one payment method must exist'}
            )

        super().perform_destroy(instance)
