import requests
from django.conf import settings
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from apps.b3_api.services.views import BaseServiceView
from apps.b3_checkout.constants import PAYMENT_METHODS


class StripeAccountAlreadyConnected(Exception):
    pass


class ConnectStripeAccount(BaseServiceView):
    def post(self, request, *args, **kwargs):
        try:
            self.validate_parameters()
            self.connect_account()
        except StripeAccountAlreadyConnected:
            return Response({
                'code': 'STRIPE_ALREADY_CONNECTED',
                'message': ('This service already has a stripe account '
                            'connected. Please disconnect the account before '
                            'connecting a new one.')
            }, status=status.HTTP_400_BAD_REQUEST)
        except ValueError as e:
            return Response({
                'code': 'STRIPE_CONNECTION_FAILED',
                'message': str(e)
            }, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_204_NO_CONTENT)

    def validate_parameters(self):
        if not self.request.data.get('code', None):
            raise ValidationError({'code': 'This field is required.'})

        partner = self.get_object()

        available_payment_method_types = partner.\
            get_available_for_creation_payment_method_types()
        if PAYMENT_METHODS.STRIPE not in available_payment_method_types:
            raise StripeAccountAlreadyConnected

    def connect_account(self):
        connect_params = {
            'client_secret': settings.STRIPE_SECRET_KEY,
            'grant_type': 'authorization_code',
            'client_id': settings.STRIPE_CLIENT_ID,
            'code': self.request.data['code']
        }
        connect_url = 'https://connect.stripe.com/oauth/token'
        connect_response = requests.post(connect_url, params=connect_params)
        if connect_response.status_code != 200:
            raise ValueError(connect_response.json()['error_description'])

        partner = self.get_object()
        token = connect_response.json()

        partner.payment_methods.create(
            type_name=PAYMENT_METHODS.STRIPE,
            public_config_arguments={
                'stripe_user_id': token['stripe_user_id'],
                'stripe_publishable_key': token['stripe_publishable_key'],
            },
            private_config_arguments={
                'access_token': token['access_token'],
                'livemode': token['livemode'],
                'refresh_token': token['refresh_token'],
                'token_type': token['token_type'],
                'scope': token['scope']
            }
        )
