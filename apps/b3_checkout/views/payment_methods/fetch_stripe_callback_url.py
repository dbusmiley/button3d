from django.conf import settings
from django.utils.http import urlencode
from rest_framework.response import Response

from apps.b3_api.services.views import BaseServiceView


class FetchStripeCallbackURL(BaseServiceView):
    def get(self, request, *args, **kwargs):
        return Response({"callbackUrl": self.generate_stripe_url()})

    def generate_stripe_url(self):
        """
        Generate stripe callback url for the given request domain
        """

        site = 'https://connect.stripe.com/oauth/authorize'
        params = {
            'response_type': 'code',
            'scope': 'read_write',
            'client_id': settings.STRIPE_CLIENT_ID,
            'state': 'stripe-connected',
            'redirect_uri': self.request.build_absolute_uri(
                settings.STRIPE_REDIRECT_PATH
            )
        }
        return f'{site}?{urlencode(params)}'
