import inflection
from rest_framework import status
from rest_framework.fields import FileField
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.serializers import Serializer

from apps.b3_checkout.models import PartnerPaymentMethod, PaymentAttachment
from apps.b3_checkout.serializers.initialize_payment import \
    PaymentInitializationSerializer
from apps.b3_checkout.serializers.payment_methods import \
    PaymentMethodUserPanelSerializer
from apps.b3_checkout.views.base_service_user_panel \
    import BaseUserPanelServiceView


class PaymentMethodUserPanelList(BaseUserPanelServiceView, ListAPIView):
    serializer_class = PaymentMethodUserPanelSerializer

    def get_queryset(self):
        return PartnerPaymentMethod.objects.filter(
            partner=self.get_service(),
            is_enabled=True
        )


class InitializePaymentView(BaseUserPanelServiceView, CreateAPIView):
    serializer_class = PaymentInitializationSerializer

    def get_serializer_context(self):
        return {'service': self.get_service()}

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        partner_payment_method = serializer.validated_data['method_id']
        price = serializer.validated_data['price']
        result = partner_payment_method.payment_method.initialize_payment(
            price, self.request.user
        )

        return Response(
            {
                'result': {
                    inflection.camelize(key, uppercase_first_letter=False):
                        value
                    for key, value in result.items()
                }
            },
            status=status.HTTP_201_CREATED
        )


class AttachmentUploadSerializer(Serializer):
    file = FileField()


class PaymentAttachmentUserPanelView(BaseUserPanelServiceView, CreateAPIView):
    parser_classes = (MultiPartParser, FormParser, )
    serializer_class = AttachmentUploadSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        attachment = self.create_payment_attachment(
            serializer.validated_data['file']
        )
        return Response(
            {'id': attachment.id},
            status=status.HTTP_201_CREATED,
        )

    def create_payment_attachment(self, uploaded_file):
        return PaymentAttachment.objects.create(
            uploader=self.request.user,
            file=uploaded_file,
            filename=uploaded_file.name,
            filesize=uploaded_file.size
        )
