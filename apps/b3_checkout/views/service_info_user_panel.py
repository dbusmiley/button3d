from rest_framework.generics import RetrieveAPIView

from apps.b3_checkout.serializers.service import ServiceUserPanelSerializer
from apps.b3_checkout.views.base_service_user_panel \
    import BaseUserPanelServiceView


class UserPanelServiceInfoView(BaseUserPanelServiceView, RetrieveAPIView):
    serializer_class = ServiceUserPanelSerializer
