from django.conf import settings
from django.db import models

from apps.b3_attachement.models import AbstractAttachment


class PaymentAttachment(AbstractAttachment):
    """
    An Attachment which belongs to a payment
    """
    ATTACHMENT_FOLDER = 'checkout_payment_attachments'

    payment = models.ForeignKey(
        'b3_checkout.Payment',
        blank=True,
        null=True,
        related_name='payment_attachments'
    )
    uploader = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='payment_attachments',
    )
