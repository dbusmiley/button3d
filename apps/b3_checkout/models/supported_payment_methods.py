from django.db import models

from apps.b3_checkout.constants import PAYMENT_METHOD_CHOICES


class SupportedPaymentMethod(models.Model):
    """
    Payment Method, explicitly enabled by administrator for `partner`
    """
    partner = models.ForeignKey(
        'partner.Partner',
        on_delete=models.CASCADE,
        related_name='supported_payment_methods',
    )
    type_name = models.CharField(
        max_length=100,
        choices=PAYMENT_METHOD_CHOICES
    )

    def __str__(self) -> str:
        return f'{self.partner}: {self.type_name}'
