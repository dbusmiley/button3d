from decimal import Decimal

from django.conf import settings
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField


class Payment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    order = models.OneToOneField('b3_order.Order', related_name='payment')
    payment_method = models.ForeignKey('b3_checkout.PartnerPaymentMethod')

    currency = models.CharField(
        _('Currency'), max_length=12, default=settings.DEFAULT_CURRENCY)
    amount = models.DecimalField(
        _('Amount'), decimal_places=2, max_digits=12,
        default=Decimal('0.00'))

    date_time = models.DateTimeField(default=now, editable=False)
    metadata = JSONField(default={}, editable=False)

    def __str__(self):
        return f'{self.payment_method.type_name}: ' \
            f'{self.amount} {self.currency}'
