from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from rest_framework.exceptions import ValidationError

import button3d.type_declarations as td
from apps.b3_checkout.constants import PAYMENT_METHOD_CHOICES
from apps.b3_checkout.exceptions import PaymentMethodWrongConfigError, \
    InvalidPaymentArgumentsError
from apps.b3_checkout.payment_methods import get_payment_method_class
from apps.b3_checkout.utils import get_next_payment_method_priority_for_partner
from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_migration.sync.payment_method_synchronization import \
    PaymentMethodsNew2OldSync


class PartnerPaymentMethod(AbstractDeletedDateModel):
    partner = models.ForeignKey(
        'partner.Partner',
        on_delete=models.CASCADE,
        related_name='payment_methods',
    )
    type_name = models.CharField(
        max_length=100,
        choices=PAYMENT_METHOD_CHOICES
    )
    # These configuration arguments are visible to the printing service
    # (but not necessarily to the user that wants to use the payment method)
    # and are validated using payment_method.PublicConfigArgumentsSerializer
    public_config_arguments = JSONField(default={}, blank=True)

    # These configuration arguments are only visible to through the
    # admin panel and are not validated.
    private_config_arguments = JSONField(default={}, blank=True)

    is_enabled = models.BooleanField(default=True)
    priority = models.IntegerField(
        help_text=_('Used as key for sorting Payment Methods'),
        db_index=True,
    )

    is_billing_address_required = models.BooleanField(default=True, blank=True)

    class Meta:
        ordering = ('priority',)

    sync_class = PaymentMethodsNew2OldSync()

    def save(self, *args, **kwargs) -> None:
        """
        Before saving, updates `priority` field. Sets value greater by 1 than
        maximum `priority` of `self.partner.payment_methods`
        """
        if self.priority is None:
            self.priority = get_next_payment_method_priority_for_partner(
                self.partner,
                self.__class__
            )

        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.name

    @property
    def name(self) -> str:
        """
        Returns PaymentMethod instance `name`
        """
        return self.payment_method.name

    @property
    def description(self) -> str:
        return self.payment_method.description

    @property
    def payment_method_class(self) -> td.PaymentMethodType:
        """
        Returns PaymentMethod class, corresponding to this instance's type_name
        :return:
        """
        try:
            return get_payment_method_class(self.type_name)
        except KeyError as e:
            raise PaymentMethodWrongConfigError from e

    @property
    def payment_method(self):
        """
        Takes PaymentMethod class, instantiates it with arguments from
        self.public_config_arguments and self.private_config_arguments.
        Returns PaymentMethod instance
        """
        try:
            return self.payment_method_class(
                **self.public_config_arguments,
                **self.private_config_arguments,
            )
        except TypeError as e:
            raise PaymentMethodWrongConfigError from e

    @property
    def arguments_serializer_class(self):
        return self.payment_method.PaymentArgumentsSerializer

    def retrieve_payment(
        self,
        order: td.Order,
        payment_arguments: td.StrKeyDict
    ) -> td.Payment:
        """
        Handles payment with this PaymentMethod behavior class
        Can raise:
            PaymentMethodWrongConfigError
                if the configuration arguments of the payment method are not
                valid and the payment method thus can not be used
            InvalidPaymentArgumentsError
                if the arguments submitted with the payment were not
                valid for the selected payment method
            PaymentNotAuthorizedError
                if the payment could not be authorized (e.g. payment gateway
                rejects the submitted token, credit card expired etc)
            PaymentGatewayError
                if problems with the payment gateway occur
            Exception
                hopefully never :0
        """
        try:
            self.payment_method.PaymentArgumentsSerializer(
                data=payment_arguments
            ).is_valid(raise_exception=True)
        except ValidationError as e:
            raise InvalidPaymentArgumentsError(e.detail)

        return self.payment_method.retrieve_payment(
            order=order,
            partner_payment_method=self,
            payment_arguments=payment_arguments,
        )

    def full_clean(self, *args, **kwargs):
        super().full_clean(*args, **kwargs)
        serializer = self.payment_method_class.PublicConfigArgumentsSerializer(
            data=dict(self.public_config_arguments),
        )
        serializer.is_valid(raise_exception=True)
