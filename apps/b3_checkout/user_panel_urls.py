from django.conf.urls import url, include

from apps.b3_checkout.views.payment_methods import \
    PaymentMethodUserPanelList
from apps.b3_checkout.views.payment_methods.payment_method_user_panel \
    import InitializePaymentView, PaymentAttachmentUserPanelView
from apps.b3_checkout.views.projects.create_order_from_project_user_panel \
    import CreateOrderFromProjectView
from apps.b3_checkout.views.projects.project_detail_user_panel \
    import ProjectDetailUserPanelView
from apps.b3_checkout.views.projects.project_price_user_panel \
    import ProjectPriceUserPanelView
from apps.b3_checkout.views.service_info_user_panel import \
    UserPanelServiceInfoView
from apps.b3_shipping.views.pickup_location_user_panel_list \
    import PickupLocationUserPanelList
from apps.b3_shipping.views.shipping_method_user_panel_list \
    import ShippingMethodUserPanelList
from apps.b3_user_panel.views.order_user_panel_detail \
    import OrderUserPanelDetail
from apps.b3_user_panel.views.order_user_panel_list import OrderUserPanelList
from apps.b3_user_panel.views.service_list_user_panel import \
    ServiceListUserPanel
from apps.b3_user_panel.views.user_details_view import UserDetailView

urlpatterns = [
    url(r'^addresses/', include('apps.b3_address.urls')),
    url(r'^user/$',
        UserDetailView.as_view()
        ),
    url(
        r'^orders/$',
        OrderUserPanelList.as_view()
    ),
    url(
        r'^orders/(?P<order_id>\d+)/$',
        OrderUserPanelDetail.as_view()
    ),
    url(
        r'^projects/(?P<project_id>\d+)/$',
        ProjectDetailUserPanelView.as_view()
    ),
    url(
        r'^projects/(?P<project_id>\d+)/price/$',
        ProjectPriceUserPanelView.as_view()
    ),
    url(
        r'^projects/(?P<project_id>\d+)/orders/$',
        CreateOrderFromProjectView.as_view()
    ),
    url(
        r'^services/$',
        ServiceListUserPanel.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/$',
        UserPanelServiceInfoView.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/pickup-locations/$',
        PickupLocationUserPanelList.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/shipping-methods/$',
        ShippingMethodUserPanelList.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/payment-methods/$',
        PaymentMethodUserPanelList.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/payment/initialize/$',
        InitializePaymentView.as_view()
    ),
    url(
        r'^services/(?P<service_id>\d+)/payment/attachments/$',
        PaymentAttachmentUserPanelView.as_view()
    ),
]
