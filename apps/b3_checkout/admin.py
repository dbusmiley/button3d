from django.contrib import admin

from apps.b3_checkout.models import PartnerPaymentMethod, \
    SupportedPaymentMethod, Payment, PaymentAttachment


@admin.register(PartnerPaymentMethod)
class PartnerPaymentMethodAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'partner', 'is_enabled', 'priority', 'type_name',
    )
    list_filter = ('partner', 'type_name', 'is_enabled')
    raw_id_fields = ('partner',)


@admin.register(SupportedPaymentMethod)
class SupportedPaymentMethodAdmin(admin.ModelAdmin):
    list_display = ('partner', 'type_name')
    raw_id_fields = ('partner',)
    list_filter = ('partner', 'type_name')


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    class PaymentAttachmentInline(admin.TabularInline):
        model = PaymentAttachment
        raw_id_fields = ('uploader',)

    inlines = (PaymentAttachmentInline,)
    raw_id_fields = ('user', 'payment_method')
