from decimal import Decimal
from types import SimpleNamespace
from unittest import mock

from apps.b3_address.factories import AddressFactory
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.models import Payment
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_tests.factories import BasketLineFactory, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class StripePaymentTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.payment_method = PartnerPaymentMethodFactory(
            partner=self.partner,
            type_name=PAYMENT_METHODS.STRIPE,
            private_config_arguments={
                'access_token': 'XXXXXXXXXXXXXX',
                'refresh_token': 'XXXXXXXXXXXXXX',
                'livemode': True,
                'token_type': 'XXXXXXXXXXXXXX',
                'scope': 'XXXXXXXXXXXXXX',
            },
            public_config_arguments={
                'stripe_publishable_key': 'key_12345',
                'stripe_user_id': 'acct_12345'
            }
        )
        AddressFactory(partner=self.payment_method.partner, user=None)

        project = BasketLineFactory(basket__owner=self.user).basket
        self.order = OrderFactory(project=project, generate_payment=NO_PAYMENT)

    @mock.patch('stripe.Charge.create')
    def test_stripe_payment(self, charge_mock):
        charge_mock.return_value = SimpleNamespace(id='123')

        payment = self.payment_method.retrieve_payment(
            order=self.order, payment_arguments={'token': 'XXX'}
        )

        self.assertEqual(Payment.objects.count(), 1)

        self.assertEqual(payment.amount, Decimal('256.04'))
        self.assertEqual(payment.currency, 'EUR')
        self.assertEqual(payment.metadata['charge_id'], '123')
        self.assertEqual(payment.metadata['stripe_account'], 'acct_12345')
        self.assertEqual(payment.payment_method, self.payment_method)
