from unittest import skip

import requests_mock
from django.conf import settings

from django.urls import reverse

from apps.b3_checkout.constants import PAYMENT_METHODS
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.payment_methods.apis.nets_api import NetsApi
from apps.b3_tests.factories import AddressFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory

from apps.b3_tests.factories import BasketFactory, StlFileFactory, \
    PartnerFactory, create_product, \
    create_stockrecord, CountryFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket
from apps.b3_order.models import Order


class NetsApiMock(object):
    base_url = settings.NETS_BASE_URL
    cart_id = '3DXXXXXXX'
    item_id = '3DYYYYYYY'
    receipt_number = 123456789

    def __init__(self, requests_mock_instance, terminal_url):
        self.terminal_url = terminal_url
        self.status = 'NOT-REGISTERED'
        self.currency = None
        self.total = None
        self.redirect_url = None

        self._mock_api(requests_mock_instance)

    def confirm_payment(self):
        self.status = 'COMMITTED'
        return '{0}&responseCode=OK'.format(self.redirect_url)

    def cancel_payment(self):
        self.status = 'CANCELLED'
        return '{0}&responseCode=CANCELLED'.format(self.redirect_url)

    def _mock_api(self, mock):
        cart_url = '{0}{1}'.format(self.base_url, NetsApi.CART_ENDPOINT)
        mock.post(cart_url, json=self._get_cart_response_callback())

        payment_url = '{0}{1}'.format(
            self.base_url,
            NetsApi.PAYMENT_ENDPOINT.format(cartId=self.cart_id)
        )
        mock.post(payment_url, json=self._get_payment_response_callback())

        verification_url = '{0}{1}'.format(
            self.base_url,
            NetsApi.VERIFICATION_ENDPOINT.format(cartId=self.cart_id)
        )
        mock.post(
            verification_url,
            json=self._get_verification_response_callback()
        )

        finalization_url = '{0}{1}'.format(
            self.base_url,
            NetsApi.FINALIZATION_ENDPOINT.format(cartId=self.cart_id)
        )
        mock.post(
            finalization_url,
            json=self._get_finalization_response_callback()
        )

        completion_url = '{0}{1}'.format(
            self.base_url,
            NetsApi.COMPLETION_ENDPOINT.format(
                cartId=self.cart_id,
                itemId=self.item_id
            )
        )
        mock.post(completion_url, text='', status_code=204)

    def _get_cart_response_callback(self):
        def callback(request, context):
            item = request.json()['items'][0]
            amount, vat_rate = item['amount'], item['vat']
            vat_amount = item.get('vatAmount', None)
            self.total = amount + (vat_amount or (amount * vat_rate / 10000.0))
            self.currency = item['currencyCode']
            return {'cartId': self.cart_id, 'itemIds': [self.item_id]}

        return callback

    def _get_payment_response_callback(self):
        def callback(request, context):
            self.redirect_url = request.json()['redirectUrl']
            self.status = 'REGISTERED'

            return {
                'terminalUrl': self.terminal_url,
                'paymentStatus': {
                    'cartStatus': 'REGISTERED',
                    'items': [
                        {
                            'itemId': self.item_id,
                            'status': 'REGISTERED'
                        }
                    ]
                }
            }

        return callback

    def _get_verification_response_callback(self):
        def callback(request, context):
            return {
                'paymentStatus': {
                    'cartStatus': self.status,
                    'items': [
                        {
                            'itemId': self.item_id,
                            'status': self.status
                        }
                    ]
                },
                'totalAmount': self.total,
                'currencyCode': self.currency
            }

        return callback

    def _get_finalization_response_callback(self):
        def callback(request, context):
            self.status = 'PAID'

            return {
                'totalAmount': self.total,
                'currencyCode': self.currency,
                'paymentMethod': 'VISA',
                'receiptNumber': self.receipt_number,
                'paymentStatus': {
                    'cartStatus': self.status,
                    'items': [
                        {
                            'itemId': self.item_id,
                            'status': self.status
                        }
                    ]
                }
            }

        return callback


class NetsPaymentTest(AuthenticatedTestCase, object):
    def setUp(self):
        super(NetsPaymentTest, self).setUp()
        # Create basket and add a line that can be ordered
        self.basket = BasketFactory(status=Basket.OPEN, owner=self.user)
        self.stlfile = StlFileFactory(showname='The Stl Model')

        self.partner = PartnerFactory()
        self.payment_method = PartnerPaymentMethodFactory(
            partner=self.partner,
            type_name=PAYMENT_METHODS.NETS
        )

        self.product = create_product()
        self.finish = create_product()
        self.stockrecord = create_stockrecord(
            child_product=self.finish,
            partner=self.partner,
            is_default=True
        )

        germany = CountryFactory(
            alpha2='DE'
        )
        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
        )
        self.user_addr = AddressFactory(
            user=self.user,
            country=germany
        )

        self.basket.add_product(
            self.stlfile,
            product=self.product,
            stockrecord=self.stockrecord,
            quantity=1
        )
        self.basket.save()

    def _prepare_order(self, mock):
        terminal_url = 'http://nets.terminal.url/'
        api_mock = NetsApiMock(mock, terminal_url)

        shipping_url = reverse('checkout:shipping-address')
        data = {
            'action': 'ship_to',
            'address_id': self.user_addr.id,
            'address_type': 'user'
        }
        response = self.client.post(shipping_url, data, follow=True)

        self.assertIn('nets', response.content.decode())

        preview_url = reverse('checkout:preview')
        data = {
            'same_as_shipping': 'same',
            'country': 'DE',
            'paymentMethod': 'nets'
        }
        self.client.post(preview_url, data, follow=True)
        data = {
            'action': 'place_order',
            'sales_terms': 'on',
            'same_as_shipping': 'same',
            'country': 'DE'
        }
        response = self.client.post(preview_url, data, follow=True)
        self.assertIn(
            terminal_url,
            [url for url, status_code in response.redirect_chain]
        )

        return api_mock

    @requests_mock.mock()
    @skip("TODO(Serhii) - Please fix this test")
    def test_successful_nets_payment(self, mock):
        api_mock = self._prepare_order(mock)

        redirect_url = api_mock.confirm_payment()

        # Some weird logic in BasketMiddleware creates a new Basket if
        # the users only basket is frozen IN TESTS ONLY. So the basket must be
        # opened for a test to work as expected.
        self.basket.refresh_from_db()
        self.basket.thaw()

        response = self.client.get(redirect_url, follow=True)
        self.assertIn('Thank you for your order!', response.content.decode())

        self.assertEqual(Order.objects.all().count(), 1)
        order = Order.objects.first()
        self.assertEqual(order.basket, self.basket)

    @requests_mock.mock()
    @skip("TODO(Serhii) - Please fix this test")
    def test_cancelled_nets_payment(self, mock):
        api_mock = self._prepare_order(mock)

        redirect_url = api_mock.cancel_payment()

        # Some weird logic in BasketMiddleware creates a new Basket if
        # the users only basket is frozen IN TESTS ONLY. So the basket must be
        # opened for a test to work as expected.
        self.basket.refresh_from_db()
        self.basket.thaw()

        response = self.client.get(redirect_url, follow=True)

        # This was: PaymentMethodConfirmationView.payment_error_message
        expected_error = ''

        self.assertIn(expected_error.encode('utf-8'), response.content)

        self.assertEqual(Order.objects.all().count(), 0)
