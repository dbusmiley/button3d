from decimal import Decimal

from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.models import Payment
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_tests.factories import BasketLineFactory, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class CustomPaymentMethodTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.payment_method = PartnerPaymentMethodFactory(
            partner=self.partner,
            type_name=PAYMENT_METHODS.CUSTOM,
            public_config_arguments={'name': 'Custom'}
        )
        project = BasketLineFactory(basket__owner=self.user).basket
        self.order = OrderFactory(
            project=project,
            generate_payment=NO_PAYMENT
        )

    def test_handle_payment_success(self):
        self.assertEqual(Payment.objects.count(), 0)
        payment = self.payment_method.retrieve_payment(
            order=self.order, payment_arguments={}
        )

        self.assertEqual(Payment.objects.count(), 1)

        self.assertEqual(payment.amount, Decimal('256.04'))
        self.assertEqual(payment.currency, 'EUR')
        self.assertEqual(payment.metadata, {})
        self.assertEqual(payment.payment_method, self.payment_method)
        self.assertEqual(self.payment_method.name, 'Custom')

        self.payment_method.public_config_arguments = {
            'name': 'Very custom payment method',
        }
        self.payment_method.save()
        self.payment_method.refresh_from_db()
        self.assertEqual(
            self.payment_method.name, 'Very custom payment method')
