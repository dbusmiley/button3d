from decimal import Decimal

from apps.b3_checkout.exceptions import InvalidPaymentArgumentsError
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.models import Payment
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_checkout.tests.utils import \
    parse_django_rest_validation_error_details
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_tests.factories import BasketLineFactory, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class VolkswagenPaymentTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.payment_method = PartnerPaymentMethodFactory(
            partner=self.partner,
            type_name=PAYMENT_METHODS.VW
        )
        project = BasketLineFactory(basket__owner=self.user).basket
        self.order = OrderFactory(project=project, generate_payment=NO_PAYMENT)

    def test_handle_successful_payment(self):
        self.assertEqual(Payment.objects.count(), 0)

        payment_arguments = {
            'creator_name': 'User',
            'creator_email': 'user@domain.com',
            'provider': '3D-Prints 2000',
            'proposal_number': '1234',
            'proposal': 'Print this',
            'description': 'Proposal to print this part',
            'delivery_date': '2018-07-19',
            'charge_from': 'Department1',
            'charge_to': 'Department2',
        }

        payment = self.payment_method.retrieve_payment(
            order=self.order, payment_arguments=payment_arguments
        )

        self.assertEqual(Payment.objects.count(), 1)

        self.assertEqual(payment.amount, Decimal('256.04'))
        self.assertEqual(payment.currency, 'EUR')

        self.assertEqual(dict(payment.metadata), payment_arguments)
        self.assertEqual(payment.payment_method, self.payment_method)

    def test_handle_payment_args_validation_fail(self):
        self.assertEqual(Payment.objects.count(), 0)

        with self.assertRaises(InvalidPaymentArgumentsError) as context:
            self.payment_method.retrieve_payment(
                order=self.order, payment_arguments={}
            )
        expected_error_message = {
            'charge_from': 'This field is required.',
            'charge_to': 'This field is required.',
            'creator_email': 'This field is required.',
            'creator_name': 'This field is required.',
            'delivery_date': 'This field is required.',
            'description': 'This field is required.',
            'proposal': 'This field is required.',
            'proposal_number': 'This field is required.',
            'provider': 'This field is required.'
        }
        actual_exception_detail = parse_django_rest_validation_error_details(
            context.exception)

        self.assertEqual(actual_exception_detail, expected_error_message)
