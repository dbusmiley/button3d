from decimal import Decimal

from apps.b3_checkout.models import Payment
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_tests.factories import BasketLineFactory, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class InvoicePaymentTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.payment_method = self.partner.payment_methods.first()
        project = BasketLineFactory(basket__owner=self.user).basket
        self.order = OrderFactory(project=project, generate_payment=NO_PAYMENT)

    def test_handle_payment(self):
        payment = self.payment_method.retrieve_payment(
            order=self.order, payment_arguments={}
        )
        self.assertEqual(Payment.objects.count(), 1)

        self.assertEqual(payment.amount, Decimal('256.04'))
        self.assertEqual(payment.currency, 'EUR')
        self.assertEqual(payment.metadata, {})
        self.assertEqual(payment.payment_method, self.payment_method)
