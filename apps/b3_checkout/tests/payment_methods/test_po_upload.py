import pathlib
from decimal import Decimal

from django.conf import settings
from django.core.files import File

from apps.b3_checkout.exceptions import InvalidPaymentArgumentsError
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.models import Payment, PaymentAttachment
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_checkout.tests.utils import \
    parse_django_rest_validation_error_details
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_tests.factories import BasketLineFactory, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class POUploadPaymentTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.payment_method = PartnerPaymentMethodFactory(
            partner=self.partner,
            type_name=PAYMENT_METHODS.PO_UPLOAD,
        )
        project = BasketLineFactory(basket__owner=self.user).basket
        self.order = OrderFactory(project=project, generate_payment=NO_PAYMENT)

    def test_handle_successful_payment_with_valid_attachment_list(self):
        self.assertEqual(Payment.objects.count(), 0)

        # create a couple attachments
        filepath = pathlib.Path(settings.BASE_DIR) / 'apps/b3_core/static' \
                                                     '/images/upload.png'
        with open(filepath, 'rb') as image_file:
            attachment = PaymentAttachment.objects.create(
                file=File(image_file),
                filename='upload.png',
                filesize=1000,
                uploader=self.user
            )

        payment = self.payment_method.retrieve_payment(
            order=self.order,
            payment_arguments={'attachments': [attachment.id]}
        )

        self.assertEqual(Payment.objects.count(), 1)

        self.assertEqual(payment.amount, Decimal('256.04'))
        self.assertEqual(payment.currency, 'EUR')
        self.assertEqual(payment.metadata, {'attachments': [attachment.id]})
        self.assertEqual(payment.payment_method, self.payment_method)

        attachment.refresh_from_db()
        self.assertEqual(attachment.payment, payment)

    def test_error_on_empty_attachment_list(self):
        self.assertEqual(Payment.objects.count(), 0)

        with self.assertRaises(InvalidPaymentArgumentsError) as context:
            self.payment_method.retrieve_payment(
                order=self.order, payment_arguments={'attachments': []}
            )

        expected_error_message = {
            'attachments': 'This list may not be empty.'
        }
        actual_exception_detail = parse_django_rest_validation_error_details(
            context.exception)

        self.assertEqual(actual_exception_detail, expected_error_message)
        self.assertEqual(Payment.objects.count(), 0)

    def test_handle_payment_with_validation_error(self):
        self.assertEqual(Payment.objects.count(), 0)

        with self.assertRaises(InvalidPaymentArgumentsError) as context:
            self.payment_method.retrieve_payment(
                order=self.order,
                # attachments do not exist
                payment_arguments={'attachments': [23, 98]}
            )
        expected_error_message = {
            'attachments': 'Not all attachments exist: 23, 98'
        }
        actual_exception_detail = parse_django_rest_validation_error_details(
            context.exception)

        self.assertEqual(actual_exception_detail, expected_error_message)
        self.assertEqual(Payment.objects.count(), 0)
