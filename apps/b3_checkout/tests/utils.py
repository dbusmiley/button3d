def parse_django_rest_validation_error_details(exception):
    return {
        key: str(value[0]) for key, value in
        exception.detail.items()
    }
