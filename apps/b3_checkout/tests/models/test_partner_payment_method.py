from apps.b3_checkout.models import PartnerPaymentMethod, \
    SupportedPaymentMethod
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_migration.sync.utils import disable_auto_sync
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.partner.models import Partner


class PartnerPaymentMethodTest(TestCase):
    def setUp(self):
        super().setUp()
        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(payment_method=None)

    def test_create_payment_method_changes_available_method_list(self):
        payment_methods_available = \
            self.partner.get_available_for_creation_payment_method_types()

        self.assertEqual(set(payment_methods_available),
                         {'stripe', 'invoice', 'custom'})

        partner_payment_method = PartnerPaymentMethod()
        partner_payment_method.partner = self.partner
        partner_payment_method.type_name = PAYMENT_METHODS.INVOICE
        partner_payment_method.is_enabled = True
        partner_payment_method.save()
        self.assertEqual(partner_payment_method.name, 'Invoice')
        self.assertIsNotNone(partner_payment_method.id)

        payment_methods_available2 = \
            self.partner.get_available_for_creation_payment_method_types()

        self.assertEqual(set(payment_methods_available2),
                         {'stripe', 'custom'})

        # add non-available payment method
        SupportedPaymentMethod.objects.bulk_create((
            SupportedPaymentMethod(partner=self.partner, type_name='invoice'),
            SupportedPaymentMethod(partner=self.partner, type_name='nets'),
        ))

        payment_methods_available3 = \
            self.partner.get_available_for_creation_payment_method_types()

        self.assertEqual(set(payment_methods_available3),
                         {'stripe', 'custom', 'nets'})

    def test_create_custom_payment_method_not_changes_available_method_list(
            self):
        payment_methods_available = \
            self.partner.get_available_for_creation_payment_method_types()

        payment_methods_available_number = len(payment_methods_available)

        partner_payment_method = PartnerPaymentMethod()
        partner_payment_method.partner = self.partner
        partner_payment_method.type_name = PAYMENT_METHODS.CUSTOM
        partner_payment_method.is_enabled = True
        partner_payment_method.public_config_arguments = {'name': 'Custom'}
        partner_payment_method.save()

        self.assertIsNotNone(partner_payment_method.id)

        payment_methods_available = \
            self.partner.get_available_for_creation_payment_method_types()

        self.assertEqual(
            len(payment_methods_available),
            payment_methods_available_number
        )

        self.assertEqual(partner_payment_method.name, 'Custom')

    def test_payment_method_config_variables_are_saved(self):
        partner_payment_method = PartnerPaymentMethod()
        partner_payment_method.partner = self.partner
        partner_payment_method.type_name = PAYMENT_METHODS.CUSTOM
        partner_payment_method.is_enabled = True
        partner_payment_method.public_config_arguments = {'name': 'test1'}
        partner_payment_method.save()

        partner_payment_method.refresh_from_db()
        self.assertFalse(
            'description' in partner_payment_method.public_config_arguments
        )
        partner_payment_method.public_config_arguments['description'] = 'test2'

        partner_payment_method.save()

        partner_payment_method.refresh_from_db()
        self.assertEqual(
            partner_payment_method.public_config_arguments,
            {'name': 'test1', 'description': 'test2'}
        )
