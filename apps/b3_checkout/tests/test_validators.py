from decimal import Decimal as D

from apps.b3_checkout.validators import errors
from apps.b3_checkout.validators.project_checkout_validator \
    import ProjectCheckoutValidator
from apps.b3_tests.factories import BasketFactory, BasketLineFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class ValidatorTest(TestCase):
    def test_project_checkout_with_valid_project(self):
        project = BasketLineFactory().basket

        validator = ProjectCheckoutValidator(project)
        self.assertTrue(validator.is_valid())

    def test_project_checkout_with_invalid_project(self):
        project = BasketLineFactory().basket
        project.partner.delete()
        project.all_lines()[0].stl_file.delete()
        project.all_lines()[0].stockrecord.delete()

        validator = ProjectCheckoutValidator(project)
        validator.is_valid()
        self.assertEqual(
            validator.errors,
            [{'code': errors.ERROR_SERVICE_DELETED,
              'message': 'The selected service has been deleted.'}]
        )

    def test_project_checkout_with_empty_project(self):
        project = BasketFactory()

        validator = ProjectCheckoutValidator(project)
        validator.is_valid()
        self.assertEqual(
            validator.errors,
            [{'code': errors.ERROR_NO_PARTS,
              'message': 'Project has no lines.'}])

    def test_project_checkout_with_manually_priced_project(self):
        self.populate_current_request()
        project = BasketLineFactory().basket
        line = project.all_lines()[0]
        line.is_manual_pricing_required = True
        line.set_manual_price(D('1.00'))
        line.save()
        project.set_as_manually_priced()
        project.save()

        validator = ProjectCheckoutValidator(project)
        self.assertTrue(validator.is_valid())

    def test_project_checkout_with_unpriced_project(self):
        project = BasketLineFactory().basket
        line = project.all_lines()[0]
        line.is_manual_pricing_required = True
        line.save()

        validator = ProjectCheckoutValidator(project)
        validator.is_valid()
        self.assertEqual(
            validator.errors,
            [{'code': errors.ERROR_MANUAL_PRICING_REQUIRED,
              'message': 'Order needs manual pricing.'}]
        )
