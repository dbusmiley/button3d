import json
from urllib.parse import urlparse, parse_qs

from requests_mock import Mocker

from apps.b3_checkout.models import PartnerPaymentMethod, \
    SupportedPaymentMethod
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_migration.sync.utils import disable_auto_sync
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.models import Partner


class CreateConfiglessPartnerPaymentMethod(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(site=self.site, payment_method=None)
            self.partner.users.add(self.user)

    def test_create_invoice_payment_method(self):
        self.create_payment_method('invoice')

    def test_create_pay_in_store_payment_method(self):
        self.create_payment_method('pay-in-store')

    def test_create_volkswagen_payment_method(self):
        self.create_payment_method('volkswagen')

    def test_create_po_upload_payment_method(self):
        self.create_payment_method('po-upload')

    def create_payment_method(self, type_name, config_arguments=None):
        config_arguments = config_arguments or {}
        if type_name not in \
                self.partner.get_available_for_creation_payment_method_types():
            SupportedPaymentMethod.objects.create(
                partner=self.partner,
                type_name=type_name,
            )

        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)
        payload = {
            'type': type_name,
            'configArguments': config_arguments,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(PartnerPaymentMethod.objects.count(), 1)
        new_payment_method = PartnerPaymentMethod.objects.first()

        self.assertEqual(
            json.loads(response.content),
            {'configArguments': config_arguments,
             'isBillingAddressRequired': True,
             'id': new_payment_method.id,
             'isEnabled': True,
             'type': type_name}
        )

        self.assertEqual(
            new_payment_method.public_config_arguments,
            config_arguments
        )

        response2 = self.client.get(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/')
        self.assertEqual(
            json.loads(response2.content),
            [{'configArguments': config_arguments,
              'id': new_payment_method.id,
              'isBillingAddressRequired': True,
              'isEnabled': True,
              'type': type_name}]
        )

    def test_create_not_allowed_method(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)
        payload = {
            'type': 'random_method_name',
            'configArguments': {},
            'isBillingAddressRequired': True,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)

        self.assertEqual(
            json.loads(response.content),
            {'code': 'Partner_VALIDATION_ERROR',
             'message': 'Model field validation error.',
             'moreInfo': [
                 {'field': 'type',
                  'message': ['invalid payment type: random_method_name']}]}
        )


class CreateCustomPartnerPaymentMethod(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(site=self.site, payment_method=None)
            self.partner.users.add(self.user)

    def test_create_custom_payment_method_validation_fail_field_required(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)
        payload = {
            'type': 'custom',
            'configArguments': {},
            'isBillingAddressRequired': True,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)

        self.assertEqual(
            json.loads(response.content),
            {'code': 'Partner_VALIDATION_ERROR',
             'message': 'Model field validation error.',
             'moreInfo': [{'field': 'configArguments',
                           'message': {
                               'name': ['This field is required.']}}]}
        )

    def test_create_custom_payment_method_validation_fail_too_long_name(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)
        payload = {
            'type': 'custom',
            'configArguments': {
                'name': 'name ' * 10000,
            },
            'isBillingAddressRequired': True,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 400)

        expected = {
            'code': 'Partner_VALIDATION_ERROR',
            'message': 'Model field validation error.',
            'moreInfo': [
                {'field': 'configArguments',
                 'message': {
                     'name': ['Ensure this field has no more than '
                              '128 characters.']}}]}

        self.assertEqual(
            expected,
            json.loads(response.content),
        )

    def test_create_custom_payment_method_success(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 0)
        payload = {
            'type': 'custom',
            'configArguments': {
                'name': 'Custom payment method'
            },
            'isBillingAddressRequired': True,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(PartnerPaymentMethod.objects.count(), 1)
        new_payment_method = PartnerPaymentMethod.objects.first()

        self.assertEqual(
            json.loads(response.content),
            {'configArguments': {'name': 'Custom payment method'},
             'id': new_payment_method.id,
             'isBillingAddressRequired': True,
             'isEnabled': True,
             'type': 'custom'}
        )

        response2 = self.client.get(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/')
        self.assertEqual(
            json.loads(response2.content),
            [{'configArguments': {'name': 'Custom payment method'},
              'id': new_payment_method.id,
              'isBillingAddressRequired': True,
              'isEnabled': True,
              'type': 'custom'}]
        )


class CreateStripePartnerPaymentMethod(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(site=self.site)
        self.partner.users.add(self.user)

    def test_create_and_change_stripe_payment_method(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 1)

        payload = {
            'type': 'stripe',
            'configArguments': {},
            'isBillingAddressRequired': True,
            'isEnabled': True,
        }

        response = self.client.post(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/',
            data=json.dumps(payload),
            content_type='application/json',
        )

        self.assertEqual(response.status_code, 400)
        expected_response = {
            'code': 'Partner_VALIDATION_ERROR',
            'message': 'Model field validation error.',
            'moreInfo': [
                {'field': 'configArguments',
                 'message': {
                     'stripe_publishable_key': ['This field is required.'],
                     'stripe_user_id': ['This field is required.'],
                 }}
            ]}

        self.assertEqual(json.loads(response.content), expected_response)

        # TODO(felix) Change FE URL. And Test Stripe connection
        response = self.client.get(
            f'/api/v2.0/service-panel/services/{self.partner.id}/'
            f'payment-methods/stripe/callbackurl/',
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)
        url = json.loads(response.content)['callbackUrl']

        parsed_url = urlparse(url)
        self.assertEqual(parsed_url.scheme, 'https')
        self.assertEqual(parsed_url.netloc, 'connect.stripe.com')
        self.assertEqual(parsed_url.path, '/oauth/authorize')
        self.assertEqual(parsed_url.params, '')
        query = parse_qs(parsed_url.query)
        self.assertEqual(
            {'client_id', 'response_type', 'redirect_uri', 'scope', 'state'},
            set(query.keys()))

        self.assertEqual(
            query['redirect_uri'],
            ['http://my.3yd/service-panel/#/settings/payment-methods/']
        )
        self.assertEqual(
            query['state'],
            ['stripe-connected']
        )
        with Mocker() as mocked_requests:
            mocked_requests.post(
                'https://connect.stripe.com/oauth/token',
                json={
                    'access_token': 'XXXXXXXXXXXXXX',
                    'livemode': True,
                    'refresh_token': 'XXXXXXXXXXXXXX',
                    'scope': 'XXXXXXXXXXXXXX',
                    'stripe_publishable_key': 'XXXXXXXXXXXXXX',
                    'stripe_user_id': 'XXXXXXXXXXXXXX',
                    'token_type': 'XXXXXXXXXXXXXX'
                },
            )
            # TODO(felix) Change FE URL. And Test Stripe connection
            response_connected = self.client.post(
                f'/api/v2.0/service-panel/'
                f'services/{self.partner.id}/payment-methods/'
                f'stripe/connect/',
                data=json.dumps({'code': '1234567'}),
                content_type='application/json'
            )

        self.assertEqual(response_connected.status_code, 204)
        self.assertEqual(PartnerPaymentMethod.objects.count(), 2)

        payment_method = PartnerPaymentMethod.objects.get(
            type_name=PAYMENT_METHODS.STRIPE
        )
        self.assertEqual(
            payment_method.public_config_arguments,
            {
                'stripe_publishable_key': 'XXXXXXXXXXXXXX',
                'stripe_user_id': 'XXXXXXXXXXXXXX',
            }
        )
        self.assertEqual(
            payment_method.private_config_arguments,
            {
                'access_token': 'XXXXXXXXXXXXXX',
                'refresh_token': 'XXXXXXXXXXXXXX',
                'livemode': True,
                'token_type': 'XXXXXXXXXXXXXX',
                'scope': 'XXXXXXXXXXXXXX',
            }
        )
        self.assertEqual(payment_method.type_name, PAYMENT_METHODS.STRIPE)

        # check if we can see config arguments and private_config_arguments
        # are not shown
        payment_method_list_response = self.client.get(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/')
        stripe_payment_method = [
            entry for entry in json.loads(payment_method_list_response.content)
            if entry['type'] == PAYMENT_METHODS.STRIPE
        ][0]
        self.assertEqual(
            stripe_payment_method,
            {
                'configArguments': {
                    'stripe_publishable_key': 'XXXXXXXXXXXXXX',
                    'stripe_user_id': 'XXXXXXXXXXXXXX'
                },
                'id': payment_method.id,
                'isBillingAddressRequired': True,
                'isEnabled': True,
                'type': 'stripe'
            }
        )

        # let's change details:
        payload_to_update = {
            'configArguments': {'stripe_publishable_key': 'YYYYYYYY',
                                'stripe_user_id': 'YYYYYYY'},
            'isEnabled': True,
        }

        update_response = self.client.put(
            f'/api/v2.0/service-panel/services/{self.partner.id}/'
            f'payment-methods/{payment_method.id}/',
            data=json.dumps(payload_to_update),
            content_type='application/json',
        )
        self.assertEqual(
            json.loads(update_response.content),
            {'configArguments': {'stripe_publishable_key': 'YYYYYYYY',
                                 'stripe_user_id': 'YYYYYYY'},
             'id': payment_method.id,
             'isBillingAddressRequired': True,
             'isEnabled': True,
             'type': 'stripe'}
        )


class UpdatePaymentMethod(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(
                site=self.site,
                payment_method=None
            )
        self.partner.users.add(self.user)

        self.new_payment_method = PartnerPaymentMethod.objects.create(
            type_name=PAYMENT_METHODS.CUSTOM,
            partner=self.partner,
            public_config_arguments={'name': 'Custom method'},
            is_enabled=True,
        )

    def test_create_and_update_payment_method(self):
        payload_to_update = {
            'isBillingAddressRequired': True,
            'isEnabled': True,
            'configArguments': {
                'name': 'Changed custom method',
            },
        }

        response2 = self.client.put(
            f'/api/v2.0/service-panel/services/{self.partner.id}/'
            f'payment-methods/{self.new_payment_method.id}/',
            data=json.dumps(payload_to_update),
            content_type='application/json',
        )

        self.assertEqual(
            json.loads(response2.content),
            {'configArguments': {'name': 'Changed custom method'},
             'id': self.new_payment_method.id,
             'isBillingAddressRequired': True,
             'isEnabled': True,
             'type': 'custom'}
        )

    def test_payment_method_cant_be_disabled(self):
        with disable_auto_sync(PartnerPaymentMethod):
            PartnerPaymentMethod.objects.create(
                type_name=PAYMENT_METHODS.CUSTOM,
                partner=self.partner,
                public_config_arguments={'name': 'Another custom method'},
                is_enabled=True,
            )

        payload_to_update = {
            'isBillingAddressRequired': True,
            'isEnabled': False,
            'configArguments': {
                'name': 'Changed custom method',
            },
        }

        with disable_auto_sync(PartnerPaymentMethod):
            response = self.client.put(
                f'/api/v2.0/service-panel/services/{self.partner.id}/'
                f'payment-methods/{self.new_payment_method.id}/',
                data=json.dumps(payload_to_update),
                content_type='application/json',
            )
        expected = {
            'configArguments': {'name': 'Changed custom method'},
            'id': self.new_payment_method.id,
            'isBillingAddressRequired': True,
            'isEnabled': False,
            'type': 'custom'}

        self.assertEqual(
            expected,
            json.loads(response.content),
        )

    def test_last_payment_method_cant_be_disabled(self):
        payload_to_update = {
            'isBillingAddressRequired': True,
            'isEnabled': False,
            'configArguments': {
                'name': 'Changed custom method',
            },
        }

        with disable_auto_sync(PartnerPaymentMethod):
            response = self.client.put(
                f'/api/v2.0/service-panel/services/{self.partner.id}/'
                f'payment-methods/{self.new_payment_method.id}/',
                data=json.dumps(payload_to_update),
                content_type='application/json',
            )
        expected = {'code': 'PartnerPaymentMethod_VALIDATION_ERROR',
                    'message': 'Model field validation error.',
                    'moreInfo': [
                        {'field': 'isEnabled',
                         'message': ['At least one payment method must '
                                     'be enabled']}]}

        self.assertEqual(
            expected,
            json.loads(response.content),
        )
