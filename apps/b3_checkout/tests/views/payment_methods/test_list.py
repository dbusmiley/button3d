import json

from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_migration.sync.utils import disable_auto_sync
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.models import Partner


class PartnerPaymentMethodListTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(site=self.site, payment_method=None)
            self.partner.users.add(self.user)

    def test_get_created_methods_list(self):
        partner_payment_method = PartnerPaymentMethod()
        partner_payment_method.partner = self.partner
        partner_payment_method.type_name = PAYMENT_METHODS.INVOICE
        partner_payment_method.is_enabled = True
        partner_payment_method.save()

        response = self.client.get(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-methods/')
        self.assertEqual(
            json.loads(response.content),
            [{'configArguments': {},
              'id': partner_payment_method.id,
              'isBillingAddressRequired': True,
              'isEnabled': True,
              'type': 'invoice'}]
        )

    def test_user_panel_list_payment_methods(self):
        invoice_payment = PartnerPaymentMethod.objects.create(
            partner=self.partner,
            type_name=PAYMENT_METHODS.INVOICE
        )
        custom_payment = PartnerPaymentMethod.objects.create(
            partner=self.partner,
            type_name=PAYMENT_METHODS.CUSTOM,
            public_config_arguments={'name': 'Bla'}
        )

        response = self.client.get(
            f'/api/v2.0/user-panel/services/{self.partner.id}/payment-methods/'
        )
        response_json = json.loads(response.content)
        self.assertEqual(len(response_json), 2)
        self.assertIn(
            {
                'id': invoice_payment.id,
                'type': PAYMENT_METHODS.INVOICE,
                'additionalInformation': {},
                'isBillingAddressRequired': True,
            },
            response_json
        )
        self.assertIn(
            {
                'id': custom_payment.id,
                'type': PAYMENT_METHODS.CUSTOM,
                'additionalInformation': {
                    'name': 'Bla',
                    'description': None
                },
                'isBillingAddressRequired': True,
            },
            response_json
        )
