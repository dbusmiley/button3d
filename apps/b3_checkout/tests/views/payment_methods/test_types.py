import json

from apps.b3_migration.sync.utils import disable_auto_sync
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.models import Partner


class PartnerPaymentMethodTypesTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(site=self.site, payment_method=None)
            self.partner.users.add(self.user)

    def test_get_available_method_types(self):
        response = self.client.get(
            f'/api/v2.0/'
            f'service-panel/services/{self.partner.id}/payment-method-types/')
        self.assertEqual(
            set(json.loads(response.content)),
            {'invoice', 'stripe', 'custom'}
        )
