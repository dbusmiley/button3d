import json

from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_checkout.payment_methods import PAYMENT_METHODS
from apps.b3_migration.sync.utils import disable_auto_sync
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.models import Partner


class DeletePartnerPaymentMethod(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        with disable_auto_sync(Partner):
            self.partner = PartnerFactory(site=self.site, payment_method=None)
            self.partner.users.add(self.user)

            with disable_auto_sync(PartnerPaymentMethod):
                self.partner_payment_method = \
                    PartnerPaymentMethod.objects.create(
                        type_name=PAYMENT_METHODS.INVOICE,
                        partner=self.partner
                    )

    def test_fail_to_delete_last_payment_method(self):
        self.assertEqual(PartnerPaymentMethod.objects.count(), 1)

        payment_method1 = PartnerPaymentMethod.objects.first()
        payment_method1_id = payment_method1.id
        self.assertIsNone(payment_method1.deleted_date)

        # try to delete the only one payment method
        with disable_auto_sync(PartnerPaymentMethod):
            response = self.client.delete(
                f'/api/v2.0/service-panel/services/{self.partner.id}/'
                f'payment-methods/{payment_method1_id}/',
            )
        self.assertEqual(response.status_code, 400)

        expected = {
            'code': 'PartnerPaymentMethod_VALIDATION_ERROR',
            'message': 'Model field validation error.',
            'moreInfo': [
                {'field': '__all__',
                 'message': 'At least one payment method must exist'}]}

        self.assertEqual(expected, json.loads(response.content))

    def test_delete_payment_method(self):
        with disable_auto_sync(PartnerPaymentMethod):
            self.partner_payment_method = PartnerPaymentMethod.objects.create(
                type_name=PAYMENT_METHODS.CUSTOM,
                partner=self.partner,
                public_config_arguments={'name': 'Custom PM'}
            )

            response = self.client.delete(
                f'/api/v2.0/service-panel/services/{self.partner.id}/'
                f'payment-methods/{self.partner_payment_method.id}/',
            )

        self.assertEqual(response.status_code, 204)

        expected = ''

        self.assertEqual(expected, response.content.decode())
