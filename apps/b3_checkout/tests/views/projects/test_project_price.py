from decimal import Decimal as D

from apps.b3_core.utils import quantize
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import StockRecordFactory, \
    PostProcessingFactory, BasketFactory, StlFileFactory, \
    PartnerExtraFeeFactory, PartnerFactory, VoucherFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.conclusionactions import FixedDiscountAction
from apps.partner.conditions import QuantityGreaterCondition


class UserPanelProjectPriceTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(
            minimum_order_price_enabled=True,
            minimum_order_price_value=D('1000.00')
        )
        price_formula = ("max(10.5/quantity, 1.25/quantity+root(volume)"
                         "+pow(box, 0.33)+min(pow(box, 0.2), area)*shells)")
        stock_record = StockRecordFactory(
            price_custom=price_formula,
            partner=self.partner
        )
        stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=1)
        )
        post_processing = PostProcessingFactory(
            stock_record=stock_record
        )
        self.fee = PartnerExtraFeeFactory(partner=self.partner)
        self.voucher = VoucherFactory(partner=self.partner)
        self.project = BasketFactory(owner=self.user)
        self.item, created = self.project.add_product(
            product=stock_record.product,
            stockrecord=stock_record,
            stlfile=StlFileFactory(),
            post_processings=[{
                'post_processing': post_processing,
                'color': None
            }],
            quantity=2,
            scale=0.5
        )
        self.project.refresh_price()
        self.shipping_method = ShippingMethodFactory(partner=self.partner)

    def test_price(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.project.id}/price/',
            data={
                'shippingMethodId': self.shipping_method.id,
                'voucherCode': self.voucher.code
            }
        )
        self.assertEqual(response.status_code, 200)
        price = response.json()

        self.assertEqual(price['currency'], self.project.currency)
        self.assertEqual(price['taxRate'], str(self.project.partner.vat_rate))
        self.assertEqual(price['taxType'], self.project.partner.tax_type)

        self.assertEqual(
            price['minimumPrice']['exclusiveTax'],
            str(self.project.partner.min_order_price_object.excl_tax)
        )
        self.assertEqual(
            price['minimumPrice']['inclusiveTax'],
            str(self.project.partner.min_order_price_object.incl_tax)
        )

        self.assertEqual(len(price['fees']), 1)
        fee = price['fees'][0]
        self.assertEqual(fee['name'], self.fee.type)
        self.assertEqual(
            fee['amount']['exclusiveTax'],
            str(self.fee.price_object.excl_tax)
        )
        self.assertEqual(
            fee['amount']['inclusiveTax'],
            str(self.fee.price_object.incl_tax)
        )

        self.assertEqual(len(price['lines']), 1)
        item = price['lines'][0]
        self.assertEqual(item['lineId'], self.item.id)
        self.assertEqual(item['quantity'], self.item.quantity)
        item_price = item['price']

        exp_unit_price_net = D('189.71')
        exp_unit_price_gross = D('189.71') + D('36.04')
        self.assertEqual(
            item_price['unitPrice']['exclusiveTax'],
            str(exp_unit_price_net)
        )
        self.assertEqual(
            item_price['unitPrice']['inclusiveTax'],
            str(exp_unit_price_gross)
        )

        exp_discount_net = D('10.00')
        exp_discount_gross = D('11.90')
        self.assertEqual(
            item_price['itemDiscount']['exclusiveTax'],
            str(exp_discount_net)
        )
        self.assertEqual(
            item_price['itemDiscount']['inclusiveTax'],
            str(exp_discount_gross)
        )

        exp_item_total_net = exp_unit_price_net * 2 - exp_discount_net
        exp_item_total_gross = exp_unit_price_gross * 2 - exp_discount_gross
        self.assertEqual(
            item_price['itemTotal']['exclusiveTax'],
            str(exp_item_total_net)
        )
        self.assertEqual(
            item_price['itemTotal']['inclusiveTax'],
            str(exp_item_total_gross)
        )

        summary = price['priceSummary']
        self.assertEqual(
            summary['subTotal']['exclusiveTax'],
            str(exp_item_total_net)
        )
        self.assertEqual(
            summary['subTotal']['inclusiveTax'],
            str(exp_item_total_gross)
        )

        exp_shipping_net = D('10.00')
        exp_shipping_gross = D('11.90')
        self.assertEqual(
            summary['shippingCost']['exclusiveTax'],
            str(exp_shipping_net)
        )
        self.assertEqual(
            summary['shippingCost']['inclusiveTax'],
            str(exp_shipping_gross)
        )

        exp_fees_net = D('10.00')
        exp_fees_gross = D('11.90')
        self.assertEqual(
            summary['fees']['exclusiveTax'],
            str(exp_fees_net)
        )
        self.assertEqual(
            summary['fees']['inclusiveTax'],
            str(exp_fees_gross)
        )

        exp_min_price_diff_net = D('1000.00') - (
            exp_item_total_net
            + exp_fees_net
        )
        exp_min_price_diff_gross = D('1190.00') - (
            exp_item_total_gross
            + exp_fees_gross
        )
        self.assertEqual(
            summary['minimumPriceDifference']['exclusiveTax'],
            str(exp_min_price_diff_net)
        )
        self.assertEqual(
            summary['minimumPriceDifference']['inclusiveTax'],
            str(exp_min_price_diff_gross)
        )

        voucher_discount_base_net = (
            exp_item_total_net
            + exp_shipping_net
            + exp_fees_net
            + exp_min_price_diff_net
        )
        voucher_discount_base_gross = (
            exp_item_total_gross
            + exp_shipping_gross
            + exp_fees_gross
            + exp_min_price_diff_gross
        )

        exp_voucher_discount_net = quantize(
            voucher_discount_base_net * D('0.10')
        )
        exp_voucher_discount_gross = quantize(
            voucher_discount_base_gross * D('0.10')
        )
        self.assertEqual(
            summary['voucherDiscount']['exclusiveTax'],
            str(exp_voucher_discount_net)
        )
        self.assertEqual(
            summary['voucherDiscount']['inclusiveTax'],
            str(exp_voucher_discount_gross)
        )

        exp_total_net = sum([
            exp_item_total_net,
            exp_shipping_net,
            exp_fees_net,
            exp_min_price_diff_net,
            -exp_voucher_discount_net,
        ])
        exp_total_gross = sum([
            exp_item_total_gross,
            exp_shipping_gross,
            exp_fees_gross,
            exp_min_price_diff_gross,
            -exp_voucher_discount_gross,
        ])
        self.assertEqual(
            summary['total']['exclusiveTax'],
            str(exp_total_net)
        )
        self.assertEqual(
            summary['total']['inclusiveTax'],
            str(exp_total_gross)
        )

    def test_price_without_mimum_price(self):
        self.partner.minimum_order_price_enabled = False
        self.partner.save()
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.project.id}/price/',
            data={
                'shippingMethodId': self.shipping_method.id,
                'voucherCode': self.voucher.code
            }
        )
        self.assertEqual(response.status_code, 200)
        self.partner.minimum_order_price_enabled = True
        self.partner.save()
