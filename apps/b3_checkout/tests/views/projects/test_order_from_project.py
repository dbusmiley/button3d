import json
import typing as t
from decimal import Decimal as D
import mock
import unittest

from django.conf import settings
from django.test import override_settings
from guardian.shortcuts import assign_perm

import button3d.type_declarations as td
from apps.b3_address.factories import AddressFactory, CountryFactory
from apps.b3_order.models import Order, OrderStatus
from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import BasketLineFactory, VoucherFactory, \
    UserFactory, StockRecordFactory, PartnerFactory
from apps.b3_tests.factories import BasketFactory as ProjectFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket

TEMPLATES = settings.TEMPLATES.copy()
TEMPLATES[0]['OPTIONS']['string_if_invalid'] = 'Invalid variable %s'
TEMPLATES[0]['OPTIONS']['debug'] = True
T_ORDER_CREATE_CB = t.Callable[[t.Any], Order]
LINE_PRICE = 100
SHIPPING_PRICE = 20
USDUSD = 1
USDEUR = 2


@override_settings(TEMPLATES=TEMPLATES)
class OrderFromProjectTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        country = CountryFactory(alpha2='DE')

        self.organization.organization_panel_admins.add(self.user)
        self.partner: td.Partner = PartnerFactory()
        price_formula = str(LINE_PRICE)
        self.stock_record = StockRecordFactory(
            price_custom=price_formula,
            partner=self.partner
        )
        self.project = BasketLineFactory(
            basket__owner=self.user,
            stockrecord=self.stock_record
        ).basket
        self.another_user = UserFactory()
        assign_perm(Basket.VIEW_PERMISSION, self.another_user, self.project)
        assign_perm(Basket.EDIT_PERMISSION, self.another_user, self.project)
        assign_perm(Basket.ORDER_PERMISSION, self.another_user, self.project)

        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
            price_excl_tax=SHIPPING_PRICE
        )
        self.shipping_method.countries.add(country)

        self.billing_address = AddressFactory(user=self.user, country=country)
        self.shipping_address = AddressFactory(user=self.user, country=country)
        self.payment_method = self.partner.payment_methods.first()
        self.voucher = VoucherFactory(partner=self.partner)
        self.pickup_location = PickupLocationFactory(partner=self.partner)
        self.expected_payment_with_shipping = (
            (D(LINE_PRICE + SHIPPING_PRICE)) * (1 + self.partner.vat_rate/100)
            * ((100 - self.voucher.action_value) / 100)).quantize(D('0.00')
                                                                  )
        self.expected_payment_with_pickup = (
            (D(LINE_PRICE)) * (1 + self.partner.vat_rate/100)
            * ((100 - self.voucher.action_value) / 100)).quantize(D('0.00')
                                                                  )
        self.assertEqual(Order.objects.count(), 0)

    def test_order_project_with_shipping_method(self):
        response = self._create_order_with_shipping_method(
            self.project
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 1)

    def test_order_project_with_no_shipping_method(self):
        response = self._create_order_with_no_shipping_method(self.project)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(Order.objects.count(), 0)
        expected = {
            'code': 'PROJECT_CAN_NOT_BE_ORDERED',
            'message':
                f'This project (id={self.project.id}) can not be ordered.',
            'moreInfo': {
                'errors': [{
                    'code': 'ERROR_EITHER_SHIPPING_OR_PICKUP',
                    'message': 'Neither shipping method or a pickup '
                               'location are specified'}]}}
        self.assertEqual(response.data, expected)

    def test_order_project_with_pickup(self):
        response = self._create_order_with_pickup_location(self.project)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 1)

    @override_settings(
        ENABLE_CURRENCY_API=False,
        DEFAULT_EXCHANGE_RATES={'USDUSD': USDUSD, 'USDEUR': USDEUR}
    )
    def test_order_project_with_user_currency(self):
        self.partner.additional_supported_currencies = ['USD']
        response = self._create_order_with_currency(
            self.project,
            currency='USD',
            authorized_amount=str(self.expected_payment_with_shipping / USDEUR)
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 1)

    def test_order_project_with_no_voucher_code(self):
        response = self._create_order_with_no_voucher(self.project)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Order.objects.count(), 1)

    def test_order_project_with_unsupported_currency(self):
        response = self._create_order_with_currency(
            self.project,
            currency='USD',
            authorized_amount=str(self.expected_payment_with_shipping)
        )
        self.assertEqual(Order.objects.count(), 0)
        self.assertEqual(response.status_code, 400)

        expected = {'code': 'PROJECT_CAN_NOT_BE_ORDERED',
                    'message':
                        f'This project (id={self.project.id}) can not '
                        f'be ordered.',
                    'moreInfo': {
                        'errors': [{
                            'code': 'ERROR_PAYMENT_WRONG_PRICE',
                            'message': 'The selected currency USD is not '
                                       'supported by the service.'
                        }]
                    }}
        self.assertEqual(json.loads(response.content), expected)

    def test_order_project_with_no_project_lines(self):
        empty_project = ProjectFactory()
        response = self._create_order_with_no_project_lines(empty_project)
        self.assertEqual(Order.objects.count(), 0)
        expected = {'code': 'BASKET_NOT_FOUND',
                    'message': 'BASKET not found for the given id',
                    'moreInfo': {'id': str(empty_project.id),
                                 'objectName': 'BASKET'}}
        self.assertEqual(response.data, expected)

    @unittest.skip('Relies on stable dict order. Fix in 3.0.0.')
    def test_order_fields_incorrectly_filled(self):
        response = self._create_order_with_incorrect_fields(self.project)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(Order.objects.count(), 0)
        expected = {'code': 'PROJECT_CAN_NOT_BE_ORDERED',
                    'message': f'This project (id={self.project.id}) can '
                    f'not be ordered.',
                    'moreInfo': {
                        'errors': [
                            {'billingAddressId':
                             ['Invalid pk "200" - object does not '
                              'exist.'],
                             'shipping': {
                                 'method_id': [
                                     'Incorrect type. Expected pk value, '
                                     'received str.']}}]}}
        self.assertEqual(json.loads(response.content), expected)

    def test_order_with_no_payment_method(self):
        response = self._create_order_with_no_payment_method(
            self.project
        )
        self.assertEqual(Order.objects.count(), 0)
        self.assertEqual(response.status_code, 400)

        expected = {'code': 'PROJECT_CAN_NOT_BE_ORDERED',
                    'message':
                        f'This project (id={self.project.id}) can not '
                        f'be ordered.',
                    'moreInfo': {'errors': [
                        {'payment': ['This field may not be null.']}]}}
        self.assertEqual(json.loads(response.content), expected)

    def test_customer_email_sent_with_shipping_method(self):
        with mock.patch(
                'django.core.mail.backends.locmem.EmailBackend.send_messages'
        ) as send_messages_mocked:
            response = self._create_order_with_shipping_method(
                self.project
            )

        self.assertEqual(response.status_code, 201)

        self.assertEqual(3, send_messages_mocked.call_count)
        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[0][0][0][0]
        )

        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[1][0][0][0]
        )

        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[2][0][0][0]
        )

    def test_customer_email_sent_with_pickup_location(self):
        with mock.patch(
                'django.core.mail.backends.locmem.EmailBackend.send_messages'
        ) as send_messages_mocked:
            response = self._create_order_with_pickup_location(self.project)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(3, send_messages_mocked.call_count)
        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[0][0][0][0]
        )

        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[1][0][0][0]
        )

        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[2][0][0][0]
        )

    def test_order_change_state_to_printing_with_shipping_method_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.PRINTING,
            self._create_order_with_shipping_method,
            self.project
        )

    def test_order_change_state_to_cancelled_with_shipping_method_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.CANCELLED,
            self._create_order_with_shipping_method,
            self.project
        )

    def test_order_change_state_to_shipped_with_shipping_method_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.SHIPPED,
            self._create_order_with_shipping_method,
            self.project
        )

    def test_order_change_state_to_printing_with_pickup_location_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.PRINTING,
            self._create_order_with_pickup_location,
            self.project
        )

    def test_order_change_state_to_cancelled_with_pickup_location_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.CANCELLED,
            self._create_order_with_pickup_location,
            self.project
        )

    def test_order_change_state_to_shipped_with_pickup_location_email(self):
        self._test_order_change_state_email_with(
            OrderStatus.SHIPPED,
            self._create_order_with_pickup_location,
            self.project
        )

    def _test_order_change_state_email_with(
            self, status: str, order_creation_func: T_ORDER_CREATE_CB, *cb_args
    ):
        order_creation_func(*cb_args)
        order: Order = Order.objects.first()

        self.populate_current_request()

        with mock.patch(
                'django.core.mail.backends.locmem.EmailBackend.send_messages'
        ) as send_messages_mocked:
            order.set_status_with_email_message(
                status,
                'Test status',
            )

        self.assertEqual(2, send_messages_mocked.call_count)
        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[0][0][0][0]
        )

        self._assert_no_invalid_variables_in_email_message(
            send_messages_mocked.call_args_list[1][0][0][0]
        )

    def _submit_order(self, project, payload):
        return self.client.post(
            f'/api/v2.0/user-panel/projects/{project.id}/orders/',
            data=json.dumps(payload),
            content_type="application/json"
        )

    def _create_order_with_shipping_method(self, project):
        payload_shipping_method = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': self.shipping_method.id,
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 'plz deliver.'
            },
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': str(self.expected_payment_with_shipping),
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_shipping_method)

    def _create_order_with_pickup_location(self, project):
        payload_with_pickup_location = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': self.pickup_location.id,
            'shipping': None,
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': str(self.expected_payment_with_pickup),
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_with_pickup_location)

    def _create_order_with_currency(
        self,
        project,
        currency,
        authorized_amount
    ):
        payload_shipping_method = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': self.shipping_method.id,
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 'plz deliver.'
            },
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': authorized_amount,
                'currency': currency,
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_shipping_method)

    def _create_order_with_no_shipping_method(self, project):
        payload_no_shipping_method = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': None,
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': '224.16',
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_no_shipping_method)

    def _create_order_with_no_voucher(self, project):
        authorized_amount = str(
            (
                (D(LINE_PRICE + SHIPPING_PRICE))
                * (1 + self.partner.vat_rate/100)).quantize(D('0.00'))
        )
        payload_with_no_voucher = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': self.shipping_method.id,
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 'plz deliver'
            },
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': authorized_amount,
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': None,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_with_no_voucher)

    def _create_order_with_no_payment_method(self, project):
        payload_no_payment_method = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': self.shipping_method.id,
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 'plz deliver'
            },
            'payment': None,
            'voucherCode': None,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(project, payload_no_payment_method)

    def _create_order_with_no_project_lines(self, empty_project):
        payload = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': self.pickup_location.id,
            'shipping': None,
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': '256.04',
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }
        return self._submit_order(empty_project, payload)

    def _create_order_with_incorrect_fields(self, project):
        self.billing_address.id = 200
        self.voucher.code = 'ThisVoucherDoesNotExists'

        payload_incorrect_fields = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': 'ThisShouldBeAInteger',
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 12044
            },
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': '224.16',
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': self.voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
            }
        }

        return self._submit_order(project, payload_incorrect_fields)

    def _assert_no_invalid_variables_in_email_message(self, email_message):
        subject = email_message.subject
        body_txt = email_message.body
        body_html = email_message.alternatives[0][0]

        self.assertFalse('Invalid variable' in subject)
        self.assertFalse('Invalid variable' in body_txt)
        self.assertFalse('Invalid variable' in body_html)
