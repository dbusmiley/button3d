from apps.b3_tests.factories import BasketFactory, StockRecordFactory, \
    StlFileFactory, PostProcessingFactory, ColorFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class UserPanelProjectDetailTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        stock_record = StockRecordFactory()
        color = ColorFactory()
        self.project = BasketFactory(owner=self.user)
        self.item, created = self.project.add_product(
            product=stock_record.product,
            stockrecord=stock_record,
            stlfile=StlFileFactory(),
            post_processings=[{
                'post_processing': PostProcessingFactory(
                    colors=[color],
                    stock_record=stock_record
                ),
                'color': color
            }]
        )

    def test_detail(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.project.id}/'
        )
        self.assertEqual(response.status_code, 200)
        project = response.json()
        self.assertEqual(project['id'], self.project.id)
        self.assertEqual(len(project['lines']), 1)

        item = project['lines'][0]
        self.assertEqual(item['id'], self.item.id)
        self.assertEqual(item['name'], self.item.name)
        self.assertEqual(item['thumbnailUrl'], self.item.thumbnail_url)
        self.assertEqual(
            item['materialName'],
            {
                'en': self.item.product.title_en,
                'de': self.item.product.title_de,
            }
        )
        self.assertEqual(
            item['postProcessings'],
            [{
                'name': self.item.post_processings[0].title,
                'colorId': self.item.post_processing_options.first().color.id
            }]
        )
