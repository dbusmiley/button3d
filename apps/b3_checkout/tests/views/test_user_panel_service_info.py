from apps.b3_tests.factories import PartnerFactory, CountryFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class UserPanelServiceInfoTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        self.partner = PartnerFactory(
            email='email@example.com',
            website='http://www.example.com',
            bluebox_preview_step_en='This is the disclaimer',
            bluebox_preview_step_de='This is the disclaimer',
            terms_conditions_en='This is the terms',
            terms_conditions_de='This is the terms',
            add_org_and_address={'country': CountryFactory(alpha2='US')}
        )
        self.partner.address.city = 'Test City'
        self.partner.address.save()

    def test_user_panel_service_info_api(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/services/{self.partner.id}/')

        expected = {
            'city': 'Test City',
            'country': 'United States',
            'description': 'Test Partner Description',
            'email': 'email@example.com',
            'id': self.partner.id,
            'logo': None,
            'name': 'Test Partner',
            'phoneNumber': '+4917661569221',
            'website': 'http://www.example.com',
            'taxType': 'VAT',
            'disclaimer': {
                'en': 'This is the disclaimer',
                'de': 'This is the disclaimer'
            },
            'termsAndConditions': {
                'en': 'This is the terms',
                'de': 'This is the terms'
            },
            'supportedCurrencies': ['EUR']
        }
        self.assertEqual(expected, response.json())
