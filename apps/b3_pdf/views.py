from django.views import generic


class PdfPreviewView(generic.DetailView):
    pk_url_kwarg = ''
