from django.contrib import admin
from apps.b3_pdf.models import OrderNoteTemplate


@admin.register(OrderNoteTemplate)
class OrderNoteTemplateAdmin(admin.ModelAdmin):
    exclude = ['tpl_vars']
    list_display = ('template_name', 'organization', 'partner')
    list_filter = ('template_name',)

    class Meta:
        model = OrderNoteTemplate
