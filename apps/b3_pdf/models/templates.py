import typing as t

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.template import Template, Context
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.models.base import (BaseEditableTemplate,
                                      EditableTemplateManager)
from apps.b3_organization.models import Organization
from apps.b3_pdf.renderer.base import TemplateInterface
from apps.partner.models.partner import Partner
from button3d import type_declarations as td

__all__ = (
    'OrderNoteTemplate',
)
OptionalID = t.Union[int, str, None]


class OrderNoteTemplateManager(EditableTemplateManager):
    def get_template(self, template_name: str,
                     org_id: OptionalID = None,
                     partner_id: OptionalID = None) -> 'OrderNoteTemplate':
        """
        Returns the first relevant match for order notes

        :param template_name: Name of the template
        :param org_id: ID of the organization associated with the order
                       note.
        :param partner_id: ID of the service associated with the order
                           note.
        :return: The template to use to render this order note
        """
        filter_args = [Q(template_name=template_name)]
        if org_id and partner_id:
            filter_args.append(
                Q(organization__id=org_id) |
                Q(service__id=partner_id)
            )
        elif org_id:
            filter_args.append(Q(organization__id=org_id))
        elif partner_id:
            filter_args.append(Q(partner__id=partner_id))
        else:
            return super().get_template(template_name)

        template = self.get_queryset().filter(*filter_args).order_by(
            'sort_order'
        ).first()
        if template:
            return template

        return super().get_template(template_name)


class OrderNoteTemplate(TemplateInterface, BaseEditableTemplate):
    """
    A template for an order note

    Right now, the supported order notes are:

    - Order confirmation
    - Delivery note
    - Invoice
    - Cancelled invoice

    They are distinguished by their template name.
    """
    # Sticking this in here for future implementation: (MRS@20180729)
    # Right now I have no clue how to detect whether this is an
    # organization that wishes to hide it's partners. But when we find a way
    #  to do that, we can implement it using this sort order.
    ORG_EXCLUSIVE_SORT_ORDER = 5
    SERVICE_SORT_ORDER = 10
    ORG_SORT_ORDER = 20
    DEFAULT_SLUG = 'default'
    partner = models.ForeignKey(
        Partner, on_delete=models.CASCADE, related_name='templates', null=True,
        blank=True, verbose_name=_('service'),
        help_text=_('Template is specific for this partner.')
    )
    organization = models.ForeignKey(
        Organization, on_delete=models.CASCADE, related_name='templates',
        null=True, blank=True, verbose_name=_('organization'),
        help_text=_('Template is specific for this organization.')
    )
    objects = OrderNoteTemplateManager()

    def clean(self):
        if self.partner is not None and self.organization is not None:
            raise ValidationError(
                _('Template cannot belong to both organization and partner.')
            )

        super().clean()

    def get_sort_order(self) -> int:
        if self.partner is not None:
            return self.__class__.SERVICE_SORT_ORDER
        elif self.organization is not None:
            return self.__class__.ORG_SORT_ORDER
        else:
            return super().get_sort_order()

    def render(self, context: td.StringKeyDict):
        tpl = Template(self.template)
        ctx = Context(dict_=context)
        return tpl.render(ctx)

    class Meta:
        verbose_name = _('order note template')
        verbose_name_plural = _('order note templates')
