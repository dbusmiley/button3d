from .templates import OrderNoteTemplate

__all__ = (
    'OrderNoteTemplate',
)
