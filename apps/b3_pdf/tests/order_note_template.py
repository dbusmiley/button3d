from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.b3_pdf.models.templates import OrderNoteTemplate
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase

__all__ = (
    'OrderNoteTemplateTest',
)


class OrderNoteTemplateTest(TestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.organization.partners_enabled.add(self.partner)
        # Force resolving translation
        self.exception_msg = str(_('Template cannot belong to both '
                                   'organization and partner.'))

    def test_fallback_creation(self):
        actual = OrderNoteTemplate(
            template_name='pdf/invoice.html', template='Pay me now!'
        )
        actual.clean()
        actual.save()
        actual.refresh_from_db()
        self.assertEqual(actual.sort_order,
                         OrderNoteTemplate.FALLBACK_SORT_ORDER)

    def test_organization_template_creation(self):
        actual = OrderNoteTemplate(
            template_name='pdf/incoive.html', template='Pay me now!',
            organization=self.organization,
        )
        actual.clean()
        actual.save()
        actual.refresh_from_db()
        self.assertEqual(actual.sort_order, OrderNoteTemplate.ORG_SORT_ORDER)

    def test_partner_template_creation(self):
        actual = OrderNoteTemplate(
            template='Pay me now!', template_name='pdf/invoice.html',
            partner=self.partner
        )
        actual.clean()
        actual.save()
        actual.refresh_from_db()
        self.assertEqual(actual.sort_order,
                         OrderNoteTemplate.SERVICE_SORT_ORDER)

    def test_clean(self):
        bogus = OrderNoteTemplate(organization=self.organization,
                                  partner=self.partner)
        with self.assertRaisesMessage(ValidationError, self.exception_msg):
            bogus.clean()

    def test_render(self):
        context = {'logo': '3yourmind.jpg'}
        template = '<img src="{{logo}}" />'
        tpl = OrderNoteTemplate(template_name='pdf/invoice.html',
                                template=template)
        actual = tpl.render(context)
        expected = '<img src="3yourmind.jpg" />'
        self.assertEqual(actual, expected)
