import typing as t
from pathlib import Path

from apps.b3_pdf.renderer.base import PdfTemplateHandler, TemplateInterface
from django.test import SimpleTestCase
from apps.b3_core.models.base import WrappedTemplate
from apps.b3_pdf.renderer.weasyprint import PdfService

OUTPUT_DIR: Path = Path(__file__).parent / 'tmp'


class WeasyPrintTest(SimpleTestCase, PdfTemplateHandler):
    #: Toggle to inspect generated documents
    keep_pdfs: bool = False
    clean_files: t.List[Path] = []

    @classmethod
    def setUpClass(cls):
        cls.context = {'title': 'Hello World!'}
        if not OUTPUT_DIR.exists():
            OUTPUT_DIR.mkdir(parents=True)
        else:
            cls.clean_files.extend(list(OUTPUT_DIR.glob('*.pdf')))

        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        if not cls.keep_pdfs:
            for path in cls.clean_files:
                path.unlink()
            OUTPUT_DIR.rmdir()

    @classmethod
    def unlink(cls, path_obj: Path):
        cls.clean_files.append(path_obj)

    def get_template(self) -> TemplateInterface:
        return WrappedTemplate('<html><body><h1>{{title}}</h1></body></html>')

    def get_output_path(self) -> t.Optional[str]:
        return str(OUTPUT_DIR) if OUTPUT_DIR.exists() else None

    def test_pdf_creation(self):
        pdf = PdfService(self, context=self.context, encrypt=False)
        outfile, digest = pdf.save()

        actual = Path(OUTPUT_DIR / outfile)
        self.assertTrue(actual.exists())
        self.unlink(actual)
