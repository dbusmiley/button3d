from django.apps import AppConfig


class B3PdfConfig(AppConfig):
    name = 'b3_pdf'
