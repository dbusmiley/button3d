import hashlib
import logging
import os
import shutil
import tempfile
import typing as t
import io
import weasyprint

from button3d import type_declarations as td
from . import base

__all__ = (
    'WeasyResourceFetcher',
    'PdfService',
)

logger = logging.getLogger(__name__)

URLFetcher = t.Callable[[str], td.StringKeyDict]


def _get_hash(fh: t.BinaryIO) -> str:
    # Wrap the hashing to have predictable memory footprints
    fh.seek(0, io.SEEK_SET)
    m = hashlib.sha256()
    chunk = fh.read(4096)
    while chunk:
        m.update(chunk)
        chunk = fh.read(4096)

    return m.hexdigest()


class WeasyResourceFetcher(base.ResourceFetcher):
    def __call__(self, url: str) -> td.StringKeyDict:
        """
        Process the url and return the response and information about the
        response in a way that weasyprint understands.

        :param url: The url to fetch
        :returns: A dict with the following keys:

            * One of ``string`` (a byte string) or ``file_obj``
              (a file-like object)
            * Optionally: ``mime_type``, a MIME type extracted e.g. from a
              *Content-Type* header. If not provided, the type is guessed from
              the file extension in the URL.
            * Optionally: ``encoding``, a character encoding extracted e.g.
              from a *charset* parameter in a *Content-Type* header
            * Optionally: ``redirected_url``, the actual URL of the resource
              if there were e.g. HTTP redirects.
            * Optionally: ``filename``, the filename of the resource. Usually
              derived from the *filename* parameter in a *Content-Disposition*
              header

            If a ``file_obj`` key is given, it is the caller’s responsibility
            to call ``file_obj.close()``.

        """
        return weasyprint.default_url_fetcher(url)


class PdfService:
    def __init__(
            self, template_handler: base.PdfTemplateHandler,
            context: td.StringKeyDict,
            resource_fetcher: t.Optional[WeasyResourceFetcher] = None,
            base_url: t.Optional[str] = None, tmpdir: str = None):
        """
        A service that can render a template to a PDF file

        This service can render a template to a PDF file. The template is
        assumed to produce HTML and can contain references to external
        resources like images, fonts and css.

        The ``template_handler`` provides the template to be rendered
        and handles how the template engine should handle the variables listed
        in ``context``. Different template engines have different
        implementations to pass in these values, but ultimately, can be
        abstracted to a mapping. This handler is responsible for translating
        the mapping to whatever the template engine wants.

        The ``context`` is assumed to be a variable name to value mapping,
        referring to variables in the template. The values are in their
        native python representation.

        The provided ``base_url`` is used to resolve URL references. These
        references are fetched using ``resource_fetcher``. This fetcher is
        the top of the chain in several fetchers and should be the "best
        fit" for the common resource and/or environment. For example,
        when running inside Django, a static fetcher that deligates to a
        media fetcher is a sane approach and avoids network traffic.

        :param template_handler: The handler dealing with template specifics
        :param context: The variable context passed to the template handler
        :param resource_fetcher: Fetches external resources
        :param base_url: Base url used to resolve relative URLs
        :param tmpdir: Temporary directory to use for the unencrypted phase
        """

        self.template_handler = template_handler
        self.resource_fetcher = resource_fetcher or WeasyResourceFetcher()
        self.base_url = base_url
        self.context = context
        self.tmpdir = tmpdir

    def get_resource_fetcher(self) -> WeasyResourceFetcher:
        # Can be overridden to insert a different handler in the chain
        # without having to update all callers.
        return self.resource_fetcher

    def render(self) -> weasyprint.HTML:
        """
        Render the HTML template

        Can be used for previewing the result. It uses Weasyprint's
        rendering engine and should fit right into an iframe.
        Any resources that cannot be fetched will also generate errors here.

        :return: The formatted HTML
        """
        template = self.template_handler.get_template()
        result = template.render(self.context)
        resource_fetcher = self.get_resource_fetcher()
        return weasyprint.HTML(
            string=result, base_url=self.base_url,
            url_fetcher=resource_fetcher,
        )

    def _save_temporary(self) -> t.BinaryIO:
        document = self.render()

        file_obj = tempfile.TemporaryFile(dir=self.tmpdir)
        document.write_pdf(target=file_obj)
        file_obj.seek(0, io.SEEK_SET)

        return file_obj

    def generate_pdf_contents(self) -> t.Tuple[str, t.BinaryIO]:
        temp = self._save_temporary()
        digest = _get_hash(temp)
        temp.seek(0, io.SEEK_SET)
        return digest, temp

    def save(self) -> t.Tuple[str, str]:
        """
        Save the document.

        :returns: A 2-tuple containing the full path to the document and a
                  hash associated with that document.
        """
        path = self.template_handler.get_output_path()
        digest, temp = self.generate_pdf_contents()
        filename = digest + '.pdf'
        outfile = os.path.join(path, filename)
        temp.seek(0)
        with open(outfile, 'wb') as fh_out:
            shutil.copyfileobj(temp, fh_out)
            temp.close()

        return outfile, digest
