import typing as t

import requests

import button3d.type_declarations as td


class ResourceFetcher(object):
    """
    Fetch a given resource by URL and return information about it

    The information provided should make sense to the renderer used.
    """

    def __call__(self, url: str) -> t.Any:
        """
        Return the resource using the ``requests`` library. Very crude,
        but works 100% of the time if one uses absolute URLs.

        .. note::

            This method's return value is not strictly prototyped as it is
            dependent on the information the caller expects. Implementations
            should provide stricter signatures.

        :param url: The URL to fetch
        :return: The response of the URL, which by default is
                 :obj:`requests.Response`.
        """
        return requests.get(url)


class TemplateInterface:
    def render(self, context: td.StringKeyDict) -> str:
        """
        A template is an object that can render itself and provide different
        strings, pending on the provided context.

        :param context: A mapping of variable names to values.
        :return: A string that is the result of combining the context and
                 itself.
        """
        raise NotImplementedError('Subclasses must implement')


class ContextProvider:
    def get_context(self) -> td.StringKeyDict:
        """
        Get the context variables for this instance of the template

        :return: A dictionary mapping variable names to values
        """
        raise NotImplementedError('Subclasses must implement')


class PdfTemplateHandler:
    """
    Abstracts the Pdf rendering contract:

    The ``get_template()`` method should return a template that can be
    rendered using a "context". A context is a mapping of variable names to
    their values. Variable names can only consist of underscores, letters
    and numbers and may only start with a letter. This method must always
    return an instance that adheres to the :class:`TemplateInterface`.

    The ``get_output_path()`` method should return an absolute file system
    path to a directory that is assumed to be writable. The PDF output or
    it's encrypted version will be put at that location. If this method
    returns ``None`` the file will be put in a location configured in the
    Django settings.

    The ``get_encryption_service()`` method should return an instance of a file
    encryption service or ``None`` if encryption is not requested.
    """

    def get_template(self) -> TemplateInterface:
        """
        Provide the template this PDF file should be rendered with

        :return: A template adhering to the template interface.
        """
        raise NotImplementedError('Subclasses must implement')

    def get_output_path(self) -> t.Optional[str]:
        """
        Provide the path where a PDF should be filed

        :return: Absolute path to a directory
        """
        raise NotImplementedError('Subclasses must implement')
