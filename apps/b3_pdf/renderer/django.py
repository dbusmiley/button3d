import logging
import mimetypes
import typing as t

import requests
import tempfile
import io

from django.core.exceptions import SuspiciousFileOperation
from django.core.files.storage import Storage
from urlobject import URLObject
from .weasyprint import WeasyResourceFetcher

from .base import ResourceFetcher

logger = logging.getLogger(__name__)
__all__ = (
    'BaseDjangoResourceFetcher',
    'DjangoStaticFetcher',
    'DjangoMediaFetcher',
)
FileInfo = t.Dict[str, t.Union[t.BinaryIO, str]]
"""
Provides a base class and two implementations that bypass the network to
serve files that are implemented using Django's Storage system:

- :class:`DjangoStaticFetcher` uses the staticfiles storage
- :class:`DjangoMediaFetcher` uses the uploaded files storage

Temporarily it also provides a fetcher that downloads thumbnails. We should
be able to get rid of this one and pass it on to weasyprint's own fetcher.
"""


class BaseDjangoResourceFetcher(ResourceFetcher):
    __doc__ = """
    Base class to map a url to a :class:`Storage` container's contents.

    The ``base_url`` is mapped to the storage class's location. This base
    class does not have a storage or base_url set, and as such is abstract,
    but all other methods are fully working once they are set.
    Storages where :func:`mimetypes.guess_mimetype` does not work reliably,
    should reimplement :meth:`guess_mimetype`.
    """
    #: The fallback mimetype if the type cannot be identified
    default_mimetype: str = 'text/plain'
    #: The fallback encoding if the encoding cannot be idenntified
    default_encoding: str = 'utf-8'
    next_fetcher: t.Callable[[str], t.Any] = WeasyResourceFetcher()
    #: Storage handler
    storage: Storage = None
    #: Base URL for the given storage engine.
    base_url: str = None
    #: The subclass wants to provide the file information itself, instead of
    #: using the guided API here. Typically, this means the file is remote
    #: or needs a transformation not handled by the standard API.
    provides_file_info = False
    #: Message to send when the file is not found
    file_not_found_msg = 'File not found: {path}'

    def clean_url(self, url: str) -> t.Optional[str]:
        """
        Validates if the given ``url`` starts with our base url or if it's
        path component does. If it does not we return ``None``, signaling
        that this URL is not one we should handle.
        """
        if url.startswith(self.base_url):
            return url

        url_path = URLObject(url).path  # type: str
        if url_path.startswith(self.base_url):
            return url_path

        return None

    def guess_mimetype(self, path: str) -> t.Tuple[str, str]:
        """
        Guesses the mimetype of a given file

        This is different from `mimetypes.guess_type` in that it always
        returns a type.

        :param path: Path to the file
        :returns: A 2-tuple containing:

            - The mimetype of the file
            - The encoding of the file

        """
        mt, encoding = mimetypes.guess_type(path)
        return mt or self.default_mimetype, encoding or self.default_encoding

    def get_file_path(self, url: str) -> t.Union[str, None]:
        """
        Returns the path to the file based on the cleaned url.

        This method can be used to download a file and save it somewhere,
        eventually a path must be returned.
        If this ResourceFetcher decides not to handle this file, it shall
        return None.
        """

        try:
            path = self.storage.path(url[len(self.base_url):])
        except SuspiciousFileOperation as exc:
            logger.exception(exc.args[0])
            return None
        logger.debug(f'Path resolved to {path}')

        if not self.storage.exists(path):
            msg = self.file_not_found_msg.format(path=path)
            logger.error(msg)
            raise FileNotFoundError(msg)

        return path

    def get_file_info(self, cleaned_url: str, orig_url: str) -> FileInfo:
        if self.provides_file_info:
            raise NotImplementedError('Subclasses must implement')

        raise TypeError('Should not be called')

    def __call__(self, url: str) -> FileInfo:
        """
        Process the url and return the response and information about the
        response in a way that weasyprint understands.

        This method drives the methods in following order:
        1. :meth:`clean_url`
        2. :meth:`get_file_path` or :meth:`get_file_info`
        3. :meth:`guess_mimetype`

        ``get_file_info`` is used when :attr:`uses_temporary_file` is used.

        :returns: A dict with the following keys:

            - ``file_obj`` (a file-like object). It is the caller’s
              responsibility to call ``file_obj.close()``.
            - ``mime_type``, a MIME type extracted e.g. from a
              *Content-Type* header. If not provided, the type is guessed from
              the file extension in the URL.
            - ``encoding``, a character encoding guessed by the `mimetype`
              module.

        """
        clsname = self.__class__.__name__
        logger.debug(f'{clsname} is fetching url: {url}')
        cleaned_url = self.clean_url(url)
        if cleaned_url is None:
            logger.info(f'{clsname}: passing {url} to next fetcher')
            return self.next_fetcher(url)

        if self.provides_file_info:
            return self.get_file_info(cleaned_url, url)

        path = self.get_file_path(cleaned_url)
        if path is None:
            return self.next_fetcher(url)

        mt, enc = self.guess_mimetype(path)
        logger.info(f'Returning {mt} using "{enc}" for {path}')
        return {
            'file_obj': open(path, 'rb'),
            'encoding': enc,
            'mime_type': mt,
        }


class DjangoStaticFetcher(BaseDjangoResourceFetcher):
    """
    Fetches resources that are in Django's static file storage

    This fetcher bypasses the network, by accessing the corresponing local
    storage for these files. If the passed in URL does not start with the
    configured ``STATIC_URL`` the url is passed on to the parent.
    """

    def __init__(self, next_fetcher: ResourceFetcher=None):
        # Keep this import here to prevent accessing Django's settings
        # before they are configured.
        from django.contrib.staticfiles.storage import staticfiles_storage
        self.storage = staticfiles_storage
        self.base_url = staticfiles_storage.base_url
        if not next_fetcher:
            self.next_fetcher = DjangoMediaFetcher()
        else:
            self.next_fetcher = next_fetcher
        self.file_not_found_msg = 'Missing static file: {path}'


class DjangoMediaFetcher(BaseDjangoResourceFetcher):
    """
    Similar to static fetcher except grabs from the media root
    """

    def __init__(self, next_fetcher: ResourceFetcher=None):
        from django.core.files.storage import FileSystemStorage

        self.storage: FileSystemStorage = FileSystemStorage()
        self.base_url = self.storage.base_url
        self.file_not_found_msg = 'Missing media file: {path}'
        if not next_fetcher:
            self.next_fetcher = Backend3dImageFetcher()
        else:
            self.next_fetcher = next_fetcher


class Backend3dImageFetcher(BaseDjangoResourceFetcher):
    """
    Convert the application/octet-stream mimetype to image/png

    This is needed temporarily till SPRI-678 is merged.
    """
    provides_file_info = True

    def __init__(self, next_fetcher: ResourceFetcher=None):
        from django.conf import settings

        backend3d = URLObject(settings.BACKEND_3D_HOST)
        self.base_url = str(backend3d.with_path('download'))

        if next_fetcher:
            self.next_fetcher = next_fetcher

    def get_file_info(self, cleaned_url: str, orig_url: str) -> FileInfo:
        """
        Download the thumbnail from 3DBackend and provide it's information

        :param cleaned_url: url to fetch
        :param orig_url: original url to pass onto
        :returns: A dict with the following keys:

            - ``file_obj`` (a file-like object). This is the downloaded
            thumbnail.
            - ``mime_type``, a MIME type extracted e.g. from a
              *Content-Type* header. If not provided, the type is guessed from
              the file extension in the URL.
            - ``encoding``, a character encoding guessed by the `mimetype`
              module.
        """
        response = requests.get(cleaned_url)
        if not response.ok:
            return self.next_fetcher(orig_url)

        thumbnail = tempfile.TemporaryFile()
        thumbnail.write(response.content)
        response.close()
        thumbnail.seek(io.SEEK_SET, 0)
        return {
            'file_obj': thumbnail,
            'mime_type': 'image/png',
            'encoding': 'utf-8',
        }
