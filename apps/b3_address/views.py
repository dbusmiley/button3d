from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from apps.b3_address.serializers import AddressSerializer
from apps.b3_address.models import Address


class AddressViewSet(ModelViewSet):
    serializer_class = AddressSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return Address.objects.filter(user=self.request.user)
