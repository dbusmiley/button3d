from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_address.models import Address


class AddressSerializer(PayloadConverterMixin, serializers.ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = Address
        fields = (
            'id',
            'user',

            'title',
            'first_name',
            'last_name',

            'company_name',
            'vat_id',
            'department',

            'line1',
            'line2',

            'city',
            'zip_code',
            'state',
            'country',
            'phone_number',

            'is_default',
        )
