from django.contrib.auth.models import User

import factory

from apps.b3_address.models import Address, Country, DefaultUserAddress


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country
        exclude = ('_countries', '_current_country')
        django_get_or_create = ('alpha2',)

    _countries = {
        'DE': {'alpha3': 'DEU', 'num': '276', 'name': 'Germany'},
        'US': {'alpha3': 'USA', 'num': '840', 'name': 'United States'},
        'ES': {'alpha3': 'ESP', 'num': '724', 'name': 'Spain'},
        'EG': {'alpha3': 'EGY', 'num': '818', 'name': 'Egypt'},
        'IT': {'alpha3': 'ITA', 'num': '380', 'name': 'Italy'},
        'ET': {'alpha3': 'ETH', 'num': '231', 'name': 'Ethiopia'},
        'SD': {'alpha3': 'SDN', 'num': '729', 'name': 'Sudan'},
        'JP': {'alpha3': 'JPN', 'num': '392', 'name': 'Japan'},
        'NL': {'alpha3': 'NLD', 'num': '528', 'name': 'Netherlands'},
    }

    alpha2 = factory.Iterator(['DE', 'ES', 'EG', 'IT', 'ET', 'SD', 'JP', 'NL'])

    alpha3 = ""
    numeric = ""
    name = ""
    official_name = ""

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        try:
            obj = super()._create(model_class, *args, **kwargs)
            obj.alpha3 = cls._countries[obj.alpha2]['alpha3']
            obj.numeric = cls._countries[obj.alpha2]['num']
            obj.name = cls._countries[obj.alpha2]['name']
            obj.official_name = cls._countries[obj.alpha2]['name']
        except KeyError:
            raise KeyError("Please select a alpha2 from the list in the"
                           "CountryFactory for testing.")

        obj.save()
        return obj


class AddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Address

    user = factory.LazyFunction(User.objects.order_by('?').first)

    title = factory.Iterator([
        'Dr',
        'Herr',
        'Frau'
    ])
    first_name = factory.Iterator([
        'Jakob',
        'Nader',
        'Serhii',
        'Justin',
        'Felix',
        'Simone',
        'Mattu',
        'Georg',
    ])
    last_name = factory.Iterator([
        'Jakob',
        'Nader',
        'Serhii',
        'Justin',
        'Felix',
        'Simone',
        'Mattu',
        'Georg',
    ])

    company_name = '3YOURMIND'
    vat_id = 'AS-123-R'
    department = factory.Iterator([
        'Sales',
        'HR',
        'Marketing',
        'Development',
        'Advanced Engineering'
    ])
    line1 = 'Bismerkstr. 12-14'
    line2 = '2 G.'
    zip_code = '10625'
    state = 'Berlin'
    city = 'Berlin'
    country = factory.SubFactory(CountryFactory, alpha2='DE')

    phone_number = '+4917661569221'

    verified_status = factory.Iterator(
        [status for status, _ in Address.ADDRESS_VERIFIED_STATUSES])


class DefaultUserAddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DefaultUserAddress

    address = factory.LazyFunction(Address.objects.order_by('?').first)
