from django.contrib import admin

from apps.b3_address.models import Country, Address


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('alpha2',)


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    raw_id_fields = ('user', 'partner', 'country')
    list_display = (
        'full_address',
        'user',
        'partner',
        'verified_status',
        'is_default_user_address',
    )

    def is_default_user_address(self, address):
        if address.is_default:
            return 'yes'
        else:
            return ''

    def name(self, address):
        return f'{address.first_name} {address.last_name}'

    def full_address(self, address):
        return address.active_address_fields()
