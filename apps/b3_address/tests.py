import copy
import json

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.models.fields import NOT_PROVIDED as FIELD_DEFAULT_NOT_PROVIDED
from rest_framework.reverse import reverse

from apps.b3_address.serializers import AddressSerializer
from apps.b3_api.utils.helpers import dict_from_class
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import UserFactory
from apps.b3_address.factories import (
    AddressFactory,
    CountryFactory,
    DefaultUserAddressFactory)
from apps.b3_address.models import (
    Country, DefaultUserAddress,
    Address)
from apps.b3_tests.factories.partner import PartnerFactory


class AddressTests(TestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.country = CountryFactory(alpha2='EG')
        self.address = AddressFactory(
            user=self.user,
            country=self.country,
        )
        self.address_default = AddressFactory(
            user=self.user,
            country=self.country
        )
        DefaultUserAddressFactory(address=self.address_default)

        self.country_hard_coded = CountryFactory(
            alpha2='DE')
        self.address_hard_coded = AddressFactory(
            title='Dr',
            first_name='Foo',
            last_name='Bar',
            company_name='3YOURMIND',
            vat_id='AX12',
            department='Sales',
            line1='Bismarkstr 10-12',
            line2='',
            zip_code='10625',
            state='Berlin',
            city='Berlin',
            country=self.country_hard_coded,
            phone_number='+493055578748',
        )

    def test_str(self):
        """Tests string representation"""
        self.assertEqual(str(self.address), self.address.summary)

    def test_validate_partner_or_user_only(self):
        """Assert that either partner or user is set but not both
        """
        self.address.partner = PartnerFactory(address=None)

        self.assertRaises(ValidationError, self.address.save)

    def test_active_address_fields(self):
        """Tests :function: `active_address_fields()`"""
        active_fields = [
            'Dr Foo Bar', '3YOURMIND', 'AX12', 'Sales',
            'Bismarkstr 10-12', '10625', 'Berlin', 'Berlin',
            'Germany', '+493055578748']
        self.assertEqual(
            self.address_hard_coded.active_address_fields(), active_fields)

    def test_summary(self):
        """Tests :function: `summary()`"""
        summary = (
            'Dr Foo Bar, 3YOURMIND, AX12, Sales, ' +
            'Bismarkstr 10-12, 10625, Berlin, Berlin,' +
            ' Germany, +493055578748')
        self.assertEqual(self.address_hard_coded.summary, summary)

    def test_salutation(self):
        """Tests :function: `salutation()`"""
        self.assertEqual(self.address_hard_coded.salutation, 'Dr Foo Bar')

    def test_is_default(self):
        """Tests :property: `is_default`"""
        self.assertFalse(self.address.is_default)
        self.assertTrue(self.address_default.is_default)


class CountryTests(TestCase):
    def setUp(self):
        super().setUp()
        self.country = CountryFactory(alpha2='DE')

    def tearDown(self):
        Country.objects.all().delete()

    def test_str(self):
        """Tests string representation"""
        self.assertEqual(str(self.country), 'Germany')


class DefaultAddressTests(TestCase):
    def setUp(self):
        super().setUp()
        self.user = UserFactory()
        self.country = CountryFactory(alpha2='EG')
        self.address = AddressFactory(
            user=self.user,
            country=self.country,
        )
        self.address_default = AddressFactory(
            user=self.user,
            country=self.country
        )
        self.default_user_address = DefaultUserAddressFactory(
            address=self.address_default)

    def test_delete_default_address_for_user(self):
        default_address = DefaultUserAddressFactory.build(
            address=self.address)
        default_address.delete_default_address_for_user()

        self.assertRaises(
            ObjectDoesNotExist,
            DefaultUserAddress.objects.get,
            pk=self.default_user_address.pk)

    def test_save(self):
        DefaultUserAddressFactory(address=self.address)
        self.assertRaises(
            ObjectDoesNotExist,
            DefaultUserAddress.objects.get,
            pk=self.default_user_address.pk)


class AddressViewSetTests(TestCase):
    def setUp(self):
        super().setUp()

        country = CountryFactory()

        self.password = 'super secret password'
        self.user = UserFactory(password=self.password)
        self.address = AddressFactory(user=self.user)

        self.address_new_dict = dict_from_class(
            AddressFactory.build())
        pop_keys = ['_user_cache', '_state', 'id', 'user_id']
        for key in pop_keys:
            self.address_new_dict.pop(key)
        self.address_new_dict['country'] = country.pk
        self.address_new_dict['phone_number'] = '+49 30 55578748'

        self.user.set_password(self.password)

        self.list_url = reverse('api_urls:address-list')
        self.detail_url = reverse(
            'api_urls:address-detail',
            (self.address.id, )
        )

    def tearDown(self):
        super().tearDown()
        User.objects.all().delete()
        Address.objects.all().delete()

    def test_list_anonymous(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 403)

    def test_list_authenticated(self):
        self.client.login(email=self.user.email, password=self.password)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, 200)

        actual = json.loads(response.content.decode('utf-8'))
        expected = AddressSerializer(
            Address.objects.filter(user=self.user), many=True).data

        self.assertEqual(actual, expected)

    def test_create_anonymous(self):
        response = self.client.post(self.list_url, data=self.address_new_dict)
        self.assertEqual(response.status_code, 403)

    def test_create_authenticated(self):
        addresses_count_old = Address.objects.filter(user=self.user).count()
        self.client.login(email=self.user.email, password=self.password)
        response = self.client.post(self.list_url, data=self.address_new_dict)
        self.assertEqual(response.status_code, 201)

        addresses_count_new = Address.objects.filter(user=self.user).count()
        self.assertEqual(addresses_count_old+1, addresses_count_new)

    def test_create_authenticated_missing_param(self):
        self.client.login(email=self.user.email, password=self.password)

        fields = Address._meta.get_fields()
        excluded_fields = ['hash']
        required_fields = []

        for f in fields:
            if (
                hasattr(f, 'blank') and
                f.blank is False and
                f.name not in excluded_fields and
                f.default == FIELD_DEFAULT_NOT_PROVIDED
            ):
                required_fields.append(f)

        for f in required_fields:
            payload = copy.copy(self.address_new_dict)
            payload.pop(f.name)
            response = self.client.post(self.list_url, data=payload)
            self.assertEqual(response.status_code, 400)

    def test_update(self):
        self.client.login(email=self.user.email, password=self.password)
        detail_response = self.client.get(self.detail_url)
        self.assertEqual(detail_response.status_code, 200)

        update_dict = json.loads(detail_response.content.decode('utf-8'))
        update_dict['first_name'] = 'Horst'
        update_response = self.client.put(
            self.detail_url,
            data=json.dumps(update_dict),
            content_type='application/json'
        )
        self.assertEqual(update_response.status_code, 200)

        self.address.refresh_from_db()
        self.assertEqual(self.address.first_name, 'Horst')

    def test_delete(self):
        self.assertEqual(Address.objects.filter(user=self.user).count(), 1)
        self.client.login(email=self.user.email, password=self.password)
        self.client.delete(self.detail_url)
        self.assertEqual(Address.objects.filter(user=self.user).count(), 0)

    def test_country_factory(self):
        for i in range(1, 20):
            country = CountryFactory()
            str(country)
            country.save()
