from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

import button3d.type_declarations as td
from apps.b3_address.verification_mails import send_verify_address,\
    send_address_verified
from apps.b3_core.fields import NonUniqueOneToOneField

from apps.b3_core.models import AbstractDeletedDateModel
from apps.partner.models.partner import Partner
from apps.b3_migration.model_descriptors import (
    country_descriptor,
    oscar_country_descriptor,
)


class Country(models.Model):
    """
    International Organization for Standardization
    (ISO) 3166-1 Country representation.

    Fields' comments:
        name:
            The commonly used name; e.g. 'United Kingdom'
        official_name:
            The full official name of a country
            e.g. 'United Kingdom of Great Britain and Northern Ireland'
    """

    alpha2 = models.CharField(
        _('ISO 3166-1 alpha-2'), max_length=2, primary_key=True
    )
    alpha3 = models.CharField(
        _('ISO 3166-1 alpha-3'), max_length=3, blank=True
    )
    numeric = models.CharField(
        _('ISO 3166-1 numeric'), blank=True, max_length=3
    )

    name = models.CharField(_('Country name'), max_length=128)

    official_name = models.CharField(_('Official name'), max_length=128)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')
        ordering = ('alpha2',)

    @property
    def is_eu_country(self):
        return self.alpha2 in [
            'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR',
            'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL',
            'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
        ]

    @property
    def is_germany(self):
        return self.alpha2 == 'DE'

    def __str__(self):
        return f'{self.name}'

    def get_source_and_target_descriptors(self):
        """
        :return source descriptor, target descriptor:
        """
        return country_descriptor, oscar_country_descriptor

    def exists_in_db(self):
        """
        Checks if `self` is already in database. This is used with the
        AutoSyncMixin for checking on save whether it is an update or a create
        """
        return Country.objects.filter(pk=self.pk).exists()


class Address(AbstractDeletedDateModel):
    """
    Address representation.
    -- Used for Shipping, Billing, and Orders' Shipping and Billing

    Field comments:
        user:
            if `user` is null, this address belongs to an order
            and is read-only, which is enforced in `self.save()`
        line1:
            Street address
        line2:
            Extra street address information
        date_created:
            Used for ordering (Not requesting-to-buy but rather as in
            sequence/arrangment of instances)

    PRECAUTIONS:
        > Changes to fields - consider the following:
            1. :function: `self.active_address_fields()`
            2. :function: `self._ensure_defaults_integrity()`
            3. :function: `self.save()`
            4. :property: `self.salutation`
            5. :function: `_validate_partner_or_user_only()`
        > Adding functions/properties that use hard-coded fields:
            1. Add functions/properties to list of PRECAUTIONS - changes
                to fields
    """
    ADDRESS_PENDING = 'pending'
    ADDRESS_VERIFIED = 'verified'
    ADDRESS_DECLINED = 'declined'
    ADDRESS_VERIFIED_STATUSES = (
        (ADDRESS_PENDING, _('Pending')),
        (ADDRESS_VERIFIED, _('Verified')),
        (ADDRESS_DECLINED, _('Declined')),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='user_address',
        verbose_name=_('User'),
        null=True,
        blank=True
    )

    partner = NonUniqueOneToOneField(
        Partner,
        on_delete=models.CASCADE,
        related_name='address',
        null=True,
        blank=True,
    )

    title = models.CharField(_('Title'), max_length=64, blank=True)
    first_name = models.CharField(_('First name'), max_length=255)
    last_name = models.CharField(_('Last name'), max_length=255)

    company_name = models.CharField(
        _('Company Name'), max_length=200, blank=True
    )
    vat_id = models.CharField(_('VAT ID'), max_length=200, blank=True)
    department = models.CharField(_('Department'), max_length=200, blank=True)

    line1 = models.CharField(_('First line of address'), max_length=255)
    line2 = models.CharField(
        _('Second line of address'), max_length=255, blank=True)
    zip_code = models.CharField(
        _('Post/Zip-code'), max_length=64
    )
    city = models.CharField(_('City'), max_length=255)
    state = models.CharField(_('State/County'), max_length=255, blank=True)
    country = models.ForeignKey(
        Country,
        on_delete=models.PROTECT,
        verbose_name=_('Country'))

    phone_number = models.CharField(
        _('Phone number'),
        max_length=128,
        help_text=_('In case we need to call you about your order'))

    verified_status = models.CharField(
        _('Verified status'),
        choices=ADDRESS_VERIFIED_STATUSES,
        default=ADDRESS_PENDING,
        max_length=30)

    date_created = models.DateTimeField(_('Date Created'), auto_now_add=True)

    class Meta:
        ordering = ['-date_created']
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')

    def __str__(self):
        return self.summary

    def _should_send_verification_emails(self):
        if not self.user:
            return False

        if self.user.user_address.filter(
                verified_status=Address.ADDRESS_VERIFIED).exists():
            return False

        return True

    def save(self, *args, **kwargs):
        if self.deleted_date:
            return super().save(*args, **kwargs)

        self._validate_address_usage()

        if self._should_send_verification_emails():
            send_address_verified(self)
            send_verify_address(self)

        super().save(*args, **kwargs)

    def get_source_and_target_descriptors(self):
        from apps.b3_migration.model_descriptors import (
            address_descriptor,
            shipping_address_descriptor,
            billing_address_descriptor,
            user_address_descriptor,
            partner_address_descriptor,
        )
        source_descriptor = address_descriptor
        if self.user:
            target_descriptor = user_address_descriptor
        elif self.partner:
            target_descriptor = partner_address_descriptor
        elif self.as_billing_address_on_orders:
            target_descriptor = billing_address_descriptor
        else:
            target_descriptor = shipping_address_descriptor

        return source_descriptor, target_descriptor

    def _validate_address_usage(self):
        """
        Validate that the address is not set to both a partner and a user
        """
        is_shipping_or_billing_address = \
            self.as_billing_address_on_orders.exists() \
            or self.as_shipping_address_on_orders.exists()

        if is_shipping_or_billing_address and (self.user or self.partner):
            raise ValidationError({
                '__all__': [
                    'Address that is already assigned to an order can not be '
                    'assigned to a user or a service.'
                ]
            })
        if self.user and self.partner:
            raise ValidationError({
                '__all__': [
                    'Address cannot be assigned to both a user and a service.'
                ]
            })

    def active_address_fields(self):
        """
        Return the non-empty components of the address
        """
        fields = [
            self.salutation,
            self.company_name,
            self.vat_id,
            self.department,
            self.line1,
            self.line2,
            self.zip_code,
            self.city,
            self.state,
            self.country.name,
            str(self.phone_number)]
        fields = [f.strip() for f in fields if f.strip()]
        return fields

    @property
    def summary(self):
        """
        Returns a single string summary of the address,
        separating fields using commas.
        """
        return ', '.join(self.active_address_fields())

    @property
    def salutation(self):
        """
        Full name (including title)
        """
        if self.title:
            fields = [self.title]
        else:
            fields = []
        fields += [
            self.first_name,
            self.last_name
        ]
        return ' '.join(fields)

    @property
    def is_default(self):
        """
        Checks if current address is default for :field: self.user
        """
        return DefaultUserAddress.objects.filter(address=self).exists()

    def clone(self) -> td.Address:
        """
        Clones the address, removing any references to user or partner.
        The cloned address can e.g. be attached to an order.
        """
        cloned_address = Address.objects.get(pk=self.pk)
        cloned_address.id = None
        cloned_address.user = None
        cloned_address.partner = None
        cloned_address.save()
        return cloned_address


class DefaultUserAddress(models.Model):
    """
    Default User Address representation.

    This is decoupled from the :model: `Address` because not all instances
    of :model: `Address` belong to a user, and thus some cannot be a default
    user address
    """
    address = models.OneToOneField(
        Address,
        on_delete=models.CASCADE
    )

    def delete_default_address_for_user(self):
        """
        Remove all instances of :model: `DefaultUserAddress`
        for :field: self.address.user
        -- Used before saving a new instance of :model: DefaultUserAddress
            to assert a single default address per user
        """
        user = self.address.user
        DefaultUserAddress.objects.filter(address__user=user).delete()

    def save(self, *args, **kwargs):
        """
        Delete default address for user :field: `self.address.user`
        before saving a new default address.
        -- Does not check if one already exists, since there is no need
            for that
        """
        self.delete_default_address_for_user()
        super().save(*args, **kwargs)
