from django.urls import reverse

from apps.b3_core.mail import EmailSender
from apps.b3_organization.utils import get_current_site, get_current_org, \
    get_base_url


def update_email_verifiers_on_pending_action(org, user_id):
    """
    send emails to all verifiers on pending actions
    :param org:
    :param user_id:
    :return:
    """
    to_emails = [u.email for u in org.verify_customer_address_users.all()]
    if not to_emails:
        return

    context = {
        'user_id': user_id,
        'verify_url': get_base_url() + reverse(
            'b3_user_panel:pending-verifications'
        )
    }
    email_sender = EmailSender()
    email_sender.send_email(
        to=to_emails,
        subject_template_path='address/emails/'
                              'email_verification_pending_subject.txt',
        body_txt_template_path='address/emails/'
                               'email_verification_pending_body.txt',
        body_html_template_path='address/emails/'
                                'email_verification_pending_body.html',
        extra_context=context,
    )


def update_email_verifiers_on_action_done(org, user_id, verified_by=None):
    """
    Send emails to all other controllers
    :param org:
    :param user_id:
    :param verified_by:
    :return:
    """
    all_controllers = org.verify_customer_address_users.all().exclude(
        id=verified_by.id
    )
    to_emails = [user.email for user in all_controllers]

    if not to_emails:
        return

    context = {
        'user_id': user_id,
        'verifying_username': verified_by.email if verified_by else None
    }

    email_sender = EmailSender()
    email_sender.send_email(
        to=to_emails,
        subject_template_path='address/emails/'
                              'email_verification_done_subject.txt',
        body_txt_template_path='address/emails/'
                               'email_verification_done_body.txt',
        body_html_template_path='address/emails/'
                                'email_verification_done_body.html',
        extra_context=context,
    )


def send_verify_address(instance):
    """
    @TODO: This logic should stay closer to the models
    :param instance:
    :return:
    """
    from apps.b3_address.models import Address
    if not get_current_site():
        # If the user is imported from a fixture (sample data),
        #   don't care about sending verification mails
        return

    org = get_current_org()

    if not org.force_verify_customer_address:
        return

    if instance.pk:
        original = Address.objects.get(pk=instance.pk)
        if original.verified_status == instance.verified_status:
            return

    if instance.verified_status == Address.ADDRESS_PENDING:
        update_email_verifiers_on_pending_action(
            org=org, user_id=instance.user.id
        )

    return


def send_address_verified(instance):
    """
    Send out verification done emails. Unfortunately now triggered on all
    save() called on the Address model.
    @TODO: This logic should stay closer to the models
    """
    from apps.b3_address.models import Address
    if not get_current_site():
        # If the user is imported from a fixture (sample data),
        #   don't care about sending verification mails
        return

    org = get_current_org()
    if not org.force_verify_customer_address:
        return

    if instance.pk:
        original = Address.objects.get(pk=instance.pk)
        if original.verified_status == instance.verified_status:
            return

    if not instance.verified_status == Address.ADDRESS_VERIFIED:
        return

    to_email = instance.user.email
    home_url = get_base_url() + reverse('home')

    context = {
        "home_url": home_url,
        "user": instance.user,
    }

    email_sender = EmailSender()
    email_sender.send_email(
        to=to_email,
        subject_template_path='address/emails/'
                              'email_address_verified_subject.txt',
        body_txt_template_path='address/emails/'
                               'email_address_verified_body.txt',
        body_html_template_path='address/emails/'
                                'email_address_verified_body.html',
        extra_context=context,
    )
