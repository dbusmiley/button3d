"""TODO: NEW_USER_PANEL. Remove this"""

from django import forms
from django.forms import HiddenInput

from apps.b3_address.models import Address

from apps.b3_organization.utils import get_current_org


class UserPanelAddressForm(forms.ModelForm):
    REQUIRED_FIELDS = [
        'first_name', 'last_name', 'line1', 'city', 'postcode', 'country'
    ]

    def __init__(self, *args, **kwargs):
        super(UserPanelAddressForm, self).__init__(*args, **kwargs)

        org = get_current_org()
        if org.company_information_visibility == org.REQUIRED:
            self.REQUIRED_FIELDS += ['company_name', 'vat_id']
        field_names = (set(self.fields) &
                       set(self.REQUIRED_FIELDS))
        for field_name in field_names:
            self.fields[field_name].required = True

        if org.company_information_visibility == org.HIDDEN:
            self.fields['company_name'].widget = HiddenInput()
            self.fields['vat_id'].widget = HiddenInput()
            self.fields['department'].widget = HiddenInput()

        self.fields['vat_id'].label = org.vat_id_label

    def clean(self):
        cleaned_data = super(UserPanelAddressForm, self).clean()
        self.check_state_requirements(cleaned_data)
        return cleaned_data

    @staticmethod
    def check_state_requirements(cleaned_data):
        # Customers from the USA must supply a state
        country = cleaned_data['country']
        no_state_supplied = not cleaned_data['state']
        state_is_required = country.alpha3 == "USA"
        if state_is_required and no_state_supplied:
            raise forms.ValidationError(
                {"state": 'State field is mandatory for US Customers'}
            )

    class Meta:
        model = Address
        fields = [
            'title', 'first_name', 'last_name', 'company_name',
            'vat_id', 'department', 'line1', 'line2', 'zip_code', 'state',
            'city', 'country', 'phone_number'
        ]
        exclude = ['user']
