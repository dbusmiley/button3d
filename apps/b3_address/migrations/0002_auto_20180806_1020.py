# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-06 10:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_address', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='hash',
            field=models.CharField(db_index=True, editable=False, max_length=255, null=True, verbose_name='Address Hash'),
        ),
    ]
