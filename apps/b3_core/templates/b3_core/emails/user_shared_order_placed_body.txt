{% extends "customer/emails/base.txt" %}
{% block body %}
{% load currency_tags i18n organization_flags %}

{% trans 'Hello ' %} {{ name }},
{% trans 'Thank you for your order.' %}


{% if not order.shipping_address.guest_email_address %}
{% url 'b3_user_panel:project-detail' order.project.id as order_link %}
{% blocktrans %}You can follow the status of your order <a href="{{ base_url }}{{ order_link }}">here</a>.{% endblocktrans %}
{% endif %}

{% trans 'Please take a moment to look at the details below' %}:

{% include "b3_core/emails/include/order_details.txt" %}
{% endblock %}
