import logging

from django.http import JsonResponse

import button3d.type_declarations as td

logger = logging.getLogger(__name__)


def change_site_configuration(request: td.HttpRequest) -> JsonResponse:
    if request.is_ajax():
        if 'country' in request.POST:
            r_country = request.POST['country']
            request.session['country'] = r_country
        if 'currency' in request.POST:
            r_currency = request.POST['currency']
            request.session['currency'] = r_currency
        return JsonResponse({'success': True})
    return JsonResponse({'success': False})
