# -*- coding: utf-8 -*-
import codecs
import copy
import csv
import datetime
import io
import ipaddress
import logging
import typing as t
import sys
import unicodedata
from decimal import Decimal
from functools import wraps

import jwt
import requests
from django.conf import settings
from django.utils import timezone
from django.core.cache import cache
from django.http import Http404
from django.utils.http import urlencode
from ipware.ip import get_ip, get_real_ip
from pytz import country_timezones
from requests.exceptions import ConnectionError
from vat_moss import billing_address

import button3d.type_declarations as td
from apps.b3_core.decimal_calc import DecimalCalc, DecimalCalcException

logger = logging.getLogger(__name__)

__all__ = (
    'obfuscate_secret_string_value',
    'get_client_ip',
    'determine_default_currency',
    'get_country',
    'get_timezone_from_country',
    'get_timezone_from_ip',
    'get_country_from_ip',
    'get_currency_symbol',
    'get_currency_from_country',
    'get_exchange_rate',
    'get_exchange_rates',
    'currency_exchange',
    'get_vat_rate',
    'fit_in_bounds',
    'clean_filename',
    'Orientation',
    'eval_price_custom',
    'eval_price_support_custom',
    'eval_price_orientation_custom',
    'CSVUnicodeWriter',
    'build_secure_download_url',
    'validate_not_null',
    'cache_result_on_instance',
    'MissingParameterException',
    'DoesNotFitInBoundsException',
    'PriceEvaluationError',
    'ManualPricingRequiredException',
    'NoPricingPossibleException',
    'quantize',
    'today',
    'require_debug',
    'get_default_currency',
    'query_yes_no',
)


def obfuscate_secret_string_value(
        string_value: str,
        *passwords: str,
        placeholder_char: str = '*'
) -> str:
    """
    When logging, it is important to not leak sensitive data, like passwords.
    To make logged data more descriptive, password part will be replaced by
    placeholder character string of the same length.

    Can be called with single argument, in this case it will be replaced with
    same length string of placeholder character.

    Multiple arguments are treated, as `string_value` for first arg, and all
    others as `password`s, that will be replaced in `string_value`.

    `placeholder_char` must be passed as keyword argument, if needed

    >>> api_key = '7653827611'
    >>> url1 = f'http://service.com/?api_key={api_key}'
    http://service.com/?api_key=7653827611

    >>> obfuscate_secret_string_value(url1, api_key)
    http://service.com/?api_key=**********

    >>> obfuscate_secret_string_value(api_key)
    **********

    >>> token = 'BS90332323'
    >>> url2 = f'http://service.com/?api_key={api_key}&token={token}'
    >>> obfuscate_secret_string_value(url2, api_key)
    http://service.com/?api_key=**********&token=BS90332323

    >>> obfuscate_secret_string_value(url2, api_key, token)
    http://service.com/?api_key=**********&token=**********
    """

    if not len(passwords):
        return placeholder_char * len(string_value)

    for password in passwords:
        if len(password):
            subs = placeholder_char * len(password)
            string_value = string_value.replace(password, subs)
    return string_value


def get_client_ip(request: td.Request) -> str:
    ip = get_ip(request) or get_real_ip(request)
    logger.debug(f'Determined a IP: {ip}')
    return ip


def determine_default_currency(request: td.Request) -> str:
    logger.debug('Determining default currency...')
    currency = request.session.get('currency')
    if currency is not None:
        logger.debug(f'Found currency in session: {currency}')
        return currency
    country = get_country(request)
    currency = get_currency_from_country(country)
    logger.debug(f'Default currency for {country} is {currency}')
    request.session['currency'] = currency
    return currency


def get_country(request: td.Request) -> str:
    """
    This function will return the users country.
    It uses the session as a cache, so
    we don't need to use ipinfo.io for every request.
    """
    current_ip = get_client_ip(request)
    if 'ip' in request.session and \
            'country' in request.session and \
            current_ip == request.session['ip']:
        # IP didn't change. We can use the country in the session
        return request.session['country']
    # IP not in session or changed. We need to lookup the country
    request.session['ip'] = current_ip
    request.session['country'] = get_country_from_ip(current_ip)
    return request.session['country']


def get_timezone_from_country(country_code: str) -> str:
    logger.debug(f'Getting timezone from country {country_code}')
    try:
        timezone = country_timezones[country_code][0]
        logger.debug(f'Timezone for {country_code} is {timezone}')
        return timezone
    except (IndexError, TypeError):
        timezone = settings.TIME_ZONE
        logger.warning(
            f'Error getting timezone for {country_code}, using '
            f'default {timezone}')
        return timezone


def get_timezone_from_ip(ip: str) -> str:
    current_country = get_country_from_ip(ip)
    return get_timezone_from_country(current_country)


def get_country_from_ip(ip: str) -> str:
    logger.info(f'Getting country via IPInfo for IP {ip}')

    default_country = settings.DEFAULT_COUNTRY

    if ipaddress.ip_address(ip).is_private:
        logger.debug(f'Skipping requesting IPInfo, IP is local: {ip}')
        return default_country

    if not settings.ENABLE_IPINFO_API:
        logger.debug(
            f'IPInfo is disabled, returning default '
            f'country: {default_country}')
        return default_country

    token = settings.IP_INFO_TOKEN
    if not token:
        logger.warning(f'IPInfo token is not defined')

    # Look up cache
    cache_key = f'country_{ip}'
    cache_timeout = 43200

    country = cache.get(cache_key)
    if country is not None:
        logger.debug(
            'Skipping requesting IPInfo, country taken from cache')
        return country

    url = f'http://ipinfo.io/{ip}/country?token={token}'
    obfuscated_url = obfuscate_secret_string_value(url, token)
    logger.debug(f'Starting request to IPInfo: {obfuscated_url}')

    # Get country from ipinfo.io
    try:
        response = requests.get(url)
    except ConnectionError as exc:
        logger.error(f'ConnectionError while accessing IPInfo: {exc}')
        country = default_country
    else:
        country = response.text.strip()
        logger.debug(f'Used ipinfo.io to determine country: {country}')
        if len(country) != 2:
            logger.error(
                f'ipinfo.io has returned unexpected response for ip {ip}:'
                f'{country}'
                f'Returning default: {default_country} instead',
                exc_info=True)
            country = default_country

    cache.set(cache_key, country, cache_timeout)
    logger.info(f'Saved country {country} for ip {ip} in cache')
    return country


def get_currency_symbol(string_currency: str) -> str:
    currency_dictionary = {
        'USD': '$',
        'GBP': '£',
        'EUR': '€',
        'PLN': 'zł',
        'NOK': 'kr',
        'SEK': 'kr',
        'HUF': 'Ft',
        'DKK': 'kr',
        'IDR': 'Rp',
    }
    return currency_dictionary[string_currency]


def get_currency_from_country(country: str) -> str:
    # array from http://country.io/currency.json
    logger.debug(f'Getting currency for country {country}')
    lookup = {"BD": "BDT", "BE": "EUR", "BF": "XOF",
              "BG": "BGN", "BA": "BAM", "BB": "BBD",
              "WF": "XPF", "BL": "EUR", "BM": "BMD",
              "BN": "BND", "BO": "BOB", "BH": "BHD",
              "BI": "BIF", "BJ": "XOF", "BT": "BTN",
              "JM": "JMD", "BV": "NOK", "BW": "BWP",
              "WS": "WST", "BQ": "USD", "BR": "BRL",
              "BS": "BSD", "JE": "GBP", "BY": "BYR",
              "BZ": "BZD", "RU": "RUB", "RW": "RWF",
              "RS": "RSD", "TL": "USD", "RE": "EUR",
              "TM": "TMT", "TJ": "TJS", "RO": "RON",
              "TK": "NZD", "GW": "XOF", "GU": "USD",
              "GT": "GTQ", "GS": "GBP", "GR": "EUR",
              "GQ": "XAF", "GP": "EUR", "JP": "JPY",
              "GY": "GYD", "GG": "GBP", "GF": "EUR",
              "GE": "GEL", "GD": "XCD", "GB": "GBP",
              "GA": "XAF", "SV": "USD", "GN": "GNF",
              "GM": "GMD", "GL": "DKK", "GI": "GIP",
              "GH": "GHS", "OM": "OMR", "TN": "TND",
              "JO": "JOD", "HR": "HRK", "HT": "HTG",
              "HU": "HUF", "HK": "HKD", "HN": "HNL",
              "HM": "AUD", "VE": "VEF", "PR": "USD",
              "PS": "ILS", "PW": "USD", "PT": "EUR",
              "SJ": "NOK", "PY": "PYG", "IQ": "IQD",
              "PA": "PAB", "PF": "XPF", "PG": "PGK",
              "PE": "PEN", "PK": "PKR", "PH": "PHP",
              "PN": "NZD", "PL": "PLN", "PM": "EUR",
              "ZM": "ZMK", "EH": "MAD", "EE": "EUR",
              "EG": "EGP", "ZA": "ZAR", "EC": "USD",
              "IT": "EUR", "VN": "VND", "SB": "SBD",
              "ET": "ETB", "SO": "SOS", "ZW": "ZWL",
              "SA": "SAR", "ES": "EUR", "ER": "ERN",
              "ME": "EUR", "MD": "MDL", "MG": "MGA",
              "MF": "EUR", "MA": "MAD", "MC": "EUR",
              "UZ": "UZS", "MM": "MMK", "ML": "XOF",
              "MO": "MOP", "MN": "MNT", "MH": "USD",
              "MK": "MKD", "MU": "MUR", "MT": "EUR",
              "MW": "MWK", "MV": "MVR", "MQ": "EUR",
              "MP": "USD", "MS": "XCD", "MR": "MRO",
              "IM": "GBP", "UG": "UGX", "TZ": "TZS",
              "MY": "MYR", "MX": "MXN", "IL": "ILS",
              "FR": "EUR", "IO": "USD", "SH": "SHP",
              "FI": "EUR", "FJ": "FJD", "FK": "FKP",
              "FM": "USD", "FO": "DKK", "NI": "NIO",
              "NL": "EUR", "NO": "NOK", "NA": "NAD",
              "VU": "VUV", "NC": "XPF", "NE": "XOF",
              "NF": "AUD", "NG": "NGN", "NZ": "NZD",
              "NP": "NPR", "NR": "AUD", "NU": "NZD",
              "CK": "NZD", "XK": "EUR", "CI": "XOF",
              "CH": "CHF", "CO": "COP", "CN": "CNY",
              "CM": "XAF", "CL": "CLP", "CC": "AUD",
              "CA": "CAD", "CG": "XAF", "CF": "XAF",
              "CD": "CDF", "CZ": "CZK", "CY": "EUR",
              "CX": "AUD", "CR": "CRC", "CW": "ANG",
              "CV": "CVE", "CU": "CUP", "SZ": "SZL",
              "SY": "SYP", "SX": "ANG", "KG": "KGS",
              "KE": "KES", "SS": "SSP", "SR": "SRD",
              "KI": "AUD", "KH": "KHR", "KN": "XCD",
              "KM": "KMF", "ST": "STD", "SK": "EUR",
              "KR": "KRW", "SI": "EUR", "KP": "KPW",
              "KW": "KWD", "SN": "XOF", "SM": "EUR",
              "SL": "SLL", "SC": "SCR", "KZ": "KZT",
              "KY": "KYD", "SG": "SGD", "SE": "SEK",
              "SD": "SDG", "DO": "DOP", "DM": "XCD",
              "DJ": "DJF", "DK": "DKK", "VG": "USD",
              "DE": "EUR", "YE": "YER", "DZ": "DZD",
              "US": "USD", "UY": "UYU", "YT": "EUR",
              "UM": "USD", "LB": "LBP", "LC": "XCD",
              "LA": "LAK", "TV": "AUD", "TW": "TWD",
              "TT": "TTD", "TR": "TRY", "LK": "LKR",
              "LI": "CHF", "LV": "EUR", "TO": "TOP",
              "LT": "LTL", "LU": "EUR", "LR": "LRD",
              "LS": "LSL", "TH": "THB", "TF": "EUR",
              "TG": "XOF", "TD": "XAF", "TC": "USD",
              "LY": "LYD", "VA": "EUR", "VC": "XCD",
              "AE": "AED", "AD": "EUR", "AG": "XCD",
              "AF": "AFN", "AI": "XCD", "VI": "USD",
              "IS": "ISK", "IR": "IRR", "AM": "AMD",
              "AL": "ALL", "AO": "AOA", "AQ": "",
              "AS": "USD", "AR": "ARS", "AU": "AUD",
              "AT": "EUR", "AW": "AWG", "IN": "INR",
              "AX": "EUR", "AZ": "AZN", "IE": "EUR",
              "ID": "IDR", "UA": "UAH", "QA": "QAR",
              "MZ": "MZN", "EU": "EUR"}
    default_currency = 'USD'
    default_european_currency = 'EUR'

    currency = lookup.get(country)

    if currency is None:
        logger.warning(
            f'Country was not in currency lookup table. '
            f'Returning {default_currency} instead')
        return default_currency

    if currency in settings.SUPPORTED_CURRENCY_SET:
        logger.debug(f'Currency for {country} is {currency}')
        return currency

    if country in ['CH', 'LI']:
        logger.debug(
            f'Currency for {country} is not supported, but for some '
            f'countries we are using {default_european_currency}')
        return default_european_currency

    logger.debug(
        f'Currency for {country} is not supported, '
        f'using {default_currency}')
    return default_currency


def get_exchange_rates() -> dict:
    logger.info('Calling CurrencyAPI')
    endpoint = 'live'
    currency_api_key = settings.CURRENCY_API_KEY

    url = f'http://apilayer.net/api/{endpoint}' \
        f'?access_key={currency_api_key}'
    obfuscated_url = obfuscate_secret_string_value(url, currency_api_key)
    logger.debug(f'CurrencyAPI URL: {obfuscated_url}')
    res = requests.get(url)

    res_json = res.json()
    if not res_json['success']:
        from apps.b3_core.exceptions import CurrencyAPIError
        logger.error(
            f'CurrencyAPI response not successful: {res_json["error"]}'
        )
        raise CurrencyAPIError(res_json['error'])

    rates = res_json['quotes']
    logger.debug(f'CurrencyAPI response: {rates}')

    return rates


def get_exchange_rate(from_c: str, to_c: str) -> float:
    logger.debug(f'Getting exchange rate from {from_c} to {to_c}')
    cache_key = 'currency_exchange_rates'

    if settings.DEBUG:
        logger.debug(f'DEBUG=True, using settings.DEFAULT_EXCHANGE_RATES')
        rates = settings.DEFAULT_EXCHANGE_RATES
    elif not settings.ENABLE_CURRENCY_API:
        rates = settings.DEFAULT_EXCHANGE_RATES
        logger.debug(
            f'Currency API is disabled, using default rates: {rates}')
    else:
        rates = cache.get(cache_key)
        if rates is None:
            rates = get_exchange_rates()
            cache.set(cache_key, rates, 43200)

    rate = float(rates['USD' + to_c]) / float(rates['USD' + from_c])
    logger.debug(f'Exchange rate from {from_c} to {to_c} is {rate}')
    return rate


def currency_exchange(from_amount: Decimal, from_c: str, to_c: str) -> Decimal:
    if from_c == to_c:
        return quantize(from_amount)

    exchange_rate = get_exchange_rate(from_c, to_c)

    return quantize(exchange_rate * float(from_amount))


def get_vat_rate(country_code: str) -> Decimal:
    """
    Uses vat_moss to get the VAT rate of an EU country.
    Fakes Zip-Code and City (vat_moss takes
     those arguments for exceptions like Heligoland)
    """
    return billing_address.calculate_rate(country_code, '0', '0')[0]


def _translate_bounds_dict_escape_none_type(bound_list: t.Sequence[float]) \
        -> t.List[float]:
    # Avoid chance of None in the to-be sorted list
    return [x if x is not None else 0.0 for x in bound_list]


def fit_in_bounds(
        min_allowed_bounds: t.Sequence[float],
        max_allowed_bounds: t.Sequence[float],
        given_bounds: t.Sequence[float],
) -> bool:
    min_allowed_bounds = sorted(
        _translate_bounds_dict_escape_none_type(min_allowed_bounds)
    )
    max_allowed_bounds = sorted(
        _translate_bounds_dict_escape_none_type(max_allowed_bounds)
    )
    given_bounds = sorted(
        _translate_bounds_dict_escape_none_type(given_bounds)
    )

    for i in range(3):
        """
        Check that:
        1 - bounds are not between ranges
        2 - given bounds are smaller than min bounds
        3 - given bounds are bigger than max bounds

        it will return False

        """
        if (given_bounds[i] < min_allowed_bounds[i] and
            given_bounds[i] > max_allowed_bounds[i]) or \
                (given_bounds[i] < min_allowed_bounds[i]) or \
                (given_bounds[i] > max_allowed_bounds[i]):
            return False
    return True


def clean_filename(filename: str) -> bytes:
    """
    filenames with special caracters make problems.
    request.post cannot handle them properly.
    Here we "normalize" the filename. Results in ascci only.
    """
    return unicodedata.normalize('NFKD', filename).encode('ascii', 'ignore')


class Orientation:
    WIDTH, DEPTH, HEIGHT = 0, 1, 2
    ORIENTATIONS = ["h-", "h+", "w-", "w+", "d-", "d+"]
    ORIENTATION_TRANSLATION = {
        'h-': (WIDTH, DEPTH, HEIGHT),
        'h+': (WIDTH, DEPTH, HEIGHT),
        'w-': (HEIGHT, DEPTH, WIDTH),
        'w+': (HEIGHT, DEPTH, WIDTH),
        'd-': (WIDTH, HEIGHT, DEPTH),
        'd+': (WIDTH, HEIGHT, DEPTH)
    }


def eval_price_custom(
        price_custom: float,
        quantity: int,
        model_volume: float,
        box_volume: float,
        shells_number: int,
        area: float,
        width: float,
        height: float,
        depth: float,
        machine_volume: float,
):
    calc = DecimalCalc.instance()
    try:
        return calc.parse(
            price_custom,
            variables={
                "quantity": quantity,
                "volume": model_volume,
                "box": box_volume,
                "shells": shells_number,
                "area": area,
                "w": width,
                "h": height,
                "d": depth,
                "machine_volume": machine_volume
            }
        )
    except DecimalCalcException as e:
        raise PriceEvaluationError(e, "PRICE_FORMULA") from e


def eval_price_support_custom(
        price_support_custom: float,
        quantity: int,
        support_volume: float,
        support_area: float,
        width: float,
        height: float,
        depth: float,
):
    calc = DecimalCalc.instance()
    try:
        return calc.parse(
            price_support_custom,
            variables={
                "quantity": quantity,
                "volume": support_volume,
                "area": support_area,
                "w": width,
                "h": height,
                "d": depth
            }
        )
    except DecimalCalcException as e:
        raise PriceEvaluationError(e, "SUPPORT_FORMULA") from e


def eval_price_orientation_custom(
        price_orientation_custom: float,
        model_costs: dict,
        support_costs: dict,
):
    orientations = Orientation.ORIENTATIONS
    variables_dict = {}
    for i in range(len(orientations)):
        variables_dict["model_cost%d" % (i + 1)] = model_costs.get(
            orientations[i], model_costs["h-"])
        variables_dict["support_cost%d" % (i + 1)] = support_costs.get(
            orientations[i], support_costs["h-"])

    calc = DecimalCalc.instance()
    try:
        return calc.parse(price_orientation_custom, variables=variables_dict)
    except DecimalCalcException as e:
        raise PriceEvaluationError(e, "ORIENTATION_FORMULA") from e


class CSVUnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self,
                 f: t.TextIO,
                 dialect: t.Type[csv.Dialect] = csv.excel,
                 encoding: str = "utf-8",
                 **kwds: dict) \
            -> None:
        # Redirect output to a queue
        self.queue = io.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row: t.Sequence[t.Any]) -> None:
        self.writer.writerow([str(s) for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.encode('utf-8').decode()
        # write to the target
        self.stream.write(data)
        # empty queue
        self.queue.seek(0)
        self.queue.truncate(0)

    def writerows(self, rows: t.Sequence[t.Sequence[t.Any]]) -> None:
        for row in rows:
            self.writerow(row)


def build_secure_download_url(
        uuid: str,
        file: t.BinaryIO,
        filename: t.Optional[str] = None,
        scale: t.Optional[float] = None,
        expiration: int = None,
        base_url: t.Optional[str] = None,
) -> str:
    from apps.b3_organization.utils import get_base_url
    """
    This function builds a URL to the download endpoint of the 3D Backend
    using JSON Web tokens for signed urls.
    See http://deepthought.3yourmind.com/backend/
    API_Documentation/apis-spri-backend/#download-file
    :param uuid: the uuid of the stl-file object
    :param file: the file that should get downloaded (e.g. repaired.stl)
    :param filename: The filename that the User sees
    :param scale The backend will scale a 3D file accordingly
    :param Overwrites the default expiration.
    :param Overwrites the base_url returned byget_base_url()
    :return:
    """

    base_url = base_url or get_base_url()

    exp = getattr(settings, 'JSON_WEB_TOKEN_EXPIRATION_SECONDS', 30)
    if expiration is not None:
        exp = expiration

    token = jwt.encode({
        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=exp),
        'uuid': uuid,
        'file': file,
        'filename': filename if filename else file,
        'scale': scale
    }, settings.SECRET_KEY)

    url_token = urlencode({'token': token})

    return f'{base_url}/download?{url_token}'


def validate_not_null(value: t.Any) -> bool:
    return value is not None


NOT_NULL = {"null": True, "validators": [validate_not_null]}


def cache_result_on_instance(method: t.Callable) -> t.Any:
    @wraps(method)
    def decorated_method(self: t.Any, *args: list, **kwargs: dict) -> t.Any:
        cache_key = '_cached_{0}'.format(method.__name__)
        if not hasattr(self, cache_key):
            setattr(self, cache_key, method(self, *args, **kwargs))
        return copy.copy(getattr(self, cache_key))

    return decorated_method


class ManualPricingRequiredException(Exception):
    pass


class NoPricingPossibleException(Exception):
    pass


class MissingParameterException(NoPricingPossibleException):
    pass


class DoesNotFitInBoundsException(NoPricingPossibleException):
    pass


class PriceEvaluationError(NoPricingPossibleException):
    def __init__(self, msg, formula_name):
        self.formula_name = formula_name
        super().__init__(msg)


def quantize(
        value: t.Union[Decimal, float, str],
        rounding: t.Optional[str] = None,
) -> Decimal:
    """
    Rounds `value` by precision from settings.CURRENCY_DECIMAL_PLACES
    """

    if not isinstance(value, Decimal):
        value = Decimal(value)
    return value.quantize(settings.CURRENCY_DECIMAL_PLACES, rounding=rounding)


def today() -> datetime.date:
    """
    Return today's date as the user sees it

    Caveats: if Django can't figure out the user's timezone, then this is
    simply `settings.TIME_ZONE`'s notion of today.
    """
    return timezone.now().today()


def require_debug(
        func: t.Callable[[td.Request, t.Any], td.DjangoResponse],
) -> t.Callable:
    """
    Decorator for view functions that must only be executed when
    settings.DEBUG=True
    """

    @wraps(func)
    def wrapper(*args: list, **kwargs: dict) -> td.DjangoResponse:
        if settings.DEBUG:
            return func(*args, **kwargs)
        raise Http404

    return wrapper


def get_default_currency() -> str:
    """
    Wraps the settings object so it can be used as callable.

    :return: The value of "DEFAULT_CURRENCY" in the Django settings.
    """
    return getattr(settings, 'DEFAULT_CURRENCY', 'EUR')


def query_yes_no(question: str, default: t.Optional[str] = "yes",
                 file_obj: io.TextIOWrapper = sys.stdout):
    """Ask a yes/no question via :func:`input` and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {
        "yes": True, "y": True, "ye": True,
        "no": False, "n": False
    }
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        file_obj.write(question + prompt)
        choice = input().lower()  # nosec
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            file_obj.write(
                "Please respond with 'yes' or 'no' (or 'y' or 'n').\n"
            )
