import pstats
import threading

from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import MiddlewareNotUsed, ImproperlyConfigured
from django.db import connection
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.template import Template, Context
from django.utils import timezone
from django.utils.functional import SimpleLazyObject

from apps.b3_core.exceptions import RedirectException, JSONBadRequest
from apps.b3_organization.tracking.trackers import UserInteractionTracker
from apps.b3_organization.utils import get_current_site
from .base import BaseMiddleware

try:
    import cProfile as profile
except ImportError:
    import profile
try:
    from cStringIO import StringIO
except BaseException:
    from io import StringIO


class ProfileMiddleware(BaseMiddleware):
    """
    Simple profile middleware to profile django views. To run it, add ?prof to
    the URL like this:

    http://localhost:8000/view/?prof

    Optionally pass the following to modify the output:

    ?sort => Sort the output by a given metric. Default is time.
    See http://docs.python.org/2/library/profile.html#pstats.Stats.sort_stats
    for all sort options.

    ?count => The number of rows to display. Default is 100.

    ?download => Download profile file suitable for visualization.
    For example in snakeviz or RunSnakeRun

    This is adapted from an example found here:
    http://www.slideshare.net/zeeg/
        django-con-high-performance-django-presentation.
    """

    def __init__(self, get_response):
        if not settings.ENABLE_PROFILER:
            raise MiddlewareNotUsed
        super().__init__(get_response)

    @staticmethod
    def show_profile(request):
        return 'prof' in request.GET

    def process_view(self, request, callback, callback_args, callback_kwargs):
        if self.show_profile(request):
            self.profiler = profile.Profile()
            args = (request,) + callback_args
            return self.profiler.runcall(callback, *args, **callback_kwargs)

    def __call__(self, request):
        response = self.get_response(request)
        if self.show_profile(request):
            self.profiler.create_stats()
            io = StringIO()
            stats = pstats.Stats(self.profiler, stream=io)
            stats.sort_stats(request.GET.get('sort', 'time'))
            stats.print_stats(int(request.GET.get('count', 100)))
            response = HttpResponse('<pre>%s</pre>' % io.getvalue())
        return response


# Log all SQL statements direct to the console (when running in DEBUG)
# Intended for use with the django development server.
class SQLLogToConsoleMiddleware(BaseMiddleware):
    def __init__(self, get_response):
        if not settings.ENABLE_SQL_DEBUG:
            raise MiddlewareNotUsed
        super().__init__(get_response)

    def __call__(self, request):
        response = self.get_response(request)
        if settings.DEBUG and connection.queries:
            time = sum([float(q['time']) for q in connection.queries])
            t = Template(
                ("{{count}} quer{{count|pluralize:\"y,ies\"}} "
                 "in {{time}} seconds:\n\n{% for sql in sqllog %}"
                 "[{{forloop.counter}}] {{sql.time}}s: "
                 "{{sql.sql|safe}}{% if not forloop.last %}"
                 "\n\n{% endif %}{% endfor %}"))
            print((t.render(Context({
                'sqllog': connection.queries,
                'count': len(connection.queries),
                'time': time}))))
        return response


class ExceptionsMiddleware(BaseMiddleware):
    def __call__(self, request):
        response = self.get_response(request)
        if response.status_code == 404 and request.path.startswith('/v1/'):
            return JsonResponse({
                "error": ("The requested resource is not found. "
                          "Please read our api documentation here "
                          "https://developers.3yourmind.com/")},
                status=404)
        if response.status_code == 500 and request.path.startswith('/v1/'):
            return JsonResponse({
                "error": ("Sorry, but the requested resource is "
                          "unavailable due to a server hiccup. "
                          "Our support team has been notified. "
                          "Please check back later.")},
                status=500)
        return response

    def process_exception(self, request, exception):
        if isinstance(exception, RedirectException):
            return redirect(exception.url)
        if isinstance(exception, JSONBadRequest):
            return JsonResponse({"error": str(exception)})


class AuthenticationMiddleware(BaseMiddleware):
    def __call__(self, request):
        if not hasattr(request, 'session'):
            raise ImproperlyConfigured(
                "The Django authentication middleware requires "
                "session middleware to be installed. "
                "Edit your MIDDLEWARE_CLASSES setting to insert "
                "'django.contrib.sessions.middleware.SessionMiddleware'"
                "before "
                "'django.contrib.auth.middleware.AuthenticationMiddleware'."
            )
        request.user = SimpleLazyObject(lambda: self.get_user(request))

        return self.get_response(request)

    @staticmethod
    def get_user(request):
        if not hasattr(request, '_cached_user'):
            request._cached_user = auth.get_user(request)
        user = request._cached_user
        site = get_current_site(request)
        if user.is_authenticated and user.userprofile.site != site:
            return AnonymousUser()
        return user


THREAD_LOCALS = threading.local()


class GlobalRequestMiddleware(BaseMiddleware):
    def __call__(self, request):
        THREAD_LOCALS.current_request = request

        response = self.get_response(request)

        if hasattr(THREAD_LOCALS, 'current_request'):
            del THREAD_LOCALS.current_request
        return response


class UserInteractionTrackingMiddleware(BaseMiddleware):
    """
    Middleware to track interactions of registered users with the application.
    Interactions are tracked at most once a day per logged-in user.
    """

    def __call__(self, request):
        if request.user.is_authenticated:
            now = timezone.now()
            if request.user.last_login and \
                    (now - request.user.last_login).days > 0:
                request.user.last_login = now
                request.user.save(update_fields=['last_login'])
                UserInteractionTracker.register_event(request.user)

        return self.get_response(request)
