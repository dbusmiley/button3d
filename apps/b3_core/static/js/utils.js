/*
*  Please put generic and reusable utility functions here
*/


function show_popup(url, width, height) {
    var top = (window.screen.height / 2) - (height / 2);
    var left = (window.screen.width / 2) - (width / 2);
    var options = 'width=' + width;
    var options = options + ',height=' + height;
    var options = options + ',scrollbars=no';
    var options = options + ',resizable=no';
    var options = options + ',menubar=no';
    var options = options + ',location=no';
    var options = options + ',status=no';
    var options = options + ',toolbar=no';
    var options = options + ',left=' + left;
    var options = options + ',top=' + top;
    popup_window = window.open(url, "popupWindow", options);
    popup_window.focus();
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
  clearTimeout (timer);
  timer = setTimeout(callback, ms);
 };
})();


function unique(arr) {
    var u = {}, a = [];
    for(var i = 0, l = arr.length; i < l; ++i){
        if(!u.hasOwnProperty(arr[i])) {
            a.push(arr[i]);
            u[arr[i]] = 1;
        }
    }
    return a;
}


function showError(e){
     $('.error-500').hide();
     $('.error-timeout').hide();

    if((e.status==500) || (e.statusText=="error")){
        console.log('500 - Error');
        $('.loading-big-panel').hide();
        $('.error-500').show();
    }
    if(e.statusText=="timeout" ){
        console.log('Timeout');
        $('.loading-big-panel').hide();
        $('.error-timeout').show();
    }
}

ko.subscribable.fn.subscribeChanged = function (callback) {
    var oldValue;
    this.subscribe(function (_oldValue) {
        oldValue = _oldValue;
    }, this, 'beforeChange');

    this.subscribe(function (newValue) {
        callback(newValue, oldValue);
    });
};


ko.bindingHandlers.img = {
        update: function (element, valueAccessor) {
            //grab the value of the parameters, making sure to unwrap anything that could be observable
            var value    = ko.utils.unwrapObservable(valueAccessor()),
                src      = ko.utils.unwrapObservable(value.src),
                fallback = ko.utils.unwrapObservable(value.fallback),
                $element = $(element);

            //now set the src attribute to either the bound or the fallback value
            if (src) {
                $element.attr("src", src);
            } else {
                $element.attr("src", fallback);
            }
        },
        init: function (element, valueAccessor) {
            var $element = $(element);

            //hook up error handling that will unwrap and set the fallback value
            $element.error(function () {
                var value = ko.utils.unwrapObservable(valueAccessor()),
                    fallback = ko.utils.unwrapObservable(value.fallback);

                $element.attr("src", fallback);
            });
        }
    };


ko.bindingHandlers.select2 = {
    init: function(el, valueAccessor, allBindingsAccessor, viewModel) {
        ko.utils.domNodeDisposal.addDisposeCallback(el, function() {
            $(el).select2('destroy');
        });

        $(el).select2(ko.utils.unwrapObservable(allBindingsAccessor().select2));
    }
};

// Disable options on selector
ko.bindingHandlers.optionsBind = {
    preprocess: function(value, key, addBinding) {
        addBinding('optionsAfterRender', 'function(option, item) { ko.bindingHandlers.optionsBind.applyBindings(option, item, ' + value + ') }');
    },
    applyBindings: function(option, item, bindings) {
        if (item !== undefined) {
            option.setAttribute('data-bind', bindings);
            ko.applyBindings(ko.contextFor(option).createChildContext(item), option);
        }
    }
};



// Custom Binding for Tooltip
ko.bindingHandlers.bootstrapToolTip = {
    init: function(element, valueAccessor, allBindings, viewModel,bindingContext) {
        var options = valueAccessor();
        var defaultOptions = {
            container: 'body',
            trigger:'hover',
            animation:true,
            html:true,
            placement:'bottom',
            template: '<div class="tooltip" role="tooltip"><div style="max-width:370px;background-color: white;border-radius: 0;border: 1px solid #BFBFBF; color:#555;box-shadow: 0px 1px 5px #BFBFBF; "class="tooltip-inner"></div></div>'
        };
        options = $.extend(true, {}, defaultOptions, options);
        $(element).tooltip(options);
    }
};

ko.bindingHandlers.hoverEffect = {
    update: function(element, valueAccessor) {
        ko.utils.registerEventHandler(element, "mouseover", function() {
            $(element).find('.btn').addClass("btn-hover");
        });
        ko.utils.registerEventHandler(element, "mouseout", function() {
            $(element).find('.btn').removeClass("btn-hover");
        });
    }
};

function currency_to_letter(currency){
    var currencyDictionary = {
        USD: '$',
        GBP: '£',
        EUR: '€',
        PLN: 'zł',
        NOK: 'kr',
        SEK: 'kr',
        HUF: 'Ft',
        DKK: 'kr',
        IDR: 'Rp'
    };
    return currencyDictionary[currency];
}

function get_stars_list(stars_num){
    var result = [];
    for(var i=0; i<5; i++)
        result.push(stars_num>i?1:0);
    return result;
}

function truncate(str, limit) {
    var bits, i;
    bits = str.split('');
    if (bits.length > limit) {
        for (i = bits.length - 1; i > -1; --i) {if (i > limit) {bits.length = i;}else if (' ' === bits[i]) {bits.length = i;break;}}
        bits.push('...');
    }
    return bits.join('');
}
function truncate_middle(fullStr, strLen, separator) {
    if (fullStr.length <= strLen) return fullStr;

    separator = separator || '...';

    var sepLen = separator.length,
        charsToShow = strLen - sepLen,
        frontChars = Math.ceil(charsToShow/2),
        backChars = Math.floor(charsToShow/2);

    return fullStr.substr(0, frontChars) +
           separator +
           fullStr.substr(fullStr.length - backChars);
}


function date_formatted_lang(date){
    if (date) {
        var millisecsSince1970 = Date.parse(date);
        var date_ = new Date(millisecsSince1970);
        return date_to_nicestr(date_);
    }
    return "";
}

function date_to_nicestr(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = date.getDate().toString();
    if(lang === 'de')
        return (dd[1] ? dd : "0" + dd[0]) + '.' + (mm[1] ? mm : "0" + mm[0]) + '.' + yyyy;
    return (mm[1] ? mm : "0" + mm[0]) + '/' + (dd[1] ? dd : "0" + dd[0]) + '/' + yyyy; // padding
}

function pointToComma(number){
    if(lang === 'de' && number){
        formatted_number = number.toString().replace('.',',');
        return formatted_number;
    }
    return number;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function isInArray(array, search) {
    return array.indexOf(search) >= 0;
}

function formatColorDropdown(colorId) {
    if(colorId.selected === undefined)
        return;
    return $('<span><div style="width: 0.8em;height: 0.8em; display: inline-block; margin-right: 0.3em; margin-left: 0.1em; background-color:' + colors[colorId.element.value].rgb + '"></div>' + colors[colorId.element.value].title + '</span>');
    //return $('<span><div class=\'color-box\' style=\'background-color:' +  + '\'></div>' + colors[colorId.element.value].title + '</span>');
}

function matchGroupWithChildren(params, data) {
    data.parentText = data.parentText || "";

    // Always return the object if there is nothing to compare
    if ($.trim(params.term) === '') {
      return data;
    }

    // Do a recursive check for options with children
    if (data.children && data.children.length > 0) {
      // Clone the data object if there are children
      // This is required as we modify the object to remove any non-matches
      var match = $.extend(true, {}, data);

      // Check each child of the option
      for (var c = data.children.length - 1; c >= 0; c--) {
        var child = data.children[c];
        child.parentText += data.parentText + " " + data.text;

        var matches = matchGroupWithChildren(params, child);

        // If there wasn't a match, remove the object in the array
        if (matches == null) {
          match.children.splice(c, 1);
        }
      }

      // If any children matched, return the new object
      if (match.children.length > 0) {
        return match;
      }

      // If there were no matching children, check just the plain object
      return matchGroupWithChildren(params, match);
    }

    // If the typed-in term matches the text of this term, or the text from any
    // parent term, then it's a match.
    var original = (data.parentText + ' ' + data.text).toUpperCase();
    var term = params.term.toUpperCase();


    // Check if the text contains the term
    if (original.indexOf(term) > -1) {
      return data;
    }

    // If it doesn't contain the term, don't return anything
    return null;
}

function updateSvgStyle() {
    $('img[src$=".svg"]').each(function () {
        var $img = jQuery(this);
        var imgURL = $img.attr('src');
        var attributes = $img.prop("attributes");

        $.get(imgURL, function (data) {
            // Get the SVG tag, ignore the rest
            var $svg = $($(data)[4]);

            // Remove any invalid XML tags
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and apply on SVG
            $.each(attributes, function () {
                if (this.name !== 'src' && this.name !== 'alt')
                    $svg.attr(this.name, this.value);
            });

            // Replace IMG with SVG
            $img.replaceWith($svg);
        }, 'html');
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function svgFallback(img) {
    $(img).replaceWith('<svg class="img-thumbnail" width="320" height="183" viewBox="-20 -20 160 186" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid" >\
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
        <g id="Group-2" class="fallback-svg">\
            <path d="M59.5,99 C76.8969696,99 91,84.8969696 91,67.5 C91,50.1030304 76.8969696,36 59.5,36 C42.1030304,36 28,50.1030304 28,67.5 C28,84.8969696 42.1030304,99 59.5,99 Z M59.6869037,64.6709286 L51.0159752,56 L48,59.0159752 L56.6709286,67.6869037 L48,76.3578323 L51.0159752,79.3738075\ L59.6869037,70.7028789 L68.3578323,79.3738075 L71.3738075,76.3578323 L62.7028789,67.6869037 L71.3738075,59.0159752 L68.3578323,56 L59.6869037,64.6709286 Z" id="Combined-Shape" opacity="0.434782609"></path>\
            <g id="Group">\
                <polygon id="Polygon" opacity="0.6" points="60 0 120 32 60 64 0 32"></polygon>\
                <polygon id="Rectangle" opacity="0.5" points="0 32 60 63.7517673 60 140.902344 0 109.150576"></polygon>\
                <polygon id="Rectangle-Copy" opacity="0.4" transform="translate(90.000000, 86.451172) scale(-1, 1) translate(-90.000000, -86.451172) " points="60 32 120 63.7517673 120 140.902344 60 109.150576"></polygon>\
            </g>\
        </g>\
    </g>\
</svg>\
        ');
}

/**
 * Function to filter an array by distinct elements.
 * @param {Object} value The value to check. It is needed because we need access to the actual value of the element.
 * @param {Number} index The index of the element which is needed because we need to check if the value is already inside of
 * the array. We will check this with the 'indexOf' method.
 * @param {Array} selfArray Array which should be checked.
 * @returns {boolean} True if the element is unique.
 */
// eslint-disable-next-line
function onlyUnique(value, index, selfArray) {
    return selfArray.indexOf(value) === index;
}

function lazyLoad (img) {
    var self = $(img);
    var source = self.attr("data-src");
    var imgPreload = new Image();
    imgPreload.src = source;
    $(imgPreload).on('error', function(){
        // if preload img erro, fall back to svg
        svgFallback(self);
    }).on('load', function(){
        // success, replace with preload pic
        self.replaceWith(imgPreload);
    });
}

$(function(){
    // when document get ready, each lazy-load element preload img
    $('img.lazy-load').each(function(){
        lazyLoad(this);
    })
})

/**
 * Function to open site configuration modal. This will also check if the function is available to reduce JavaScript
 * errors.
 * @returns {boolean} Always return true because of click events.
 */
// eslint-disable-next-line
function showSiteConfigurationModal() {
    if (typeof Button3d.modals.show_site_configuration_modal === 'function') {
        Button3d.modals.show_site_configuration_modal();
    }
    return true;
}
