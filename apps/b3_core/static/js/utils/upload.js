var uploadUtility = (function createModal() {
    function getErrorParagraphTexts(amountFiles) {
        function getFirstErrorParagraphText(amount) {
            var firstParagraphText = gettext('A 3D model is currently too large to be processed:');
            if (amount > 1) {
                firstParagraphText = gettext('Some 3D models are currently too large to be processed:');
            }
            return firstParagraphText;
        }

        return {
            first: getFirstErrorParagraphText(amountFiles)
        };
    }

    function createFileProgressBar(config) {
        var headingStyle = config.type === 'warning' ? 'color: #f0ad4e' : '';
        return (
            '<div class="row file-progress-row" style="padding-left:15px;padding-right:15px;">' +
            '<h5 style="' +
            headingStyle +
            '">' +
            config.filename +
            '</h5>' +
            '<div class="progress progress-files" style="margin-bottom:10px;background-color:#ededed">' +
            '<div class="progress-bar progress-bar-' +
            config.type +
            ' progress-bar-striped progress-' +
            slugify(config.filename) +
            '" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:' +
            config.progress +
            '%;"></div>' +
            '</div>' +
            '</div>'
        );
    }

    function createLoadingContainer(config) {
        return (
            '<div class="text-center" style="overflow:hidden">' +
            '<h4 style="font-size:1.5em;">' +
            config.loadingMessage +
            '</h4>' +
            '<div class="loader"></div>' +
            '</div>'
        );
    }

    function slugify(text) {
        return text
            .toString()
            .toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }

    function mapInvalidFilesToHtml(invalidFiles) {
        var entries = invalidFiles.map(function(invalidFile) {
            var errorMessage = Button3d.common.fileExtensions.getErrorMessage(invalidFile);
            if (errorMessage === null) {
                return null;
            }
            return (
                '<li>' +
                invalidFile.name +
                ' (' +
                errorMessage +
                ')</li>'
            );
        });
        entries = entries.filter(function(element) {
            return element !== null;
        });
        return entries.join('');
    }

    return {
        mapInvalidFilesToHtml: mapInvalidFilesToHtml,
        getErrorParagraphTexts: getErrorParagraphTexts,
        createFileProgressBar: createFileProgressBar,
        createLoadingContainer: createLoadingContainer,
        slugify: slugify
    };
})();

// If node.js is runtime export the module for testing.
if (typeof module !== 'undefined' && module.exports) {
    module.exports = {
        getErrorParagraphTexts: uploadUtility.getErrorParagraphTexts,
        slugify: uploadUtility.slugify,
        createFileProgressBar: uploadUtility.createFileProgressBar,
        createLoadingContainer: uploadUtility.createLoadingContainer,
        mapInvalidFilesToHtml: uploadUtility.mapInvalidFilesToHtml
    };
}
