var Button3d = Button3d || {};

var project_api = extend(Button3d, 'userpanel.project.api');

$.extend(project_api, (function() {
    var publix = {};

    // --- Get calls ---
    publix.get_project = function(project_id, currency, success_callback, errorCallback) {
        var url = currency ?  get_project_url(project_id) + '?currency=' + currency : get_project_url(project_id);
        get_json(url, success_callback, errorCallback);
    };

    publix.projects_search = function(data, success_callback) {
        var url = get_project_url(null) + 'search/';
        $.ajax({
            url: url,
            data: data,
            type: 'GET',
            success: success_callback
        });
    };

    publix.get_project_line = function(project_id, line_id, currency, success_callback, errorCallback) {
        var url = currency ? get_line_url(project_id, line_id) + '?currency=' + currency : get_line_url(project_id, line_id);
        get_json(url, success_callback, errorCallback);
    };

    publix.get_project_url = function(project_id, success_callback, errorCallback) {
        var url = get_project_url(project_id) + "url/";
        get_json(url, success_callback, errorCallback);
    };

    function get_json(url, successCallback, errorCallback) {
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                successCallback(data);
            },
            error: function (e) {
                if (errorCallback) {
                    errorCallback(e);
                }
            }
        });
    }

    // --- Post calls ---

    publix.set_project_title = function(project_id, title) {
        var url = get_project_url(project_id);
        var jsonData = JSON.stringify({'title': title});

        post_json(url, jsonData);
    };

    publix.set_project_reference = function(project_id, reference) {
        var url = get_project_url(project_id);
        var jsonData = JSON.stringify({'reference': reference});

        post_json(url, jsonData);
    };

    publix.set_line_quantity = function(project_id, line_id, quantity, currency, success) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'quantity': quantity});
        if (currency) {
            url += '?currency=' + currency;
        }
        post_json(url, jsonData, success);
    };

    publix.set_line_fields_to_null = function(project_id, line_id) {
        return project_api.set_line_fields(project_id, line_id, null, null, null, null);
    };

    publix.set_line_fields = function(project_id, line_id, product_slug, supplier_slug, post_processings, currency, success_callback) {
        var url = get_line_url(project_id, line_id);
        var productSlug = product_slug ? product_slug : '';
        var supplierSlug = supplier_slug ? supplier_slug : '';
        var postProcessings = post_processings ? post_processings : [];

        var jsonData = JSON.stringify({
            'product_slug': productSlug,
            'supplier_slug': supplierSlug,
            'post_processings': postProcessings
        });
        if (currency) {
            url += '?currency=' + currency;
        }
        post_json(url, jsonData, success_callback);
    };

    publix.set_line_fields_with_qty = function(
        project_id, line_id, product_slug, supplier_slug, postProcessings, quantity, currency, success_callback) {
        var url = get_line_url(project_id, line_id);
        var productSlug = product_slug ? product_slug : '';
        var supplierSlug = supplier_slug ? supplier_slug : '';

        var jsonData = JSON.stringify({
            product_slug: productSlug,
            supplier_slug: supplierSlug,
            post_processings: postProcessings,
            quantity: quantity
        });
        if (currency) {
            url += '?currency=' + currency;
        }
        post_json(url, jsonData, success_callback);
    }

    publix.set_line_product_slug = function(project_id, line_id, product_slug) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'product_slug': product_slug});

        post_json(url, jsonData);
    };

    publix.set_line_supplier_slug = function(project_id, line_id, supplier_slug) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'supplier_slug': supplier_slug});

        post_json(url, jsonData);
    };

    publix.set_line_color_id = function(project_id, line_id, color_id) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'color_id': color_id});

        post_json(url, jsonData);
    };

    publix.set_line_file_name = function(project_id, line_id, filename) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'filename': filename});

        post_json(url, jsonData);
    };

    publix.set_line_scale_and_unit = function(project_id, line_id, scale, measure_unit) {
        var url = get_line_url(project_id, line_id);
        var jsonData = JSON.stringify({'scale': scale,
                                       'unit': measure_unit});

        post_json(url, jsonData);
    };

    publix.clone_project = function(project_id, new_name, success_callback) {
        var url = get_project_url(project_id) + 'clone/';
        var jsonData = JSON.stringify({'project_name': new_name});

        $.ajax({
            url: url,
            type: 'POST',
            contentType: "application/json",
            data: jsonData,
            success: success_callback
        });
    };

    publix.clone_line = function(project_id, line_id, success_callback) {
        var url = get_line_url(project_id, line_id) + 'clone/';
        $.ajax({
            url: url,
            type: 'POST',
            success: success_callback
        });
    };

    publix.request_manual_price = function(project_id, success_callback) {
        var url = get_project_url(project_id) + 'manual_price/';
        $.ajax({
            url: url,
            type: 'POST',
            success: success_callback
        });
    }

    publix.set_manual_prices = function(project_id, price_data, success_callback) {
        // price_data should be an object containing the line_id as key and the manual chosen price as value
        var url = get_project_url(project_id) + 'set_prices/';
        $.ajax({
            url: url,
            data: JSON.stringify(price_data),
            type: 'POST',
            success: success_callback
        });
    }

    publix.set_basket_as_priced = function(project_id, success_callback) {
        var url = get_project_url(project_id) + 'set_as_priced/';
        $.ajax({
            url: url,
            type: 'POST',
            success: success_callback
        });
    }

    function post_json(url, jsonData, success_callback) {
        $.ajax({
            url: url,
            type: 'POST',
            contentType: "application/json",
            data: jsonData,
            success: success_callback
        });
    }

    // --- Put calls ---

    publix.add_project = function(title, successCallback) {
        $.ajax({
            url: get_project_url(null) + 'create/',
            type: 'PUT',
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(
                {
                    'title': title
                }
            ),
            success: successCallback
        });
    };

    publix.add_file_to_project = function addFileToProject(
        project_id,
        uuid,
        success_callback,
        error_callback
    ) {
        var data = {
            uuid: uuid
        };
        $.ajax({
            type: 'PUT',
            async: false,
            url: get_project_url(project_id) + 'add/',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            success: success_callback,
            error: error_callback
        });
    };

    // --- Delete calls ---

    publix.delete_project = function(project_id, success_callback) {
        var url = get_project_url(project_id);
        delete_request(url, success_callback);
    };

    publix.delete_line = function(project_id, line_id, success_callback) {
        var url = get_line_url(project_id, line_id);
        delete_request(url, success_callback);
    };

    function delete_request(url, success_callback) {
        $.ajax({
            url: url,
            type: 'DELETE',
            success: success_callback
        });
    }

    // --- Permission --
    publix.getPermissionInfo = function (projectId, callback) {
        var url = get_project_url(projectId) + 'users/';
        get_json(url, callback);
    }

    publix.sharePermission = function (projectId, emailAddress, callback) {
        var url = get_project_url(projectId) + 'users/';
        var data = {'email': emailAddress}
        post_json(url, JSON.stringify(data), callback);
    }
    publix.deletePermission = function (projectId, emailAddress, callback) {
        var url = get_project_url(projectId) + 'users/';
        $.ajax({
            url: url,
            type: 'DELETE',
            data: JSON.stringify({'email': emailAddress}),
            success: callback
        });
    }
    // --- Utilities ---

    function get_line_url(project_id, line_id) {
        var lineId = line_id ? line_id + '/' : '';
        return get_project_url(project_id) + 'lines/' + lineId;
    }

    function get_project_url(project_id) {
        var projectId = project_id ? project_id + '/' : '';
        return '/basket-ajax/' + projectId;
    }

    return publix;
}()));
