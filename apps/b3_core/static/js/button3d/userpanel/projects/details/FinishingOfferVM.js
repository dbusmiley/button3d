/* global pre_materials, pre_finishes */
function FinishingOfferVM(offer, projectLineVM) {
    var self = this;
    self.projectLineVM = projectLineVM;
    self.materialTitle = '';
    self.setMaterialTitle = function setMaterialTitle(finishing) {
        var tempMaterialSlug = finishing.material;
        pre_materials.forEach(function (material) {
            if (material.slug === tempMaterialSlug) {
                self.materialTitle = material.title;
            }
        });
    };

    // Find product title
    for (var i = 0; i < pre_finishes.length; ++i) {
        if (pre_finishes[i].slug === offer.slug) {
            var finishing = pre_finishes[i];
            self.setMaterialTitle(finishing);
            self.product_title = ko.observable(finishing.title);
            break;
        }
    }
    self.slug = ko.observable(offer.slug);
    self.selectedColor = ko.observable(null);

    self.manualPricingRequired = offer.manual_pricing_required;

    self.isSelected = ko.observable(false);
    self.unit_price_incl_tax = ko.pureComputed(function () {
        return offer.price
            ? offer.price.incl_tax
            : 0;
    });
    self.price = ko.observable(offer.price);
    // TODO - not sure if just displaying the first shipping method info is enough. (maybe avg instead)
    self.deliveryDaysRange = ko.observable(offer.shipping_methods[0].delivery_days_range);
    self.delivery_days_max = ko.observable(offer.shipping_methods[0].delivery_max_days);
    self.delivery_days_min = ko.observable(offer.shipping_methods[0].delivery_min_days);

    self.tax_price = ko.pureComputed(function () {
        return offer.price
            ? offer.price.tax
            : 0;
    });

    self.color_ids = ko.observableArray(offer.color_ids);
    // Operations
    self.hasColors = ko.pureComputed(function () {
        return self.color_ids().length > 0;
    }, self);

    self.differenceToDefault = ko.pureComputed(function () {
        var defaultFinishingVM = projectLineVM.finishingOffers()[0];
        var difference = 0;
        if (defaultFinishingVM) {
            difference = self.unit_price_incl_tax() - defaultFinishingVM.unit_price_incl_tax();
        }
        return parseFloat(difference);
    }, self);

    self.dropdownDisplay = ko.pureComputed(function () {
        return self.product_title().replace(self.materialTitle + ' ', '');
    }, self);
}
