/*global djangoData */
function ProjectVM() {
    var self = this;
    // Observables
    self.title = ko.observable();
    self.canEditTitle = false;
    self.reference = ko.observable(null);
    self.owner_email = ko.observable();
    self.creation_date = ko.observable();
    self.project_lines_are_loading = ko.observable(true);
    self.quotation_number = ko.observable();
    self.price = ko.observable();
    self.subtotal = ko.observable();
    self.extra_fees = ko.observable();
    self.show_net_prices = ko.observable();
    self.min_price = ko.observable();
    self.diff_to_min_price = ko.observable();
    self.voucher_discount = ko.observable();
    self.tax_type = ko.observable();
    // Project Status
    self.status = ko.observable();
    self.isProjectOrdered = ko.pureComputed(function () {
        return self.status() === 'Submitted';
    });

    // Price Requst Observables
    self.is_manual_pricing_required = ko.observable(false);
    self.has_manual_price_set = ko.observable();
    self.is_manual_pricing_user = ko.observable(false);
    self.is_frozen = ko.observable();
    self.is_manual_pricing_finished = ko.observable();
    self.manual_pricing_currency = ko.observable();

    self.basket_attachments = ko.observableArray();
    self.currency = ko.observable('EUR');

    self.enableCurrencyChanged = false;

    self.projectLines = ko.observableArray();

    self.order = ko.observable(new OrderVM(self));
    self.materialsBySlug = material_products;

    // Price Request Logic Computed
    self.showRequestButton = ko.pureComputed(function(){
        // Check if all project lines are set.
        if (!self.order().same_supplier()) {
            return false;
        }
        var nbLines = self.projectLines().length;
        if (self.order().number_of_items() < nbLines) {
            return false;
        }

        // for user, show the request button when manual price required
        if (!self.is_manual_pricing_user() && !self.is_manual_pricing_finished() && self.is_manual_pricing_required() && self.isBasketEditable())
            return true;
        return false;
    });
    self.showRequestSave = ko.pureComputed(function(){
        // for pricer, when pricing hasn't finihing
        if (self.is_manual_pricing_user() && !self.is_manual_pricing_finished() && self.is_manual_pricing_required())
            return true;
        return false;
    });
    self.showRequestSendMessage = ko.pureComputed(function(){
        if (!self.is_manual_pricing_user() && !self.is_manual_pricing_finished() && self.is_manual_pricing_required() && !self.isBasketEditable())
            return true;
        return false;
    });
    self.showPriceSetMessage = ko.pureComputed(function(){
        if (self.isProjectOrdered()) {
            return false;
        }
        if (!self.is_manual_pricing_user() && self.is_manual_pricing_finished() && self.is_manual_pricing_required()) {
            return true;
        }
        return false;
    });
    self.showPriceNeedMessage = ko.pureComputed(function(){
        if (!self.is_manual_pricing_finished() && self.is_manual_pricing_required())
            return true;
        return false;
    });
    self.showPriceFinishedMessage = ko.pureComputed(function(){
        if (self.is_manual_pricing_user() && self.is_manual_pricing_finished() && self.is_manual_pricing_required())
            return true;
        return false;
    });

    self.userIsOwner = function userIsOwner() {
        if (self.projectPermissionVM.ownPermission() !== undefined) {
            var projectOwnerEmail = self.projectPermissionVM.ownPermission().contact.email;
            return djangoData.requestUser.email === projectOwnerEmail;
        }
        return false;
    };

    /**
     * For user, show order button.
     */
    self.showOrderButton = ko.pureComputed(function showOrderButton() {
        if (djangoData.basketNoOwner && djangoData.requestUserEmail === undefined) {
            return true;
        }
        if (!self.is_manual_pricing_user() &&
            (self.is_manual_pricing_required() && self.is_manual_pricing_finished() || !self.is_manual_pricing_required())) {
            return true;
        }
        return false;
    });
    self.isBasketEditable = ko.pureComputed(function(){
        // control the project edit
        if (self.is_frozen())
            return false;
        return true;
    });

    self.projectPermissionVM = new ProjectPermissionVM(projectId, self);

    self.supplierHasAccess = ko.observable(false);

    project_api.get_project(projectId, self.currency(), function(data) {
            self.title(data.title);
            self.canEditTitle = true;
            self.reference(data.reference);
            self.creation_date(data.date_created);
            self.owner_email(data.owner_email);
            self.quotation_number(data.quotation_number);
            self.basket_attachments(data.attachments);

            // Price Request Data
            self.is_manual_pricing_user(data.is_manual_pricing_user);
            self.is_manual_pricing_required(data.is_manual_pricing_required);
            self.has_manual_price_set(data.has_manual_price_set);
            self.is_frozen(data.is_frozen);
            self.is_manual_pricing_finished(data.is_manually_priced);
            self.manual_pricing_currency(data.manual_pricing_currency);
            self.price(data.price);
            self.subtotal(data.subtotal);
            self.extra_fees(data.extra_fees);
            self.show_net_prices(data.show_net_prices);
            self.min_price(data.min_price);
            self.diff_to_min_price(data.diff_to_min_price);
            self.voucher_discount(data.voucher_discount);
            self.tax_type(data.tax_type);
            var lines = data.lines;
            var nbLines = lines.length;
            var projectLinesVMArray = [];
            for (var i = 0; i < nbLines; ++i) {
                var lineVM = new ProjectLineVM(lines[i].id, self);
                projectLinesVMArray.push(lineVM);
            }
            self.projectLines(projectLinesVMArray);
            self.project_lines_are_loading(false);
            self.enableCurrencyChanged = true;

            // Project Status
            self.status(data.status)
            if (self.status() === 'Submitted') {
                self.is_frozen(true);
            }
    });
    // Operations
    self.formatted_creation_date = ko.pureComputed(function(){
        return date_formatted_lang(self.creation_date());
    });

    /**
     * Will describe if an analysis of one line failed.
     * @returns {boolean} True if one or more lines has failed to be analysed. If all lines have been analysed it will
     * return false.
     */
    self.hasSomeLineAnalysisFailed = function hasSomeLineAnalysisFailed() {
        return self.projectLines().some(function hasAnalysisFailed(projectLine) {
            return !projectLine.isAnalysing() && projectLine.error_not_printable();
        });
    };

    self.is_orderable = ko.pureComputed(function() {
        if (self.hasSomeLineAnalysisFailed() || !self.order().showPricePreview()) {
            return false;
        }
        var nbLines = self.projectLines().length;
        return self.order().same_supplier() && self.order().number_of_items() >=  nbLines;
    }, self);

    /**
     * Will check if line is manual priced, but the price was not set by the pricer yet.
     * @param {Object} projectLine  Project Line which should be checked for the condition.
     * @returns {boolean} True if manual pricing is required but the price was not set. For non manual priced lines and
     * manual priced lines, where the pricer has set a price, it will return false.
     */
    self.lineNeedsToBePriced = function lineNeedsToBePriced(projectLine) {
        return projectLine.isManualPricingRequired() && !projectLine.has_manual_price_set();
    };

    self.someManualPricedLineNotPriced = function someManualPricedLineNotPriced() {
        var projectLines = self.projectLines();
        // We will check if some line still needs to be priced. self.lineNeedsToBePriced is a function.
        return projectLines.some(self.lineNeedsToBePriced);
    };

    self.quotationVisible = ko.computed(function quotationVisible() {
        return self.is_orderable() && !self.someManualPricedLineNotPriced();
    });

    self.showWarningPrintabilityModal = function() {
        Button3d.modals.show_material_not_printable_modal();
    };

    self.number_items_error = ko.observable(false);
    self.same_supplier_error = ko.observable(false);

    self.display_missing_order_info = function(data, event) {
        if (self.order().warningState().type !== 0) {
            self.order().highlightError(true);
        }
    };

    /**
     * Show attachment section when only project has attachment.
     */
    self.showProjectAttachment = ko.computed(function () {
        return self.basket_attachments().some(function (item) {
            return item.basket_line_id === null;
        });
    });

    // Listeners
    self.reference.subscribe(function onNewReference(newReference) {
        var ref = null;
        if (newReference) {
            ref = newReference ? newReference : -1;
        }
        project_api.set_project_reference(projectId, ref);
    });

    self.title.subscribe(function (newTitle) {
        if (!self.canEditTitle || !newTitle || newTitle === "")
            return;

        project_api.set_project_title(projectId, newTitle);
    });

    // Currency
    $(document).ready(function() {
        // initialization which uses the DOM needs to be placed here
        self.currency($('.initial-currency').text());
    });

    function updateCurrency(currency){
        var selected_currency = currency;
       //send currency to server to save it
        $.ajax({
            type: 'POST',
            url: '/change_site_configuration/',
            data: {
                "currency": selected_currency
            }
        }).done(function() {
            self.currency(selected_currency);
        });
    }

    self.apply_to_all = function(lineToClone) {
        self.projectLines().forEach(function (line) {
            if (lineToClone.id === line.id) {
                return;
            }
            line.apply(lineToClone);
        })
    };

    self.print_quotation = function(quotation_url) {
        quotation_url += '?currency=' + self.currency();
        if (self.isProjectOrdered()) {
            window.open(quotation_url, '_blank');
        } else {
            // Update line information of django with displayed data
            // In case of tiemout, network failure...
            var nbLines = self.projectLines().length;
            var ajax_calls = [];
            for (var i = 0; i < nbLines; ++i) {
                var currentProjectLine = self.projectLines()[i];
                ajax_calls.push(
                    project_api.set_line_fields(
                        projectId,
                        currentProjectLine.id,
                        currentProjectLine.preSelectedMaterial(),
                        currentProjectLine.selectedSupplier().slug(),
                        currentProjectLine.postProcessingsReducer(
                            currentProjectLine.finishingOffers()
                        ),
                        self.currency()
                    )
                );
            }

            $.when.apply($, ajax_calls).then(function() {
                window.open(quotation_url, '_blank');
            });
        }
    };
    self.requestPrice = function() {
        Button3d.modals.show_price_request_modal();
    };
}
