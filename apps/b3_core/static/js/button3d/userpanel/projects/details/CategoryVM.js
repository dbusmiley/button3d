function CategoryVM(data) {
    var self = this;

    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);

    self.materialOffers = ko.observableArray();

    self.materialOffersSorted = ko.pureComputed(function () {
        return self.materialOffers().sort(function(l, r) {
            return l.title().toLowerCase() < r.title().toLowerCase() ? -1 : 1;
        });
    }, self);

    self.add_material_offer = function(matOfferVM) {
        self.materialOffers.push(matOfferVM);
    };
}
