/* global setCurrency */
function OrderVM(projectVM) {
    var self = this;

    function is_line_in_order(lineVM) {
        if (!lineVM)
            return false;
        if (lineVM.selectedSupplier() && lineVM.quantity() > 0)
            return true;
    }

    self.show_net_prices = ko.pureComputed(function() {
        return projectVM.show_net_prices()
    }, self);

    self.same_supplier = ko.pureComputed(function() {
        var supplier_slug = null;
        var nbLines = projectVM.projectLines().length;
        for (var i = 0; i < nbLines; ++i) {
            var lineVM = projectVM.projectLines()[i];
            if (is_line_in_order(lineVM)) {
                if (supplier_slug === null)
                    supplier_slug = lineVM.selectedSupplier().slug();
                else if (supplier_slug !== lineVM.selectedSupplier().slug())
                    return false;
            }
        }
        return supplier_slug ? true : false;
    }, self);

    self.supplier_tag = ko.pureComputed(function() {
        if (self.same_supplier()) {
            // Find the first line with a set finishing
            var supplier_slug = {title: '', logo_url: ''};
            for (var i =0 ; i < projectVM.projectLines().length; ++i){
                if (is_line_in_order(projectVM.projectLines()[i])){
                    supplier_slug.title = projectVM.projectLines()[i].selectedSupplier().partner() || '';
                    supplier_slug.logo_url = projectVM.projectLines()[i].selectedSupplier().logo_url() || '';
                    return supplier_slug;
                }
            }
        }
    }, self);

    self.no_supplier_selected = ko.pureComputed(function(){
        var nbLines = projectVM.projectLines().length;
        for (var i = 0; i < nbLines; ++i){
            var lineVM = projectVM.projectLines()[i];
            if (!lineVM.selectedSupplier())
                return true;
        }
        return false;
    });

    self.number_of_items = ko.pureComputed(function() {
        var count = 0;

        var nbLines = projectVM.projectLines().length;
        for (var i = 0; i < nbLines; ++i) {
            var lineVM = projectVM.projectLines()[i];
            if (is_line_in_order(lineVM)) {
                count += parseInt(lineVM.quantity());
            }
        }

        if (count > 0) {
            projectVM.number_items_error(false);
        }

        return count;
    }, self);

    self.can_be_printed = ko.pureComputed(function() {

        var nbLines = projectVM.projectLines().length;
        var can_be_printed = true;
        for (var i = 0; i < nbLines; ++i) {
            var lineVM = projectVM.projectLines()[i];
            if (is_line_in_order(lineVM) && !lineVM.is_printable())
                can_be_printed = false;
        }
        return can_be_printed;
    }, self);

    self.delivery_time = ko.pureComputed(function() {
        if (projectVM.isProjectOrdered()) {
            return orderDeliveryDaysRange;
        }
        var minDays = 0;
        var maxDays = 0;

        var nbLines = projectVM.projectLines().length;
        for (var i = 0; i < nbLines; ++i) {
            var lineVM = projectVM.projectLines()[i];
            if (is_line_in_order(lineVM)) {
                var range = lineVM.deliveryDaysValues();
                if (range[0] > minDays) {
                    minDays = range[0]
                }
                if (range[1] > maxDays) {
                    maxDays = range[1]
                }
            }
        }

        return minDays + ' - ' + maxDays + ' ' + gettext('days');
    }, self);

    self.shipping_costs_value_incl_tax = ko.pureComputed(function() {
        if (projectVM.isProjectOrdered()) {
            return parseFloat(orderShippingCostsInclTax);
        }
        if (self.same_supplier()) {
            // Find the first line with a set finishing
            for (var i = 0; i < projectVM.projectLines().length; i++) {
                if (is_line_in_order(projectVM.projectLines()[i])) {
                    return parseFloat(projectVM.projectLines()[i]
                        .selectedSupplier()
                        .shipping_price_incl_tax());
                }
            }
        }
        return 0;
    }, self);

    self.shipping_costs_value_excl_tax = ko.pureComputed(function() {
        if (projectVM.isProjectOrdered()) {
            return parseFloat(orderShippingCostsExclTax);
        }
        if (self.same_supplier()) {
            // Find the first line with a set finishing
            for (var i = 0; i < projectVM.projectLines().length; i++) {
                if (is_line_in_order(projectVM.projectLines()[i])) {
                    return parseFloat(projectVM.projectLines()[i]
                        .selectedSupplier()
                        .shipping_price_excl_tax());
                }
            }
        }
        return 0;
    }, self);

    self.shipping_costs_display = ko.pureComputed(function () {
        var shippingCost = self.show_net_prices() ? self.shipping_costs_value_excl_tax() : self.shipping_costs_value_incl_tax();
        return shippingCost !== 0 ? setCurrency(shippingCost, projectVM.currency()) : "--- ";
    }, self);

    self.min_price = ko.pureComputed(function() {
        if (!projectVM.min_price())
            return 0;
        var min = self.show_net_prices() ? projectVM.min_price().excl_tax : projectVM.min_price().incl_tax;
        if (min) {
            return parseFloat(min);
        }
    }, self);

    self.min_price_display = ko.pureComputed(function() {
        return setCurrency(self.min_price().toFixed(2), projectVM.currency());
    }, self);

    self.difference_to_min_price = ko.pureComputed(function() {
        var difference = projectVM.diff_to_min_price();
        if (!difference) {
            return 0;
        }
        var differenceWithTaxType = self.show_net_prices() ? difference.excl_tax : difference.incl_tax;
        if (difference) {
            return parseFloat(differenceWithTaxType);
        }
        return 0;
    }, self);

    self.difference_to_min_price_display = ko.pureComputed(function() {
        return setCurrency(self.difference_to_min_price().toFixed(2), projectVM.currency());
    }, self);

    self.list_extra_fees = ko.pureComputed(function() {
        var fees = projectVM.extra_fees();
        if (!fees) {
            return null;
        }
        return fees.map(function(fee) {
            var feeWithTaxType = (self.show_net_prices() ? fee.excl_tax : fee.incl_tax);
            return {
                name: fee.name,
                value: setCurrency(feeWithTaxType, projectVM.currency())
            };
        });
    }, self);

    self.discount_sum_value = ko.pureComputed(function() {
        if (!projectVM.price() || !projectVM.price().discount)
            return 0;
        var discount = self.show_net_prices()? projectVM.price().discount.excl_tax : projectVM.price().discount.incl_tax;
        if (discount) {
            return parseFloat(discount);
        }
        return 0;
    }, self);

    self.discount_sum_display = ko.pureComputed(function() {
        return "- "+setCurrency(self.discount_sum_value().toFixed(2), projectVM.currency());
    }, self);

    self.total_price_value = ko.pureComputed(function() {
        if (projectVM.price() === null)
            return 0;
        var price = projectVM.price().incl_tax;
        var sum = parseFloat(price);
        var shipping = self.shipping_costs_value_incl_tax();
        return sum + shipping
    }, self);

    self.total_price_display = ko.pureComputed(function() {
        return setCurrency(self.total_price_value().toFixed(2), projectVM.currency());
    }, self);

    self.vat_rate = ko.pureComputed(function() {
        var tax_rate = projectVM.price().tax_rate;
        return tax_rate;
    }, self);

    self.total_vat_value = ko.pureComputed(function() {
        if (!projectVM.price())
            return 0;
        var tax = projectVM.price().tax;
        var item_vat = parseFloat(tax);
        var shipping_tax_inclusive =  self.shipping_costs_value_incl_tax() - self.shipping_costs_value_excl_tax();
        return item_vat + shipping_tax_inclusive;
    }, self);

    // Fetch tax_type from parent project
    self.tax_type = projectVM.tax_type;

    // display tax fee
    self.total_vat_display = ko.pureComputed(function() {
        return setCurrency(self.total_vat_value().toFixed(2), projectVM.currency());
    }, self);

    self.voucher_discount_value = ko.pureComputed(function() {
        return self.show_net_prices() ? projectVM.voucher_discount().excl_tax : projectVM.voucher_discount().incl_tax;
    }, self);

    self.voucher_discount_display = ko.pureComputed(function() {
        return setCurrency(parseFloat(self.voucher_discount_value()).toFixed(2), projectVM.currency());
    }, self);

    self.total_net_price_value = ko.pureComputed(function() {
        if (projectVM.price() === null)
            return 0;
        return parseFloat(projectVM.price().excl_tax) + self.shipping_costs_value_excl_tax()
    }, self);

    // display net price
    self.total_net_price_display = ko.pureComputed(function() {
        return setCurrency(self.total_net_price_value().toFixed(2), projectVM.currency());
    }, self);

    self.item_price_value = ko.pureComputed(function() {
        var subtotal = projectVM.subtotal();
        if (!subtotal)
            return 0;
        var subtotalWithTaxType = self.show_net_prices() ? subtotal.excl_tax : subtotal.incl_tax;
        return parseFloat(subtotalWithTaxType)
    }, self);

    /**
     * Display representation of the item price. The item price includes the Net Price and the tax added together. This
     * computed property will give a nice representation of the value.
     * @returns {String} Returns the representation string of the display item price.
     */
    self.item_price_display = ko.pureComputed(function getItemPrice() {
        return setCurrency(self.item_price_value().toFixed(2), projectVM.currency());
    }, self);

    // Show price only when all line set and has same supplier
    self.showPricePreview = ko.pureComputed(function() {
        if (projectVM.hasSomeLineAnalysisFailed() || self.no_supplier_selected()) {
            return false;
        }
        return self.same_supplier()
    }, self);

    self.highlightError = ko.observable(false);

    function getLineSelection(currentSelected, preSelected) {
        var isNotCurrentlySelected = (
            currentSelected === undefined ||
            currentSelected === null ||
            currentSelected === 0
        )
        var isNotPreselected = (
            preSelected === undefined ||
            preSelected === 0 ||
            currentSelected === null ||
            currentSelected === 0
        )
        if (isNotCurrentlySelected && isNotPreselected) {
            return false;
        }
        if (preSelected !== undefined && preSelected !== null) {
            return true;
        }
        return false;
    }

    /**
     * State which will describe if there is an error currently in the project. It will then return different types of
     * errors containing additional information.
     * The change listener is set on the project lines.
     * States existing are:
     * 0 - Default state. No error.
     * 1 - No Material selected in some line.
     * 2 - No Supplier selected in some line.
     * 3 - Different Suppliers selected.
     * 4 - No finishing selected in some line.
     * 5 - No reference given for project.
     * 6 - No 3d file given.
     * 7 - An analysis failed for one item line.
     */
    self.warningState = ko.computed(function () {
        // Reset Highlight Error because line is changed when this function is called.
        self.highlightError(false);

        var projectLines = projectVM.projectLines();
        var errorDict = {
            0: { type: 0, message: '' },
            1: { type: 1, message: gettext('Select a Print Material.') },
            2: { type: 2, message: gettext('Select a Supplier.') },
            3: { type: 3, message: gettext('You can only order from one 3D printing service.') },
            4: { type: 4, message: gettext('Select a Post-process.') },
            5: { type: 5, message: gettext('Your 3D project must include a reference to be ordered.') },
            6: { type: 6, message: gettext('You need to add at least one 3D-File.') },
            7: { type: 7, message: gettext('Model Analysis Failed. Please Remove the Item or Upload a Replacement.') },
            8: {
                type: 3,
                message: gettext(
                    'The selected materials are manufactured in different company locations. ' +
                    'To place your order, please select a different material combination or ' +
                    'create separate orders for different materials.'
                )
            }
        };

        function getLineWithoutMaterialSelection(projectLine) {
            return getLineSelection(projectLine.selectedMaterial(), projectLine.preSelectedMaterial());
        }

        function getLineWithoutSupplierSelection(projectLine) {
            return getLineSelection(projectLine.selectedSupplier(), projectLine.preSelectedSupplier());
        }

        function getLineWithoutFinishingSelection(projectLine) {
            return true;
        }

        function getSuppliers(projectLinesParam) {
            return projectLinesParam.reduce(function (newArray, projectLine) {
                var partner = null;
                if (projectLine.preSelectedSupplier() && projectLine.preSelectedSupplier().partner !== undefined) {
                    partner = projectLine.preSelectedSupplier().partner();
                }
                if (projectLine.selectedSupplier() && projectLine.selectedSupplier().partner !== undefined) {
                    partner = projectLine.selectedSupplier().partner();
                }
                if (partner !== null) {
                    newArray.push(partner);
                }
                return newArray;
            }, []);
        }

        function getUniqueSuppliers(suppliersParam) {
            function onlyUnique(value, index, selfReference) {
                return selfReference.indexOf(value) === index;
            }
            return suppliersParam.filter(onlyUnique);
        }

        // We need to check if hasSomeLineAnalysisFailed was initialized.
        if (typeof (projectVM.hasSomeLineAnalysisFailed) === 'function' &&  projectVM.hasSomeLineAnalysisFailed()) {
            return errorDict[7];
        }

        if (projectLines.length === 0) {
            return errorDict[6];
        }

        if (projectLines.length !== projectLines.filter(getLineWithoutMaterialSelection).length) {
            return errorDict[1];
        }

        if (projectLines.length !== projectLines.filter(getLineWithoutSupplierSelection).length && !is_single_supplier_account) {
            return errorDict[2];
        }

        // Supplier difference Check.
        var suppliers = getSuppliers(projectLines);
        if (getUniqueSuppliers(suppliers).length > 1) {
            if (is_single_supplier_account) {
                return errorDict[8];
            }
            return errorDict[3];
        }

        if (projectLines.length !== projectLines.filter(getLineWithoutFinishingSelection).length) {
            return errorDict[4];
        }

        // Default State.
        return errorDict[0];
    }, projectVM);
}
