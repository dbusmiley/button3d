function ProjectLineVM(lineId, projectVM) {
  var self = this;
  self.projectVM = projectVM;
  self.categories_by_id = {};
  self.categories_by_name = {};
  // --- Project Information ---
  self.id = lineId;
  self.attrId = ko.observable(lineId);
  self.uuid = ko.observable();
  self.fileName = ko.observable();
  self.show_net_prices = ko.observable();
  self.add_date = ko.observable();
  self.quantity = ko.observable();
  self.scale = ko.observable(); // Always in mm
  self.configure_url_value = ko.observable();
  self.thumbnail_url = ko.observable();
  self.requests_ajax = 0;
  self.d = ko.observable();
  self.w = ko.observable();
  self.h = ko.observable();
  self.measure_unit = ko.observable();
  self.configure_url = ko.pureComputed(function() {
    var config_url = [self.configure_url_value()];
    if (!self.configure_url_value()) return '#';
    if (!endsWith(self.configure_url_value(), '/')) config_url.push('/');
    if (!endsWith(self.configure_url_value(), '#/')) config_url.push('#/');
    return config_url.join('');
  }, self);
  self.configUrlWithOpenPanel = ko.pureComputed(function() {
    var url = [self.configure_url_value()];
    if (!self.configure_url_value()) return '?fromproject=true';
    if (!endsWith(self.configure_url_value(), '/')) url.push('/');
    if (!endsWith(self.configure_url_value(), '#/'))
      url.push('?fromproject=true');
    return url.join('');
  }, self);

  // --- Selector ---
  self.materialOffers = ko.observableArray([]);
  self.categoriesOffers = ko.observableArray();
  self.selectedMaterial = ko.observable(null);
  self.materialSelectPlaceHolder = ko.observable('Choose a material...');
  self.preSelectedMaterial = ko.observable(null);
  self.categoriesOffersSorted = ko.pureComputed(function() {
    return self
      .categoriesOffers()
      .filter(function(item) {
        return item.materialOffers().length > 0;
      })
      .sort(function(l, r) {
        return l.name().toLowerCase() < r.name().toLowerCase() ? -1 : 1;
      });
  }, self);
  self.materialDropdown = null;
  self.materialDropdownClicked = ko.observable(false);

  self.supplierOffers = ko.observableArray([]);
  self.selectedSupplier = ko.observable(null);
  self.combinablePostProcessings = ko.observable(false);
  self.preSelectedSupplier = ko.observable(null);
  self.showSupplier = ko.observable(false);
  self.supplierSelectPlaceHolder = ko.observable(supplierPlaceholder);
  self.supplierOffersSorted = ko.pureComputed(function() {
    return self.supplierOffers().sort(function(l, r) {
      return l.partner().toLowerCase() < r.partner().toLowerCase() ? -1 : 1;
    });
  }, self);
  self.supplierErrorMsg = ko.observable(null);
  self.isSingleSupplierAccount = ko.observable(is_single_supplier_account);
  self.isSupplierLoading = ko.observable(false);

  self.finishingOffers = ko.observableArray([]);
  self.preSelectedPostProcessings = ko.observableArray([]);
  self.itemCompare = function(a, b) {
    return JSON.stringify(a) == JSON.stringify(b);
  };
  self.showFinish = ko.observable(false);
  self.finishingSelectPlaceHolder = ko.observable(finishingPlaceholder);
  self.isFinishingLoading = ko.observable(false);

  self.currencyListenerEnabled = false;

  // --- Printability ---
  self.isAnalysing = ko.observable(true);
  self.error_not_printable = ko.observable(false);
  self.is_printable = ko.observable(null);
  self.printabilityText = ko.observable();
  self.printabilityDict = {
    printable: { text: gettext('Printable'), value: true },
    warning: { text: gettext('Suspicious'), value: true },
    not_printable: { text: gettext('Not Printable'), value: false },
    unknown: { text: gettext('Unknown'), value: true }
  };
  self.analysingDetail = ko.observable('');
  self.printability = ko.observable(null);
  self.jobErrorMessage = ko.observable(null);
  self.errorMessagesDict = {
    E_EMPTY: gettext('Empty error.'),
    E_TOO_LARGE: gettext('File too large'),
    E_INVALID_FILE: gettext('Invalid file.'),
    E_JSON_FORMAT: gettext('Json format error.'),
    E_INVALID_UUID: gettext('Invalid uuid.'),
    E_INVALID_PARAM: gettext('Invalid parameter.'),
    E_TIMEOUT: gettext('Timeout.'),
    E_STL_CONVERSION: gettext('STL conversion failed'),
    E_WRL: gettext('WRL conversion failed'),
    E_PLY_CONVERSION: gettext('PLY conversion failed'),
    E_ZIP_NOT_SUPPORTED: gettext('ZIP format not supported'),
    E_INTERNAL_ERROR: gettext('An error occurred.')
  };
  self.analysingDetailDict = {
    converting: gettext('Converting...'),
    optimising: gettext('Optimizing...'),
    analysing: gettext('Analyzing...')
  };

  // --- Price Module ---
  self.price = ko.observable();
  self.discount = ko.observable();
  self.unitPrice = ko.computed(function() {
    return self.price() ? self.price() : 0;
  });
  self.lineDiscountPriceValue = ko.computed(function() {
    if (!self.discount() || !Number(self.discount())) {
      return 0
    }
    return self.discount();
  });
  self.lineDiscountPriceRepresentation = ko.computed(function() {
    return "-" + setCurrency(self.lineDiscountPriceValue(), projectVM.currency());
  });
  self.unitPriceRepresentation = ko.computed(function() {
    return setCurrency(self.unitPrice(), projectVM.currency());
  });
  self.totalLinePriceRepresentation = ko.pureComputed(function() {
    var net = self.unitPrice() && self.quantity() ? self.unitPrice() * self.quantity() : 0;
    var discount = self.lineDiscountPriceValue() ? self.lineDiscountPriceValue() : 0;
    return setCurrency(net - discount, projectVM.currency());
  });
  self.has_manual_price_set = ko.observable();
  self.isManualPricingRequired = ko.pureComputed(function() {
    if (
      self.selectedSupplier() &&
      self.selectedSupplier().manual_pricing_required
    ) {
      return true;
    }
    for (var i = 0; i < self.finishingOffers().length; i++) {
      if (
        self.finishingOffers()[i].manualPricingRequired &&
        self.finishingOffers()[i].isSelected()
      ) {
        return true;
      }
    }
    return false;
  });
  self.manual_price = ko.observable();
  self.manualRequestPrice = ko.observable();
  self.manualRequestPrice.subscribe(function(newValue) {
    var postData = {};
    if (newValue && newValue !== '') {
      if (isNaN(newValue)) {
        return self.manualRequestPrice('Please insert number');
      }
      postData[lineId] = newValue;
      project_api.set_manual_prices(projectId, postData);
    }
  });
  self.showRequestMessage = ko.pureComputed(function() {
    return !projectVM.is_manual_pricing_user() &&
      !self.has_manual_price_set() &&
      self.isManualPricingRequired()
      ? true
      : false;
  }, self);
  self.showRequestInput = ko.pureComputed(function() {
    return projectVM.is_manual_pricing_user() &&
      self.isManualPricingRequired() &&
      !projectVM.is_manual_pricing_finished()
      ? true
      : false;
  }, self);
  self.showPriceTag = ko.pureComputed(function() {
    return !self.isManualPricingRequired() ||
      projectVM.is_manual_pricing_finished()
      ? true
      : false;
  }, self);

  // --- Delivery Module ---
  self.baseDeliveryMinDays = ko.observable(null);
  self.baseDeliveryMaxDays = ko.observable(null);
  self.deliveryDaysValues = ko.pureComputed(function() {
    var minDays = Number(self.baseDeliveryMinDays());
    var maxDays = Number(self.baseDeliveryMaxDays());
    for (var i = 0; i < self.finishingOffers().length; i++) {
        if (self.finishingOffers()[i].isSelected()) {
            minDays +=
                Number(self.finishingOffers()[i].delivery_days_min()) -
                Number(self.baseDeliveryMinDays());
            maxDays +=
                Number(self.finishingOffers()[i].delivery_days_max()) -
                Number(self.baseDeliveryMaxDays());
        }
    }
    return [minDays, maxDays]
    });

  self.deliveryDaysRange = ko.pureComputed(function() {
      var days = self.deliveryDaysValues();
      var minDays = days[0];
      var maxDays = days[1];
      return minDays + ' - ' + maxDays + ' ' + gettext('days');
  });

  // --- Attachment Module ---
  self.lineAttachment = ko.pureComputed(function() {
    var attachementLength = projectVM.basket_attachments().length;
    var lineAttachmentArrary = [];
    for (var i = 0; i < attachementLength; ++i) {
      if (projectVM.basket_attachments()[i].basket_line_id === lineId) {
        lineAttachmentArrary.push(projectVM.basket_attachments()[i]);
      }
    }
    return lineAttachmentArrary;
  });

  // --- Error Handing ---
  self.serverError = ko.observable('');
  self.uploadStatusError = ko.observable('');
  self.onApiError = function onApiError(errorData) {
    if (errorData.status === 500) {
      var errorInstance = JSON.parse(errorData.responseText);
      self.showApiErrorMessage(errorInstance.error);
    }
  };
  self.showApiErrorMessage = function showApiErrorMessage(message) {
    self.serverError(message);
    self.isAnalysing(false);
  };

  for (var i = 0; i < material_categories.length; ++i) {
    var vm = new CategoryVM(material_categories[i]);
    self.categories_by_id[material_categories[i].id] = vm;
    self.categories_by_name[material_categories[i].name] = vm;
    self.categoriesOffers.push(vm);
  }

  $(document).ready(function() {
    getProjectLineInfo();
  });
  function priceToUse(price_object){
      if (!price_object){
          return 0;
      }
      return projectVM.show_net_prices() ? price_object.excl_tax : price_object.incl_tax
  }
  // --- API Call Action ---
  function getProjectLineInfo() {
    project_api.get_project_line(
      projectId,
      lineId,
      projectVM.currency(),
      function(data) {
        if (data.success) {
          self.uuid(data.stl_file);
          self.price(priceToUse(data.price));
          self.discount(priceToUse(data.discount));
          self.fileName(data.showname);
          self.add_date(data.stl_file_creation_date);
          self.quantity(data.quantity);
          self.scale(data.scale);
          self.configure_url_value(data.configure_url);
          self.manual_price(data.price);
          self.measure_unit(data.measure_unit);
          self.has_manual_price_set(data.has_manual_price_set);
          self.preSelectedMaterial(data.product_slug);
          self.preSelectedSupplier(data.supplier_slug);
          self.preSelectedPostProcessings(data.post_processings);
          self.materialDropdown = $('select[data-id=' + self.id + ']');
          self.currencyListenerEnabled = true;
          getLineStatus(data);
        } else {
          lazyLoadImg();
          self.error_not_printable(true);
          self.isAnalysing(false);
        }
      },
      self.onApiError
    );
  }
  function updateProjectLinePrices(data) {
    self.price(priceToUse(data.price));
    self.discount(priceToUse(data.discount));
  }

  function init() {
    var materialTitle = projectVM.materialsBySlug[self.preSelectedMaterial()];
    self.materialSelectPlaceHolder(
      materialTitle ? materialTitle.title : gettext('Choose a material...')
    );
    self.supplierSelectPlaceHolder(gettext('Choose a supplier...'));
    self.finishingSelectPlaceHolder(gettext('Choose a finishing...'));
    if (self.preSelectedMaterial()) {
      updateSupplierList(
        self.preSelectedMaterial(),
        self.preSelectedSupplier()
      );
    }
  }
  self.initByClick = function initByClick() {
    if (!projectVM.isBasketEditable()) return;
    self.materialDropdown.select2('open');
    if (!self.materialDropdownClicked()) {
      $('.project-dropdown-loading').remove(); // remover previous loading animation
      $('.select2-results__option').hide();
      $('.select2-results').after(
        '<div class="project-dropdown-loading"><i class="fa fa-spinner fa-pulse fa-2x" aria-hidden="true"></i></div>'
      );
      updateMaterialList(self.preSelectedMaterial());
    }
  };

  self.getLineStatusCounter = 1;
  function getLineStatus(data) {
    self.uploadStatusError('');
    self.analysingDetail(self.analysingDetailDict[data.analysing_detail]);
    // finished analyse, save d-w-h
    if (data.status === 'finished') {
      self.error_not_printable(false);
      lazyLoadImg(data.thumbnail_url);
      if (data.dimensions) {
        self.d(data.dimensions.d);
        self.w(data.dimensions.w);
        self.h(data.dimensions.h);
      }
      finishProgressBar();
      setTimeout(function() {
        self.isAnalysing(false);
      }, 1000);
      init();
    }
    // stil analyse, set-time out get infomation again, add the counter
    if (data.status === 'analysing') {
      setTimeout(function() {
        project_api.get_project_line(projectId, lineId, null, function(data) {
          getLineStatus(data);
        });
      }, 5000);
      self.getLineStatusCounter++;
    }
    // error, set flag
    if (data.status === 'error') {
      lazyLoadImg(null);
      self.error_not_printable(true);
      self.jobErrorMessage(
        data.error
          ? self.errorMessagesDict[data.error]
          : self.errorMessagesDict[self.errorMessagesDict.length - 1]
      );
      finishProgressBar();
      setTimeout(function() {
        self.isAnalysing(false);
      }, 1000);
    }
  }
  // Load Img when it's ready
  function lazyLoadImg(url) {
    var imgPreload = new Image();
    var imgElement = $('#' + self.attrId() + '-img');
    if (url) {
      imgPreload.src = url;
      $(imgPreload)
        .on('load', function() {
          imgElement.replaceWith(imgPreload);
        })
        .on('error', function() {
          svgFallback(imgElement);
        });
    } else {
      svgFallback(imgElement);
    }
  }

  function finishProgressBar() {
    var progress_bar = $('#' + self.attrId() + '-progress-bar');
    progress_bar.css('animation-play-state', 'paused');
    var progress_width =
      progress_bar.width() / progress_bar.parent().width() * 100 + '%';
    progress_bar
      .removeClass('progress-animation')
      .css('width', progress_width)
      .animate(
        {
          width: '100%',
        },
        {
          duration: 500,
          easing: 'easeInQuint',
          complete: function() {
            progress_bar.fadeOut().css('border', 'none');
            progress_bar
              .parent()
              .fadeOut()
              .css('border', 'none');
          }
        }
      );
  }

    function updateProjectInfo() {
        project_api.get_project(projectId, projectVM.currency(), function(data) {
            projectVM.price(data.price);
            projectVM.extra_fees(data.extra_fees);
            projectVM.subtotal(data.subtotal);
            projectVM.min_price(data.min_price);
            projectVM.diff_to_min_price(data.diff_to_min_price);
            projectVM.tax_type(data.tax_type);
            projectVM.has_manual_price_set(data.has_manual_price_set);
            projectVM.is_manual_pricing_required(data.is_manual_pricing_required);
        });
    }

  function handleNoMaterialOffers() {
    self.materialDropdown.select2('close');
    self.supplierErrorMsg(
      gettext(
        'The 3D model is too big or small to be printed in the selected material. Try to ' +
          'change the scale of the 3D Model or select another material.'
      )
    );
    self.selectedMaterial(null);
  }

  // --- Update Functions ---
  function updateMaterialList(material_slug) {
    compare_api.get_material_offers(
      self.uuid(),
      self.scale(),
      projectVM.currency(),
      function(data) {
        // Error Handler.
        if (data.results.length === 0) {
          handleNoMaterialOffers();
          return;
        }

        // Continue with main logic.
        var results = data.results;
        var nbResults = results.length;
        var materialOfferVMArray = [];
        var selectedMaterialVM;
        for (var i = 0; i < nbResults; ++i) {
          var materialOffer = results[i];
          if (materialOffer.amount_stockrecords === 0) {
            continue;
          }
          var materialOfferVM = new MaterialOfferVM(materialOffer, self);
          materialOfferVMArray.push(materialOfferVM);
        }
        self.materialOffers(materialOfferVMArray);
        self.selectedMaterial(selectedMaterialVM);
        self.materialDropdownClicked(nbResults ? true : false);
        $('.project-dropdown-loading').remove();
        $('.select2-results__option').show();
        self.materialDropdown.select2('close');
        self.materialDropdown.select2('open');
      },
      self.onApiError
    );
  }

  function updateSupplierList(material_slug, supplier_slug) {
    self.isSupplierLoading(true);
    self.showFinish(false);
    compare_api.get_supplier_offers(
      self.uuid(),
      self.scale(),
      projectVM.currency(),
      material_slug,
      function(data) {
        // Error Handler.
        if (data.results.length === 0) {
          handleNoMaterialOffers();
          return;
        }

        var results = data.results;
        var nbResults = results.length;
        var supplierOfferVMArray = [];
        var selectedSupplierVM;
        for (var i = 0; i < nbResults; ++i) {
          var supplierOffer = results[i];
          var supplierOfferVM = new SupplierOfferVM(supplierOffer, self);
          if (supplier_slug === supplierOfferVM.slug()) {
            selectedSupplierVM = supplierOfferVM;
          }
          supplierOfferVMArray.push(supplierOfferVM);
        }
        if (!supplierOfferVMArray.length) {
          self.supplierErrorMsg(gettext('No supplier available'));
        } else {
          self.supplierErrorMsg(null);
        }
        self.supplierOffers(supplierOfferVMArray);
        self.selectedSupplier(selectedSupplierVM ? selectedSupplierVM : null);
        updatePrintability(supplierOfferVMArray);
        self.showSupplier(true);
        self.finishingOffers([]);
        if (is_single_supplier_account && self.supplierOffers().length == 1)
          self.selectedSupplier(self.supplierOffers()[0]);
        self.isSupplierLoading(false);
        self.showFinish(true);
      },
      self.onApiError
    );
  }

  function updatePrintability(supplierOffers) {
    var printability = supplierOffers.length
      ? supplierOffers[0].printability()
      : 'unknown';
    self.printability(printability);
    self.printabilityText(self.printabilityDict[printability].text);
    self.is_printable(self.printabilityDict[printability].value);
  }

  function updateFinishingList(material_slug, partner_slug) {
    self.isFinishingLoading(true);
    compare_api.get_finishing_offers(
      self.uuid(),
      self.scale(),
      projectVM.currency(),
      material_slug,
      partner_slug,
      function(data, materialSlug) {
        var results = data.results;
        var nbResults = results.length;
        var finishingOfferVMArray = [];
        for (var i = 0; i < nbResults; ++i) {
          var finishingOffer = results[i];
          if (!finishingOffer.slug.endsWith(materialSlug + '-raw')) {
            var finishingOfferVM = new FinishingOfferVM(finishingOffer, self);
            finishingOfferVMArray.push(finishingOfferVM);
          }
        }
        self.finishingOffers(finishingOfferVMArray);
        self.postProcessingsExpander(self.preSelectedPostProcessings());
        self.showFinish(true);
        self.isFinishingLoading(false);
      },
      self.onApiError
    );
  }

  // --- Subscriber ---
  self.fileName.subscribeChanged(function(newValue, oldValue) {
    if (oldValue && newValue) {
      project_api.set_line_file_name(projectId, lineId, newValue);
    }
  });
  self.selectedMaterial.subscribe(function(newMaterialVM) {
    if (newMaterialVM === '' || typeof newMaterialVM === 'undefined') {
      // init() will go to this state and do nothing
      return;
    }
    if (newMaterialVM) {
      // newMaterial => set line to new material then update supplier list
      project_api.set_line_fields(
        projectId,
        lineId,
        newMaterialVM.slug(),
        null,
        null,
        projectVM.currency(),
        null
      );
      var nextSelectedSupplier = self.selectedSupplier()
        ? self.selectedSupplier().slug()
        : self.preSelectedSupplier();
      self.preSelectedMaterial(newMaterialVM.slug());
      updateSupplierList(newMaterialVM.slug(), nextSelectedSupplier);
    } else {
      // !newMaterial => set line to all null then disable other dropdown
      project_api.set_line_fields(
        projectId,
        lineId,
        null,
        null,
        null,
        projectVM.currency(),
        null
      );
      self.preSelectedMaterial(null);
      self.showSupplier(false);
      self.selectedSupplier(null);
      self.printability(null);
      self.showFinish(false);
      self.finishingOffers([]);
    }
  });

  self.selectedSupplier.subscribeChanged(function(newSupplierVM) {
    projectVM.same_supplier_error(false);
    if (newSupplierVM) {
      // newSupplierVM => set line to newSupplier then update finishing list
      var nextSelectedMaterial = self.selectedMaterial()
        ? self.selectedMaterial().slug()
        : self.preSelectedMaterial();
      if (!projectVM.isProjectOrdered()) {
        project_api.set_line_fields(
          projectId,
          lineId,
          nextSelectedMaterial,
          newSupplierVM.slug(),
          self.preSelectedPostProcessings(),
          projectVM.currency(),
          function(data) {
            updateProjectLinePrices(data);
            if (!data.success) {
              return;
            }
            updateProjectInfo();
          }
        );
      }
      self.baseDeliveryMinDays(newSupplierVM.delivery_min_days);
      self.baseDeliveryMaxDays(newSupplierVM.delivery_max_days);
      self.combinablePostProcessings(
        newSupplierVM.combinable_post_processings()
      );
      self.preSelectedSupplier(newSupplierVM.slug());
      self.finishingOffers([]);
      updateFinishingList(nextSelectedMaterial, newSupplierVM.slug());
      self.isFinishingLoading(false);
      if (window.projectCheckoutBridge && newSupplierVM) {
          window.projectCheckoutBridge.serviceId = newSupplierVM.partner_id;
      }
      return;
    }
    // !newSupplierVM => disable other dropdown
    self.showFinish(false);
    self.selectedSupplier(null);
  });

  self.updateColor = function(selectedColor) {
    if (!selectedColor()) {
      return false;
    }
    self.updatePostProcessings();
    return true;
  };

  self.updatePostProcessings = function() {
    project_api.set_line_fields(
      projectId,
      lineId,
      self.preSelectedMaterial(),
      self.preSelectedSupplier(),
      self.postProcessingsReducer(self.finishingOffers()),
      projectVM.currency(),
      function(data) {
        if (!data.success) {
          return false;
        }
        updateProjectLinePrices(data);
        updateProjectInfo();
      }
    );
    return true;
  };

  self.selectPostProcessing = function(selectedOffer) {
    if (!self.combinablePostProcessings()) {
      self.finishingOffers().forEach(function(offer) {
        offer.isSelected(false);
      });
    }
    selectedOffer.isSelected(true);
    self.updatePostProcessings();
    return true;
  };

  self.clearSelectedPostProcessings = function() {
    self.finishingOffers().forEach(function(offer) {
      offer.isSelected(false);
    });
    self.updatePostProcessings();
    return true;
  };

  self.postProcessingsReducer = function(postProcessingsArray) {
    var reducedArray = [];
    for (var i = 0; i < postProcessingsArray.length; i++) {
      if (postProcessingsArray[i].isSelected()) {
        reducedArray.push({
          slug: postProcessingsArray[i].slug(),
          color_id: postProcessingsArray[i].selectedColor()
        });
      }
    }
    return reducedArray;
  };

  self.postProcessingsExpander = function(postProcessingsArray) {
    postProcessingsArray.forEach(function(postProcessing) {
      var targetPostProcessing = self.finishingOffers().find(function(e) {
        return postProcessing.slug === e.slug();
      });
      if (!targetPostProcessing) {
        return;
      }
      targetPostProcessing.isSelected(true);
      targetPostProcessing.selectedColor(postProcessing.color_id);
    });
  };

  self.showPostProcessings = ko.pureComputed(function() {
    return self.finishingOffers().length > 0;
  });

  self.quantity.subscribe(function(newQuantity) {
    projectVM.number_items_error(false);
    if (projectVM.isProjectOrdered()) {
      updateProjectInfo();
    } else {
      project_api.set_line_quantity(
        projectId,
        lineId,
        newQuantity,
        projectVM.currency(),
        function(data) {
          updateProjectLinePrices(data);
          updateProjectInfo();
        }
      );
    }
  });

  // --- Operations ---
  self.formatted_add_date = ko.pureComputed(function() {
    return date_formatted_lang(self.add_date());
  }, self);

  self.switchMeasureUnit = function(unit) {
    if (!projectVM.isBasketEditable()) return;

    if (self.measure_unit() !== unit) {
      var newScale;
      if (unit === 'in') {
        newScale = self.scale() * Button3d.constants.INCH;
      } else {
        newScale = self.scale() / Button3d.constants.INCH;
      }
      self.scale(newScale);
    }
    self.measure_unit(unit);
    project_api.set_line_scale_and_unit(projectId, self.id, self.scale(), unit);
    init();
  };

  self.file_size = ko.pureComputed(function() {
    if (self.d() && self.w() && self.h()) {
      var d = self.d();
      var w = self.w();
      var h = self.h();
      var arr = [
        pointToComma(h.toFixed(2)),
        'x',
        pointToComma(w.toFixed(2)),
        'x',
        pointToComma(d.toFixed(2))
      ];
      return arr.join(' ');
    }
    return null;
  }, self);

  self.remove_line = function() {
    project_api.delete_line(projectId, lineId, function() {
      projectVM.projectLines.remove(function(item) {
        return item.id === lineId;
      });
      updateProjectInfo();
    });
  };

  self.clone_line = function() {
    project_api.clone_line(projectId, lineId, function(data) {
      projectVM.projectLines.push(new ProjectLineVM(data.pk, projectVM));
      updateProjectInfo();
    });
  };

  self.apply = function(other) {
    self.selectedMaterial(null);
    self.selectedSupplier(null);
    self.preSelectedMaterial(
      other.selectedMaterial()
        ? other.selectedMaterial().slug()
        : other.preSelectedMaterial()
    );
    self.preSelectedSupplier(
        other.selectedSupplier() ? other.selectedSupplier().slug() : null
    );
    self.preSelectedPostProcessings(other.postProcessingsReducer(other.finishingOffers()));
    self.materialDropdownClicked(false);
    self.switchMeasureUnit(other.measure_unit());
    self.quantity(other.quantity());
    init();
    self.applyPostProcessings(other.finishingOffers());
  };

  self.applyPostProcessings = function(processingsToBeCopied) {
    compare_api.get_finishing_offers(
      self.uuid(),
      self.scale(),
      projectVM.currency(),
      self.preSelectedMaterial(),
      self.preSelectedSupplier(),
      function(data) {
        var results = data.results;
        var finishingOfferVMArray = results
          .filter(function(finishingOffer) {
            return finishingOffer.slug.indexOf('-raw') < 0;
          })
          .map(function(finishingOffer) {
            return new FinishingOfferVM(finishingOffer, self);
          });
        self.finishingOffers(finishingOfferVMArray);
        processingsToBeCopied.forEach(function(otherProcessing) {
          self.finishingOffers().forEach(function(ownProcessing) {
            if (
              ownProcessing.slug() === otherProcessing.slug() &&
              otherProcessing.isSelected()
            ) {
              ownProcessing.isSelected(true);
              ownProcessing.selectedColor(otherProcessing.selectedColor());
            }
          });
        });
        self.showFinish(true);
        self.isFinishingLoading(false);
        self.updatePostProcessings();
      },
      self.onApiError
    );
  };
  // Download 3D files from line
  self.downloadFile = function() {
    Button3d.modals.show_download_upload_modal(
      '/' + lang + '/u/' + self.uuid(),
      self.scale()
    );
  };

  // Only for GKN, when attachment link click, select post-processing to machining
  self.selectMachiningWithUpload = function() {
    var finishingLength = self.finishingOffers().length;
    for (var i = 0; i < finishingLength; i++) {
      var slugStr = self.finishingOffers()[i].slug();
      if (slugStr.includes('machined') || slugStr.includes('machining')) {
        var finishingOfferVM = self.finishingOffers()[i];
        self.selectedFinishing(finishingOfferVM);
        $('#finishing option').trigger('change');
        return true;
      }
    }
  };
}
