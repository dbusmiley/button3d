function MaterialOfferVM(offer, projectLineVM) {
    var self = this;
    var product = projectLineVM.projectVM.materialsBySlug[offer.slug];

    self.title = ko.observable(product.title);
    self.slug = ko.observable(product.slug);

    self.status = ko.observable(offer.status);
    self.error = ko.observable(offer.error);
    self.printability = ko.observable(offer.printability);
    self.printability_problem = ko.observable(offer.printability_problem);

    for(var product_slug in material_products){
        if (product_slug === product.slug) {
            self.category = ko.observable(projectLineVM.categories_by_name[material_products[product_slug].category]);
            self.category().add_material_offer(self);
        }
    }
}
