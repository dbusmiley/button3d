function SupplierOfferVM(offer, projectLineVM) {
    var self = this;
    var cheapest_shipping_method = function () {
        if (offer.shipping_methods.length > 0) {
            return offer.shipping_methods.slice().sort(
                function (a, b) {
                    return parseFloat(a.price.incl_tax) - parseFloat(b.price.incl_tax);
                }
            )[0];
        } else {
            return {'price': {
                'incl_tax': 0,
                'excl_tax': 0
                }}
        }
    };

    self.partner = ko.observable(suppliers[offer.supplier].title);
    self.partner_id = suppliers[offer.supplier].id;
    self.slug = ko.observable(offer.supplier);
    self.printability = ko.observable(offer.printability)
    self.combinable_post_processings = ko.observable(offer.combinable_post_processings);
    self.manual_pricing_required = offer.manual_pricing_required;
    // TODO do not take just the first shipping method but find cheapest
    // self.delivery_days_range = ko.observable(offer.shipping_methods[0].delivery_days_range);
    self.shipping_price_incl_tax = ko.observable(cheapest_shipping_method().price.incl_tax);
    self.shipping_price_excl_tax = ko.observable(cheapest_shipping_method().price.excl_tax);
    self.delivery_max_days = offer.delivery_max_days;
    self.delivery_min_days = offer.delivery_min_days;
    self.price = offer.price;
    self.vat_rate = ko.pureComputed(function() {
        return Number((suppliers[offer.supplier].vat_rate).toString()) || 0;
    });
    // self.tax_price = ko.pureComputed(function(){
    //     if (offer.price)
    //         return offer.price.tax;
    //     return 0;
    // });
    self.logo_tag = ko.observable(suppliers[offer.supplier].logo_tag);
    self.logo_url = ko.observable(suppliers[offer.supplier].logo_url);

    // Operations
    // self.total_price = ko.pureComputed(function() {
    //     var total_price = self.unit_price_incl_tax();
    //     return parseFloat(total_price);
    // }, self);

    self.dropdownDisplay = ko.pureComputed(function() {
        return self.partner();
    }, self);
}
