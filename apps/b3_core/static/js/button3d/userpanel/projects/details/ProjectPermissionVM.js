// Project Permission VM binding to b3_user_panel/projects/project_permission.html
function ProjectPermissionVM(projectId, projectVM) {
    var self = this;
    self.permissionArray = ko.observableArray();
    self.sharingPermissionArrary = ko.observableArray();
    self.listNumber = ko.observable(3);
    self.showFullList = function() {
        return self.listNumber(self.sharingPermissionArrary().length);
    };
    self.ownPermission = ko.observable();
    project_api.getPermissionInfo(projectId, function (data) {
        var permissionVMArray = [];
        for (var i = 0; i < data.length; i++) {
            var permissionVM = new PermissionVM(data[i]);
            if (permissionVM.role === 'owner') {
                self.ownPermission(permissionVM);
                continue;
            }
            if (permissionVM.role === 'supplier') {
                projectVM.supplierHasAccess(true);
                continue;
            }
            permissionVMArray.push(permissionVM);
        }
        self.sharingPermissionArrary(permissionVMArray);
    });
}

function PermissionVM(data){
    var self = this;
    self.user = data.name;
    self.role = data.role;
    self.roleDict = {
        'owner': gettext('Owner'),
        'share': gettext('Share')
    };
    self.displayRole = ko.computed(function (){
        return self.roleDict[self.role]
    })
    self.email = data.email;
    self.contact = data.contact;
    var contactInfoArray = [];
    for (var key in self.contact) {
        var contactInfo =
            '<div class="project-permission contact"><label>'+ key +
            '</label><span>' + data.contact[key] + '</span></div>';
        contactInfoArray.push(contactInfo);
    }
    self.content = contactInfoArray.join('');
}
