function ProjectsOverviewVM() {
    var self = this;
    self.projects = ko.observableArray([]);

    self.results = ko.observableArray([]);
    self.num_results = ko.observable(null);
    self.status_projects = ko.observable('all');

    self.search_project_name = ko.observable('');
    self.project_name_delayed = ko.computed(self.search_project_name).extend({ throttle: 400 });
    self.search_date_0 = ko.observable(null);
    self.search_date_1 = ko.observable(null).extend({ throttle: 600 });

    self.search_items_per_page = ko.observable(20);
    self.search_page = ko.observable(1);

    self.no_results =  ko.observable(false);
    self.search_done = ko.observable(false);
    self.is_scroll_top = ko.observable(null);

    // Reset search page on ScrollTop
    self.search_project_name.subscribe(function() {
        if(self.is_scroll_top())
            self.search_page(1);
    });
    self.search_date_1.subscribe(function() {
        if(self.is_scroll_top())
            self.search_page(1);
    });
    self.status_projects.subscribe(function() {
        if(self.is_scroll_top())
            self.search_page(1);
    });

    self.activatedTab = ko.observable('all');

    self.doSearch = ko.computed(function() {
        if ((self.search_date_0() && !self.search_date_1()) || (!self.search_date_0() && self.search_date_1())){ return; }

        self.no_results(false);
        self.search_done(false);
        var ajax_data = {
            "items_per_page": self.search_items_per_page(),
            "date_0" : self.search_date_0(),
            "date_1" : self.search_date_1(),
            "name": self.project_name_delayed,
            "page": self.search_page(),
            "status" : self.status_projects(),
            "currency": getCurrencyIdentifier()
        };
        project_api.projects_search(ajax_data, function(response) {
            var list = response.list;
            if (self.search_page() === 1) {
                self.projects.removeAll();
            }

            self.num_results(response.num_results);

            for (var i = 0; i < list.length; i++) {
               self.projects.push(new ProjectVM(list[i]));
            }
            self.results(self.projects().filter(function (project) {
            return !project.is_ordered();
            }));
            self.no_results(list.length === 0);
            self.search_done(true);
        });
    });

    self.allOrders = function () {
        self.activatedTab('all');
        self.results(self.projects());
    };

    self.orderedOrders = function () {
        self.activatedTab('ordered');
        var filteredOrders = self.projects().filter(function (order) {
            return order.is_ordered();
        });
        self.results(filteredOrders);
    };

    self.ordersSharedWithUser = function () {
        self.activatedTab('shared');
        var filteredOrders = self.projects().filter(function (order) {
            return order.isShared();
        });
        self.results(filteredOrders);
    };

    self.cardViewEnabled = ko.observable(false)
    self.changeView = function() {
        if (self.cardViewEnabled()) {
            self.cardViewEnabled(false);
        } else {
            self.cardViewEnabled(true);
        }
    }


    self.sortDateAscending = ko.observable(false);
    self.sortByDate = function () {
        self.results.sort(function (left, right) {
            return self.sortDateAscending() ? new Date(right.creation_date()) - new Date(left.creation_date()) :
                new Date(left.creation_date()) - new Date(right.creation_date());
        });
        if (self.sortDateAscending()) {
            self.sortDateAscending(false);
        } else {
            self.sortDateAscending(true);
        }
    }
    self.sortTitleAscending = ko.observable(false);
    self.sortByTitle = function () {
        self.results.sort(function (left, right) {
            if (self.sortTitleAscending()){
                return (left.title() === right.title()) ? 0 : (left.title() < right.title() ? 1 : -1);
            } else {
                return (left.title() === right.title()) ? 0 : (left.title() > right.title() ? 1 : -1);
            }
        });
        if (self.sortTitleAscending()) {
            self.sortTitleAscending(false);
        } else {
            self.sortTitleAscending(true);
        }
    }
}


function ProjectVM(data) {
    var self = this;
    self.id = ko.observable(data.project_id);
    self.title = ko.observable(data.title);
    self.url = ko.observable(data.url);

    self.creation_date = ko.observable(date_to_nicestr(new Date(data.date_created)));
    self.nb_lines = ko.observable(data.nb_lines);
    self.project_lines = ko.observableArray(data.lines);
    self.isLoaded = ko.observable(true);
    self.isShared = ko.observable(data.is_shared);

    self.nb_lines_text = ko.pureComputed(function() {
        if (self.nb_lines() === 0) {
            return gettext('No files');
        }
        return (self.nb_lines() === 1) ? 1 + ' ' + gettext('file') :  self.nb_lines() + ' ' + gettext('files');
    });

    self.price = ko.observable(data.price);
    self.totalPrice = ko.pureComputed(function() {
        return (self.price()) ? setCurrency(self.price().incl_tax, self.price().currency) : gettext('Not available');
    });

    self.error = ko.observable(false);
    self.is_ordered = ko.observable(data.is_ordered);
    self.quotationNumber = ko.observable(data.quotation_number);

    self.show_delete_project_modal = function() {
        Button3d.modals.show_delete_project_modal(self.id());
    };


}
var projectsOverview  = new ProjectsOverviewVM();
ko.applyBindings(projectsOverview, $('#projects-overview')[0]);

function getCurrencyIdentifier() {
    return $('.currency-selected').first().text().split(' ')[1];
}

// Infinite scroll
$(window).scroll(function () {
    if (($(document).height() - $(window).height() - $(window).scrollTop()) < ($(window).height() / 7)) {
        if ((projectsOverview.search_items_per_page() * projectsOverview.search_page() < projectsOverview.num_results())) {
            projectsOverview.search_page(projectsOverview.search_page() + 1);
        }
    }
    if ($(window).scrollTop() === 0) {
        projectsOverview.is_scroll_top(true);
    }
});

$('.date-picker').on('apply.daterangepicker', function(ev, picker) {
  projectsOverview.search_date_0(picker.startDate.format('YYYY-MM-DD'));
  projectsOverview.search_date_1(picker.endDate.format('YYYY-MM-DD'));
});

$('.fa-calendar').click(function(){
    $( '.date-picker' ).focus();
});

$('.clear-date').click(function(){
    $('.date-picker').val('');
    projectsOverview.search_date_0(null);
    projectsOverview.search_date_1(null);
});


