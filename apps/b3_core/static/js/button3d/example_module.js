Button3d.uploads.materials = (function(){
    var publix = {};
    var private_member = "I'm private";

    var public_member = "I'm public";
    publix.public_member = public_member;

    function private_function() {

    }

    publix.public_function = public_function; // this is what makes it public.
    function public_function() {
        // Call a function from here
        private_function();

        // Call a function somewhere else (has to be public)
        Button3d.uploads.repair.getCurrentFiletype();
    }

    // Most modules have a public init() function that will be called manually on load.

    // Return public members, so they are visible from the outside.
    return publix;
}());