var Button3d = Button3d || {};

var compare = extend(Button3d, 'compare');

$.extend(compare, {
    init: function () {
        Button3d.compare.materials.init();
        Button3d.compare.three_d_tools.init();

        // When compare page is loaded, the upload already completed.
        // Trigger the event so that repair.js will initialize

        $('body').trigger("uploadFinished");
        $('#repair_wta_progress').addClass("progress-animation");

        // make material tooltips work
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });


        $('.b3-hidden-remove').removeClass("b3-hidden-remove");
    }
});
