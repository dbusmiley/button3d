/* global onlyUnique */

function StepMaterialsVM(parentVM){
    var self = this;
    self.technologiesFilters = ko.observableArray();
    self.FilterCategories = new FilterCategoriesVM();
    self.FilterCharacteristics = new FilterCharacteristicsVM();
    self.FilterTechnologies = new FilterTechnologiesVM();
    self.FilterProperties = new FilterPropertiesVM();

    self.request_arguments = ko.computed(function(){
        return {
            scale: parentVM.scale(),
            currency: parentVM.currency()
        };
    });

    self.has_maximum_view = ko.observable();
    self.sort_method = ko.observable();
    self.ajax_offers = ko.observableArray();

    /**
     * Sorting function for offer array by Printability.
     * @param {object} offerA One offer to compare.
     * @param {object} offerB Another offer to compare.
     * @returns {number} 0 if equal. -1 if offer a is more 'printable' than offer b. 1 if offer b is more 'printable'
     * than offer a.
     */
    function sortOffersByPrintability(offerA, offerB) {
        var printabilityWeights = {
            'unknown': 0,
            'not_printable': 1,
            'warning': 2,
            'printable': 3
        };
        var aOfferPrintabilityWeight = printabilityWeights[offerA.printability] || 0;
        var bOfferPrintabilityWeight = printabilityWeights[offerB.printability] || 0;
        if (aOfferPrintabilityWeight === bOfferPrintabilityWeight) {
            return 0;
        }
        if (aOfferPrintabilityWeight > bOfferPrintabilityWeight) {
            return -1;
        }
        return 1;
    }

    self.ajax_offers_sorted = ko.computed(function offersSorted() {
        var offers = self.ajax_offers();
        var sortMethod = self.sort_method();
        if (sortMethod === 'PRICE') {
            offers.sort(function sortOffersByPrice(offerA, offerB) {
                if (offerA.min_price.incl_tax === undefined && offerA.manualPricingRequired()) {
                    return 1;
                }
                if (offerB.min_price.incl_tax === undefined && offerB.manualPricingRequired()) {
                    return -1;
                }
                var aMaterialPrice = parseFloat(offerA.min_price.incl_tax);
                var bMaterialPrice = parseFloat(offerB.min_price.incl_tax);
                return (aMaterialPrice > bMaterialPrice)
                    ? 1
                    : -1;
            });
        } else if (sortMethod === 'DELIVERY') {
            offers.sort(function sortOffersByDeliveryTime(offerA, offerB) {
                if (offerA.delivery_min_days === offerB.delivery_min_days) {
                    return offerA.delivery_max_days - offerB.delivery_max_days;
                }
                return offerA.delivery_min_days - offerB.delivery_min_days;
            });
        } else if (sortMethod === 'POPULARITY') {
            offers.sort(function sortOffersByPopularity(offerA, offerB) {
                return offerB.popularity - offerA.popularity;
            });
        } else if (sortMethod === 'PRINTABILITY') {
            offers.sort(sortOffersByPrintability);
        }
        return offers;
    });

    self._auto_hide_unavailable_characteristics = ko.computed(function(){
        var offers = self.ajax_offers();
        var all_characteristics = self.FilterCharacteristics.options();
        for(var i=0;i<all_characteristics.length;i++){
            var one_has_characteristic = false;
            for(var j=0;j<offers.length;j++){
                if(!offers[j].is_checked())
                    continue;
                if(slug_to_product[offers[j].material.slug].get_attr(all_characteristics[i].key)==true) {
                    one_has_characteristic = true;
                    break;
                }
            }
            all_characteristics[i].visible(one_has_characteristic);
        }
    })

    self.offers = self.ajax_offers_sorted;
    self.any_offer_visible = ko.computed(function(){
        var offers = self.offers();
        var any = false;
        for(var i=0;i<offers.length;i++)
            if(offers[i].is_visible())
                any=true;
        return any;
    })
    self.is_loading = ko.observable(true);
    self.error_message = ko.observable();

    self._update_offers_timeout = 0;

    self._update_offers = ko.computed(function(){
        self.request_arguments();
        parentVM.step();
        Button3d.common.repair.is_finished();
        Button3d.common.repair.is_parameter_available();

        self._update_offers_timeout && clearTimeout(self._update_offers_timeout);
        self._update_offers_timeout = setTimeout(function(){
            var request_arguments = self.request_arguments();
            if(parentVM.step() != "materials") return;

            var offers = self.offers();
            for(var i=0;i<offers.length;i++)
                offers[i].is_loading(true);

            self.is_loading(true);

            if(!Button3d.common.repair.is_parameter_available())
                return;

            if(self._prev_xhr){
                self._prev_xhr.abort();
                self._prev_xhr = undefined;
            }

            self._prev_xhr = compare_api.get_material_offers(
                parentVM.uuid,
                request_arguments.scale,
                request_arguments.currency,
                function (data) {
                    self.error_message("");
                    var technologies = [];
                    if (data.results){
                        var result = [];
                        for (var i = 0; i < data.results.length; i++) {
                            var cur = data.results[i];
                            if(cur.amount_stockrecords>0)
                                result.push(new MaterialsOfferVM(self, cur));
                        }
                        self.ajax_offers(result);
                        self.FilterTechnologies.init(getAllDistinctTechnologies(result));
                    }
                    self.has_maximum_view(null);

                    self.is_loading(false);
                },
                function (e) {
                    console.log(e);
                    showError(e);
                }
            );
        }, 100);
    });

    self.sort_method.subscribe(function() {
        Button3d.common.send_event_to_ga("sort_by_" + self.sort_method(), "sort");
    });

    self.height_panel = ko.observable('auto');
    self.SetMaximumView = function(currentOffer,element){
        self.has_maximum_view(currentOffer);
        currentOffer.material.show_printability(true);
        var element = element.currentTarget;
        if ($( window ).width()>639){
            self.height_panel($(element).find('.b3-snippet-material-col-left').height()+'px');
        }

    }

    /**
     * Function to get all technologies by the materials response. We will get them out of the data here to have a more
     * dynamic approach other than creating a static array of technologies.
     * @param {Array.<Object>} materials An array of materials which is iterated over to get all technologies.
     * @returns {Array.<String>} An array of strings which contains all technologies used by the materials returned by
     * the API request.
     */
    function getAllDistinctTechnologies(materials) {
        var technologies = [];
        // We need to add the standard case 'All' to have a default case.
        technologies.push(gettext('All'));
        for (var i = 0; i < materials.length; i++) {
            technologies.push(materials[i].material.technology());
        }
        return technologies.filter(onlyUnique);
    }
}
