function MaterialsFilterHistogramVM(parentVM){
    var self = this;

    self.selected_property = ko.observable(parentVM.free_filter_properties()[0]);

    self.filter_properties = ko.computed(function(){
        var properties = parentVM.free_filter_properties().slice();
        properties.unshift(self.selected_property());
        return properties;
    });

    self.min = ko.observable();
    self.max = ko.observable();
    self.histogram = ko.observableArray();
    self.histogram_bar_width = ko.computed(function(){
        if(self.histogram())
            return 99/self.histogram().length+"%";
        return "99%";
    });
    self.from = ko.observable();
    self.till = ko.observable();
    self.from_percentage = ko.observable();
    self.till_percentage = ko.observable();

    self.min_text = ko.computed(function(){
        if(self.min()!=undefined) return self.min().toFixed(self.selected_property().precision);
    });
    self.max_text = ko.computed(function(){
        if(self.max()!=undefined) return self.max().toFixed(self.selected_property().precision);
    });
    self.step_size = ko.computed(function(){
        return Math.pow(10, -self.selected_property().precision);
    });

    self.from.subscribe(function(){
        if(self.selected_property().modifier=="log")
            self.from_percentage((Math.log(self.from())-Math.log(self.min()))/(Math.log(self.max())-Math.log(self.min()))*100);
        if(self.selected_property().modifier=="none")
            self.from_percentage((self.from()-self.min())/(self.max()-self.min())*100);
    });
    self.from_percentage.subscribe(function(){
        var updated;
        if(self.selected_property().modifier=="log")
            updated = (Math.exp((self.from_percentage()/100)*(Math.log(self.max())-Math.log(self.min()))+Math.log(self.min()))).toFixed(self.selected_property().precision);
        if(self.selected_property().modifier=="none")
            updated = ((self.from_percentage()/100)*(self.max()-self.min())+self.min()).toFixed(self.selected_property().precision);
        if(updated!=self.from())
            self.from(updated);
    });

    self.till.subscribe(function(){
        if(self.selected_property().modifier=="log")
            self.till_percentage((Math.log(self.till())-Math.log(self.min()))/(Math.log(self.max())-Math.log(self.min()))*100);
        if(self.selected_property().modifier=="none")
            self.till_percentage((self.till()-self.min())/(self.max()-self.min())*100);
    });
    self.till_percentage.subscribe(function(){
        var updated;
        if(self.selected_property().modifier=="log")
            updated = (Math.exp((self.till_percentage()/100)*(Math.log(self.max())-Math.log(self.min()))+Math.log(self.min()))).toFixed(self.selected_property().precision);
        if(self.selected_property().modifier=="none")
            updated = ((self.till_percentage()/100)*(self.max()-self.min())+self.min()).toFixed(self.selected_property().precision);
        if(updated!=self.till())
            self.till(updated);
    });

    self.from_percentage.subscribe(function(){
        if(self.from_percentage()<0)
            self.from_percentage(0);
        else if(self.from_percentage()>self.till_percentage()-0.01)
            self.from_percentage(self.till_percentage()-0.01);
    });
    self.till_percentage.subscribe(function(){
        if(self.till_percentage()>100)
            self.till_percentage(100);
        else if(self.till_percentage()<self.from_percentage()+0.01)
            self.till_percentage(self.from_percentage()+0.01);
    });

    self._update_histogram = ko.computed(function(){
        Button3d.common.send_event_to_ga("filter " + self.selected_property().title, "use");
        var property = self.selected_property();
        var min_key = property.key || property.keys[0];
        var max_key = property.key || property.keys[1];
        
        var min,max;
        for(var i=0;i<list_products.length;i++){
            if(list_products[i].material)
                continue;
            if(min==undefined || list_products[i][min_key]<min)
                min = list_products[i][min_key];
            if(max==undefined || list_products[i][max_key]>max)
                max = list_products[i][max_key];
        }

        if(min<0) min = min*1.1;
        else      min = min*0.9;
        if(max>0) max = max*1.1;
        else      max = max*0.9;

        self.min(min);
        self.max(max);
        self.from(parseFloat(min.toFixed(property.precision)));
        self.till(parseFloat(max.toFixed(property.precision)));
        self.from_percentage(0);
        self.till_percentage(100);

        var step_size;
        if(property.modifier=="log")
            step_size = (Math.log(max)-Math.log(min))/property.steps_amount;
        if(property.modifier=="none")
            step_size = (max-min)/property.steps_amount;
        var histogram = [];
        for(var i=0;i<property.steps_amount;i++){
            histogram.push(0);
        }
        for(var i=0;i<list_products.length;i++) {
            if(list_products[i].material)
                continue;

            var average = (list_products[i][min_key] + list_products[i][max_key])/2;
            if(property.modifier=="log")
                histogram[Math.floor((Math.log(average) - Math.log(min)) / step_size)] += 1;
            if(property.modifier=="none")
                histogram[Math.floor((average-min)/step_size)]+=2;
        }
        for(var i=0;i<histogram.length;i++) {
            if(histogram[i]>0)
                histogram[i]+=2;
        }
        self.histogram(histogram);


        self.text_all = ko.computed(function(){
            self.from();
            self.till();
            var all=0, available=0;
            if(self.from() && self.till()){
                for(var i=0;i<list_products.length;i++) {
                    if(list_products[i].material)
                        continue;
                    all++;
                    var prop_min = list_products[i][min_key];
                    var prop_max = list_products[i][max_key];
                    if(!(prop_min>self.till() || prop_max<self.from()))
                        available++;
                }
            }
            return available+"/"+all;
        })
    });

    var which_slider_moved;
    var rheostat_div;

    var bind_mousemove = function(event){
        var click_percent = (event.clientX-$(rheostat_div).offset().left)*100/$(rheostat_div).width();
        updateSlider(click_percent);
    }
    var unbind_mousemove = function(event){
        $("body").unbind("mousemove", bind_mousemove);
        which_slider_moved=undefined;
        rheostat_div=undefined;
    }
    var updateSlider = function(click_percent){
        var min = which_slider_moved=="max"?(self.from_percentage()+1):0;
        var max = which_slider_moved=="min"?(self.till_percentage()-1):100;

        click_percent = Math.max(min,Math.min(max,click_percent));

        if(click_percent<=self.from_percentage() || which_slider_moved=="min"){
            self.from_percentage(click_percent);
        } else if(click_percent>=self.till_percentage() || which_slider_moved=="max"){
            self.till_percentage(click_percent);
        } else {
            if(click_percent-self.from_percentage() < self.till_percentage()-click_percent){
                self.from_percentage(click_percent);
            } else {
                self.till_percentage(click_percent);
            }
        }
    }
    self.FindClosestClick = function(data, event){
        var click_percent = (event.clientX-$(event.currentTarget).offset().left)*100/$(event.currentTarget).width();
        updateSlider(click_percent);
    }
    self.MouseDown = function(data, event){
        if($(event.target).hasClass("min"))
            which_slider_moved="min";
        else
            which_slider_moved="max";
        rheostat_div = $(event.currentTarget).parent();
        $("body").bind("mousemove", bind_mousemove);
        $("body").bind("mouseup mouseleave", unbind_mousemove);
        var click_percent = (event.clientX-$(rheostat_div).offset().left)*100/$(rheostat_div).width();
        updateSlider(click_percent);
    }
}
