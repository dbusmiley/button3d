function ProductVM(data, parentVM){
    var self = this;
    slug_to_product[data.slug] = self;

    if(data.material)
        self.material = slug_to_product[data.material];
    self.slug = data.slug;
    self.parentvm = parentVM;
    self.title = data.title;
    self.attr = data;
    self.description = data.description;
    self.description_short = truncate(data.description, 50);
    self.cached_error = ko.observable();
    self.cached_printability = ko.observable();
    self.cached_printability_at_scale = ko.observable();
    self.cached_printability_problem = ko.observable();
    if (self.attr.tradename) {
        self.tradename = self.attr.tradename;
    } else {
        self.tradename = '';
    }

    self.get_attr = function(name){
        if(self.attr[name])
            return self.attr[name];
        if(parentVM.get_attr)
            return parentVM.get_attr(name);
        return undefined;
    };



    self.attr_strength = ko.computed(function(){
        return self.get_attr("attr_strength") || 0;
    });
    self.attr_strength_stars = ko.computed(function(){
        return get_stars_list(self.attr_strength());
    });
    self.attr_detail = ko.computed(function(){
        return self.get_attr("attr_detail") || 0;
    });
    self.attr_detail_stars = ko.computed(function(){
        return get_stars_list(self.attr_detail());
    });
    self.technology = ko.computed(function(){
        return self.get_attr("technology") || "";
    });
    self.attr_wall_min = ko.computed(function(){
        return self.get_attr("attr_wall_min") || 0;
    });
    self.attr_wall_opt = ko.computed(function(){
        return self.get_attr("attr_wall_opt") || 0;
    });

    self.scoreDetail = ko.computed(function(){
        return self.get_attr('score_detail') || 0;
    });
    self.scoreDensity = ko.computed(function(){
        return self.get_attr('score_density') || 0;
    })
    self.scoreStrength = ko.computed(function(){
        return self.get_attr('score_strength') || 0;
    })


    self._has_stockrecords = ko.observable(false);
    self.has_stockrecords = ko.computed(function(){
        if(self.material)
            self.material.has_stockrecords();
        return self._has_stockrecords();
    });

    self.set_has_stockrecords = function(value){
        if(self.material)
            self.material.set_has_stockrecords(value);
        self._has_stockrecords(value);
    }

    self.error = ko.computed(function(){
        if(Button3d.common.repair.is_finished()){
            if(self.material)
                self.material.error();
            return self.cached_error();
        }
    });

    self.printability = ko.computed(function(){
        if(Button3d.common.repair.is_finished()){
            if(self.material)
                self.material.printability();
            var scale = Button3d.compare.materials.Steps.scale();
            if(self.cached_printability() && self.cached_printability_at_scale() == scale)
                return self.cached_printability();
        }
    });

    self.printability_problem = ko.computed(function(){
        if(Button3d.common.repair.is_finished()){
            if(self.material)
                self.material.printability_problem();
            var scale = Button3d.compare.materials.Steps.scale();
            if(self.cached_printability_problem() && self.cached_printability_at_scale() == scale) {
                return self.cached_printability_problem();
            }
        }
    });

    self.is_printability_printable = ko.computed(function(){
        return self.printability() == "printable";
    });
    self.is_printability_thin_walls = ko.computed(function(){
        return self.printability() == "not_printable";
    });
    self.is_printability_suspicious = ko.computed(function(){
        return self.printability() == "warning";
    });
    self.is_printability_unknown = ko.computed(function(){
        return self.printability() == "unknown";
    });

    self.show_printability = function(dont_animate) {
        Button3d.compare.materials.Steps.printable_selected(self);

        Button3d.common.repair.show_printability(self.printability());
        if(dont_animate!==true)
            $('html,body').animate({ scrollTop: 0 }, { duration: 600 });
    }


    self.subcategories = ko.computed(function(){
        var result = [];
        for(var key in material_products){
            if(material_products[key].material == self.slug){
                result.push(new ProductVM(material_products[key], self));
            }
        }
        return result;
    });
}

function get_stars_list(stars_num){
    var result = [];
    for(var i=0; i<5; i++)
        result.push(stars_num>i?1:0);
    return result;
}
