function SupplierVM(data){
    var self = this;
    self.id = data.id;
    self.website = data.website;
    self.email = data.email;
    self.phone = data.phone;
    self.address = data.address;
    self.city = data.city;
    self.country = data.country;
    self.postcode = data.postcode;
    self.logo_tag = data.logo_tag;
    self.title = data.title;
    self.payment_methods = data.payment_methods;
    self.slug = data.slug;
    self.certifications = data.certifications;
    // We need to add those values here because total value is just rounding the total value.
    self.partnerTotalRating = data.rating_quality + data.rating_reliability + data.rating_service;
    self.rating_quality_stars = get_stars_list(data.rating_quality);
    self.rating_reliability_stars = get_stars_list(data.rating_reliability);
    self.rating_service_stars = get_stars_list(data.rating_service);
    self.rating_total_stars = get_stars_list(data.rating_total);

    self.rating_detail = ko.computed(function(){
        var ratings = {};
        ratings['quality'] = self.rating_quality_stars;
        ratings['reliability'] = self.rating_reliability_stars;
        ratings['service'] = self.rating_service_stars;
        return ratings;
    });

    self.is_invoice_enabled = data.payment_methods.indexOf("invoice")>=0;
    self.is_stripe_enabled = data.payment_methods.indexOf("stripe")>=0;
}
