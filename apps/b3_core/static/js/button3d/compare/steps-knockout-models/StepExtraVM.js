function StepExtraVM(parentVM){
    var self = this;
    self.request_arguments = ko.computed(function(){
        return {
            scale: parentVM.scale(),
            currency: parentVM.currency(),
            material: parentVM.selected_material_slug(),
            partner: parentVM.selected_partner_slug()
        };
    });
    self.offers = ko.observableArray([]);
    self.selected_offers = ko.observableArray([]);
    self.step3_manual_request_url = ko.pureComputed(function() {
        var url = manual_request_url_base + '?stl_file=' + $('#uuid')[0].value
            + '&product=' + parentVM.selected_material_slug()
            + '&supplier=' + parentVM.selected_partner_slug()
            + self.post_processings_string()
            + self.post_processing_colors_string()
            + '&unit=' + $('.b3-btn-unit-active').text()
            + '&scale=' + parentVM.scale();
        var colorId = null;
        return url;
    }, self);
    self.post_processings_string = ko.pureComputed(function() {
        var result_string = '';
        self.selected_offers().forEach(function(offer) {
            result_string += (
                '&post_processing=' + offer.slug().replace(
                    parentVM.selected_material_slug() + '-',
                    ''
                )
            );
        });
        return result_string;
    });
    self.post_processing_colors_string = ko.pureComputed(function() {
        var result_string = '';
        self.selected_offers().forEach(function(offer) {
            result_string += '&post_processing_color=' + (offer.selected_color_id() || []);
        });
        return result_string;
    });

    self.is_loading = ko.observable(true);

    self.quantity = ko.observable(1);
    self.combinable_post_processings = ko.observable(null);
    self.base_delivery_min_days = ko.observable(null);
    self.base_delivery_max_days = ko.observable(null);
    self.base_material_price = ko.observable(null);
    self.base_material_datasheet = ko.observable(null);
    self.base_material_manual_pricing_required = ko.observable(null);
    self.supplier_cheapest_shipping_method = ko.observable(null);
    self.base_material_price_representation = ko.pureComputed(function() {
        return self.base_material_price ?
            setCurrency(self.base_material_price(), parentVM.currency()) :
            ''
    });
    self.show_net_prices = Button3d.compare.materials.Steps.show_net_prices;

    self.total_price = ko.pureComputed(function () {
        var post_processings_total_price = 0;
        for (var i = 0; i < self.selected_offers().length; i++) {
            var selectedPrice = self.show_net_prices
            ? self.selected_offers()[i].price.excl_tax
            : self.selected_offers()[i].price.incl_tax;

            post_processings_total_price += Number(
                selectedPrice -
                self.base_material_price()
            );
        }
        return self.quantity() * (Number(self.base_material_price()) + post_processings_total_price)
    });

    self.total_price_representation = ko.pureComputed(function () {
        return setCurrency(self.total_price(), parentVM.currency());
    });

    self.post_processing_price_representation = function(offer) {
        var newPrice = self.show_net_prices ? offer.price.excl_tax : offer.price.incl_tax;
        return setCurrency(
            newPrice - self.base_material_price(),
            parentVM.currency());
    };

    self.delivery_days_range = ko.pureComputed(function () {
        var min_days = self.base_delivery_min_days();
        var max_days = self.base_delivery_max_days();
        for (var i = 0; i < self.selected_offers().length; i++) {
            min_days += self.selected_offers()[i].delivery_min_days;
            max_days += self.selected_offers()[i].delivery_max_days;
        }
        return min_days + ' - ' + max_days;
    });

    self.set_base_material_info = function (offer) {
        self.base_material_price(self.show_net_prices ? offer.price.excl_tax : offer.price.incl_tax);
        self.base_material_manual_pricing_required(offer.manualPricingRequired);
        self.base_material_datasheet(offer.datasheet);
        self.combinable_post_processings(offer.combinable_post_processings);
        self.base_delivery_min_days(offer.delivery_min_days);
        self.base_delivery_max_days(offer.delivery_max_days);
        self.supplier_cheapest_shipping_method(offer.supplier_cheapest_shipping_method);
    };

    // get necessary product info if the user refreshes the page
    parentVM.selected_material_slug.subscribe(function() {
        if (
            !self.combinable_post_processings() ||
            !self.base_material_price() ||
            !self.base_delivery_min_days() ||
            !self.delivery_max_days() ||
            !self.supplier_cheapest_shipping_method()
        ) {
            compare_api.get_supplier_offers(
                parentVM.uuid,
                parentVM.scale(),
                parentVM.currency(),
                parentVM.selected_material_slug(),
                function(data) {
                    for (var i = 0; i < data.results.length; i++) {
                        if (parentVM.selected_partner_slug() === data.results[i].supplier) {
                            var supplierOffer = new SupplierOfferVM(data.results[i]);
                            self.set_base_material_info(supplierOffer);
                        }
                    }
                }
            )
        }
    })

    self._update_offers_timeout = 0;
    self._update_offers = ko.computed(function(){
        self.request_arguments();
        parentVM.step();
        Button3d.common.repair.is_finished();
        Button3d.common.repair.is_parameter_available();

        self._update_offers_timeout && clearTimeout(self._update_offers_timeout);
        self._update_offers_timeout = setTimeout(function(){
            var request_arguments = self.request_arguments();
            if(parentVM.step() != "extra") return;

            self.is_loading(true);

            if(!Button3d.common.repair.is_parameter_available())
                return;

            if(self._prev_xhr){
                self._prev_xhr.abort();
                self._prev_xhr = undefined;
            }

            self._prev_xhr = compare_api.get_finishing_offers(
                parentVM.uuid,
                request_arguments.scale,
                request_arguments.currency,
                request_arguments.material,
                request_arguments.partner,
                function (data) {
                    var result = [];
                    if (data.results.length && data.results[0].slug.indexOf('-raw') < 0) {
                        throw "First offer should be the raw 'post processing'.";
                    }
                    // start at one because of the raw 'finishing'
                    for (var i = 1; i < data.results.length; i++) {
                        result.push(new FinishingOfferVM(data.results[i], self));
                    }
                    self.offers(result);
                    self.is_loading(false);
                },
                function (e) {
                    console.log(e);
                    showError(e);
                }
            );
        }, 100);
    });

    self.manual_pricing_required = ko.computed(function () {
        selected_offers = self.selected_offers();
        if (self.base_material_manual_pricing_required()) {
            return true;
        }
        return selected_offers.some(function (offer) {
            return offer.manualPricingRequired;
        })
    });

    self.clear_selected_offers = function (event) {
        for (var i = 0; i < self.selected_offers().length; i++) {
            self.selected_offers()[i].is_selected(false);
        }
        self.selected_offers([]);
    };

    self.post_processings_reducer = function (postProcessingsArray) {
        var reducedArray = [];
        for (var i = 0; i < postProcessingsArray.length; i++) {
            reducedArray.push({
                slug: postProcessingsArray[i].slug(),
                color_id: postProcessingsArray[i].selected_color_id() || null,
            });
        }
        return reducedArray;
    };

    self.post_processings_datasheets = ko.pureComputed(function() {
        var result = [];
        self.selected_offers().forEach(function(offer) {
            if (!offer.datasheet) {
                return;
            }
            result.push(offer.datasheet);
        })
        return result;
    })

    self.SaveConfiguration = function(basket_url) {
        $('#btn_add_basket').text(gettext('Loading...'));
        if (headerVM && !headerVM.isProjectOrdered()) {
            var request_arguments = self.request_arguments();
            project_api.set_line_fields_with_qty(
                projectId,
                lineId,
                request_arguments.material,
                request_arguments.partner,
                self.post_processings_reducer(self.selected_offers()),
                self.quantity(),
                request_arguments.currency,
                function(data) {
                    if (data.success) {
                        window.location.href = basket_url;
                    } else {
                        $('#btn_add_basket').text(gettext('Continue'));
                        console.log(data.message);
                    }
                }
            );
        } else {
            window.location.href = basket_url;
        }
    };
    self.AddToProject = function() {
        $('#btn_add_basket').text(gettext('Loading...'));

         if (parentVM.printable_selected().is_printability_thin_walls()) {
            Button3d.modals.show_material_not_printable_modal(self.step3_manual_request_url() + '&not-printable=True');
        } else {
            var payload = {
                'partner_slug': parentVM.selected_partner_slug(),
                'material_slug': parentVM.selected_material_slug(),
                'scale': parentVM.scale(),
                'unit': parentVM.unit(),
                'amount': self.quantity(),
                'post_processings': self.post_processings_reducer(self.selected_offers()),
                'to_project': true
            };
            $.ajax({
                url: '/' + lang + '/u/' + $('#uuid').val() + '/generic_upload/',
                type: 'POST',
                dataType: 'json',
                data: JSON.stringify(payload),
                success: function (data) {
                    if (data.success === false) {
                        //TODO
                        alert(data.message);
                    } else {
                        window.location.href = data.redirect_url;
                    }
                },
                error: function (e) {
                    // TODO error handling
                    $('#btn_add_basket').text(gettext('Continue'));
                }
            });
        }
    };
}
