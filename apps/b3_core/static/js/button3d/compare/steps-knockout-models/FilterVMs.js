function FilterCategoriesVM(){
    var self = this;
    self.root_category = new CategoryVM({path:"", depth:0}, self);
}

// eslint-disable-next-line
function FilterTechnologiesVM() {
    // Local Fields.
    var selectedTechnologies = [];

    // Knockout Fields.
    this.technologies = ko.observableArray([]);
    this.selectedTechnologies = ko.observableArray();
    this.showResetFilter = ko.observable(false);
    this.listExpanded = ko.observable(false);

    // Knockout Functions.
    this.init = initialize;
    this.initializeAllElement = initializeAllElement;
    this.onClick = onClick;
    this.reset = reset;
    this.showMore = showMore;
    /**
     * Needed that we do not initialize this filter multiple times.
     * @type {boolean} False is the current state which will get set to true if the filter has been initialized.
     */
    this.initialized = false;

    // Public functions Functions.
    /**
     * Function to initialize the View Model. It will need an array of strings as parameter to create the technologies
     * locally.
     * @param {Array.<String>} technologies Array of strings of the technologies used by the offered materials.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function initialize(technologies) {
        if (this.initialized) {
            return null;
        }
        for (var i = 0; i < technologies.length; i++) {
            this.technologies.push(new TechnologyViewModel(technologies[i], this));
        }
        this.initializeAllElement();
        this.selectedTechnologies([]);
        this.initialized = true;
        adjustHeight();
        return null;
    }

    /**
     * Function to initialize the 'All Element'. This is the default element of the technologies.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function initializeAllElement() {
        this.technologies()[0].selected(true);
        selectedTechnologies.push(this.technologies()[0]);
    }

    /**
     * Click handler which is used when an element is clicked.
     * @param {Object} technologyViewModel View model of the clicked elements. It is the source of the click.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function onClick(technologyViewModel) {
        if (technologyViewModel.title() === gettext('All')) {
            disableOtherThanDefault(this.technologies());
        } else {
            this.technologies()[0].selected(false);
            technologyViewModel.selected()
                ? technologyViewModel.selected(true)
                : technologyViewModel.selected(false);
            noSelectionFallback(this.technologies());
        }
        this.selectedTechnologies(getRawSelectedTechnologies(this.technologies()));
        if ((this.selectedTechnologies().length === 0) ||
            (this.selectedTechnologies().length === 1 && this.selectedTechnologies()[0] === gettext('All'))) {
            this.showResetFilter(false);
        } else {
            this.showResetFilter(true);
        }
    }

    /**
     * Function to reset the view model to an initial state.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function reset() {
        for (var i = 1; i < this.technologies().length; i++) {
            this.technologies()[i].selected(false);
        }
        var defaultTechnology = this.technologies()[0];
        defaultTechnology.selected(true);
        this.showResetFilter(false);
        this.selectedTechnologies(['All']);
    }

    /**
     * Simple method to change the state of the show more anchor tag.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function showMore() {
        this.listExpanded(true);
    }

    // Local functions.

    /**
     * Function to disable all inputs other than the default input.
     * @param {Array.<Object>} technologies Array of technologies which should get disabled. The first element needs to
     * be the default element.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function disableOtherThanDefault(technologies) {
        for (var j = 1; j < technologies.length; j++) {
            technologies[j].selected(false);
        }
        technologies[0].selected(true);
    }

    /**
     * Function to produce an array of selected technologies.
     * @param {Array} technologies Array of all technologies of the view model.
     * @returns {Array} An array of strings containing all selected titles of the technologies.
     */
    function getRawSelectedTechnologies(technologies) {
        var temporaryArray = [];
        technologies.forEach(function (technology) {
            if (technology.selected()) {
                temporaryArray.push(technology.title());
            }
        });
        return temporaryArray;
    }

    /**
     * Function to have fallback mechanism if necessary if there was no technology selected.
     * @param {Array} technologies An array of all technologies of the view model.
     * @returns {Null} Nothing because it is changing the state of the View Model.
     */
    function noSelectionFallback(technologies) {
        var noneSelected = true;
        technologies.forEach(function (technology) {
            if (technology.selected()) {
                noneSelected = false;
            }
        });
        if (noneSelected) {
            technologies[0].selected(true);
        }
    }
}

function TechnologyViewModel(title, parentViewModel) {
    this.title = ko.observable(title);
    this.selected = ko.observable(false);

    this.onClick = function (technologyViewModel) {
        parentViewModel.onClick(technologyViewModel);
        return true;
    };
}

function CharacteristicVM(data,parentVM){
    var self = this;
    self.title = data.title;
    self.key = data.key;
    self.checked = ko.observable(false);
    self.visible = ko.observable(true);

    self.checkCharacteristic = function(){
        if(self.checked()){
            Button3d.common.send_event_to_ga("characteristics_" + self.key, "unchecked");
            self.checked(false)
            parentVM.selected.remove(self);
        }else{
            Button3d.common.send_event_to_ga("characteristics_" + self.key, "checked");
            self.checked(true)
            parentVM.selected.push(self);
        }
        return true;
    };
}

function FilterCharacteristicsVM(){
    var self = this;

    self.show_all = ko.observable(false);
    self.options = ko.observableArray();
    self.selected = ko.observableArray();

    for(var i = 0; i < filter_characteristics.length;i++){
        self.options.push(new CharacteristicVM(filter_characteristics[i],self));
    }

    self.visible_options = ko.computed(function(){
        var options = self.options();
        var result = [];

        for(var i=0;i<options.length;i++){
            if(options[i].visible()){
                result.push(options[i]);
            }
        }
        return result;
    })

    self.resetCharacteristics = function(){
        self.show_all(false);
        self.selected.removeAll();
        for(var i=0;i<self.options().length;i++){
            self.options()[i].checked(false);
        }
        return true;
    }

    self.showMore = function(){
        self.show_all(true);
        return true;
    }

}

function FilterPropertiesVM(){
    var self = this;

    self.histograms = ko.observableArray([]);

    self.free_filter_properties = ko.computed(function(){
        var free_filter_properties = filter_properties.slice();
        for(var i=0;i<self.histograms().length;i++){
            var selected = self.histograms()[i].selected_property();
            for(var j=0;j<free_filter_properties.length;j++){
                if(free_filter_properties[j] == selected){
                    free_filter_properties.splice(j, 1);
                    break;
                }
            }
        }
        return free_filter_properties;
    });

    self.histograms.push(new MaterialsFilterHistogramVM(self));

    self.RemoveHistogram = function(item){
        self.histograms.remove(item);
        adjustHeight();
    }
    self.AddHistogram = function(){
        self.histograms.push(new MaterialsFilterHistogramVM(self));
        adjustHeight();
    }

    self.showMore = ko.computed(function(){
        // console.log(self.histograms().length + " - " + filter_properties.length )
        if(self.histograms().length < filter_properties.length )
            return true;
        return false;
    });
}
var $filter;

$(document).ready(function () {
    var $sidebar = $('.b3-panel-expansible');
    $filter = $('.b3-col-filtering');
    $filter.hcSticky({
        innerSticker: $sidebar,
        top: 15,
        offResolutions: -992,
        stickTo: $('.b3-container-comparator')
    });

    $(window).resize(function () {
        if ($(window).width() < 991) {
            var parent = $filter.parent();
            $filter.width(parent.width());
        }
    });
});

$('body').on('click', '.b3-col-filtering', function () {
    adjustHeight();
});

function adjustHeight() {
    if ($filter === undefined) return;
    var wrapper = $filter.parent();
    var height = $filter.height();
    wrapper.height(height);
}
