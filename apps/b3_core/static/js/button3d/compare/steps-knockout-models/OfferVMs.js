/* global lang */

function MaterialsOfferVM(parentVM, data){
    /**
     * Binding to compare_step_materials.html
     */
    var self = this;
    self.material = slug_to_product[data.slug];
    self.material.set_has_stockrecords(data.has_stockrecords);
    self.material.cached_printability(data.printability);
    self.material.cached_printability_at_scale(data.printability_at_scale);
    self.material.cached_printability_problem(data.printability_problem);
    self.material.cached_error(data.error);
    self.error = data.error;
    self.printability = data.printability;
    self.printability_problem = data.printability_problem;
    self.printability_at_scale = data.printability_at_scale;

    // self.delivery_days_range = data.delivery_days_range;
    self.delivery_min_days = data.delivery_min_days;
    self.delivery_max_days = data.delivery_max_days;
    self.show_net_prices = Button3d.compare.materials.Steps.show_net_prices;
    self.min_price = new PriceVM(data.min_price);
    self.max_price = new PriceVM(data.max_price);
    self.popularity = data.popularity;
    self.amount_stockrecords = data.amount_stockrecords;
    self.amount_stockrecords_text = data.amount_stockrecords_text;
    self.is_approximated = data.is_approximated;
    self.tradename = self.material.tradename;
    self.manualPricingRequired = ko.observable(data.manual_pricing_required);

    self.isSingleAccount = is_single_supplier_account;

    // Display Name
    self.displayName = ko.pureComputed(function (){
        var string = '';
        if (self.tradename.length !== 0) {
            string = self.tradename + '<span>(' + self.material.title + ')</span>';
            return string;
        }
        string = self.material ? self.material.title : '';
        return string;
    });

    self.densityWidth = ko.pureComputed(function() {
        return self.widthMapping(self.material.scoreDensity());
    });
    self.densityColor = ko.pureComputed(function() {
        return self.colorMapping(self.material.scoreDensity());
    });

    self.densityValue = ko.pureComputed(function () {
        var densityMin = self.material.attr.attr_density_min;
        var densityMax = self.material.attr.attr_density_max;
        if (densityMax && densityMin) {
            var average = ((densityMin + densityMax) / 2).toFixed(2);
            return pointToComma(average);
        }
        return densityMin ? pointToComma(densityMin) : pointToComma(densityMax);
    });
    self.detailsWidth = ko.pureComputed(function() {
        return self.widthMapping(self.material.scoreDetail());
    });
    self.detailsColor = ko.pureComputed(function() {
        return self.colorMapping(self.material.scoreDetail());
    });
    self.detailsValue = ko.pureComputed(function() {
        return self.detailsMapping(self.material.scoreDetail());
    });

    self.strengthWidth = ko.pureComputed(function() {
        return self.widthMapping(self.material.scoreStrength());
    });
    self.strengthColor = ko.pureComputed(function() {
        return self.colorMapping(self.material.scoreStrength());
    });
    self.strengthValue = ko.pureComputed(function() {
        var strengthMin = self.material.attr.attr_ultimate_tensile_strength_min;
        var strengthMax = self.material.attr.attr_ultimate_tensile_strength_max;
        if (strengthMax && strengthMin) {
            var average = ((strengthMax + strengthMin) / 2).toFixed(0);
            return pointToComma(average);
        }
        return strengthMin ? strengthMin : strengthMax;
    });

    self.colorMapping = function (val) {
        if (val <= 20) { return self.shadeColor(barColor, 0.4); }
        if (val <= 40 ) { return self.shadeColor(barColor, 0.2); }
        if (val <= 60 ) { return self.shadeColor(barColor, 0); }
        if (val <= 80 ) { return self.shadeColor(barColor, -0.2); }
        if (val > 80 ) { return self.shadeColor(barColor, -0.4); }
    }

    self.detailsMapping = function (val) {
        if (val <= 20) { return gettext('Very Low')}
        if (val <= 40 ) { return gettext('Low')}
        if (val <= 60 ) { return gettext('Medium')}
        if (val <= 80 ) { return gettext('High')}
        if (val > 80 ) { return gettext('Very High')}
    }
    self.widthMapping = function (val) {
        var width = val + '%';
        return width;
    }

    self.shadeColor = function (color, percent) {
        var f = parseInt(color.slice(1), 16),
            t = percent < 0 ? 0 : 255,
            p = percent < 0 ? percent * -1 : percent,
            R = f >> 16,
            G = f >> 8 & 0x00FF,
            B = f & 0x0000FF;
        return '#' + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (
            Math.round((t - B) * p) + B)).toString(16).slice(1);
    }

    self.printabilityDict = {
        'printable': gettext('Printable'),
        'warning': gettext('Suspicious'),
        'not_printable': gettext('Not Printable'),
        'unknown': gettext('Unknown')
    };

    self.printabilityText = ko.observable(self.printabilityDict[data.printability]);


    // Display Delivery Days Range
    self.delivery_days_range = ko.observable(data.delivery_days_range);
    self.price_range = ko.pureComputed( function () {
        var priceCurrency = self.min_price.currency;
        var minPrice = self.show_net_prices ? self.min_price.excl_tax : self.min_price.incl_tax;
        var maxPrice = self.show_net_prices ? self.max_price.excl_tax : self.max_price.incl_tax;

        if (self.manualPricingRequired()) {
            return;
        }
        return minPrice != maxPrice
            ? setCurrency(minPrice, priceCurrency) +
              ' - ' +
              setCurrency(maxPrice, priceCurrency)
            : setCurrency(minPrice, priceCurrency);
    })

    self.price_text = ko.pureComputed(function computedPriceText() {
        if (is_single_supplier_account) {
            var minPrice = self.show_net_prices ? self.min_price.excl_tax : self.min_price.incl_tax;
            return setCurrency(minPrice, self.min_price.currency);
        }
        return self.is_approximated ? gettext('ca. ') + self.price_range() : self.price_range();
    }, self);

    self.price_css = ko.pureComputed(function() {
        var len = self.price_text().length;
        if (len < 11)
            return '';
        return 'b3-price-md';
    }, self);

    self.is_checked = ko.computed(function(){
        return parentVM.FilterCategories.root_category.checked_materials_slugs().indexOf(self.material.slug)>=0;
    })

    self.is_visible = ko.computed(function(){
        if(!self.is_checked())
            return false;

        var selected_characteristics = parentVM.FilterCharacteristics.selected();
        for(var j=0;j<selected_characteristics.length;j++){
            if(selected_characteristics[j].visible()) {
                if(self.material.get_attr(selected_characteristics[j].key)!=true) {
                    return false;
                }
            }
        }

        var technologies = parentVM.FilterTechnologies.selectedTechnologies();
        // Check if technologies has at least the default element and not just 'All' is checked. Because if 'All' is
        // checked there is not need to apply the filter.
        if ((!(technologies.length === 0)) && (!(technologies.length === 1 && technologies[0] === gettext('All')))) {
            var visibleByTechnology = false;
            // We need to loop through the array here to check if the technology is checked in the array of
            // technologies.
            technologies.forEach(function (technology) {
                if (self.material.technology() === technology) {
                    visibleByTechnology = true;
                }
            });
            if (!visibleByTechnology) return false;
        }

        var histograms = parentVM.FilterProperties.histograms();
        for(var j=0;j<histograms.length;j++){
            var property = histograms[j].selected_property();

            var prop_min = self.material.get_attr(property.key || property.keys[0]);
            var prop_max = self.material.get_attr(property.key || property.keys[1]);
            if(prop_min === undefined || prop_max === undefined || prop_min>histograms[j].till() || prop_max<histograms[j].from()) {
                return false;
            }
        }
        return true;
    })

    self.is_loading = ko.observable();
}


function PriceVM(price_obj) {
    var self = $.extend(this, price_obj);
    if (price_obj !== null) {
        var price = Button3d.compare.materials.Steps.show_net_prices ? price_obj.excl_tax : price_obj.incl_tax;
        if(price.length < 15) {
            self.incl_tax_limit_display = setCurrency(price, self.currency);
        } else {
            self.incl_tax_limit_display = '> ' + setCurrency('9999999', self.currency);
        }
    } else {
        //TODO: monkey way to fix this problem
        self.incl_tax_limit_display = 0;
    }
}

function SupplierOfferVM(data){
    var self = this;
    var rawIndex = data.slug.indexOf('-raw');
    if (rawIndex >= 0) {
        data.slug = data.slug.slice(0, rawIndex);
    }
    self.product = slug_to_product[data.slug];
    self.product.cached_printability(data.printability);
    self.product.cached_printability_at_scale(data.printability_at_scale);
    self.product.cached_printability_problem(data.printability_problem);
    self.product.cached_error(data.error);
    self.error = data.error;
    self.printability = data.printability;
    self.printability_problem = data.printability_problem;
    self.printability_at_scale = data.printability_at_scale;
    self.combinable_post_processings = data.combinable_post_processings;

    self.delivery_days_range = data.delivery_days_range;
    self.delivery_min_days = data.delivery_min_days;
    self.delivery_max_days = data.delivery_max_days;
    self.stockrecord_id = data.stockrecord_id;
    self.price = new PriceVM(data.price);
    self.is_draft = false; //DISABLED, but may be used later
    self.datasheet = data.datasheet;
    self.finishes = data.finishes;
    self.has_color_finishes = data.has_color_finishes;

    self.supplier = new SupplierVM(suppliers[data.supplier]);

    self.is_loading = ko.observable();

    self.support = data.support;
    self.manualPricingRequired = data.manual_pricing_required;

    self.pickup_mock_shipping_method = {
        'name': 'Pickup',
        'price': {
            'incl_tax': 0,
            'excl_tax': 0
        },
        'delivery_days_range': '0'
    };

    if(data.shipping_methods){
        var fastest_shipping_method = function () {
            if (data.shipping_methods.length > 0) {
                return data.shipping_methods.slice().sort(
                    function (a, b) {
                        return a.delivery_min_days - b.delivery_min_days || a.delivery_max_days - b.delivery_max_days;
                    })[0];
            } else {
                return self.pickup_mock_shipping_method;
            }
        };
        self.supplier_fastest_shipping_method = fastest_shipping_method().name;
        self.supplier_fastest_shipping_price = fastest_shipping_method().price.incl_tax;
        self.delivery_days_range = fastest_shipping_method().delivery_days_range;

        var cheapest_shipping_method = function () {
            if (data.shipping_methods.length > 0) {
                return data.shipping_methods.slice().sort(
                    function (a, b) {
                        return parseFloat(a.price.incl_tax) - parseFloat(b.price.incl_tax);
                    })[0];
            } else {
                return self.pickup_mock_shipping_method;
            }
        };

        self.supplier_cheapest_shipping_price_incl_tax = cheapest_shipping_method().price.incl_tax;
        self.supplier_cheapest_shipping_price_excl_tax = cheapest_shipping_method().price.excl_tax;
        self.supplier_cheapest_shipping_method = cheapest_shipping_method().name;
    }
}


function FinishingOfferVM(data, parentVM){
    var self = this;
    self.slug = ko.observable(data.slug);
    self.product = slug_to_product[data.slug];
    self.product.material.cached_printability(data.printability);
    self.product.material.cached_printability_at_scale(data.printability_at_scale);
    self.product.material.cached_printability_problem(data.printability_problem);
    self.product.material.cached_error(data.error);

    self.error = data.error;
    self.printability = data.printability;
    self.printability_problem = data.printability_problem;
    self.printability_at_scale = data.printability_at_scale;

    self.is_selected = ko.observable(false);

    self.delivery_min_days = data.delivery_min_days;
    self.delivery_max_days = data.delivery_max_days;
    self.price = new PriceVM(data.price);
    self.is_draft = false; //DISABLED, but may be used later
    self.color_ids = data.color_ids;
    self.datasheet = data.datasheet;

    self.supplier = new SupplierVM(suppliers[data.supplier]);
    self.manualPricingRequired = data.manual_pricing_required;

    self.selected_color_id = ko.observable();

    self.short_product_title = ko.computed(function () {
        var temp = self.product.title.substr(0, self.product.title.length);
        if (temp.substring(0, 3) === ' / ') {
            temp = temp.substring(3);
        }
        return temp;
    });

    self.is_loading = ko.observable();

    self.support = data.support;

    self.is_selected.subscribe(function (newIsSelected) {
        if (newIsSelected) {
            if (!parentVM.combinable_post_processings()) {
                // clear the selections if only one is allowed
                for (var i = 0; i < parentVM.offers().length; i++) {
                    if (!(self === parentVM.offers()[i])) {
                        parentVM.offers()[i].is_selected(false);
                    }
                }
            }
            parentVM.selected_offers.push(self);
        } else {
            parentVM.selected_offers.remove(self);
        }
    });
}
