function CategoryVM(data, parentVM){
    var self = this;
    self.id = data.id;
    self.path = data.path;
    self.slug = data.slug;
    self.name = data.name;
    self.depth = parseInt(data.depth);

    self.checked = ko.observable();

    self.all_sub_checked_safe = ko.observable();
    self.all_sub_checked_safe_prev = false;

    self.all_sub_checked = ko.observable();

    self.categories = ko.computed(function(){
        var result = [];

        for(var i=0;i<material_categories.length;i++){
            if(material_categories[i].path.indexOf(self.path)==0 && material_categories[i].depth == self.depth+1 ){
                var category = new CategoryVM(material_categories[i], self);
                result.push(category);
                category.checked.subscribe(function(enabled){
                    if(enabled) self.all_sub_checked(false);
                })
            }
        }

        return result;
    });

    self.all_sub_checked.subscribe(function(enabled){
        if(enabled){
            var categories = self.categories();
            for(var i=0;i<categories.length;i++){
                categories[i].checked(false);
                categories[i].all_sub_checked(true);
            }
        }
    });

    self.checked.subscribe(function(){
        if (self.checked()) {
            Button3d.common.send_event_to_ga("filter_material_" + self.slug, "checked");
        }
        self.all_sub_checked(true);
    });

    self.pre_materials = ko.computed(function(){
        var result = [];
        for(var product_slug in material_products){
            if(material_products[product_slug].category === self.id) {
                result.push(new ProductVM(product_slug, self));
            }
        }
        return result;
    });

    self.materials = ko.computed(function(){
        var all = self.pre_materials();
        var result = [];
        for(var i=0;i<all.length;i++)
            if(all[i].has_stockrecords())
                result.push(all[i]);
        return result;
    });

    self.materials_slugs = ko.computed(function(){
        var result = [];
        var materials = self.materials();
        for(var i=0;i<materials.length;i++){
            result.push(materials[i].slug);
        }
        return result;
    });

    self.checked_materials_slugs = ko.computed(function(){
        var result = [];
        var categories = self.categories();
        for(var i=0;i<categories.length;i++){
            if(self.all_sub_checked() || categories[i].checked())
                result = result.concat(categories[i].checked_materials_slugs());
        }
        result = result.concat(self.materials_slugs());
        return result;
    });

    self.all_sub_checked(data.depth==0);

    self.all_sub_checked_safe_update = ko.computed(function(){
        if(self.all_sub_checked_safe_prev == false && self.all_sub_checked_safe() && !self.all_sub_checked()){
            self.all_sub_checked(true);
        }
        self.all_sub_checked_safe(self.all_sub_checked());
        self.all_sub_checked_safe_prev = self.all_sub_checked_safe();
    });

    self.all_sub_checked_update = ko.computed(function(){
        var categories = self.categories();

        for(var i=0;i<categories.length;i++){
            if(categories[i].checked())
                return;
        }

        self.all_sub_checked(true);
    });

    self.resetMaterial = function(){
        self.all_sub_checked_safe(true);
    };

    self.num_materials = ko.computed(function(){

        if(self.depth == 0 || self.depth == 1){
            var nb_materials = 0;
            for(var i=0;i < self.categories().length;i++){
                nb_materials = nb_materials + self.categories()[i].num_materials();
            }
            return nb_materials;
        }else {
            return self.materials().length;
        }

    })
}
