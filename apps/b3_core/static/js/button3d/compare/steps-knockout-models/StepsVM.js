function StepsVM(){
    var self = this;
    self.line_material = ko.observable();
    Button3d.userpanel.project.api.get_project_line(
        projectId,  lineId,
        false, get_line_success,
        get_line_error
    );
    Button3d.compare.materials.Steps = self;
    self.scale = ko.observable(Button3d.common.scale.getScale());
    self.unit = ko.observable(Button3d.common.scale.getUnit());
    self.show_label_printability = Button3d.common.repair.is_finished;
    self.printable_selected = ko.observable();
    self.default_material_slug = 'pa12-pa2200';
    self.country_user = $(".select-country option[value='"+country_user+"']").text();
    // Hacky way to get globals vars here by using hidden inputs...
    self.uuid = $('#uuid').val();
    self.show_net_prices = $('#show_net_prices').val() === 'True';

    $("body").on("scaleChange", function(){
        self.scale(Button3d.common.scale.getScale());
        self.unit(Button3d.common.scale.getUnit());
    });

    self._auto_update_basket_tooltip = ko.computed(function(){
        if(Button3d.common.repair.is_finished() || Button3d.common.repair.is_parameter_available()){
            $('[data-toggle="tooltip-basket"]').tooltip();
        }
    })

    $(".b3-currencies > a").click(function(){
        updateCurrency($(this).text());
    });

    function get_line_success(line_response) {
        self.line_material(slug_to_product[line_response.product_slug]);
    }

    function get_line_error(line_error) {
        self.line_material(null);
    }

    function updateCurrency(currency){
        Button3d.common.send_event_to_ga("currency", "change_to_" + currency);
        var selected_currency = currency;
       //send currency to server to save it
        $.ajax({
            type: 'POST',
            url: '/change_site_configuration/',
            data: {
                "currency": selected_currency
            }
        }).done(function() {
            self.currency(selected_currency);
        });
    }

    self.currency = ko.observable($('.initial-currency').text());
    self.step = ko.observable("materials");
    self.selected_material_slug = ko.observable();
    self.selected_material = ko.computed(function(){
        return slug_to_product[self.selected_material_slug()];
    });
    self.combinable_post_processings = ko.observable(null);
    self.base_material_price = ko.observable(null);
    self.base_delivery_min_days = ko.observable(null);
    self.base_delivery_max_days = ko.observable(null);

    self.selected_partner_slug = ko.observable();
    self.selected_partner_title = ko.observable("-----");
    self.selected_partner_city = ko.observable("-----");
    self.selected_partner_country = ko.observable("-----");
    self.selected_partner_address = ko.observable("-----");
    self.selected_partner_email = ko.observable("-----");
    self.selected_partner_website = ko.observable("-----");
    self.selected_partner_phone = ko.observable("-----");
    self._auto_update_selected_partner_slug = ko.computed(function(){
        if(!self.selected_partner_slug()) return;
        var supplier = new SupplierVM(suppliers[self.selected_partner_slug()]);
        self.selected_partner_title(supplier.title);
        self.selected_partner_address(supplier.address);
        self.selected_partner_email(supplier.email);
        self.selected_partner_website(supplier.website);
        self.selected_partner_phone(supplier.phone);
        self.selected_partner_city(supplier.city);
        self.selected_partner_country(supplier.country);
    })
    self.selected_partner_full_address = ko.computed(function(){
        return self.selected_partner_address()+' '+self.selected_partner_city() + '-' +self.selected_partner_country();
    });

    self.selected_partner_info = ko.computed(function(){
        return {
         phone : self.selected_partner_phone(),
         email : self.selected_partner_email(),
         website : self.selected_partner_website(),
         address : self.selected_partner_full_address()
        }
    });
    self.update_contact_panel_info = ko.observable(null);

    self.StepMaterials = ko.observable(new StepMaterialsVM(self));
    self.StepPartners = ko.observable(new StepPartnersVM(self));
    self.StepExtra = ko.observable(new StepExtraVM(self));

    self.SwitchToStepMaterials = function(){
        self.step("materials");
        self.StepExtra().selected_offers([])
        // scrollableSideBar();
        adjustHeight();
    };

    self.SwitchToStepPartners = function(){
        self.step("partners");
        self.StepExtra().selected_offers([])
        // scrollableSideBar();
        adjustHeight();
    };

    self.StepMaterialsSelect = function(offer){
        self.handleStepMaterialsSelect(offer.material.slug);
    };

    self.get_cheapest_offer = function (material_slug) {
        compare_api.get_supplier_offers(
            self.uuid,
            self.scale(),
            self.currency(),
            material_slug,
            function (offers) {
                var cheapest_offer = offers.results.sort(function (a, b) {
                    return a.price.incl_tax < b.price.incl_tax;
                })[0];
                self.StepExtra().combinable_post_processings(cheapest_offer.combinable_post_processings);
                self.StepExtra().base_delivery_min_days(cheapest_offer.delivery_min_days);
                self.StepExtra().base_delivery_max_days(cheapest_offer.delivery_max_days);
                if (cheapest_offer.price) {
                    self.StepExtra().base_material_price(
                        self.show_net_prices ? cheapest_offer.price.excl_tax : cheapest_offer.price.incl_tax
                    );
                } else {
                    self.StepExtra().base_material_manual_pricing_required(
                        cheapest_offer.manual_pricing_required
                    );
                }
                self.selected_partner_slug(cheapest_offer.supplier);
                self.step("extra");
                self.update_contact_panel_info(true);
            },
            function (e) {}
        );
    }

    self.handleStepMaterialsSelect = function(material_slug){
        self.selected_material_slug(material_slug);
        if (is_single_supplier_account){
            self.get_cheapest_offer(material_slug);
        } else {
            self.step("partners");
            Button3d.compare.materials.Steps.StepMaterials().has_maximum_view(null);
            // scrollableSideBar();
            adjustHeight();
        }
    };

    self.StepPartnersSelect = function(offer){
        self.selected_partner_slug(offer.supplier.slug);
        self.StepExtra().combinable_post_processings(offer.combinable_post_processings);
        self.StepExtra().base_material_price(self.show_net_prices ? offer.price.excl_tax : offer.price.incl_tax);
        self.StepExtra().base_delivery_min_days(offer.delivery_min_days);
        self.StepExtra().base_delivery_max_days(offer.delivery_max_days);
        self.StepExtra().supplier_cheapest_shipping_method(offer.supplier_cheapest_shipping_method);
        self.step("extra");
        // scrollableSideBar();
        adjustHeight();
    };

    self.sammy = Sammy(function() {
        this.get('#/:material/:partner', function(){
            if(self.step() == "extra")return;
            var material_slug = this.params.material;

            self.handleStepMaterialsSelect(material_slug);

            var partner_slug = this.params.partner;

            self.selected_partner_slug(partner_slug);
            self.update_contact_panel_info(true);

            //Show Extra Step
            self.step("extra");
            // scrollableSideBar();
            adjustHeight();
        });

        this.get('#/:material', function(){
            if(self.step() == "partners") return;

            var material_slug = this.params.material;
            self.handleStepMaterialsSelect(material_slug);
        });

        this.get('#/', function() {
            self.step("materials");
            // scrollableSideBar();
            adjustHeight();
        });

        this.bind('event-context-after', function() {
            if (typeof ga != 'undefined')
                ga('send', 'pageview', {'page': location.pathname+location.search+location.hash});
        });

        this.notFound = function(){
            //do nothing
        };

        // don't touch forms
        this._bind = this.bind;
        this.bind = function(action, func){
            if(action!='submit')
                this._bind(action, func);
        };
    }).run();

    self._auto_update_hashtag=ko.computed(function(){
        var step = self.step();
        var partner_slug = self.selected_partner_slug();
        var selected_material_slug = self.selected_material_slug();
        if(step == "materials"){
            location.href = '#/';
        } else if(step == "partners"){
            location.href = '#/'+selected_material_slug;
        } else if(step == "extra"){
            if(is_single_supplier_account)
                location.href = '#/'+selected_material_slug;
            else
                location.href = '#/'+selected_material_slug+'/'+partner_slug;

        }
    });

    self.material_printability = ko.computed(function(){
        var line_material  = self.line_material();
        var selected_material = self.selected_material();
        var step = self.step();
        var printable_selected = self.printable_selected();
        var offers = self.StepMaterials().offers() || [];
        var is_printable_selected_in_offers = false;
        if(printable_selected){
            for(var i=0;i<offers.length;i++){
                if(offers[i].material.slug==printable_selected.slug){
                    is_printable_selected_in_offers = true;
                    break;
                }
            }
        }
        if (line_material && !selected_material) {
            return line_material;
        } else if(selected_material && step != "materials") {
            return selected_material;
        } else if(printable_selected && is_printable_selected_in_offers) {
            return printable_selected;
        } else if(step == "materials" && offers.length) {
            return slug_to_product[offers[0].material.slug];
        } else {
            return slug_to_product[self.default_material_slug];
        }
    });

    self.materialDisplayName = ko.computed(function () {
        var stringName = '';
        if (self.selected_material()) {
            stringName = (self.selected_material().tradename)
             ? self.selected_material().tradename + ' (' + self.selected_material().title + ')'
             : self.selected_material().title;
            return stringName;
        }
    });

    self._auto_update_material_printability = ko.computed(function(){
        self.scale();
        var material_printability = self.material_printability();
        if (self.show_label_printability() && material_printability) {
            material_printability.show_printability(true);
        }
    })
}
