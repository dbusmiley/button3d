function StepPartnersVM(parentVM){
    var self = this;

    self.sort_method = ko.observable();

    self.request_arguments = ko.computed(function(){
        return {
            scale: parentVM.scale(),
            currency: parentVM.currency(),
            material: parentVM.selected_material_slug()
        };
    });

    self.is_sculpteo_available = ko.observable(false);
    function checkSculpteo(offers){
        for(var i=0;i < offers.length;i++){
            if(offers[i].partner_slug == 'sculpteo'){
                self.is_sculpteo_available(true);
            }
        }
    }

    /**
     * Function to sort two SupplierOfferViewModel's by their location.
     * The location will get fetched by a global JavaScript variable.
     * @param {object} supplierOfferViewModelA The first View Model to compare.
     * @param {object} supplierOfferViewModelB The second View Model to compare.
     * @returns {number} -1, 0, 1 regarding to the result of the comparison. For further information lookup
     * {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/sort|Array.prototype.sort()}.
     */
    function sortByLocation(supplierOfferViewModelA, supplierOfferViewModelB) {
        var fullCountry;
        var countries = Button3d.common.countries();
        for (var i = 0; i < countries.length; i++) {
            if (countries[i].iso === country_user) {
                fullCountry = countries[i];
                break;
            }
        }
        var result = 0;
        // Intercept if there are no offers for the main country of the user.
        if (fullCountry === undefined) {
            return result;
        }
        if (supplierOfferViewModelA.supplier.country === fullCountry.name) {
            result = -1;
        } else if (supplierOfferViewModelB.supplier.country === fullCountry.name) {
            result = 1;
        }
        if (supplierOfferViewModelA.supplier.country === supplierOfferViewModelB.supplier.country) {
            result = 0;
        }
        return result;
    }

    /**
     * Function to sort two SupplierOfferViewModel's by their total rating.
     * @param {object} supplierOfferViewModelA The first View Model to compare.
     * @param {object} supplierOfferViewModelB The second View Model to compare.
     * @returns {number} -1, 0, 1 regarding to the result of the comparison. For further information lookup
     * {@link https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/sort|Array.prototype.sort()}.
     */
    function sortByRating(supplierOfferViewModelA, supplierOfferViewModelB) {
        if (supplierOfferViewModelA.supplier.partnerTotalRating === supplierOfferViewModelB.supplier.partnerTotalRating) {
            return 0;
        }
        var result = supplierOfferViewModelA.supplier.partnerTotalRating < supplierOfferViewModelB.supplier.partnerTotalRating;
        return result ? 1 : 0;
    }
    function sortByPrice(a, b) {
        if (a.manualPricingRequired) {
            return 1;
        }
        if (b.manualPricingRequired) {
            return -1;
        }
        return a.price.incl_tax - b.price.incl_tax;
    }

    function sortByDelivery(a, b) {
        if (a.delivery_min_days === b.delivery_min_days) {
            return a.delivery_max_days - b.delivery_max_days;
        }
        return a.delivery_min_days - b.delivery_min_days;
    }

    self.ajax_offers = ko.observableArray();
    self.offers = ko.computed(function() {
        var result = self.ajax_offers();
        if (self.sort_method() === 'PRICE') {
            result.sort(sortByPrice);
        } else if (self.sort_method() === 'DELIVERY') {
            result.sort(sortByDelivery);
        } else if (self.sort_method() === 'RATING') {
            result.sort(sortByRating);
        } else if (self.sort_method() === 'BY_LOCATION') {
            // We need to sort by price first and then by location to get the right ordering after home country
            // suppliers.
            result.sort(sortByPrice);
            result.sort(sortByLocation);
        }
        checkSculpteo(result);
        // this line disables discount display
        self.is_sculpteo_available(false);
        return result;
    });

    self.is_loading = ko.observable(true);


    self._update_offers_timeout = 0;
    self._update_offers = ko.computed(function(){
        self.is_sculpteo_available(false);
        self.request_arguments();
        parentVM.step();
        //don't touch, must be subscribed for both events
        Button3d.common.repair.is_finished();
        Button3d.common.repair.is_parameter_available();

        self._update_offers_timeout && clearTimeout(self._update_offers_timeout);
        self._update_offers_timeout = setTimeout(function(){
            var request_arguments = self.request_arguments();
            if(parentVM.step() != "partners") return;

            if(self._old_material != parentVM.selected_material_slug())
                self.ajax_offers([]);
            self._old_material = parentVM.selected_material_slug();
            var offers = self.offers();
            for(var i=0;i<offers.length;i++)
                offers[i].is_loading(true);

            self.is_loading(true);

            if(!Button3d.common.repair.is_parameter_available())
                return;

            if(self._prev_xhr){
                self._prev_xhr.abort();
                self._prev_xhr = undefined;
            }

            self._prev_xhr = compare_api.get_supplier_offers(
                parentVM.uuid,
                request_arguments.scale,
                request_arguments.currency,
                request_arguments.material,
                function (data) {
                    var result = [];
                    data.results.forEach(function (dataEntry) {
                        if (dataEntry.manual_pricing_required || dataEntry.price) {
                            result.push(new SupplierOfferVM(dataEntry));
                        }
                    });
                    self.ajax_offers(result);

                    self.is_loading(false);
                },
                function (e) {
                    console.log(e);
                }
            );
        }, 100);
    });
}
