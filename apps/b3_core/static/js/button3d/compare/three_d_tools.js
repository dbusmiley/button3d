Button3d.compare.three_d_tools = (function () {
    var publix = {};

    var panel_active, selected_panel, initScale, scale, panel_changed;

    // starts with the progressbar animation immediately
    publix.init = _init;
    function _init() {
        panel_active = false;
        initScale = scale = Button3d.common.scale.getScale();
        $('body').on('scaleChange', update3DInformations);
    }
    publix.show_signup = Button3d.modals.showSignup;

    //does all the animations after the opening/closing of the 3D tools panel or the file settings panel
    publix.animate_panel = animate_panel;

    var panel;

    function moveTriangle (panelName) {
        if (panelName == 'options'){
            panel = $('#viewer-options');
        } else if (panelName == 'settings' ){
            panel = $('#viewer-settings');
        }
    }

    function moveControl (control) {
        if ( control == 'move') {
            $('#webgl').addClass("move_model").removeClass("move_model_back");
            $('#b3-webgl-scale').addClass("move_webgl_scale").removeClass("move_webgl_scale_back");
            $('.b3-button-holder').addClass("move_legend").removeClass("move_legend_back");
            $('.sprite-mouse_rotate').hide();
            $('.sprite-mouse_zoom').hide();
            $('.sprite-mouse_pan').hide();
        } else {
            $('#webgl').removeClass("move_model").addClass("move_model_back");
            $('#b3-webgl-scale').removeClass("move_webgl_scale").addClass("move_webgl_scale_back");
            $('.b3-button-holder').removeClass("move_legend").addClass("move_legend_back");
            $('.sprite-mouse_rotate').show();
            $('.sprite-mouse_zoom').show();
            $('.sprite-mouse_pan').show();
            $('.b3-3d-tools-legend').hide();
        }
    }

    $(window).resize(function() {
        var height = $(window).height();
        var width = $(window).width();
        var newPanelHeight = height - 50;
        if (Button3d.viewer.webgl.fullscreen.isEnabled()){
            if (width > 991) {
                $('.b3-3d-tools-panels').css('height', newPanelHeight);
            }
        } else {
            if (width > 991) {
                $('.b3-3d-tools-panels').css('height', 420);
            } else {
                $('.b3-3d-tools-panels').css('height', 'auto')
            }
        }

        if (panel_active){
            if (width > 991) {
                moveControl('move');
            } else {
                moveControl('moveback');
            }
        }

        moveTriangle(selected_panel);
    });

    function animate_panel (sel_panel, change, fromtag) {
    /**
     * variable: sel_panel,
     *  options => open options panel window
     *  settings => open settings panel window
     * variable: change,
     *  true => move the panel very fast
     *  false => with animation
     * variable: fromtag,
     *  true => will not close the options panel when click the same
     *  false => behave as the same
     */

        if(show_signup){
            Button3d.modals.showSignup();
            return;
        }

        panel_changed = change;
        selectedPanel = sel_panel;

        moveTriangle(sel_panel);

        // no panel was active before
        if (!panel_active) {

            // fullscreen, when clicked
            if (Button3d.viewer.webgl.fullscreen.isEnabled()) {
                if ($(window).width() < 992) {
                    panel.show().removeClass("move_panel move_panel_back");
                    $('.viewer-tools').css("height", "inherit");
                    Button3d.viewer.webgl.fullscreen.disable();
                    window.scrollTo(0, $('#b3-3d-tools').offset().top);
                } else {
                    if (change) {
                        panel.show().addClass("move_panel_fast").removeClass("move_panel_back");
                    } else {
                        panel.show().addClass("move_panel").removeClass("move_panel_back");
                    }
                }
            } else {
                if (change) {
                    panel.show().addClass("move_panel_fast").removeClass("move_panel_back");
                } else {
                    panel.show().addClass("move_panel").removeClass("move_panel_back");
                }
            }

            setTimeout(function () {
                $('.b3-triangle').show();
                $('.b3-triangle-border').show();
            }, 300);



            if (sel_panel == "options") {
                $('.b3-3d-tools-legend').show();
            }


            if ($(window).width() >= 992) {
                moveControl('move');
            }
            panel_active = true;
            // reload the analysed.ply file only when printability&scale button was clicked.
            if (sel_panel == 'options') {
                setTimeout(function () {
                    Button3d.common.repair.replaceSTLFile(updateModelColor);
                }, 10);
            } else {
                if (change) {
                    $('.b3-3d-tools-legend').hide();
                    setTimeout(function () {
                        Button3d.common.repair.replaceSTLFile();
                    }, 300);
                }
            }
            selected_panel = sel_panel;
        } else {
            // a panel was active
            if (selected_panel == sel_panel && !fromtag) {
                // when it was the same panel, close it
                panel.removeClass("move_panel move_panel_fast").addClass("move_panel_back");
                $('.b3-triangle').hide();
                $('.b3-triangle-border').hide();

                if ($(window).width() >= 992) {
                    moveControl('moveback');
                }

                panel_active = false;

                setTimeout(function () {
                    panel.hide();
                    if (sel_panel == 'options') {
                        Button3d.common.repair.replaceSTLFile();
                    }
                }, 300);

            } else {
                // when it is a different panel, find out which one it was and close old one, and open new one
                if (selected_panel == 'options') {
                    panel = $('#viewer-options');
                    panel.removeClass("move_panel move_panel_fast").hide();
                } else if (selected_panel == 'settings') {
                    panel = $('#viewer-settings');
                    panel.removeClass("move_panel move_panel_fast").hide();
                }
                panel_active = false;
                animate_panel(sel_panel, true);
            }

        }
    }

    //updates the Model color dynamically when the analysed.ply file was loaded
    function updateModelColor() {
        Button3d.common.repair.updateFile(true);
    }


    // sets the 3D Informations from the database to the 3D tools panel
    var d,w,h,a,vol  = null;


    publix.update3DInformations = update3DInformations;
    function update3DInformations() {

        var stlFile = Button3d.common.repair.getStlFile();
        var scale = Button3d.common.scale.getScale();

        if (!stlFile)
            return;

        if (stlFile.shells != null) {
            $('#info-shells').text(stlFile.shells);
        } else {
            $('#info-shells').text("n/a");
        }
        if (stlFile.faces != null) {
            $('#info-faces').text(stlFile.faces);
        } else {
            $('#info-faces').text("n/a");
        }
        if (stlFile.holes != null) {
            $('#info-holes').text(stlFile.holes);
        } else {
            $('#info-holes').text("n/a");
        }
        if (stlFile.d != null && stlFile.w != null && stlFile.h != null) {
            var box_volume = stlFile.d  * stlFile.w * stlFile.h;
            box_volume *= Math.pow(scale/1, 3);
            updateBoxVolume(box_volume);

        } else {
            $('#info-box-volume').text("n/a");
        }
        if (stlFile.volume != null) {
            var volume = stlFile.volume;
            volume *= Math.pow(scale/1, 3);
            updateVolume(volume);

        } else {
            $('#info-volume').text("n/a");
        }
        if (stlFile.area != null) {
            var area = stlFile.area;
            area *= Math.pow(scale/1, 2);
            updateArea(area);
        } else {
            $('#info-area').text("n/a");
        }
    }

    // returns if one of the panels are open or closed
    publix.is_panel_active = is_panel_active;
    function is_panel_active(){
        return panel_active;
    }

    // returns if there was a panel change
    publix.get_active_panel = get_active_panel;
    function get_active_panel(){
        return selected_panel;
    }

    // updates the 3D Informations, when the scale is changed
   /* publix.update3DInformations = update3DInformations;
    function update3DInformations() {

        $('#printability-panel-info').hide();
        $('#wta-info-alert-printability').hide();
        $('#wta-info-text-printable').hide();

        $('#printability-panel-loading').show();

        var newScale = Button3d.common.scale.getScale();
        var box_volume = d * w * h;
        box_volume *= Math.pow(newScale/1, 3);
        updateBoxVolume(box_volume);

        var volume = vol;
        volume *= Math.pow(newScale/1, 3);
        updateVolume(volume);

        var area = a;
        area *= Math.pow(newScale/1, 2);
        updateArea(area);

        scale = newScale;
    }*/

    function updateVolume(volume){
        var unit = $('.b3-btn-unit-active').data('unit');
        if (volume >= 1000000000) {
            if(unit == 'mm'){
                $('#info-volume').text(pointToComma((Math.round(volume * 0.000000001 * 100)/ 100) + " m³"));
            }else{
                 // mm3 to yd3 : 0.0000000013079506
                 $('#info-volume').text(pointToComma((Math.round(volume * 0.0000000013079506 * 100)/ 100) + " yd³"));
                 // inch3 to yd3: 0.00002143347
                 //$('#info-volume').text((Math.round(volume * 0.00002143347 * 100)/ 100) + " yd³");
            }
        }else if (volume >= 100000) {
            if(unit == 'mm'){
                $('#info-volume').text(pointToComma((Math.round(volume  * 0.001 * 100)/ 100)  + " cm³"));
            }else{
                // mm3 to inch3 : 0.000061023744
                $('#info-volume').text(pointToComma((Math.round(volume *  0.000061023744 * 100)/ 100)  + " in³"));
                // inch3 to inch3
                //$('#info-volume').text((Math.round(volume  * 100)/ 100) + " in³");
            }
        } else {
            if(unit == 'mm'){
                $('#info-volume').text(pointToComma((Math.round(volume * 100)/ 100)  + " mm³"));
            }else{
                // mm3 to inch3 : 0.000061023744
                 $('#info-volume').text(pointToComma((Math.round(volume * 0.000061023744 * 100)/ 100)  + " in³"));
                // inch3 to inch3
                //$('#info-volume').text((Math.round(volume  * 100)/ 100) + " in³");
            }
        }
        if (isNaN(volume) || volume == 0 ) {
            $('#info-volume').text("n/a");
        }
    }

    function updateBoxVolume(box_volume){
        var unit = $('.b3-btn-unit-active').data('unit');
        if (box_volume >= 1000000000) {
            if(unit == 'mm'){
                $('#info-box-volume').text(pointToComma((Math.round(box_volume * 0.000000001 * 100)/ 100) + " m³"));
            }else{
                 // mm3 to yd3 : 0.0000000013079506
                 $('#info-box-volume').text(pointToComma((Math.round(box_volume * 0.0000000013079506 * 100)/ 100) + " yd³"));
                 // inch3 to yd3: 0.00002143347
                 //$('#info-box-volume').text((Math.round(box_volume * 0.00002143347 * 100)/ 100) + " yd³");
            }
        }else if (box_volume >= 100000) {
            if(unit == 'mm'){
                $('#info-box-volume').text(pointToComma((Math.round(box_volume  * 0.001 * 100)/ 100)  + " cm³"));
            }else{
                 // mm3 to inch3 : 0.000061023744
                 $('#info-box-volume').text(pointToComma((Math.round(box_volume *  0.000061023744 * 100)/ 100) + " in³"));
                 // inch3 to inch3
                 //$('#info-box-volume').text((Math.round(box_volume  * 100)/ 100) + " in³");
            }
        } else {
            if(unit == 'mm'){
                $('#info-box-volume').text(pointToComma((Math.round(box_volume * 100)/ 100)  + " mm³"));
            }else{
                 // mm3 to inch3 : 0.000061023744
                 $('#info-box-volume').text(pointToComma((Math.round(box_volume * 0.000061023744 * 100)/ 100)  + " in³"));
                 // inch3 to inch3
                 //$('#info-box-volume').text((Math.round(box_volume  * 100)/ 100) + " in³");
            }
        }
        if (isNaN(box_volume) || box_volume == 0) {
            $('#info-box-volume').text("n/a");
        }
    }

    function updateArea(area){
        var unit = $('.b3-btn-unit-active').data('unit');
        if (area >= 1000000) {
            if(unit == 'mm'){
                $('#info-area').text(pointToComma((Math.round(area * 0.000001 * 100)/ 100) + " m²"));
            }else{
                // mm2 to yd2 : 0.00000119599
                $('#info-area').text(pointToComma((Math.round(area  * 0.00000119599 * 100)/ 100)  + " yd²"));
                // inch2 to yd2 : 0.00077160
                // $('#info-area').text((Math.round(area  * 0.00077160 * 100)/ 100)  + " yd²");
            }
        }else if (area >= 10000) {
            if(unit == 'mm'){
                $('#info-area').text(pointToComma((Math.round(area  * 0.01 * 100)/ 100)  + " cm²"));
            }else{
                // mm2 to inch2 : 0.0015500
                $('#info-area').text(pointToComma((Math.round(area * 0.0015500 * 100)/ 100)  + " in²"));
                // inch2 to inch2
                //$('#info-area').text((Math.round(area  * 100)/ 100)  + " in²");
            }
        } else {
            if(unit == 'mm'){
                $('#info-area').text(pointToComma((Math.round(area * 100)/ 100)  + " mm²"));
            }else{
                // mm2 to inch2 : 0.0015500
                $('#info-area').text(pointToComma((Math.round(area * 0.0015500 * 100)/ 100)  + " in²"));
                // inch2 to inch2
                //$('#info-area').text((Math.round(area  * 100)/ 100)  + " in²");
            }
        }

        if (isNaN(area) || area == 0) {
            $('#info-area').text("n/a");
        }
    }



    return publix;
}());
