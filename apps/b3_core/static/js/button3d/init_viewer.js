var Button3d = Button3d || {};

var viewer = extend(Button3d, 'viewer');
$.extend(viewer, {
    init: function () {
        Button3d.viewer.supportstructure.init();
        Button3d.viewer.orientation.init();
        Button3d.viewer.draw.init();
        $('body').on('analysingFinished', function() {
            Button3d.viewer.wta.init();
        });
    }
});
