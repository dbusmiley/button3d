Button3d.viewer.wta = function () {
    var s3_url = undefined;
    var distances = undefined;
    var coloredMesh = undefined;
    var oldMesh = undefined;

    var publix = {
        isJsonPresent: false,
        show: show,
        hide: hide,
        update: update,
        init: init
    };

    function createColorableMesh(geom) {
        var geometry;
        if (geom.index) {
            geometry = nonIndexedFromIndexed(geom);
        } else {
            geometry = geom;
        }
        var numFaces = geometry.getAttribute("position").count / 3;
        var colors = new Float32Array(numFaces * 3 * 3);
        geometry.addAttribute('color', new THREE.BufferAttribute(colors, 3));
        geometry.dynamic = true;
        geometry.__dirtyColors = true;
        geometry.computeVertexNormals();
        return new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({vertexColors: THREE.VertexColors}));
    }

    function nonIndexedFromIndexed(geometry) {
        var indices = geometry.index.array;
        var oldPositions = geometry.getAttribute("position").array;
        var newPositions = new Float32Array(indices.length * 3);
        for (var i = 0; i < indices.length; i++) {
            for (var j = 0; j < 3; j++) {
                newPositions[3 * i + j] = oldPositions[3 * indices[i] + j]
            }
        }
        var newGeometry = new THREE.BufferGeometry();
        newGeometry.addAttribute("position", new THREE.BufferAttribute(newPositions, 3));
        return newGeometry
    }

    function show() {
        if (publix.isJsonPresent) {
            update();
            oldMesh = Button3d.viewer.webgl.getCurrentMesh();
            Button3d.viewer.webgl.replaceMesh(coloredMesh);
            Button3d.viewer.webgl.render();
        }
    }

    function update() {
        if (publix.isJsonPresent) {
            var colorInfo = getColorInformation();
            var colors = [];
            for (var i = 0; i < distances.length; i++) {
                var dist = distances[i] * colorInfo.scale;
                if (dist <= colorInfo.minThick) {
                    // red
                    colors.push(new THREE.Color(0xA7082A));
                } else if (dist >= colorInfo.optThick) {
                    // green
                    colors.push(new THREE.Color(0x3C9D07))
                } else {
                    // yellow
                    colors.push(new THREE.Color(0xF7B20F))
                }
            }

            var colorBuffer = coloredMesh.geometry.getAttribute("color").array;
            for (i = 0; i < colors.length; i++) {
                var color = colors[i];
                var triangleBuffer = [color.r, color.g, color.b, color.r, color.g, color.b, color.r, color.g, color.b];
                for (var j = 0; j < 9; j++) {
                    colorBuffer[i * 9 + j] = triangleBuffer[j];
                }
            }
            coloredMesh.geometry.colorsNeedUpdate = true;
            coloredMesh.geometry.attributes.color.needsUpdate = true;
            Button3d.viewer.webgl.render();
        } else {
            legacyUpdate();
        }
    }

    // methods for updating the colors of analysed.ply model
    var legacyUpdate = function () {
        function setTimeoutWithClear(key, callback, timeout) {
            key = key + "SetTimeoutId";
            if (window[key])
                clearTimeout(window[key]);
            window[key] = setTimeout(callback, timeout)
        }

        // We update the mesh, instead of reloading the mesh,
        // we only change the color of the mesh.
        // Then the position doesn't move and it's faster.
        function updateMesh() {
            setTimeoutWithClear("updateMesh", function () {
                forceUpdateMesh();
            }, 100);
        }

        function forceUpdateMesh() {
            try {
                var colorInfo = getColorInformation();
                var current_mesh = Button3d.viewer.webgl.getCurrentMesh();
                if (!current_mesh.geometry.colors) return;

                var geometry = current_mesh.geometry;
                for (var i = 0; i < geometry.colors.length; i++) {
                    var dist = geometry.lineDistances[i] * colorInfo.scale;
                    if (dist <= colorInfo.minThick) {
                        // Set face red color
                        geometry.colors[i].setHex(0xA7082A);
                    } else if (dist >= colorInfo.optThick) {
                        // Set face green color
                        geometry.colors[i].setHex(0x3c9d07);
                    } else {
                        // Set face yellow color
                        geometry.colors[i].setHex(0xF7B20F);
                    }
                }

                geometry.colorsNeedUpdate = true;
                Button3d.viewer.webgl.render();

            } catch (err) {
                console.log(err);
                window.forceUpdateMeshErrorCounter = (window.forceUpdateMeshErrorCounter || 0) + 1;
                forceUpdateMeshErrorCounter < 10 && forceUpdateMesh();
            }
        }

        return updateMesh
    }();


    function hide() {
        if (publix.isJsonPresent) {
            Button3d.viewer.webgl.replaceMesh(oldMesh);
        }
    }


    function init(callback) {
        var url = '/u/' + document.getElementById('uuid').value + '/';
        $.getJSON(url + 'download_wta/')
            .done(function (json) {
                new THREE.CTMLoader().load(url + "download_viewer_optimized", function (geometry) {
                    geometry.applyMatrix(Button3d.viewer.webgl.computeCenteringMatrix(geometry));
                    distances = json.distances;
                    coloredMesh = createColorableMesh(geometry);
                    publix.isJsonPresent = true;
                    callback && callback();
                });
            })
            .fail(function () {
                console.log("wta.json not present");
                publix.isJsonPresent = false;
                callback && callback()
            });
    }

    function getColorInformation() {
        var cur_material, scale;
        cur_material = Button3d.compare.materials.Steps.printable_selected();
        scale = Button3d.compare.materials.Steps.scale();

        var minThick = cur_material.attr_wall_min();
        var optThick = cur_material.attr_wall_opt();

        return {minThick: minThick, optThick: optThick, scale: scale}
    }

    $("body").on("reloadFiles", function() {
        init(Button3d.viewer.webgl.render);
    });

    return publix;
}();
