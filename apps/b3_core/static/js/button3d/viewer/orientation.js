/**
 * Created by jakob on 19.09.16.
 */

Button3d.viewer.orientation = function () {
    var orientation = [0, 0, 0];

    // reverse_matrix holds a rotation matrix which orients the model back to the state of [0.0,0]
    var reverse_matrix = new THREE.Matrix4();
    var ground_plane_shown = false;
    var ground_size;

    var publix = {params: orientation};

    var render;

    function init(callback) {
        render = Button3d.viewer.webgl.render;

        var initial_orientation_request = get_orientation();
        initial_orientation_request.done(function (data) {
            if (Button3d.viewer.webgl.getModelObject()) {
                Button3d.viewer.webgl.getModelObject().rotation.set(0, 0, 0);
            }
            publix.params = data.rotation.slice();
            var rotation_matrix = createRotationMatrix(publix.params);
            reverse_matrix.getInverse(rotation_matrix);

            if (ground_plane_shown) {
                show_ground_plane();
            }
        });
    }

    function post_request(uuid, fields) {
        return $.ajax({
            url: '/' + lang + '/u/' + uuid + '/',
            type: 'POST',
            data: fields
        });
    }

    function get_request(uuid) {
        return $.ajax({
            url: '/' + lang + '/u/' + uuid + '/',
            type: 'GET'
        });
    }

    function set_orientation(x, y, z) {
        var fields = {'orientation_x': x, 'orientation_y': y, 'orientation_z': z};
        return post_request(document.getElementById('uuid').value, fields)
    }

    function get_orientation() {
        return get_request(document.getElementById('uuid').value)
    }

    // rotates the file on the server and reloads the file in the viewer afterwards.
    function rotate(success_callback, error_callback) {
        // this function reloads the viewer if the rotation job is finished and rechecks every 2000ms if not
        function check_finished() {
            var status_request = get_orientation();
            status_request.done(function (data) {
                if (data.status.status == "finished") {
                    $("body").trigger("reloadFiles");
                    Button3d.common.repair.replaceSTLFile(success_callback);
                } else if (data.status.status == "error") {
                    error_callback && error_callback();
                } else if (data.status.status == "analysing") {
                    setTimeout(check_finished, 2000);
                }
            });
            status_request.fail(function () {
                error_callback && error_callback();
            })
        }

        // request rotation of the file from server
        var request = set_orientation(publix.params[0], publix.params[1], publix.params[2]);
        request.done(check_finished);
        request.fail(function () {
            if (error_callback) {
                error_callback()
            }
        });
    }

    // rotates the model in the viewer, not making any changes to the underlying file.
    function viewer_rotate() {
        var model = Button3d.viewer.webgl.getModelObject();
        // first reset the model to orientation [0,0,0]
        model.matrix = reverse_matrix;
        model.rotation.setFromRotationMatrix(model.matrix);

        // then do exactly the same as the backend (rotate the initial, unrotated file)
        var rotation_matrix = createRotationMatrix(publix.params);
        rotation_matrix.multiply(model.matrix);
        model.matrix = rotation_matrix;
        model.rotation.setFromRotationMatrix(model.matrix);

        if (ground_plane_shown) {
            show_ground_plane();
        }

        render();
    }


    function createRotationMatrix(orientation) {
        // creates a matrix to rotate the model around the world axis (not around his own axis!)
        var rotation_matrix = new THREE.Matrix4();
        [
            [[1, 0, 0], orientation[0] * Math.PI / 180],
            [[0, 0, 1], -orientation[1] * Math.PI / 180],
            [[0, 1, 0], orientation[2] * Math.PI / 180]
        ].forEach(function (axis) {
            var axis_rotation = new THREE.Matrix4();
            axis_rotation.makeRotationAxis(new THREE.Vector3().fromArray(axis[0]), axis[1]);
            rotation_matrix.premultiply(axis_rotation);
        });
        return rotation_matrix;
    }

    // a ground plane with a grid is added to the viewer to better visualize the orientation
    function show_ground_plane() {
        if (ground_plane_shown) {
            hide_ground_plane()
        }
        var bbox = new THREE.Box3().setFromObject(Button3d.viewer.webgl.getCurrentMesh());
        var min = bbox.min;
        var max = bbox.max;
        if (!ground_size) {
            ground_size = (
                Math.ceil(
                    Math.sqrt(
                        Math.pow(max.x - min.x, 2) + Math.pow(max.y - min.y, 2) + Math.pow(max.z - min.z, 2)
                    ) / 10
                ) + 1) * 10;
        }

        var ground_geo = new THREE.BoxGeometry(ground_size, ground_size, 1);
        var ground_material = new THREE.MeshLambertMaterial({color: 0xffffff, transparent: true, opacity: 0.3});

        var ground_plane = new THREE.Mesh(ground_geo, ground_material);
        ground_plane.name = "ground_plane";
        ground_plane.rotation.x = -Math.PI / 2;
        ground_plane.position.y = min.y;

        var grid = new THREE.GridHelper(ground_size / 2, 10);
        grid.rotation.x = Math.PI / 2;
        ground_plane.add(grid);

        Button3d.viewer.webgl.getScene().add(ground_plane);
        ground_plane_shown = true;
        render();
    }

    function hide_ground_plane() {
        var ground_plane = Button3d.viewer.webgl.getScene().getObjectByName("ground_plane");
        if (ground_plane) {
            Button3d.viewer.webgl.getScene().remove(ground_plane);
            ground_plane_shown = false;
            render();
        }
    }

    $("body").on("reloadFiles", function () {
        init();
    });

    publix.init = init;
    publix.viewer_rotate = viewer_rotate;
    publix.rotate = rotate;
    publix.ground_plane = {
        show: show_ground_plane,
        hide: hide_ground_plane
    };

    return publix

}();
