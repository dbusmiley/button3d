Button3d.viewer.supportstructure = (function(){

    var params = {
        angle: 45,
        offset: 0
    };
    var shown = false;
    var initialized = false;
    var blocks = [];
    var base_ground_area = undefined;
    var support_mesh = undefined;
    var viewer_support_mesh = undefined;
    var scene;

    function show_support(side) {
        if(shown) {
            scene.remove(viewer_support_mesh);
        }

        function add_to_scene() {
            scene.add(support_mesh);
            viewer_support_mesh = support_mesh;
            shown = true;
            Button3d.viewer.webgl.render();
        }

        if(initialized && !side) {
            add_to_scene()
        } else {
            init_support(add_to_scene, side)
        }
    }

    function hide_support() {
        if (shown) {
            scene.remove(viewer_support_mesh);
            shown = false;
            Button3d.viewer.webgl.render();
        }
    }

    function update_support() {
        // Find first block that needs support
        for (var i = 0; i < blocks.length; i++) {
            if (blocks[i].degree > params.angle)
                break;
        }
        var mesh, geometry;
        var ground_area = base_ground_area;
        var volume = 0;
        for (; i < blocks.length; i++) {
            if (geometry) {
                geometry.merge(blocks[i].geometry);
            } else {
                geometry = blocks[i].geometry;
            }
            volume += blocks[i].volume;
            // if block is supported by the ground, add area of its projection to the ground area (needed for the raft)
            if (blocks[i].ground_supported) {
                ground_area += blocks[i].projection_area;
            }
        }

        if (geometry) {
            mesh = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({
                color: 0x000000,
                transparent: true,
                opacity: 0.5,
                polygonOffset: true,
                polygonOffsetFactor: 0.1,
                polygonOffsetUnits: 1
            }));
        } else {
            mesh = new THREE.Mesh();
        }
        mesh.name = "support";
        support_mesh = mesh;

        // The volume of the raft is added to the volume of the support blocks
        volume += ground_area * params.offset;
        if (volume) {
            console.log("Volume of support-structure: "+volume)
        } else {
            console.log("No support-structure needed");
        }

    }

    function init_support(callback, side) {
        var url = '/u/' + document.getElementById('uuid').value + "/";
        scene = Button3d.viewer.webgl.getScene();
        new THREE.CTMLoader().load(url + "download_viewer_optimized", function(ctm_geometry) {
            $.getJSON(url + "download_support", function(json) {
                side = side || "h-";
                // If json stores support values for multiple orientations, use values of the default orientation (or the one specifined by side)
                if("orientations" in json) {
                    json = json["orientations"][side]
                }
                blocks = [];
                base_ground_area = json.base_ground_area;
                var centeringMatrix = Button3d.viewer.webgl.computeCenteringMatrix(ctm_geometry);
                var len = json.num_blocks;
                var BLOCKMESH_FACES = [[3, 4, 5], [2, 1, 0], [0, 3, 2], [2, 3, 5], [1, 5, 4], [2, 5, 1], [0, 4, 3], [1, 4, 0]];

                var model_vertices = ctm_geometry.getAttribute("position").array;
                var model_faces = ctm_geometry.index.array;

                for(var i = 0; i < len; i++) {
                    var block = {
                        face_id: json.blocks[i][0],
                        degree: json.blocks[i][1],
                        proj_z: json.blocks[i][2],
                        volume: json.blocks[i][3],
                        projection_area: json.blocks[i][5],
                        ground_supported: json.blocks[i][6]
                    };
                    var top_face = [model_faces[block.face_id*3], model_faces[block.face_id*3+1], model_faces[block.face_id*3+2]];
                    var top_face_verts = [];
                    for(var j = 0; j < top_face.length; j++) {
                        var ind = top_face[j];
                        top_face_verts.push([model_vertices[ind*3], model_vertices[ind*3+1], model_vertices[ind*3+2]])
                    }

                    var proj_verts = [];
                    for(j = 0; j < top_face_verts.length; j++) {
                        var v = top_face_verts[j];
                        // depending on the side which the support is projected to, proj_z is actually proj_y or proj_x
                        if(side[0] === "h") {
                            proj_verts.push([v[0], v[1], block.proj_z])
                        } else if(side[0] === "d") {
                            proj_verts.push([v[0], block.proj_z, v[2]])
                        } else if(side[0] === "w") {
                            proj_verts.push([block.proj_z, v[1], v[2]])
                        }
                    }

                    var all_verts = top_face_verts.concat(proj_verts);
                    var block_verts = [];
                    for(j = 0; j < all_verts.length; j++) {
                        v = all_verts[j];
                        block_verts.push(new THREE.Vector3(v[0], v[1], v[2]))
                    }

                    var block_faces = [];
                    for(j = 0; j < BLOCKMESH_FACES.length; j++) {
                        f = BLOCKMESH_FACES[j];
                        block_faces.push(new THREE.Face3(f[0], f[1], f[2]))
                    }

                    var geometry = new THREE.Geometry();
                    geometry.faces = block_faces;
                    geometry.vertices = block_verts;
                    geometry.applyMatrix(centeringMatrix);
                    geometry.computeFaceNormals();
                    blocks.push({
                        geometry: geometry,
                        degree: block.degree,
                        volume: block.volume,
                        projection_area: block.projection_area,
                        ground_supported: !!block.ground_supported});
                }

                update_support();
                initialized = true;
                callback && callback();
            });
        });
    }

    var publix = {params: params};
    publix.show = show_support;
    publix.hide = hide_support;
    publix.update = update_support;

    function init() {}
    publix.init = init;


    return publix;
}());
