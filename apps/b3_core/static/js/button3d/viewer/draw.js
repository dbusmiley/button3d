/*  Painting By Face
 *  current state:
 *   - needs completion/changes and testing as a separated file (works internally in webgls)
 *   - brush for multiple smooth selection to be added
 *   - to work in current state requires bufferGeometry of the mesh replacement with Geometry object
 *  Created by Krzysztof on 28.10.2016
 */

Button3d.viewer.draw = (function(){

	var publix = {};

    publix.enable = enable;
    publix.disable = disable;

    publix.init = init;

	var mouse3D; // cursor cordinates

	var raycaster; // checks intersection

	function init() {}



	function enable() {

		/* Converts geometry of an model to Geometry object instead of bufferGeometry,
		*  to allow identification and painting on the faces the model, also event controlers
		*  in which operates all of the code responsible for painting operations, at this point.
		*/

		raycaster = new THREE.Raycaster();

	    mouse3D = new THREE.Vector2();


		if(mesh.geometry.faces == null){
		convGeom(true); // converts bufferGeometry to Geometry format
	}

		container.addEventListener( 'mousedown', onViewerMouseDown, false );

		webgl_fullscreen();

		console.log("draw_functionality_enabled");

		return true;
	}

	function convGeom(n){

		/* Temporary function that might be replaced by alterations in webgl.js code
		*  for geometry handling.
		*/


        var c_mesh, geometry, wireframe;

        for (var i = 0; i < scene.children.length; i++) {
                if (scene.children[i].type == "Mesh" && !(scene.children[i].name === "support")) {
                    c_mesh = scene.children[i];
                }
                if (scene.children[i].type == "Line") {
                    wireframe = scene.children[i];
                }
            
            }

        scene.remove(c_mesh);
        scene.remove(wireframe);
        var geometry = c_mesh.geometry;

        var material;

        if (n = true) { geometry = new THREE.Geometry().fromBufferGeometry( geometry );


        	material = new THREE.MeshPhongMaterial({
            vertexColors: THREE.FaceColors,
            side: THREE.DoubleSide,
            wireframe: false,
            reflectivity: 0,
            color: 0xd4d4d4,
            specular: 0x333333,
            shininess: 20,
            emissive: 0x000000
            });

        geometry.computeFaceNormals();
        geometry.computeVertexNormals();

        geometry.colorsNeedUpdate = true;


        } // Doesnt seem to work right for now, some variables might happen to make model not load
        else { geometry = new THREE.BufferGeometry().fromGeometry( geometry );

        	material = new THREE.MeshPhongMaterial({
                    wireframe: false,
                    reflectivity: 0,
                    color: 0xd4d4d4,
                    specular: 0x333333,
                    shininess: 20,
                    emissive: 0x000000,
                    shading: THREE.FlatShading,
                    vertexColors: THREE.VertexColors
                });

        	geometry.computeVertexNormals();

        }


        var n_mesh = new THREE.Mesh(geometry, material);
        mesh = n_mesh;
        scene.add(mesh);

        wireframe = new THREE.WireframeHelper( mesh, 0xFFFFFF );
        wireframe.material.transparent = true;
        scene.add(wireframe);
        adjustWireframe();

        requestAnimationFrame(render);
    }


	function disable(){

		// convGeom(false); // should converse geometry back to buffer type, but doesnt load the model for now
		container.removeEventListener( 'mousedown', onViewerMouseDown );

		webgl_close_fullscreen();
		console.log("draw_functionality_disabled");
	}
	

	function onViewerMouseDown(event) {

		event.preventDefault();


	    mouse3D.x = ( (event.clientX) / width) * 2 - 1;
	    mouse3D.y = - ( (event.clientY) / height) * 2 + 1;

	    raycaster.setFromCamera( mouse3D, camera );
	    

		// create an array containing all objects in the scene with which the ray intersects
		var intersects = raycaster.intersectObject( mesh );

		// if there is one (or more) intersections
		if ( intersects.length > 0 )
		{

		    controls.enabled = false;  // stops camera rotation

			// change the color of the closest face.
			intersects[ 0 ].face.color.setRGB( 100, 149, 237 ); 
	        intersects[ 0 ].object.geometry.colorsNeedUpdate = true;

	        container.addEventListener( 'mouseup', onViewerMouseUp, false );
	        container.addEventListener( 'mousemove', onViewerMouseMove, false );
	    }
	}


	function onViewerMouseMove(event){

	    event.preventDefault();


	    mouse3D.x = ( (event.clientX) / width) * 2 - 1;
	    mouse3D.y = - ( (event.clientY) / height) * 2 + 1;

	    raycaster.setFromCamera( mouse3D, camera );
	    

	    // catch intersection with single specified object
	    var intersects = raycaster.intersectObject( mesh );

	    // if there is one (or more) intersections
	    if ( intersects.length > 0 )
	    {

	        // change the color of the closest face.
	        intersects[ 0 ].face.color.setRGB( 100, 149, 237 ); 
	        intersects[ 0 ].object.geometry.colorsNeedUpdate = true;

	        requestAnimationFrame(renderer);
	    }
	}


	function onViewerMouseUp(event){

	    event.preventDefault();


	    controls.enabled = true;

	    container.removeEventListener( 'mousemove', onViewerMouseMove );
	    container.removeEventListener( 'mouseup', onViewerMouseUp );
	}


    return publix;

}());