Button3d.checkout = {
    init: function () {

        if (typeof oscar != 'undefined') {
            oscar.basket.init({basketURL: '../../basket/'});
        }

        $('form').submit(function () {
            $(this).find('.btn').text(gettext('Loading...')).attr("disabled", "disabled");
        });

        /**********************************************************
         *                   Checkout Page
         ***********************************************************/
        var sameAsShippingInputs = $("input[name='same_as_shipping']");
        if (sameAsShippingInputs[0] != null) {
            $('#b3-payment-form').removeClass('b3-hidden');
        }
    }
};