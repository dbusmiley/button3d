// Global Scope

var clickedUploadButton;

window.onunload = function () {
};

// UTIL: Some unimportant loading stuff
// See http://www.paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
UTIL = {
    fire: function (func, funcname, args) {

        var namespace = Button3d; // indicate your obj literal namespace here

        funcname = (funcname === undefined) ? 'init' : funcname;
        if (func !== '' && namespace[func] && typeof namespace[func][funcname] == 'function') {
            namespace[func][funcname](args);
        }
    },

    loadEvents: function () {
        var bodyId = document.body.id;

        // hit up common first.
        UTIL.fire('common');
        UTIL.fire('modals');

        // do all the classes too.
        $.each(document.body.className.split(/\s+/), function (i, classnm) {
            UTIL.fire(classnm);
            UTIL.fire(classnm, bodyId);
        });

        UTIL.fire('common', 'finalize');
    }
};

(function ($) {
    $(function () {
        // The DOM is ready!
        UTIL.loadEvents();
    });
})(jQuery);


// -------------------------------------------------- Begin button3d scope ------------------------------------------------
Button3d = {
    common: {
        init: function () {
            this.ajax_setup();
            if (Button3d.common.scale) {
                Button3d.common.scale.init();
            }
            if (Button3d.common.repair) {
                Button3d.common.repair.init();
            }
        },

        contact_me: function() {
            var formData = new FormData($('#contact_me_form')[0]);
            $.ajax({
                url: $('#contact_me_form').attr('action'),
                type: 'POST',
                success: function (response) {
                    if(response.success == "true") {
                        $('#contact_result').css("color", "#555").show().html("Thank you! We will contact you as soon as possible");
                        $('#btn_contact').attr("disabled", true);

                    } else {
                        $('#contact_result').show().html("Please give a valid email address").css("color", "#A7082A");
                        $('#btn_contact').attr("disabled", false);
                    }
                },
                error: function () {
                    $('#contact_result').css("color", "#555").show().html("There was a problem, please check your internet connection and try again");
                    $('#btn_contact').attr("disabled", false);
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
            $('#btn_contact').css("bottom", "-17%", "important");
        },

        // The next 4 functions:
        // Setup Ajax to submit the csrfToken with all POST requests.
        // This need to be done, so django is happy.

        csrfSafeMethod: function (method) {
            // these HTTP methods do not require CSRF protection
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        },

        sameOrigin: function (url) {
            // test that a given url is a same-origin URL
            // url could be relative or scheme relative or absolute
            var host = document.location.host; // host + port
            var protocol = document.location.protocol;
            var sr_origin = '//' + host;
            var origin = protocol + sr_origin;
            // Allow absolute or scheme relative URLs to same origin
            return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                    // or any other URL that isn't scheme relative or absolute i.e relative.
                !(/^(\/\/|http:|https:).*/.test(url));
        },

        getCookie: function (name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        },

        ajax_setup: function () {
            $.ajaxSetup({
                timeout: 30000,
                beforeSend: function (xhr, settings) {
                    if (!Button3d.common.csrfSafeMethod(settings.type) && Button3d.common.sameOrigin(settings.url)) {
                        // Send the token to same-origin, relative URLs only.
                        // Send the token only if the method warrants CSRF protection
                        // Using the CSRFToken value acquired earlier
                        xhr.setRequestHeader("X-CSRFToken", Button3d.common.getCookie('csrftoken'));
                    }
                }
            });
        },

        send_event_to_ga: function(object, action) {
            // Send event to Google analytics
            if (typeof ga != 'undefined') {
                ga('send', 'event', object, action);
            }
        }
    },

    modals: {
        init: function () {
            $('.b3-modal').each(function (idx, elem) {
                var modalVM = ko.dataFor(elem);
                if (modalVM && modalVM.publicMembers) {
                    for (var modalAction in modalVM.publicMembers)
                        Button3d.modals[modalAction] = modalVM.publicMembers[modalAction];
                }
            });
        }
    },
    constants: {
        INCH: 25.4
    }
};

// From https://addyosmani.com/blog/essential-js-namespacing/ and adapted
// a convenience function for parsing string namespaces and
// automatically generating nested namespaces
function extend( ns, ns_string ) {
    var parts = ns_string.split('.'),
        parent = ns,
        pl, i;

    if (parts[0] == "Button3d") {
        parts = parts.slice(1);
    }

    pl = parts.length;
    for (i = 0; i < pl; i++) {
        //create a property if it doesnt exist
        if (typeof parent[parts[i]] == 'undefined') {
            parent[parts[i]] = {};
        }

        parent = parent[parts[i]];
    }

    return parent;
}
