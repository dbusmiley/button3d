var Button3d = Button3d || {};

var compare_api = extend(Button3d, 'compare.api');

$.extend(compare_api, (function() {
    var publix = {};

    publix.get_stl_file = function(uuid, success_callback, error_callback) {
        return $.ajax({
            url: '/' + lang + '/u/' + uuid + '/',
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.get_upload_info = function(uuid, success_callback, error_callback) {
        return $.ajax({
            url: '/v1/uploads/' + uuid + '/?lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.get_material_offers = function(uuid, scale, currency, success_callback, error_callback) {
        return $.ajax({
            url: '/v1/material-prices/?uuid=' + uuid + '&country=' + country_user + '&scale=' + scale + '&currency=' + currency + '&lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: function(data){
                for(var i = 0; i < data.results.length; i++)
                    data.results[i]['printability_at_scale'] = scale;
                success_callback(data)
            },
            error: error_callback
        });
    };

    publix.get_supplier_offers = function(uuid, scale, currency, material_slug, success_callback, error_callback) {
        return $.ajax({
            url: '/v1/supplier-prices/?uuid=' + uuid + '&country=' + country_user + '&material=' + material_slug + '&scale=' + scale + '&currency=' + currency + '&lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: function(data){
                for(var i = 0; i < data.results.length; i++)
                    data.results[i]['printability_at_scale'] = scale;
                success_callback(data)
            },
            error: error_callback
        });
    };

    publix.get_finishing_offers = function(uuid, scale, currency, material_slug, partner_slug, success_callback, error_callback) {
        return $.ajax({
            url: '/v1/finishes-prices/?uuid=' + uuid + '&country=' + country_user + '&material=' + material_slug + '&scale=' + scale + '&currency=' + currency + '&supplier=' + partner_slug + '&lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: function(data){
                if(data.results){
                    for(var i = 0; i < data.results.length; i++)
                        data.results[i]['printability_at_scale'] = scale;
                }
                success_callback(data, material_slug)
            },
            error: error_callback
        });
    };

    publix.get_suppliers = function(success_callback, error_callback) {
        return $.ajax({
            url: '/v1/suppliers/?lang=' + lang + '&country=' + country_user + '&domain=' + location.hostname,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.get_colors = function(success_callback, error_callback) {
        return $.ajax({
            url: '/v1/colors/?lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.get_materials = function(success_callback, error_callback) {
        return $.ajax({
            url: '/v1/materials/?lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.get_finishes = function(success_callback, error_callback) {
        return $.ajax({
            url: '/v1/finishes/?lang=' + lang + '&domain=' + location.hostname,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    publix.test_price = function(
        uuid,
        stockrecord_id,
        scale,
        price_formula,
        support_enabled,
        support_formula,
        support_angle,
        support_offset,
        orientation_formula,
        success_callback,
        error_callback
    ) {

        var query_args = '?uuid=' + uuid +
            '&stockrecord_id=' + stockrecord_id +
            '&scale=' + scale +
            '&price_formula=' + price_formula +
            '&support_enabled=' + (support_enabled ? "true" : "");

        if (support_enabled) {
            query_args += '&support_formula=' + support_formula +
                    '&support_angle=' + support_angle +
                    '&support_offset=' + support_offset +
                    '&orientation_formula=' + orientation_formula;
        }

        return $.ajax({
            url: '/v1/custom-price/' + query_args,
            type: 'GET',
            success: success_callback,
            error: error_callback
        });
    };

    return publix;
}()));
