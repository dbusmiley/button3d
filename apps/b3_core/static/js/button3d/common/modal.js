/* global slugify, uploadUtility */
Button3d.common.modal = (function createModal() {
    var modal = null;
    var totalProgressBar = null;
    var fileValidatorContainer = null;
    var uploadingContainer = null;
    var body = null;
    var totalProgressRow = null;

    function initialize(selector, fileValidators) {
        modal = $(selector);
        totalProgressBar = $('.total-progress');
        fileValidatorContainer = $('.file-validators');
        uploadingContainer = $('.modal-body');
        body = $('body');
        totalProgressRow = $('.total-progress-row');

        modal.on('hidden.bs.modal', function onModalHide() {
            $(publix).trigger('dropAllFiles');
            Button3d.common.modal.setTotalProgressBar(0);
            $('.file-progress-row').each(function onEachFileProgressRow() {
                $(this).remove();
            });
            $('.modal-body').show();
            $('.modal-error').text('');
        });

        modal.on('show.bs.modal', function onModalShow() {
            Button3d.common.modal.setTotalProgressBar(0);
        });

        body.on('click', '.file-validator-checkbox', validateFileCheckboxes);
        body.on('click', '.btn-upload', function onUploadClick() {
            if ($(this).hasClass('btn-fake-disabled')) {
                return;
            }
            $(publix).trigger('startUpload');
        });

        body.on('click', '.stop-upload', function () {
            $(publix).trigger('stopUploads');
        });

        fileValidators.forEach(function onFileValidator(fileValidator) {
            fileValidatorContainer.append('<label class="b3-control-label b3-big-checkbox"><input type="checkbox" class="file-validator-checkbox"><span>' + fileValidator + '</span></label>');
        });
    }

    function setTotalProgressBar(percent) {
        if (!totalProgressBar || totalProgressBar === null) return;
        totalProgressBar.text(percent + '%');
        totalProgressBar.width(percent + '%');
    }

    function checkIfAnyCheckboxIsSelected() {
        var uploadEnabled = true;
        $('.file-validator-checkbox').each(function onFileValidatorCheckbox() {
            if ($(this).is(':checked')) {
                uploadEnabled = false;
            }
        });
        return uploadEnabled;
    }

    /**
     * Method to check if all checkboxes are unchecked. Otherwise it will disable the upload button.
     * @return {object} null.
     */
    function validateFileCheckboxes() {
        var uploadEnabled = checkIfAnyCheckboxIsSelected();
        var uploadButton = $('.btn-upload');
        var fileValidationRequest = $('.file-validation-request');
        uploadEnabled
            ? fileValidationRequest.addClass('hidden')
            : fileValidationRequest.removeClass('hidden');
        uploadEnabled
            ? uploadButton.removeClass('btn-fake-disabled')
            : uploadButton.addClass('btn-fake-disabled');
        uploadButton.css('cursor', uploadEnabled ? 'pointer' : 'not-allowed');
    }

    function addInvalidFile(filename) {
        uploadingContainer.append(uploadUtility.createFileProgressBar({
            filename: filename,
            type: 'warning',
            progress: 100
        }));
    }

    function addFileDownload(filename) {
        uploadingContainer.append(uploadUtility.createFileProgressBar({
            filename: filename,
            type: 'success',
            progress: 0
        }));
    }

    function show() {
        modal.modal('show');
    }

    function hide() {
        modal.modal('hide');
    }

    function changeFileProgress(progress, filename) {
        var percentageText = progress + '%';
        $('.progress-' + uploadUtility.slugify(filename)).width(percentageText);
    }

    function showError(errorMessage) {
        $('.modal-body').hide();
        $('.modal-error').text(errorMessage);
    }

    function showTotalProgress() {
        totalProgressRow.show();
    }

    function hideTotalProgress() {
        totalProgressRow.hide();
    }

    function disableModal() {
        $('#cancel-button').remove();
        $('.modal-header button').remove();
        $('div.modal-footer > div > div:nth-child(2)').append('<span>Redirecting...</span>');
        $('.btn-validated-upload').addClass('btn-fake-disabled');
        var loadingMessage = gettext('Redirecting to your 3D Project.');
        $('.modal-body').html(uploadUtility.createLoadingContainer({
            loadingMessage: loadingMessage
        }));
        $('.modal-footer').html('');
    }

    var publix = {
        initialize: initialize,
        setTotalProgressBar: setTotalProgressBar,
        addFileDownload: addFileDownload,
        addInvalidFile: addInvalidFile,
        show: show,
        hide: hide,
        changeFileProgress: changeFileProgress,
        showError: showError,
        showTotalProgress: showTotalProgress,
        hideTotalProgress: hideTotalProgress,
        disableModal: disableModal
    };
    return publix;
})();
