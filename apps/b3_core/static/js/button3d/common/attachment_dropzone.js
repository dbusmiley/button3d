/* Needs:
- attachment-dropzone, an element for handling the dropzone with this id
- max_file_size_in_mb, a global variable
- attachment_url, a global variable ({% url 'manual-request-attachments' %})
- attachment-upload-progress-container & attachment-upload-progress, elements with these ids to display progress of attachment upload
- attachments_context_vm, a global variable being an instance of AttachmentsVM
- the form in the html, returned from manual request view
*/

$(function() {
    if (window.myDropzone) {
        myDropzone.drop = function () {};
    }
    var attachments_context_vm = new AttachmentsVM();
    ko.applyBindings(attachments_context_vm, $('.attachments-form')[0]);

    $(function() {
        $('#id_type').select2({
            minimumResultsForSearch: Infinity
        });
    });


    var attachmentDropzone = new Dropzone($('#attachment-dropzone')[0], {
        url: attachment_url,
        autoProcessQueue: true,
        maxFilesize: max_file_size_in_mb,
        clickable: true,
        multipleUpload: true,
        parallelUploads: 10,
        previewTemplate: '<div style="display: none;"></div>',
        params: {
            'csrfmiddlewaretoken': getCookie('csrftoken')
        }
    });

    var progressElementContainer = $('#attachment-upload-progress-container');
    var progressElement = $('#attachment-upload-progress');

    attachmentDropzone.on('success', function(file, data) {
        if (data.success) {
            var attachments = data.attachments;
            var attachmentsLen = attachments.length;
            var current_attachment_context = attachments_context_vm.attachments;

            for (var i = 0; i < attachmentsLen; ++i) {
                attachments[i].name = truncate_middle(attachments[i].name, 65);
                current_attachment_context.push(attachments[i]);
                updateSvgStyle();
            }
        } else {
            // TODO show error modal
        }
    });

    attachmentDropzone.on('totaluploadprogress', function(uploadProgress, totalBytes, totalBytesSent) {
        progressElementContainer.show();
        progressElement.attr('value', uploadProgress);
        if (uploadProgress === 100) {
            progressElementContainer.hide();
        }
    });

    attachmentDropzone.on('uploadprogress', function(file, progress, bytesSent){
        progressElementContainer.show();
        $(file.previewElement.querySelector('#attachment-upload-progress')).attr('value', parseInt(progress));
        if (progress === 100 ){
            progressElementContainer.hide();
        }
    });

    attachmentDropzone.on('error', function(stuff, errorMessage, xhrObject) {
        attachments_context_vm.error(errorMessage);
    });

    /* Needs:

    */

    // Populate with existing attachment (if from a form error)
    var existing_attachments = eval($('input[name="attachment_ids"]')[0].value);
    if (existing_attachments !== undefined) {
        for (var i = 0; i < existing_attachments.length; ++i) {
            $.ajax({
                url: attachment_url + '?id=' + existing_attachments[i],
                type: 'GET',
                success: function(data) {
                    if (data.success) {
                        data.attachment.name = truncate_middle(data.attachment.name, 65);
                        attachments_context_vm.attachments.push(data.attachment);
                        updateSvgStyle();
                    } else {
                        console.log("Could not retrieve attachment");
                        // TODO handle error, could not retrive
                    }
                }
            });
        }
    }


});
