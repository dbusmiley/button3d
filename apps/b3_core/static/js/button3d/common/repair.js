/*
This file is responsible for the analyzing process.

- It goes through a series of states: READY -> ANALYZING_PARAMETER_UNAVAILABLE -> ANALYZING_PARAMETER_AVAILABLE -> FINISHED
- It uses webgl.js to display the right file

- It triggers the following events:
  - parameterAvailable (to init scale slider, get prices, ... )
  - analysingFinished

- It listens to the following events:
  - uploadFinished

- Before triggering this event:
  - the UUID of the file to process must be stored in $('#uuid').val()
  - the current domain must be stored in $('#domain').val()

- It holds an instance of StlFileVM.
  Use Button.common.repair.getStlFile() to get the data you need

 */

function RepairVM(){
    var publix = this;

    var states = {
        READY: 0,  // nothing uploaded yet
        ANALYZING_PARAMETER_UNAVAILABLE: 1, // progress bar is shown, no paramenters are available yet
        ANALYZING_PARAMETER_AVAILABLE: 2, // progress bar is still there, prices can be shown (inaccurate since not repaired yet)
        FINISHED: 3, // progress bar is finished, prices will update (accurate since repaired)
        ERROR: 4 // something was wrong -> Fail UN-gracefully with an error modal.
    };
    publix.states = states;

    // initial state
    publix.state = ko.observable(states.READY);

    publix.is_parameter_available = ko.computed(function(){
        return publix.state() == states.FINISHED || publix.state() == states.ANALYZING_PARAMETER_AVAILABLE;
    });

    publix.is_finished = ko.computed(function(){
        return publix.state() == states.FINISHED;
    });

    var body_element = $('body');

    publix.getState = function() { return publix.state(); };

    var stlFile;
    var uuid;

    publix.getStlFile = function() { return stlFile; };
    /*
     * Does the convert and the repair job when the file was initialized
     */
    publix.init = function() { };

    function startAnalysing(uuid_param) {
        uuid = uuid_param;
        publix.state(states.ANALYZING_PARAMETER_UNAVAILABLE);
        pollStatus();
    }

    body_element.on("uploadFinished", function() {
        startAnalysing($('#uuid').val());
    });

    function onAnalysedFinished() {
        body_element.trigger('analysingFinished');
        finishProgressBar(true);
        enableElements();
    }


    /*
     * This method is called frequently, and waits for the file to be analyzed.
     */
    function pollStatus() {
        compare_api.get_upload_info($('#uuid').val(), // TODO global var
            function (data) {
                if (stlFile === undefined && data.parameter) {
                    // If this is the first time that some data for the file is available
                    publix.state(states.ANALYZING_PARAMETER_AVAILABLE);
                    stlFile = new StlFileVM(data);
                    if (data.status != 'finished') {
                        // Only load the unrepaired file.
                        // If analysis is finished, we can show the repaired file (see below)
                        replaceSTLFile();
                    }

                    body_element.trigger("parameterAvailable"); // init scale slider, get prices, ...
                }

                var progressBar = $('#repair_wta_progress')[0].children[0];
                if (data.status == 'analysing') {
                    if (publix.state() == states.READY)
                        publix.state(states.ANALYZING_PARAMETER_UNAVAILABLE);

                    // Update Text in ProgressBar according to analysing_detail status
                    if (data.analysing_detail === "converting") {
                        progressBar.innerText = gettext("Converting...");
                    } else if (data.analysing_detail === "optimising") {
                        progressBar.innerText = gettext("Optimizing...");
                    } else {
                        progressBar.innerText = gettext("Analyzing...");
                    }

                    setTimeout(pollStatus, 2000);
                }

                else if (data.status == 'finished') {
                    progressBar.innerText = gettext("Analyzing...");
                    publix.state(states.FINISHED);

                    stlFile = new StlFileVM(data);
                    replaceSTLFile(onAnalysedFinished);
                    Button3d.compare.three_d_tools.update3DInformations();
                }

                else if (data.status == 'error') {
                    progressBar.innerText = gettext("Error!");
                    publix.state(states.ERROR);
                    showRepairError(data.error);
                }
            },

            function (error_data) {
                if (error_data.status == 500) {
                    showRepairError("E_SERVER_ERROR");
                }
                else {
                    showRepairError("E_UNREACHABLE");
                }

            }
        );
    }

    /*
     * Replaces the underlying 3D-file for the webGL viewer.
     * Displays the right filetype according to:
     * - State
     * - Button3d.compare.three_d_tools.is_panel_active()
     * - getStlFile().multicolor
     */
    publix.replaceSTLFile = replaceSTLFile;
    function replaceSTLFile(callback) {
        function process_filename(filename) {
            $('#webgl-loading').show();
            if (!Button3d.viewer.webgl.load(filename, callback)) {
                callback && callback(false);
            }
        }

        switch(publix.state()) {
            case states.ANALYZING_PARAMETER_UNAVAILABLE:
                break;
            case states.ANALYZING_PARAMETER_AVAILABLE:
                process_filename("viewer_original");
                break;
            case states.FINISHED:
                if (Button3d.compare.three_d_tools.is_panel_active()) {
                    if (Button3d.compare.three_d_tools.get_active_panel() == 'options') {
                        if(Button3d.viewer.wta.isJsonPresent) {
                            Button3d.viewer.wta.show();
                            callback && callback(true);
                        } else {
                            process_filename("viewer_optimized");
                        }
                    } else {
                        process_filename("viewer_optimized");
                    }
                } else {
                    if (publix.getStlFile().multicolor) {
                        process_filename("viewer_original");
                    } else {
                        process_filename("viewer_optimized");
                    }
                }

                break;
            case states.READY:
            case states.ERROR:
                return;
        }
    }

    /*
     * Reload the file.
     */
    publix.updateFile = updateFile;
    function updateFile(force) {
        if (publix.state() == states.FINISHED) {
            if (Button3d.compare.three_d_tools.is_panel_active()) {
                Button3d.viewer.wta.init(function() {
                    Button3d.viewer.wta.update();
                    Button3d.viewer.wta.show();
                });

            }
            setTimeout(function(){
                maybeToggleWtaInfo();
            },1500);
        }
    }

    // updates the material informations
    publix.updateMaterialInfo = updateMaterialInfo;
    function updateMaterialInfo() {
        var elMinThick = $('#wta-info-min');
        var elOptThick = $('#wta-info-opt');
        var elName = $('#printability-material');
        var cur_material = Button3d.compare.materials.Steps.printable_selected();

        var sMinThick = cur_material.attr_wall_min().toString().replace(".", get_format('DECIMAL_SEPARATOR'));
        elMinThick.text('< ' + sMinThick + ' mm');

        var sOptThick = cur_material.attr_wall_opt().toString().replace(".", get_format('DECIMAL_SEPARATOR'));
        elOptThick.text('> ' + sOptThick + ' mm');

        elName.text(cur_material.title);
    }

    // shows the wta information related to the printability status
    function maybeToggleWtaInfo() {

        $('#printability-panel-loading').show();

        var elPrintable = $('#wta-info-printable');
        var elSuspicious = $('#wta-info-suspicious');
        var elUnprintable = $('#wta-info-unprintable');
        var elUnprintableUnknown = $('#wta-info-unprintable-unknown');
        var alertPanel = $('#wta-info-alert-printability');
        var printablePanel = $('#wta-info-text-printable');
        var legendPanel = $('.b3-3d-tools-legend-in-panel-tools');
        var elVisible;

        var printability = Button3d.compare.materials.Steps.printable_selected().printability();
        var printability_problem = Button3d.compare.materials.Steps.printable_selected().printability_problem();

        alertPanel.removeClass('alert-danger');
        alertPanel.removeClass('alert-warning');
        alertPanel.removeClass('alert-black');
        legendPanel.addClass('with-alert');

        $('.printability-erroneous-message').css('margin-top','');

        if (printability == "printable") {
            elVisible = elPrintable;
            alertPanel.hide();
            printablePanel.show();
            legendPanel.removeClass('with-alert');
        } else if (printability == "warning"){
            alertPanel.addClass('alert-warning');
            legendPanel.addClass('with-alert');
            alertPanel.show();
            printablePanel.hide();
            elVisible = elSuspicious;
            $('#wta-info-suspicious-text').show();
            $('#wta-info-unprintable-text').hide();
            $('#wta-info-unprintable-shells-text').hide();
            $('#wta-info-unprintable-unknown-text').hide();
            $('.printability-erroneous-message').css('margin-top','7px');
        } else if (printability == "not_printable") {
            elVisible = elUnprintable;
            alertPanel.addClass('alert-danger');
            legendPanel.addClass('with-alert');
            alertPanel.show();
            printablePanel.hide();

            if (printability_problem == "E_THIN_WALLS") {
                $('#wta-info-suspicious-text').hide();
                $('#wta-info-unprintable-text').show();
                $('#wta-info-unprintable-shells-text').hide();
                $('#wta-info-unprintable-unknown-text').hide();
            } else if (printability_problem == "E_MULTIPLE_SHELLS") {
                $('#wta-info-suspicious-text').hide();
                $('#wta-info-unprintable-text').hide();
                $('#wta-info-unprintable-shells-text').show();
                $('#wta-info-unprintable-unknown-text').hide();
            }
        } else if (printability == "unknown") {
            elVisible = elUnprintableUnknown;
            if (printability_problem == "E_TOO_MANY_FACES") {
                alertPanel.show();
                alertPanel.addClass('alert-black');

                $('#wta-info-suspicious-text').hide();
                $('#wta-info-unprintable-text').hide();
                $('#wta-info-unprintable-shells-text').hide();
                $('#wta-info-unprintable-unknown-text').show();
            }
        }

        elPrintable.hide();
        elSuspicious.hide();
        elUnprintable.hide();
        elUnprintableUnknown.hide();

        elVisible && $('#printability-panel-loading').hide();

        elVisible && elVisible.show();
        elVisible && $('#printability-panel-info').show();

    }

    publix.show_printability = function() {
        updateMaterialInfo();
        updateFile(false);
    };


    function finishProgressBar(success) {
        if ($('#b3-progress').is(":visible")) {
            var progress_bar = $('#repair_wta_progress');
            progress_bar.css("animation-play-state", "paused");
            var progress_width = progress_bar.width() / progress_bar.parent().width() * 100 + '%';
            progress_bar.removeClass("progress-animation")
                .css("width", progress_width)
                .animate({
                    width: "100%"
                }, {
                    duration: 500,
                    easing: "easeInQuint",
                    complete: function () {
                        if (success) {
                            $('.progress-text').hide();
                            $('.progress-analyse').fadeOut();
                            setTimeout(function () {
                                $('#b3-progress').hide();
                                $('#b3-3d-tools').show();
                            }, 500);
                        } else {
                            $('.progress-text').hide();
                            $('.progress-analyse').fadeOut().css("border", "none");
                        }
                    }
            });
        }
    }


    function enableElements() {
        $('.b3-scale-panel').css('pointer-events', 'auto');
        $('input[type=range]').animate({opacity: 1}, 500);
        $('.b3-scale-input-text').animate({backgroundColor: "white"}, 500);
        $('.b3-material-panel').css('pointer-events', 'auto');
        $('.b3-tab-material-category').animate({backgroundColor: "rgb(245, 245, 245)"}, 500);
        $('.b3-btn-print').css('pointer-events', 'auto').animate({opacity: 1}, 500);
        $('.b3-button-blocker').css('display', 'none');
        $('#b3-cube-holder').animate({opacity: 1}, 500);
        $('.b3-legend-controls').animate({opacity: 0.4}, 500);
    }

    /*
     * binds the errormessage to html, when any repair error occured
     */

    publix.showRepairError = showRepairError;
    function showRepairError(error) {
        publix.state(states.ERROR);
        finishProgressBar(false);

        var pleaseUploadFileAsRequestMessage = gettext("Please upload the file as a ") + '<a href="' + manual_request_url + '">' + gettext("manual pricing request") + "</a> " + gettext('(24 hour response time)');
        var uploadFileAsRequestMessage = gettext('Upload the file as a ') + '<a href="' + manual_request_url + '">' + gettext("manual pricing request") + "</a> " + gettext('(24 hour response time)');

        var errorMessages = [];
        if (typeof error != undefined) {
            switch (error) {
                case "E_TIMEOUT":
                    errorMessages.push(gettext("The model is too complex to be processed by our servers.") + "<br><br>");
                    errorMessages.push(gettext("You can either:") + "<br>");
                    errorMessages.push('<ul class="error-modal-ul">');
                    errorMessages.push('<li>1) ' + gettext("Simplify the complexity of your model.") + "</li>");
                    errorMessages.push('<li>2) ' + uploadFileAsRequestMessage + "</li>");
                    errorMessages.push('</ul>');
                    break;
                case "E_INVALID_UUID":
                    errorMessages.push(gettext("The model can't be found in our database.") + "<br>");
                    errorMessages.push(gettext("Please upload it again."));
                    break;
                case "E_JSON_FORMAT":
                    errorMessages.push(gettext("We experienced a problem while analyzing your model.") + '<br>');
                    errorMessages.push(gettext("This may have to do with the complexity or formatting within the design.") + "<br><br>");
                    errorMessages.push(uploadFileAsRequestMessage);
                    break;
                case "E_ZIP_CONTENT":
                    errorMessages.push(gettext("The content of the zip folder is invalid.") + "<br>");
                    errorMessages.push(gettext("Please verify the files and re-upload the zip folter."));
                    break;
                case "E_INVALID_FILE":
                    errorMessages.push(gettext("We could not process the file you provided.") + "<br>");
                    errorMessages.push('<ul class="error-modal-ul">');
                    errorMessages.push('<li>1) ' + gettext("Please verify that your file is not corrupt.") + '</li>');
                    errorMessages.push('<li>2) ' + gettext("Confirm you are using one of our supported file types.") + '</li>');
                    errorMessages.push('<li>3) ' + uploadFileAsRequestMessage + '</li>');
                    errorMessages.push('</ul>');
                    break;
                case "ERROR_INVALID_ZIP_MULTIPLE_TEXTURE_FILES":
                    errorMessages.push(gettext("The zip folder contains multiple texture files.") + "<br>");
                    errorMessages.push(gettext("Please remove one texture and re-upload the zip folder."));
                    break;
                case "ERROR_INVALID_ZIP_MULTIPLE_MODEL_FILES":
                    errorMessages.push(gettext("The zip folder contains multiple model files") + "<br>");
                    errorMessages.push(gettext("Please remove one model and re-upload the zip folder."));
                    break;
                case "ERROR_INVALID_ZIP_MISSING_MODEL_FILE":
                    errorMessages.push(gettext("The zip folder doesn't include any model file.") + "<br>");
                    errorMessages.push(gettext("Please add the model and re-upload the zip folder."));
                    break;
                case "ERROR_INVALID_ZIP_MISSING_MATERIAL_FILE":
                    errorMessages.push(gettext("The zip folder doesn't have any material file.") + "<br>");
                    errorMessages.push(gettext("Please add the material file and re-upload the zip folder."));
                    break;
                case "ERROR_NO_CONVERTED_TEXTURE_FILE":
                    errorMessages.push(gettext("The zip folder doesn't have any converted texture file.") + "<br>");
                    errorMessages.push(gettext("Please add the converted texture and re-upload the zip folder."));
                    break;
                case "ERROR_INVALID_ZIP_MISSING_TEXTURE_FILE":
                    errorMessages.push(gettext("The zip folder doesn't have any texture file.") + "<br>");
                    errorMessages.push(gettext("Please add the texture and re-upload the zip folder."));
                    break;
                case "E_UNREACHABLE":
                    errorMessages.push(gettext("The server is unreachable.") + "<br>");
                    errorMessages.push(gettext("Check your internet connection."));
                    break;
                case "E_INTERNAL_ERROR":
                default:
                    errorMessages.push(gettext("We were not able to process your model.") + "<br>");
                    errorMessages.push(pleaseUploadFileAsRequestMessage);
                    break;
            }

            // manual_request_url is global
            Button3d.modals.show_error_modal(errorMessages.join(""), undefined, manual_request_url);

            // Send event to Google analytics
            if (typeof ga != 'undefined') {
                // Only if we are not on the demo  page
                if(window.location.href.indexOf("demo") == -1) {
                    ga('send', 'event', 'error', 'show', errorMessage);
                }
            }
        }
    }

    return publix;
}


// Compression Errors occur, when the last statement
// in a js file is a constructor with empty argumenets.
// So add a empty string
Button3d.common.repair = new RepairVM('');




