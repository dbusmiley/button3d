// eslint-disable-next-line
Button3d.common.beautify = (function() {
    function prettyByCurrentDate(date) {
        return prettyDate(date, Math.round(new Date().getTime() / 1000));
    }

    /**
     *
     * Method to beautify unix timestamps.
     *
     * @param {Number} time Time which should be prettified.
     * @param {Number} currentTime Time to base on. I include this here for better testability.
     * @returns {String} The prettified string of the time parameter.
     */
    function prettyDate(time, currentTime) {
        var differenceInSeconds = currentTime - time;
        if (differenceInSeconds < 10) {
            return gettext('just now');
        }
        if (differenceInSeconds < 60) {
            return differenceInSeconds + gettext(' seconds ago');
        }
        if (differenceInSeconds < 120) {
            return gettext('a minute ago');
        }
        if (differenceInSeconds < 3600) {
            return (
                parseInt(differenceInSeconds / 60, 10) + gettext(' minutes ago')
            );
        }
        if (differenceInSeconds < 7200) {
            return gettext('an hour ago');
        }
        if (differenceInSeconds < 86400) {
            return (
                parseInt(differenceInSeconds / 3600, 10) + gettext(' hours ago')
            );
        }
        if (differenceInSeconds < 172800) {
            return gettext('yesterday');
        }
        if (differenceInSeconds < 86400 * 7) {
            return (
                parseInt(differenceInSeconds / 86400, 10) + gettext(' days ago')
            );
        }
        if (differenceInSeconds < 604800 * 2) {
            return gettext('a week ago');
        }
        if (differenceInSeconds < 31 * 86400) {
            return (
                parseInt(differenceInSeconds / (7 * 86400), 10) +
                gettext(' weeks ago')
            );
        }
        if (differenceInSeconds < 2 * 31 * 86400) {
            return gettext('a month ago');
        }
        if (differenceInSeconds < 365 * 86400) {
            return (
                parseInt(differenceInSeconds / (31 * 86400), 10) +
                gettext(' months ago')
            );
        }

        if (differenceInSeconds < 2 * 365 * 86400) {
            return gettext('a year ago');
        }
        return (
            parseInt(differenceInSeconds / (365 * 86400), 10) +
            gettext(' years ago')
        );
    }
    return {
        prettyDate: prettyDate,
        prettyByCurrentDate: prettyByCurrentDate
    };
})();
