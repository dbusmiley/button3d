// eslint-disable-next-line
Button3d.common.fileExtensions = (function () {
    var fileExtensions = [
        'obj',
        'dae',
        'wrl',
        'ply',
        '3ds',
        'step',
        'iges',
        'stl',
        'zip',
        'stp',
        'igs',
        'ctm',
        'x3d',
        '3mf',
        'fbx',
        'wrl',
        'vda',
        'catpart',
        'sldprt',
        'prt',
        'skp',
        'acs',
        'jt',
        '3dm'
    ];

    function getMaximumFilesize(filename) {
        var defaultMaximumFilesize = 64;
        var extension = getExtension(filename);
        var mb128Files = [
            '3ds',
            '3mf',
            'acs',
            'ctm',
            'dae',
            'fbx',
            'obj',
            'ply',
            'wrl',
            'x3d',
            'stl'
        ];
        if (mb128Files.indexOf(extension) > 0) {
            return 128;
        }
        return defaultMaximumFilesize;
    }

    function get3dFileExtensions() {
        return fileExtensions.sort();
    }

    function getDottedFileExtensions() {
        var result = [];
        get3dFileExtensions().forEach(function (extension) {
            result.push('.' + extension);
        });
        return result;
    }

    function getReadableExtensions() {
        return getDottedFileExtensions().join(', ');
    }

    function getCommaSeperated() {
        return getDottedFileExtensions().join(',');
    }

    function getReadableExtensionsAlternative() {
        return getDottedFileExtensions().join(' ');
    }

    function is3DFile(filename) {
        var parts = filename.split('.');
        var extension = parts[parts.length - 1].toLowerCase();
        var dottedJoinedFileExtensions = fileExtensions.join('.');
        var is3DFileExtension = dottedJoinedFileExtensions.indexOf(extension) > 0;
        return is3DFileExtension;
    }

    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }

    function getErrorMessage(file) {
        if (!is3DFile(file.name)) {
            return null;
        }
        var maximumFileSize = getMaximumFilesize(file.name);
        if (file.size < maximumFileSize * 1024 * 1024) {
            return null;
        }
        var extension = getExtension(file.name).toLowerCase();
        return '.' + extension + gettext(' files can be uploaded up to ') + maximumFileSize + ' MB';
    }

    return {
        get3dFileExtensions: get3dFileExtensions,
        getReadableExtensions: getReadableExtensions,
        getCommaSeperated: getCommaSeperated,
        getReadableExtensionsAlternative: getReadableExtensionsAlternative,
        is3DFile: is3DFile,
        getExtension: getExtension,
        getMaximumFilesize: getMaximumFilesize,
        getErrorMessage: getErrorMessage
    };
})();
