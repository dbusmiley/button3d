/* Needs:
- attachment_url, a global variable = {% url 'manual-request-attachments' %}
- the form in the html, returned by the manual request url
*/

function AttachmentsVM() {
    var self = this;

    self.error = ko.observable();
    self.attachments = ko.observableArray();

    self.remove_attachment = function() {
        var attachment = this;
        $.ajax({
            url: attachment_url + '?' + $.param({"id": attachment.id}),
            type: 'DELETE',
            success: function(data) {
                if (data.success) {
                    self.attachments.remove(attachment);
                } else {
                    console.log("Error removing attachment: " + data.message);
                }
            }
        });
    };

    self.attachments.subscribe(function(newValue) {
        var str = '';
        var attachmentLen = self.attachments().length;

        for (var i = 0; i < attachmentLen; ++i) {
            str += self.attachments()[i].id;
            if (i+1 != attachmentLen) {
                str += ', ';
            }
        }
        $('input[name="attachment_ids"]')[0].value = '[' + str + ']';
    });
}

