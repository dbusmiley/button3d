Button3d.common.scale = (function(){
    /****************************************************************
     *                        Uploads, scale slider
     *****************************************************************/
    /*     *
     * We always keep the "official" scale and unit in theScale and theUnit.
     * The "official" scale is the scale with respect to mm.
     * So when inch is selected the shown scale is the official scale / INCH.
     *
     * We use sScale as variable name when the scale is the shown (wrong) one.
     *
     * - Th module triggers the following events:
     *   - scaleChange
     *   - scaleEnabled
     *
     * - It listens to the following events:
     *   - parameterAvailable
     *
     * - The module is dependant on repair.js
     */

    var publix = {};

    var INCH = 25.4;
    publix.INCH = INCH;

    var theScale = 1;
    publix.getScale = getScale;
    function getScale() { return theScale; }

    var theUnit= "mm";
    publix.getUnit = getUnit;
    function getUnit() { return theUnit; }

    var mmFieldPrecision = 1;
    var sliderOne = 0;


    publix.init = init;
    function init() {

        /* Update all other fields when slider moves. */
        $('#comparator-scale-slider').on('input change', scaleSliderChanged);

        /* Update all other fields when changing mm fields. */
        $('.scale-mm').on('keyup change', scaleMMChanged);

        /* Update all other when percentage changes */
        $('#scale-percentage').on('keyup change', scalePercentageChanged);

        /* Make unit choice clickable */
        $('.b3-btn-unit').click(scaleUnitChanged);

        function enable() {
            $(".b3-scale-panel").removeClass("disabled");
            updateData();
            updateAll();
            $('body').trigger("scaleEnabled");
        }
        $("body").on("parameterAvailable", enable);
    }

    function initSlider() {
        /* calculate default value for scale slider */
        sliderOne = Math.round(100 * Math.pow(2, -(Math.log(getMaxScale()) / Math.log(10))));
        $('#scale-slider-list option').text(sliderOne);
    }

    function scaleSliderChanged() {
        var sScale = sliderToScale($(this).val());
        setScale(sScale);
        updateMMFields();
        updatePercentage();
    }

    function scaleMMChanged() {
        var sScale = $(this).val() / $(this).data('original');
        setScale(sScale);
        updateSlider();
        updatePercentage();

        // Here we would like to call updateMMFields()
        // to update all other mm fields.
        // But we need to prevent the changed field to be updated as well.
        var thisField = $(this);
        $('.scale-mm').each(function (index) {
            /* Do not update the mm-field that was changed by the user. */
            if ($(this).attr('id') !== thisField.attr('id')) {
                var newMM = $(this).data('original') * sScale;
                $(this).val(newMM.toFixed(1));
            }
        });

    }

    function scalePercentageChanged() {
        var sScale = $(this).val() / 100;
        setScale(sScale);
        updateMMFields();
        updateSlider();
    }

    function scaleUnitChanged() {
        $('.b3-btn-unit').removeClass('b3-btn-unit-active');
        $(this).addClass('b3-btn-unit-active');

        // change precision of mm fields
        if ($(this).data('unit') == "in") {
            theUnit = "in";
            $('.scale-mm').each(function () {
                $(this).attr('step', '0.01');
                mmFieldPrecision = 2;
                var v = parseFloat($(this).val());
                $(this).val(v.toFixed(2));
            });
        } else {
            theUnit = "mm";
            $('.scale-mm').each(function () {
                $(this).attr('step', '0.1');
                mmFieldPrecision = 1;
                var v = parseFloat($(this).val());
                $(this).val(v.toFixed(1));
            });
        }

        var sScale = $('#scale-percentage').val() / 100;
        setScale(sScale);
        initSlider();
        updateSlider();
    }


    publix.updateAll = updateAll;
    function updateAll() {
        initSlider();
        updateMMFields();
        updateSlider();
        updatePercentage();
        initSlider();
    }


    /*
    Reads the stl-file object (coming from AJAX and updates the necessary fields in DOM)
     */
    function updateData() {
        var stlFile = Button3d.common.repair.getStlFile();
        var scale = stlFile.scale;
        var unit = stlFile.unit;
        if (unit === 'in') {
            scale /= INCH;
        }

        $('#scale-mm-h').data('original', stlFile.h);
        $('#scale-mm-w').data('original', stlFile.w);
        $('#scale-mm-d').data('original', stlFile.d);

        $('#scale-mm-h').attr('max', (stlFile.h * stlFile.maxScale).toFixed(mmFieldPrecision));
        $('#scale-mm-w').attr('max', (stlFile.w * stlFile.maxScale).toFixed(mmFieldPrecision));
        $('#scale-mm-d').attr('max', (stlFile.d * stlFile.maxScale).toFixed(mmFieldPrecision));

        $('#scale-percentage').attr('max', (stlFile.maxScale * 100).toFixed(1));

        // change the unit
        $('#unit-' + unit).click();

        // change the scale
        setScale(scale);
    }


    var scaleChangeTimeout;
    /*
     * Set the underlying scale with respect to the unit
     */
    publix.setScale = setScale;
    function setScale(scale) {

        scaleChangeTimeout && clearTimeout(scaleChangeTimeout);
        scaleChangeTimeout = setTimeout(function() {
            saveScaleAndUnitInDatabase(scale, theUnit);
            $('body').trigger("scaleChange");
        }, 200);

        if (theUnit === 'in') {
            scale *= INCH;
        }

        theScale = scale;

        Button3d.viewer.webgl.scaleLine.update();
    }

    function saveScaleAndUnitInDatabase(scale, unit) {
        // Update project line
        if (typeof projectId !== 'undefined' && typeof lineId !== 'undefined'
            && typeof headerVM !== 'undefined') {
            if (!headerVM.isProjectOrdered()) {
                project_api.set_line_scale_and_unit(
                    projectId, lineId, scale, unit
                );
            }
        } else { // or update stl file
            $.ajax({
                url: Button3d.common.repair.getStlFile().REST_url,
                type: 'POST',
                data: {
                    'scale': scale,
                    'unit': unit
                }
            });
        }
    }


    function updatePercentage() {
        var sScale = getShownScale();
        var percentage = sScale * 100;
        percentage = percentage.toFixed(1);
        $('#scale-percentage').val(percentage);
    }

    function updateMMFields() {
        var sScale = getShownScale();
        $('.scale-mm').each(function (index) {
            var newMM = sScale * $(this).data('original');
            $(this).val(newMM.toFixed(mmFieldPrecision));
        });
    }

    function updateSlider() {
        var sScale = getShownScale();
        $('#comparator-scale-slider').val(scaleToSlider(sScale));
    }

    /* Slider goes from 0 to 100. Calculate the scale from given slider value.
     * This is done by 2 linear functions seperated at scale = 1.
     */
    function sliderToScale(val) {
        var maxScale = getMaxScale();
        var scale = 0.0;
        var m, n;
        if (val > sliderOne) {
            m = (maxScale - 1) / (100 - sliderOne);
            n = (-(maxScale - 1) * sliderOne) / (100 - sliderOne) + 1;
            scale = m * val + n;
            scale = scale.toFixed(1);
        } else {
            m = (1 / sliderOne);
            scale = m * val;
            scale = scale.toFixed(2);
        }

        if (scale > maxScale) {
            scale = maxScale;
        }
        return scale;
    }

    /* Slider goes from 0 to 100. Calculate the slider value from given scale.
     * Reversed function from sliderToScale().
     */
    function scaleToSlider(val) {
        var maxScale = getMaxScale();
        var slider = 0;
        if (val >= maxScale) {
            return 100;
        }
        if (val > 1) {
            slider = ((100 - sliderOne) * (val - 1)) / (maxScale - 1) + sliderOne;
        } else {
            slider = val * sliderOne;
        }
        return Math.round(slider);
    }

    /* Returns maxscale acording to unit */
    function getMaxScale() {
        var maxScale = Button3d.common.repair.getStlFile().maxScale;
        if (theUnit == 'in') {
            maxScale = maxScale / INCH;
        }
        if (maxScale < 1) {
            maxScale = 1;
        }
        return maxScale;
    }

    /* Returns current scale according to selected unit.
     *
     * Note that the scale in elScaleSlider.data is always
     * correct and is the scale with respect to mm
     *
     * So when inch is selected and the shown scale is 1 ( 100 % ),
     * the underlying scale will be 25,4 ( 2540 % )
     */
    function getShownScale() {
        var scale = theScale;
        if (theUnit == 'in') {
            scale = scale / INCH;
        }
        return scale;
    }

    return publix;
}());
