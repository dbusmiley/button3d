(function popoverContext() {
    ko.bindingHandlers.bootstrapPopover = {
        init: function initialize(element, valueAccessor) {
            var options = valueAccessor();
            var content;
            var template;
            var type = valueAccessor().data.type;
            var values = valueAccessor().data.values;
            if (type === 'rating_partner') {
                content = formatPopOverRatingPartner(values);

                var templateNew =
                    $('<div>', { class: 'popover', role: 'popover' }).css('width', '280px').css('height', '120px')
                        .append($('<div>', { class: 'arrow' }))
                        .append($('<h3>', { class: 'popover-title' }))
                        .append($('<div>', { class: 'popover-content'})
                            .append($('<div>', { class: 'data-content' })));
                template =  templateNew[0].outerHTML;
            } else if (type === 'info_partner') {
                content = formatPopOverContactInfo(values);
            }
            var defaultOptions = {
                animation: true,
                html: true,
                placement: 'auto',
                content: content,
                template: template
            };
            options = $.extend(true, {}, defaultOptions, options);
            $(element).popover(options);

            $('body').on('click', function onBodyClick(e) {
                $(element).each(function forEachElement() {
                    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                        $(this).popover('hide');
                    }
                });
            });
        },
        update: function update(element, valueAccessor) {
            if (Button3d.compare.materials.Steps.update_contact_panel_info()) {
                ko.utils.registerEventHandler(element, 'click', function onElementClick() {
                    if (valueAccessor().data.type === 'info_partner') {
                        var content = formatPopOverContactInfo(Button3d.compare.materials.Steps.selected_partner_info());
                        $('.popover-partner').find('.popover-content').html(content);
                        Button3d.compare.materials.Steps.update_contact_panel_info(false);
                    }
                });
            }
        }
    };

    /**
     * This function will create the html syntax for key value information boxes inside the contact information.
     * @param {object} data Data which should be converted to the HTML.
     * @returns {string} HTML text which will get returned.
     */
    function formatPopOverContactInfo(data) {
        function createLabel(key, value) {
            return '<div class="b3-label-value">' + key[0].outerHTML + '<br class="visible-xxxs hidden">' + value[0].outerHTML + '</div>';
        }

        function createKeyContainer(key) {
            return $('<div>', { class: 'b3-label-right pull-left' }).html(key + '&nbsp;');
        }

        var phoneAnchor = $('<a>', {href: 'tel:' + data.phone}).html(data.phone);
        var emailAnchor = $('<a>', {href: 'mailto:' + data.email}).html(data.email);
        var websiteAnchor = $('<a>', {href: data.website, target: '_blank', rel: 'noopener noreferrer'}).html(data.website);

        var phoneValueContainer = $('<div>', { class: 'b3-value' }).append(phoneAnchor);
        var emailValueContainer = $('<div>', { class: 'b3-value' }).append(emailAnchor);
        var websiteValueContainer = $('<div>', { class: 'b3-value' }).append(websiteAnchor);
        var addressValueContainer = $('<div>', { class: 'b3-value', style: 'margin-bottom:15px;' }).html(data.address);

        var content = $('<div>', {class: 'userpanel-order'});
        if (data.phone) content.append(createLabel(createKeyContainer(gettext('Telephone:')), phoneValueContainer));
        if (data.email) content.append(createLabel(createKeyContainer(gettext('Email:')), emailValueContainer));
        if (data.website) content.append(createLabel(createKeyContainer(gettext('Website:')), websiteValueContainer));
        if (data.address) content.append(createLabel(createKeyContainer(gettext('Address:')), addressValueContainer));
        return content[0].outerHTML;
    }

    function formatPopOverRatingPartner(data) {
        var ratings = [
            {text: gettext('Quality ') + ': ', data: data.quality},
            {text: gettext('Reliability ') + ': ', data: data.reliability},
            {text: gettext('Service ') + ': ', data: data.service}
        ];

        function divStarRating(startData) {
            var starDivs = [];
            for (var i = 0; i < startData.length; i++) {
                if (startData[i] === 0) {
                    starDivs.push($('<div>', {class: 'fa fa-lg fa-star-o'}));
                } else if (startData[i] === 1) {
                    starDivs.push($('<div>', {class: 'fa fa-lg fa-star'}));
                }
            }
            return starDivs.map(function convertToRawHtml(div) {
                return div[0].outerHTML;
            }).join('');
        }

        function createLabelValue(text, stars) {
            return $('<div>', {class: 'b3-label-value'})
                .append($('<div>', {class: 'b3-label-right pull-left'}).html(text))
                .append($('<div>', {class: 'b3-value'}).css('color', '#fc0;').html(stars));
        }

        var ratingsDomElement = ratings.map(function mapRatings(rating) {
            return createLabelValue(rating.text, divStarRating(rating.data));
        });

        return $('<div>', {class: 'userpanel-order panel'})
            .css('border', '0')
            .append(ratingsDomElement)
            [0].outerHTML;
    }
}());
