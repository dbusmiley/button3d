function StlFileVM(data){
    var self = this;
    self.uuid = data["uuid "];
    self.name = data.name;
    self.maxScale = data.parameter.max_scale;
    self.volume = data.parameter.volume;
    self.area = data.parameter.area;
    self.h = data.parameter.h;
    self.w = data.parameter.w;
    self.d = data.parameter.d;
    self.shells = data.parameter.shells;
    self.holes = data.parameter.holes;
    self.faces = data.parameter.faces;
    self.thumbnail_url = data.thumbnail_url;
    self.RESTUrl = data.REST_url;
    self.scale = data.scale;
    self.unit = data.unit;
    self.multicolor = data.multicolor
    self.get_prices = function(){
        for(var i=0; i<parentVM.material_list().length;i++){
            parentVM.material_list()[i].get_price();
        }
    }

}