/* global Dropzone, project_api */

Button3d.common.dropzone = (function dropzoneScope() {
    function init() {
    }

    function createDropzone() {
        var projectUrl = null;
        var newProjectUrl = null;
        var addedFiles = 0;
        var acceptedFiles = 0;
        var uuidsToPushToProject = [];
        var uploadDropzone = null;
        var domain = null;
        var psConfigUpload = null;
        var isOnProjectListView = false;
        var isOnProjectListUrl = null;
        var cancelledUpload = false;
        var uploadIncludingInvalidFiles = false;

        function getFileType(filename, configuration) {
            var extension = Button3d.common.fileExtensions.getExtension(filename).toLowerCase();
            var fileTypeInstance = null;

            configuration.files.forEach(function onFileType(filetype) {
                if (filetype.types === '*' && fileTypeInstance === null) {
                    fileTypeInstance = filetype;
                }
                var allFileTypes = filetype.types.split(',');
                var dottedExtension = '.' + extension;
                if (allFileTypes.indexOf(dottedExtension) > -1) {
                    fileTypeInstance = filetype;
                }
            });
            return fileTypeInstance;
        }

        function init(configuration) {
            Dropzone.autoDiscover = false;
            var dropzoneOptions = {};
            dropzoneOptions.parallelUploads = configuration.parallelUploads;
            dropzoneOptions.url = 'http://app.3yourmind.com'; // We need to have some standard url.
            dropzoneOptions.clickable = configuration.clickable;
            dropzoneOptions.maxFilesize = configuration.maxFilesize;
            dropzoneOptions.timeout = configuration.timeout;
            dropzoneOptions.multipleUpload = configuration.multipleUpload;
            dropzoneOptions.autoProcessQueue = false;
            dropzoneOptions.previewTemplate = configuration.previewTemplate;
            psConfigUpload = configuration.psConfigUpload;

            dropzoneOptions.accept = function accept(file, done) {
                acceptedFiles += 1;
                done();
            };

            dropzoneOptions.init = function () {

                waitToUpload();

                function waitToUpload() {
                    var intervalId = setInterval(function () {
                        clearInterval(intervalId);
                        if (!(configuration.showFileValidation) &&
                            !(acceptedFiles != addedFiles || addedFiles === 0) &&
                            !uploadIncludingInvalidFiles
                        ) {
                            uploadDropzone.processQueue();
                        }
                        waitToUpload();
                    }, 1000);
                }

                var projectCreating = false;

                this.on('addedfile', function onAddedFile(file) {
                    cancelledUpload = false;

                    if ($('.ordered-tag').length > 0) {
                        $(publix).trigger('addedFile', file);
                        addedFiles += 1;
                        return;
                    }

                    if ($('.ribbon-dropzone-text').length > 0 && !projectCreating) {
                        projectCreating = true;
                        project_api.add_project('3D Project', function onResponse(data) {
                            basketId = null;
                            projectId = data.id;
                            projectUrl = data.redirect_url;
                        });
                    }

                    var is3DFile = Button3d.common.fileExtensions.is3DFile(file.name);
                    if (is3DFile) {
                        var uploadLimit = Button3d.common
                            .fileExtensions.getMaximumFilesize(file.name);
                        if (file.size > uploadLimit * 1024 * 1024) {
                            this.removeFile(file);
                            $(publix).trigger('invalid3DFile', file);
                            uploadIncludingInvalidFiles = true;
                            return;
                        }
                    }
                    $(publix).trigger('addedFile', file);
                    addedFiles += 1;
                });

                this.on('totaluploadprogress', function onTotalUploadProgress(uploadProgress) {
                    $(publix).trigger('totalUploadProgress', uploadProgress);
                });

                this.on('sending', function onSending(file, xhr, formData) {
                    if ($('.ordered-tag').length > 0) {
                        return;
                    }

                    var fileTypeInstance = getFileType(file.name, configuration);
                    var uploadLimit = Button3d.common
                        .fileExtensions.getMaximumFilesize(file.name);
                    var uploadLimitInBytes = uploadLimit * 1024 * 1024;
                    if (file.size > uploadLimitInBytes) {
                        return;
                    }
                    if (fileTypeInstance.setCorsHeaders) {
                        formData.append('origin', 'button3d');
                        if (domain) {
                            formData.append('domain', domain);
                        }
                    }
                });

                this.on('processing', function onProcessing(file) {
                    if ($('.ordered-tag').length > 0) {
                        this.options.url = '/basket-ajax/attachment/ajax/';
                        if (basketId) {
                            this.options.headers = {};
                            this.options.params = {
                                'csrfmiddlewaretoken': getCookie('csrftoken'),
                                'basket_id': basketId
                            };
                        }

                        if (!(projectId === '' || projectId === 'None')) {
                            this.options.headers = {};
                            this.options.params = {
                                'csrfmiddlewaretoken': getCookie('csrftoken'),
                                'basket_id': projectId
                            };
                        }
                        return;
                    }

                    var fileTypeInstance = getFileType(file.name, configuration);
                    this.options.url = fileTypeInstance.url;
                    var uploadLimit = Button3d.common
                        .fileExtensions.getMaximumFilesize(file.name);
                    var uploadLimitInBytes = uploadLimit * 1024 * 1024;
                    if (file.size > uploadLimitInBytes) {
                        this.options.url = '/basket-ajax/attachment/ajax/';
                    }

                    if (file.size < uploadLimitInBytes && fileTypeInstance.setCorsHeaders) {
                        this.options.params = {};
                        this.options.headers = {
                            'Cache-Control': '',
                            'X-Requested-With': ''
                        };
                    }

                    if ((basketId && (fileTypeInstance.setParamsForProject) || file.size > uploadLimitInBytes)) {
                        this.options.headers = {};
                        this.options.params = {
                            'csrfmiddlewaretoken': getCookie('csrftoken'),
                            'basket_id': basketId
                        };
                    }
                    if (!(projectId === '' || projectId === 'None') && (fileTypeInstance.setParamsForProject || file.size > uploadLimitInBytes)) {
                        this.options.headers = {};
                        this.options.params = {
                            'csrfmiddlewaretoken': getCookie('csrftoken'),
                            'basket_id': projectId
                        };
                    }
                });

                var singleFileUrl = null;

                this.on('success', function onSuccess(file, response) {
                    singleFileUrl = response.url;
                    var eventPayload = generateUploadEventPayload(response.uuid);
                    tryDispatchEventToParentIFrame(eventPayload);
                    uuidsToPushToProject.push(response.uuid);
                });

                function generateUploadEventPayload(uuid) {
                    return {
                        type: '3D_FILE_UPLOADED',
                        uuid: uuid
                    };
                }

                function tryDispatchEventToParentIFrame(eventPayload) {
                    if (parent) {
                        var targetFrameUrl = '*';
                        parent.postMessage(
                            JSON.stringify(eventPayload),
                            targetFrameUrl
                        );
                    }
                }

                this.on('queuecomplete', function onQueueComplete() {
                    if (uuidsToPushToProject && uuidsToPushToProject.length > 0) {
                        $(publix).trigger('uploadComplete');
                    }
                    if (isOnProjectListView) {
                        processUuids(uuidsToPushToProject, function () {
                            window.location = isOnProjectListUrl;
                        });
                        return;
                    }

                    if (psConfigUpload) {
                        $(publix).trigger('priceCheck', uuidsToPushToProject[0]);
                        return;
                    }

                    if (cancelledUpload) {
                        cancelledUpload = false;
                        return;
                    }

                    if (!inProjectView && uuidsToPushToProject.length === 1) {
                        if (singleFileUrl) {
                            window.location = singleFileUrl;
                        } else {
                            window.location = projectUrl;
                        }
                        return;
                    }

                    if (uuidsToPushToProject.length === 1 && (!projectId || projectId === '' || projectId === 'None')) {
                        window.location = singleFileUrl;
                        return;
                    }

                    processUuids(uuidsToPushToProject, function () {
                        if (newProjectUrl) {
                            window.location = newProjectUrl;
                            return;
                        }
                        window.location = projectUrl;
                    });

                    function processUuids(uuids, callback) {
                        uuids.forEach(function (uuid) {
                            if (basketId !== 'None' && !projectId) {
                                project_api.add_file_to_project(basketId, uuid);
                            } else {
                                project_api.add_file_to_project(projectId, uuid);
                            }
                        });
                        callback();
                    }
                });

                this.on('uploadprogress', function onUploadProgress(file, progress, bytesSent) {
                    var eventObject = {
                        file: file,
                        progress: progress,
                        bytesSent: bytesSent
                    };
                    $(publix).trigger('uploadProgress', eventObject);
                });

                this.on('drop', function onFileDrop() {
                    $(publix).trigger('filesDropped');
                });
            };
            uploadDropzone = new Dropzone(configuration.domElement, dropzoneOptions);
        }

        function dropAllFiles() {
            uploadDropzone.removeAllFiles();
        }

        function startUpload() {
            uploadDropzone.processQueue();
        }

        function setProjectId(projectIdParam) {
            projectId = projectIdParam;
        }

        function setProjectUrl(projectUrlParam) {
            projectUrl = projectUrlParam;
        }

        function getUploadCount() {
            return uploadDropzone.files.length;
        }

        function setDomain(domainParam) {
            domain = domainParam;
        }

        function setIsOnProjectListView(url) {
            isOnProjectListView = true;
            isOnProjectListUrl = url;
        }

        function stopUpload() {
            cancelledUpload = true;
            uploadDropzone.files.forEach(function dropFile(file) {
                uploadDropzone.cancelUpload(file);
            });
            dropAllFiles();
        }

        function resetUpload() {
            addedFiles = 0;
            acceptedFiles = 0;
            uploadIncludingInvalidFiles = false;
        }

        var publix = {
            init: init,
            dropAllFiles: dropAllFiles,
            startUpload: startUpload,
            setProjectId: setProjectId,
            setProjectUrl: setProjectUrl,
            getUploadCount: getUploadCount,
            setDomain: setDomain,
            setIsOnProjectListView: setIsOnProjectListView,
            stopUpload: stopUpload,
            resetUpload: resetUpload
        };

        return publix;
    }

    return {
        init: init,
        createDropzone: createDropzone
    };
})();
