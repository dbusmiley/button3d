function GroupVM(parentVM, id, showname, users, permissions, editable, deletable) {
    var self = this;

    self.parentVM = parentVM;
    self.id = id;
    self.showname = ko.observable(showname);
    self.users = ko.observableArray(users);
    self.permissions = ko.observableArray(permissions);
    self.editable = ko.observable(editable);
    self.deletable = ko.observable(deletable);

    self.not_contained_users = ko.pureComputed(function() {
        return self.parentVM.users().filter(function(item) {
            return $.inArray(item, self.users()) === -1;
        });
    }, self);

    self.not_contained_permissions = ko.pureComputed(function() {
        return self.parentVM.permissions().filter(function(item) {
            return $.inArray(item, self.permissions()) === -1;
        });
    }, self);

    self.selected_in_users = ko.observableArray();
    self.selected_not_in_users = ko.observableArray();
    self.selected_in_permissions = ko.observableArray();
    self.selected_not_in_permissions = ko.observableArray();

    self.add_user = function() {
        var selectedUsers = self.selected_not_in_users();
        for (var i = 0; i < selectedUsers.length; ++i) {
            var user = selectedUsers[i];
            org_management_api.add_user_to_group(user.id, self.id, add_user_to_group_callback);
        }
    };

    function add_user_to_group_callback(data) {
        if (data.success) {
            self.users.push(selectedUsers.filter(function(item) {
                return item.id == data.id;
            })[0]);
        } else {
            // TODO error handling
            console.log("Error adding user: " + data.message);
        }
    }

    self.remove_user = function() {
        if (self.editable() && !self.deletable()) { // Admin group
            var len = self.selected_in_users().filter(function(item) {
                return item.id == self.parentVM.current_user().id;
            }).length;

            if (len == 1) {
                console.log("Do not push, current admin contained");
                return;
            }
        }

        var selectedUsers = self.selected_in_users();
        for (var i = 0; i < selectedUsers.length; ++i) {
            var user = selectedUsers[i];
            org_management_api.remove_user_from_group(user.id, self.id, remove_user_from_group_callback);
        }
    };

    function remove_user_from_group_callback(data) {
        if (data.success) {
            self.users.remove(selectedUsers.filter(function(item) {
                return item.id == data.id;
            })[0]);
        } else {
            // TODO error handling
            console.log("Error removing user: " + data.message);
        }
    }

    self.can_add_user = ko.pureComputed(function() {
        return self.editable() && self.not_contained_users().length > 0;
    }, self);

    self.can_remove_user = ko.pureComputed(function() {
        return self.editable() && self.users().length > 0;
    }, self);

    self.add_permission = function() {
        var selectedPermissions = self.selected_not_in_permissions();
        for (var i = 0; i < selectedPermissions.length; ++i) {
            var perm = selectedPermissions[i];
            org_management_api.add_permission_to_group(perm.id, self.id, add_permission_to_group_callback);
        }
    };

    function add_permission_to_group_callback(data) {
        if (data.success) {
            self.permissions.push(self.selected_not_in_permissions().filter(function(item) {
                return item.id == data.id;
            })[0]);
        } else {
            // TODO error handling
            console.log("Error adding permission: " + data.message);
        }
    }

    self.remove_permission = function() {
        var selectedPermissions = self.selected_in_permissions();
        for (var i = 0; i < selectedPermissions.length; ++i) {
            var perm = selectedPermissions[i];
            org_management_api.remove_permission_from_group(perm.id, self.id, remove_permission_from_group_callback);
        }
    };

    function remove_permission_from_group_callback(data) {
        if (data.success) {
            self.permissions.remove(self.selected_in_permissions().filter(function(item) {
                return item.id == data.id;
            })[0]);
        } else {
            // TODO error handling
            console.log("Error removing permission: " + data.message);
        }
    }

    self.can_add_permission = ko.pureComputed(function() {
        return self.not_contained_permissions().length > 0;
    }, self);

    self.can_remove_permission = ko.pureComputed(function() {
        return self.permissions().length > 0;
    }, self);
}
