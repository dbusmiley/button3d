function UserVM(id, email, is_staff) {
    var self = this;

    self.id = id;
    self.email = ko.observable(email);
    self.is_staff = ko.observable(is_staff === 'True' || is_staff === true);
}
