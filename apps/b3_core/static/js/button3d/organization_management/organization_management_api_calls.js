var Button3d = Button3d || {};

var org_management_api = extend(Button3d, 'organization.management.api');

$.extend(org_management_api, (function() {
    var publix = {};

    // ------- User calls
    publix.create_user = function(email, password, success_callback) {
        $.ajax({
            url: '/manage/users/create/',
            contentType: "application/json",
            data: JSON.stringify({'email': email, 'password': password}),
            type: 'PUT',
            success: success_callback
        });
    };

    publix.delete_user = function(id, success_callback) {
        $.ajax({
            url: '/manage/users/delete/',
            contentType: "application/json",
            data: JSON.stringify({'id': id}),
            type: 'DELETE',
            success: success_callback
        });
    };

    publix.create_group = function(name, success_callback) {
        $.ajax({
            url: '/manage/groups/create/',
            contentType: "application/json",
            data: JSON.stringify({'name': name}),
            type: 'PUT',
            success: success_callback
        });
    };

    publix.delete_group = function(id, success_callback) {
        $.ajax({
            url: '/manage/groups/delete/',
            contentType: "application/json",
            data: JSON.stringify({'id': id}),
            type: 'DELETE',
            success: success_callback
        });
    };

    publix.add_user_to_group = function(user_id, group_id, success_callback) {
       _update_user_group(user_id, group_id, 'add', success_callback);
    };

    publix.remove_user_from_group = function(user_id, group_id, success_callback) {
       _update_user_group(user_id, group_id, 'remove', success_callback);
    };

    function _update_user_group(user_id, group_id, action, callback) {
       $.ajax({
            url: '/manage/users/update/',
            contentType: "application/json",
            data: JSON.stringify({'user_id': user_id, 'group_id': group_id, 'action': action}),
            type: 'POST',
            success: callback
        });
    }

    publix.add_permission_to_group = function(perm_id, group_id, success_callback) {
       _update_group_permission(perm_id, group_id, 'add', success_callback);
    };

    publix.remove_permission_from_group = function(perm_id, group_id, success_callback) {
       _update_group_permission(perm_id, group_id, 'remove', success_callback);
    };

    function _update_group_permission(perm_id, group_id, action, callback) {
       $.ajax({
            url: '/manage/groups/update/',
            contentType: "application/json",
            data: JSON.stringify({'perm_id': perm_id, 'group_id': group_id, 'action': action}),
            type: 'POST',
            success: callback
        });
    }

    // ------ Supplier/partner calls
    publix.enable_partner = function(partner_id, success_callback) {
        $.ajax({
            url: '/manage/partners/enable/',
            contentType: "application/json",
            data: JSON.stringify({'id': partner_id}),
            type: 'POST',
            success: success_callback
        });
    };

    publix.disable_partner = function(partner_id, success_callback) {
        $.ajax({
            url: '/manage/partners/disable/',
            contentType: "application/json",
            data: JSON.stringify({'id': partner_id}),
            type: 'POST',
            success: success_callback
        });
    };

    publix.override_option = function(option_name, option_value, partner_id, success_callback) {
        $.ajax({
            url: '/manage/options/enable/',
            contentType: "application/json",
            data: JSON.stringify({'option_name': option_name,
                                  'option_value': option_value,
                                  'partner_id': partner_id}),
            type: 'POST',
            success: success_callback
        });
    };

    publix.unoverride_option = function(option_name, partner_id, success_callback) {
        $.ajax({
            url: '/manage/options/disable/',
            contentType: "application/json",
            data: JSON.stringify({'option_name': option_name,
                                  'partner_id': partner_id}),
            type: 'POST',
            success: success_callback
        });
    };

    // ------ Addresses calls
    publix.delete_organization_address = function(addr_id, success_callback) {
        $.ajax({
            url: '/manage/address/delete/',
            contentType: "application/json",
            data: JSON.stringify({'id': addr_id}),
            type: 'DELETE',
            success: success_callback
        });
    };

    // ------ Settings calls
    publix.update_org_showname = function(showname, success_callback) {
        $.ajax({
            url: '/manage/showname/update/',
            contentType: "application/json",
            data: JSON.stringify({'name': showname}),
            type: 'POST',
            success: success_callback
        });
    };

    publix.update_org_configuration_flag = function(flags_list, success_callback) {
        $.ajax({
            url: '/manage/flags/update/',
            contentType: "application/json",
            data: JSON.stringify(flags_list),
            type: 'POST',
            success: success_callback
        });
    };

    publix.update_org_key_manager = function(id, success_callback) {
        $.ajax({
            url: '/manage/organization/keymanager/update/',
            contentType: "application/json",
            data: JSON.stringify({'id': id}),
            type: 'POST',
            success: success_callback
        });
    };

    publix.get_key_manager_form = function(id, success_callback) {
        var url = '/manage/keymanager/get/';
        if (id)
            url += '?id=' + id;

        $.ajax({
            url: url,
            contentType: "application/json",
            type: 'GET',
            success: success_callback
        });
    };

    publix.delete_key_manager = function(id, success_callback) {
        $.ajax({
            url: '/manage/keymanager/delete/',
            contentType: "application/json",
            data: JSON.stringify({'id': id}),
            type: 'DELETE',
            success: success_callback
        });
    };

    // ------ Analytics calls
    publix.get_graph_data = function(key, timespan, success_callback, users, type) {
        var data = 'key=' + key + '&timespan=' + timespan;
        if (users)
            data += '&users=' + users;
        if (type)
            data += "&type=" + type;

        $.ajax({
            url: '/manage/analytics/graph/',
            contentType: "application/json",
            data: data,
            type: 'GET',
            success: success_callback
        });
    };

    publix.get_user_activity = function(timespan, success_callback) {
        $.ajax({
            url: '/manage/analytics/users/',
            contentType: "application/json",
            data: 'timespan=' + timespan,
            type: 'GET',
            success: success_callback
        });
    };

    publix.get_activity_log = function(success_callback) {
        $.ajax({
            url: '/manage/analytics/log/',
            contentType: "application/json",
            data: 'lang=' + lang,
            type: 'GET',
            success: success_callback
        });
    };

    return publix;
}()));
