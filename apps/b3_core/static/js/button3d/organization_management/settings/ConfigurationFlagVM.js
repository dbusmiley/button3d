function ConfigurationFlagVM(data) {
    var self = this;

    self.name = ko.observable(data.name);
    self.verbose_name = ko.observable(data.verbose_name);
    self.is_enabled = ko.observable(data.is_enabled);
}
