function KeyManagerVM(parentVM, data) {
    var self = this;

    self.id = data.id;
    self.parentVM = parentVM;
    self.is_used = ko.observable(data.is_used);
    self.name = ko.observable(data.name);
    self.title = ko.observable(data.title);
    self.title_en = ko.observable(data.title_en);
    self.title_de = ko.observable(data.title_de);
    self.role = ko.observable(data.role);
    self.role_en = ko.observable(data.role_en);
    self.role_de = ko.observable(data.role_de);
    self.email = ko.observable(data.email);
    self.telephone = ko.observable(data.telephone);
    self.manager_photo = ko.observable(self.manager_photo);
    self.nb_referenced_by = ko.observable(Number(data.nb_referenced_by));

    self.modify_keymanager = function() {
        Button3d.modals.show_create_modify_key_manager_modal(self, false);
    };

    self.delete_keymanager = function() {
        Button3d.modals.show_delete_key_manager_modal(self.id, self.name(), self.nb_referenced_by(), function(data) {
            if (data.success) {
                self.parentVM.key_managers.remove(self);
            } else {
                console.log("Could not delete key manager: " + data.message);
            }
        });
    };
}
