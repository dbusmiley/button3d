function PartnerOptionVM(parentVM, data) {
    var self = this;

    self.parentVM = parentVM;
    self.name = ko.observable(data.name);
    self.verbose_name = ko.observable(data.verbose_name);
    self.default_value = ko.observable(data.default_value);
    self.is_overriden = ko.observable(data.is_overriden);
    if (data.overriden_value === 'None')
        data.overriden_value = '';
    self.overriden_value = ko.observable(data.overriden_value);
    self.type = ko.observable(data.type);

    self.is_overriden.subscribe(function(newValue) {
        if (newValue) {
            org_management_api.override_option(self.name(), self.overriden_value(), self.parentVM.partner_id(), function(data) {
                if (!data.success) {
                    console.log("Error putting new value: " + data.message);
                }
            });
        } else {
            org_management_api.unoverride_option(self.name(), self.parentVM.partner_id(), function(data) {
                if (!data.success) {
                    console.log("Error unoverriding option: " + data.message);
                }
            });
        }
    });
    
    self.overriden_value.subscribe(function(newValue) {
        org_management_api.override_option(self.name(), newValue, self.parentVM.partner_id(), function(data) {
            if (!data.success) {
                console.log("Error updating value: " + data.message);
            }
        });
    });
}
