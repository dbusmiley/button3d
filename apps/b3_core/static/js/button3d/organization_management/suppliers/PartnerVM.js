function PartnerVM(parentVM, id, name, base_config_url) {
    var self = this;

    self.parentVM = parentVM;
    self.id = id;
    self.name = ko.observable(name);
    self.configure_url = ko.observable(base_config_url.replace('0', self.id));
    self.enabled = ko.observable(false);

    self.enable_disable_text = ko.pureComputed(function() {
        if (!self.enabled()) {
            return gettext('Enable');
        }
        return gettext('Disable');
    }, self);

    self.show_delete_modal = function() {
        Button3d.modals.show_delete_supplier_modal(self);
    };

    self.delete = function() {
        if (self.enabled())
            self.parentVM.enabled_partners.remove(self);

        self.parentVM.organization_partners.remove(self);
    };
}
