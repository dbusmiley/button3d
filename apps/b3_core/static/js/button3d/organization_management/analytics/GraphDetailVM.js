function GraphDetailVM(key, title, users, required_types) {
    var self = this;

    self.key = ko.observable(key);
    self.title = ko.observable(title);
    self.timespan = ko.observable('week');
    self.timespan_text = ko.pureComputed(function() {
        if (self.timespan() === 'week') {
            return gettext('week');
        }
        if (self.timespan() === 'month') {
            return gettext('month');
        }
        if (self.timespan() === 'year') {
            return gettext('year');
        }
    }, self);
    self.total = ko.observable();

    self.users = ko.observableArray();
    self.selected_user = ko.observable();
    var i, len, arr;
    if (typeof users !== 'undefined') {
        len = users.length;
        arr = [];
        for (i = 0; i < len; ++i) {
            arr.push({
                title: users[i][0],
                key: users[i][1]
            });
        }

        self.users(arr);
        self.selected_user(arr[0]);

        self.selected_user.subscribe(function(newValue) {
            update_graph();
        });
    }

    self.types = ko.observableArray();
    self.selected_type = ko.observable();
    if (typeof required_types !== 'undefined') {
        len = required_types.length;
        arr = [];
        for (i = 0; i < len; ++i) {
            arr.push({
                title: required_types[i][0],
                key: required_types[i][1]
            });
        }

        self.types(arr);
        self.selected_type(arr[0]);

        self.selected_type.subscribe(function(newValue) {
            update_graph();
        });
    }

    self.week = function() {
        self.timespan('week');
        update_graph();
    };
    self.month = function() {
        self.timespan('month');
        update_graph();
    };
    self.year = function() {
        self.timespan('year');
        update_graph();
    };

    function update_graph() {
        make_api_call(function(response) {
            if (response.success) {
                for (var i = 0, sum = 0; i < response.data.length; sum += parseFloat(response.data[i++])) ;
                if (sum % 1 === 0) { // Test if int
                    self.total(sum);
                } else {
                    self.total(setCurrency(sum.toFixed(2), 'EUR')); // TODO not hard-code euro
                }

                self.chart.data.datasets[0].data = response.data;
                self.chart.data.labels = response.labels;
                self.chart.update();
            } else {
                // TODO error handling
                console.log('Error updating graph: ' + response.message);
            }
        });
    }

    function make_api_call(callback) {
        if (self.users().length > 0) {
            if (self.types().length > 0) {
                org_management_api.get_graph_data(self.key(), self.timespan(), callback, self.selected_user().key, self.selected_type().key);
            } else {
                org_management_api.get_graph_data(self.key(), self.timespan(), callback, self.selected_user().key);
            }
        } else {
            org_management_api.get_graph_data(self.key(), self.timespan(), callback);
        }
    }


    // Init
    $(function() {
        var ctx = $("#" + self.key());
        self.chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: [
                    {
                        data: []
                    }
                ]
            },
            options: {
                legend: {
                    display: false
                }
            }
        });
        update_graph();
    });
}
