$(function() {
    $key_text = $('#id_key');
    var value_text = $('#id_value').val();

    var config_toolbar =  [
        ['Bold', 'Italic', 'Underline'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter',
         'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink'],
        ['Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe'],
        ['RemoveFormat', 'Source']
    ];
    var richtextfields = [
        'bluebox_preview_step',
        'bluebox_preview_step_de',
        'bluebox_preview_step_en',
        'terms_conditions',
        'terms_conditions_de',
        'terms_conditions_en'
    ];

    setTimeout(function(){
        $key_text.on('change keyup keypress',function(){
            if('id_value' in CKEDITOR.instances){
                var instance = CKEDITOR.instances['id_value'];
                $('#id_value').val(instance.getData());
                instance.destroy(true)
            }
            if(isInArray(richtextfields, $key_text.val())){
                var editor = CKEDITOR.replace( 'id_value', {
                    autoParagraph: false,
                    toolbar: config_toolbar
                });
                editor.setData(value_text);
            }

        })
    },200);


    $('input[type=submit]').click(function() {
        if('id_value' in CKEDITOR.instances){
            var value = CKEDITOR.instances['id_value'].getData();
            $('#id_value').val(value);
        }
    });

    setTimeout(function(){
        $key_text.trigger('change');
    },200);
})