"use strict";
Button3d.viewer = Button3d.viewer || {};
Button3d.viewer.webgl = function () {
    var publix = {};

    var width, height, depth, dist;
    var scene, scene_axis, renderer, model_object, camera, controls, trackballObject, axisObject, scene_globe, camera_globe, rotateMeshObject, modelIsBig;
    var option_nav_height, webgl_original_height;

    var current_mesh, current_wireframe;

    var models = {};

    var lock_counter = 0;
    var setup_done = false;
    var zoom = 0.02;

    publix.init = function () {

        function init() {
            var mycanvas = document.getElementById('webglcanvas');
            var webgl_available = check_webgl(mycanvas);

            width = mycanvas.clientWidth;
            height = mycanvas.clientHeight;
            depth = document.getElementById('depth');


            if (!webgl_available) {
                $('#webgl-loading').hide();
                no_webgl_message();
                return false;
            }

            scene = new THREE.Scene();
            scene_axis = new THREE.Scene();
            model_object = new THREE.Object3D();
            scene.add(model_object);

            scene_globe = new THREE.Scene();

            //orthographic cameras to enable measuring
            camera = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, -1e5, 1e5);
            camera_globe = new THREE.OrthographicCamera(width / -2, width / 2, height / 2, height / -2, 0, 10000);

            addGlobeAndAxis();
            addLights();

            controls = setControls(camera, true, false, false);
            controls.addEventListener('change', function () {
                render();
            });

            // renderer
            renderer = Detector.webgl ? new THREE.WebGLRenderer({
                canvas: mycanvas,
                antialias: true,
                alpha: true
            }) : new THREE.CanvasRenderer();
            renderer.autoClear = false; //to draw to scenes on top of each other, don't forget to manually clear

            one_time_setup();

            setup_done = true;
            return true;
        }

        //checks if a WebGL context can be created
        function check_webgl(mycanvas) {
            var gl;
            try {
                gl = mycanvas.getContext("webgl") || mycanvas.getContext("experimental-webgl");
            }
            catch (e) {
            }
            if (!gl) {
                gl = null;
                return false;
            }
            return true;
        }

        //Displays an error message if the creation of a WebGL context failed
        function no_webgl_message() {
            Button3d.common.send_event_to_ga("webgl-error-message", "show");
            var no_webgl = $('#webgl-error-message');
            var thumbnail_url = no_webgl.children('span').text();
            var img = no_webgl.children('img');
            img.attr('src', thumbnail_url);
            img.height(height);
            no_webgl.show();

            // Disable fullscreen functionnality
            $('.svg-fullscreen').hide();
            $('#3d-tools-fullscreen').hide();
            $('.svg-fullscreen-exit').hide();
            $('#3d-tools-close-fullscreen').hide();
        }

        function setControls(camera_var, noZoom, noPan, staticMoving) {
            var control = new THREE.TrackballControls(camera_var, document.getElementById('webgl'));
            control.noZoom = noZoom;
            control.noPan = noPan;
            control.noRotate = false;
            control.noRoll = true;
            control.staticMoving = staticMoving;
            control.dynamicDampingFactor = 0.7;
            control.screen.left = window.innerWidth * 0.2;
            control.screen.top = 0;
            control.screen.width = window.innerWidth;

            control.screen.height = window.innerHeight;
            return control;
        }

        //on repair and back clicks the init function gets called again so we need to seperate
        // functions that only need to be done once and functions that are needed with every init-call and mesh reload
        function one_time_setup() {
            // Fix OpenCTM Cross Origin Error
            // ('Refused to get unsafe header "Content-Length"')
            THREE.ImageUtils.crossOrigin = '';
            // Well, this is not the fix but it could have something to do with it.

            if (detectIE() && detectIE() <= 11) {
                // disable cube in IE 11 and below
                $('#b3-cube-holder').hide();
            }

            window.addEventListener('resize', onWindowResize, false);

            //important! without this, the model doesnt turn back to the center immediately, after change width from <=991px to >=992px
            window.onresize = function (event) {
                var mywebgl = document.getElementById('webgl');
                renderer.setSize(mywebgl.clientWidth, mywebgl.clientHeight);
                if ($(window).width() > 992 && !Button3d.compare.three_d_tools.is_panel_active()) {
                    $('.progress-panel').css("margin-top", "0");
                }
            };

            $("body").on("reloadFiles", function () {
                models = {};
            });

            //rotation controls with the cube
            //main faces
            $('.b3-inner-main-top').click(function () {
                publix.camera.setPosition('TOP');
            });
            $('.b3-inner-main-front').click(function () {
                publix.camera.setPosition('FRONT');
            });
            $('.b3-inner-main-left').click(function () {
                publix.camera.setPosition('LEFT');
            });
            $('.b3-inner-main-back').click(function () {
                publix.camera.setPosition('BACK');
            });
            $('.b3-inner-main-right').click(function () {
                publix.camera.setPosition('RIGHT');
            });
            $('.b3-inner-main-bottom').click(function () {
                publix.camera.setPosition('BOTTOM');
            });

            //(counter)clockwise rotations
            $('#arrow-counterclockwise').click(function () {
                publix.camera.rotateOnDisplayZ(-1);
            });

            $('#arrow-clockwise').click(function () {
                publix.camera.rotateOnDisplayZ(1);
            });

            var ZOOM_SPEED = 25;

            // Zoom buttons
            $('#zoom-in').click(function () {
                changeZoom(ZOOM_SPEED);
            });

            $('#zoom-out').click(function () {
                changeZoom(-ZOOM_SPEED);
            });

            $('.b3-to-left').click(function () {
                publix.camera.setPosition('LEFT');
            });
            $('.b3-to-right').click(function () {
                publix.camera.setPosition('RIGHT');
            });
            $('.b3-to-top').click(function () {
                publix.camera.setPosition('TOP');
            });
            $('.b3-to-bottom').click(function () {
                publix.camera.setPosition('BOTTOM');
            });
            $('.b3-to-front').click(function () {
                publix.camera.setPosition('FRONT');
            });
            $('.b3-to-back').click(function () {
                publix.camera.setPosition('BACK');
            });

            //show arrows only on hovering
            $('#b3-arrow-holder').hover(
                function () {
                    $('#arrow-clockwise').removeClass('b3-hidden');
                    $('#arrow-counterclockwise').removeClass('b3-hidden');
                }, function () {
                    $('#arrow-clockwise').addClass('b3-hidden');
                    $('#arrow-counterclockwise').addClass('b3-hidden');
                }
            );

            $('#b3-cube-holder').hover(
                function () {
                    $('#arrow-clockwise').removeClass('b3-hidden');
                    $('#arrow-counterclockwise').removeClass('b3-hidden');
                }, function () {
                    $('#arrow-clockwise').addClass('b3-hidden');
                    $('#arrow-counterclockwise').addClass('b3-hidden');
                }
            );

            //zoom control
            $('#webgl').mousewheel(function (event) {
                if (modelIsBig) {showSimpleRotationMesh();}
                changeZoom((event.deltaY * event.deltaFactor) / 10);
                clearTimeout($.data(this, 'timer'));
                $.data(this, 'timer', setTimeout(function() {
                    if (modelIsBig) {hideSimpleRotationMesh();}
                    render();
                }, 250));
            });

            //prevent page scrolling on webgl-div
            $('.b3-webgl-ribbon').on('mousewheel DOMMouseScroll', function (e) {
                e.preventDefault() && e.stopPropagation() && e.preventDefault();
            });

            if (is_mobile_or_tablet()) {
                $('.b3-zoom-holder').css('visibility', 'visible');
            }

            webgl_original_height = $('.b3-webgl-ribbon').height();
            option_nav_height = $('.progress-panel').height();
        }

        /**
         * detect IE
         * returns version of IE or false, if browser is not Internet Explorer
         */
        function detectIE() {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');
            if (msie > 0) {
                // IE 10 or older => return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            }

            var trident = ua.indexOf('Trident/');
            if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            }

            var edge = ua.indexOf('Edge/');
            if (edge > 0) {
                // IE 12 => return version number
                return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            }

            // other browser
            return false;
        }

        function changeZoom(delta) {
            if (!current_mesh) {
                return;
            }

            var width = camera.right / zoom;
            var height = camera.top / zoom;
            //no negative zoom as the result is unexpected for the user
            var zoom_tmp = zoom;
            zoom_tmp -= delta * 0.0001;
            if (zoom_tmp > 0) {
                zoom -= delta * 0.0001;
            }
            //make sure the camera never 'flips', meaning left & bottom never positive / right & top never negative
            if (-zoom * width < 0 && zoom * width > 0) {
                camera.left = -zoom * width;
                camera.right = zoom * width;
                camera.top = zoom * height;
                camera.bottom = -zoom * height;
                camera.updateProjectionMatrix();
                controls.panSpeed = camera.right / (current_mesh.geometry.boundingSphere.radius * 9.9);
            }

            adjustWireframe();
            requestAnimationFrame(render);
        }

        function addLights() {

            //front-right-up
            var spotLight = new THREE.SpotLight(0xffffff, 0.5);
            spotLight.position.set(1000000000, 1000000000, 1000000000);
            spotLight.castShadow = true;
            scene.add(spotLight);

            //front-left
            spotLight = new THREE.SpotLight(0xffffff, 0.5);
            spotLight.position.set(1000000000, 0, -1000000000);
            spotLight.castShadow = true;
            scene.add(spotLight);

            //back-right-down
            spotLight = new THREE.SpotLight(0xffffff, 0.5);
            spotLight.position.set(-1000000000, -1000000000, 1000000000);
            spotLight.castShadow = true;
            scene.add(spotLight);

            //back-left
            spotLight = new THREE.SpotLight(0xffffff, 0.5);
            spotLight.position.set(-1000000000, 0, -1000000000);
            spotLight.castShadow = true;
            scene.add(spotLight);

            scene.add(new THREE.AmbientLight(0x333333));

        }

        /*
         add and create globe around the mesh
         */
        function addGlobeAndAxis() {

            var numLines = 10;
            var pointsPerLine = 100;

            trackballObject = new THREE.Object3D;

            var e = 180 / (numLines + 1);
            var t = THREE.Math.degToRad(e);

            var points = [];
            var outsideCirclePoints = [];
            //mesh.geometry.computeBoundingSphere();
            //var trackballRadius = mesh.geometry.boundingSphere.radius;
            var trackballRadius = 1;

            var u, v;
            // Generate Points for Longitudes
            for (var n = 0; n < numLines; n++) {
                var r = Math.sin(t * (n + 1) - Math.PI / 2) * trackballRadius;
                var i = Math.cos(t * (n + 1) - Math.PI / 2) * trackballRadius;
                var o = [];
                for (u = 0; u < pointsPerLine; u++) {
                    var a = 360 / (pointsPerLine - 1) * u;
                    var f = THREE.Math.degToRad(a);
                    var l = Math.cos(f) * i;
                    var c = Math.sin(f) * i;
                    o.push(new THREE.Vector3(l, c, r));
                }
                points.push(o);
            }

            // Generate Points for Latitudes
            for (n = 0; n < numLines; n++) {
                var h = Math.PI / numLines * n;
                var p = [];
                for (u = 0; u < pointsPerLine; u++) {
                    var d = Math.PI * 2 / (pointsPerLine - 1) * u;
                    v = new THREE.Vector3(Math.sin(d) * Math.cos(h) * trackballRadius, Math.sin(d) * Math.sin(h) * trackballRadius, Math.cos(d) * trackballRadius);
                    p.push(v);
                }
                points.push(p);
            }

            // Draw Lines
            var y = new THREE.LineBasicMaterial({color: (new THREE.Color("#b2b2b2")).getHex(), depthTest: false});
            var b = new THREE.Object3D;
            b.name = "TRACKBALLGLOBE";
            for (n = 0; n < points.length; n++) {
                var w = new THREE.Geometry;
                for (u = 0; u < points[n].length; u++) {
                    w.vertices.push(points[n][u]);
                }
                var E = new THREE.Line(w, y);
                b.add(E);
            }

            trackballObject.add(b);

            // Generate Points for one outer circle
            var m = 150;

            var g = Math.PI * 2 / (m - 1);
            for (n = 0; n < m; n++) {
                v = new THREE.Vector3(Math.cos(g * n) * trackballRadius, Math.sin(g * n) * trackballRadius, 0);
                outsideCirclePoints.push(v);
            }

            // Draw Lines
            var S = new THREE.Geometry;
            for (n = 0; n < outsideCirclePoints.length; n++) {
                S.vertices.push(outsideCirclePoints[n]);
            }
            var outsideCircle = new THREE.Line(S, y);

            outsideCircle.name = "TRACKBALLCIRCLE"; // whatsoever...
            outsideCircle.matrixAutoUpdate = false; // whatsoever...
            outsideCircle.rotationAutoUpdate = false; // whatsoever...
            trackballObject.add(outsideCircle);

            // Draw colored axis
            var T = trackballRadius / 4.5;
            var N = new THREE.Geometry;
            N.vertices.push(new THREE.Vector3, new THREE.Vector3(T, 0, 0), new THREE.Vector3, new THREE.Vector3(0, T, 0), new THREE.Vector3, new THREE.Vector3(0, 0, T));
            N.colors.push(new THREE.Color(16711680), new THREE.Color(16711680), new THREE.Color(65280), new THREE.Color(65280), new THREE.Color(255), new THREE.Color(255));
            var C = new THREE.LineBasicMaterial({vertexColors: THREE.VertexColors});

            var axisHelper = new THREE.LineSegments(N, C);
            axisObject = new THREE.Object3D;
            axisObject.add(axisHelper);

            trackballObject.visible = false;
            axisObject.visible = false;
            // rotate the axis, so it fits to the rotated model!
            axisObject.rotateX(-Math.PI / 2);

            scene_globe.add(trackballObject);
            scene_axis.add(axisObject);

            //model_object.remove(simpleRotationMesh);

            // Helper objects should only be shown on left-click
            $('#webglcanvas').mousedown(function (event) {
                if (event.which === 1 || event.which === 3) {
                    trackballObject.visible = true;
                    axisObject.visible = true;
                    if (modelIsBig) {showSimpleRotationMesh();}
                    //model_object.add(simpleRotationMesh);
                }
            });

            $(document).mouseup(function () {
                if (trackballObject.visible || axisObject.visible) {
                    trackballObject.visible = false;
                    axisObject.visible = false;
                    if (modelIsBig) {hideSimpleRotationMesh();}
                    //model_object.add(current_mesh);
                    render(); // make sure trackball and axis are removed from display
                }
            });

        }

        // May be moved to another file & be more elegant than testing the user-agent
        // From http://stackoverflow.com/a/11381730
        var is_mobile_or_tablet = function () {
            var check = false;
            (function (a) {
                if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
            })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        };

        return init;
    }();

    publix.load = function () {
        var initialRotationDone = false;

        // initializes webgl if not done yet.
        // returns true if webgl is ok and file will be loaded.
        // executes callback with parameter "true" after load if init was successfull.
        // If init was not successfull, it does not execute the callback (caller should then call it with parameter "false")
        function load(file_version, callback) {
            if (!setup_done) {
                $('#webgl-loading').show();
                if (!publix.init()) {
                    return false;
                }
            }

            current_mesh && model_object.remove(current_mesh);
            current_wireframe && model_object.remove(current_wireframe);
            $('#webgl-loading').show();

            getModelMesh(file_version, function (mesh, wireframe) {
                current_mesh = mesh;
                current_wireframe = wireframe;

                model_object.add(current_mesh);
                model_object.add(current_wireframe);

                //compute optimal distance to camera
                var radius = current_mesh.geometry.boundingSphere.radius;
                dist = radius * 4;

                trackballObject.scale.x = radius;
                trackballObject.scale.y = radius;
                trackballObject.scale.z = radius;

                axisObject.scale.x = radius;
                axisObject.scale.y = radius;
                axisObject.scale.z = radius;

                camera_globe.near = camera.near;
                camera_globe.position.z = dist;
                camera_globe.far = camera_globe.position.distanceTo(mesh.position) + 0.001; //the globe should be cut in half through the far factor
                camera_globe.updateProjectionMatrix();
                controls.panSpeed = camera.right / (radius * 9.9);

                onWindowResize();
                render();
                animate();
                $('#webgl-loading').hide();
                console.log("End load " + file_version);

                if (!initialRotationDone) {
                    startInitialModelRotation();
                    initialRotationDone = true;
                }

                callback && callback(true);
            });

            return true;
        }

        function getModelMesh(file_version, callback) {
            if (models[file_version]) {
                // timeout to render DOM/webgl-loading
                setTimeout(function () {
                    callback(models[file_version].mesh, models[file_version].wireframe);
                }, 10);

            } else {
                loadModelFromServer(file_version, function (mesh, wireframe) {
                    models[file_version] = {mesh: mesh, wireframe: wireframe};
                    callback(mesh, wireframe);
                });
            }
        }

        function loadModelFromServer(file_version, callback) {

            var loader = new THREE.CTMLoader();
            if (file_version === "viewer_optimized"){
                try {
                    var simpleURL = '/u/' + document.getElementById('uuid').value + '/download_repaired_simple/';
                    loader.load(simpleURL, function(simpleGeo){
                        modelIsBig = true;
                        rotateMeshObject = new THREE.Object3D;
                        scene.add(rotateMeshObject);
                        model_object.visible = false;
                        rotateMeshObject.visible = true;

                        simpleGeo.applyMatrix(computeCenteringMatrix(simpleGeo));
                        simpleGeo.computeBoundingBox();
                        simpleGeo.computeBoundingSphere();

                        simpleGeo.computeFaceNormals();
                        simpleGeo.computeVertexNormals();
                        var simpleRotationMesh = new THREE.Mesh(simpleGeo, new THREE.MeshPhongMaterial({
                                wireframe: false,
                                reflectivity: 0,
                                color: 0xd4d4d4,
                                specular: 0x333333,
                                shininess: 20,
                                emissive: 0x000000,
                                shading: THREE.FlatShading
                            })
                        );
                        rotateMeshObject.add(simpleRotationMesh);
                        simpleRotationMesh.castShadow = true;
                        simpleRotationMesh.receiveShadow = true;
                    });
                } catch (e) {
                    modelIsBig = false;
                }
            }

            lock_counter = lock_counter + 1;
            var local_lock = lock_counter;
            var url = '/u/' + document.getElementById('uuid').value + '/download_' + file_version;
            /*$.get(url).done(function(data){

            });*/
            loader.load(url, function (geometry, texturefile) {
                // Cancel this callback if a more recent init occured.
                if (lock_counter !== local_lock) {
                    return;
                }

                var mesh, material;


                if (typeof texturefile !== "undefined") {
                    // Texture Color
                    if (file_version === "viewer_optimized") {
                        url = '/u/' + document.getElementById('uuid').value + "/download_texture_optimized";
                    } else {
                        url = '/u/' + document.getElementById('uuid').value + "/download_texture_original";
                    }
                    material = new THREE.MeshPhongMaterial({
                        wireframe: false,
                        reflectivity: 0,
                        map: THREE.ImageUtils.loadTexture(url),
                        color: 0xd4d4d4,
                        specular: 0x333333,
                        shininess: 20,
                        emissive: 0x000000
                    });
                } else if (typeof geometry.getAttribute("color") !== "undefined"){
                    // Vertex Color
                    material = new THREE.MeshPhongMaterial({
                        wireframe: false,
                        reflectivity: 0,
                        vertexColors: THREE.VertexColors,
                        specular: 0x333333,
                        shininess: 20,
                        emissive: 0x000000,
                        shading: THREE.FlatShading
                    });

                } else {
                    // No Color
                    material = new THREE.MeshPhongMaterial({
                        wireframe: false,
                        reflectivity: 0,
                        color: 0xd4d4d4,
                        specular: 0x333333,
                        shininess: 20,
                        emissive: 0x000000,
                        shading: THREE.FlatShading
                    });
                }

                geometry.applyMatrix(computeCenteringMatrix(geometry));
                geometry.computeBoundingBox();
                geometry.computeBoundingSphere();

                geometry.computeFaceNormals();
                geometry.computeVertexNormals();

                mesh = new THREE.Mesh(geometry, material);
                mesh.castShadow = true;
                mesh.receiveShadow = true;

                /*
                 wireframe on top of the mesh to show triangles
                 */
                //var wireframe = new THREE.WireframeHelper(mesh, 0xFFFFFF);
                var wireframe = new THREE.LineSegments(new THREE.WireframeGeometry(geometry.clone()), new THREE.LineBasicMaterial({color: 0xffffff}));
                wireframe.material.transparent = true;

                callback(mesh, wireframe);
            });

        }

        //rotate css cube and globe acording to the camera position
        function cssRotateCube() {
            var rm = new THREE.Mesh();
            rm.setRotationFromMatrix(camera.matrixWorldInverse);
            var angleX = THREE.Math.radToDeg(rm.rotation.x);
            var angleY = THREE.Math.radToDeg(rm.rotation.y);
            var angleZ = THREE.Math.radToDeg(rm.rotation.z);
            var elCube = document.getElementById('b3-cube-rotation');

            if (!elCube) {
                return;
            }

            var sRot = "rotateX(" + -angleX + "deg) " + "rotateY(" + angleY + "deg) rotateZ(" + -angleZ + "deg)";
            elCube.style["transform"] = sRot;
            elCube.style["-webkit-transform"] = sRot;

            var trackballGlobe = trackballObject.getObjectByName("TRACKBALLGLOBE");

            trackballGlobe.rotation.x = rm.rotation.x;
            trackballGlobe.rotation.y = rm.rotation.y;
            trackballGlobe.rotation.z = rm.rotation.z;
        }

        // See https://www.airtightinteractive.com/2015/01/building-a-60fps-webgl-game-on-mobile/
        // Search for "clock"
        var clock = new THREE.Clock();
        clock.start();

        function animate() {
            requestAnimationFrame(animate);

            TWEEN.update();

            if (current_mesh) {
                cssRotateCube();
                controls.update(clock.getDelta()); // Use clock to smoothen animation.
            }
        }

        //rotate the 3d model
        function startInitialModelRotation() {
            var position = {
                x: 270,
                t: (camera.top * 4),
                b: (camera.bottom * 4),
                r: (camera.right * 4),
                l: (camera.left * 4)
            }; // rotation on x axis

            new TWEEN.Tween(position)
                .to({
                    x: 45, t: (camera.top),
                    b: (camera.bottom),
                    r: (camera.right),
                    l: (camera.left)
                }, 3000)
                .easing(TWEEN.Easing.Exponential.Out)
                .onUpdate(function () {
                    camera.top = position.t;
                    camera.bottom = position.b;
                    camera.right = position.r;
                    camera.left = position.l;
                    camera.updateProjectionMatrix();

                    camera.position.x = dist * Math.cos((position.x * Math.PI) / 180);
                    camera.position.y = dist * Math.cos((45 * Math.PI) / 180);
                    camera.position.z = dist * Math.sin((position.x * Math.PI) / 180);
                })
                .onComplete(function(){
                    if (modelIsBig) {hideSimpleRotationMesh();}
                })
                .start();

        }

        return load;
    }();

    publix.camera = function () {

        //rotation (counter)clockwise; get camera 'right(/left)' and make it the new camera.up; tween camera.up for animation
        function rotateOnDisplayZ(direction) {
            var topv = new THREE.Vector3();
            var dir = camera.localToWorld(new THREE.Vector3(0, 0, 1));
            roundVector3(dir);
            dir.normalize();
            topv.crossVectors(dir, camera.up);
            topv.multiplyScalar(direction);
            var tween = new TWEEN.Tween(camera.up).to(
                {
                    x: topv.x,
                    y: topv.y,
                    z: topv.z
                }, 1000);
            tween.easing(TWEEN.Easing.Sinusoidal.Out);
            tween.onUpdate(function () {
                render();
            });

            tween.onStart(function(){
            if (modelIsBig) {showSimpleRotationMesh();}
            });

            tween.onComplete(function() {
            if (modelIsBig) {hideSimpleRotationMesh();}
            render();
            });


            tween.start();
        }

        //main function for camera positioning; camera gets moved to static positions representing different sides of the mesh
        function cR(side) {
            var idealUps;
            var px = 0;
            var py = 0;
            var pz = 0;
            switch (side) {
                case 'FRONT':
                    pz = dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 1, 0),
                        two: new THREE.Vector3(-1, 0, 0),
                        three: new THREE.Vector3(0, -1, 0),
                        four: new THREE.Vector3(1, 0, 0)
                    };
                    break;
                case 'BACK':
                    pz = -dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 1, 0),
                        two: new THREE.Vector3(-1, 0, 0),
                        three: new THREE.Vector3(0, -1, 0),
                        four: new THREE.Vector3(1, 0, 0)
                    };
                    break;
                case 'BOTTOM':
                    py = -dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 0, 1),
                        two: new THREE.Vector3(-1, 0, 0),
                        three: new THREE.Vector3(0, 0, -1),
                        four: new THREE.Vector3(1, 0, 0)
                    };
                    break;
                case 'TOP':
                    py = dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 0, 1),
                        two: new THREE.Vector3(-1, 0, 0),
                        three: new THREE.Vector3(0, 0, -1),
                        four: new THREE.Vector3(1, 0, 0)
                    };
                    break;
                case 'LEFT':
                    px = dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 1, 0),
                        two: new THREE.Vector3(0, 0, -1),
                        three: new THREE.Vector3(0, -1, 0),
                        four: new THREE.Vector3(0, 0, 1)
                    };
                    break;
                case 'RIGHT':
                    px = -dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 1, 0),
                        two: new THREE.Vector3(0, 0, -1),
                        three: new THREE.Vector3(0, -1, 0),
                        four: new THREE.Vector3(0, 0, 1)
                    };
                    break;
                default:
                    py = -dist;
                    idealUps = {
                        one: new THREE.Vector3(0, 0, 1),
                        two: new THREE.Vector3(-1, 0, 0),
                        three: new THREE.Vector3(0, 0, -1),
                        four: new THREE.Vector3(1, 0, 0)
                    };
                    break;
            }
            var goal = new THREE.Vector3(px, py, pz);
            var topv = findBestUp(idealUps, camera.up.clone(), goal);
            var ani_time = 1000;
            tweenCam({x: px, y: py, z: pz}, topv, ani_time);

        }

        function findBestUp(idealUp, newUp, goal) {
            // We first apply the rotation to the idealUp vectors
            var angle = goal.angleTo(camera.position);
            var axis = new THREE.Vector3();
            axis.crossVectors(goal, camera.position).normalize();

            // Then we calculate the angles between the idealUp vectors and the camera Up vector.
            var angle1 = idealUp.one.clone().applyAxisAngle(axis, angle).angleTo(newUp);
            var angle2 = idealUp.two.clone().applyAxisAngle(axis, angle).angleTo(newUp);
            var angle3 = idealUp.three.clone().applyAxisAngle(axis, angle).angleTo(newUp);
            var angle4 = idealUp.four.clone().applyAxisAngle(axis, angle).angleTo(newUp);

            var rvalue;

            // Then we choose the up vector with the smallest angle
            if (angle1 < angle2 && angle1 < angle3 && angle1 < angle4) {
                rvalue = idealUp.one;
            } else if (angle2 < angle1 && angle2 < angle3 && angle2 < angle4) {
                rvalue = idealUp.two;
            } else if (angle3 < angle1 && angle3 < angle2 && angle3 < angle4) {
                rvalue = idealUp.three;
            } else if (angle4 < angle1 && angle4 < angle2 && angle4 < angle3) {
                rvalue = idealUp.four;
            } else {
                //shouldn't be happening, if it does there is a problem somewhere
                rvalue = newUp;
            }
            return rvalue;
        }

        //main animation; tween camera to new position and adjust camera.up every step
        function tweenCam(position, up, time) {
            var values = {
                px: controls.object.position.x,
                py: controls.object.position.y,
                pz: controls.object.position.z,
                ux: controls.object.up.x,
                uy: controls.object.up.y,
                uz: controls.object.up.z,
                tx: controls.target.x,
                ty: controls.target.y,
                tz: controls.target.z
            };
            var tween = new TWEEN.Tween(values).to(
                {
                    px: position.x,
                    py: position.y,
                    pz: position.z,
                    ux: up.x,
                    uy: up.y,
                    uz: up.z,
                    tx: 0,
                    ty: 0,
                    tz: 0
                }, time);
            tween.easing(TWEEN.Easing.Sinusoidal.Out);
            tween.onUpdate(function () {
                controls.object.position.x = values.px;
                controls.object.position.y = values.py;
                controls.object.position.z = values.pz;
                controls.object.up.x = values.ux;
                controls.object.up.y = values.uy;
                controls.object.up.z = values.uz;
                controls.target.x = values.tx;
                controls.target.y = values.ty;
                controls.target.z = values.tz;
            });
            tween.onStart(function(){
            if (modelIsBig) {showSimpleRotationMesh();}
            });
            tween.onComplete(function() {
            if (modelIsBig) {hideSimpleRotationMesh();}
            });

            tween.start();
        }

        //floating number problems with the rotation vectors so we round them
        function roundVector3(vector) {
            var factor = 100000000000000;
            vector.x = basicDecimalRound(vector.x, factor);
            vector.y = basicDecimalRound(vector.y, factor);
            vector.z = basicDecimalRound(vector.z, factor);
        }

        /*
         simple function to round for decimals
         value: to be rounded number
         roundFactor: precision, i.e 100 for a hundredths
         roundFactor default: 100
         */
        function basicDecimalRound(value, roundFactor) {
            roundFactor = typeof roundFactor !== 'undefined' ? roundFactor : 100;
            value *= roundFactor;
            value = Math.round(value);
            value /= roundFactor;
            return value;
        }

        return {
            setPosition: cR,
            rotateOnDisplayZ: rotateOnDisplayZ
        };
    }();

    publix.fullscreen = function () {
        var is_fullscreen = false;

        function webgl_fullscreen() {

            if (show_signup) {
                //noinspection JSUnresolvedFunction
                Button3d.modals.showSignup();
                return;
            }

            modifyDisplayStyle('header', false);

            if ($('#userpanel-navbar') && $('#project-detail-header')) {
                modifyDisplayStyle('#userpanel-navbar', false);
                modifyDisplayStyle('#project-detail-header', false);
            }

            var browser_height = $(window).height();
            var new_height = browser_height - option_nav_height;
            is_fullscreen = true;

            if ($(window).width() > 991) {
                var top = '25%';
                if ($('.b3-webgl-ribbon').hasClass('projects'))
                    {top = '15%';}
                $('.b3-bar-viewer').css({
                    'position': 'absolute',
                    'top': top,
                    'height': 420
                });
            } else {
                $('.viewer-tools').removeClass("move_panel");
            }

            $('.viewer-tools').css('top', '0');
            $('.b3-3d-tools-panels').css('height', new_height);
            $('.b3-webgl-ribbon').animate({
                height: new_height
            }, 0, function () {
                onWindowResize();
            });


            $("html, body").animate({scrollTop: 0}, "slow");

            $('.svg-fullscreen').css('display', 'none');
            $('#3d-tools-fullscreen').css('display', 'none');
            $('.svg-fullscreen-exit').css('display', 'inline');
            $('#3d-tools-close-fullscreen').css('display', 'inline');

        }

        function webgl_close_fullscreen() {
            modifyDisplayStyle('header', true);

            if ($('#userpanel-navbar') && $('#project-detail-header')) {
                modifyDisplayStyle('#userpanel-navbar', 'block');
                modifyDisplayStyle('#project-detail-header', true);
            }

            is_fullscreen = false;

            if ($(window).width() > 991) {
                $('.b3-bar-viewer').css("position", "absolute").css("top", "0");
                $('.b3-3d-tools-panels').css('height', webgl_original_height);
            } else {
                $('.viewer-tools').removeClass("move_panel");
                $('.b3-3d-tools-panels').css('height', 'auto');
            }


            $('.b3-webgl-ribbon').animate({
                height: webgl_original_height
            }, 0, function () {
                onWindowResize();
            });

            $('.svg-fullscreen').css('display', 'inline');
            $('#3d-tools-fullscreen').css('display', 'inline');
            $('.svg-fullscreen-exit').css('display', 'none');
            $('#3d-tools-close-fullscreen').css('display', 'none');

        }

        function modifyDisplayStyle(elementSelector, shouldAppear) {
            var displayValue;
            if (shouldAppear === true) {
                displayValue = 'inline';
            } else if (shouldAppear === false) {
                displayValue = 'none';
            } else if (shouldAppear === 'block') {
                displayValue = 'block';
            }

            $(elementSelector).attr('style', 'display: ' + displayValue + ';');
        }

        function isEnabled() {
            return is_fullscreen;
        }

        return {
            enable: webgl_fullscreen,
            disable: webgl_close_fullscreen,
            isEnabled: isEnabled
        };
    }();

    publix.scaleLine = function () {
        /*
         below the functions for the scale
         don't ask how that works, but it seems it does
         projects a 2-d coordinate system into the 3d-world
         */
        var validValues = [.001, .005, .01, .05, .1, .5, 1, 5, 10, 50, 100, 250, 500, 1e3, 5e3, 1e4, 5e4, 1e5, 5e5];

        function getModelWorldViewportWidth() {
            var t = new THREE.Vector3(1, 1, .5);
            var n = new THREE.Vector3(-1, 1, .5);
            t.unproject(camera);
            n.unproject(camera);
            return n.distanceTo(t);
        }

        function getInchesPerPixel() {
            var mywebgl = document.getElementById('webgl');
            return getModelWorldViewportWidth() / mywebgl.clientWidth;
        }

        function getUnitOfMeasure(e) {
            var t = .001;
            var n = 0;
            while (validValues[n] <= e) {t = validValues[n++];}
            return t;
        }

        function update_scale_line() {
            if (!current_mesh) {
                return;
            }

            var mywebgl = document.getElementById('webgl');
            var e = getInchesPerPixel();
            e *= Button3d.common.scale.getScale();
            if ($('.b3-btn-unit-active').text() === 'in') {
                e /= 25.4;
            }
            var t = e * mywebgl.clientWidth * 0.4;
            t *= 25.4;


            var n = getUnitOfMeasure(t);
            var i = n / e;
            //i *= .03937; //if we want to show the scale in inch
            i = Math.round(i);
            i % 2 !== 1 && i++;
            $('#b3-webgl-scale-number').text(n / 100.0 + ' ' + $('.b3-btn-unit-active').text());
            $('#webgl-scale-line').width(i / 100.0);
        }

        return {update: update_scale_line};
    }();

    publix.render = render;
    function render() {
        publix.scaleLine.update();
        renderer.clear();
        renderer.render(scene, camera); //scene with mesh
        renderer.clearDepth();
        renderer.render(scene_globe, camera_globe); //scene with globe
        renderer.clearDepth();
        renderer.render(scene_axis, camera); //scene with axis
    }


    publix.computeCenteringMatrix = computeCenteringMatrix;
    function computeCenteringMatrix(geometry) {
        // move center of the models Bounding Box to the origin
        geometry.computeBoundingBox();

        console.log(geometry.boundingBox.min);
        console.log(geometry.boundingBox.max);

        var offset = new THREE.Vector3();
        offset.addVectors(geometry.boundingBox.min, geometry.boundingBox.max);
        offset.multiplyScalar(-0.5);
        var translation_matrix = new THREE.Matrix4().makeTranslation(offset.x, offset.y, offset.z);

        console.log(offset);

        // rotate the model
        var rotation_matrix = new THREE.Matrix4().makeRotationX(-Math.PI / 2);
        translation_matrix.premultiply(rotation_matrix);
        return translation_matrix;

    }

    publix.replaceMesh = replaceMesh;
    function replaceMesh(newMesh) {
        model_object.remove(current_mesh);
        model_object.add(newMesh);
        current_mesh = newMesh;
    }

    publix.replaceWireframe = replaceWireframe;
    function replaceWireframe(newWireframe) {
        model_object.remove(current_wireframe);
        model_object.add(newWireframe);
        current_wireframe = newWireframe;
    }

    publix.getCurrentMesh = function () {
        return current_mesh;
    };

    publix.getCurrentWireframe = function () {
        return current_wireframe;
    };

    publix.getScene = function () {
        return scene;
    };

    publix.getModelObject = function () {
        return model_object;
    };

    publix.invalidateCache = function (key) {
        delete models[key];
    };

    publix.onWindowResize = onWindowResize;
    function onWindowResize() {

        function initZoom() {
            zoom = 0.02;
        }

        if (typeof current_mesh === 'undefined') {
            return;
        }

        var mywebgl = document.getElementById('webgl');

        width = mywebgl.clientWidth;
        height = mywebgl.clientHeight;

        // update the camera
        var aspect_vector = new THREE.Vector2(width, height);
        aspect_vector.normalize();

        camera.left = current_mesh.geometry.boundingSphere.radius * -3 * aspect_vector.x;
        camera.right = current_mesh.geometry.boundingSphere.radius * 3 * aspect_vector.x;
        camera.top = current_mesh.geometry.boundingSphere.radius * 3 * aspect_vector.y;
        camera.bottom = current_mesh.geometry.boundingSphere.radius * -3 * aspect_vector.y;
        camera.far = camera.position.distanceTo(current_mesh.position) + current_mesh.geometry.boundingSphere.radius * 10;
        camera.updateProjectionMatrix();

        camera_globe.left = camera.left;
        camera_globe.right = camera.right;
        camera_globe.top = camera.top;
        camera_globe.bottom = camera.bottom;
        camera_globe.updateProjectionMatrix();

        renderer.setSize(width, height);


        initZoom();
        if (current_wireframe)
            {adjustWireframe();}

        if (publix.fullscreen.isEnabled()) {
            var browser_height = $(window).height();
            var new_height = browser_height - option_nav_height;

            $('.b3-webgl-ribbon').css('height', new_height);
        }

        render();

    }

    function showSimpleRotationMesh() {
        model_object.visible = false;
        rotateMeshObject.visible = true;
        render();
    }

    function hideSimpleRotationMesh() {
        model_object.visible = true;
        rotateMeshObject.visible = false;
        render();
    }


    /*
     This function sets the opacity of the wireframe dependant on
     - number of faces
     - zoom
     */
    function adjustWireframe() {
        var num_faces = current_wireframe.geometry.attributes.position.count;
        var avg_faces_per_px = (num_faces * zoom * zoom) / 20;
        var opacity = 1 - avg_faces_per_px;

        if (opacity < 0) {
            opacity = 0;
        }
        if (opacity > 1) {
            opacity = 1;
        }

        current_wireframe.material.opacity = opacity;
    }

    return publix;
}();
