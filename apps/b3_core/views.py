import typing as t
import button3d.type_declarations as td

import sys
from django.core.cache import cache
from django.http import HttpResponseForbidden
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.views.debug import ExceptionReporter

from apps.b3_address.models import Country
from apps.b3_core.utils import get_country
from apps.b3_signup.views import LoginView


class HomeView(LoginView):
    template_name = 'b3_core/home.html'

    def dispatch(self, request: td.Request, *args: list, **kwargs: dict) \
            -> td.DjangoResponse:
        if 'country' not in request.session:
            get_country(request)

        if request.user.is_authenticated:
            return redirect('b3_organization:dashboard')

        if request.site is None:
            return redirect('https://www.3yourmind.com')

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args: list, **kwargs: dict) \
            -> t.Dict[str, t.Any]:
        context = super().get_context_data(*args, **kwargs)
        context['view_name'] = 'home_pro_account'
        return context


def handler500(request: td.Request) -> td.DjangoResponse:
    if request.path.startswith('/v1/'):
        return JsonResponse({
            "error": ("Sorry, but the requested resource is unavailable "
                      "due to a server hiccup. Our support team has been "
                      "notified. Please check back later.")},
            status=500)
    return render(request, 'b3_core/errors/500.html')


def handler404(request: td.Request) -> td.DjangoResponse:
    return render(request, 'b3_core/errors/404.html')


def get_all_countries_view(request: td.Request) -> JsonResponse:
    cache_key = 'all-countries'
    all_countries = cache.get(cache_key)
    if not all_countries:
        all_countries = list(Country.objects.all().order_by('name'))
        cache.set(cache_key, all_countries)

    result = [
        {"iso": country.alpha2, "name": country.name}
        for country in all_countries
    ]

    return JsonResponse({"list": result})


def csrf_failure(request: td.Request, reason: str = "") \
        -> HttpResponseForbidden:
    return HttpResponseForbidden(
        get_template('b3_core/csrf_failure.html').render(request=request))


class TestException(Exception):
    pass


def exception_view(request: td.Request) -> None:
    try:
        raise TestException("This is a test exception that is raised "
                            "when opening /exception/. It can be used "
                            "in _tests and for checking sentry")
    except TestException:
        type, value, tb = sys.exc_info()

        # Simulate detailed error reporting (e.g. to sentry).
        # Sometimes this throws an exception itself.
        # Then the test test_exception_rendering() will fail.

        reporter = ExceptionReporter(request, type, value, tb)
        reporter.get_traceback_text()

        raise
