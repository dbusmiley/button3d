import button3d.type_declarations as td

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class DeletedDateManager(models.Manager):
    def get_queryset(self) -> td.QuerySet:
        return super(DeletedDateManager, self).get_queryset().filter(
            deleted_date=None)


class AbstractDeletedDateModel(models.Model):
    deleted_date = models.DateTimeField(
        _('Date deleted'),
        null=True,
        blank=True
    )

    objects = DeletedDateManager()
    all_objects = models.Manager()

    class Meta:
        abstract = True

    @property
    def is_deleted(self) -> bool:
        return self.deleted_date is not None

    def delete(self, **kwargs) -> None:
        if self.deleted_date:
            return
        self.deleted_date = timezone.now()
        self.save()
