import typing as t

from django.core.validators import RegexValidator
from django.db import models
from django.template import Template, Context
from django.template.backends.django import Template as DjangoTemplate
from django.template.exceptions import TemplateDoesNotExist

from apps.b3_pdf.renderer.base import TemplateInterface
from button3d import type_declarations as td

__all__ = (
    'EditableTemplateManager',
    'BaseEditableTemplate',
    'WrappedTemplate',
    'TemplateVarDescription',
)

varname_validator = RegexValidator(
    regex=r'^[A-Za-z][A-Za-z0-9_]+$',
    message='Variable names start with a letter and may only consist of '
            'letters, numbers and underscores.'
)


class WrappedTemplate(TemplateInterface):
    """
    Wraps a Django template to deal with implementation agnostic context
    """

    def __init__(self,
                 template_string: str=None, template_obj: DjangoTemplate=None):
        #: The actual Django template
        self.template, self.template_obj = None, None
        if template_string:
            self.template = Template(template_string)
        elif template_obj:
            self.template_obj = template_obj
        else:
            raise TypeError(f'{self.__class__} needs a template string or obj')

    def render(self, context: td.StringKeyDict):
        """
        Proxy the Django template's render method

        Transforms the dictionary into a Django context and passes it to the
        wrapped template.

        :param context: The variable name / value mapping. If a
                        `DjangoTemplate` was passed in at initialization,
                        this should contain a key `request` containing the
                        request object.
        :return: The result of the rendering operation.
        """
        if self.template:
            ctx = Context(dict_=context)
            return self.template.render(ctx)
        else:
            request = context.pop('request', None)
            return self.template_obj.render(context=context, request=request)


class TemplateVarDescription(models.Model):
    varname = models.CharField(
        max_length=64, verbose_name='variable name',
        validators=[varname_validator],
        help_text='Name of the variable in the template'
    )
    description = models.CharField(
        max_length=200, verbose_name='short description'
    )
    example_text = models.TextField(verbose_name='Example text')

    def __str__(self):
        return self.varname

    class Meta:
        verbose_name = 'template variable description'
        verbose_name_plural = 'template variable descriptions'


class EditableTemplateManager(models.Manager):
    def get_template(self, template_name: str, **filters):
        """
        Retrieve the prefered match for the given template name.

        Returns the first match with the highest priority sort order. This
        is the lowest number. Numbers shall not exceed
        :attr:`BaseEditableTemplate.FALLBACK_SORT_ORDER`.

        :param template_name: The name / path of the template
        :return: the template object with the highest priority
        """
        filters.update(template_name=template_name)
        template = self.filter(**filters).order_by('sort_order').first()
        if template:
            return template

        raise TemplateDoesNotExist(f'No template found for "{template_name}"')


class BaseEditableTemplate(models.Model):
    __doc__ = """
    An editable template base class

    An editable template is stored in the database. It still has a so called
    "path" to identify the template being searched for. In addition to that
    the base class is primed for a fallback mechanism of it's own. For that,
    the ``sort_order`` field is used and the specialized method
    :meth:`EditableTemplateManager.get_template`.

    At present the ``sort_order`` field is not editable. There is no use case
    in where an admin would override the sorting order, such as to allow a
    "partner" to override an existing "organization" template.

    In fact, this would probably create more hassle in terms of support
    requests, then it would solve a real world problem.
    """
    #: The fallback sort order. When changing this number, make sure to create
    #: a data migration that change the old to the new value.
    FALLBACK_SORT_ORDER = 99
    template_name = models.CharField(
        max_length=255, db_index=True, verbose_name='template name',
        help_text="A template path like 'b3_core/nav.html'",
    )
    template = models.TextField(
        verbose_name='template',
        help_text='The Django template.'
    )
    sort_order = models.PositiveSmallIntegerField(editable=False,
                                                  db_index=True)
    tpl_vars = models.ManyToManyField(
        TemplateVarDescription, related_name='templates',
        verbose_name='template variables'
    )

    def get_sort_order(self):
        """
        Retrieves the sorting order for templates with the same name.

        Should be overridden by concrete classes to implement custom
        sorting. A lower value means earlier in the list.

        :return: An integer indicating sort order.
        """
        return self.FALLBACK_SORT_ORDER

    def __str__(self):
        return self.template_name

    def get_template(self) -> WrappedTemplate:
        return WrappedTemplate(self.template)

    @property
    def context_description(self) -> t.Dict[str, t.Dict[str, str]]:
        """
        A convenience wrapper to quickly be able to serialize the variable
        descriptions for rendering.

        :return: A dictionary containing the variable name as key and it's
        description and example text as a fixed-key dictionary.
        """
        context = {}
        for var in self.tpl_vars.all():  # type: TemplateVarDescription
            context[var.varname] = {
                'description': var.description,
                'example': var.example_text,
            }

        return context

    def save(self, update_fields: t.Iterable[str]=None, **kwargs):
        old_val = self.sort_order
        self.sort_order = self.get_sort_order()
        has_changed = old_val != self.sort_order
        if has_changed and update_fields is not None:
            update_fields = set(update_fields)
            update_fields.add('sort_order')

        super().save(update_fields=update_fields, **kwargs)

    class Meta:
        abstract = True
