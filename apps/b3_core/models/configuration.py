from django.db import models
from django.utils.translation import ugettext_lazy as _


class Configuration(models.Model):
    MM_TO_INCH = 1/25.4

    MM, INCH = 'mm', 'in'
    UNITS = (
        (MM, _('Millimeter')),
        (INCH, _('Inch')),
    )
    unit = models.CharField(_('Unit of measurement'), default=MM, max_length=2,
                            choices=UNITS)
    scale = models.FloatField(_('Scale'), default=1.0)

    class Meta:
        db_table = "configuration"

    def update(self, *args: list, **kwargs: dict) -> bool:
        updated = False
        if 'scale' in kwargs:
            self.scale = kwargs['scale']
            updated = True
        if 'unit' in kwargs:
            self.unit = kwargs['unit']
            updated = True

        if updated:
            self.save()
        return updated

    def __str__(self) -> str:
        return 'Scale: {0} ({1})'.format(self.unit, self.scale)

    @property
    def scale_in_unit(self) -> float:
        scale: float = self.scale
        if self.unit == self.INCH:
            return scale * self.MM_TO_INCH
        return scale

    def clone(self) -> 'Configuration':
        cloned_config = Configuration.objects.get(pk=self.pk)
        cloned_config.id = None
        cloned_config.save()
        return cloned_config
