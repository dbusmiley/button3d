from django.db import models


class EstimatedTimeHistogram(models.Model):
    class Meta:
        db_table = "estimated_time_histogram"

    file_extension = models.CharField(max_length=255, db_column='extension')
    lower_bound_megabyte = models.FloatField(db_column='lower_bound_in_MB')
    upper_bound_megabyte = models.FloatField(db_column='upper_bound_in_MB')
    duration_in_milliseconds = models.FloatField(db_column='time_in_ms')
    events_handled = models.IntegerField(db_column='number_of_values')
