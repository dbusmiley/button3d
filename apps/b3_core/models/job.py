from django.db import models
from django.utils import timezone

from apps.b3_core.models import StlFile


class Job(models.Model):
    uuid = models.CharField(max_length=36)
    DTYPE = models.CharField(max_length=255)
    error = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    creation_date = models.DateTimeField(default=timezone.now)
    job_duration = models.BigIntegerField(blank=True, null=True, default=None)
    stl_file = models.ForeignKey(StlFile, null=True, related_name="jobs")

    class Meta:
        db_table = "job"
