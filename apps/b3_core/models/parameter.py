import typing as t

from django.db import models


class Parameter(models.Model):
    """ Model specific parameters
    """
    max_scale = models.FloatField(default=10.)
    area = models.FloatField(default=10.)
    volume = models.FloatField(default=10.)
    machine_volume = models.FloatField(null=True)
    h = models.FloatField(default=10.)
    w = models.FloatField(default=10.)
    d = models.FloatField(default=10.)
    faces = models.IntegerField(default=0)
    shells = models.IntegerField(null=True)
    holes = models.IntegerField(null=True)
    inverted_normals = models.IntegerField(null=True)
    duplicated_vertices = models.IntegerField(null=True)
    face_intersections = models.IntegerField(null=True)

    class Meta:
        db_table = "parameter"

    def get_volume(self, scale: float) -> float:
        scale = float(scale)
        return self.volume * scale * scale * scale

    def get_area(self, scale: float) -> float:
        scale = float(scale)
        return self.area * scale * scale

    def get_bb_volume(self, scale: float) -> float:
        scale = float(scale)
        return self.h * self.w * self.d * scale * scale * scale

    def get_bb_width(self, scale: float) -> float:
        scale = float(scale)
        return self.w * scale

    def get_bb_height(self, scale: float) -> float:
        scale = float(scale)
        return self.h * scale

    def get_bb_depth(self, scale: float) -> float:
        scale = float(scale)
        return self.d * scale

    def get_bounds(self, scale: float) -> t.List[float]:
        scale = float(scale)
        dim_x_mm = self.w * scale
        dim_y_mm = self.d * scale
        dim_z_mm = self.h * scale
        return [dim_x_mm, dim_y_mm, dim_z_mm]

    def get_machine_volume(self, scale) -> float:
        scale = float(scale)
        if self.machine_volume is not None:
            return self.machine_volume * scale * scale * scale
        else:
            return self.get_bb_volume(scale)
