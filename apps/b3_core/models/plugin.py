from django.db import models
from django.forms import ModelForm


# Not used
class PluginRequest(models.Model):
    email = models.EmailField(max_length=254)
    software = models.CharField(max_length=254)
    time = models.DateTimeField(auto_now_add=True, blank=True)
    ip = models.GenericIPAddressField()

    def __str__(self) -> str:
        return "Request from %s: %s" % (self.email, self.software)

    class Meta:
        db_table = "plugin_request"


class PluginRequestForm(ModelForm):
    class Meta:
        model = PluginRequest
        fields = ['email', 'software']
