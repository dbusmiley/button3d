import json
import logging
import typing as t
import urllib.request

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.managers import StlFileManager
from apps.b3_core.models import Parameter, Configuration, \
    AbstractDeletedDateModel
from apps.b3_core.supportstructure import SupportStructure
from apps.b3_core.utils import cache_result_on_instance
from apps.b3_organization.models import AbstractSiteModel
from apps.b3_organization.utils import get_current_site, get_base_url

logger = logging.getLogger(__name__)


class StlFile(AbstractSiteModel, AbstractDeletedDateModel):
    uuid = models.CharField(max_length=36, unique=True, db_index=True)

    showname = models.CharField(max_length=200)
    filetype = models.CharField(max_length=10, default="stl")

    creation_date = models.DateTimeField(
        db_column='creation_date',
        default=timezone.now
    )
    origin = models.CharField(max_length=200, default="")

    multicolor = models.BooleanField(default=False)

    o_parameter = models.ForeignKey(Parameter, null=True, related_name='+')
    r_parameter = models.ForeignKey(Parameter, null=True, related_name='+')
    parameter = models.ForeignKey(Parameter, null=True, related_name='+')

    orientation_x = models.FloatField(default=0)
    orientation_y = models.FloatField(default=0)
    orientation_z = models.FloatField(default=0)

    is_opened = models.BooleanField(default=False)

    ip_address = models.CharField(
        max_length=39,
        null=True,
        blank=True,
        default=None
    )

    owner = models.ForeignKey(
        User,
        related_name='stlfiles',
        null=True,
        blank=True,
        verbose_name=_("Owner")
    )

    is_api_upload = models.BooleanField(default=False)

    objects = StlFileManager()

    # this manager is required to get ALL files regardless of current site.
    # Is used (at leat once) in b3_api.healthcheck.views.StlFileCheck
    all_objects = models.Manager()

    printable_suspicious_scale = models.FloatField(blank=True, null=True)
    suspicious_thinwalls_scale = models.FloatField(blank=True, null=True)
    optimum_gap_scale = models.FloatField(blank=True, null=True)
    minimum_gap_scale = models.FloatField(blank=True, null=True)
    is_successfully_optimized = models.NullBooleanField()
    is_wta_job_too_complex = models.BooleanField(default=False)
    texture_name = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        default=None
    )

    def get_printability_info(self, wall_min, scale):
        """
        returns dict with printable_scale, suspicious_scale,
            printablility (and printablility_problem, if present)
        must not be called before get_job_status()["status"] == "finished",
            since it only _tests for multiple shells and thin walls
        """
        if self.r_parameter is None:
            # TODO diagnose deeper why this is happening
            return {"printability": "unknown"}

        if self.r_parameter.shells > 1:
            return {"printability": "not_printable",
                    "printability_problem": "E_MULTIPLE_SHELLS"}

        if self.is_wta_job_too_complex:
            # wta job failed because model is too complex
            return {"printability": "unknown",
                    "printability_problem": "E_TOO_MANY_FACES"}

        if not (self.printable_suspicious_scale and
                self.suspicious_thinwalls_scale):
            # This means that WTAJob is not finished yet.
            # This method should not have been called in this case!
            return {"printability": "unknown"}

        info = {
            "printable_scale": self.printable_suspicious_scale * wall_min,
            "suspicious_scale": self.suspicious_thinwalls_scale * wall_min
        }

        if scale < self.suspicious_thinwalls_scale * wall_min:
            info["printability"] = "not_printable"
            info["printability_problem"] = "E_THIN_WALLS"

        elif scale < self.printable_suspicious_scale * wall_min:
            info["printability"] = "warning"

        else:
            info["printability"] = "printable"

        return info

    def can_view(self, request):
        if self.is_demo:
            return True
        if not request.user.is_anonymous and \
                self.is_in_partner_order(request):
            return True
        if self.is_deleted:
            return False
        if self.is_mine(request):
            return True
        if request.user.is_staff:
            return True
        if self.is_pricing_user_of_a_basket(request.user):
            return True
        if self.belongs_to_basket_that_user_can_view(request):
            return True
        return False

    def can_order(self, request):
        if self.is_demo:
            return True
        if self.is_deleted:
            return False
        if self.is_mine(request):
            return True
        if request.user.is_staff:
            return True
        if self.belongs_to_basket_that_user_can_view(request):
            return True
        return False

    def can_download(self, request):
        if self.is_demo:
            return True
        if not request.user.is_anonymous and \
                self.is_in_partner_order(request):
            return True
        if self.is_deleted:
            return False
        if request.user.is_staff:
            return True
        if self.origin == 'meshify.dk':
            return False
        if self.is_mine(request):
            return True
        if self.is_pricing_user_of_a_basket(request.user):
            return True
        if self.belongs_to_basket_that_user_can_view(request):
            return True
        return False

    def get_lines_with_this_file(self):
        return self.basket_lines.select_related('basket').all()

    def get_baskets_with_this_file(self):
        lines_with_this_file = self.get_lines_with_this_file()
        return [line.basket for line in lines_with_this_file]

    def belongs_to_basket_that_user_can_view(self, request):
        baskets_with_this_file = self.get_baskets_with_this_file()
        return any(
            basket.can_user_view(request.user, request)
            for basket in baskets_with_this_file
        )

    def belongs_to_only_ordered_baskets(self):
        baskets_with_this_file = self.get_baskets_with_this_file()
        if not baskets_with_this_file:
            return False

        return all(
            basket.is_submitted for basket in baskets_with_this_file
        )

    def is_pricing_user_of_a_basket(self, pricer):
        """Tests if the user is the pricer of a
        basket needing price containing this file
        TODO: this is not exact and can contain error
        (even if few only, because the pricer needs the uuid)
        """
        from apps.basket.models import Basket
        manual_priced_baskets = Basket.objects.filter(
            pricing_status=Basket.WAITING_FOR_MANUAL_PRICING,
        )
        filtered_baskets_by_uuid = [
            basket for basket in manual_priced_baskets if any(
                line.uuid == self.uuid for line in basket.lines.all()
            )
        ]
        return any(
            basket.can_user_view(user=pricer)
            for basket in filtered_baskets_by_uuid
        )

    def is_in_partner_order(self, request):
        # TODO(felix) Test or remove this
        return request.user.partners.filter(
            orders__lines__stl_file=self
        ).exists()

    def can_delete(self, request):
        if self.is_demo:
            return False
        if self.is_deleted:
            return False
        if request.user.is_staff:
            return True
        return self.is_mine(request)

    def can_edit(self, request):
        if self.is_demo:
            return False
        if request.user.is_staff:
            return True
        return self.is_mine(request)

    def set_rotation(self, x, y, z):
        try:
            angles = [float(angle) for angle in [x, y, z]]
        except BaseException:
            return

        if all([-360 < angle < 360 for angle in angles]):
            rotation = [
                angles[0] - self.orientation_x,
                angles[1] - self.orientation_y,
                angles[2] - self.orientation_z
            ]
            if any(rotation):
                # save old orientation,
                # in case that the backend can't be reached
                old_values = [
                    self.orientation_x,
                    self.orientation_y,
                    self.orientation_z
                ]

                # store new orientation in database
                self.orientation_x = angles[0]
                self.orientation_y = angles[1]
                self.orientation_z = angles[2]
                self.save()

                # request orientation job (rotating the STL) from backend
                url = '{0}/job'.format(get_base_url())
                data = {"jobtype": "orientation", "file": self.uuid}
                request = urllib.request.Request(
                    url, data=json.dumps(data).encode(),
                    headers={'Content-type': 'application/json'})
                try:
                    json_data = urllib.request.urlopen(request)  # nosec
                    json_data = json_data.read()
                    json_data = json_data.decode()
                    json.loads(json_data)
                    # fixme: value of response is not used!
                except BaseException:
                    # If backend does not respond, revert old values
                    (
                        self.orientation_x,
                        self.orientation_y,
                        self.orientation_z
                    ) = old_values
                    self.save()

    def get_rotation(self):
        return [self.orientation_x, self.orientation_y, self.orientation_z]

    def is_mine(self, request):
        if request.user.is_anonymous:
            if not self.owner and \
                    (not self.is_opened or
                        self.uuid in self.list_pending_stlfiles(request)):
                return True
        else:
            if self.owner_id == request.user.id or \
                    not self.owner_id and \
                    (not self.is_opened or
                     self.uuid in self.list_pending_stlfiles(request)):
                return True
        return False

    def assign_to_user_in_request(self, request):
        # if not request.user.is_anonymous:
        #    StlFile.clean_pending_stlfiles(request)

        if not self.owner and not self.is_opened:
            if request.user.is_anonymous:
                self.put_to_pending_stlfiles(request)
                self.site = get_current_site(request)
            else:
                self.owner = request.user
                self.site = request.user.userprofile.site

            self.is_opened = True
            self.save()

    def put_to_pending_stlfiles(self, request):
        request.session['pending_stlfiles'] = json.dumps(
            json.loads(
                request.session.get('pending_stlfiles', "[]")) + [self.uuid])

    @staticmethod
    def clean_pending_stlfiles(request):
        if 'pending_stlfiles' in request.session:
            del request.session['pending_stlfiles']

    @staticmethod
    def list_pending_stlfiles(request):
        return json.loads(request.session.get('pending_stlfiles', "[]"))

    def delete(self, **kwargs):
        super(StlFile, self).delete()
        # Delete related project lines that are not ordered
        for line in self.basket_lines.all():
            line.delete()

    def __str__(self):
        return "StlFile: %s (%s)" % (self.showname, self.uuid)

    class Meta:
        db_table = "stl_file"

    def get_parameter(self) -> Parameter:
        return self.parameter

    def get_filetype(self):
        return self.filetype

    def get_scaled_dimensions(self, scale=1) -> t.Dict[str, float]:
        scale = float(scale)
        parameter = self.get_parameter()
        dimensions = {
            'w': parameter.w * scale,
            'h': parameter.h * scale,
            'd': parameter.d * scale,
        }
        return dimensions

    def get_original_filename(self):
        return "originalFile." + self.filetype.lower()

    def get_optimized_filename(self):
        if self.multicolor:
            return 'model.zip'
        return 'repaired.stl'

    def get_extension(self):
        if self.multicolor:
            return 'zip'
        return 'stl'

    def clean(self, *args, **kwargs):
        if self.is_deleted:
            raise ValidationError('StlFile is deleted.')
        super(StlFile, self).clean(*args, **kwargs)

    @property
    def is_demo(self):
        return self.uuid == settings.DEMO_UUID

    @property
    def download_optimized_url(self):
        return reverse(
            'short_uploads_views:download_optimized',
            kwargs={"uuid": self.uuid})

    @property
    def download_original_url(self):
        return reverse(
            'short_uploads_views:download_original',
            kwargs={"uuid": self.uuid})

    @property
    def thumbnail_url(self):
        return reverse(
            'short_uploads_views:download_thumbnail',
            kwargs={"uuid": self.uuid})

    @property
    def REST_url(self):
        return reverse(
            'short_uploads_views:upload', kwargs={"uuid": self.uuid})

    @property
    def share_url(self):
        if self.is_demo:
            return get_base_url() + reverse('demo')
        else:
            return get_base_url() + reverse(
                'short_uploads', kwargs={"uuid": self.uuid})

    @property
    def support_structure(self):
        if not hasattr(self, '_support_structure'):
            setattr(self, '_support_structure', SupportStructure(self))
        return self._support_structure

    @property
    def all_jobs_finished(self) -> bool:
        job_status = self.get_job_status()['status']
        return job_status == 'finished'

    @cache_result_on_instance
    def get_job_status(self):
        """
        returns dict with status (and error if status == "error")
        """
        jobs = self.jobs.all()
        result = {}

        if all([job.status == "FINISHED" for job in jobs]):
            status = "finished"
        elif any([job.status == "FAILED" for job in jobs]):
            status = "error"
        else:
            status = "analysing"
            unfinished_jobs = [job for job in jobs
                               if job.status in ["PENDING", "PROCESSING"]]
            if any([job.DTYPE == "CONVERT" for job in unfinished_jobs]):
                analysing_detail = "converting"
            elif any([job.DTYPE == "REPAIR" for job in unfinished_jobs]):
                analysing_detail = "optimising"
            else:
                analysing_detail = "analysing"
            result["analysing_detail"] = analysing_detail
        result["status"] = status

        if status == "error":
            # Only some errors which are written to the
            # database by the backend are "readable" error codes.
            # If none of the errors is readable, return "E_INTERNAL_ERROR"
            readable_errors = [
                "E_EMPTY", "E_TOO_LARGE", "E_INVALID_FILE",
                "E_JSON_FORMAT", "E_INVALID_UUID",
                "E_INVALID_PARAM", "E_TIMEOUT", "E_STL_CONVERSION",
                "E_WRL_CONVERSION", "E_CTM_CONVERSION",
                "E_PLY_CONVERSION", "E_ZIP_NOT_SUPPORTED"]
            readable_error_jobs = list(jobs.filter(
                status="FAILED", error__in=readable_errors))
            result["error"] = readable_error_jobs[0].error \
                if readable_error_jobs else "E_INTERNAL_ERROR"

        return result

    def get_printability_status(self, wall_min, scale):
        """
        returns dict with status, printability and additional info
        see https://sites.google.com/a/
            3yd.de/3yd-wiki/home/departments/developer/status-and-printability
        """
        # For easy debugging:
        # return {"status":"analysing", "printability":"unknown"}
        # return {"status":"error", "error":"E_EMPTY"}
        # return {"status":"finished", "printability":"unknown",
        #   "printability_problem":"E_TOO_MANY_FACES"}

        status_info = self.get_job_status()

        if status_info["status"] != "finished":
            # status is analyzing or error
            status_info["printability"] = "unknown"
        else:
            # all jobs finished successfully, printability is known
            status_info.update(self.get_printability_info(wall_min, scale))

        return status_info

    def latest_belonging_line(self, request):
        from apps.basket.models import Line
        user = request.user
        lines = None
        if user.is_authenticated:
            lines = [l for l in Line.objects.filter(
                basket__owner=user).order_by('-date_created')
                if l.uuid == self.uuid]
        elif hasattr(request, 'basket'):  # Anonymous user & has basket
            lines = [l for l in request.basket.lines.all()
                     if l.uuid == self.uuid]

        return lines[0] if lines else None

    def to_dict(self, request):  # this is HARDCODE, TODO
        email = 'Anonymous'
        if hasattr(self, 'owner'):
            if hasattr(self.owner, 'email'):
                email = self.owner.email
        return {
            "uuid": self.uuid,
            "owner": email,
            "showname": self.showname,
            "filetype": self.filetype,
            "scale": 1,
            "unit": Configuration.MM,
            "status": self.get_job_status(),
            "creation_date": self.creation_date,
            "multicolor": self.multicolor,
            "thumbnail_url": self.thumbnail_url,
            "can_view": self.can_view(request),
            "can_order": self.can_order(request),
            "can_download": self.can_download(request),
            "can_delete": self.can_delete(request),
            "can_edit": self.can_edit(request),
            "download_optimized_url": self.download_optimized_url,
            "download_original_url": self.download_original_url,
            "REST_url": self.REST_url,
            "share_url": self.share_url,
            "is_demo": self.is_demo,
            "parameter": {
                "h": self.parameter.h,
                "w": self.parameter.w,
                "d": self.parameter.d,
                "max_scale": self.parameter.max_scale,
                "area": self.parameter.area,
                "volume": self.parameter.volume,
                "faces": self.parameter.faces,
                "shells": self.parameter.shells,
                "holes": self.parameter.holes,
            } if self.parameter else None,
            "rotation": [
                self.orientation_x,
                self.orientation_y,
                self.orientation_z]
        }


class SpecialPermissions(models.Model):
    uuid = models.CharField(max_length=36, null=True)  # just for django

    class Meta:
        permissions = (
            ("invoice_for_all_ps_enabled",
                "Can order from each printing service by invoice"),
        )
