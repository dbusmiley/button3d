from celery import task
from django.core.mail import EmailMessage


@task(ignore_result=True)
def send_email(email_message: EmailMessage) -> None:
    """
    Celery task, that sends EmailMessage. Used for sending emails
    asynchronously
    :param email_message:
    :return:
    """
    email_message.send(fail_silently=False)
