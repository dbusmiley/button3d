from django.http import HttpResponseBadRequest, HttpResponseNotFound, \
    HttpResponseForbidden, JsonResponse

from apps.b3_api.utils.helpers import underscore_dict_to_camelcase


class JsonHttpResponseBadRequest(HttpResponseBadRequest, JsonResponse):
    pass


class JsonHttpResponseUnauthorized(JsonResponse):
    status_code = 401


class JsonHttpResponseForbidden(HttpResponseForbidden, JsonResponse):
    pass


class JsonHttpResponseNotFound(HttpResponseNotFound, JsonResponse):
    pass


class ErrorResponseFactory:
    """ Linked to our error handling:
    http://deepthought.3yourmind.com/general/Technical/guides-error-handling/
    """

    ERROR_CLASSES = {
        '400': JsonHttpResponseBadRequest,
        '401': JsonHttpResponseUnauthorized,
        '403': JsonHttpResponseForbidden,
        '404': JsonHttpResponseNotFound
    }

    def __init__(self, module=None):
        self.module = '{}_'.format(module) if module else ''

    def make(self, status_code, error_code, message=None, **kwargs):
        httpresponse_class = self.ERROR_CLASSES.get(str(status_code))
        if not httpresponse_class:
            raise ValueError('Invalid error status code')

        error_data = {
            'code': '{}{}'.format(self.module, error_code),
            'message': message if message else '',
            'moreInfo': underscore_dict_to_camelcase(kwargs)
        }

        return httpresponse_class(error_data)

    def make_badrequest(self, error_code, message=None, **kwargs):
        return self.make(400, error_code, message, **kwargs)

    def make_unauthorized(self, error_code, message=None, **kwargs):
        return self.make(401, error_code, message, **kwargs)

    def make_forbidden(self, error_code, message=None, **kwargs):
        return self.make(403, error_code, message, **kwargs)

    def make_notfound(self, error_code, message=None, **kwargs):
        return self.make(404, error_code, message, **kwargs)
