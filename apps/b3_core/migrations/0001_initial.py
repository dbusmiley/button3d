# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-09 10:58


import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sites', '0002_alter_domain_unique'),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unit', models.CharField(choices=[('mm', 'Millimeter'), ('in', 'Inch')], default='mm', max_length=2, verbose_name='Unit of measurement')),
                ('scale', models.FloatField(default=1.0, verbose_name='Scale')),
            ],
            options={
                'db_table': 'configuration',
            },
        ),
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.CharField(max_length=36)),
                ('DTYPE', models.CharField(max_length=255)),
                ('error', models.CharField(max_length=255)),
                ('status', models.CharField(max_length=255)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('job_duration', models.BigIntegerField(blank=True, default=None, null=True)),
            ],
            options={
                'db_table': 'job',
            },
        ),
        migrations.CreateModel(
            name='Parameter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('max_scale', models.FloatField(default=10.0)),
                ('area', models.FloatField(default=10.0)),
                ('volume', models.FloatField(default=10.0)),
                ('machine_volume', models.FloatField(null=True)),
                ('h', models.FloatField(default=10.0)),
                ('w', models.FloatField(default=10.0)),
                ('d', models.FloatField(default=10.0)),
                ('faces', models.IntegerField(default=0)),
                ('shells', models.IntegerField(null=True)),
                ('holes', models.IntegerField(null=True)),
            ],
            options={
                'db_table': 'parameter',
            },
        ),
        migrations.CreateModel(
            name='PluginRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('software', models.CharField(max_length=254)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('ip', models.GenericIPAddressField()),
            ],
            options={
                'db_table': 'plugin_request',
            },
        ),
        migrations.CreateModel(
            name='SpecialPermissions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.CharField(max_length=36, null=True)),
            ],
            options={
                'permissions': (('invoice_for_all_ps_enabled', 'Can order from each printing service by invoice'),),
            },
        ),
        migrations.CreateModel(
            name='StlFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted_date', models.DateTimeField(blank=True, null=True, verbose_name='Date deleted')),
                ('uuid', models.CharField(db_index=True, max_length=36, unique=True)),
                ('showname', models.CharField(max_length=200)),
                ('filetype', models.CharField(default='stl', max_length=10)),
                ('creation_date', models.DateTimeField(blank=True, db_column='creation_date', default=django.utils.timezone.now)),
                ('origin', models.CharField(default='', max_length=200)),
                ('multicolor', models.BooleanField(default=False)),
                ('orientation_x', models.FloatField(default=0)),
                ('orientation_y', models.FloatField(default=0)),
                ('orientation_z', models.FloatField(default=0)),
                ('is_opened', models.BooleanField(default=False)),
                ('ip_address', models.CharField(blank=True, default=None, max_length=39, null=True)),
                ('is_api_upload', models.BooleanField(default=False)),
                ('public_can_view', models.BooleanField(default=False)),
                ('public_can_order', models.BooleanField(default=False)),
                ('public_can_download', models.BooleanField(default=False)),
                ('company_can_view', models.BooleanField(default=False)),
                ('company_can_order', models.BooleanField(default=False)),
                ('company_can_download', models.BooleanField(default=False)),
                ('printable_suspicious_scale', models.FloatField(blank=True, null=True)),
                ('suspicious_thinwalls_scale', models.FloatField(blank=True, null=True)),
                ('is_wta_job_too_complex', models.BooleanField(default=False)),
                ('o_parameter', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='b3_core.Parameter')),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='stlfiles', to=settings.AUTH_USER_MODEL, verbose_name='Owner')),
                ('parameter', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='b3_core.Parameter')),
                ('r_parameter', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='b3_core.Parameter')),
                ('site', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'db_table': 'stl_file',
            },
        ),
        migrations.AddField(
            model_name='job',
            name='stl_file',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='jobs', to='b3_core.StlFile'),
        ),
    ]
