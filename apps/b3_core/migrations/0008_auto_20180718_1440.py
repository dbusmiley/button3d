# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-18 14:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_core', '0007_auto_20180528_0937'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stlfile',
            name='company_can_download',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='stlfile',
            name='company_can_order',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='stlfile',
            name='company_can_view',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='stlfile',
            name='public_can_download',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='stlfile',
            name='public_can_order',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='stlfile',
            name='public_can_view',
            field=models.NullBooleanField(default=False),
        ),
    ]
