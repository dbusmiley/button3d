from django.db.models import Q

from apps.b3_core.models import DeletedDateManager
from apps.b3_organization.models import CurrentSiteManager


class StlFileManager(CurrentSiteManager, DeletedDateManager):
    def get_queryset(self):
        return super(StlFileManager, self).get_queryset().select_related(
            'o_parameter', 'r_parameter', 'owner')

    def get_own_files(self, user):
        own_stl_files = self.filter(
            Q(deleted_date=None, owner_id=user.id, o_parameter__isnull=False))

        # merge own and company stl files and sort them
        return own_stl_files.order_by('-creation_date')
