from django.contrib import admin

from apps.b3_core.models import StlFile, Configuration


def allow_selection(modeladmin, request, queryset):
    """Dummy action to allow PM team to select items for highlighting"""
    pass


allow_selection.name = "Allow Selection"
allow_selection.short_description = " "


class DeletedDateAdmin(admin.ModelAdmin):
    """This admin removes the possibility to delete
    objects by selecting multiple ones on the list.
    Because doing this actually calls QuerySet.delete()
    which does not call individually the delete()
    method. Meaning the objects get deleted from DB
    instead of setting deleted_date.
    A dummy action is re-added to allow selecting models for
    demonstrations, screenshots etc
    """
    actions = [allow_selection]

    def get_actions(self, request):
        actions = super(DeletedDateAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class StlFileAdmin(DeletedDateAdmin):
    def get_queryset(self, request):
        return StlFile.all_objects.all()

    readonly_fields = ('o_parameter', 'r_parameter', 'parameter')
    list_display = ('showname', 'site')


admin.site.register(StlFile, StlFileAdmin)


@admin.register(Configuration)
class ConfigurationAdmin(admin.ModelAdmin):
    pass
