from django.contrib.admin.widgets import AdminFileWidget
from django.forms import FileInput, HiddenInput, forms


class ReadOnlyFieldsMixin(forms.Form):
    readonly_fields = ()

    def __init__(self, *args: list, **kwargs: dict) -> None:
        super().__init__(*args, **kwargs)
        for field in self.fields.values():
            field.widget.attrs['disabled'] = 'true'
            field.required = False

    def clean(self) -> dict:
        cleaned_data = super().clean()
        field: str
        for field in self.readonly_fields:
            cleaned_data[field] = getattr(self.instance, field)

        return cleaned_data


class RequiredFieldsMixin(forms.Form):
    def __init__(self, *args: list, **kwargs: dict) -> None:
        super().__init__(*args, **kwargs)
        for field in self.fields:
            if (self.fields[field].required and
                type(self.fields[field].widget) not in
                    (AdminFileWidget, HiddenInput, FileInput) and
                    '__prefix__' not in self.fields[field].widget.attrs):

                self.fields[field].widget.attrs['required'] = 'required'
                if self.fields[field].label:
                    self.fields[field].label += ' *'
