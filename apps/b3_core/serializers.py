from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.utils.file_urls import generate_file_urls
from apps.b3_core.models.stlfile import StlFile


class StlFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = StlFile
        fields = ('uuid', 'origin')


class PartStlFileSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    """
    StlFile serializer for use in :serializer: `PartDetailSerializer`
    """

    name = serializers.CharField(source='showname')
    created = serializers.DateTimeField(source='creation_date')
    thumbnail_url = serializers.CharField()
    download_links = serializers.SerializerMethodField()

    def get_download_links(self, obj):
        return generate_file_urls(
            request=self.context.get('request'), stl_file=obj
        )

    class Meta:
        model = StlFile
        fields = ('name', 'created', 'thumbnail_url', 'uuid', 'download_links')
        read_only_fields = (
            'name',
            'created',
            'thumbnail_url',
            'uuid',
            'download_links',
        )
