class RedirectException(Exception):
    def __init__(self, url: str) -> None:
        super(RedirectException, self).__init__(f'Redirect to {url}')
        self.url = url


class JSONBadRequest(Exception):
    pass


class CurrencyAPIError(Exception):
    pass
