import json
import logging

from django.core.cache import cache
from django.db.models.fields.files import ImageFieldFile, FileField
from django.template.loader import render_to_string
from django.test import RequestFactory, override_settings

from apps.b3_address.factories import CountryFactory
from apps.b3_core.mail import fill_email_context
from apps.b3_core.utils import get_country_from_ip, currency_exchange

from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_core.views import get_all_countries_view, TestException


class CoreTests(TestCase):
    def setUp(self):
        super().setUp()
        CountryFactory(alpha2='DE')
        CountryFactory(alpha2='US')
        CountryFactory(alpha2='ES')

    def test_countries_cached(self):
        cache.set('all-countries', None)

        request = RequestFactory().get('/')
        request.session = {}

        with self.assertNumQueries(1):
            r1 = get_all_countries_view(request)
            response_dict = json.loads(r1.content)['list']
            self.assertEqual(len(response_dict), 3)

            r2 = get_all_countries_view(request)
            response_dict = json.loads(r2.content)['list']
            self.assertEqual(len(response_dict), 3)

    def test_get_all_countries(self):
        cache.set('all-countries', None)

        request = RequestFactory().get('/')
        request.session = {}

        r = get_all_countries_view(request)
        response_dict = json.loads(r.content)['list']
        self.assertEqual(response_dict[0]['iso'], 'DE')
        self.assertEqual(response_dict[0]['name'], 'Germany')
        self.assertEqual(response_dict[1]['iso'], 'ES')
        self.assertEqual(response_dict[1]['name'], 'Spain')

    @override_settings(ENABLE_IPINFO_API=False, DEFAULT_COUNTRY='US')
    def test_country_from_ip_no_api(self):
        self.assertEqual(
            get_country_from_ip('127.0.0.1'), 'US')  # Default country
        self.assertEqual(
            get_country_from_ip('8.8.8.8'), 'US')  # Google
        self.assertEqual(
            get_country_from_ip('2.0.0.1'), 'US')  # Orange
        self.assertEqual(
            get_country_from_ip('2.160.0.1'), 'US')  # Telekom

    @override_settings(
        DEBUG=False,
        ENABLE_CURRENCY_API=False,
        DEFAULT_EXCHANGE_RATES={'USDUSD': 1, 'USDEUR': 2, 'USDGBP': 4})
    def test_currency_exchange_no_api(self):
        # Make sure cache is empty
        cache.set('currency_exchange_rates', None)
        self.assertIsNone(cache.get('currency_exchange_rates'))

        self.assertEqual(currency_exchange(10, 'USD', 'EUR'), 20)
        self.assertEqual(currency_exchange(20, 'EUR', 'GBP'), 40)
        self.assertEqual(currency_exchange(40, 'GBP', 'USD'), 10)

        self.assertIsNone(cache.get('currency_exchange_rates'))

    @override_settings(MEDIA_URL='http://absolute.url/media/files/')
    def test_mail_urls_absolute_media_url(self):
        self.populate_current_request()
        self.organization.theme.email_logo_image = ImageFieldFile(
            instance=None,
            field=FileField(),
            name='test.png'
        )
        self.organization.save()
        email_string = render_to_string(
            'customer/emails/base.html',
            fill_email_context(site=self.site)
        )
        expected_logo_url = 'http://absolute.url/media/files/test.png'
        self.assertIn(expected_logo_url, email_string)

    @override_settings(MEDIA_URL='/media/files/')
    def test_mail_urls_relative_media_url(self):
        self.populate_current_request()
        self.organization.theme.email_logo_image = ImageFieldFile(
            instance=None,
            field=FileField(),
            name='test.png'
        )
        self.organization.save()
        email_string = render_to_string(
            'customer/emails/base.html',
            fill_email_context(site=self.site)
        )
        expected_logo_url = 'http://my.3yd/media/files/test.png'
        self.assertIn(expected_logo_url, email_string)

    def test_exception_rendering(self):
        logging.disable(logging.CRITICAL)  # Don't print exception anywere

        try:
            self.client.get("/exception/")
        except TestException:
            logging.disable(logging.NOTSET)  # Enable logging again
            return

        logging.disable(logging.NOTSET)
        self.fail("Exception Rendering is not working. Try opening"
                  "/exception/. It should raise a TestException.")
