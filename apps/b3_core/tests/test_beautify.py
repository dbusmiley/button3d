import datetime

from django.utils import timezone
from apps.b3_core.beautify import pretty_times, \
    pretty_filesize_with_unit, pretty_date
from apps.b3_tests.testcases.common_testcases import TestCase


class BeautifyPrettyTimesTests(TestCase):
    def test_none_pretty_times(self):
        self.assertRaises(
            Exception, self.beautify_invalid_time_range)
        self.assertRaises(
            Exception, self.beautify_invalid_time_range_alternative)

    def test_no_range_single_day(self):
        one_day = (1, 1)
        self.assertEqual('1 day', pretty_times(one_day))

    def test_no_range_multiple_days(self):
        three_days = (3, 3)
        self.assertEqual('3 days', pretty_times(three_days))
        three_days = (6, 6)
        self.assertEqual('6 days', pretty_times(three_days))

    def test_range_three_five(self):
        one_day = (3, 5)
        self.assertEqual('3 - 5 days', pretty_times(one_day))

    def test_range_ten_twenty(self):
        one_day = (10, 20)
        self.assertEqual('10 - 20 days', pretty_times(one_day))

    @staticmethod
    def beautify_invalid_time_range():
        time_tuple = (None, 3)
        pretty_times(time_tuple)

    @staticmethod
    def beautify_invalid_time_range_alternative():
        time_tuple = (4, None)
        pretty_times(time_tuple)


class BeautifyPrettyFileSizeWithUnitTests(TestCase):
    def test_byte(self):
        self.assertEqual('553.00 B', pretty_filesize_with_unit(553))
        self.assertEqual('5.00 B', pretty_filesize_with_unit(5))

    def test_kilo_byte(self):
        self.assertEqual('1.02 kB', pretty_filesize_with_unit(1024))
        self.assertEqual('2.05 kB', pretty_filesize_with_unit(2048))

    def test_mega_byte(self):
        self.assertEqual('1.04 MB', pretty_filesize_with_unit(1044055))
        self.assertEqual('1.06 MB', pretty_filesize_with_unit(1057055))

    def test_giga_byte(self):
        self.assertEqual('1.00 GB', pretty_filesize_with_unit(1000000001))
        self.assertEqual('1.16 GB', pretty_filesize_with_unit(1164000000))
        self.assertEqual('1.17 GB', pretty_filesize_with_unit(1167000000))
        self.assertEqual('9.99 GB', pretty_filesize_with_unit(9995000000))


class BeautifyPrettyDateTests(TestCase):
    def test(self):
        date_now = timezone.now()
        self.assertEqual('just now', pretty_date(date_now))

    def test_seconds_ago(self):
        date_now = timezone.now()
        twenty_seconds_ago = date_now - datetime.timedelta(seconds=20)
        fourty_seconds_ago = date_now - datetime.timedelta(seconds=40)
        # We should not check actual time because of computation time.
        self.assertTrue(
            pretty_date(twenty_seconds_ago).find(' seconds ago') != -1)
        self.assertTrue(
            pretty_date(fourty_seconds_ago).find(' seconds ago') != -1)

    def test_minute_ago(self):
        date_now = timezone.now()
        minute_ago = date_now - datetime.timedelta(seconds=61)
        minute_ago_alternative = date_now - datetime.timedelta(seconds=75)
        self.assertEqual('a minute ago', pretty_date(minute_ago))
        self.assertEqual('a minute ago', pretty_date(minute_ago_alternative))

    def test_minutes_ago(self):
        date_now = timezone.now()
        minutes_ago = date_now - datetime.timedelta(minutes=3)
        minutes_ago_alternative = date_now - datetime.timedelta(minutes=4)
        # We should not check actual time because of computation time.
        self.assertTrue(pretty_date(minutes_ago).find(' minutes ago') != -1)
        self.assertTrue(
            pretty_date(minutes_ago_alternative).find(' minutes ago') != -1)

    def test_hour_ago(self):
        date_now = timezone.now()
        hour_ago = date_now - datetime.timedelta(minutes=61)
        hour_ago_alternative = date_now - datetime.timedelta(minutes=75)
        self.assertEqual('an hour ago', pretty_date(hour_ago))
        self.assertEqual('an hour ago', pretty_date(hour_ago_alternative))

    def test_hours_ago(self):
        date_now = timezone.now()
        hours_ago = date_now - datetime.timedelta(hours=3)
        hours_ago_alternative = date_now - datetime.timedelta(hours=4)
        # We should not check actual time because of computation time.
        self.assertTrue(pretty_date(hours_ago).find(' hours ago') != -1)
        self.assertTrue(
            pretty_date(hours_ago_alternative).find(' hours ago') != -1)

    def test_yesterday(self):
        date_now = timezone.now()
        day_ago = date_now - datetime.timedelta(hours=25)
        day_ago_alternative = date_now - datetime.timedelta(hours=26)
        self.assertEqual('Yesterday', pretty_date(day_ago))
        self.assertEqual('Yesterday', pretty_date(day_ago_alternative))

    def test_days_ago(self):
        date_now = timezone.now()
        days_ago = date_now - datetime.timedelta(days=2)
        days_ago_alternative = date_now - datetime.timedelta(days=3)
        # We should not check actual time because of computation time.
        self.assertTrue(pretty_date(days_ago).find(' days ago') != -1)
        self.assertTrue(
            pretty_date(days_ago_alternative).find(' days ago') != -1)

    def test_weeks_ago(self):
        date_now = timezone.now()
        weeks_ago = date_now - datetime.timedelta(weeks=2)
        weeks_ago_alternative = date_now - datetime.timedelta(weeks=3)
        # We should not check actual time because of computation time.
        self.assertTrue(
            pretty_date(weeks_ago).find(' weeks ago') != -1)
        self.assertTrue(
            pretty_date(weeks_ago_alternative).find(' weeks ago') != -1)

    def years_weeks_ago(self):
        date_now = timezone.now()
        years_ago = date_now - datetime.timedelta(years=2)
        years_ago_alternative = date_now - datetime.timedelta(years=3)
        # We should not check actual time because of computation time.
        self.assertTrue(pretty_date(years_ago).find(' years ago') != -1)
        self.assertTrue(
            pretty_date(years_ago_alternative).find(' years ago') != -1)
