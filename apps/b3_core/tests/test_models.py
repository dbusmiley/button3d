from apps.b3_tests.factories import ConfigurationFactory, \
    StlFileFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.basket.models import Line


class ConfigurationTests(TestCase):
    def test_scale_in_unit(self):
        config = ConfigurationFactory(scale=25.4, unit='mm')
        self.assertEqual(config.scale, 25.4)
        self.assertEqual(config.scale_in_unit, 25.4)

        config.unit = 'in'
        config.save()
        self.assertEqual(config.scale, 25.4)
        self.assertAlmostEqual(config.scale_in_unit, 1)

    def test_update(self):
        config = ConfigurationFactory()

        self.assertTrue(config.update(scale=4))
        config.refresh_from_db()
        self.assertEqual(config.scale, 4)

        self.assertTrue(config.update(unit='in'))
        config.refresh_from_db()
        self.assertEqual(config.unit, 'in')

        self.assertTrue(config.update(scale=2, unit='mm'))
        config.refresh_from_db()
        self.assertEqual(config.scale, 2)
        self.assertEqual(config.unit, 'mm')

    def test_configuration_clone(self):
        config = ConfigurationFactory(scale=3.4, unit='in')
        cloned_config = config.clone()

        self.assertEqual(config.scale, cloned_config.scale)
        self.assertEqual(config.unit, cloned_config.unit)
        self.assertNotEqual(config.id, cloned_config.id)


class StlFileTests(TestCase):
    def test_stlfile_delete(self):
        stlfile = StlFileFactory(deleted_date=None)
        self.assertFalse(stlfile.is_deleted)
        stlfile.delete()
        self.assertTrue(stlfile.is_deleted)

    def test_stlfile_deletes_also_lines(self):
        b = BasketFactory()
        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)

        self.assertFalse(b.is_deleted)
        self.assertFalse(stlfile.is_deleted)
        self.assertFalse(line.is_deleted)

        stlfile.delete()
        line = Line.all_objects.get(pk=line.pk)
        self.assertFalse(b.is_deleted)
        self.assertTrue(stlfile.is_deleted)
        self.assertTrue(line.is_deleted)
