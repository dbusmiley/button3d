# -*- encoding: utf-8 -*-
from django.test import override_settings

from apps.b3_tests.factories import StlFileFactory, \
    ManualRequestAttachmentFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket


@override_settings(DEBUG=True)
class DeleteUser(AuthenticatedTestCase):
    def test_active_basket_has_uncomplete_lines(self):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()

        b = BasketFactory(owner=self.user, status=Basket.OPEN, title='Täst')
        stlfile = StlFileFactory(owner=self.user, showname='Türbine Täst')
        line = b.add_empty_line(stlfile)
        line.name = 'Täst Line'
        line.save()

        ManualRequestAttachmentFactory(
            uploader=self.user, filename='Täst attachment')

        response = self.client.get(
            '/en/admin/auth/user/{0}/delete/'.format(self.user.id),
            follow=True)
        self.assertEqual(response.status_code, 200)
