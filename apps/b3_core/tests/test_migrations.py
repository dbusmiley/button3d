import subprocess  # nosec
from unittest import skipIf

from django.conf import settings
from django.db import connection
from django.test import SimpleTestCase, tag
from django_migration_linter import MigrationLinter


@tag('migrations')
class MigrationTest(SimpleTestCase):
    """
    Use SimpleTestCase case to avoid exception in Transactions atomic blocks.
    See https://stackoverflow.com/q/21458387/2036206
    SimpleTestCase does not allow modification to the database.
    So don't add tests here that insert objects into the database.
    """
    ignored_migrations = []
    git_version = '3.0.0'
    excluded_apps = []
    ignore_name_contains = []
    include_apps = []

    def test_no_missing_migrations(self):
        """Test if there are no migrations missing"""
        # If we set check_changes to True, sys.exit() is called which stops
        # the entire test chain. This is why it's explitly set it to False.
        p = subprocess.Popen(  # nosec
            ['python', 'manage.py', 'makemigrations', '--verbosity=1',
             '--check', '--dry-run', '--noinput'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        output, err = p.communicate()
        if p.returncode != 0:
            print(output.decode())
            self.fail("There are migrations pending")

    @skipIf(connection.vendor != 'mysql', 'Only lint migrations on MySQL')
    @skipIf(
        not open('version').read().startswith('3.'),
        'Lint only from 3.0.0 on.'
    )
    @tag('migration')
    def test_latest_migrations(self):
        print(f"ProjectPath: {settings.BASE_DIR}")
        print(f"ignore_name_contains: {self.ignore_name_contains}")
        print(f"ignore_name: {self.ignored_migrations}")
        print(f"include_apps: {self.include_apps}")
        print(f"exclude_app: {self.excluded_apps}")

        linter = MigrationLinter(
            settings.BASE_DIR,
            ignore_name_contains=self.ignore_name_contains,
            ignore_name=self.ignored_migrations,
            include_apps=self.include_apps,
            exclude_apps=self.excluded_apps,
        )
        linter.lint_all_migrations(git_commit_id=self.git_version)
        linter.print_summary()
        self.assertFalse(linter.has_errors)
