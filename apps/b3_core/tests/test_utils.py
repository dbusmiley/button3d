from apps.b3_core.utils import obfuscate_secret_string_value
from apps.b3_tests.testcases.common_testcases import TestCase


class TestObfuscateSecretStringValue(TestCase):
    def test_obfuscate_single_value(self):
        self.assertEqual('******', obfuscate_secret_string_value('secret'))
        self.assertEqual(
            '######',
            obfuscate_secret_string_value('secret', placeholder_char='#')
        )
        self.assertEqual('', obfuscate_secret_string_value(''))

    def test_obfuscate_password_in_string(self):
        self.assertEqual(
            'http://domain.com/?token=*******',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567', '1234567'))

        self.assertEqual(
            'http://domain.com/?token=1234567',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567', ''))

        self.assertEqual(
            'http://domain.com/?token=12***67',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567', '345'))

        self.assertEqual(
            'http://domain.com/?token=*******&key=9876543',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567&key=9876543', '1234567'))

        self.assertEqual(
            'http://domain.com/?token=1234567&key=*******',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567&key=9876543', '9876543'))

        self.assertEqual(
            'http://domain.com/?token=1234567&key=*******',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567&key=9876543', '9876543'))

        self.assertEqual(
            'http://domain.com/?token=*******&key=*******',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567&key=9876543',
                '9876543',
                '1234567'))

        self.assertEqual(
            'http://domain.com/?token=&&&&&&&&key=&&&&&&&',
            obfuscate_secret_string_value(
                'http://domain.com/?token=1234567&key=9876543',
                '1234567',
                '9876543', placeholder_char='&'))
