import typing as t

from django.http import HttpRequest, HttpResponse

import button3d.type_declarations as td

MiddlewareOrView = t.Callable[[HttpRequest], HttpResponse]


class BaseMiddleware(object):
    __doc__ = """
        Django 1.10+ middleware is a chain, where each middleware calls the
        next in the chain. The last one calls the view.
        The middleware is initialized with the next one as positional
        argument. The last in the chain calls the view method (in request
        phase) or ends the response cycle by handling execution back to the
        WSGI layer (in response phase).

        The `__init__` method stores the next Middleware in the chain as the
        `get_response` attribute.

        The `__call__` method must follow the this order:

        1) Do work on request if needed
        2) call `self.get_response`
        3) Do work on response if needed
        4) return the obtained or modified response

        Boilerplate::

            def __call__(self, request):
                # Optional: Do work on request here

                response = self.get_response(request)
                # Optional: Do work on response here or return your own to
                # short-circuit the loop.

                return response

        Further info:
        https://docs.djangoproject.com/en/dev/topics/http/middleware/

    """

    def __init__(self, get_response: MiddlewareOrView) -> None:
        self.get_response = get_response

    def __call__(self, request: td.HttpRequest):
        raise NotImplementedError('Middlewares must implement __call__')
