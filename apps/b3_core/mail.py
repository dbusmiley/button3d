import logging
import typing as t
import button3d.type_declarations as td

from urllib.parse import urljoin

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.translation import get_language

from apps.b3_organization.utils import get_current_site, \
    get_from_email, get_base_url, get_customer_support_phone

logger = logging.getLogger(__name__)


def fill_email_context(ctx: dict = None, **kwargs: dict) -> dict:
    """This method will not override the given context
    kwargs can contain 'site' and/or 'org' which will force the
    context site and org.
    Can contain 'country' also
    """
    site: td.Site = kwargs.get('site', get_current_site())
    org: td.Organization = kwargs.get('org', site.organization)
    country: t.Optional[td.Country] = kwargs.get('country', None)

    # If settings.MEDIA_URL is an absolute url,
    # urljoin will ignore the first argument. This is intended here.
    absolute_media_url: str = \
        urljoin(get_base_url(org=org), settings.MEDIA_URL)

    new_ctx = {
        'current_site': site,
        'current_org': org,
        'show_net_prices': org.show_net_prices,
        'media_base_url': absolute_media_url,
        'base_url': get_base_url(org=org),
        'customer_support_phone': get_customer_support_phone(country),
        'LANGUAGE_CODE': get_language()
    }
    if ctx:
        new_ctx.update(ctx)

    return new_ctx


class EmailSender:
    def __init__(
        self,
        site: td.Site = None,
        organization: td.Organization = None,
        country: t.Optional[td.Country] = None,
    ):
        self.site = site or get_current_site()
        self.organization = organization or self.site.organization
        self.country = country

    def send_email(
        self,
        subject_template_path: str,
        body_txt_template_path: str,
        body_html_template_path: t.Optional[str],
        extra_context: t.Optional[t.Dict] = None,
        fail_silently: bool = False,
        to: t.Union[str, t.Sequence[str]] = None,
        bcc: t.Union[str, t.Sequence[str]] = None
    ) -> t.Union[EmailMultiAlternatives, bool]:
        if not to and not bcc:
            return False

        if isinstance(to, str):
            to = [to]

        if isinstance(bcc, str):
            bcc = [bcc]

        subject, txt_body, html_body = self.render_email(
            subject_template_path,
            body_txt_template_path,
            body_html_template_path,
            extra_context,
        )

        from_email = get_from_email()
        logger.info(
            f'Sending email to {to} from {from_email} with subject: {subject}')

        email = EmailMultiAlternatives(
            subject=subject,
            body=txt_body,
            from_email=from_email,
            to=to,
            bcc=bcc,
        )
        if html_body:
            email.attach_alternative(html_body, 'text/html')

        email.send(fail_silently=fail_silently)
        return email

    def _get_email_context(
        self,
        context: t.Optional[t.Dict] = None,
    ) -> t.Dict:
        """
        This method will not override the given context
        """

        _context = context or {}

        absolute_media_url: str = urljoin(
            get_base_url(org=self.organization),
            settings.MEDIA_URL,
        )

        common_context = {
            'current_site': self.site,
            'current_org': self.organization,
            'show_net_prices': self.organization.show_net_prices,
            'media_base_url': absolute_media_url,
            'base_url': get_base_url(org=self.organization),
            'customer_support_phone': get_customer_support_phone(self.country),
            'LANGUAGE_CODE': get_language(),
        }

        return {**_context, **common_context}

    def render_email(
        self,
        subject_template_path: str,
        body_txt_template_path: str,
        body_html_template_path: t.Optional[str] = None,
        context: t.Optional[t.Dict] = None,
    ) -> t.Tuple[str, str, t.Optional[str]]:

        _context = self._get_email_context(context or {})

        try:
            subject = render_to_string(subject_template_path, _context)
        except Exception as exc:
            logger.exception(f'Email subject {subject} rendering error', exc)
            raise

        subject = subject.replace('\n', '').replace('\r', '').strip()

        try:
            text_body = render_to_string(body_txt_template_path, _context)
        except Exception as exc:
            logger.exception(
                f'Email text body {body_txt_template_path} rendering error',
                exc,
            )
            raise

        if body_html_template_path:
            try:
                html_body = render_to_string(body_html_template_path, _context)
            except Exception as exc:
                logger.exception(
                    f'Email HTML body {body_html_template_path} '
                    f'rendering error',
                    exc)
                raise

        else:
            html_body = None

        return subject, text_body, html_body
