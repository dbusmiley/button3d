import json
import logging
import urllib.request
import urllib.error
from django.conf import settings

from django.core.cache import cache

from apps.b3_core.utils import build_secure_download_url

logger = logging.getLogger(__name__)


class SupportStructure(object):
    def __init__(self, stl_file):
        super(SupportStructure, self).__init__()
        self._stl_file = stl_file
        self._histogram = None
        self._support_data = None

    def get_values(self, max_angle, offset, scale, use_cache=True):
        """
        returns support volume, supporting area and supported area
        for a given maximum angle, offset and scale.
        If an error occurs, log it and pass it on to the caller.
        """
        try:
            values_unscaled = self._get_unscaled_values(
                float(max_angle),
                use_cache
            )
        except SupportStructure.DataUnavailableException as e:
            self._log_error_if_support_job_finished(e)
            raise
        except SupportStructure.DataInvalidException as e:
            logger.exception(e)
            raise

        return self._scale_values(values_unscaled, float(scale), float(offset))

    def _get_unscaled_values(self, max_angle, use_cache):
        """
        Get unscaled values either from cache or from the backend.
        Unscaled values can be cached globally for future requests.
        """
        cache_key = 'support:uuid={0}:angle={1}'.format(
            self._stl_file.uuid, max_angle
        )

        if use_cache and cache_key in cache:
            # use cached values stored by past requests
            return cache.get(cache_key)

        self._populate_support_data()
        values_unscaled = self._calculate_unscaled(max_angle)

        # cache values for future requests
        cache.set(cache_key, values_unscaled, timeout=None)
        return values_unscaled

    def _log_error_if_support_job_finished(self, error: Exception) -> None:
        """
        If the data is unavailable, but all jobs for the stl file are
        finished, logs an exception. Otherwise, just logs a warning.
        """
        if self._stl_file.all_jobs_finished:
            logger.exception(error)
        else:
            logger.warning(error)

    def _populate_support_data(self):
        """
        Populates the _support_data field with the data from the backend.
        If histogram is not available, fetch full json data instead.
        """
        if self._support_data:
            return
        try:
            self._support_data = self._fetch_support_json()
            self._histogram = True
        except SupportStructure.DataUnavailableException:
            self._support_data = self._fetch_support_json(histogram=False)
            self._histogram = False

    def _fetch_support_json(self, histogram=True):
        """
        Fetches support.json or support_histogram.json from the backend
        and deserializes it.
        """
        url = build_secure_download_url(
            uuid=self._stl_file.uuid,
            file='support_histogram.json' if histogram else 'support.json',
            base_url=settings.BACKEND_3D_HOST
        )

        try:
            # fetch file from the backend and deserialize it
            response = urllib.request.urlopen(url)  # nosec
            return json.loads(response.read().decode('UTF-8'))
        except urllib.error.URLError as e:
            raise SupportStructure.DataUnavailableException(e)
        except (IOError, UnicodeError, ValueError) as e:
            raise SupportStructure.DataInvalidException(e)

    def _calculate_unscaled(self, max_angle):
        """
        returns block_volume, ground_area, supported_area and
        supporting_area for a given maximum angle and scale = 1.
        Uses various fallbacks for old formats of support data.
        """
        if not self._histogram:
            # Fallback (using full data)
            try:
                support_data = self._support_data['orientations']
            except KeyError:
                raise SupportStructure.DataInvalidException(
                    "'orientations' is not in support data."
                )

            try:
                values_unscaled = self._calculate_unscaled_fallback(
                    support_data,
                    max_angle
                )
            except (KeyError, ValueError) as e:
                raise SupportStructure.DataInvalidException(e)
        else:
            # default behavior (using histogram)
            try:
                values_unscaled = self._calculate_unscaled_from_histogram(
                    self._support_data,
                    max_angle
                )
            except KeyError as e:
                raise SupportStructure.DataInvalidException(e)

        if 'h-' not in values_unscaled:
            raise SupportStructure.DataInvalidException(
                "Orientation 'h-' is not in support data."
            )
        return values_unscaled

    @staticmethod
    def _calculate_unscaled_from_histogram(support_data, max_angle):
        values_unscaled = {}
        for orientation, data in support_data.items():
            default_ground_area = data[-1]['ground_area'] if data else 0
            values = (0, default_ground_area, default_ground_area, 0)
            for item in data:
                if item['angle'] > max_angle:
                    values = (
                        item['block_volume'],
                        item['ground_area'],
                        item['supported_area'],
                        item['supporting_area']
                    )
                    break
            values_unscaled[orientation] = values
        return values_unscaled

    @staticmethod
    def _calculate_unscaled_fallback(support_data, max_angle):
        values_unscaled = {}
        for orientation, data in support_data.items():
            # find first block with angle > max_angle
            i = 0
            while i < data['num_blocks'] and data['blocks'][i][1] <= max_angle:
                i += 1
            # extract values:
            #   block_volumes: volume of the block
            #   ground_areas: area on which the block
            #      stands on the ground
            #   face_areas: area of the supported face
            #   supporting_areas: same as ground_area, but
            #      for the case that the block stands on the model
            (block_volumes, ground_areas, face_areas, supporting_areas) = \
                [], [], [], []
            for _, _, _, volume, face_area, projection_area, ground_supported \
                    in data['blocks'][i:]:
                block_volumes.append(volume)
                face_areas.append(face_area)
                if ground_supported:
                    ground_areas.append(projection_area)
                else:
                    supporting_areas.append(projection_area)

            # compute volume of all supportblocks
            block_volume = sum(block_volumes)
            # compute area of support and model that touches the ground
            ground_area = sum(ground_areas) + data['base_ground_area']
            # compute area of the model that is supported
            supported_area = sum(face_areas) + data['base_ground_area']
            # compute area of the model that has support
            #   blocks standing on it
            supporting_area = sum(supporting_areas)

            values_unscaled[orientation] = (
                block_volume,
                ground_area,
                supported_area,
                supporting_area
            )
        return values_unscaled

    @staticmethod
    def _scale_values(values_unscaled, scale, offset):
        """
        scales unscaled values up to the specified scale. Since the offset
        is independent of the scale, it is taken into account here.
        """
        values_scaled = {}
        for orientation in values_unscaled:
            (
                block_volume_unscaled,
                ground_area_unscaled,
                supported_area_unscaled,
                supporting_area_unscaled
            ) = values_unscaled[orientation]

            block_volume = block_volume_unscaled * (scale ** 3)
            raft_volume = offset * (ground_area_unscaled * (scale ** 2))
            supported_area = supported_area_unscaled * (scale ** 2)
            supporting_area = supporting_area_unscaled * (scale ** 2)

            values_scaled[orientation] = (
                block_volume + raft_volume,
                supported_area,
                supporting_area
            )
        return values_scaled

    class DataException(Exception):
        pass

    class DataUnavailableException(DataException):
        pass

    class DataInvalidException(DataException):
        pass
