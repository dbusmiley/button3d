import typing as t

from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


def pretty_times(times: t.Sequence[int]) -> str:
    min_time, max_time = times
    if min_time is None or max_time is None:
        raise Exception('Time can\'t be calculated')
    string_days = _('days')
    if min_time == max_time:
        if min_time == 1:
            string_days = _('day')
        return f'{min_time} {string_days}'
    return f'{min_time} - {max_time} {string_days}'


def pretty_date(time: bool = False) -> str:
    """
    Get a timezone object or a int() Epoch timestamp and return a
    pretty string like 'an hour ago', 'Yesterday', '3 months ago',
    'just now', etc
    """

    # Remove the timezone
    now = timezone.now()
    if type(time) is int:
        diff = now - timezone.datetime.fromtimestamp(time)
    elif isinstance(time, timezone.datetime):
        diff = now - time
    elif not time:
        diff = now - now

    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return ''

    if day_diff == 0:
        if second_diff < 10:
            return str(_("just now"))
        if second_diff < 60:
            return str(_('%(seconds)s seconds ago') % {'seconds': second_diff})
        if second_diff < 120:
            return str(_("a minute ago"))
        if second_diff < 3600:
            return str(_('%(minutes)s minutes ago')
                       % {'minutes': (second_diff / 60)})
        if second_diff < 7200:
            return str(_("an hour ago"))
        if second_diff < 86400:
            return str(_('%(hours)s hours ago')
                       % {'hours': (second_diff / 3600)})
    if day_diff == 1:
        return str(_("Yesterday"))
    if day_diff < 7:
        return str(_('%(days)s days ago') % {'days': day_diff})
    if day_diff < 31:
        return str(_('%(weeks)s weeks ago') % {'weeks': (day_diff / 7)})
    if day_diff < 365:
        return str(_('%(months)s months ago') % {'months': (day_diff / 30)})
    return str(_('%(years)s years ago') % {'years': (day_diff / 365)})


def pretty_filesize_with_unit(nb_bytes: int) -> str:
    if nb_bytes > 1000000000:
        unit = 'GB'
        factor = 1000000000
    elif nb_bytes > 1000000:
        unit = 'MB'
        factor = 1000000
    elif nb_bytes > 1000:
        unit = 'kB'
        factor = 1000
    else:
        unit = 'B'
        factor = 1
    return '{0:.2f} {1}'.format(float(nb_bytes) / float(factor), unit)
