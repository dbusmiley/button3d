import typing as t

from rest_framework.filters import OrderingFilter

import button3d.type_declarations as td


class FieldMappingOrderingFilter(OrderingFilter):
    """
    OrderingFilter, that allows to give `ordering_field_mapping` of query
    param items in "ordering" to field names.

    Does not change filtering/ordering behavior, if
    `view.ordering_field_mapping` is undefined
    """

    def _get_ordering_field_mapping(
            self, mapping: td.StrKeyDict) -> td.StrKeyDict:
        """
        Updates `mapping` dict with keys and valued prepended with "minus"
        sign.

        For example {'key': 'value'} -> {'key': 'value', '-key': '-value'}
        """
        negated_mapping = {
            f'-{key}': f'-{value}' for key, value in mapping.items()
        }
        return {**mapping, **negated_mapping}

    def get_ordering(self, request, queryset, view) -> t.Sequence[str]:
        ordering_field_mapping: td.StrKeyDict = \
            self._get_ordering_field_mapping(
                getattr(view, 'ordering_field_mapping', {})
            )

        ordering = super().get_ordering(request, queryset, view) or []
        return [
            ordering_field_mapping.get(field_name, field_name)
            for field_name
            in ordering
        ]
