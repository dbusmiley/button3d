from django.conf import settings

import button3d.type_declarations as td
from apps.b3_core.utils import determine_default_currency
from .utils import get_country, get_client_ip


def core_context_processor(request: td.HttpRequest) -> dict:
    current_country = get_country(request)
    is_debug_ip = get_client_ip(request) in getattr(settings, "DEBUG_IPS", [])
    return {
        'current_country': current_country,
        'is_debug_ip': is_debug_ip,
        'active_currency': determine_default_currency(request)
    }
