from django.conf import settings
from django.db import models


class CurrencyField(models.DecimalField):
    def __init__(self, *args, **kwargs) -> None:
        kwargs['max_digits'] = settings.CURRENCY_FIELD_DIGITS
        kwargs['decimal_places'] = settings.CURRENCY_FIELD_DECIMAL_PLACES
        super().__init__(*args, **kwargs)


class NonUniqueOneToOneField(models.OneToOneField):
    """
    This class drops `unique`=True for original class.
    We need OneToOneField without uniqueness constraint to work correctly on
    MS SQL server.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._unique = False
