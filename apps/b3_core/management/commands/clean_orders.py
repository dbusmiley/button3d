from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from apps.b3_order.models import Order
from apps.b3_core.utils import query_yes_no


class Command(BaseCommand):
    help = 'Deletes invalid orders from the database. First lists the orders' \
           'and then asks for confirmation'

    def handle(self, *args, **options):

        items = self._get_invalid_orders()

        if not items:
            print("All Orders are fine :)")
            return

        print("The following Orders are invalid:")

        for item in items:
            order, reason = item["order"], item["reason"]

            try:
                order_partner = order.partner if order.partner else "???"
            except AttributeError:
                order_partner = "???"

            print("{0}: By {1} from {2} at {3} ({4})".format(
                order.number,
                order.user if order.user else "???",
                order_partner,
                order.date_placed if order.date_placed else "???",
                reason
            ))

        if query_yes_no("Do you want to delete all of those orders?", "no"):
            self._delete_orders([item["order"] for item in items])

    @staticmethod
    def _get_invalid_orders():
        orders = []
        for order in Order.all_objects.all():
            try:
                order.validate()
            except ValidationError as e:
                orders.append({"order": order, "reason": e.message})
        return orders

    @staticmethod
    def _delete_orders(orders):
        for order in orders:
            order.delete()
            print("Deleted {}".format(order))
