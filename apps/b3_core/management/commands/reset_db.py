from django.core import management
from django.db import connections, DEFAULT_DB_ALIAS
from django.db.utils import InternalError
from django.conf import settings
import argparse


def is_pgsql(conn):
    return conn.vendor.startswith('postgre')


def is_mysql(conn):
    return conn.vendor.startswith('mysql')


class Command(management.base.BaseCommand):
    help = """Drops and recreates the database. Handle with care. The
    encoding names and collation names should be assumed database vendor
    specific. While "utf8" seems to be supported by most, collation names
    differ greatly. Please consult the manual for your database server.

    CAVEATS
    -------
    Connections are made to databases, not database servers. That means that
    if the database was dropped and not recreated, this tool is quite
    useless and you will have to recreate the database by other means."""

    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument('--encoding', type=str, help='Default encoding',
                            default='utf8')
        parser.add_argument('--collate', type=str, help='Sorting collation')
        parser.add_argument('--dbalias', type=str,
                            help='Database alias in settings file',
                            default=DEFAULT_DB_ALIAS)
        parser.add_argument('--migrate', help='Migrate if successful',
                            default=False, action='store_true')

    def _get_tables(self, connection):
        return connection.introspection.table_names(include_views=False)

    def drop_all_tables(self, connection):
        tables = self._get_tables(connection)
        if not tables:
            self.stderr.write(self.style.ERROR('No tables in database'))
            return

        sql = 'DROP TABLE ' + ', '.join(tables)
        if is_pgsql(connection):
            sql = sql + ' CASCADE;'

        with connection.cursor() as cursor:
            self.stdout.write(
                self.style.WARNING(sql)
            )
            try:
                cursor.execute(sql)
            except InternalError:
                raise
            else:
                self.stdout.write(self.style.SUCCESS('OK...all tables '
                                                     'dropped'))

    def handle(self, *args, **options):
        dbalias = options['dbalias']
        encoding = options['encoding']
        collation = options.get('collate', None)
        verbosity = options['verbosity']
        do_migrate = options['migrate']

        dbname = settings.DATABASES[dbalias]['NAME']
        connection = connections[dbalias]

        with connection.cursor() as cursor:
            if is_pgsql(connection):
                self.drop_all_tables(connection)
                return

            if verbosity > 1:
                self.stdout.write(self.style.WARNING(
                    f"DROP DATABASE IF EXISTS {dbname}"
                ))
            try:
                cursor.execute(f"DROP DATABASE IF EXISTS {dbname}")
            except InternalError:
                raise RuntimeError(f'Cannot connect as connection requires '
                                   f'the database "{dbname}" to exist.')
            else:
                self.stdout.write(
                    self.style.SUCCESS("Dropped database. Recreating...")
                )

            sql_args = dict(dbname=dbname, collate_args='')
            if is_pgsql(connection):
                sql_args['encoding_args'] = f' ENCODING {encoding}'
                if collation:
                    sql_args['collate_args'] = f' LC_COLLATE={collation}'
            elif is_mysql(connection):
                sql_args['encoding_args'] = f' CHARACTER SET {encoding}'
                if collation:
                    sql_args['collate_args'] = f' COLLATE {collation}'
            else:
                raise RuntimeError(f'Database: {connection.vendor}'
                                   f' not supported by this tool.')

            create_sql = 'CREATE DATABASE {dbname}{encoding_args}' \
                         '{collate_args}'.format(**sql_args)
            if verbosity > 1:
                self.stdout.write(self.style.WARNING(create_sql))

            try:
                cursor.execute(create_sql)
            except InternalError:
                raise RuntimeError(f'Failed to recreate {dbname}')
            else:
                self.stdout.write(self.style.SUCCESS(
                    f"Recreated database {dbname}. Migrating..."
                ))

        connections.close_all()
        if do_migrate:
            management.call_command('migrate', database=dbalias)
