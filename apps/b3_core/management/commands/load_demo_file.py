from django.core.management.base import BaseCommand, CommandError
from apps.b3_core.models import Parameter, StlFile, Job


class Command(BaseCommand):
    help = 'Loads the Demo File into the database'

    def handle(self, *args, **options):

        stl_file, created = \
            StlFile.objects.get_or_create(
                uuid='00000000-0000-0000-0000-000000000000')

        if not created:
            return

        stl_file.showname = 'LittleShip'
        stl_file.creation_date = '2015-08-31 14:16:00'
        stl_file.origin = 'button3d'

        o_parameter = Parameter()
        o_parameter.max_scale = 3.907
        o_parameter.area = 19881.75
        o_parameter.volume = 9667.697
        o_parameter.h = 97.858
        o_parameter.w = 29.877
        o_parameter.d = 144.6
        o_parameter.faces = 1646

        r_parameter = Parameter()
        r_parameter.max_scale = 3.907
        r_parameter.area = 17672.93
        r_parameter.volume = 23286.07
        r_parameter.h = 97.856
        r_parameter.w = 29.878
        r_parameter.d = 144.6
        r_parameter.faces = 2308
        r_parameter.shells = 1
        r_parameter.holes = 0

        o_parameter.save()
        r_parameter.save()
        stl_file.o_parameter_id = o_parameter.id
        stl_file.r_parameter_id = r_parameter.id
        stl_file.parameter_id = r_parameter.id

        stl_file.save()

        job_convert = Job()
        job_convert.uuid = '4dec0c2e-3efd-4c3d-9e8b-b913d41cfa27'
        job_convert.DTYPE = 'CONVERT'
        job_convert.error = ''
        job_convert.status = 'FINISHED'
        job_convert.stl_file_id = stl_file.id
        job_convert.save()

        job_convert = Job()
        job_convert.uuid = '4dec0c2e-3efd-4c3d-9e8b-b913d41cfa28'
        job_convert.DTYPE = 'PARAMETER'
        job_convert.error = ''
        job_convert.status = 'FINISHED'
        job_convert.stl_file_id = stl_file.id
        job_convert.save()

        job_convert = Job()
        job_convert.uuid = '4dec0c2e-3efd-4c3d-9e8b-b913d41cfa29'
        job_convert.DTYPE = 'WTA'
        job_convert.error = ''
        job_convert.status = 'FINISHED'
        job_convert.stl_file_id = stl_file.id
        job_convert.save()

        job_convert = Job()
        job_convert.uuid = '4dec0c2e-3efd-4c3d-9e8b-b913d41cfa2a'
        job_convert.DTYPE = 'REPAIR'
        job_convert.error = ''
        job_convert.status = 'FINISHED'
        job_convert.stl_file_id = stl_file.id
        job_convert.save()
