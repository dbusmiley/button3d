import json
import logging
from functools import wraps

from django.http import HttpResponseBadRequest
from django.utils.decorators import available_attrs
from django.views.decorators.http import require_http_methods

logger = logging.getLogger(__name__)


def require_ajax(function):
    def _is_ajax(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return function(request, *args, **kwargs)

    return _is_ajax


def require_json_keys(json_keys_list):
    """
    Decorator to make a view only accept a json body,
    containing at least certain keys. Usage:

        @require_json_keys(["user_id", "group_id"])
        def my_view(request):
            # I can assume that the request body is in
            # json format and contains at least these keys
            # ...
    """
    def decorator(func):
        @wraps(func, assigned=available_attrs(func))
        def inner(request, *args, **kwargs):
            try:
                body_json = json.loads(request.body.decode('utf-8'))
            except ValueError as e:
                logger.warning(
                    'Could not parse request body to json: ' + str(e))
                return HttpResponseBadRequest()

            for required_key in json_keys_list:
                if required_key not in body_json:
                    logger.warning(
                        'Key {0} not found in request body'.format(
                            required_key))
                    return HttpResponseBadRequest()

            return func(request, *args, **kwargs)
        return inner
    return decorator


require_PUT = require_http_methods(["PUT"])
require_PUT.__doc__ = ("Decorator to require that a view "
                       "only accepts the PUT method.")

require_DELETE = require_http_methods(["DELETE"])
require_DELETE.__doc__ = ("Decorator to require that a view "
                          "only accepts the DELETE method.")
