from decimal import Decimal
import ply.lex as lex
import ply.yacc as yacc


class DecimalCalc(object):
    lexer = None
    yacc = None
    variables = {}

    tokens = (
        'NAME', 'NUMBER',
        'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'POWER',
        'LPAREN', 'RPAREN', 'COMMA'
    )

    # Tokens

    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_POWER = r'\^'
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_NAME = r'[a-zA-Z_][a-zA-Z0-9_]*'
    t_COMMA = r','

    def t_NUMBER(self, t):
        r'[\d]+([\.][\d]*)?'
        t.value = Decimal(t.value)
        return t

    # Ignored characters
    t_ignore = " \t"

    def t_error(self, t):
        raise DecimalCalcException("Illegal character '%s'" % t.value[0])

    # Precedence rules for the arithmetic operators
    precedence = (
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE', 'POWER'),
        ('right', 'UMINUS'),
    )

    def p_statement_expr(self, p):
        '''statement : expression'''
        p[0] = p[1]

    def p_expression_binop(self, p):
        '''expression : expression PLUS expression
                      | expression MINUS expression
                      | expression TIMES expression
                      | expression DIVIDE expression
                      | expression POWER expression'''
        if p[2] == '+':
            p[0] = p[1] + p[3]
        elif p[2] == '-':
            p[0] = p[1] - p[3]
        elif p[2] == '*':
            p[0] = p[1] * p[3]
        elif p[2] == '/':
            p[0] = p[1] / p[3]
        elif p[2] == '^':
            p[0] = p[1] ** p[3]

    def p_expression_uminus(self, p):
        'expression : MINUS expression %prec UMINUS'
        p[0] = -p[2]

    def p_parameters(self, p):
        '''parameters : expression
                      | parameters COMMA expression'''
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1]
            p[0].append(p[3])

    def p_expression_group(self, p):
        'group : LPAREN parameters RPAREN'
        p[0] = p[2]

    def p_expression_from_group(self, p):
        'expression : group'
        if len(p[1]) == 1:
            p[0] = p[1][0]
        else:
            raise DecimalCalcException("Must be one argument '%s'" % p[1])

    def p_expression_function(self, p):
        'expression : NAME group'
        if p[1] == 'root' and len(p[2]) == 1:
            p[0] = p[2][0] ** Decimal('0.5')
        elif p[1] == 'pow' and len(p[2]) == 2:
            p[0] = p[2][0] ** p[2][1]
        elif p[1] == 'min':
            p[0] = min([x for x in p[2] if x is not None])
        elif p[1] == 'max':
            p[0] = max([x for x in p[2] if x is not None])
        elif p[1] == 'if_g_le':
            if p[2][1] < p[2][0] <= p[2][2]:
                p[0] = p[2][3]
            else:
                p[0] = None
        elif p[1] == 'round' and len(p[2]) == 2:
            nb_places = Decimal(10) ** -p[2][1]
            p[0] = p[2][0].quantize(nb_places)
        else:
            raise DecimalCalcException("Undefined function '%s'" % p[1])

    def p_expression_number(self, p):
        'expression : NUMBER'
        p[0] = p[1]

    def p_expression_name(self, p):
        'expression : NAME'
        try:
            p[0] = Decimal(self.variables[p[1]])
        except LookupError:
            raise DecimalCalcException("Undefined variable '%s'" % p[1])

    def p_error(self, p):
        if p is None:
            raise DecimalCalcException("Syntax error: empty expression.")
        raise DecimalCalcException("Syntax error at '%s'" % p.value)

    def __init__(self):
        self.lexer = lex.lex(module=self)
        self.yacc = yacc.yacc(debug=0, optimize=1, write_tables=1, module=self)

    def parse(self, string, variables=None):
        try:
            self.variables = variables or {}
            return self.yacc.parse(string)
        except DecimalCalcException as e:
            raise e
        except Exception as e:
            raise DecimalCalcException(e) from e

    _instance = None

    @staticmethod
    def instance() -> 'DecimalCalc':
        if DecimalCalc._instance:
            return DecimalCalc._instance
        else:
            DecimalCalc._instance = DecimalCalc()
            return DecimalCalc._instance


class DecimalCalcException(Exception):
    pass
