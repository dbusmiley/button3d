from modeltranslation.translator import translator, TranslationOptions

from apps.partner.models import Partner, StockRecord, PostProcessing


class PartnerTranslationOptions(TranslationOptions):
    fields = Partner.translated_fields


class StockRecordTranslationOptions(TranslationOptions):
    fields = ('datasheet_pdf',)


class PostProcessingTranslationOptions(TranslationOptions):
    fields = ('datasheet_pdf',)


translator.register(Partner, PartnerTranslationOptions)
translator.register(StockRecord, StockRecordTranslationOptions)
translator.register(PostProcessing, PostProcessingTranslationOptions)

# Temporary fix for a bug in modeltranslation. See
# https://github.com/deschler/django-modeltranslation/issues/455
# Should be removed as soon as bug is resolved
Partner._meta.base_manager_name = None
StockRecord._meta.base_manager_name = None
PostProcessing._meta.base_manager_name = None
