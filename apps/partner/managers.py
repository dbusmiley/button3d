from django.db import models

from apps.b3_core.managers import DeletedDateManager
from apps.b3_organization.utils import get_current_site


class NaturalKeyPartnerManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, code):
        return self.get(code=code)


class DeletedDatePartnerManager(NaturalKeyPartnerManager, DeletedDateManager):
    pass


class SitePartnerManager(DeletedDatePartnerManager):
    def get_queryset(self):
        site = get_current_site()
        qs = super(SitePartnerManager, self).get_queryset()

        if not site:
            # Normally the site is never NULL. However, during server
            # startup, some really wired initialisations are happening,
            # calling this method without a proper site.
            return qs

        # Show site partners + main site partners
        # filtered by site.organization.partners_enabled
        partners_enabled = site.organization.partners_enabled.all()
        if not partners_enabled.count():
            return qs.none()

        # Convert to list to prevent subquery on partner filtering.
        partners_enabled_ids = list(
            partners_enabled.values_list('id', flat=True))
        # Filter by enable partner ids list.
        return qs.filter(id__in=partners_enabled_ids)


class NaturalKeyStockRecordManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, partner_code, partner_sku):
        return self.get(
            partner__code=partner_code,
            partner_sku=partner_sku
        )


class StockRecordManager(NaturalKeyStockRecordManager, DeletedDateManager):
    def get_queryset(self):
        # SQL optimization: select_related() for JOINs (product, partner)
        # and prefetch_related() to get colors once.
        qs = super(StockRecordManager, self).get_queryset().select_related(
            'partner', 'product')

        # Filter StockRecords for the current domain
        site = get_current_site()
        if not site:
            # Normally the site is never NULL. However, during server
            # startup, some really wired initialisations are happening,
            # calling this method without a proper site.
            return qs

        # Show site partners + main site partners
        # filtered by site.organization.partners_enabled
        partners_enabled = site.organization.partners_enabled.all()
        return qs.filter(partner__in=partners_enabled)


class NaturalKeyPostProcessingManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, partner_code, partner_sku, slug):
        return self.get(
            stock_record__partner__code=partner_code,
            stock_record__partner_sku=partner_sku,
            slug=slug
        )

    def published(self):
        from apps.partner.admin import StockRecord
        return self.get_queryset().filter(
            published=True,
            stock_record__edit_status=StockRecord.PUBLISHED,
            stock_record__deleted_date=None,
        )


class PostProcessingManager(
    DeletedDateManager, NaturalKeyPostProcessingManager
):
    pass
