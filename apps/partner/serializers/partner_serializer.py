from rest_framework import serializers

from apps.b3_address.serializers import AddressSerializer
from apps.partner.models import Partner


class PartnerSerializer(serializers.ModelSerializer):
    address = AddressSerializer()
    tax_type = serializers.CharField(source='get_tax_type_display')

    class Meta:
        model = Partner
        fields = (
            'id',
            'name',
            'logo',
            'website',
            'email',
            'address',
            'tax_type',
        )
