from decimal import Decimal as D
import typing as t

import button3d.type_declarations as td

from apps.b3_core.utils import DoesNotFitInBoundsException,\
    cache_result_on_instance, quantize, NoPricingPossibleException, \
    ManualPricingRequiredException
from apps.partner.pricing.calculators.base import AbstractPriceCalculator
from apps.partner.pricing.calculators.unit import UnitPriceCalculator


class PartPriceCalculator(AbstractPriceCalculator):
    """
    This class is responsible for providing unit-price, item-discount and
    item-total. It uses an instance of UnitPriceCalculator to get the base
    price of a model, checks if this model needs manual pricing and creates
    a price object representing the unit-price of the model. It also applies
    the material specific discounts to the unit-price and calculates the
    item-total depending on the quantity of the model.

    If Any problems occur during the calculation, NoPricingPossibleException
    is raised.
    If the model needs manual pricing, ManualPricingRequiredException
    is raised.
    """

    def __init__(
        self,
        stl_file: td.StlFile,
        stock_record: td.StockRecord,
        post_processings: td.PostProcessings,
        scale: float = 1.0,
        quantity: int = 1,
        currency: t.Optional[str] = None,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ):
        super().__init__(
            currency=currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

        post_processings = post_processings or []
        self._stock_record = stock_record
        self._quantity = quantity
        self._post_processings = post_processings

        self._unit_price_calculator = UnitPriceCalculator(
            stl_file=stl_file,
            stock_record=stock_record,
            post_processings=post_processings,
            scale=scale,
            quantity=quantity
        )

    @property
    @cache_result_on_instance
    def unit_price(self) -> td.Price:
        if self._stock_record.always_priced_manually:
            raise ManualPricingRequiredException
        for post_processing in self._post_processings:
            if post_processing.always_priced_manually:
                raise ManualPricingRequiredException

        unit_price_raw = self._calculate_unit_price()

        if self._is_manual_pricing_required(unit_price_raw):
            raise ManualPricingRequiredException

        return self.create_price(unit_price_raw)

    @property
    def item_price(self):
        return self.unit_price * self._quantity

    @property
    @cache_result_on_instance
    def item_discount(self) -> td.Price:
        item_discount_raw = self._calculate_discount()

        if item_discount_raw > self.item_price.excl_tax:
            item_discount_raw = self.item_price.excl_tax
        if item_discount_raw < D('0.00'):
            raise NoPricingPossibleException

        return self.create_price(item_discount_raw)

    @property
    def item_total(self) -> td.Price:
        return self.item_price - self.item_discount

    @property
    def partner(self) -> td.Partner:
        return self._stock_record.partner

    def _calculate_unit_price(self) -> td.Decimal:
        try:
            unit_price_raw = self._unit_price_calculator.get_price()
        except DoesNotFitInBoundsException as e:
            raise NoPricingPossibleException(e)
        if unit_price_raw < D('0.00'):
            raise NoPricingPossibleException
        return quantize(unit_price_raw)

    def _calculate_discount(self) -> td.Decimal:
        base_price = self.item_price.excl_tax

        applicable_discounts = [
            discount
            for rule in self._stock_record.get_discount_rules()
            for discount in rule.get_conclusion_actions()
            if rule.is_applying(
                price=base_price,
                quantity=self._quantity
            )
        ]
        if not applicable_discounts:
            return D('0.00')

        # Only the discount with the highest benefit is applied
        best_discount = max(
            applicable_discounts,
            key=lambda discount: discount.get_amount(base_price)
        )
        return quantize(best_discount.get_amount(base_price))

    def _is_manual_pricing_required(self, price: td.Decimal) -> bool:
        return any(
            rule.is_applying(price=price, quantity=self._quantity)
            for rule in self._stock_record.get_manual_pricing_rules()
        )
