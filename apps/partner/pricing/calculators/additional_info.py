"""
AdditionalInfo

Classes in this module are used to collect additional information about the
pricing process, such as the support structure values of the corresponding
StlFile, the costs for the model in different orientations etc.

This logic is extracted from the actual PriceCalculator classes,
and implemented as a subclass. Whenevery you need the additional information,
you can instantiate AdditionalInfoPartPriceCalculator or
AdditionalInfoUnitPriceCalculator instead of their superclasses.
"""
from decimal import Decimal as D

import typing as t

import button3d.type_declarations as td

from apps.b3_core.utils import Orientation, cache_result_on_instance
from apps.partner.pricing.calculators import PartPriceCalculator
from apps.partner.pricing.calculators.unit import UnitPriceCalculator


class AdditionalInfoUnitPriceCalculator(UnitPriceCalculator):
    @property
    @cache_result_on_instance
    def additional_info(self):
        return AdditionalPricingInfo(
            self.stock_record,
            self.stl_file,
            self.scale
        )

    def _calculate_model_price(self, orientation: str = 'h-') -> td.Decimal:
        model_price = super()._calculate_model_price(orientation)

        if self.stock_record.support_enabled:
            width, depth, height = self._get_stl_file_dimensions(orientation)
            self.additional_info.update_orientation(
                orientation,
                model_cost=model_price,
                w=width,
                d=depth,
                h=height
            )

        return model_price

    def _calculate_support_price(self, orientation: str = 'h-') -> td.Decimal:
        support_price = super()._calculate_support_price(orientation)

        self.additional_info.update_orientation(
            orientation,
            support_cost=support_price
        )

        return support_price

    def _get_stl_file_support_values(
        self, orientation: str = 'h-'
    ) -> t.Tuple[float, float, float]:
        support_values = super()._get_stl_file_support_values(orientation)

        support_volume, supported_area, supporting_area = support_values
        self.additional_info.update_orientation(
            orientation,
            volume=support_volume,
            area=(supported_area + supporting_area)
        )

        return support_values


class AdditionalInfoPartPriceCalculator(PartPriceCalculator):
    def __init__(
        self,
        stl_file: td.StlFile,
        stock_record: td.StockRecord,
        post_processings: td.PostProcessings,
        scale: float = 1.0,
        quantity: int = 1
    ):
        super().__init__(
            stl_file,
            stock_record,
            post_processings,
            scale,
            quantity
        )
        self._unit_price_calculator = AdditionalInfoUnitPriceCalculator(
            stl_file=stl_file,
            stock_record=stock_record,
            post_processings=post_processings,
            scale=scale,
            quantity=quantity
        )

    @property
    def additional_info(self) -> t.Dict[str, t.Any]:
        return dict(self._unit_price_calculator.additional_info)


class AdditionalPricingInfo(dict):
    def __init__(
        self,
        stock_record: td.StockRecord,
        stl_file: td.StlFile,
        scale: float
    ):
        super(AdditionalPricingInfo, self).__init__()
        self.stock_record = stock_record
        self.stl_file = stl_file
        self.scale = scale
        self.update(self._build_additional_info())

    def update_orientation(
        self,
        orientation: str,
        **kwargs: td.Number
    ) -> None:
        support_info = self['support']
        orientation_label = 'orientation{0}'.format(
            Orientation.ORIENTATIONS.index(orientation) + 1
        )
        support_info.setdefault(orientation_label, {}).update(kwargs)

    def _build_additional_info(self) -> t.Dict[str, t.Any]:
        parameter = self.stl_file.parameter
        model_volume = D(parameter.get_volume(self.scale))
        model_box_volume = D(parameter.get_bb_volume(self.scale))
        model_area = D(parameter.get_area(self.scale))
        model_shells_number = parameter.shells if parameter.shells else 1
        model_machine_volume = D(parameter.get_machine_volume(self.scale))

        additional_info = {
            "volume": model_volume,
            "box_volume": model_box_volume,
            "area": model_area,
            "shells_number": model_shells_number,
            "machine_volume": model_machine_volume
        }

        if self.stock_record.support_enabled:
            additional_info['support'] = {}
        else:
            dimensions = {
                'w': D(self.stl_file.parameter.get_bb_width(self.scale)),
                'd': D(self.stl_file.parameter.get_bb_depth(self.scale)),
                'h': D(self.stl_file.parameter.get_bb_height(self.scale))
            }
            additional_info.update(dimensions)
        return additional_info
