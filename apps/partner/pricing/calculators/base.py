from decimal import Decimal
import typing as t

import button3d.type_declarations as td

from apps.partner.pricing.price import Price


class AbstractPriceCalculator:
    """
    This class can be used as a base for all price calculators that return
    Price objects. Adds helper methods for building Price objects.
    """

    def __init__(
        self,
        currency: t.Optional[str] = None,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ):
        self._currency = currency
        self._shipping_country = shipping_country
        self._billing_country = billing_country
        self._is_b2b_transaction = is_b2b_transaction

    @property
    def partner(self) -> td.Partner:
        raise NotImplementedError('Subclasses must implement .partner')

    @property
    def zero(self) -> Price:
        return self.create_price(Decimal('0.00'))

    @property
    def currency(self) -> str:
        return self._currency or self.partner.price_currency

    @property
    def tax_rate(self) -> td.Decimal:
        if self.partner.should_remove_tax(
            self._shipping_country,
            self._billing_country,
            self._is_b2b_transaction
        ):
            return Decimal('0')
        return self.partner.org_options.vat_rate

    def create_price(self, excl_tax: td.Decimal) -> Price:
        price_object = self.partner.create_price(
            excl_tax=excl_tax,
            shipping_country=self._shipping_country,
            billing_country=self._billing_country,
            is_b2b_transaction=self._is_b2b_transaction
        )

        if self._currency is not None:
            return price_object.in_currency(self._currency)
        return price_object


def get_is_b2b_transaction(
        billing_address: t.Optional[td.Address]
) -> bool:

    address = billing_address
    return bool(address and address.company_name and address.vat_id)


def get_billing_and_shipping_country(
        billing_address: t.Optional[td.Address],
        shipping_address: t.Optional[td.Address],
) -> t.Tuple[t.Optional[td.Country], t.Optional[td.Country]]:
    shipping_country, billing_country = None, None

    if shipping_address:
        shipping_country = shipping_address.country

    if billing_address:
        billing_country = billing_address.country

    return shipping_country, billing_country
