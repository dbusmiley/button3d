import typing as t
from collections import namedtuple

import button3d.type_declarations as td
from apps.b3_voucher.models import Voucher

from apps.partner.pricing.calculators.base import AbstractPriceCalculator
from apps.partner.pricing.price import Price

OrderFeeApplication = namedtuple('OrderFeeApplication', ['name', 'amount'])


class OrderPriceCalculator(AbstractPriceCalculator):
    """
    This class is responsible for providing all information that is necessary
    for the order price breakdown. It is instantiated with a list of
    PartPriceCalculator instances that then are used to calculate the prices
    of the items the order contains. It applies voucher, extra fees and
    shipping to eventually obtain the total price of the order.
    """

    def __init__(
        self,
        parts: t.Sequence[td.PartPriceCalculator],
        shipping_method: td.ShippingMethod = None,
        voucher: t.Optional[td.Voucher] = None,
        currency: t.Optional[str] = None,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ):
        super().__init__(
            currency=currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

        if not parts:
            raise ValueError('parameter "parts" must not be empty.')

        partner = parts[0].partner
        if not all(item.partner == partner for item in parts):
            raise ValueError('All parts must be from the same partner.')

        self._parts = parts
        self._shipping_method = shipping_method
        self._voucher = voucher
        self._currency = currency

    @property
    def subtotal(self) -> td.Price:
        return Price.sum(*[part.item_total for part in self._parts])

    @property
    def shipping(self) -> td.Price:
        if not self._shipping_method:
            return self.zero
        return self.create_price(self._shipping_method.price_excl_tax)

    @property
    def voucher(self) -> td.Voucher:
        return self._voucher

    @property
    def voucher_discount(self) -> td.Price:
        voucher = self._voucher
        if not voucher:
            return self.zero

        base_total = Price.sum(
            self.subtotal,
            self.shipping,
            self.fees,
            self.min_price_diff
        )

        if voucher.action_type == Voucher.FIXED:
            discount = self.create_price(voucher.action_value)
        elif voucher.action_type == Voucher.PERCENTAGE:
            discount = base_total * (voucher.action_value / 100)
        else:
            raise ValueError("Invalid action type")

        if discount > base_total:
            discount = base_total
        return discount

    @property
    def fee_objects(self) -> t.Sequence[OrderFeeApplication]:
        return [
            OrderFeeApplication(
                name=fee.type,
                amount=self.create_price(fee.value)
            )
            for fee in self.partner.extra_fees.all()
        ]

    @property
    def fees(self) -> td.Price:
        return Price.sum(*[
            self.create_price(fee.value)
            for fee in self.partner.extra_fees.all()
        ])

    @property
    def min_price(self) -> td.Price:
        if not self.partner.minimum_order_price_enabled:
            return self.zero
        return self.create_price(
            self.partner.minimum_order_price_value
        )

    @property
    def min_price_diff(self) -> td.Price:
        base_total = self.subtotal + self.fees
        minimum_order_price = self.min_price
        if base_total < minimum_order_price:
            return minimum_order_price - base_total
        return self.zero

    @property
    def total(self) -> td.Price:
        return Price.sum(
            self.subtotal,
            self.shipping,
            self.fees,
            self.min_price_diff,
            -self.voucher_discount,
        )

    @property
    def partner(self) -> td.Partner:
        return self._parts[0].partner
