import typing as t
import button3d.type_declarations as td

from apps.partner.pricing.calculators import PartPriceCalculator, \
    OrderPriceCalculator


class LinePriceCalculator(PartPriceCalculator):
    """
    This class is a special case of PartPriceCalculator. It can be instantiated
    with a Line instance and extracts the necessary information from there.
    It also makes sure that the price of a line is not derived from its items
    if the line has been priced manually and instead returns the manually set
    price in this case.
    """

    def __init__(
        self,
        line: td.Line,
        currency: t.Optional[str] = None,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ):
        stl_file = line.stl_file
        stock_record = line.stockrecord
        post_processings = line.post_processings
        scale = line.scale
        quantity = line.quantity
        self.line = line

        super().__init__(
            stl_file=stl_file,
            stock_record=stock_record,
            post_processings=post_processings,
            scale=scale,
            quantity=quantity,
            currency=currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

    @property
    def unit_price(self) -> td.Price:
        if self._is_manual_price_set():
            return self.create_price(self.line.price_excl_tax)
        return super().unit_price

    @property
    def item_discount(self) -> td.Price:
        if self._is_manual_price_set():
            return self.zero
        return super().item_discount

    def _is_manual_price_set(self) -> bool:
        return self.line.basket.is_manually_priced \
            and self.line.has_manual_price_set


class ProjectPriceCalculator(OrderPriceCalculator):
    """
    This class is a special case of OrderPriceCalculator that uses
    LinePriceCalculators to calculate the price of its lines.
    """

    def __init__(
        self,
        project: td.Project,
        shipping_method: td.ShippingMethod = None,
        voucher: t.Optional[td.Voucher] = None,
        currency: t.Optional[str] = None,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ):
        lines = [
            LinePriceCalculator(
                line=line,
                currency=currency,
                shipping_country=shipping_country,
                billing_country=billing_country,
                is_b2b_transaction=is_b2b_transaction
            )
            for line in project.all_lines()
        ]
        super().__init__(
            parts=lines,
            shipping_method=shipping_method,
            voucher=voucher,
            currency=currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

    @property
    def lines(self):
        return self._parts

    @property
    def currency_supported(self):
        return self.currency in self.partner.supported_currencies
