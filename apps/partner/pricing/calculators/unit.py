from decimal import Decimal as D
import typing as t

import button3d.type_declarations as td

from apps.b3_core.utils import Orientation, eval_price_custom, \
    eval_price_support_custom, eval_price_orientation_custom, \
    MissingParameterException, DoesNotFitInBoundsException


class UnitPriceCalculator:
    """
    This class abstracts parts of the calculation of the unit price of a line.
    Basically it deals with our price formulas: it retrieves the parameters,
    which are used as variables in price formulas, from the StlFile, it
    selects the right price formulas to use and arranges their evalualtion.
    It also checks that the passed StlFile fits into the bounds of the
    specified StockRecord/StlFile.

    Instances of this class are meant to be used by PartPriceCalculator, which
    takes care of things like manual pricing, discounts and tax.
    """

    def __init__(
        self,
        stl_file: td.StlFile,
        stock_record: td.StockRecord,
        post_processings: td.PostProcessings,
        scale: float,
        quantity: int
    ):
        self.stl_file = stl_file
        self.stock_record = stock_record
        self.post_processings = post_processings
        self.scale = scale
        self.quantity = quantity

    def get_price(self) -> td.Decimal:
        if self.stl_file.parameter is None:
            raise MissingParameterException

        if not self._fits_in_bounds():
            raise DoesNotFitInBoundsException

        if not self.stock_record.support_enabled:
            return self._calculate_model_price()

        if not self.stock_record.price_orientation_custom:
            return self._calculate_model_price() \
                + self._calculate_support_price()

        return self._calculate_multi_orientation_price()

    def _calculate_model_price(self, orientation: str = 'h-') -> td.Decimal:
        parameter = self.stl_file.parameter
        model_volume = D(parameter.get_volume(self.scale))
        model_box_volume = D(parameter.get_bb_volume(self.scale))
        model_area = D(parameter.get_area(self.scale))
        model_shells_number = parameter.shells if parameter.shells else 1
        model_machine_volume = D(parameter.get_machine_volume(self.scale))
        width, depth, height = self._get_stl_file_dimensions(orientation)

        price_print = eval_price_custom(
            price_custom=self.stock_record.price_custom,
            quantity=self.quantity,
            model_volume=model_volume,
            box_volume=model_box_volume,
            area=model_area,
            shells_number=model_shells_number,
            width=width,
            height=height,
            depth=depth,
            machine_volume=model_machine_volume
        )

        price_post_processings = D('0.00')
        for post_processing in self.post_processings:
            price_post_processings += eval_price_custom(
                price_custom=post_processing.price_formula,
                quantity=self.quantity,
                model_volume=model_volume,
                box_volume=model_box_volume,
                area=model_area,
                shells_number=model_shells_number,
                width=width,
                height=height,
                depth=depth,
                machine_volume=model_machine_volume
            )

        return price_print + price_post_processings

    def _calculate_support_price(self, orientation: str = 'h-') -> td.Decimal:
        support_values = self._get_stl_file_support_values(orientation)
        support_volume, supported_area, supporting_area = support_values
        width, depth, height = self._get_stl_file_dimensions(orientation)

        return eval_price_support_custom(
            price_support_custom=self.stock_record.price_support_custom,
            quantity=self.quantity,
            support_volume=support_volume,
            support_area=(supported_area + supporting_area),
            width=width,
            depth=depth,
            height=height
        )

    def _calculate_multi_orientation_price(self) -> td.Decimal:
        model_costs = {
            orientation: self._calculate_model_price(orientation)
            for orientation in Orientation.ORIENTATIONS
        }
        support_costs = {
            orientation: self._calculate_support_price(orientation)
            for orientation in Orientation.ORIENTATIONS
        }

        orientation_formula = self.stock_record.price_orientation_custom
        return eval_price_orientation_custom(
            price_orientation_custom=orientation_formula,
            model_costs=model_costs,
            support_costs=support_costs
        )

    def _fits_in_bounds(self) -> bool:
        fits_in_stock_record = self.stock_record.fits_in_bounds(
            self.stl_file, self.scale
        )
        fits_in_post_processings = all(
            post_processing.fits_in_bounds(self.stl_file, self.scale)
            for post_processing in self.post_processings
        )

        return fits_in_stock_record and fits_in_post_processings

    def _get_stl_file_dimensions(
        self, orientation: str = 'h-'
    ) -> t.Tuple[td.Decimal, td.Decimal, td.Decimal]:
        dimensions = (
            D(self.stl_file.parameter.get_bb_width(self.scale)),
            D(self.stl_file.parameter.get_bb_depth(self.scale)),
            D(self.stl_file.parameter.get_bb_height(self.scale))
        )
        w, d, h = Orientation.ORIENTATION_TRANSLATION[orientation]
        return dimensions[w], dimensions[d], dimensions[h]

    def _get_stl_file_support_values(
        self, orientation: str = 'h-'
    ) -> t.Tuple[float, float, float]:
        support_values = self.stl_file.support_structure.get_values(
            max_angle=self.stock_record.support_angle,
            offset=self.stock_record.support_offset,
            scale=self.scale
        )
        return support_values[orientation]
