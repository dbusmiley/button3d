import typing as t
from decimal import Decimal as D

import button3d.type_declarations as td

from apps.b3_core.utils import currency_exchange, quantize


class Price:
    @classmethod
    def zero(cls, currency: str = None) -> td.Price:
        return cls(currency, excl_tax=0, tax=0)

    @classmethod
    def sum(cls, *prices: td.Price) -> td.Price:
        return sum(prices, cls.zero())

    def __init__(
        self,
        currency: str,
        excl_tax: td.Number,
        tax: t.Optional[td.Number] = None,
        tax_rate: t.Optional[td.Number] = None
    ):
        if tax is not None and tax_rate is not None:
            raise ValueError(
                'Only one of tax and tax_rate can be specified '
                'when creating a Price.'
            )

        self.currency = currency
        self.excl_tax = quantize(excl_tax)
        if tax is not None:
            self.tax = quantize(tax)
        elif tax_rate is not None:
            self.tax = quantize(self.excl_tax * (D(tax_rate) / 100))
        else:
            self.tax = None

    @property
    def incl_tax(self) -> t.Optional[td.Decimal]:
        if not self.is_tax_known:
            return None
        return self.excl_tax + self.tax

    @property
    def is_tax_known(self) -> bool:
        return self.tax is not None

    @property
    def tax_rate(self) -> t.Optional[td.Decimal]:
        if not self.is_tax_known:
            return None

        return quantize(self.tax / self.excl_tax) * 100

    def __repr__(self):
        return "%s(currency=%r, excl_tax=%r, incl_tax=%r, tax=%r)" % (
            self.__class__.__name__, self.currency, self.excl_tax,
            self.incl_tax, self.tax)

    def _check_comparable(self, other: td.Price) -> None:
        if not self.excl_tax:
            self.currency = other.currency
        elif not other.excl_tax:
            other.currency = self.currency
        if self.currency != other.currency:
            raise ValueError('Prices must have the same currency.')
        if self.is_tax_known != other.is_tax_known:
            raise ValueError(
                'Tax must be known for either both or none of the prices.'
            )

    def __eq__(self, other: td.Price) -> bool:
        """
        Two price objects are equal if currency, price.excl_tax and tax match.
        """
        self._check_comparable(other)
        return self.excl_tax == other.excl_tax and self.tax == other.tax

    def __lt__(self, other: td.Price) -> bool:
        self._check_comparable(other)
        return self.incl_tax < other.incl_tax

    def __gt__(self, other: td.Price) -> bool:
        self._check_comparable(other)
        return self.incl_tax > other.incl_tax

    def __neg__(self) -> td.Price:
        return Price(
            currency=self.currency,
            excl_tax=-self.excl_tax,
            tax=-self.tax if self.is_tax_known else None
        )

    def __add__(self, other: td.Price) -> td.Price:
        self._check_comparable(other)
        return Price(
            currency=self.currency,
            excl_tax=self.excl_tax + other.excl_tax,
            tax=self.tax + other.tax if self.is_tax_known else None
        )

    def __sub__(self, other: td.Price) -> td.Price:
        self._check_comparable(other)
        return Price(
            currency=self.currency,
            excl_tax=self.excl_tax - other.excl_tax,
            tax=self.tax - other.tax if self.is_tax_known else None
        )

    def __mul__(self, other: td.Number) -> td.Price:
        return Price(
            currency=self.currency,
            excl_tax=self.excl_tax * other,
            tax=self.tax * other if self.is_tax_known else None
        )
    __rmul__ = __mul__

    def in_currency(self, currency: str) -> td.Price:
        if currency is None:
            return Price(
                excl_tax=self.excl_tax,
                tax=self.tax,
                currency=self.currency
            )
        if not self.excl_tax:
            return Price(
                excl_tax=self.excl_tax,
                tax=self.tax,
                currency=currency
            )
        exchanged_excl_tax = currency_exchange(
            from_amount=self.excl_tax,
            from_c=self.currency,
            to_c=currency,
        )
        exchanged_tax = currency_exchange(
            from_amount=self.tax,
            from_c=self.currency,
            to_c=currency,
        )
        return Price(
            excl_tax=exchanged_excl_tax,
            tax=exchanged_tax,
            currency=currency
        )

    def as_dict(self, currency: t.Optional[str] = None) -> t.Dict[str, t.Any]:
        if currency:
            return self.in_currency(currency).as_dict()
        return {
            'currency': self.currency,
            'excl_tax': self.excl_tax,
            'tax': self.tax,
            'incl_tax': self.incl_tax,
            'is_tax_known': self.is_tax_known
        }

    def as_camel_case_dict(
        self,
        currency: t.Optional[str] = None
    ) -> t.Dict[str, t.Any]:
        underscore_dict = self.as_dict(currency=currency)
        return {
            'currency': underscore_dict['currency'],
            'exclusiveTax': underscore_dict['excl_tax'],
            'tax': underscore_dict['tax'],
            'inclusiveTax': underscore_dict['incl_tax'],
            'isTaxKnown': underscore_dict['is_tax_known']
        }
