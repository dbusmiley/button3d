from collections import defaultdict

from django.core.management import BaseCommand

PARENT, CHILD, STANDALONE = 'parent', 'child', 'standalone'
PUBLISHED, DRAFT = 0, 1


def check_upgrade_conditions(product_model, partner_model):
    def is_raw(product):
        return product.slug.endswith('-raw')

    def get_raw_stock_record(stock_records):
        for stock_record in stock_records:
            if stock_record.product.slug.endswith('-raw'):
                return stock_record
        return None

    errors = defaultdict(list)

    standalone_products = product_model.objects.filter(
        structure=STANDALONE
    )
    child_products = product_model.objects.filter(
        structure=CHILD
    )
    parent_products = product_model.objects.filter(
        structure=PARENT
    )

    if standalone_products:
        errors['standalone'] = [product for product in standalone_products]

    for child_product in child_products:
        if child_product.children.count():
            errors['child_has_children'].append(child_product)
        if child_product.parent is None:
            errors['child_has_no_parent'].append(child_product)

    for parent_product in parent_products:
        if parent_product.parent:
            errors['parent_has_parent'].append(parent_product)

        if parent_product.children.count() == 1:
            continue

        raw_children = [
            child for child in parent_product.children.all() if is_raw(child)
        ]
        if len(raw_children) == 0:
            errors['no_raw_child'].append(parent_product)
        elif len(raw_children) > 1:
            errors['multiple_raw_children'].append(
                (parent_product, raw_children)
            )
        for child in parent_product.children.all():
            if is_raw(child):
                continue
            if not child.title_en.startswith(parent_product.title_en):
                errors['inconsistent_title_en'].append(child)
            elif not child.title_en[len(parent_product.title_en):].strip():
                errors['empty_title_en'].append(child)
            if not child.title_de.startswith(parent_product.title_de):
                errors['inconsistent_title_de'].append(child)
            elif not child.title_de[len(parent_product.title_de):].strip():
                errors['empty_title_de'].append(child)

    for partner in partner_model.objects.filter(deleted_date=None):
        stock_record_groups = defaultdict(list)
        for stock_record in partner.stockrecords.filter(deleted_date=None):
            if stock_record.product.structure != CHILD:
                errors['sr_invalid_structure'].append(stock_record)
                continue
            stock_record_groups[stock_record.product.parent].append(
                stock_record
            )
        for parent, group in stock_record_groups.items():
            if len(group) == 1:
                continue
            raw_stock_record = get_raw_stock_record(group)
            has_published_offers = any(
                stock_record.edit_status == PUBLISHED
                for stock_record in group
            )
            if raw_stock_record is None:
                errors['sr_no_raw_offer'].append((partner, parent))
            elif has_published_offers and \
                    raw_stock_record.edit_status == DRAFT:
                errors['sr_raw_unpublished'].append((partner, parent))

    return not errors, errors


class Command(BaseCommand):
    def handle(self, *args, **options):
        from apps.catalogue.models.product import Product as product_model
        from apps.partner.models import Partner as partner_model
        valid, errors = check_upgrade_conditions(product_model, partner_model)

        if valid:
            self.stdout.write(
                'No conflicts found. Upgrade should be possible.'
            )
        else:
            self.stderr.write(
                'Upgrade not possible due to the following errors:\n\n'
            )

        if errors['standalone']:
            self.stderr.write(
                'Standalone products found. Please remove any Product with '
                'structure "standalone" or correct their structure '
                'in order to upgrade.\n'
            )
            self.stderr.write(
                '\t' + '\n\t'.join(
                    product.title for product in errors['standalone']
                )
            )
            self.stderr.write('\n')

        if errors['parent_has_parent'] or errors['child_has_children']:
            self.stderr.write(
                'The only type of product that can have children is a product '
                'with structure "parent". The only type of product that can '
                'have a parent is a product with structure "child". Also, '
                'every product with structure "child" must have a parent. '
                'Please correct the errors below.'
            )
        if errors['parent_has_parent']:
            self.stderr.write('The following parent products have a parent:')
            self.stderr.write(
                '\t' + '\n\t'.join(
                    product.title for product in errors['parent_has_parent']
                )
            )
            self.stderr.write('\n')

        if errors['child_has_children']:
            self.stderr.write('The following child products have children:')
            self.stderr.write(
                '\t' + '\n\t'.join(
                    product.title for product in errors['child_has_children']
                )
            )
            self.stderr.write('\n')

        if errors['child_has_no_parent']:
            self.stderr.write('The following child products have no parent:')
            self.stderr.write(
                '\t' + '\n\t'.join(
                    product.title for product in errors['child_has_no_parent']
                )
            )
            self.stderr.write('\n')

        if errors['no_raw_child'] or errors['multiple_raw_children']:
            self.stderr.write(
                'Products with ambiguous "raw" child product found. '
                'Each product with multiple child products needs exactly one '
                'child product with a slug ending in "-raw" which indicates '
                'the child that represents the raw material.\n'
            )
        if errors['no_raw_child']:
            self.stderr.write(
                'The following parent products have no "raw" children:\n'
            )
            self.stderr.write(
                '\t' + '\n\t'.join(
                    product.title for product in errors['no_raw_child']
                )
            )
            self.stderr.write('\n')

        if errors['multiple_raw_children']:
            self.stderr.write(
                'The following parent products have multiple "raw" children:\n'
            )
            self.stderr.write(
                '\t' + '\n\t'.join(
                    '{0}: {1}'.format(
                        product.title,
                        ', '.join(child.title for child in children)
                    ) for product, children in errors['multiple_raw_children']
                )
            )
            self.stderr.write('\n')

        if errors['inconsistent_title_en'] or errors['empty_title_en'] or \
                errors['inconsistent_title_de'] or errors['empty_title_de']:
            self.stderr.write(
                'Some child products in the database have a title that does '
                'not match the title of the parent product. Titles of child '
                'products must start exactly with the title of their parent.\n'
            )

        if errors['inconsistent_title_en'] or errors['inconsistent_title_de']:
            self.stderr.write(
                'The following child products\' titles don\'t match the '
                'title of their parent:'
            )
            if errors['inconsistent_title_en']:
                self.stderr.write(
                    '(en)\t' + '\n\t'.join(
                        product.title_en
                        for product in errors['inconsistent_title_en']
                    )
                )
            if errors['inconsistent_title_de']:
                self.stderr.write(
                    '(de)\t' + '\n\t'.join(
                        '{0} ({1})'.format(product.title_de, product.title_en)
                        for product in errors['inconsistent_title_de']
                    )
                )
            self.stderr.write('\n')

        if errors['empty_title_en'] or errors['empty_title_de']:
            self.stderr.write(
                'The following child products\' titles would be empty after '
                'removing title of their parent (i.e. they almost exactly '
                'match their parents titles):'
            )
            if errors['empty_title_en']:
                self.stderr.write(
                    '(en)\t' + '\n\t'.join(
                        product.title_en
                        for product in errors['empty_title_en']
                    )
                )
            if errors['empty_title_de']:
                self.stderr.write(
                    '(de)\t' + '\n\t'.join(
                        '{0} ({1})'.format(product.title_de, product.title_en)
                        for product in errors['empty_title_de']
                    )
                )
            self.stderr.write('\n')

        if errors['sr_invalid_structure']:
            self.stderr.write(
                'StockRecords must always link to a product with structure '
                '"child". The following StockRecords are linked to a product '
                'that is not a child:'
            )
            self.stderr.write(
                '\t' + '\n\t'.join(
                    '{0}: {1}'.format(
                        stock_record.partner.name,
                        stock_record.product.title
                    ) for stock_record in errors['sr_invalid_structure']
                )
            )
            self.stderr.write('\n')

        if errors['sr_no_raw_offer'] or errors['sr_raw_unpublished']:
            self.stderr.write(
                'If a printing service offers a post-processed product, '
                'he must also offer the corresponding raw product. This is '
                'not the case for all services/products.'
            )
            if errors['sr_no_raw_offer']:
                self.stderr.write(
                    'The following products are offered with '
                    'post-processings, while the corresponding raw product '
                    'is not offered. Please create an offer for the raw '
                    'product and publish it:'
                )
                self.stderr.write(
                    '\t' + '\n\t'.join(
                        '{0}: {1}'.format(partner.name, product.title)
                        for partner, product in errors['sr_no_raw_offer']
                    )
                )
                self.stderr.write('\n')

            if errors['sr_raw_unpublished']:
                self.stderr.write(
                    'For the following products, a StockRecord with the '
                    'raw product does exist, but it is unpublished. Please '
                    'publish the corresponding StockRecord:'
                )
                self.stderr.write(
                    '\t' + '\n\t'.join(
                        '{0}: {1}'.format(partner.name, product.title)
                        for partner, product in errors['sr_raw_unpublished']
                    )
                )
                self.stderr.write('\n')
