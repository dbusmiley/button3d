# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-07-18 14:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0024_auto_20180712_1149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='is_oscarapi',
            field=models.NullBooleanField(default=True),
        ),
    ]
