# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-03-10 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0011_auto_20180310_1319'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='payment_conditions',
            field=models.CharField(default='Payment terms: Credit Card or 14 days after invoicing', max_length=1000),
        ),
        migrations.AlterField(
            model_name='partner',
            name='payment_conditions_de',
            field=models.CharField(default='Payment terms: Credit Card or 14 days after invoicing', max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='payment_conditions_en',
            field=models.CharField(default='Payment terms: Credit Card or 14 days after invoicing', max_length=1000, null=True),
        ),
    ]
