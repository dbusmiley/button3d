# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-06-15 08:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0021_merge_20180605_1242'),
        ('partner', '0022_post_processing_data_migration'),
    ]

    operations = [
    ]
