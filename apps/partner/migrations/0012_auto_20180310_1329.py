# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-10 13:29
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0011_auto_20180228_1645'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='partner',
            managers=[
                ('all_objects', django.db.models.manager.Manager()),
            ],
        ),
    ]
