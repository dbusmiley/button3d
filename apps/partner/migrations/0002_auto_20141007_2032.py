# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stockrecord',
            name='price_currency',
            field=models.CharField(default='EUR', max_length=12, verbose_name='Currency'),
        ),
    ]
