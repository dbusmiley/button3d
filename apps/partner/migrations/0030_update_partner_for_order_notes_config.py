# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-08-27 13:26
from __future__ import unicode_literals

from django.db import migrations, models
from django_add_default_value import AddDefaultValue


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0029_auto_20180912_0955'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='delivery_note_prefix',
            field=models.CharField(default='DL-', max_length=10, verbose_name='delivery note prefix'),
        ),
        migrations.AddField(
            model_name='partner',
            name='general_sales_conditions',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='general_shipping_conditions',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='partner',
            name='invoice_prefix',
            field=models.CharField(default='IN-', max_length=10,
                                   verbose_name='invoice prefix'),
        ),
        migrations.AddField(
            model_name='partner',
            name='order_confirmation_prefix',
            field=models.CharField(default='OC-', max_length=10,
                                   verbose_name='order confirmation prefix'),
        ),
        migrations.AddField(
            model_name='partner',
            name='footer_company_information',
            field=models.TextField(blank=True, null=True),
        ),
        AddDefaultValue(
            model_name='partner',
            name='invoice_prefix',
            value='IN-'
        ),
        AddDefaultValue(
            model_name='partner',
            name='order_confirmation_prefix',
            value='OC-',
        ),
        AddDefaultValue(
            model_name='partner',
            name='delivery_note_prefix',
            value='DL-'
        ),
    ]
