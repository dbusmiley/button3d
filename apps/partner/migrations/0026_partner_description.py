# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-07 14:23
from __future__ import unicode_literals

from django.db import migrations, models
from django_add_default_value import AddDefaultValue


class Migration(migrations.Migration):

    dependencies = [
        ('partner', '0025_auto_20180718_1450'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='description',
            field=models.CharField(default='', max_length=1000),
        ),
        AddDefaultValue(
            model_name='partner',
            name='description',
            value=''
        ),
    ]
