# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import defaultdict
from contextlib import suppress
from uuid import uuid4

from django.db import migrations
from django.db.models import Manager
from django.utils import timezone
from django.utils.text import slugify

from apps.partner.management.commands.check_upgrade_conditions import \
    check_upgrade_conditions

PARENT, CHILD, STANDALONE = 'parent', 'child', 'standalone'
PUBLISHED, DRAFT = 0, 1


def create_post_processings_from_stock_records(product_model, partner_model):
    def get_raw_stock_record(stock_records):
        for sr in stock_records:
            if sr.product.slug.endswith('-raw') and sr.deleted_date is None:
                return sr

    for partner in partner_model.objects.filter(deleted_date=None):
        stock_record_groups = defaultdict(list)
        for stock_record in partner.stockrecords.all():
            stock_record_groups[stock_record.product.parent].append(
                stock_record
            )

        raw_stock_record_map = {}
        for product, group in stock_record_groups.items():
            raw_stock_record = get_raw_stock_record(group)
            if raw_stock_record is None:
                published_group = [s for s in group if s.deleted_date is None]
                if len(published_group) == 1:
                    finishing = published_group[0]
                elif not published_group and group:
                    finishing = group[0]
                else:
                    raise ValueError()

                raw_stock_record = partner.stockrecords.get(pk=finishing.pk)
                raw_stock_record.pk = None
                raw_stock_record.save()

            raw_stock_record_map[raw_stock_record] = [
                stock_record for stock_record in group
                if stock_record != raw_stock_record
            ]

        for raw_stock_record, finishings in raw_stock_record_map.items():
            parent_product = raw_stock_record.product.parent
            if parent_product is None \
                and raw_stock_record.product.structure == PARENT \
                    and raw_stock_record.deleted_date:
                parent_product = raw_stock_record.product

            raw_stock_record.product = parent_product
            raw_stock_record.combinable_post_processings = False
            raw_stock_record.partner_sku = slugify(
                f'{raw_stock_record.partner.code}'
                f'_{raw_stock_record.product.slug}'
            )
            raw_stock_record.save()

            for stock_record in finishings:
                product = stock_record.product
                parent_title_len = len(parent_product.title_en)
                title = product.title_en[parent_title_len:].strip()
                description = product.description_en or product.description
                published = stock_record.edit_status == PUBLISHED
                if stock_record.deleted_date:
                    slug = str(uuid4())
                else:
                    slug = slugify(title)

                if None in [
                    raw_stock_record.min_production_days,
                    raw_stock_record.max_production_days,
                    stock_record.min_production_days,
                    stock_record.max_production_days
                ]:
                    prod_days_min, prod_days_max = 0, 0
                else:
                    prod_days_min = \
                        stock_record.min_production_days \
                        - raw_stock_record.min_production_days
                    prod_days_max = \
                        stock_record.max_production_days \
                        - raw_stock_record.max_production_days
                    if prod_days_min < 0 or prod_days_max < 0:
                        prod_days_min, prod_days_max = 0, 0

                if stock_record.price_custom == raw_stock_record.price_custom \
                    or not stock_record.price_custom \
                    or not raw_stock_record.price_custom:
                    price_formula = '0'
                else:
                    price_formula = '({0}) - ({1})'.format(
                        stock_record.price_custom,
                        raw_stock_record.price_custom
                    )

                manually_priced = \
                    stock_record.always_priced_manually \
                    or stock_record.rules.filter(
                        conclusion="manual_pricing"
                    ).exists()

                post_processing = raw_stock_record.post_processings.create(
                    slug=slug,
                    published=published,
                    title=title,
                    description=description,
                    price_formula=price_formula,
                    production_days_min=prod_days_min,
                    production_days_max=prod_days_max,
                    always_priced_manually=manually_priced,
                    bounds_x_min=stock_record.min_x_mm,
                    bounds_x_max=stock_record.max_x_mm,
                    bounds_y_min=stock_record.min_y_mm,
                    bounds_y_max=stock_record.max_y_mm,
                    bounds_z_min=stock_record.min_z_mm,
                    bounds_z_max=stock_record.max_z_mm,
                    datasheet_pdf_de=stock_record.datasheet_pdf_de,
                    datasheet_pdf_en=stock_record.datasheet_pdf_en,
                )
                post_processing.colors.add(*stock_record.colors.all())

                for basket_line in stock_record.basket_lines.all():
                    basket_line.stock_record = raw_stock_record
                    basket_line.post_processing_options.create(
                        post_processing=post_processing,
                        color=basket_line.color
                    )
                    basket_line.save()

                for order_line in stock_record.line_set.all():
                    order_line.stock_record = raw_stock_record
                    order_line.post_processing_options.create(
                        post_processing=post_processing,
                        color=order_line.color
                    )
                    order_line.save()

                for manual_request in stock_record.manualrequest_set.all():
                    manual_request.stock_record = raw_stock_record
                    manual_request.post_processing_options.create(
                        post_processing=post_processing,
                        color=manual_request.color
                    )
                    manual_request.save()

                if stock_record.deleted_date:
                    post_processing.deleted_date = timezone.now()
                    post_processing.slug = f'{post_processing.id}-deleted'
                    post_processing.save()
                else:
                    stock_record.deleted_date = timezone.now()
                    stock_record.save()

    for product in product_model.objects.filter(structure=PARENT):
        product.structure = STANDALONE
        product.save()
        for child in product.children.all():
            for stock_record in child.stockrecords.all():
                stock_record.product = product
                stock_record.save()
            for manual_request in child.manualrequest_set.all():
                manual_request.product = product
                manual_request.save()
            for basket_line in child.basket_lines.all():
                basket_line.product = product
                basket_line.save()
            for order_line in child.line_set.all():
                order_line.product = product
                order_line.save()

            with suppress(AttributeError):
                child.organizationproductoption_set.all().delete()
            with suppress(AttributeError):
                child.rangeproduct_set.all().delete()
            with suppress(AttributeError):
                child.stats.delete()
            child.delete()


def forwards(apps, schema_editor):
    product_model = apps.get_model('catalogue', 'Product')
    partner_model = apps.get_model('partner', 'Partner')
    product_model.objects = Manager()
    product_model.objects.model = product_model
    partner_model.objects = Manager()
    partner_model.objects.model = partner_model

    valid, errors = check_upgrade_conditions(product_model, partner_model)
    if not valid:
        raise ValueError(
            'Not all conditions that are necessary for the upgrade are '
            'met. Please run "check_upgrade_conditions" for further '
            'details.'
        )

    create_post_processings_from_stock_records(product_model, partner_model)


class Migration(migrations.Migration):
    dependencies = [
        ('partner', '0021_auto_20180604_0900'),
        ('b3_manual_request', '0003_auto_20180310_1737'),
        ('basket', '0006_auto_20180310_1737'),
        ('catalogue', '0010_auto_20180317_1101'),
        ('order', '0007_oscar_address_to_b3_and_additional'),
    ]

    operations = [
        migrations.RunPython(forwards, migrations.RunPython.noop)
    ]
