from apps.partner.models import *
from django.core.exceptions import ValidationError


def validate_stockrecords():
    result = []
    partners = list(Partner.objects.all())
    for partner in partners:
        partner_errors = validate_stockrecords_by_partner(partner.id)
        if len(partner_errors):
            result.append((partner, partner_errors))
    return result


def validate_stockrecords_by_partner(partner_id):
    result = []
    stockrecords = list(StockRecord.objects.filter(partner_id=partner_id))
    for stockrecord in stockrecords:
        try:
            stockrecord.full_clean()

            errors = {}
            # validate Only one strockrecord for each product per partner
            if len(list(StockRecord.objects
                        .filter(partner_id=stockrecord.partner_id,
                                product_id=stockrecord.product_id))) > 1:
                errors["duplicate_stockrecord"] = "Duplicate stockrecord " \
                                                  "with the same product " \
                                                  "and partner"

            if len(errors):
                raise ValidationError(errors)

        except Exception as e:
            result.append((stockrecord, e))
    return result
