# -*- coding: utf-8 -*-

from ckeditor.widgets import CKEditorWidget
from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from django.forms import BaseInlineFormSet
from django.utils.decorators import method_decorator
from django.utils.translation import activate, get_language, ugettext, \
    ugettext_lazy as _
from django.views.decorators.csrf import csrf_protect
from modeltranslation.admin import TranslationAdmin

from apps.b3_address.models import Address
from apps.b3_core.admin import DeletedDateAdmin
from apps.partner.models import PostProcessing, Partner, StockRecord

csrf_protect_m = method_decorator(csrf_protect)


class StockRecordAdmin(DeletedDateAdmin):
    list_display = ('product', 'partner', 'partner_sku', 'edit_status')
    list_filter = ('partner',)
    exclude = ('datasheet_pdf', 'colors')

    def get_object(self, request, object_id, from_field=None):
        # Hook obj for use in formfield_for_manytomany
        self.obj = super(StockRecordAdmin, self).get_object(request, object_id,
                                                            from_field)
        return self.obj

    def get_queryset(self, request):
        # use our manager, rather than the default one
        qs = StockRecord.all_objects.get_queryset()

        ordering = self.get_ordering(request)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class UserWithEmail(User):
    def get_username(self):
        user_profile = hasattr(self, 'userprofile') \
            and hasattr(self.userprofile, 'site') \
            and self.userprofile.site is not None
        site_name = self.userprofile.site.domain if user_profile else 'None'
        return '{0} - {1} (site:{2})'.format(self.username, self.email,
                                             site_name)

    class Meta:
        proxy = True


class PartnerAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PartnerAdminForm, self).__init__(*args, **kwargs)
        self.fields['bluebox_preview_step_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['bluebox_preview_step_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['terms_conditions_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['terms_conditions_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['thank_you_text_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['thank_you_text_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        try:
            self.fields['users'].queryset = UserWithEmail.objects.filter(
                userprofile__site__id=kwargs['instance'].site.id
            ).select_related('userprofile__site')
        except (KeyError, AttributeError):
            # When creating a new partner, kwargs['instance'] leads to KeyError
            pass

    def clean(self):
        cleaned_data = super(PartnerAdminForm, self).clean()
        partner_site = cleaned_data.get('site')
        users = cleaned_data.get('users')

        if len([u for u in users if u.userprofile.site != partner_site]) != 0:
            self.add_error('users', _(
                'Selected users are not all in the same site as the partner'))

        return cleaned_data

    def full_clean(self):
        super(PartnerAdminForm, self).full_clean()
        self._errors.pop('logo', None)
        self._errors.pop('logo_temp', None)

    class Meta:
        model = Partner
        fields = '__all__'


class RequiredInlineFormSet(BaseInlineFormSet):
    """
    Generates an inline formset that is required
    """

    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form


class AddressInline(admin.StackedInline):
    model = Address
    max_num = 1
    can_delete = False
    formset = RequiredInlineFormSet
    fieldsets = (
        (None, {
            'fields': (
                ('title', 'first_name', 'last_name'),
                ('company_name', 'vat_id', 'department'),
                'line1',
                'line2',
                ('zip_code', 'city', 'country'),
                'phone_number',
            )
        }),)


class PartnerAdmin(TranslationAdmin, DeletedDateAdmin):
    form = PartnerAdminForm
    list_display = ('name', 'code', 'site')
    search_fields = ['name', 'code', 'site__name']

    fieldsets = (
        (None, {
            'fields': (
                'name',
                'site',
                'users',
                ('website', 'email')
            )
        }),
        ('Payment Options', {
            'classes': ('collapse',),
            'fields': (
                'price_currency',
                'tax_type',
                'vat_rate',
                'payment_conditions_en',
                'payment_conditions_de',
                'minimum_order_price_enabled',
                'minimum_order_price_value'
            )
        }),
        ('Rating', {
            'classes': ('collapse',),
            'fields': (
                (
                    'rating_quality',
                    'rating_reliability',
                    'rating_service'
                ),
            )
        }),
        ('Information Box on Checkout Page', {
            'classes': ('collapse',),
            'fields': (
                (
                    'bluebox_preview_step_en',
                    'bluebox_preview_step_de'
                )
            )
        }),
        ('Terms and Conditions', {
            'classes': ('collapse',),
            'fields': (
                (
                    'terms_conditions_en',
                    'terms_conditions_de'
                )
            )
        }),
        ('Thank You Text', {
            'classes': ('collapse',),
            'fields': (
                (
                    'thank_you_text_en',
                    'thank_you_text_de'
                )
            )
        }),
        ('Advanced Options', {
            'classes': ('collapse',),
            'fields': (
                'vat_id',
                ('logo', 'logo_temp'),
                'mes_activated',
                'certifications_pdf',
                'webhook'
            )
        })
    )
    inlines = [
        AddressInline,
    ]

    def get_queryset(self, request):
        return Partner.all_objects.all()

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        formfield = super(PartnerAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs)
        if db_field.name == 'users':
            formfield.queryset = UserWithEmail.objects.select_related(
                'userprofile', 'userprofile__site').all()
        return formfield

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(PartnerAdmin, self).formfield_for_dbfield(db_field,
                                                                **kwargs)

        if db_field.name == 'bluebox_preview_step_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext(
                "3YOURMIND is only the transmitter of this order. Your order "
                "is processed by the selected 3D printing service, from which "
                "you'll receive a separate order confirmation and invoice.")
            activate(current_lang)
        elif db_field.name == 'terms_conditions_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext(
                "I've read and accept the <a href='"
                "https://www.3yourmind.com/terms' target='_blank'>"
                "Terms & Conditions</a>")
            activate(current_lang)
        elif db_field.name == 'thank_you_text_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext(
                "The manufacturing process will start soon. To make sure you "
                "receive best quality parts our 3D printing experts will "
                "manually review the designs before production.")
            activate(current_lang)

        return field


class PostProcessingAdmin(admin.ModelAdmin):
    readonly_fields = ['stock_record']


admin.site.register(Partner, PartnerAdmin)
admin.site.register(StockRecord, StockRecordAdmin)
admin.site.register(PostProcessing, PostProcessingAdmin)
