import typing as t
from decimal import Decimal
from decimal import Decimal as D

from apps.b3_core.utils import quantize
from apps.basket.models import Line


class AbstractDiscountAction(object):
    def __init__(self, **kwargs):
        self.args = kwargs

    def apply_to(self, obj, simulate=False):
        if isinstance(obj, Line):
            return self.apply_to_line(obj, simulate)
        else:
            raise ValueError("obj must be a Line")

    def get_discountable_amount(self, obj):
        return self.apply_to(obj, simulate=True)

    def get_amount(self, base_price: t.Optional[Decimal] = None) -> Decimal:
        raise NotImplementedError(
            'Subclasses of AbstractDiscountAction must implement get_amount().'
        )


class PercentageDiscountAction(AbstractDiscountAction):
    def get_amount(self, base_price: t.Optional[Decimal] = None) -> Decimal:
        if base_price is None:
            raise ValueError(
                'base_price must be passed to PercentageDiscountAction '
                'in order to apply the discount.'
            )

        return base_price * self._discount_rate()

    def _discount_rate(self):
        return D(self.args["rate"])

    def apply_to_line(self, line, simulate):
        line_price = line.line_price_excl_tax
        discount_amount = quantize(line_price * self._discount_rate())
        if simulate:
            return min(discount_amount, line_price)
        return line.apply_discount(discount_amount)


class FixedDiscountAction(AbstractDiscountAction):
    def _total_discount_amount(self):
        return D(self.args["amount"])

    def get_amount(self, base_price: t.Optional[Decimal] = None) -> Decimal:
        return self._total_discount_amount()

    def apply_to_line(self, line, simulate):
        discount_amount = quantize(self._total_discount_amount())
        if simulate:
            line_price = line.line_price_excl_tax_incl_discounts
            return min(discount_amount, line_price)
        return line.apply_discount(discount_amount)


# Line*DiscountAction and Basket*DiscountAction are still referenced by
# PartnerRule and StockRecordRule instances in the Database. Before cleaning
# up these instances, the names must stay in this dictionary
conclusion_action_classes = {
    'LinePercentageDiscountAction': PercentageDiscountAction,
    'BasketPercentageDiscountAction': PercentageDiscountAction,
    'LineFixedDiscountAction': FixedDiscountAction,
    'BasketFixedDiscountAction': FixedDiscountAction,
    'PercentageDiscountAction': PercentageDiscountAction,
    'FixedDiscountAction': FixedDiscountAction
}
