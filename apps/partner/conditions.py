import numbers


class RuleCondition(object):
    required_args = []

    def __init__(self, **kwargs):
        for name, value in kwargs.items():
            if name not in self.required_args:
                raise ValueError("argument {0} is not required.".format(name))
            if not isinstance(value, numbers.Number):
                raise ValueError("argument {0} is not a number.".format(name))
        if len(kwargs.keys()) != len(self.required_args):
            raise ValueError("not all required arguments were specified.")

        self.args = kwargs


class PriceGreaterCondition(RuleCondition):
    required_args = ["max_price"]

    def is_applying(self, **kwargs):
        if "price" not in kwargs:
            return False
        return kwargs["price"] > self.args["max_price"]


class PriceSmallerCondition(RuleCondition):
    required_args = ["min_price"]

    def is_applying(self, **kwargs):
        if "price" not in kwargs:
            return False
        return kwargs["price"] < self.args["min_price"]


class PriceInRangeCondition(PriceGreaterCondition, PriceSmallerCondition):
    required_args = PriceSmallerCondition.required_args \
                    + PriceGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "price" not in kwargs:
            return False
        return not (PriceSmallerCondition.is_applying(self, **kwargs) or
                    PriceGreaterCondition.is_applying(self, **kwargs))


class PriceOutsideRangeCondition(PriceInRangeCondition):
    required_args = PriceSmallerCondition.required_args \
                    + PriceGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "price" not in kwargs:
            return False
        return \
            not super(PriceOutsideRangeCondition, self).is_applying(**kwargs)


class QuantityGreaterCondition(RuleCondition):
    required_args = ["max_quantity"]

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs:
            return False
        return kwargs["quantity"] > self.args["max_quantity"]


class QuantitySmallerCondition(RuleCondition):
    required_args = ["min_quantity"]

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs:
            return False
        return kwargs["quantity"] < self.args["min_quantity"]


class QuantityInRangeCondition(QuantityGreaterCondition,
                               QuantitySmallerCondition):
    required_args = QuantitySmallerCondition.required_args + \
                    QuantityGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs:
            return False
        return not (QuantitySmallerCondition.is_applying(self, **kwargs) or
                    QuantityGreaterCondition.is_applying(self, **kwargs))


class QuantityOutsideRangeCondition(QuantityInRangeCondition):
    required_args = QuantitySmallerCondition.required_args + \
                    QuantityGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs:
            return False
        return not super(QuantityOutsideRangeCondition, self)\
            .is_applying(**kwargs)


class PriceQuantityProductGreaterCondition(RuleCondition):
    required_args = ["max_price_quantity_product"]

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs or "price" not in kwargs:
            return False
        calculated_price = kwargs["price"] * kwargs["quantity"]
        max_price_quantity_product = self.args["max_price_quantity_product"]
        return calculated_price > max_price_quantity_product


class PriceQuantityProductSmallerCondition(RuleCondition):
    required_args = ["min_price_quantity_product"]

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs or "price" not in kwargs:
            return False
        calculated_price = kwargs["price"] * kwargs["quantity"]
        max_price_quantity_product = self.args["min_price_quantity_product"]
        return calculated_price < max_price_quantity_product


class PriceQuantityProductInRangeCondition(
    PriceQuantityProductGreaterCondition, PriceQuantityProductSmallerCondition
):
    required_args = PriceQuantityProductSmallerCondition.required_args + \
                    PriceQuantityProductGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs or "price" not in kwargs:
            return False
        return not (
            PriceQuantityProductSmallerCondition.is_applying(self, **kwargs) or
            PriceQuantityProductGreaterCondition.is_applying(self, **kwargs)
        )


class PriceQuantityProductOutsideRangeCondition(
    PriceQuantityProductInRangeCondition
):
    required_args = PriceQuantityProductSmallerCondition.required_args + \
                    PriceQuantityProductGreaterCondition.required_args

    def is_applying(self, **kwargs):
        if "quantity" not in kwargs or "price" not in kwargs:
            return False
        return not super(PriceQuantityProductOutsideRangeCondition, self)\
            .is_applying(**kwargs)


condition_classes = {
    'PriceGreaterCondition': PriceGreaterCondition,
    'PriceSmallerCondition': PriceSmallerCondition,
    'PriceInRangeCondition': PriceInRangeCondition,
    'PriceOutsideRangeCondition': PriceOutsideRangeCondition,
    'QuantityGreaterCondition': QuantityGreaterCondition,
    'QuantitySmallerCondition': QuantitySmallerCondition,
    'QuantityInRangeCondition': QuantityInRangeCondition,
    'QuantityOutsideRangeCondition': QuantityOutsideRangeCondition,
    'PriceQuantityProductGreaterCondition':
        PriceQuantityProductGreaterCondition,
    'PriceQuantityProductSmallerCondition':
        PriceQuantityProductSmallerCondition,
    'PriceQuantityProductInRangeCondition':
        PriceQuantityProductInRangeCondition,
    'PriceQuantityProductOutsideRangeCondition':
        PriceQuantityProductOutsideRangeCondition,
}
