from apps.b3_core.utils import fit_in_bounds
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import StockRecordFactory
from django.core.files.uploadedfile import SimpleUploadedFile


class PartnerStockRecordCase(TestCase):
    # Helper
    def get_bounds_json(self, stl_file_dimensions, scale):
        scale = float(scale)
        dim_x_mm = stl_file_dimensions['w'] * scale
        dim_y_mm = stl_file_dimensions['d'] * scale
        dim_z_mm = stl_file_dimensions['h'] * scale
        return [dim_x_mm, dim_y_mm, dim_z_mm]

    def setUp(self):
        super(PartnerStockRecordCase, self).setUp()

        self.stockrecord = {
            'min_x_mm': 30,
            'max_x_mm': 600,
            'min_y_mm': None,
            'max_y_mm': 340,
            'min_z_mm': None,
            'max_z_mm': 340,
        }

        # LittleShip Model
        self.stl_file = {
            'w': 29.878,
            'h': 97.856,
            'd': 144.6
        }

        self.min_allowed_bounds = [self.stockrecord['min_x_mm'],
                                   self.stockrecord['min_y_mm'],
                                   self.stockrecord['min_z_mm']]

        self.max_allowed_bounds = [self.stockrecord['max_x_mm'],
                                   self.stockrecord['max_y_mm'],
                                   self.stockrecord['max_z_mm']]

    def test_fit_in_bounds(self):
        self.assertTrue(
            fit_in_bounds(self.min_allowed_bounds, self.max_allowed_bounds,
                          self.get_bounds_json(
                              stl_file_dimensions=self.stl_file, scale=1.3)))

        self.assertTrue(
            fit_in_bounds(self.min_allowed_bounds, self.max_allowed_bounds,
                          self.get_bounds_json(
                              stl_file_dimensions=self.stl_file, scale=1.0)))

    def test_doesnt_fit_in_bounds(self):
        self.assertFalse(
            fit_in_bounds(self.min_allowed_bounds, self.max_allowed_bounds,
                          self.get_bounds_json(
                              stl_file_dimensions=self.stl_file, scale=0.2)))


class StockRecordDataSheetTest(TestCase):
    def setUp(self):
        super().setUp()
        self.stock_record = StockRecordFactory()
        self.stock_record.datasheet_pdf_en = SimpleUploadedFile(
            'englishCertification.pdf',
            'SomeEnglishPdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.stock_record.datasheet_pdf_de = SimpleUploadedFile(
            'germanCertification.pdf',
            'SomeGermanPdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.stock_record.save()

    def test_get_english_datasheet_url(self):
        self.assertTrue(
            'englishCertification' in self.stock_record.datasheet_url('en')
        )

    def test_get_german_datasheet_url(self):
        self.assertTrue(
            'germanCertification' in self.stock_record.datasheet_url('de')
        )

    def test_get_default_datasheet_url(self):
        self.assertTrue(
            'englishCertification' in self.stock_record.datasheet_url()
        )
