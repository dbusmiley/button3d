from apps.b3_tests.testcases.common_testcases import TestCase
from apps.partner.conditions import \
    PriceGreaterCondition, PriceSmallerCondition, PriceInRangeCondition, \
    PriceOutsideRangeCondition, \
    QuantityGreaterCondition, QuantitySmallerCondition, \
    QuantityInRangeCondition, QuantityOutsideRangeCondition, \
    PriceQuantityProductGreaterCondition, \
    PriceQuantityProductSmallerCondition, \
    PriceQuantityProductInRangeCondition, \
    PriceQuantityProductOutsideRangeCondition


class RuleConditionTest(TestCase):
    def test_price_greater_condition(self):
        condition = PriceGreaterCondition(max_price=10)
        self.assertTrue(condition.is_applying(price=11))
        self.assertFalse(condition.is_applying(price=10))
        self.assertFalse(condition.is_applying(price=9))

    def test_price_smaller_condition(self):
        condition = PriceSmallerCondition(min_price=10)
        self.assertFalse(condition.is_applying(price=11))
        self.assertFalse(condition.is_applying(price=10))
        self.assertTrue(condition.is_applying(price=9))

    def test_price_inrange_condition(self):
        condition = PriceInRangeCondition(min_price=10, max_price=15)
        self.assertFalse(condition.is_applying(price=5))
        self.assertTrue(condition.is_applying(price=10))
        self.assertTrue(condition.is_applying(price=12.5))
        self.assertTrue(condition.is_applying(price=15))
        self.assertFalse(condition.is_applying(price=20))

    def test_price_outrange_condition(self):
        condition = PriceOutsideRangeCondition(min_price=10, max_price=15)
        self.assertTrue(condition.is_applying(price=5))
        self.assertFalse(condition.is_applying(price=10))
        self.assertFalse(condition.is_applying(price=12.5))
        self.assertFalse(condition.is_applying(price=15))
        self.assertTrue(condition.is_applying(price=20))

    def test_qty_greater_condition(self):
        condition = QuantityGreaterCondition(max_quantity=10)
        self.assertTrue(condition.is_applying(quantity=11))
        self.assertFalse(condition.is_applying(quantity=10))
        self.assertFalse(condition.is_applying(quantity=9))

    def test_qty_smaller_condition(self):
        condition = QuantitySmallerCondition(min_quantity=10)
        self.assertFalse(condition.is_applying(quantity=11))
        self.assertFalse(condition.is_applying(quantity=10))
        self.assertTrue(condition.is_applying(quantity=9))

    def test_qty_inrange_condition(self):
        condition = QuantityInRangeCondition(min_quantity=10, max_quantity=15)
        self.assertFalse(condition.is_applying(quantity=5))
        self.assertTrue(condition.is_applying(quantity=10))
        self.assertTrue(condition.is_applying(quantity=12.5))
        self.assertTrue(condition.is_applying(quantity=15))
        self.assertFalse(condition.is_applying(quantity=20))

    def test_qty_outrange_condition(self):
        condition = QuantityOutsideRangeCondition(min_quantity=10,
                                                  max_quantity=15)
        self.assertTrue(condition.is_applying(quantity=5))
        self.assertFalse(condition.is_applying(quantity=10))
        self.assertFalse(condition.is_applying(quantity=12.5))
        self.assertFalse(condition.is_applying(quantity=15))
        self.assertTrue(condition.is_applying(quantity=20))

    def test_price_qty_prod_greater_condition(self):
        condition = PriceQuantityProductGreaterCondition(
            max_price_quantity_product=10)
        self.assertTrue(condition.is_applying(price=2, quantity=6))
        self.assertFalse(condition.is_applying(price=2, quantity=5))
        self.assertFalse(condition.is_applying(price=1, quantity=5))

    def test_price_qty_prod_smaller_condition(self):
        condition = PriceQuantityProductSmallerCondition(
            min_price_quantity_product=10)
        self.assertFalse(condition.is_applying(price=2, quantity=6))
        self.assertFalse(condition.is_applying(price=2, quantity=5))
        self.assertTrue(condition.is_applying(price=1, quantity=5))

    def test_price_qty_prod_inrange_condition(self):
        condition = PriceQuantityProductInRangeCondition(
            min_price_quantity_product=10, max_price_quantity_product=20)
        self.assertFalse(condition.is_applying(price=4, quantity=6))
        self.assertTrue(condition.is_applying(price=4, quantity=5))
        self.assertTrue(condition.is_applying(price=3, quantity=5))
        self.assertTrue(condition.is_applying(price=2, quantity=5))
        self.assertFalse(condition.is_applying(price=2, quantity=4))

    def test_price_qty_prod_outrange_condition(self):
        condition = PriceQuantityProductOutsideRangeCondition(
            min_price_quantity_product=10, max_price_quantity_product=20)
        self.assertTrue(condition.is_applying(price=4, quantity=6))
        self.assertFalse(condition.is_applying(price=4, quantity=5))
        self.assertFalse(condition.is_applying(price=3, quantity=5))
        self.assertFalse(condition.is_applying(price=2, quantity=5))
        self.assertTrue(condition.is_applying(price=2, quantity=4))
