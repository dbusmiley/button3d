from unittest import expectedFailure

from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.exceptions import ValidationError
from django.template import Context, Template

from apps.b3_core.utils import today
from apps.b3_order.factories import OrderFactory
from apps.b3_organization.utils import get_current_org
from apps.b3_organization.utils import get_current_site
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, \
    StockRecordFactory, StlFileFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase, \
    AuthenticatedTestCase
from apps.basket.models import Basket
from apps.b3_order.models import order_notes
from apps.partner.models import Partner


class TemplateTest(AuthenticatedTestCase):
    def setUp(self):
        super(TemplateTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)

        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()

    def tag_test(self, template, output):
        t = Template(template)
        c = Context({'basket': self.basket, 'vat_rate': 19.54000})
        self.assertEqual(t.render(c), output)

    def test_for_temp_logo_tag(self):
        template = (
            '{% load partner_tags %}{% with basket.first_line.partner'
            ' as partner %}{{ partner|get_partner_logo_temp_tag }}'
            '{% endwith %}'
        )
        output = (
            u'<img src="/media/partners/temp/logo_sy_2018-01-19_'
            '13-35-22.jpg" style="width:180px;" alt="Meltwerk">'
        )
        self.tag_test(template, output)

    def test_format_vat_rate(self):
        template = ('{% load total_tags %}{{ vat_rate|format_vat_rate }}')
        output = ('19.54')
        self.tag_test(template, output)


class PartnerTest(TestCase):
    def test_valid_partner_code(self):
        partner = PartnerFactory(code='test-partner')
        self.assertEqual('test-partner', partner.code)

    def test_valid_tax_rate(self):
        partner = PartnerFactory()
        self.assertEqual(19.00000, partner.vat_rate)

    def test_partner_enabled(self):
        partner = PartnerFactory()
        self.assertEqual(partner.enabled(), True)

    def test_partner_disabled(self):
        partner = PartnerFactory()
        get_current_org().partners_enabled.remove(partner)
        self.assertEqual(partner.enabled(), False)
        get_current_org().partners_enabled.add(partner)

    def test_default_prefixes(self):
        partner: Partner = PartnerFactory()
        self.assertEqual(partner.order_confirmation_prefix, 'OC-')
        self.assertEqual(partner.delivery_note_prefix, 'DL-')
        self.assertEqual(partner.invoice_prefix, 'IN-')


class PartnerCertificationsTest(TestCase):
    CERTIF_UPLOAD_PATH = '/media/partners/certifications/'

    def setUp(self):
        super(PartnerCertificationsTest, self).setUp()
        self.partner = PartnerFactory()

    @classmethod
    def get_upload_path(cls, filename):
        return '{0}{1}'.format(cls.CERTIF_UPLOAD_PATH, filename)

    def tearDown(self, *args, **kwargs):
        if self.partner.certifications_pdf_en:
            self.partner.certifications_pdf_en.delete()
        if self.partner.certifications_pdf_de:
            self.partner.certifications_pdf_de.delete()
        super(PartnerCertificationsTest, self).tearDown(*args, **kwargs)

    def test_no_certification(self):
        self.assertIsNone(self.partner.get_certifications_url('en'))
        self.assertIsNone(self.partner.get_certifications_url('de'))
        self.assertIsNone(self.partner.get_certifications_url())

    def test_english_certification(self):
        self.partner.certifications_pdf_en = SimpleUploadedFile(
            'englishCertification.pdf',
            'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.partner.save()

        upload_path = self.get_upload_path('englishCertification.pdf')
        self.assertEqual(
            self.partner.get_certifications_url('en'), upload_path
        )
        self.assertEqual(
            self.partner.get_certifications_url('de'), upload_path
        )
        self.assertEqual(self.partner.get_certifications_url(), upload_path)

    def test_german_certification(self):
        self.partner.certifications_pdf_de = SimpleUploadedFile(
            'germanCertification.pdf',
            'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.partner.save()

        upload_path = self.get_upload_path('germanCertification.pdf')
        self.assertEqual(
            self.partner.get_certifications_url('en'), upload_path
        )
        self.assertEqual(
            self.partner.get_certifications_url('de'), upload_path
        )
        self.assertEqual(self.partner.get_certifications_url(), upload_path)

    def test_both_certifications(self):
        self.partner.certifications_pdf_en = SimpleUploadedFile(
            'englishCertification.pdf',
            'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.partner.certifications_pdf_de = SimpleUploadedFile(
            'germanCertification.pdf',
            'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.partner.save()

        upload_path_en = self.get_upload_path('englishCertification.pdf')
        upload_path_de = self.get_upload_path('germanCertification.pdf')

        self.assertEqual(
            self.partner.get_certifications_url('en'), upload_path_en
        )
        self.assertEqual(
            self.partner.get_certifications_url('de'), upload_path_de
        )
        self.assertEqual(self.partner.get_certifications_url(), upload_path_en)


class PartnerValidationTest(TestCase):
    def test_website_validation(self):
        partner = PartnerFactory(website='www.example.com')
        try:
            partner.full_clean(exclude=['logo_temp'])
            partner.save()
        except ValidationError as e:
            self.assertEqual(
                e.args[0]['website'][0].message,
                'Must start with http:// or https://'
            )
        else:
            self.fail('Exception not thrown')

    def test_vat_rate_validation(self):
        partner = PartnerFactory(vat_rate='-1.55')
        try:
            partner.full_clean(exclude=['logo_temp'])
            partner.save()
        except ValidationError as e:
            self.assertEqual(
                e.args[0]['vat_rate'][0].message, 'Must be positive value.')
        else:
            self.fail('Exception not thrown')

    def test_minimum_order_price_value_validation(self):
        partner = PartnerFactory(minimum_order_price_value='-1.55')
        try:
            partner.full_clean(exclude=['logo_temp', 'site'])
            partner.save()
        except ValidationError as e:
            self.assertEqual(
                e.args[0]['minimum_order_price_value'][0].message,
                'Minimum Order Price must be greater than 0.')
        else:
            self.fail('Exception not thrown')


@expectedFailure
class OrderNotesConfigurationTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.site = get_current_site()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.save()

    def test_prefix_alteration(self):
        basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket.site = self.site
        basket.save()
        stockrecord = StockRecordFactory(partner=self.partner)
        line1, created = basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()
        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        order = OrderFactory(
            project=basket,
            purchased_by=self.user,
            shipping_method=shipping_method.code,
        )
        order.site = self.site
        order.save()
        self.assertEqual(self.partner.order_confirmation_prefix, 'OC-')

        order_confirmation_note_prefix_expected = 'OC-2018'
        self.partner.order_confirmation_prefix = \
            order_confirmation_note_prefix_expected
        self.partner.save()
        order_confirmation_note_actual = order_notes.OrderConfirmationNote()
        order_confirmation_note_actual.order = order
        order_confirmation_note_actual.issue_date = today()
        order_confirmation_note_actual.shipping_date = today()
        order_confirmation_note_actual.created_by = self.user
        order_confirmation_note_actual.set_note_prefix(self.partner)
        order_confirmation_note_actual.save()
        order_confirmation_note_actual.refresh_from_db()
        self.assertEqual(order_confirmation_note_actual.note_prefix,
                         order_confirmation_note_prefix_expected)
