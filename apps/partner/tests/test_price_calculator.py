from decimal import Decimal as D

from apps.b3_core.utils import MissingParameterException, currency_exchange, \
    NoPricingPossibleException, ManualPricingRequiredException
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import StockRecordFactory, StlFileFactory, \
    PostProcessingFactory, PartnerFactory, \
    PartnerExtraFeeFactory, BasketFactory, VoucherFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket
from apps.basket.models.basket import NotAllLinesPricedException
from apps.partner.conclusionactions import FixedDiscountAction
from apps.partner.conditions import QuantityGreaterCondition
from apps.partner.pricing.calculators import PartPriceCalculator, \
    OrderPriceCalculator
from apps.partner.pricing.calculators.project import ProjectPriceCalculator


class PriceCalculatorTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        price_formula = ("max(10.5/quantity, 1.25/quantity+root(volume)"
                         "+pow(box, 0.33)+min(pow(box, 0.2), area)*shells)")
        self.stock_record = StockRecordFactory(
            partner=self.partner,
            price_custom=price_formula
        )
        self.post_processing = PostProcessingFactory(
            stock_record=self.stock_record
        )
        self.stl_file = StlFileFactory()

    def assert_price_correct(self, price, currency, excl_tax, tax):
        self.assertEqual(price.currency, currency)
        self.assertEqual(price.excl_tax, excl_tax)
        self.assertEqual(price.tax, tax)
        self.assertEqual(price.incl_tax, excl_tax + tax)

    def test_item_price(self):
        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=1)
        )
        calculator = PartPriceCalculator(
            self.stl_file,
            self.stock_record,
            post_processings=[self.post_processing],
            scale=0.5,
            quantity=2
        )
        exp_currency = self.partner.price_currency
        exp_unit_price_net = D('189.71')
        exp_unit_price_tax = D('36.04')
        exp_discount_net = D('10.00')
        exp_discount_tax = D('1.90')

        exp_item_total_net = exp_unit_price_net * 2 - exp_discount_net
        exp_item_total_tax = exp_unit_price_tax * 2 - exp_discount_tax

        self.assert_price_correct(
            calculator.unit_price,
            exp_currency,
            exp_unit_price_net,
            exp_unit_price_tax
        )
        self.assert_price_correct(
            calculator.item_discount,
            exp_currency,
            exp_discount_net,
            exp_discount_tax
        )
        self.assert_price_correct(
            calculator.item_total,
            exp_currency,
            exp_item_total_net,
            exp_item_total_tax
        )

    def test_order_price(self):
        stock_record2 = StockRecordFactory(
            partner=self.partner,
            price_custom='min(pow(box, 0.2), area) * shells'
        )
        post_processing2 = PostProcessingFactory(
            stock_record=stock_record2
        )
        stl_file2 = StlFileFactory()

        PartnerExtraFeeFactory(partner=self.partner)
        self.partner.minimum_order_price_enabled = True
        self.partner.minimum_order_price_value = D('1000.00')
        self.partner.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        voucher = VoucherFactory(partner=self.partner)

        parts = [
            PartPriceCalculator(
                stl_file=self.stl_file,
                stock_record=self.stock_record,
                post_processings=[self.post_processing],
                scale=0.5,
                quantity=2
            ),
            PartPriceCalculator(
                stl_file=stl_file2,
                stock_record=stock_record2,
                post_processings=[post_processing2],
                scale=0.5,
                quantity=2
            )
        ]

        calculator = OrderPriceCalculator(
            parts=parts,
            shipping_method=shipping_method,
            voucher=voucher
        )

        exp_currency = self.partner.price_currency

        exp_item_total_net_item1 = D('189.71') * 2
        exp_item_total_tax_item1 = D('36.04') * 2
        exp_item_total_net_item2 = D('98.94') * 2
        exp_item_total_tax_item2 = D('18.80') * 2

        exp_subtotal_net = exp_item_total_net_item1 + exp_item_total_net_item2
        exp_subtotal_tax = exp_item_total_tax_item1 + exp_item_total_tax_item2

        exp_shipping_net = D('10.00')
        exp_shipping_tax = D('1.90')

        exp_voucher_discount_net = D('101.00')
        exp_voucher_discount_tax = D('19.19')

        exp_fees_net = D('10.00')
        exp_fees_tax = D('1.90')

        exp_min_price_diff_net = D('1000.00') - (
            exp_subtotal_net
            + exp_fees_net
        )
        exp_min_price_diff_tax = D('190.00') - (
            exp_subtotal_tax
            + exp_fees_tax
        )

        exp_total_net = sum([
            exp_subtotal_net,
            exp_shipping_net,
            exp_fees_net,
            exp_min_price_diff_net,
            -exp_voucher_discount_net
        ])
        exp_total_tax = sum([
            exp_subtotal_tax,
            exp_shipping_tax,
            exp_fees_tax,
            exp_min_price_diff_tax,
            -exp_voucher_discount_tax,
        ])

        self.assert_price_correct(
            calculator.subtotal,
            exp_currency,
            exp_subtotal_net,
            exp_subtotal_tax
        )
        self.assert_price_correct(
            calculator.shipping,
            exp_currency,
            exp_shipping_net,
            exp_shipping_tax
        )
        self.assert_price_correct(
            calculator.voucher_discount,
            exp_currency,
            exp_voucher_discount_net,
            exp_voucher_discount_tax
        )
        self.assert_price_correct(
            calculator.fees,
            exp_currency,
            exp_fees_net,
            exp_fees_tax
        )
        self.assert_price_correct(
            calculator.min_price_diff,
            exp_currency,
            exp_min_price_diff_net,
            exp_min_price_diff_tax
        )
        self.assert_price_correct(
            calculator.total,
            exp_currency,
            exp_total_net,
            exp_total_tax
        )

    def test_manual_pricing(self):
        self.stock_record.add_manual_pricing_rule(
            QuantityGreaterCondition(max_quantity=1)
        )
        price_calculator = PartPriceCalculator(
            self.stl_file,
            self.stock_record,
            post_processings=[self.post_processing],
            scale=0.5,
            quantity=2
        )
        with self.assertRaises(ManualPricingRequiredException):
            price_calculator.unit_price

    def test_invalid_price_formula(self):
        self.stock_record.price_custom = 'nonsense'
        price_calculator = PartPriceCalculator(
            self.stl_file,
            self.stock_record,
            post_processings=[self.post_processing],
            scale=0.5,
            quantity=2
        )
        with self.assertRaises(NoPricingPossibleException):
            price_calculator.unit_price

    def test_missing_parameter(self):
        self.stl_file.parameter = None
        price_calculator = PartPriceCalculator(
            self.stl_file,
            self.stock_record,
            post_processings=[self.post_processing],
            scale=0.5,
            quantity=2
        )
        with self.assertRaises(MissingParameterException):
            price_calculator.unit_price

    def test_out_of_bounds(self):
        self.stock_record.max_x_mm = D('0.00')
        price_calculator = PartPriceCalculator(
            self.stl_file,
            self.stock_record,
            post_processings=[self.post_processing],
            scale=0.5,
            quantity=2
        )
        with self.assertRaises(NoPricingPossibleException):
            price_calculator.unit_price

    def test_manually_priced_project(self):
        self.populate_current_request()
        self.stock_record.add_manual_pricing_rule(
            QuantityGreaterCondition(max_quantity=1)
        )
        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=1),
            QuantityGreaterCondition(max_quantity=1)
        )
        project = BasketFactory(owner=self.user)
        line, _ = project.add_product(
            product=self.stock_record.product,
            stockrecord=self.stock_record,
            stlfile=self.stl_file,
            quantity=2
        )

        price_calculator = ProjectPriceCalculator(project)
        with self.assertRaises(ManualPricingRequiredException):
            price_calculator.total
        with self.assertRaises(NotAllLinesPricedException):
            project.set_as_manually_priced()

        manual_price = D('10.00')
        line.set_manual_price(manual_price)
        project.set_as_manually_priced()

        project = Basket.objects.get(id=project.id)
        price_calculator = ProjectPriceCalculator(project)

        self.assertTrue(project.are_all_lines_priced)
        # Project price available, discount does not apply
        self.assertEqual(price_calculator.subtotal.excl_tax, manual_price * 2)

    def test_project_price_currency_conversion(self):
        project = BasketFactory(owner=self.user)
        line, _ = project.add_product(
            product=self.stock_record.product,
            stockrecord=self.stock_record,
            stlfile=self.stl_file,
            quantity=2,
        )
        voucher = VoucherFactory()
        price_calculator_eur = ProjectPriceCalculator(
            project, voucher=voucher
        )
        price_calculator_usd = ProjectPriceCalculator(
            project, voucher=voucher, currency='USD'
        )
        self.assertEqual(price_calculator_eur.subtotal.currency, 'EUR')
        self.assertEqual(price_calculator_usd.subtotal.currency, 'USD')

        converted_price = currency_exchange(
            price_calculator_eur.total.excl_tax,
            from_c='EUR',
            to_c='USD'
        )
        self.assertAlmostEqual(
            price_calculator_usd.total.excl_tax,
            converted_price,
            delta=D('0.01')
        )
