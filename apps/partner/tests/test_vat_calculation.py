from apps.b3_address.factories import CountryFactory, AddressFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class VatCalculationTest(TestCase):
    def setUp(self):
        super().setUp()
        self.germany = CountryFactory(alpha2='DE')
        self.italy = CountryFactory(alpha2='IT')
        self.usa = CountryFactory(alpha2='US')
        self.partner = PartnerFactory(
            add_org_and_address={'country': CountryFactory(alpha2='ES')}
        )

    def test_vat_missing_parameters(self):
        self.assertFalse(
            self.partner.should_remove_tax(None, None, False)
        )
        self.assertFalse(
            self.partner.should_remove_tax(self.germany, None, False)
        )
        self.assertFalse(
            self.partner.should_remove_tax(None, self.germany, False)
        )

    def test_vat_in_germany_german_partner(self):
        self.assertNotEqual(self.germany, self.partner.address.country)
        self.assertTrue(
            self.partner.should_remove_tax(
                shipping_country=self.germany,
                billing_country=self.germany,
                is_b2b_transaction=True
            )
        )

    def test_vat_not_germany_german_partner(self):
        self.assertNotEqual(self.italy, self.partner.address.country)
        self.assertTrue(
            self.partner.should_remove_tax(
                shipping_country=self.italy,
                billing_country=self.italy,
                is_b2b_transaction=True
            )
        )

    def test_vat_partner_exceptions_germany(self):
        partner = PartnerFactory(
            is_3yd_contract_partner=True,
        )
        self.assertFalse(
            partner.should_remove_tax(
                shipping_country=self.germany,
                billing_country=self.germany,
                is_b2b_transaction=True
            )
        )

    def test_billing_shipping_different(self):
        self.assertFalse(
            self.partner.should_remove_tax(
                shipping_country=self.partner.address.country,
                billing_country=self.germany,
                is_b2b_transaction=True
            )
        )

    def test_noneu_to_noneu_order(self):
        usa_address = AddressFactory(country=self.usa)
        partner = PartnerFactory(address=usa_address)
        self.assertFalse(
            partner.should_remove_tax(
                shipping_country=self.germany,
                billing_country=self.usa,
                is_b2b_transaction=True
            )
        )

    def test_eu_to_noneu_order(self):
        germany_address = AddressFactory(country=self.germany)
        partner = PartnerFactory(address=germany_address)
        self.assertTrue(
            partner.should_remove_tax(
                shipping_country=self.usa,
                billing_country=self.usa,
                is_b2b_transaction=True
            )
        )

    def test_eu_to_eu_order(self):
        germany_address = AddressFactory(country=self.germany)
        partner = PartnerFactory(address=germany_address)
        self.assertFalse(
            partner.should_remove_tax(
                shipping_country=self.germany,
                billing_country=self.germany,
                is_b2b_transaction=True
            )
        )

    def test_noneu_to_eu_order(self):
        usa_address = AddressFactory(country=self.usa)
        partner = PartnerFactory(address=usa_address)
        self.assertFalse(
            partner.should_remove_tax(
                shipping_country=self.germany,
                billing_country=self.germany,
                is_b2b_transaction=True
            )
        )
