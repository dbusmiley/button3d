from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import PostProcessingFactory
from django.core.files.uploadedfile import SimpleUploadedFile


class StockRecordDataSheetTest(TestCase):
    def setUp(self):
        super().setUp()
        self.post_processing = PostProcessingFactory()
        self.post_processing.datasheet_pdf_en = SimpleUploadedFile(
            'englishCertification.pdf',
            'SomeEnglishPdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.post_processing.datasheet_pdf_de = SimpleUploadedFile(
            'germanCertification.pdf',
            'SomeGermanPdfContent'.encode('utf-8'),
            content_type='application/pdf'
        )
        self.post_processing.save()

    def test_get_english_datasheet_url(self):
        self.assertTrue(
            'englishCertification' in self.post_processing.datasheet_url('en')
        )

    def test_get_german_datasheet_url(self):
        self.assertTrue(
            'germanCertification' in self.post_processing.datasheet_url('de')
        )

    def test_get_default_datasheet_url(self):
        self.assertTrue(
            'englishCertification' in self.post_processing.datasheet_url()
        )
