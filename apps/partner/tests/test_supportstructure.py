import json
import os
from collections import namedtuple

from django.core.cache import cache

from apps.b3_core.models import StlFile
from apps.b3_core.supportstructure import SupportStructure
from apps.b3_core.utils import quantize
from apps.b3_tests.factories import StlFileFactory, StockRecordFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.partner.pricing.calculators import PartPriceCalculator

RES_DIRNAME = os.path.join(os.path.dirname(__file__), 'res')

# Only the values for the default orientation are tested here!
RESOURCES = [
    {
        'filename': 'support_bear.json',
        'histogram': False,
        'settings': [
            {
                'input': (45, 0, 1),
                'expected_output': (
                    6102.167687000001,
                    1961.712188,
                    89.12241900000001
                )
            },
            {
                'input': (45, 0, 2),
                # the scale should have a cubic influence on volume and a
                # quadratic influence on areas.
                'expected_output': (
                    8 * 6102.167687000001,
                    4 * 1961.712188,
                    4 * 89.12241900000001
                )
            },
            {
                'input': (45, 5, 1),
                'expected_output': (
                    15350.135647000001,
                    1961.712188,
                    89.12241900000001
                )
            },
            {
                'input': (50, 5, 1),
                'expected_output': (
                    13974.765939,
                    1921.16829,
                    62.273636999999994
                )
            },
            {
                'input': (40, 5, 1),
                'expected_output': (
                    19123.707252,
                    2051.80818,
                    119.33439000000003
                )
            }
        ]
    },
    {
        'filename': 'histogram_bear.json',
        'histogram': True,
        'settings': [
            {
                'input': (45, 0, 1),
                'expected_output': (
                    0.4114385,
                    2.8751986,
                    0.3585368
                )
            },
            {
                'input': (45, 0, 2),
                # the scale should have a cubic influence on volume and a
                # quadratic influence on areas.
                'expected_output': (
                    8 * 0.4114385,
                    4 * 2.8751986,
                    4 * 0.3585368
                )
            },
            {
                'input': (45, 5, 1),
                'expected_output': (
                    2.02284725,
                    2.8751986,
                    0.3585368
                )
            },
            {
                'input': (50, 5, 1),
                'expected_output': (
                    2.04158914,
                    2.8870602,
                    0.36497778
                )
            },
            {
                'input': (40, 5, 1),
                'expected_output': (
                    1.92832506,
                    2.8385434,
                    0.3463629
                )
            }
        ]
    }
]

FakeStlFile = namedtuple('FakeStlFile', ['uuid'])


class SupportValuesTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(SupportValuesTestCase, cls).setUpTestData()
        cls.stl_file = StlFileFactory()

    def test_resources(self):
        for test_file in RESOURCES:
            for setting in test_file['settings']:
                set_support_structure_data(
                    test_file['filename'],
                    test_file['histogram']
                )
                output = self.stl_file.support_structure.get_values(
                    *setting['input'], use_cache=False
                )['h-']
                for i, value in enumerate(output):
                    self.assertAlmostEqual(
                        value,
                        setting['expected_output'][i]
                    )

    def test_empty_histogram(self):
        empty_histogram = {
            "w-": [],
            "w+": [],
            "d-": [],
            "d+": [],
            "h-": [],
            "h+": []
        }
        values = SupportStructure._calculate_unscaled_from_histogram(
            empty_histogram, 45
        )
        self.assertIn('h-', values)


class SupportPricingTestCase(TestCase):
    def setUp(self):
        super(SupportPricingTestCase, self).setUp()
        self.stockrecord = StockRecordFactory(
            price_custom='0',
            price_support_custom='area + volume',
        )
        self.stockrecord.support_enabled = True
        self.stl_file = StlFileFactory()

    def test_resources(self):
        for test_file in RESOURCES:
            for setting in test_file['settings']:
                set_support_structure_data(
                    test_file['filename'],
                    test_file['histogram']
                )
                support_angle, support_offset, scale = setting['input']
                self.stockrecord.support_angle = support_angle
                self.stockrecord.support_offset = support_offset
                item = PartPriceCalculator(
                    stl_file=self.stl_file,
                    scale=scale,
                    post_processings=[],
                    stock_record=self.stockrecord
                )
                expected_price_excl_tax = quantize(
                    sum(setting['expected_output'])
                )
                self.assertEqual(
                    item.item_total.excl_tax,
                    expected_price_excl_tax
                )


def set_support_structure_data(filename, histogram):
    """
    Set support structure data on the StlFile class to avoid calls to
    the backend during the _tests.
    """
    cache.clear()
    support_structure = SupportStructure(FakeStlFile(uuid='TEST'))
    setattr(StlFile, '_support_structure', support_structure)
    with open(os.path.join(RES_DIRNAME, filename), 'r') as f:
        support_structure._support_data = json.load(f)
        support_structure._histogram = histogram
