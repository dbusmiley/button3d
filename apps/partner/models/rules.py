import json

from django.db import models

from apps.b3_core.utils import NOT_NULL
from apps.partner.conditions import condition_classes
from apps.partner.conclusionactions import conclusion_action_classes
from apps.partner.models import Partner
from apps.partner.models import StockRecord


class AbstractRule(models.Model):
    conclusion = models.CharField(max_length=255)
    conditions = models.TextField(default="[]")
    conclusion_actions = models.TextField(default="[]", **NOT_NULL)

    class Meta:
        abstract = True

    def add_condition(self, condition, commit=True):
        conditions = json.loads(self.conditions)
        conditions.append([condition.__class__.__name__, condition.args])
        self.conditions = json.dumps(conditions)
        if commit:
            self.save()

    def clear_conditions(self, commit=True):
        self.conditions = json.dumps([])
        if commit:
            self.save()

    def add_conclusion_action(self, conclusion_action, commit=True):
        conclusion_actions = json.loads(self.conclusion_actions)
        conclusion_actions.append(
            [conclusion_action.__class__.__name__, conclusion_action.args]
        )
        self.conclusion_actions = json.dumps(conclusion_actions)
        if commit:
            self.save()

    def clear_conclusion_actions(self, commit=True):
        self.conclusion_actions = json.dumps([])
        if commit:
            self.save()

    def get_conclusion_actions(self):
        return [conclusion_action_classes[cls_name](**args)
                for cls_name, args in json.loads(self.conclusion_actions)]

    def get_conditions(self):
        return [condition_classes[cls_name](**args)
                for cls_name, args in json.loads(self.conditions)]

    def is_applying(self, **info_dict):
        conditions = self.get_conditions()
        return all([condition.is_applying(**info_dict)
                    for condition in conditions])

    def to_dict(self):
        return json.loads(self.conditions)

    def to_dict_extended(self):
        """
        A better parsable output for the frontend with id for individual edits
        """
        conclusion_actions_parsed = json.loads(self.conclusion_actions)
        discountType = list(conclusion_actions_parsed[0][1].keys())[0]
        return {
            "id": self.id,
            "type": json.loads(self.conditions)[0][0],
            "action": conclusion_actions_parsed[0][0],
            "discountType": discountType,
            "discountValue": conclusion_actions_parsed[0][1][discountType],
            "args": json.loads(self.conditions)[0][1]
        }


class StockRecordRule(AbstractRule):
    stockrecord = models.ForeignKey(
        StockRecord,
        on_delete=models.CASCADE,
        related_name="rules"
    )


class PartnerRule(AbstractRule):
    partner = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name="rules"
    )

    def update_rule(self, discount, *conditions):
        # Remove all conditions first
        self.clear_conclusion_actions()
        self.clear_conditions()
        if conditions:
            for condition in conditions:
                self.add_condition(condition)
        if discount:
            self.add_conclusion_action(discount)
        return self
