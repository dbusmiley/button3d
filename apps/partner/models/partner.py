import logging
import typing as t

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator, RegexValidator
from stdimage.models import StdImageField
from stdimage.validators import MinSizeValidator


import button3d.type_declarations as td
from apps.b3_checkout.payment_methods import PAYMENT_METHOD_CLASSES
from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_misc.validators import FileValidator
from apps.b3_organization.models import OrganizationPartnerOption, \
    OrganizationOptionModel
from apps.b3_organization.utils import get_current_org
from apps.partner.managers import SitePartnerManager, \
    NaturalKeyPartnerManager, DeletedDatePartnerManager
from apps.partner.pricing.price import Price
from apps.partner.utils import UploadToPartnerLogoSlug

logger = logging.getLogger(__name__)


class AddressDoesNotExistError(Exception):
    pass


TAX_TYPES = (
    ('VAT', _('VAT')), ('Sales_Tax', _('Sales Tax')), ('Tax', _('Tax'))
)


class Partner(AbstractDeletedDateModel, OrganizationOptionModel):
    """
    A fulfillment partner. An individual or company who can fulfil products.
    E.g. for physical goods, somebody with a warehouse and means of delivery.

    Creating one or more instances of the Partner model is a required step in
    setting up an Oscar deployment. Many Oscar deployments will only have one
    fulfillment partner.
    """
    translated_fields = (
        'bluebox_preview_step',
        'terms_conditions',
        'payment_conditions',
        'thank_you_text',
        'certifications_pdf',
    )

    code = models.CharField(
        _('Code'), max_length=128,
        unique=True,
    )

    name = models.CharField(
        _('Name'),
        help_text='Name of the Organization.',
        max_length=256,
    )
    description = models.CharField(default='', max_length=1000, blank=True)

    vat_id = models.CharField(
        _('VAT Id'), max_length=128,
        blank=True,
        null=True
    )

    # Logo is cropped, logo_temp is full (and possibly does not correspond to
    # the full size logo)
    logo = StdImageField(
        null=True,
        upload_to=UploadToPartnerLogoSlug(
            populate_from='code', folder='cropped'
        ),
        blank=True,
        verbose_name=_('Logo'),
        validators=[
            MinSizeValidator(189, 44),
            FileValidator(allowed_mimetypes=('image/png',))
        ]
    )

    logo_temp = StdImageField(
        null=True,
        upload_to=UploadToPartnerLogoSlug(populate_from='code', folder='temp'),
        blank=True,
        verbose_name=_('Logo Temp'),
        validators=[
            MinSizeValidator(189, 44),
            FileValidator(allowed_mimetypes=('image/png',))
        ]
    )

    website = models.CharField(
        _('Website-URL'), max_length=256, blank=True, validators=[
            RegexValidator(
                regex='^https?://',
                message='Must start with http:// or https://'
            )]
    )

    email = models.CharField(_('Email'), max_length=256, blank=True)

    is_3yd_contract_partner = models.BooleanField(
        _('Is 3yd Contract Partner'), default=False
    )

    # # Other

    price_currency = models.CharField(
        _('Currency'),
        max_length=12,
        default=settings.DEFAULT_CURRENCY,
        choices=settings.CHOICE_CURRENCY
    )

    tax_type = models.CharField(
        _('Tax type'),
        max_length=12,
        choices=TAX_TYPES,
        default='VAT',
    )

    vat_rate = models.DecimalField(
        _('Tax rate'), default=0.00, max_digits=7, decimal_places=5,
        validators=[MinValueValidator(
            0, 'Must be positive value.'
        )]
    )

    # Rating
    rating_quality = models.FloatField(default=0.0)

    rating_reliability = models.FloatField(default=0.0)

    rating_service = models.FloatField(default=0.0)

    site = models.ForeignKey(
        'sites.Site',
        on_delete=models.CASCADE,
        null=True
    )

    bluebox_preview_step = models.TextField(
        _('Information Box content'),
        default='3YOURMIND is only the transmitter of this order. '
                'Your order is processed by the selected printing '
                "service, from which you'll receive a separate "
                'order confirmation and invoice.',
        help_text='This wil be shown to the user in an obvious '
                  'blue box on checkout page.',
        blank=True,
        null=True
    )

    terms_conditions = models.TextField(
        default="I've read and accept the "
                "<a href='https://www.3yourmind.com/terms' target='_blank'>"
                "Terms & Conditions</a>",
    )
    payment_conditions = models.CharField(
        default='Payment terms: Credit Card or 14 days after invoicing',
        max_length=1000,
        blank=True,
    )
    thank_you_text = models.TextField(
        _('Custom text Thank you page + Confirmation Order Email'),
        help_text=_(
            'Remember this text is shared on the Thank you page and '
            'the Confirmation Order Email'
        ),
        default='The manufacturing process will start soon. To make sure you '
                'receive best quality parts our 3D printing experts will '
                'manually review the designs before production.',
        null=True,
        blank=True
    )

    certifications_pdf = models.FileField(
        _('Certifications (.pdf)'),
        upload_to='partners/certifications/',
        null=True,
        blank=True,
        validators=[
            FileValidator(allowed_mimetypes=('application/pdf',)),
        ]
    )

    webhook = models.CharField(
        _('Web Hook'),
        max_length=1024,
        blank=True,
        null=True,
        help_text=_('This Web Hook will be called after receiving on order')
    )

    mes_activated = models.BooleanField(
        _('MES System Activated'),
        default=False,
        blank=True
    )

    minimum_order_price_enabled = models.BooleanField(
        _('Minimum Order Price Activated'),
        default=False,
        blank=True
    )
    minimum_order_price_value = models.DecimalField(
        default=0.00,
        max_digits=7,
        decimal_places=2,
        help_text='Amount tax exclusive',
        validators=[MinValueValidator(
            0, 'Minimum Order Price must be greater than 0.')]
    )
    #: A partner can have users assigned to it. This is used
    #: for access modelling in the permission-based dashboard
    users = models.ManyToManyField(
        User,
        related_name='partners',
        blank=True,
        verbose_name=_('Users')
    )
    order_confirmation_prefix = models.CharField(
        default='OC-', max_length=10, verbose_name='order confirmation prefix',
    )
    delivery_note_prefix = models.CharField(
        default='DL-', max_length=10, verbose_name='delivery note prefix',
    )
    invoice_prefix = models.CharField(
        default='IN-', max_length=10, verbose_name='invoice prefix'
    )
    general_sales_conditions = models.TextField(null=True, blank=True)
    general_shipping_conditions = models.TextField(null=True, blank=True)
    footer_company_information = models.TextField(null=True, blank=True)

    # For the options
    org_option_class = OrganizationPartnerOption

    all_objects = NaturalKeyPartnerManager()
    undeleted = DeletedDatePartnerManager()
    objects = SitePartnerManager()

    class Meta:
        app_label = 'partner'
        permissions = (('dashboard_access', 'Can access dashboard'),)
        verbose_name = _('Fulfillment partner')
        verbose_name_plural = _('Fulfillment partners')
        ordering = ('pk',)

    def __str__(self):
        return self.display_name

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = self.create_code_slug()

        return super().save(*args, **kwargs)

    @property
    def organization(self):
        return self.site.organization

    @property
    def rating_total(self):
        total = float(self.org_options.rating_quality) + \
            float(self.org_options.rating_reliability) + \
            float(self.org_options.rating_service)
        return total / 3

    def get_certifications_url(self, language=None):
        if not self.certifications_pdf_en and not self.certifications_pdf_de:
            return None
        if language == 'de':
            certifs = self.certifications_pdf_de or self.certifications_pdf_en
        else:
            certifs = self.certifications_pdf_en or self.certifications_pdf_de
        return certifs.url

    @property
    def display_name(self):
        # XXX: name has null=False so will this ever return self.code?
        return self.name or self.code

    @property
    def slug(self) -> str:
        from django.utils.text import slugify
        return slugify(self.display_name)

    @property
    def supported_currencies(self):
        return [self.price_currency] + self.additional_supported_currencies

    @property
    def additional_supported_currencies(self):
        return [
            supported_currency.currency
            for supported_currency
            in self.supportedcurrency_set.all()
        ]

    @additional_supported_currencies.setter
    def additional_supported_currencies(self, currencies):
        self.supportedcurrency_set.all().delete()
        for currency in currencies:
            self.supportedcurrency_set.create(currency=currency)

    def get_tax_type(self):
        # magically returns the correct tuple value. hail Django
        return self.get_tax_type_display()

    def enabled(self):
        current_organization = get_current_org()
        return current_organization.partners_enabled.filter(pk=self.pk
                                                            ).count() > 0

    def get_payment_methods(self):
        partner_payment_methods = []
        for method in self.payment_methods.all():
            if method.is_enabled:
                partner_payment_methods.append(method.type_name)
        return partner_payment_methods

    def get_fastest_shipping_method(self, country=None):
        shipping_methods = self.shipping_methods.all()
        if country:
            shipping_methods = shipping_methods.filter(
                countries__alpha2=country
            ).distinct()

        if not shipping_methods:
            return None

        return sorted(
            list(shipping_methods), key=lambda method: method.shipping_time
        )[0]

    def create_code_slug(self):
        slug = slugify(self.name)
        new_slug = slug
        count = 0
        # noinspection PyProtectedMember
        mgr = Partner._default_manager
        while mgr.filter(code=new_slug).exclude(id=self.id).exists():
            count += 1
            new_slug = f'{slug}-{count}'

        return new_slug

    def add_discount_rule(self, discount, *conditions):
        rule = self.rules.create(conclusion='discount')
        if conditions:
            for condition in conditions:
                rule.add_condition(condition)
        if discount:
            rule.add_conclusion_action(discount)
        return rule

    def create_price(
        self,
        excl_tax: td.Decimal,
        shipping_country: t.Optional[td.Country] = None,
        billing_country: t.Optional[td.Country] = None,
        is_b2b_transaction: bool = False
    ) -> td.Price:
        if self.should_remove_tax(
            shipping_country,
            billing_country,
            is_b2b_transaction
        ):
            tax_rate = 0
        else:
            tax_rate = self.org_options.vat_rate
        return Price(
            currency=self.price_currency,
            excl_tax=excl_tax,
            tax_rate=tax_rate,
        )

    def should_remove_tax(
        self,
        shipping_country,
        billing_country,
        is_b2b_transaction: bool,
    ) -> bool:

        from apps.b3_address.models import Address  # avoid circular import

        log_message = f'Deciding if we have to remove tax for shipping ' \
                      f'country {shipping_country}, billing country ' \
                      f'{billing_country} and B2B transaction ' \
                      f'{is_b2b_transaction}'

        if shipping_country is None or billing_country is None:
            logger.debug(
                f'{log_message}: False, because one of countries is None')
            return False

        try:
            partner_country = self.address.country
        except Address.DoesNotExist as exc:
            raise AddressDoesNotExistError(
                f'Partner {self.id} does not have "new" Address'
            ) from exc

        if not partner_country.is_eu_country:
            logger.debug(
                f'{log_message}: False, because one of countries is None, '
                f'because Partner.Country {partner_country} is not EU')
            return False

        if not shipping_country.is_eu_country:
            logger.debug(
                f'{log_message}: True, because shipping country is not EU')
            return True

        if not is_b2b_transaction:
            logger.debug(
                f'{log_message}: False, because this is not B2B transaction')
            return False

        if self.is_3yd_contract_partner:
            if shipping_country.is_germany or billing_country.is_germany:
                logger.debug(
                    f'{log_message}: False, because Partner is 3YD partner '
                    f'and either shipping or billing country is Germany')
                return False
            logger.debug(
                f'{log_message}: True, because Partner is 3YD partner '
                f'and neither shipping nor billing country is Germany')
            return True

        if partner_country in [shipping_country, billing_country]:
            logger.debug(
                f'{log_message}: False, because Partner country equals to '
                f'either shipping or billing country')
            return False

        logger.debug(f'{log_message}: True by default')
        return True

    @property
    def min_order_price_object(self) -> Price:
        if not self.minimum_order_price_enabled:
            return Price.zero(currency=self.price_currency)
        min_price = self.minimum_order_price_value
        tax_rate = self.org_options.vat_rate
        min_order_price_obj = Price(
            currency=self.price_currency,
            excl_tax=min_price,
            tax_rate=tax_rate
        )
        return min_order_price_obj

    @property
    def total_extra_fee_price_object(self) -> Price:
        aggregation = self.extra_fees.all().aggregate(sum=Sum('value'))
        tax_rate = self.org_options.vat_rate
        if not aggregation['sum']:
            return Price.zero(currency=self.price_currency)
        excl_tax = aggregation['sum']
        price_obj = Price(
            currency=self.price_currency,
            excl_tax=excl_tax,
            tax_rate=tax_rate
        )
        return price_obj

    # used by dump_site/load_site
    def natural_key(self):
        return self.code,

    def get_available_for_creation_payment_method_types(
            self) -> t.Sequence[str]:
        """
        Returns payment methods list available to a Partner, already selected
        methods are filtered out.
        """
        visible_payment_types = set(
            type_name
            for type_name, payment_class
            in PAYMENT_METHOD_CLASSES
            if payment_class.visible_at_service_panel
        )

        supported_payment_types = set(
            self.supported_payment_methods.values_list('type_name', flat=True)
        )

        existing_payment_types = set(
            self.payment_methods.exclude(
                type_name='custom'
            ).values_list('type_name', flat=True)
        )

        types_available_for_creation = \
            (visible_payment_types | supported_payment_types) \
            - existing_payment_types

        return tuple(types_available_for_creation)

    def has_other_enabled_payment_method_left_except(
        self,
        partner_payment_method: td.PartnerPaymentMethod
    ) -> bool:
        """
        Checks if partner has payment methods enabled, other than
        `partner_payment_method`

        This method is used during validation, before payment method is
        disabled or deleted
        """

        return self.payment_methods \
            .filter(is_enabled=True) \
            .exclude(id=partner_payment_method.id) \
            .exists()

    def get_order_lines(self):
        from apps.b3_order.models import OrderLine
        return OrderLine.objects.filter(order__partner_id=self.id)
