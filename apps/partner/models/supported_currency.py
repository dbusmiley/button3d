from django.conf import settings
from django.db import models

from apps.partner.models import Partner


class SupportedCurrency(models.Model):
    partner = models.ForeignKey(Partner)

    currency = models.CharField(
        max_length=12,
        default=settings.DEFAULT_CURRENCY,
        choices=settings.CHOICE_CURRENCY
    )

    class Meta:
        unique_together = ('partner', 'currency')
