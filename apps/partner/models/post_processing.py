from decimal import Decimal as D

from django.core.validators import MinValueValidator
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_misc.validators import FileValidator
from apps.catalogue.models.color import Color
from apps.partner.managers import PostProcessingManager, \
    NaturalKeyPostProcessingManager
from apps.partner.models import StockRecord, MissingParameterException, \
    fit_in_bounds


class PostProcessing(AbstractDeletedDateModel):
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, blank=True)
    description = models.CharField(max_length=1000, blank=True)
    production_days_min = models.IntegerField(null=True, blank=True)
    production_days_max = models.IntegerField(null=True, blank=True)
    price_formula = models.TextField(blank=True)
    always_priced_manually = models.BooleanField()
    published = models.BooleanField()
    colors = models.ManyToManyField(
        Color,
        related_name='post_processings',
        blank=True
    )
    stock_record = models.ForeignKey(
        StockRecord,
        related_name='post_processings'
    )

    datasheet_pdf = models.FileField(
        upload_to='post_processings/pdfs/',
        max_length=255,
        blank=True,
        null=True,
        validators=[FileValidator(allowed_mimetypes=('application/pdf', ))]
    )
    bounds_x_min = models.DecimalField(
        _("Minimum x in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    bounds_x_max = models.DecimalField(
        _("Maximum x in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    bounds_y_min = models.DecimalField(
        _("Minimum y in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    bounds_y_max = models.DecimalField(
        _("Maximum y in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    bounds_z_min = models.DecimalField(
        _("Minimum z in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    bounds_z_max = models.DecimalField(
        _("Maximum z in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )

    objects = PostProcessingManager()
    all_objects = NaturalKeyPostProcessingManager()

    class Meta:
        unique_together = ('stock_record', 'slug')

    def __str__(self):
        return '{0} (StockRecord: {1})'.format(
            self.title,
            self.stock_record
        )

    @property
    def partner(self):
        return self.stock_record.partner

    @property
    def product(self):
        return self.stock_record.product

    @property
    def full_slug(self):
        return '{0}-{1}'.format(self.stock_record.product.slug, self.slug)

    def datasheet_url(self, language=None):
        if not self.datasheet_pdf_en and not self.datasheet_pdf_de:
            return None
        if language == 'de':
            datasheet = self.datasheet_pdf_de or self.datasheet_pdf_en
        else:
            datasheet = self.datasheet_pdf_en or self.datasheet_pdf_de
        return datasheet.url

    def get_non_colliding_slug(self):
        original_slug = self.slug or slugify(self.title)
        colliding_slugs = PostProcessing.all_objects.filter(
            stock_record=self.stock_record,
            slug__startswith=original_slug
        ).exclude(pk=self.pk).values_list('slug')

        candidate, count = original_slug, 1
        while candidate in colliding_slugs or candidate == 'raw':
            count += 1
            candidate = '{0}-{1}'.format(original_slug, count)
        return candidate

    def save(self, **kwargs):
        self.slug = self.get_non_colliding_slug()
        return super().save(**kwargs)

    def fits_in_bounds(self, stl_file, scale=1.0):
        if stl_file.parameter is None:
            raise MissingParameterException

        given_bounds = stl_file.parameter.get_bounds(scale)

        min_allowed_bounds = [self.bounds_x_min,
                              self.bounds_y_min,
                              self.bounds_z_min]
        max_allowed_bounds = [self.bounds_x_max,
                              self.bounds_y_max,
                              self.bounds_z_max]
        return fit_in_bounds(
            min_allowed_bounds, max_allowed_bounds, given_bounds
        )

    def delete(self, **kwargs):
        self.slug = '{0}-deleted'.format(str(self.id))
        super(PostProcessing, self).delete(**kwargs)

    # used by dump_site/load_site
    def natural_key(self):
        return (
            self.stock_record.partner.code,
            self.stock_record.partner_sku,
            self.slug
        )


class PostProcessingOption(models.Model):
    post_processing = models.ForeignKey(PostProcessing)
    color = models.ForeignKey(Color, null=True)

    class Meta:
        abstract = True


class BasketLinePostProcessingOption(PostProcessingOption):

    class Meta:
        unique_together = (("line", "post_processing"), )
    line = models.ForeignKey(
        'basket.Line',
        related_name='post_processing_options'
    )


class ManualRequestPostProcessingOption(PostProcessingOption):

    class Meta:
        unique_together = (("manual_request", "post_processing"), )
    manual_request = models.ForeignKey(
        'b3_manual_request.ManualRequest',
        related_name='post_processing_options'
    )
