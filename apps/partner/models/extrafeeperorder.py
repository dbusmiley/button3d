from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinValueValidator

from apps.partner.models import Partner
from apps.partner.pricing.price import Price


class AbstractFee(models.Model):
    type = models.CharField(
        _('Type'), max_length=128, unique=False, blank=True
    )
    value = models.DecimalField(
        blank=True, default=0.0, max_digits=7, decimal_places=2,
        validators=[MinValueValidator(
            0, _('Fee must be greater than 0.'))]
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.type


class ExtraFeePerOrder(AbstractFee):
    partner = models.ForeignKey(
        Partner,
        on_delete=models.CASCADE,
        related_name='extra_fees'
    )

    def __str__(self):
        return f'{self.partner} - {self.type}'

    class Meta:
        unique_together = ('partner', 'type')

    @property
    def price_object(self):
        return Price(
            currency=self.partner.price_currency,
            excl_tax=self.value,
            tax_rate=self.partner.org_options.vat_rate
        )
