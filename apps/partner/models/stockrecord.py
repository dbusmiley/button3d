# -*- coding: utf-8 -*-
from decimal import Decimal as D

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_core.utils import eval_price_custom, \
    eval_price_support_custom, eval_price_orientation_custom, fit_in_bounds, \
    MissingParameterException
from apps.b3_misc.validators import FileValidator
from apps.catalogue.models.product import Product
from apps.partner.managers import StockRecordManager, \
    NaturalKeyStockRecordManager
from apps.partner.models.partner import Partner


@python_2_unicode_compatible
class StockRecord(AbstractDeletedDateModel):
    # --- OSCAR --- #
    product = models.ForeignKey(
        Product,
        related_name="stockrecords",
        verbose_name=_("Product")
    )
    partner = models.ForeignKey(
        Partner,
        verbose_name=_("3D Printing Service"),
        related_name='stockrecords'
    )

    #: The fulfilment partner will often have their own SKU for a product,
    #: which we store here.  This will sometimes be the same the product's UPC
    #: but not always.  It should be unique per partner.
    #: See also http://en.wikipedia.org/wiki/Stock-keeping_unit
    partner_sku = models.CharField(
        _("3D Printing Service SKU"), max_length=128, blank=True, null=True
    )

    # This is the base price for calculations - tax should be applied by the
    # appropriate method.  We don't store tax here as its calculation is highly
    # domain-specific.  It is NULLable because some items don't have a fixed
    # price but require a runtime calculation (possible from an external
    # service).
    price_excl_tax = models.DecimalField(
        _("Price (excl. tax)"),
        decimal_places=2,
        max_digits=12,
        blank=True,
        null=True
    )

    #: Retail price for this item.  This is simply the recommended price from
    #: the manufacturer.  If this is used, it is for display purposes only.
    #: This prices is the NOT the price charged to the customer.
    price_retail = models.DecimalField(
        _("Price (retail)"),
        decimal_places=2,
        max_digits=12,
        blank=True,
        null=True
    )

    #: Cost price is the price charged by the fulfilment partner.  It is not
    #: used (by default) in any price calculations but is often used in
    #: reporting so merchants can report on their profit margin.
    cost_price = models.DecimalField(
        _("Cost Price"),
        decimal_places=2,
        max_digits=12,
        blank=True,
        null=True
    )

    #: Number of items in stock
    num_in_stock = models.PositiveIntegerField(
        _("Number in stock"), blank=True, null=True
    )

    #: The amount of stock allocated to orders but not fed back to the master
    #: stock system.  A typical stock update process will set the num_in_stock
    #: variable to a new value and reset num_allocated to zero
    num_allocated = models.IntegerField(
        _("Number allocated"), blank=True, null=True
    )

    #: Threshold for low-stock alerts.  When stock goes beneath this threshold,
    #: an alert is triggered so warehouse managers can order more.
    low_stock_threshold = models.PositiveIntegerField(
        _("Low Stock Threshold"), blank=True, null=True
    )

    # Date information
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)
    date_updated = models.DateTimeField(
        _("Date updated"), auto_now=True, db_index=True
    )

    # --- 3YD --- #
    objects = StockRecordManager()
    all_objects = NaturalKeyStockRecordManager()

    partner_title = models.CharField(
        _("Partner Product Title"), max_length=255, blank=True
    )

    min_x_mm = models.DecimalField(
        _("Minimum x in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    max_x_mm = models.DecimalField(
        _("Maximum x in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )

    min_y_mm = models.DecimalField(
        _("Minimum y in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    max_y_mm = models.DecimalField(
        _("Maximum y in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )

    min_z_mm = models.DecimalField(
        _("Minimum z in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )
    max_z_mm = models.DecimalField(
        _("Maximum z in mm"),
        blank=True,
        null=True,
        decimal_places=4,
        max_digits=30,
        validators=[MinValueValidator(D('0.00'))]
    )

    min_production_days = models.PositiveIntegerField(
        _("Minimum days for print"), blank=True, null=True
    )

    max_production_days = models.PositiveIntegerField(
        _("Maximum days for print"), blank=True, null=True
    )

    custom_text = models.CharField(
        _("Custom Text"), max_length=255, blank=True
    )

    PUBLISHED, DRAFT = (0, 1)
    STATUS_CHOICES = (
        (PUBLISHED, _('Published')),
        (DRAFT, _('Draft')),
    )
    edit_status = models.PositiveSmallIntegerField(
        _("Status"),
        choices=STATUS_CHOICES,
        default=DRAFT
    )

    price_custom = models.TextField(_("Price"), blank=True)

    colors = models.ManyToManyField(
        'catalogue.Color',
        related_name="%(class)s_colors",
        blank=True,
        verbose_name=_("Colors")
    )

    is_default = models.BooleanField(_('Is Default'), default=False)

    max_price_excl_tax = models.DecimalField(
        _('Max Price (excl. tax)'),
        blank=True,
        null=True,
        default=None,
        max_digits=6,
        decimal_places=2
    )

    datasheet_pdf = models.FileField(
        _('Datasheet (.pdf)'),
        upload_to='stockrecords/pdfs/',
        max_length=255,
        blank=True,
        null=True,
        validators=[FileValidator(allowed_mimetypes=('application/pdf', ))]
    )

    support_enabled = models.BooleanField(
        _("Activate support structure pricing"), default=False, blank=True
    )
    support_angle = models.DecimalField(
        _(
            "Angle from which on faces need to be supported, where 0° = "
            "vertical and 90° = horizontal"
        ),
        max_digits=7,
        decimal_places=4,
        default=45.0,
    )
    support_offset = models.DecimalField(
        _("Offset (in mm) that seperates the model from the ground (raft)"),
        max_digits=10,
        decimal_places=2,
        default=0.0
    )
    price_support_custom = models.TextField(_("Price Support"), blank=True)

    price_orientation_custom = models.TextField(
        _("Price calculation using different orientations"), blank=True
    )

    always_priced_manually = models.BooleanField(default=False)

    combinable_post_processings = models.BooleanField(default=False)

    @property
    def full_product_title(self):
        return self.product.full_product_title

    @property
    def full_custom_text(self):
        return self.custom_text

    @property
    def full_partner_title(self):
        return self.partner_title

    @property
    def is_published(self):
        return self.edit_status == self.PUBLISHED

    @property
    def is_draft(self):
        return self.edit_status == self.DRAFT

    @property
    def partner_sku_default_slug(self):
        return self.partner.code + "_" + self.product.slug

    @property
    def production_time(self):
        return self.min_production_days or 0, self.max_production_days or 0

    def datasheet_url(self, language=None):
        if not self.datasheet_pdf_en and not self.datasheet_pdf_de:
            return None
        if language == 'de':
            datasheet = self.datasheet_pdf_de or self.datasheet_pdf_en
        else:
            datasheet = self.datasheet_pdf_en or self.datasheet_pdf_de
        return datasheet.url

    def delete(self, **kwargs):
        super(StockRecord, self).delete(**kwargs)
        for post_processing in self.post_processings.all():
            post_processing.delete()

    def update(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

        self.save()

    def clean(self):
        errors = {}

        if self.is_published:
            # validate partner_sku
            try:
                if not self.partner_sku:
                    raise ValidationError("Partner_sku must exist")
            except Exception as e:
                errors["partner_sku"] = e

            # validate min_production_days
            try:
                if not self.min_production_days:
                    raise ValidationError("Min days for print must exist")
            except Exception as e:
                errors["min_production_days"] = e

            # validate max_production_days
            try:
                if not self.max_production_days:
                    raise ValidationError("Max days for print must exist")

                if self.min_production_days and self.max_production_days and \
                        self.min_production_days > self.max_production_days:
                    raise ValidationError(
                        "Max days for print must be bigger than min days for "
                        "print"
                    )
            except Exception as e:
                errors["max_production_days"] = e

            # validate price_custom
            try:
                if self.price_custom:
                    result = eval_price_custom(price_custom=self.price_custom,
                                               quantity=1,
                                               model_volume=D('23286.07'),
                                               box_volume=D('422773.03'),
                                               shells_number=D('1'),
                                               area=D('17672.93'),
                                               width=D('75'),
                                               height=D('75'),
                                               depth=D('75'),
                                               machine_volume=D('23286.07'))
                    if result <= 0:
                        raise ValidationError(
                            "Test result (volume=23286.07; box=422773.03; "
                            "shells=1; area=17672.93; width=75; height=75; "
                            "depth=75; machine_volume=23286.07): " +
                            str(result)
                        )

                if not self.price_custom:
                    raise ValidationError("Price custom must exist")

            except Exception as e:
                errors["price_custom"] = e

            # validate support structure parameters
            if self.support_angle > 90 or self.support_angle < 0:
                if self.support_enabled:
                    errors["support_angle"] = ValidationError(
                        _("Angle must be between 0 and 90")
                    )
                else:
                    self.support_angle = 45

            if self.support_offset < 0:
                if self.support_enabled:
                    errors["support_offset"] = ValidationError(
                        _("Offset must be positive")
                    )
                else:
                    self.support_offset = 0

            # validate price_support_custom
            try:
                if self.price_support_custom:
                    result = eval_price_support_custom(
                        price_support_custom=self.price_support_custom,
                        quantity=1,
                        support_volume=D('2328.60'),
                        support_area=D('1767.29'),
                        width=D('75'),
                        height=D('75'),
                        depth=D('75')
                    )

                    if result <= 0:
                        raise ValidationError(
                            _("Test result:") +
                            " (volume=2328.60; area=1767.29; "
                            "width=75; height=75; depth=75): " + str(result)
                        )

                if not self.price_support_custom and self.support_enabled:
                    raise ValidationError(
                        _(
                            'Support formula must be present if'
                            ' Support-Pricing is enabled!'
                        )
                    )

            except Exception as e:
                errors["price_support_custom"] = e

            # validate price_orientation_custom
            try:
                if self.price_orientation_custom:
                    result = eval_price_orientation_custom(
                        price_orientation_custom=self.price_orientation_custom,
                        model_costs={"h-": 352.36},
                        support_costs={"h-": 173.73},
                    )

                    if result <= 0:
                        raise ValidationError(
                            "Test result:" + " (model_costX=173.73, "
                            "support_costX=352.36): " + str(result)
                        )

            except Exception as e:
                errors["price_orientation_custom"] = e

            # validate max bound x
            try:
                if not self.max_x_mm:
                    raise ValidationError(_("Maximum x in mm must exist"))
            except Exception as e:
                errors["max_x_mm"] = e

            # validate max bound y
            try:
                if not self.max_y_mm:
                    raise ValidationError(_("Maximum y in mm must exist"))
            except Exception as e:
                errors["max_y_mm"] = e

            # validate max bound z
            try:
                if not self.max_z_mm:
                    raise ValidationError(_("Maximum z in mm must exist"))
            except Exception as e:
                errors["max_z_mm"] = e

        if len(errors) > 0:
            raise ValidationError(errors)

    def save(self, force_update=False, **kwargs):
        if not self.partner_sku:
            self.partner_sku = slugify(self.partner_sku_default_slug)
        return super(StockRecord, self).save(
            force_update=force_update, **kwargs
        )

    def add_manual_pricing_rule(self, *conditions):
        rule = self.rules.create(conclusion="manual_pricing")
        if conditions:
            for condition in conditions:
                rule.add_condition(condition)
        return rule

    def get_manual_pricing_rules(self):
        return self.rules.filter(conclusion="manual_pricing")

    def clear_manual_pricing_rules(self):
        return self.get_manual_pricing_rules().delete()

    def add_discount_rule(self, discount, *conditions):
        rule = self.rules.create(conclusion="discount")
        if conditions:
            for condition in conditions:
                rule.add_condition(condition)
        if discount:
            rule.add_conclusion_action(discount)
        return rule

    def get_discount_rules(self):
        return self.rules.filter(conclusion="discount")

    def clear_discount_rules(self):
        return self.get_discount_rules().delete()

    # used by dump_site/load_site
    def natural_key(self):
        return self.partner.code, self.partner_sku

    # --- OSCAR --- #
    @property
    def price_currency(self):  # Oscar use it
        return self.partner.price_currency

    def __str__(self):
        msg = "Partner: %s, product: %s" % (
            self.partner.display_name,
            self.product,
        )
        if self.partner_sku:
            msg = "%s (%s)" % (msg, self.partner_sku)
        return msg

    class Meta:
        app_label = 'partner'
        verbose_name = _("Stock record")
        verbose_name_plural = _("Stock records")

    @property
    def net_stock_level(self):
        """
        The effective number in stock (eg available to buy).

        This is correct property to show the customer, not the num_in_stock
        field as that doesn't account for allocations.  This can be negative in
        some unusual circumstances
        """
        if self.num_in_stock is None:
            return 0
        if self.num_allocated is None:
            return self.num_in_stock
        return self.num_in_stock - self.num_allocated

    def fits_in_bounds(self, stl_file, scale=1.0):
        if stl_file.parameter is None:
            raise MissingParameterException

        given_bounds = stl_file.parameter.get_bounds(scale)

        min_allowed_bounds = [self.min_x_mm,
                              self.min_y_mm,
                              self.min_z_mm]
        max_allowed_bounds = [self.max_x_mm,
                              self.max_y_mm,
                              self.max_z_mm]
        return fit_in_bounds(
            min_allowed_bounds, max_allowed_bounds, given_bounds
        )
