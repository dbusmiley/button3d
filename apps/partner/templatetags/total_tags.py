from django.template import Library

register = Library()


@register.filter
def format_vat_rate(vat_rate):
    # round to 5 decimal places if necessary and trim trailing zeroes
    vat_string = str(round(float(vat_rate), 5))
    return vat_string
