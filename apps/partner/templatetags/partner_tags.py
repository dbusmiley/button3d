from django.template import Library
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from apps.b3_misc.templatetags.b3_utils import inline_truncate

register = Library()


@register.filter
def get_partner_logo_tag(partner):
    truncated_name = inline_truncate(partner.name, 25)
    if partner.logo:
        url = strip_tags(partner.logo.url)
        tag = f'<img src="{url}" style="width:180px;" alt="{truncated_name}">'
    else:
        tag = f'<p>{truncated_name}</p>'
    return mark_safe(tag)  # nosec


@register.filter
def get_partner_logo_temp_tag(partner):
    truncated_name = inline_truncate(partner.name, 25)
    if partner.logo_temp:
        url = strip_tags(partner.logo_temp.url)
        tag = f'<img src="{url}" style="width:180px;" alt="{truncated_name}">'
    else:
        tag = f'<p>{truncated_name}</p>'
    return mark_safe(tag)  # nosec
