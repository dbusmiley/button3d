import logging
import os
from decimal import Decimal as D

from django.utils import timezone
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from stdimage.utils import UploadTo
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.utils import Orientation

logger = logging.getLogger(__name__)


def get_option(options, key):
    try:
        return (item
                for item in options
                if item["option"].name == key).next()['value']
    except BaseException:
        return None


class UploadToPartnerLogoSlug(UploadTo):
    def __init__(self, populate_from, **kwargs):
        self.populate_from = populate_from
        self.folder = kwargs.get('folder')
        super(UploadToPartnerLogoSlug, self).__init__(populate_from, **kwargs)

    def __call__(self, instance, filename):
        field_value = getattr(instance, self.populate_from)
        folder_value = self.folder
        now_formatted = timezone.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.kwargs.update({
            'path': 'partners/%s/' % folder_value,
            'name': 'logo_{0}_{1}'.format(slugify(field_value), now_formatted),
        })

        filename_without_extension, extension = os.path.splitext(filename)
        filename_with_date = '{0}{1}{2}'.format(
            filename_without_extension, now_formatted, extension
        )
        return super(UploadToPartnerLogoSlug, self).__call__(
            instance, filename_with_date
        )


def validate_slug(value):
    """
    Method to check if the value given as parameter is in the slug format.
    If not it will raise a validation error. This is used as validator for
    the django models to secure the properties to be slugs. Otherwise there
    can be weird issues in the JavaScript because the fulfillment partner
    cant be viewed.
    :param value: Value which should be checked for a slug-ish type.
    :return: Nothing if successful. Otherwise it will raise a validation error.
    """
    if value != slugify(value):
        raise ValidationError(_('This value needs to be in the slug format'))


class AdditionalPricingInfo(dict):
    def __init__(self, stock_record, stl_file, scale):
        super(AdditionalPricingInfo, self).__init__()
        self.stock_record = stock_record
        self.stl_file = stl_file
        self.scale = scale
        self.update(self._build_additional_info())

    def update_orientation(self, orientation, **kwargs):
        support_info = self['support']
        orientation_label = 'orientation{0}'.format(
            Orientation.ORIENTATIONS.index(orientation) + 1
        )
        support_info.setdefault(orientation_label, {}).update(kwargs)

    def _build_additional_info(self):
        parameter = self.stl_file.parameter
        model_volume = D(parameter.get_volume(self.scale))
        model_box_volume = D(parameter.get_bb_volume(self.scale))
        model_area = D(parameter.get_area(self.scale))
        model_shells_number = parameter.shells if parameter.shells else 1
        model_machine_volume = D(parameter.get_machine_volume(self.scale))

        additional_info = {
            "volume": model_volume,
            "box_volume": model_box_volume,
            "area": model_area,
            "shells_number": model_shells_number,
            "machine_volume": model_machine_volume
        }

        if self.stock_record.support_enabled:
            additional_info['support'] = {}
        else:
            dimensions = {
                'w': D(self.stl_file.parameter.get_bb_width(self.scale)),
                'd': D(self.stl_file.parameter.get_bb_depth(self.scale)),
                'h': D(self.stl_file.parameter.get_bb_height(self.scale))
            }
            additional_info.update(dimensions)
        return additional_info
