from collections import namedtuple
from dateutil.relativedelta import relativedelta

Event = namedtuple('Event', ['actor', 'timestamp'])
StateChange = namedtuple(
    'StateChange',
    ['actor', 'timestamp', 'new_state', 'old_state']
)
TimeInterval = namedtuple('TimeInterval', ['start', 'end'])


class VERBS(object):
    INTERACTS = 'interacts'
    STATE_CHANGE = 'state_change'


class REGISTRATION(object):
    FIRST_STEP = 'first_step'
    REGISTERED = 'registered'


class ORDER(object):
    PENDING = 'Pending'
    PRINTING = 'Printing'
    SHIPPED = 'Shipped'
    CANCELLED = 'Cancelled'


def get_time_intervals(start, end, aggregation_type):
    if aggregation_type is None:
        return [TimeInterval(start=start, end=end)]

    interval_size = get_interval_size(aggregation_type)
    intervals = []
    interval_start = end
    while interval_start > start:
        interval_end = interval_start
        interval_start = interval_end - interval_size
        intervals.append(TimeInterval(start=interval_start, end=interval_end))
    return list(reversed(intervals))


def get_interval_size(aggregation_type):
    if aggregation_type == 'daily':
        return relativedelta(days=1)
    if aggregation_type == 'weekly':
        return relativedelta(weeks=1)
    if aggregation_type == 'monthly':
        return relativedelta(months=1)
    if aggregation_type == 'quarterly':
        return relativedelta(months=3)
    if aggregation_type == 'yearly':
        return relativedelta(years=1)
