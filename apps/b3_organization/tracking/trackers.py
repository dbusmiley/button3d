import logging

from actstream import action as action_signal
from actstream.models import model_stream
from django.apps.registry import apps
from django.contrib.auth.models import User

from apps.b3_organization.tracking.utils import Event, get_time_intervals, \
    REGISTRATION, VERBS, StateChange, ORDER

logger = logging.getLogger(__name__)


class AbstractActivityTracker(object):
    model_class = None
    verb = None

    def get_stream_queryset(self, start=None, end=None):
        stream = model_stream(
            apps.get_model(*self.model_class.split('.')),
            verb=self.verb
        ).order_by('timestamp')
        if start:
            stream = stream.filter(timestamp__gte=start)
        if end:
            stream = stream.filter(timestamp__lt=end)
        return stream


class AbstractEventTracker(AbstractActivityTracker):
    def get_stream(self, start=None, end=None):
        stream = self.get_stream_queryset(start, end)
        return (
            Event(
                actor=action.actor,
                timestamp=action.timestamp
            ) for action in stream
        )

    def get_aggregated(
        self, start, end, aggregation_type=None, unique_actors=True
    ):
        intervals = get_time_intervals(start, end, aggregation_type)
        event_stream = self.get_stream(
            intervals[0].start,
            intervals[-1].end
        )
        if unique_actors:
            return self.get_unique_actors_for_intervals(
                event_stream, intervals
            )
        return self.get_actors_for_intervals(event_stream, intervals)

    @classmethod
    def register_event(cls, actor):
        action_signal.send(actor, verb=cls.verb)

    @staticmethod
    def get_unique_actors_for_intervals(event_stream, intervals):
        event = next(event_stream, None)
        for interval in intervals:
            actors = set()
            while event and event.timestamp < interval.end:
                if event.timestamp >= interval.start:
                    actors.add(event.actor)
                event = next(event_stream, None)
            yield interval, list(actors)

    @staticmethod
    def get_actors_for_intervals(event_stream, intervals):
        event = next(event_stream, None)
        for interval in intervals:
            actors = []
            while event and event.timestamp < interval.end:
                if event.timestamp >= interval.start:
                    actors.append(event.actor)
                event = next(event_stream, None)
            yield interval, list(actors)


class AbstractStateTracker(AbstractActivityTracker):
    verb = VERBS.STATE_CHANGE
    states = None

    def get_stream(self, start=None, end=None):
        stream = self.get_stream_queryset(start, end)
        return (
            StateChange(
                actor=action.actor,
                timestamp=action.timestamp,
                new_state=action.data['new_state'],
                old_state=action.data.get('old_state', None)
            ) for action in stream
        )

    def get_aggregated(self, start, end, aggregation_type=None):
        intervals = get_time_intervals(start, end, aggregation_type)
        stream = self.get_stream(end=intervals[-1].end)
        return self.get_actors_in_states_for_intervals(
            stream, intervals, self.states
        )

    @classmethod
    def register_state_change(cls, actor, new_state, old_state=None):
        action_signal.send(
            actor, verb=cls.verb, new_state=new_state, old_state=old_state
        )

    @classmethod
    def get_actors_in_states_for_intervals(
        cls, state_change_stream, intervals, states
    ):
        actors_in_state = {state: set() for state in states}
        state_change = next(state_change_stream, None)

        for interval in intervals:
            while state_change and state_change.timestamp < interval.end:
                cls.process_state_change(actors_in_state, state_change)
                state_change = next(state_change_stream, None)

            yield interval, {
                state: list(actors)
                for state, actors in actors_in_state.items()
            }

    @staticmethod
    def process_state_change(actors_in_state, state_change):
        try:
            actors_in_state[state_change.new_state].add(state_change.actor)
            if state_change.old_state:
                actors_in_state[state_change.old_state].remove(
                    state_change.actor
                )
        except KeyError:
            warning_message = (f'Error while processing state change '
                               f'({state_change.old_state} '
                               f'-> {state_change.new_state}): '
                               f'Actor {state_change.actor} is not in '
                               f'state {state_change.old_state}')
            logger.warning(warning_message)


class UserInteractionTracker(AbstractEventTracker):
    model_class = 'auth.User'
    verb = VERBS.INTERACTS

    def __init__(self, organization=None):
        self.organization = organization

    def get_stream_queryset(self, start=None, end=None):
        stream = super(UserInteractionTracker, self).get_stream_queryset(
            start, end
        )
        if self.organization is None:
            return stream

        users_in_organization = User.objects.filter(
            userprofile__site__organization=self.organization
        )
        return stream.filter(
            actor_object_id__in=[user.id for user in users_in_organization]
        )


class RegistrationStateTracker(AbstractStateTracker):
    model_class = 'b3_signup.Signup'
    states = [REGISTRATION.FIRST_STEP, REGISTRATION.REGISTERED]

    def __init__(self, organization=None):
        self.organization = organization

    def get_stream_queryset(self, start=None, end=None):
        stream = super(RegistrationStateTracker, self).get_stream_queryset(
            start, end
        )
        if self.organization is None:
            return stream

        return stream.filter(
            signup__site__organization=self.organization
        )


class OrderStateTracker(AbstractStateTracker):
    model_class = 'b3_order.Order'
    states = [ORDER.PENDING, ORDER.PRINTING, ORDER.SHIPPED, ORDER.CANCELLED]

    def __init__(self, organization=None, partner=None):
        self.organization = organization
        self.partner = partner

    def get_stream_queryset(self, start=None, end=None):
        stream = model_stream(
            apps.get_model('b3_order.Order'),
            verb=self.verb
        ).order_by('timestamp')
        if self.partner:
            stream = stream.filter(
                order__partner=self.partner
            ).distinct()  # XXX: double distinct?
        if self.organization:
            # @formatter:off
            stream = stream.filter(
                order__partner__site__organization=self.organization
            )
            # @formatter:on
        return stream.distinct()
