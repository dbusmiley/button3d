from modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from django.contrib import admin
from django.utils.translation import activate, get_language, ugettext, \
    ugettext_lazy as _

from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from apps.partner.models import Partner
from apps.b3_organization.models import Organization, Link, KeyManager, \
    OrganizationTheme, UserProfile, OrganizationCustomTemplate
from apps.b3_organization.forms import OrganizationForm, OrganizationThemeForm
from apps.b3_core.admin import DeletedDateAdmin


class LinkTranslatedAdmin(SortableAdminMixin, TranslationAdmin):
    list_display = ('_get_organization', 'title', 'url', 'published',)
    list_filter = ('organization_theme',)

    def _get_organization(self, obj):
        return '%s' % obj.organization_theme.site.organization
    _get_organization.short_description = _('Organization')

    def publish_link(modeladmin, request, queryset):
        queryset.update(published=True)
    publish_link.short_description = _('Publish selected links')

    def hide_link(modeladmin, request, queryset):
        queryset.update(published=False)
    hide_link.short_description = _('Hide selected links')

    actions = [publish_link, hide_link]


class LinkInline(SortableInlineAdminMixin, TranslationTabularInline):
    model = Link
    verbose_name = _('Link Footer')
    verbose_name_plural = _('Links Footer')
    extra = 0


class OrganizationThemeAdmin(TranslationAdmin):
    form = OrganizationThemeForm
    list_display = ('site', )
    search_fields = ['site__domain']

    fieldsets = (
        (None, {
            'fields': ('site',)
        }),
        ('Colors', {
            'classes': ('collapse',),
            'fields': (
                'body_bkg_color',
                'navbar_bkg_color',
                'navbar_txt_color',
                'primary_bkg_color',
                'primary_txt_color',
                'primary_elements_color',
                'btn_secondary_bkg_color',
                'btn_secondary_txt_color',
                'link_color',
                'checkout_navbar_bkg_color',
                'checkout_navbar_txt_color',
                'viewer_bkg_color',
                'viewer_txt_color',
                'warning_color',
                'error_color',
                'success_color',
            )
        }),
        ('Images', {
            'classes': ('collapse',),
            'fields': (
                (
                    'navbar_logo_image',
                    'navbar_logo_url'
                ),
                'favicon',
                'email_logo_image',
                (
                    'viewer_bkg_image',
                    'viewer_bkg_repeat'
                )
            )
        }),
        ('General Data', {
            'classes': ('collapse',),
            'fields': (
                (
                    'description_portal_en',
                    'description_portal_de'
                ),
                'customer_support_phone',
                'project_default_material',
                'is_address_company_required',
                'powered_by_3yourmind',
                (
                    'signup_text_button_en',
                    'signup_text_button_de'
                ),
                (
                    'signup_text_en',
                    'signup_text_de'
                ),
                'customize_css')
        }),
        ('Additional Panel at Supplier', {
            'classes': ('collapse',),
            'fields': (
                'supplier_additional_panel_text_en',
                'supplier_additional_panel_text_de',
            )
        }),
        ('Email Footer', {
            'classes': ('collapse',),
            'fields': (
                'from_email',
                'footer_email_en',
                'footer_email_de',
            )
        }),
        ('Website Footer - Contact', {
            'classes': ('collapse',),
            'fields': (
                'contact_info_footer_en',
                'contact_info_footer_de'
            )
        }),
        ('Manual Request', {
            'classes': ('collapse',),
            'fields': (
                'request_receiving_email',
                (
                    'request_page_title_en',
                    'request_page_title_de'
                ),
                (
                    'request_user_subject_mail_en',
                    'request_user_subject_mail_de'
                ),
                'request_attachment_explanation_text_en',
                'request_attachment_explanation_text_de',
                'request_compare_step3_text_en',
                'request_compare_step3_text_de',
                'request_user_confirmation_enabled',
                'request_user_confirmation_text_en',
                'request_user_confirmation_text_de'
            )
        }),
    )

    inlines = [LinkInline, ]
    sortable_field_name = "position"

    class Media:
        css = {
            'all': ('css/admin_custom.css',)
        }


class OrganizationAdmin(DeletedDateAdmin, TranslationAdmin):
    form = OrganizationForm
    list_display = ('site', 'showname')
    search_fields = ['site__domain']

    fieldsets = (
        (None, {
            'fields': (
                'site',
                'slug',
                'showname',
                'description',
                # 'creation_date',
                'partners_enabled',
                'private_api_key',
            )
        }),
        ('Authentication', {
            'classes': ('collapse',),
            'fields': (
                'auth_plugins_enabled',
                'disable_normal_login',
                'automatic_redirect_to_oauth',
                'is_http_auth_enabled',
                'http_auth_user',
                'http_auth_password',
                'is_company_information_in_payment_step_enabled'
            )
        }),
        ('User Roles', {
            'classes': ('collapse',),
            'fields': (
                'key_manager',
                'manual_pricing_user',
                'printing_service_onboarding_user',
                'force_verify_customer_address',
                'verify_customer_address_users',
                'is_extended_registration_enabled',
                'organization_panel_admins',
                'notify_organizational_panel_admins'
            )
        }),
        ('Company, VAT and Customer Number', {
            'classes': ('collapse',),
            'fields': (
                'company_information_visibility',
                'vat_id_label_en',
                'vat_id_label_de',
                'customer_number_visibility',
                'customer_number_label_en',
                'customer_number_label_de',
            )
        }),
        ('File Validation', {
            'classes': ('collapse',),
            'fields': (
                'file_validation_enabled',
                'file_validation_message'
            )
        }),
        ('General Settings', {
            'classes': ('collapse',),
            'fields': (
                'is_single_supplier_account',
                'anonymous_upload_enabled',
                'register_enabled',
                'login_enabled',
                'filters_enabled',
                'link_sharing_enabled',
                'manual_request_attachment_upload_enabled',
                'single_upload_redirects_to_project_detail',
                'show_net_prices',
                'mandatory_company_name',
                'enable_optimized_file_download',
                'is_yoda_active',
                'allow_insecure_download',
                'allowed_iframe_domains'
            )
        }),
        ('Terms and Conditions', {
            'classes': ('collapse',),
            'fields': (
                (
                    'terms_conditions_en',
                    'terms_conditions_de'
                )
            )
        }),
    )

    def get_queryset(self, request):
        return Organization.objects.all()

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == "partners_enabled":
            kwargs["queryset"] = Partner.all_objects.all()
        return super(OrganizationAdmin, self).formfield_for_manytomany(
            db_field, request, **kwargs)

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(OrganizationAdmin, self).formfield_for_dbfield(
            db_field, **kwargs)
        if db_field.name == 'terms_conditions_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext(
                "I've read and accept the <a href='"
                "https://www.3yourmind.com/terms' target='_blank'>"
                "Terms & Conditions</a>")
            activate(current_lang)
        if db_field.name == 'vat_id_label_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext('VAT Number')
            activate(current_lang)
        if db_field.name == 'customer_number_label_de':
            current_lang = get_language()
            activate('de')
            field.initial = ugettext('Customer Number')
            activate(current_lang)
        return field


class KeyManagerTranslatedAdmin(TranslationAdmin):
    pass


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'site')
    search_fields = [
        'user__username',
        'user__first_name',
        'site__name'
    ]

    def get_queryset(self, request):
        return UserProfile.all_objects.all()


admin.site.register(Organization, OrganizationAdmin)

admin.site.register(Link, LinkTranslatedAdmin)

admin.site.register(KeyManager, KeyManagerTranslatedAdmin)

admin.site.register(OrganizationTheme, OrganizationThemeAdmin)

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(OrganizationCustomTemplate)
