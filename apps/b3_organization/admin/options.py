from django.contrib import admin
from ..models import OrganizationPartnerOption, OrganizationProductOption
from ..forms import OrganizationPartnerOptionForm, \
    OrganizationProductOptionForm


class OrganizationPartnerAdmin(admin.ModelAdmin):
    list_display = ('partner', 'site')
    ordering = ('site', 'partner')


class OrganizationProductOptionAdmin(admin.ModelAdmin):
    form = OrganizationProductOptionForm
    list_display = ('site', 'product', 'key', 'value', )


class OrganizationPartnerOptionAdmin(admin.ModelAdmin):
    form = OrganizationPartnerOptionForm

    list_display = ('site', 'get_partners_string', 'key', 'value', )
    list_filter = ('site', 'partners__name', )

    class Media:
        js = (
            'js/utils.js',
            'js/admin/custom-richtextfields.js',
            'ckeditor/ckeditor/ckeditor.js',
            'ckeditor/ckeditor-init.js'
        )


admin.site.register(OrganizationPartnerOption, OrganizationPartnerOptionAdmin)
admin.site.register(OrganizationProductOption, OrganizationProductOptionAdmin)
