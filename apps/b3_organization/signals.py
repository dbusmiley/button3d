import types
import logging
import typing as t
from django.dispatch import receiver
from django.db.models.signals import class_prepared


logger = logging.getLogger(__name__)

if t.TYPE_CHECKING:
    class Frame:
        #: next outer frame (this frame's caller)
        f_back: 'Frame' = ...
        #: built-in namespace seen by this frame
        f_builtins: t.Any = ...
        #: code object being executed in this frame
        f_code: t.Any = ...
        #: global namespace seen by this frame
        f_globals: t.Tuple[t.Any] = ...
        #: index of last attempted instruction in bytecode
        f_lasti: int = ...
        #: current line number in Python source code
        f_lineno: int = ...
        #: local namespace seen by this frame
        f_locals: t.Tuple[t.Any] = ...
        #: tracing function for this frame, or None
        f_trace: t.Optional[t.Any] = ...

    class Traceback:
        #: filename of the frame object this information belongs to
        filename: str = ...
        #: line number of the frame object
        lineno: int = ...
        #: function or method name the current line belongs to
        function: str = ...
        #: lines around the current line
        code_context: t.List[str] = ...
        #: index of the current line within the context
        index: int = ...


@receiver(class_prepared)
def site_monkey_patch(sender, **kwargs):
    """
    Monkey patch Site.objects.get_current to
        get current site from threadlocals instead
    of using settings.SITE_ID. class_prepared signal
        receiver must always be placed in __init__.py
    and it should run before import of sender class (Site).
    """

    def _get_current_site(self, request=None):
        import inspect
        from apps.b3_organization.utils import get_current_site

        caller: Frame = inspect.currentframe().f_back
        info: Traceback = inspect.getframeinfo(caller)
        filename: str = info.filename
        # Raven (sentry's client) calls this in it's logger, so prevent
        # infinite recursion here.
        if '/site-packages/' in filename and '/raven/' not in filename:
            logger.warning(
                f'Site.objects.get_current() called from {filename}'
            )

        try:
            site = get_current_site(request)
        except Exception:
            # For debugging, as we got unexpected uncaught exceptions
            # with monkey patched Site.objects.get_current
            logger.info('Unable to get current site!')
            site = None

        return site

    if sender.__module__ == 'django.contrib.sites.models' and \
            sender.__name__ == 'Site':
        from django.contrib.sites.models import SiteManager
        SiteManager.get_current = types.MethodType(
            _get_current_site, SiteManager
        )
        sender.objects.get_current = types.MethodType(
            _get_current_site,
            sender.objects
        )
