from modeltranslation.translator import translator, TranslationOptions
from apps.b3_organization.models import OrganizationTheme, \
    Link, KeyManager, Organization


class OrganizationThemeTranslationOptions(TranslationOptions):
    fields = (
        'footer_email',
        'contact_info_footer',
        'signup_text_button',
        'signup_text',
        'description_portal',
        'supplier_additional_panel_text',

        # Manual requests fields
        'request_page_title',
        'request_attachment_explanation_text',
        'request_compare_step3_text',
        'request_user_subject_mail',
        'request_user_confirmation_text'
    )


class LinkTranslationOptions(TranslationOptions):
    fields = ('title', 'url',)


class KeyManagerTranslationOptions(TranslationOptions):
    fields = ('title', 'role',)


class OrganizationTranslationOptions(TranslationOptions):
    fields = (
        'file_validation_message',
        'terms_conditions',
        'vat_id_label',
        'customer_number_label'
    )


translator.register(OrganizationTheme, OrganizationThemeTranslationOptions)
translator.register(Link, LinkTranslationOptions)
translator.register(KeyManager, KeyManagerTranslationOptions)
translator.register(Organization, OrganizationTranslationOptions)

# Temporary fix for a bug in modeltranslation. See
# https://github.com/deschler/django-modeltranslation/issues/455
# Should be removed as soon as bug is resolved
OrganizationTheme._meta.base_manager_name = None
Link._meta.base_manager_name = None
KeyManager._meta.base_manager_name = None
Organization._meta.base_manager_name = None
