from django.urls import reverse

from apps.b3_core.utils import get_country
from apps.b3_organization.utils import get_customer_support_phone, \
    get_current_site, get_current_org, \
    force_verify_customer_address, getattr_theme
from apps.b3_migration.models import Switch


def site_organizations(request):
    navbar_logo_url = getattr_theme("navbar_logo_url", reverse('home'))
    organization = get_current_org()

    customer_support_phone = get_customer_support_phone(get_country(request))

    return {
        'site': get_current_site(request),
        'organization': organization,
        'navbar_logo_url': navbar_logo_url,
        'customer_support_phone': customer_support_phone,
        'force_verify_customer_address': force_verify_customer_address(),
        'use_left_navigation': Switch.is_active(Switch.LEFT_NAVIGATION),
        'use_new_user_panel': Switch.is_active(Switch.NEW_USER_PANEL)
    }
