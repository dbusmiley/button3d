import logging
import os
from urllib.parse import urlparse
import typing as t

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.http.request import split_domain_port
from django.test import RequestFactory

from button3d.threadlocals import _threadlocals
from button3d import type_declarations as td


logger = logging.getLogger(__name__)


def _set_thread_variable(key, val):
    setattr(_threadlocals, key, val)


def _get_thread_variable(key, default=None):
    return getattr(_threadlocals, key, default)


def _get_site_cache(domain):
    site_cache_key = "b3_organization-site-%s" % domain
    return cache.get(site_cache_key)


def _set_site_cache(domain, site):
    site_cache_key = "b3_organization-site-%s" % domain
    cache.set(
        site_cache_key,
        site,
        getattr(settings, 'B3_SITE_CACHE_TIMEOUT', 0))


def _get_site_by_request(request):
    domain = request.get_host().split(':')[0]

    site_cache = _get_site_cache(domain)
    if site_cache:
        return site_cache
    else:
        site = Site.objects.filter(domain__iexact=domain).first()

    if not site:
        # Fallback to looking up site after stripping port from the host.
        domain, port = split_domain_port(domain)
        if not port:
            if request.environ and 'SERVER_PORT' in request.environ:
                port = request.environ['SERVER_PORT']
            if not port:
                return

        site = Site.objects.filter(domain__iexact=domain).first()

    if site:
        _set_site_cache(domain, site)
    return site


def _get_site_by_environ():
    """
    Get site from os.environ - for pro-account site collectstatic/compress.
    Could be also useful to run separate
        python process tied to a pro account site
    i.e. "SITE_DOMAIN='prodemo.dev.3yourmind.com' python manage.py runserver"
    """
    domain = os.environ.get('SITE_DOMAIN', None)
    if domain:
        site_cache = _get_site_cache(domain)
        if site_cache:
            return site_cache
        else:
            site = Site.objects.filter(domain__iexact=domain).first()
            _set_site_cache(domain, site)
            return site


def get_current_site(request=None) -> t.Optional[td.Site]:
    """
    Get current site from thread local variable if request is not set.

    IMPORTANT: Please keep
    django.contrib.sites.models.Site import here, otherwise
    monkey patching for Site.objects.get_current will not work!!!
    """
    if not request:
        # Get site from os.environ - for site collectstatic/compress
        site = _get_site_by_environ()
        if not site:
            # Site not set in environ - get from thread variable
            return _get_thread_variable('site')
        else:
            return site
    else:
        site = _get_site_by_request(request)
        return site


def set_current_site(value):
    _set_thread_variable('site', value)


def get_current_org() -> td.Organization:
    site = get_current_site()
    return site.organization


def getattr_theme(attr, default=None):
    theme = get_current_org().theme
    return getattr(theme, attr, default)


def get_from_email() -> str:
    from_email = getattr_theme("from_email", settings.DEFAULT_FROM_EMAIL)
    if from_email:
        return from_email
    else:
        return settings.DEFAULT_FROM_EMAIL


def get_customer_support_phone(country: t.Optional[str]) -> str:
    default_phone = "+1 (628) 252 9422" \
                    if country in ["US", "CA"] else "+49 (0)30 555 78748"

    return getattr_theme('customer_support_phone', default_phone)


def force_verify_customer_address():
    org = get_current_org()
    return org.force_verify_customer_address


def get_base_url(org: t.Optional[td.Organization] = None) -> str:
    """
    Returns the complete base url. Either form the app or an organization
    kwargs: can contain 'org' to force the organization url
    """

    http_request = get_current_request()

    parsed_url = urlparse(http_request.build_absolute_uri(None))
    scheme = parsed_url.scheme if parsed_url.scheme else 'https'
    port = ':' + str(parsed_url.port) if parsed_url.port else ''
    _org = org or get_current_org()

    return f'{scheme}://{_org.site.domain}{port}'


class RequestUnavailableException(Exception):
    pass


def get_current_request():
    from apps.b3_core.middleware import THREAD_LOCALS
    if not hasattr(THREAD_LOCALS, 'current_request'):
        raise RequestUnavailableException()
    return THREAD_LOCALS.current_request


def set_current_request(request):
    from apps.b3_core.middleware import THREAD_LOCALS
    THREAD_LOCALS.current_request = request


class CurrentRequestFactory(RequestFactory):
    def __init__(self, site, **defaults):
        super(CurrentRequestFactory, self).__init__(**defaults)
        self.site = site

    def _base_environ(self, **request):
        environ = super(CurrentRequestFactory, self)._base_environ(**request)
        environ['SERVER_NAME'] = self.site.domain
        return environ
