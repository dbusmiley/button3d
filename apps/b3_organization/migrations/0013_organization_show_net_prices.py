# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-06-21 15:55
from __future__ import unicode_literals

from django.db import migrations, models
from django_add_default_value import AddDefaultValue


class Migration(migrations.Migration):

    dependencies = [
        ('b3_organization', '0012_terms_conditions'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='show_net_prices',
            field=models.BooleanField(default=False, help_text='Enabling this will also turn on Mandatory Company Name', verbose_name='Show only Net Prices for customers'),
        ),
        AddDefaultValue(
            model_name='organization',
            name='show_net_prices',
            value=0
        )
    ]
