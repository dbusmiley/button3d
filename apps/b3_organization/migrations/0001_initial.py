# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-09 10:58


import apps.b3_misc.validators
import apps.b3_organization.models.organization
import colorfield.fields
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('catalogue', '0003_auto_20150703_1002'),
        ('auth', '0007_alter_validators_add_error_messages'),
        ('sites', '0002_alter_domain_unique'),
        ('b3_auth_plugins', '0001_initial'),
        ('partner', '0004_auto_20150703_1002'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupProfile',
            fields=[
                ('group_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='auth.Group')),
                ('showname', models.CharField(max_length=50)),
                ('site', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            bases=('auth.group', models.Model),
            managers=[
                ('all_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='KeyManager',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('title_en', models.CharField(max_length=255, null=True)),
                ('title_de', models.CharField(max_length=255, null=True)),
                ('name', models.CharField(max_length=255)),
                ('role', models.CharField(max_length=255)),
                ('role_en', models.CharField(max_length=255, null=True)),
                ('role_de', models.CharField(max_length=255, null=True)),
                ('email', models.CharField(max_length=255)),
                ('telephone', models.CharField(max_length=255)),
                ('manager_photo', models.ImageField(blank=True, null=True, upload_to=b'key-managers/')),
            ],
        ),
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128)),
                ('title_en', models.CharField(max_length=128, null=True)),
                ('title_de', models.CharField(max_length=128, null=True)),
                ('url', models.URLField(blank=True)),
                ('url_en', models.URLField(blank=True, null=True)),
                ('url_de', models.URLField(blank=True, null=True)),
                ('published', models.BooleanField(default=True)),
                ('position', models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='Position')),
            ],
            options={
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True, unique=True)),
                ('showname', models.CharField(blank=True, max_length=255)),
                ('description', models.TextField(blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('disable_normal_login', models.BooleanField(default=False, help_text='Setting this would disable login via Email/Password methods')),
                ('automatic_redirect_to_oauth', models.BooleanField(default=False, help_text="If set, the user would be automatically redirected to OAUTH login page. Use it only after enabling some 'auth_plugins_enabled' with valid configurations.")),
                ('private_api_key', models.CharField(blank=True, max_length=255)),
                ('is_single_supplier_account', models.BooleanField(default=False)),
                ('anonymous_upload_enabled', models.BooleanField(default=True)),
                ('register_enabled', models.BooleanField(default=True)),
                ('login_enabled', models.BooleanField(default=True)),
                ('filters_enabled', models.BooleanField(default=True)),
                ('all_public_partners_enabled', models.BooleanField(default=False, help_text='This field is not used anymore')),
                ('company_sharing_enabled', models.BooleanField(default=True)),
                ('link_sharing_enabled', models.BooleanField(default=True)),
                ('file_validation_enabled', models.BooleanField(default=False)),
                ('file_validation_message', models.TextField(blank=True, help_text='Input fields of type checkbox or radio must define the binding attribute:data-bind="checkedOptions".<br>The value of the input must contain \'disable\'if the corresponding option should disable the upload or the value must contain \'enable\'to enable the upload.<br>By default, when opening the pop-up, upload will be disabled.To enable upload by default, add on the highest HTML tag the attribute:data-default-enable-upload="true".', null=True)),
                ('file_validation_message_en', models.TextField(blank=True, help_text='Input fields of type checkbox or radio must define the binding attribute:data-bind="checkedOptions".<br>The value of the input must contain \'disable\'if the corresponding option should disable the upload or the value must contain \'enable\'to enable the upload.<br>By default, when opening the pop-up, upload will be disabled.To enable upload by default, add on the highest HTML tag the attribute:data-default-enable-upload="true".', null=True)),
                ('file_validation_message_de', models.TextField(blank=True, help_text='Input fields of type checkbox or radio must define the binding attribute:data-bind="checkedOptions".<br>The value of the input must contain \'disable\'if the corresponding option should disable the upload or the value must contain \'enable\'to enable the upload.<br>By default, when opening the pop-up, upload will be disabled.To enable upload by default, add on the highest HTML tag the attribute:data-default-enable-upload="true".', null=True)),
                ('force_verify_customer_address', models.BooleanField(default=False)),
                ('is_extended_registration_enabled', models.BooleanField(default=False)),
                ('manual_request_attachment_upload_enabled', models.BooleanField(default=True)),
                ('single_upload_redirects_to_project_detail', models.BooleanField(default=False)),
                ('is_http_auth_enabled', models.BooleanField(default=False)),
                ('http_auth_user', models.CharField(blank=True, max_length=255, null=True)),
                ('http_auth_password', models.CharField(blank=True, max_length=255, null=True)),
                ('is_company_information_in_payment_step_enabled', models.BooleanField(default=True)),
                ('is_yoda_active', models.BooleanField(default=False)),
                ('auth_plugins_enabled', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='b3_auth_plugins.SiteAuthPluginConfig')),
                ('key_manager', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='b3_organization.KeyManager')),
                ('manual_pricing_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pricing_orgs', to=settings.AUTH_USER_MODEL)),
                ('organization_panel_admins', models.ManyToManyField(blank=True, to=settings.AUTH_USER_MODEL)),
                ('partners_enabled', models.ManyToManyField(to='partner.Partner')),
                ('printing_service_onboarding_user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='onboarding_orgs', to=settings.AUTH_USER_MODEL)),
                ('site', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
                ('verify_customer_address_users', models.ManyToManyField(blank=True, related_name='verify_organizations', to=settings.AUTH_USER_MODEL, verbose_name='Verifying customer address users')),
            ],
        ),
        migrations.CreateModel(
            name='OrganizationCustomTemplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('template_name', models.CharField(help_text="A template path like 'b3_core/nav.html'", max_length=255)),
                ('template', models.TextField(blank=True, help_text='The HTML template')),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
        ),
        migrations.CreateModel(
            name='OrganizationPartnerOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(message='Key contains invalid characters!', regex=b'[a-zA-Z_][a-zA-Z0-9_]*')])),
                ('value', models.CharField(max_length=255)),
                ('partners', models.ManyToManyField(to='partner.Partner', verbose_name=b'Partners')),
                ('site', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrganizationProductOption',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(message='Key contains invalid characters!', regex=b'[a-zA-Z_][a-zA-Z0-9_]*')])),
                ('value', models.CharField(max_length=255)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalogue.Product')),
                ('site', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrganizationTheme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('body_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#EEEEEE', max_length=10, verbose_name='Background color')),
                ('navbar_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#FFFFFF', max_length=10, verbose_name='Topmost navigation bar color')),
                ('navbar_txt_color', colorfield.fields.ColorField(blank=True, default=b'#555555', max_length=10, verbose_name='Topmost navigation bar text color  ')),
                ('navbar_logo_image', models.ImageField(blank=True, null=True, upload_to=apps.b3_organization.models.organization._theme_upload_to, verbose_name='Logo')),
                ('navbar_logo_url', models.URLField(blank=True, verbose_name='Logo link')),
                ('primary_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#2C66C4', max_length=10, verbose_name='Main color')),
                ('primary_txt_color', colorfield.fields.ColorField(blank=True, default=b'#FFFFFF', max_length=10, verbose_name='Main text color')),
                ('primary_elements_color', colorfield.fields.ColorField(blank=True, default=b'#2C66C4', help_text='Cube, Sliders, highlights, calendar, header-bar of uploaded 3D model, ...', max_length=10, verbose_name='Main color for Visual elements')),
                ('btn_secondary_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#FF7800', max_length=10, verbose_name='Secondary button color')),
                ('btn_secondary_txt_color', colorfield.fields.ColorField(blank=True, default=b'#FFFFFF', max_length=10, verbose_name='Secondary button text color')),
                ('link_color', colorfield.fields.ColorField(blank=True, default=b'#2C66C4', max_length=10, verbose_name='Link color')),
                ('checkout_navbar_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#3D3D3D', max_length=10, verbose_name='Second navigation bar color')),
                ('checkout_navbar_txt_color', colorfield.fields.ColorField(blank=True, default=b'#FFFFFF', max_length=10, verbose_name='Second navigation bar text color')),
                ('viewer_bkg_image', models.ImageField(blank=True, null=True, upload_to=apps.b3_organization.models.organization._theme_upload_to, verbose_name='3D Viewer background image')),
                ('viewer_bkg_color', colorfield.fields.ColorField(blank=True, default=b'#2c66c4', max_length=10, verbose_name='3D Viewer background Color')),
                ('viewer_txt_color', colorfield.fields.ColorField(blank=True, default=b'#ccc', max_length=10, verbose_name='3D Viewer text color')),
                ('viewer_bkg_repeat', models.CharField(choices=[(b'repeat', b'Repeat Background'), (b'cover', b'Display as cover')], default=b'repeat', max_length=255, verbose_name='3D Viewer background repeat')),
                ('error_color', colorfield.fields.ColorField(blank=True, default=b'#a7082a', max_length=10, verbose_name='Custom color Error')),
                ('warning_color', colorfield.fields.ColorField(blank=True, default=b'#ff7800', max_length=10, verbose_name='Custom color Warning')),
                ('success_color', colorfield.fields.ColorField(blank=True, default=b'#3c9d07', max_length=10, verbose_name='Custom color Success')),
                ('favicon', models.ImageField(blank=True, null=True, upload_to=apps.b3_organization.models.organization._theme_upload_to, validators=[apps.b3_misc.validators.FileValidator(allowed_mimetypes=(b'image/vnd.microsoft.icon', b'image/x-icon'))], verbose_name='Custom Favicon (.ico)')),
                ('is_enabled', models.BooleanField(default=False, verbose_name='Enable template - Not used anymore')),
                ('is_iframe', models.BooleanField(default=False, verbose_name='Is Iframe?')),
                ('portal_name', models.CharField(default=b'3YOURMIND', max_length=255, verbose_name='Portal Name ProAccount - Not used anymore. Use Organization showname instead.')),
                ('description_portal', models.CharField(default=b'Only the best 3D printing services', max_length=255, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.')),
                ('description_portal_en', models.CharField(default=b'Only the best 3D printing services', max_length=255, null=True, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.')),
                ('description_portal_de', models.CharField(default=b'Only the best 3D printing services', max_length=255, null=True, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.')),
                ('customer_support_phone', models.CharField(default=b'+49 (0)30 555 78748', max_length=255, verbose_name='Customer Support Telephone')),
                ('email_logo_image', models.ImageField(blank=True, null=True, upload_to=apps.b3_organization.models.organization._theme_upload_to)),
                ('footer_email', models.TextField(blank=True, null=True, verbose_name='Email footer')),
                ('footer_email_en', models.TextField(blank=True, null=True, verbose_name='Email footer')),
                ('footer_email_de', models.TextField(blank=True, null=True, verbose_name='Email footer')),
                ('from_email', models.CharField(blank=True, default=b'', max_length=255, verbose_name='Email From Address')),
                ('contact_info_footer', models.TextField(blank=True, null=True, verbose_name='Website footer - contact information')),
                ('contact_info_footer_en', models.TextField(blank=True, null=True, verbose_name='Website footer - contact information')),
                ('contact_info_footer_de', models.TextField(blank=True, null=True, verbose_name='Website footer - contact information')),
                ('powered_by_3yourmind', models.BooleanField(default=True)),
                ('signup_text_button', models.CharField(default=b'Sign Up', max_length=24, verbose_name='Text button Sign Up Modal')),
                ('signup_text_button_en', models.CharField(default=b'Sign Up', max_length=24, null=True, verbose_name='Text button Sign Up Modal')),
                ('signup_text_button_de', models.CharField(default=b'Sign Up', max_length=24, null=True, verbose_name='Text button Sign Up Modal')),
                ('signup_text', models.CharField(default=b'Sign up for your account and start managing your 3D printing projects online.', max_length=255, verbose_name='Text content Sign Up Modal')),
                ('signup_text_en', models.CharField(default=b'Sign up for your account and start managing your 3D printing projects online.', max_length=255, null=True, verbose_name='Text content Sign Up Modal')),
                ('signup_text_de', models.CharField(default=b'Sign up for your account and start managing your 3D printing projects online.', max_length=255, null=True, verbose_name='Text content Sign Up Modal')),
                ('request_receiving_email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email address where the manual requests should be sent')),
                ('request_page_title', models.CharField(blank=True, max_length=256, null=True, verbose_name='Title of the manual request page')),
                ('request_page_title_en', models.CharField(blank=True, max_length=256, null=True, verbose_name='Title of the manual request page')),
                ('request_page_title_de', models.CharField(blank=True, max_length=256, null=True, verbose_name='Title of the manual request page')),
                ('request_attachment_explanation_text', models.TextField(blank=True, null=True, verbose_name='Explanation text under attachment on manual request page')),
                ('request_attachment_explanation_text_en', models.TextField(blank=True, null=True, verbose_name='Explanation text under attachment on manual request page')),
                ('request_attachment_explanation_text_de', models.TextField(blank=True, null=True, verbose_name='Explanation text under attachment on manual request page')),
                ('request_compare_step3_text', models.TextField(blank=True, default=b'Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button')),
                ('request_compare_step3_text_en', models.TextField(blank=True, default=b'Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button')),
                ('request_compare_step3_text_de', models.TextField(blank=True, default=b'Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button')),
                ('request_user_subject_mail', models.CharField(blank=True, default=b'3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user')),
                ('request_user_subject_mail_en', models.CharField(blank=True, default=b'3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user')),
                ('request_user_subject_mail_de', models.CharField(blank=True, default=b'3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user')),
                ('request_user_confirmation_enabled', models.BooleanField(default=False)),
                ('request_user_confirmation_text', models.TextField(blank=True, default=b'Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user')),
                ('request_user_confirmation_text_en', models.TextField(blank=True, default=b'Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user')),
                ('request_user_confirmation_text_de', models.TextField(blank=True, default=b'Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user')),
                ('supplier_additional_panel_text', models.TextField(blank=True, default=b'', null=True, verbose_name='Text on addtional panel on supplier page')),
                ('supplier_additional_panel_text_en', models.TextField(blank=True, default=b'', null=True, verbose_name='Text on addtional panel on supplier page')),
                ('supplier_additional_panel_text_de', models.TextField(blank=True, default=b'', null=True, verbose_name='Text on addtional panel on supplier page')),
                ('customize_css', models.TextField(blank=True, null=True, verbose_name='Customize CSS')),
                ('is_address_company_required', models.BooleanField(default=False, verbose_name='Is address company field required')),
                ('project_default_material', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalogue.Product', verbose_name='3D Project default material')),
                ('site', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('site', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            managers=[
                ('all_objects', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AddField(
            model_name='link',
            name='organization_theme',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='b3_organization.OrganizationTheme'),
        ),
        migrations.AlterUniqueTogether(
            name='organizationcustomtemplate',
            unique_together=set([('site', 'template_name')]),
        ),
        migrations.AlterUniqueTogether(
            name='groupprofile',
            unique_together=set([('site', 'showname')]),
        ),
    ]
