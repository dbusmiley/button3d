# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-03-15 17:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('b3_organization', '0004_auto_20180310_1737'),
        ('b3_organization', '0004_auto_20180314_1057'),
    ]

    operations = [
    ]
