# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-03-07 16:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_organization', '0002_auto_20180227_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='deleted_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Date deleted'),
        ),
    ]
