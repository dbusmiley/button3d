# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-03-10 17:37
from __future__ import unicode_literals

import apps.b3_misc.validators
import apps.b3_organization.models.organization
import colorfield.fields
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_organization', '0003_organization_deleted_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='keymanager',
            name='manager_photo',
            field=models.ImageField(blank=True, null=True, upload_to='key-managers/'),
        ),
        migrations.AlterField(
            model_name='organizationpartneroption',
            name='key',
            field=models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(message='Key contains invalid characters!', regex='[a-zA-Z_][a-zA-Z0-9_]*')]),
        ),
        migrations.AlterField(
            model_name='organizationpartneroption',
            name='partners',
            field=models.ManyToManyField(to='partner.Partner', verbose_name='Partners'),
        ),
        migrations.AlterField(
            model_name='organizationproductoption',
            name='key',
            field=models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(message='Key contains invalid characters!', regex='[a-zA-Z_][a-zA-Z0-9_]*')]),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='body_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#EEEEEE', max_length=10, verbose_name='Background color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='btn_secondary_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#FF7800', max_length=10, verbose_name='Secondary button color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='btn_secondary_txt_color',
            field=colorfield.fields.ColorField(blank=True, default='#FFFFFF', max_length=10, verbose_name='Secondary button text color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='checkout_navbar_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#3D3D3D', max_length=10, verbose_name='Second navigation bar color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='checkout_navbar_txt_color',
            field=colorfield.fields.ColorField(blank=True, default='#FFFFFF', max_length=10, verbose_name='Second navigation bar text color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='customer_support_phone',
            field=models.CharField(default='+49 (0)30 555 78748', max_length=255, verbose_name='Customer Support Telephone'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='description_portal',
            field=models.CharField(default='Only the best 3D printing services', max_length=255, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='description_portal_de',
            field=models.CharField(default='Only the best 3D printing services', max_length=255, null=True, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='description_portal_en',
            field=models.CharField(default='Only the best 3D printing services', max_length=255, null=True, verbose_name='Description Portal - this is shown when a link is posted in facebook or slack.'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='error_color',
            field=colorfield.fields.ColorField(blank=True, default='#a7082a', max_length=10, verbose_name='Custom color Error'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='favicon',
            field=models.ImageField(blank=True, null=True, upload_to=apps.b3_organization.models.organization._theme_upload_to, validators=[apps.b3_misc.validators.FileValidator(allowed_mimetypes=('image/vnd.microsoft.icon', 'image/x-icon'))], verbose_name='Custom Favicon (.ico)'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='from_email',
            field=models.CharField(blank=True, default='', max_length=255, verbose_name='Email From Address'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='link_color',
            field=colorfield.fields.ColorField(blank=True, default='#2C66C4', max_length=10, verbose_name='Link color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='navbar_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#FFFFFF', max_length=10, verbose_name='Topmost navigation bar color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='navbar_txt_color',
            field=colorfield.fields.ColorField(blank=True, default='#555555', max_length=10, verbose_name='Topmost navigation bar text color  '),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='portal_name',
            field=models.CharField(default='3YOURMIND', max_length=255, verbose_name='Portal Name ProAccount - Not used anymore. Use Organization showname instead.'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='primary_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#2C66C4', max_length=10, verbose_name='Main color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='primary_elements_color',
            field=colorfield.fields.ColorField(blank=True, default='#2C66C4', help_text='Cube, Sliders, highlights, calendar, header-bar of uploaded 3D model, ...', max_length=10, verbose_name='Main color for Visual elements'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='primary_txt_color',
            field=colorfield.fields.ColorField(blank=True, default='#FFFFFF', max_length=10, verbose_name='Main text color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_compare_step3_text',
            field=models.TextField(blank=True, default='Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_compare_step3_text_de',
            field=models.TextField(blank=True, default='Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_compare_step3_text_en',
            field=models.TextField(blank=True, default='Ask our 3D printing experts by creating a manual request.Our in-house 3D Modelers can assist you editing the file.', null=True, verbose_name='Text on compare page step3, above the manual request button'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_confirmation_text',
            field=models.TextField(blank=True, default='Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_confirmation_text_de',
            field=models.TextField(blank=True, default='Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_confirmation_text_en',
            field=models.TextField(blank=True, default='Thank you for contacting our team.<br>We are reviewing your request and will provide feedback within one working day.', null=True, verbose_name='The confirmation email content sent to the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_subject_mail',
            field=models.CharField(blank=True, default='3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_subject_mail_de',
            field=models.CharField(blank=True, default='3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='request_user_subject_mail_en',
            field=models.CharField(blank=True, default='3YOURMIND Support', max_length=512, null=True, verbose_name='Subject of the request confirmation email for the user'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text',
            field=models.CharField(default='Sign up for your account and start managing your 3D printing projects online.', max_length=255, verbose_name='Text content Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text_button',
            field=models.CharField(default='Sign Up', max_length=24, verbose_name='Text button Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text_button_de',
            field=models.CharField(default='Sign Up', max_length=24, null=True, verbose_name='Text button Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text_button_en',
            field=models.CharField(default='Sign Up', max_length=24, null=True, verbose_name='Text button Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text_de',
            field=models.CharField(default='Sign up for your account and start managing your 3D printing projects online.', max_length=255, null=True, verbose_name='Text content Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='signup_text_en',
            field=models.CharField(default='Sign up for your account and start managing your 3D printing projects online.', max_length=255, null=True, verbose_name='Text content Sign Up Modal'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='success_color',
            field=colorfield.fields.ColorField(blank=True, default='#3c9d07', max_length=10, verbose_name='Custom color Success'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='supplier_additional_panel_text',
            field=models.TextField(blank=True, default='', null=True, verbose_name='Text on addtional panel on supplier page'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='supplier_additional_panel_text_de',
            field=models.TextField(blank=True, default='', null=True, verbose_name='Text on addtional panel on supplier page'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='supplier_additional_panel_text_en',
            field=models.TextField(blank=True, default='', null=True, verbose_name='Text on addtional panel on supplier page'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='viewer_bkg_color',
            field=colorfield.fields.ColorField(blank=True, default='#2c66c4', max_length=10, verbose_name='3D Viewer background Color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='viewer_bkg_repeat',
            field=models.CharField(choices=[('repeat', 'Repeat Background'), ('cover', 'Display as cover')], default='repeat', max_length=255, verbose_name='3D Viewer background repeat'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='viewer_txt_color',
            field=colorfield.fields.ColorField(blank=True, default='#ccc', max_length=10, verbose_name='3D Viewer text color'),
        ),
        migrations.AlterField(
            model_name='organizationtheme',
            name='warning_color',
            field=colorfield.fields.ColorField(blank=True, default='#ff7800', max_length=10, verbose_name='Custom color Warning'),
        ),
    ]
