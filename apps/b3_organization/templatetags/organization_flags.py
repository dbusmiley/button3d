from django.template import Library
from django import template
from django.template.exceptions import TemplateSyntaxError
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from django.utils.translation import get_language

from apps.b3_address.models import Address
from apps.b3_organization import utils
from apps.b3_organization.utils import get_current_org

register = Library()


@register.filter('hex_to_light_or_dark_border')
def hex_to_light_or_dark_border(request):
    if '#' not in request:
        return request
    hex_val = request.strip('#')
    rgb = list(int(hex_val[i:i + 2], 16) for i in (0, 2, 4))
    darkness = 1 - (0.299 * rgb[0] + 0.587 * rgb[1] + 0.114 * rgb[2]) / 255
    if darkness < 0.5:
        return (255, 255, 255, 0.24)
    else:
        return (0, 0, 0, 0.24)  # Is dark


@register.filter('is_single_supplier')
def is_single_supplier(request):
    return request.organization.is_single_supplier_account


@register.filter('show_upload_button')
def show_upload_button(request):
    org = request.organization if \
        request.organization else utils.get_current_org()
    if org.anonymous_upload_enabled:
        return True

    if request.user.is_authenticated:
        if not org.force_verify_customer_address:
            return True

        is_user_address_verified = Address.objects.filter(
            user=request.user,
            verified_status=Address.ADDRESS_VERIFIED).exists()

        if is_user_address_verified:
            return True
        if org.verify_customer_address_users.filter(
                pk=request.user.pk).exists():
            return True

    return False


@register.filter('show_register_button')
def show_register_button(request):
    return request.organization.register_enabled


@register.filter('show_login_button')
def show_login_button(request):
    return request.organization.login_enabled


@register.filter('show_file_validation')
def show_file_validation(request):
    return request.organization.file_validation_enabled


@register.filter('show_filters_panel')
def show_filters_panel(request):
    return request.organization.filters_enabled


@register.filter('link_sharing_enabled')
def link_sharing_enabled(request):
    return request.organization.link_sharing_enabled


@register.filter('attachment_upload_enabled')
def attachment_upload_enabled(request):
    return request.organization.manual_request_attachment_upload_enabled


class OrgThemeCustom(template.Node):
    def __init__(self, field, nodelist):
        self.field = field
        self.nodelist = nodelist

    def render(self, context):
        value = utils.getattr_theme(self.field)
        if value:
            if self.field in [
                'navbar_logo_image',
                'viewer_bkg_image',
                'favicon',
                    'email_logo_image']:
                return value.url
            return value
        return self.nodelist.render(context)


@register.tag
def customize(parser, token):
    bits = list(token.split_contents())
    nodelist = parser.parse(('endcustomize',))
    parser.delete_first_token()

    if len(bits) != 2:
        raise TemplateSyntaxError("Missing field")
    else:
        value = bits[1][1:-1]

    return OrgThemeCustom(value, nodelist)


@register.filter('show_organization')
def show_organization(request):
    site = utils.get_current_site(request)
    return site.organization.showname


@register.filter('show_organization_url')
def show_organization_url(request):
    site = utils.get_current_site(request)
    return site.domain


@register.assignment_tag()
def get_org_logo_tag_or_name():
    organization = get_current_org()
    logo = organization.theme.navbar_logo_image
    if logo:
        url = strip_tags(logo.url)
        logo_tag = f'<img src="{url}" style="width:180px;" ' \
                   f'alt="{organization} Logo">'
        return mark_safe(logo_tag)  # nosec
    name = strip_tags(get_current_org().showname)
    name_tag = f'<h1 class="blue-text" style="text-align:right;">{name}</h1>'
    return mark_safe(name_tag)  # nosec


@register.assignment_tag()
def get_terms_and_condition_message():
    organization = get_current_org()
    current_lang = get_language()
    if current_lang == 'de':
        return mark_safe(organization.terms_conditions_de  # nosec
                         ) if organization.terms_conditions_de else False

    return mark_safe(organization.terms_conditions_en  # nosec
                     ) if organization.terms_conditions_en else False
