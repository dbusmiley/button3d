import os
import shutil

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.files.storage import default_storage as storage
from django.core.management import BaseCommand, call_command
from django.db.models.signals import post_save

from apps.b3_organization.models.organization import \
    create_organization_theme

IMPORT_DIR = os.path.join(settings.BASE_DIR, "export")


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            "name",
            nargs=1,
            type=str,
            help="name of the site that should be loaded")
        parser.add_argument(
            "-p",
            "--path",
            nargs=1,
            type=str,
            help="specify the path of 3yd_site_export directory")
        parser.add_argument(
            "-d",
            "--site_domain",
            nargs=1,
            type=str,
            help="the domain of the new Site")

    def handle(self, *args, **kwargs):
        display_name = kwargs["name"][0]

        if kwargs["path"]:
            backup_path = kwargs["path"][0]
            backup_dir = os.path.join(backup_path, display_name)
        else:
            backup_dir = os.path.join(IMPORT_DIR, display_name)

        if not os.path.isdir(backup_dir):
            print((
                "backup-directory %s does not exist.\nImport aborted!" %
                backup_dir))
            return

        data_backup_file = os.path.join(backup_dir, "data.json")
        if os.path.isfile(data_backup_file):
            print(("loading data from %s ..." % data_backup_file))
            # Prevent creation of default OrganizationTheme
            post_save.disconnect(
                create_organization_theme,
                sender=Site,
                dispatch_uid="create_organization_theme")
            call_command("loaddata", data_backup_file)
            post_save.connect(
                create_organization_theme,
                sender=Site,
                dispatch_uid="create_organization_theme")
            print("done.\n")

        if kwargs["site_domain"]:
            site_domain = kwargs["site_domain"][0]

            try:  # If we find the Site, set its domain accordingly
                loaded_site = Site.objects.get(name=display_name)
                loaded_site.domain = site_domain
                loaded_site.save()
            except Site.DoesNotExist:
                print((
                    "Site %s Not found. Could not set the Domain to %s" %
                    (display_name, site_domain)))
        else:
            print(
                ("No domain Specified, Remember to set it "
                 "correctly yourself in the Admin panel"))

        media_backup_dir = os.path.join(backup_dir, "mediafiles")
        if os.path.isdir(media_backup_dir):
            print(("loading mediafiles from %s..." % media_backup_dir))

            for subdir, dirs, files in os.walk(media_backup_dir):
                for fl in files:
                    name = os.path.join(
                        os.path.relpath(
                            subdir, media_backup_dir), fl)
                    print(name)
                    if storage.exists(name):
                        storage.delete(name)
                    with open(os.path.join(media_backup_dir, name), 'rb') as f:
                        storage.save(name, f)
            print("done.")

        template_backup_dir = os.path.join(backup_dir, "org_templates")
        template_target_dir = os.path.join(
            settings.BASE_DIR,
            "org_templates/%s/" %
            display_name)
        print(("loading template files from %s ..." % template_backup_dir))
        try:
            shutil.copytree(template_backup_dir, template_target_dir)
            print("done.")
        except OSError:
            print("no template files found.")
