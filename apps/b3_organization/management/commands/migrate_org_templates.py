import os

from django.contrib.sites.models import Site
from django.core.management import BaseCommand
from django.utils.text import slugify

from apps.b3_organization.models import OrganizationCustomTemplate


class Command(BaseCommand):
    custom_templates_folder = 'org_templates'

    def handle(self, *args, **kwargs):
        custom_templates = self.build_custom_templates_dict()
        all_sites = Site.objects.all()
        for template_pair, full_path in custom_templates.items():
            site_slug, template_name = template_pair

            sites = [s for s in all_sites if slugify(s.name) == site_slug]
            if len(sites) == 0:
                print(
                    ('Could not find site instance for {0}'.format(site_slug))
                )
                continue
            if len(sites) > 1:
                print((
                    ('Found more than 1 site instance for {0}. '
                     'Ignoring it').format(site_slug)))
                continue
            site = sites[0]
            try:
                custom_template = OrganizationCustomTemplate.objects.get(
                    site=site, template_name=template_name)
                text = 'updated'
            except OrganizationCustomTemplate.DoesNotExist:
                custom_template = OrganizationCustomTemplate.objects.create(
                    site=site, template_name=template_name)
                text = 'created'
            with open(full_path, 'r') as f:
                custom_template.template = f.read()
                custom_template.save()
                print(('{0} {1}!'.format(custom_template, text)))

    def build_custom_templates_dict(self):
        ''' Builds a dict such as:
        - key is a pair
            (site_slug, template_name) => ('gkn', 'b3_core/nav.html')
        - value is the full path of the
            file => 'org_templates/gkn/b3_core/nav.html'
        '''

        if not os.path.isdir(self.custom_templates_folder):
            raise ValueError(
                '{0} is not a directory'.format(
                    self.custom_templates_folder))

        custom_templates = {}
        for root, subdirs, files in os.walk(self.custom_templates_folder):
            for f in files:
                full_template_name = '{0}/{1}'.format(root, f)
                path_from_site = full_template_name.lstrip(
                    self.custom_templates_folder).lstrip('/')
                try:
                    site_slug, template_name = path_from_site.split('/', 1)
                    template_pair = (site_slug, template_name)
                    custom_templates[template_pair] = full_template_name
                except ValueError:
                    pass  # files at root of org_templates dir
        return custom_templates
