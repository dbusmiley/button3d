import os
import errno
import shutil

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.files.storage import default_storage as storage
from django.core import serializers
from django.core.management import BaseCommand
from django.db.models import FileField
from django.db import transaction

from apps.b3_address.models import Country, Address
from apps.b3_checkout.models import PartnerPaymentMethod
from apps.b3_organization.utils import set_current_site
from apps.catalogue.models.product import Product
from apps.catalogue.models.technology import Technology
from apps.catalogue.models.color import Color

EXPORT_DIR = os.path.join(settings.BASE_DIR, 'export')


class Command(BaseCommand):
    """
     Exports Data for a given Site

     Usage:
       python manage.py dump_site <site-name>

     Exported data is stored in export/<site-name>/

     The export-directory can be copied to the destination and imported using
       python manage.py load_site <site-name>


     The following objects are exported:
       The Site-object
           The OrganizationTheme-object belonging to the Site
               All Link-objects belonging to the OrganizationTheme
           The Organization-object belonging to the Site
               The KeyManager-object belonging to the Organization
               All Partner-objects in site.organization.partners_enabled
                that belong to the Site
                   The PartnerAddress-object of each Partner
                   The StripAccount-object of each Partner (if present)
                   All ShippingMethod-objects of each Partner
                   All StockRecord-objects of each Partner

       site.organization.partners_enabled is
        reduced to only those Partners that belong to the Site
       All MediaFiles that are referenced by
        a FileField in one of the exported objects are exported too

       For serialization, natural keys are used to avoid pk-collision.
       Therefore, the following uniqeness is
        assumed (additional to uniqueness implied by OneToOneFields):
           Link: (title, url)
           KeyManager: (name, email)
           OrganizationShippingAddress: (hash,)
           Partner: (code,)
           PartnerAddress: (first_name, last_name, line1, line2, line3, line4)
           StripeAccount: (stripe_user_id,)
           ShippingMethod: (code,)
           StockRecord: (partner_sku,)

     The following files/directories are exported:
       org_templates/<site-name>/
    """

    def add_arguments(self, parser):
        parser.add_argument(
            'name',
            type=str,
            help='name of the site that should be exported')
        parser.add_argument(
            '-p',
            '--path',
            type=str,
            help='specify the path of 3yd_site_export directory')

    def handle(self, name, path, *args, **kwargs):
        transaction.set_autocommit(False)

        display_name = name

        try:
            site = Site.objects.get(name=display_name)
            set_current_site(site)
        except Site.DoesNotExist:
            print(f'Site {display_name} not found.\nExport aborted!')
            return

        self.init_dump_directory(display_name, path)

        objects = self.get_objects_to_dump(site)

        self.save_objects(objects)

        self.save_media_files(objects)

        self.save_templates(display_name)

        object_count = len(objects)
        print(f'Export of {object_count} objects successfully finished!')

        # clean all that we changed in the DB
        transaction.rollback()

    def get_objects_to_dump(self, site: Site) -> list:

        site.organization.printing_service_onboarding_user = None
        site.organization.organization_panel_admins = []
        site.organization.save()
        objects = [
            site,
            site.organizationtheme,
            site.organization.key_manager,
            site.organization,
        ]

        objects.extend(Country.objects.iterator())

        objects.extend(Technology.objects.iterator())
        objects.extend(Product.objects.iterator())
        objects.extend(Color.objects.iterator())

        objects.extend(site.organizationcustomtemplate_set.all())
        objects.extend(site.organizationtheme.link_set.all())
        objects.extend(site.organizationbillingaddress_set.all())
        objects.extend(site.organizationshippingaddress_set.all())

        for partner in site.organization.partners_enabled.all():
            # we are not migrating User objects, so no need to keep links to
            # them in partner
            partner.users = []
            if partner.site != site:
                continue

            objects.append(partner)
            for stock_record in partner.stockrecords.all():
                objects.append(stock_record)
                objects.extend(stock_record.post_processings.all())

            if partner.stripe_connect_id:
                objects.append(partner.stripe_connect_id)

            objects.append(partner.address)
            addresses = Address.objects.filter(partner=partner)
            objects.extend(addresses)

            shipping_methods = partner.shipping_methods.all()
            objects.extend(shipping_methods)

            payment_methods = \
                PartnerPaymentMethod.objects.filter(partner=partner)
            objects.extend(payment_methods)

        return objects

    def init_dump_directory(self, display_name, path):
        if path:
            backup_path = path
            if not os.path.isdir(backup_path):
                print(f'invalid backup path: {backup_path}.\nExport aborted!')
                raise Exception()
            self.backup_dir = os.path.join(backup_path, display_name)
        else:
            if not os.path.isdir(EXPORT_DIR):
                os.mkdir(EXPORT_DIR)
            self.backup_dir = os.path.join(EXPORT_DIR, display_name)
        self.media_backup_dir = os.path.join(self.backup_dir, 'mediafiles')
        if os.path.isdir(self.backup_dir):
            shutil.rmtree(self.backup_dir)
        os.mkdir(self.backup_dir)

    def save_objects(self, objects):
        data_backup_file = os.path.join(self.backup_dir, 'data.json')
        print(f'dumping data to {data_backup_file} ...')

        serialized_objects = serializers.serialize(
            'json',
            objects,
            indent=2,
        )
        with open(data_backup_file, 'w') as f:
            f.write(serialized_objects)
        print('done.')

    def save_templates(self, display_name):
        template_backup_dir = os.path.join(self.backup_dir, 'org_templates')
        print(f'dumping template files to {template_backup_dir} ...')
        try:
            source_path = os.path.join(
                settings.BASE_DIR,
                f'org_templates/{display_name}/',
            )
            shutil.copytree(source_path, template_backup_dir)
            print('done.')
        except OSError:
            print('no template files found.')

    def save_media_files(self, objects):
        print(f'dumping media files to {self.media_backup_dir} ...')
        for obj in objects:
            for field in obj._meta.get_fields():
                if isinstance(field, FileField):
                    try:
                        self.save_media_file(getattr(obj, field.name).name)
                    except AttributeError:
                        self.save_media_file(
                            getattr(obj, f'{field.name}_de').name)
                        self.save_media_file(
                            getattr(obj, f'{field.name}_en').name)
        print('done.')

    def save_media_file(self, name):
        if name and storage.exists(name):
            file_name = os.path.join(self.media_backup_dir, name)
            print(f'saving media file {name} to {file_name}')
            if not os.path.exists(file_name):
                try:
                    os.makedirs(os.path.dirname(file_name))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise
            with open(file_name, 'wb') as f:
                f.write(storage.open(name).read())
