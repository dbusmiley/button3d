from django.shortcuts import render

from apps.b3_core.utils import determine_default_currency
from apps.b3_user_panel.permissions import check_user_permission
from apps.basket.models import Basket
from apps.b3_order.models import Order


@check_user_permission
def dashboard(request):
    orders = Order.objects.filter(
        purchased_by=request.user).order_by('-datetime_placed')[:3]
    template = 'b3_organization/dashboard.html'
    context = {
        'current_currency': determine_default_currency(request),
        'orders': orders
    }

    baskets, _ = Basket.search(owner=request.user, items_per_page=3)
    context['projects'] = [{
        'id': b.id,
        'title': b.title,
        'date': b.date_created,
        'lines': b.lines.all()[:2],  # Display two thumbnails
        'total_lines': b.num_lines
    } for b in baskets]

    return render(request, template, context)
