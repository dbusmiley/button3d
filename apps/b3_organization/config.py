from django.apps import AppConfig


class OrganizationConfig(AppConfig):
    name = 'apps.b3_organization'

    def ready(self):
        from actstream import registry
        from django.contrib.auth.models import User
        registry.register(User)
