from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

from apps.b3_organization.utils import get_current_site


class DomainEmailBackend(ModelBackend):
    """
    Custom auth backend that uses an email address and password
    This is copied from
    /oscar/apps/customer/auth_backends.py with the extension that we also
    check for the users domain
    """

    def authenticate(self, request, email=None, password=None,
                     *args, **kwargs):
        if email is None:
            if 'username' not in kwargs or kwargs['username'] is None:
                return None
            clean_email = self.normalise_email(kwargs['username'])
        else:
            clean_email = self.normalise_email(email)

            # Check if we're dealing with an email address
        if '@' not in clean_email or not isinstance(clean_email, str):
            return None

        if clean_email:
            current_site = get_current_site()
            return next((user
                         for user in User.objects.filter(email=clean_email)
                         if user.userprofile.site == current_site and
                         user.check_password(password)),
                        None)
        return None

    # Overwrite as suggested in
    # http://stackoverflow.com/questions/1404131/how-to-get-unique-users-
    # across-multiple-django-sites-powered-by-the-sites-fram?lq=1
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

    @staticmethod
    def normalise_email(email):
        """
        The local part of an email address is case-sensitive, the domain part
        isn't.
        This function lowercases the host and should be used in all email
        handling.
        """
        clean_email = email.strip()
        if '@' in clean_email:
            local, host = clean_email.split('@')
            return local + '@' + host.lower()
        return clean_email
