from django.utils.translation import ugettext
from guardian.shortcuts import get_users_with_perms, get_user_perms

from apps.basket.models import Basket


class ContactInformationFactory:

    PERMISSION_DICT = {
        Basket.PRICING_PERMISSION: {
            'ownership_text': ugettext('Pricer'),
            'ownership_key': 'pricer',
            'rank': 2,
        },
        Basket.VIEW_PERMISSION: {
            'ownership_text': ugettext('Viewer'),
            'ownership_key': 'viewer',
            'rank': 1,
        },
        'owner': {
            'ownership_text': ugettext('Owner'),
            'ownership_key': 'owner',
        }
    }

    @classmethod
    def get_contact_information_for_user(cls, user):
        if user and user.userprofile:
            contact_data = {
                'email': user.email
            }
            current_phone_index = 0
            for phone_number in user.userprofile.get_phone_numbers():
                if current_phone_index == 0:
                    contact_data['phone'] = phone_number
                else:
                    contact_data['phone' + current_phone_index] = phone_number
                ++current_phone_index
            owner_data = {
                'user': user.userprofile.full_name,
                'contact': contact_data
            }
            return owner_data
        else:
            return {'user': ugettext('Anonymous user')}

    @classmethod
    def get_contact_information_for_basket(cls, basket):
        user_contact_information = []

        # Other users
        all_users_with_permissions = get_users_with_perms(basket)
        for user in all_users_with_permissions:
            user_perms = get_user_perms(user, basket)
            current_rank = 0
            contact_information = ContactInformationFactory.\
                get_contact_information_for_user(user)
            for user_perm in user_perms:
                permission_info = cls.PERMISSION_DICT.get(user_perm)
                if permission_info and current_rank < permission_info['rank']:
                    current_rank = permission_info['rank']
                    contact_information.update(permission_info)
            user_contact_information.append(contact_information)

        # Add basket owner
        contact_information = ContactInformationFactory.\
            get_contact_information_for_user(basket.owner)
        contact_information.update(cls.PERMISSION_DICT['owner'])

        user_contact_information.append(contact_information)
        return user_contact_information
