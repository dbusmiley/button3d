from django.template import Origin, TemplateDoesNotExist
from django.template.loaders.base import Loader

from apps.b3_organization.models import OrganizationCustomTemplate
from ..utils import get_current_site


class SiteLoader(Loader):
    def get_template_sources(self, template_name):
        yield Origin(
            name=template_name,
            template_name=template_name,
            loader=self,
        )

    def get_contents(self, origin):
        site = get_current_site()
        try:
            return OrganizationCustomTemplate.objects.get(
                site=site, template_name=origin.template_name).template
        except BaseException:
            raise TemplateDoesNotExist(origin)
