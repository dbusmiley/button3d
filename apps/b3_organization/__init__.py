# flake8: noqa
from .signals import *

default_app_config = 'apps.b3_organization.config.OrganizationConfig'
