from apps.b3_tests.testcases.common_testcases import TestCase


class SiteTest(TestCase):
    def setUp(self):
        super(SiteTest, self).setUp()
        self.organization.allowed_iframe_domains = \
            '*.test.test     ' \
            'blub.de\n\n' \
            'another.example'
        self.organization.save()
        self.site.refresh_from_db()

    def test_iframe_header(self):
        response = self.client.get('/')
        self.assertEqual(
            response['Content-Security-Policy'],
            "frame-ancestors *.test.test blub.de another.example 'self';"
        )

    def test_iframe_header_404(self):
        response = self.client.get('/404/')
        self.assertEqual(
            response['Content-Security-Policy'],
            "frame-ancestors *.test.test blub.de another.example 'self';"
        )
