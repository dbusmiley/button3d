from apps.b3_organization.utils import get_base_url, set_current_site
from apps.b3_tests.factories import SiteFactory, OrganizationFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class UtilsTest(TestCase):
    def setUp(self):
        super(UtilsTest, self).setUp()
        self.populate_current_request()

    def test_get_base_url_default_app(self):
        base_url = ':'.join(get_base_url().split(':')[0:2])
        self.assertEqual(base_url, 'http://my.3yd')

    def test_get_base_url_default_org(self):
        orgsite = SiteFactory()
        OrganizationFactory(site=orgsite)
        set_current_site(orgsite)
        base_url = ':'.join(get_base_url().split(':')[0:2])
        self.assertEqual(base_url, 'http://org.my.3yd')

    def test_get_base_url_force_app(self):
        orgsite = SiteFactory()
        OrganizationFactory(site=orgsite)
        set_current_site(orgsite)
        base_url = get_base_url(org=self.organization)
        base_url = ':'.join(base_url.split(':')[0:2])
        self.assertEqual(base_url, 'http://my.3yd')

    def test_get_base_url_force_org(self):
        orgsite = SiteFactory()
        org = OrganizationFactory(site=orgsite)
        base_url = ':'.join(get_base_url(org=org).split(':')[0:2])
        self.assertEqual(base_url, 'http://org.my.3yd')
