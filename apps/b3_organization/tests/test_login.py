from django.test import RequestFactory

from apps.b3_organization.auth_backend import DomainEmailBackend
from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import SiteFactory, RequestMock, \
    OrganizationFactory, UserAnyEmailFactory, SignupFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class MultipleEmailTest(TestCase):
    def setUp(self):
        super(MultipleEmailTest, self).setUp()
        self.site_main = self.site
        self.site_subdomain = SiteFactory()
        self.organization_site_subdomain = OrganizationFactory(
            site=self.site_subdomain)
        self.site_external_domain = SiteFactory(
            name="external.domain", domain="external.domain")
        self.organization_external_domain = OrganizationFactory(
            site=self.site_external_domain)
        self.factory = RequestMock()

    def test_login(self):
        set_current_site(self.site_main)
        user_main = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user_main.email)

        set_current_site(self.site_external_domain)
        user_external = UserAnyEmailFactory.create(
            username='tester2', is_staff=False)
        SignupFactory.create(email=user_external.email)

        backend = DomainEmailBackend()

        set_current_site(self.site_main)
        request = RequestFactory()
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertEqual(res_user, user_main)

        set_current_site(self.site_subdomain)
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertIsNone(res_user)

        set_current_site(self.site_external_domain)
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertEqual(res_user, user_external)

    def test_login2(self):
        set_current_site(self.site_subdomain)
        user_main = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user_main.email)

        set_current_site(self.site_external_domain)
        user_external = UserAnyEmailFactory.create(
            username='tester2', is_staff=False)
        SignupFactory.create(email=user_external.email)

        backend = DomainEmailBackend()

        set_current_site(self.site_main)
        request = RequestFactory()
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertIsNone(res_user)

        set_current_site(self.site_subdomain)
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertEqual(res_user, user_main)

        set_current_site(self.site_external_domain)
        res_user = backend.authenticate(request=request,  # nosec
                                        email=user_main.email,
                                        password='testpassword')
        self.assertEqual(res_user, user_external)

    def test_login_fail(self):
        set_current_site(self.site_external_domain)
        user_external = UserAnyEmailFactory.create(
            username='tester2', is_staff=False)
        SignupFactory.create(email=user_external.email)

        backend = DomainEmailBackend()

        set_current_site(self.site_main)
        request = RequestFactory()
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_external.email,
            password='testpassword')
        self.assertIsNone(res_user)

        set_current_site(self.site_subdomain)
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_external.email,
            password='testpassword')
        self.assertIsNone(res_user)

        set_current_site(self.site_external_domain)
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_external.email,
            password='testpassword')
        self.assertEqual(res_user, user_external)

    def test_login_fail2(self):
        set_current_site(self.site_subdomain)
        user_subdomain = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user_subdomain.email)

        backend = DomainEmailBackend()

        set_current_site(self.site_main)
        request = RequestFactory()
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_subdomain.email,
            password='testpassword')
        self.assertIsNone(res_user)

        set_current_site(self.site_subdomain)
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_subdomain.email,
            password='testpassword')
        self.assertEqual(res_user, user_subdomain)

        set_current_site(self.site_external_domain)
        res_user = backend.authenticate(  # nosec
            request=request,
            email=user_subdomain.email,
            password='testpassword')
        self.assertIsNone(res_user)
