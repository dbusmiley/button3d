import os

from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse

from apps.b3_tests.factories import SiteFactory, \
    RequestMock, OrganizationFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from ..utils import set_current_site, get_current_site, get_base_url


class SiteTest(TestCase):
    def setUp(self):
        super(SiteTest, self).setUp()
        self.orgsite = SiteFactory()
        self.org = OrganizationFactory(site=self.orgsite)
        self.factory = RequestMock()

    def test_sites_monkey_patch(self):
        # Site.objects.get_current should return always
        #    None, not site matching with settings.SITE_ID.
        # This is required otherwise oscar will create new orders using
        # settings.SITE_ID instead of current site.

        # Custom Site/Org
        set_current_site(self.orgsite)
        site = Site.objects.get_current()
        self.assertIsNotNone(site)
        self.assertEqual(site, self.orgsite)

    def test_get_set_current_site(self):
        # Custom Site/Org
        set_current_site(self.orgsite)
        site = get_current_site()
        self.assertIsNotNone(site)
        self.assertEqual(site, self.orgsite)

    def test_request_site(self):
        # Custom Site/Org
        request = self.factory.get(
            reverse('demo'),
            SERVER_NAME=self.orgsite.domain,
            HTTP_HOST=self.orgsite.domain)
        site = get_current_site(request)
        self.assertEqual(request.get_host(), self.orgsite.domain)
        self.assertEqual(site, self.orgsite)

    def test_request_main_site(self):
        # Main site
        request = self.factory.get(
            reverse('demo'),
            SERVER_NAME=self.site.domain)
        site = get_current_site(request)
        self.assertEqual(request.get_host(), self.site.domain)
        self.assertEqual(site, self.site)
        self.assertNotEqual(site, self.orgsite)

    def test_invalid_site(self):
        r = self.client.get(reverse('demo'), SERVER_NAME='invalid.my.3yd',
                            HTTP_HOST='invalid.my.3yd')
        self.assertEqual(r.status_code, 400)

    def test_site_from_env(self):
        os.environ['SITE_DOMAIN'] = 'org.my.3yd'
        self.assertEqual('org.my.3yd', self.orgsite.domain)
        del os.environ['SITE_DOMAIN']

    def test_site_without_organization(self):
        test_site: Site = SiteFactory(
            domain='test.my.3yd', name='test.my.3yd',
        )
        with self.assertRaises(ObjectDoesNotExist):
            org = test_site.organization  # flake8: noqa

        r = self.client.get(reverse('demo'), SERVER_NAME=test_site.domain,
                            HTTP_HOST=test_site.domain)
        self.assertEqual(r.status_code, 400)


class GetBaseUrlTest(TestCase):
    def setUp(self):
        super(GetBaseUrlTest, self).setUp()
        self.populate_current_request()

    def test_get_base_url_implicit(self):
        # *** Implicit org _tests
        # On app
        set_current_site(self.site)
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://my.3yd'
        )

        # On pro account
        site = SiteFactory(domain='site.my.3yd')
        OrganizationFactory(site=site)
        set_current_site(site)
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://site.my.3yd'
        )

        # On pro account with domain
        site.domain = 'site.mindyour3.com'
        site.save()
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://site.mindyour3.com'
        )

    def test_get_base_url_explicit(self):
        # *** Explicit org _tests
        # Force app on pro account
        site = SiteFactory(domain='site.my.3yd')
        org = OrganizationFactory(site=site)
        set_current_site(site)
        self.assertEqual(
            ':'.join(get_base_url(org=self.organization).split(':')[0:2]),
            'http://my.3yd'
        )

        # Force pro account on app
        set_current_site(self.site)
        self.assertEqual(
            ':'.join(get_base_url(org=org).split(':')[0:2]),
            'http://site.my.3yd'
        )

        # Force pro account with external on app
        site.domain = 'site.mindyour3.com'
        site.save()
        self.assertEqual(
            ':'.join(get_base_url(org=org).split(':')[0:2]),
            'http://site.mindyour3.com'
        )

        # Force app on app
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://my.3yd'
        )
        self.assertEqual(
            ':'.join(get_base_url(org=self.organization).split(':')[0:2]),
            'http://my.3yd'
        )

        # Force another pro account on pro account
        site2 = SiteFactory(domain='second.my.3yd')
        org2 = OrganizationFactory(site=site2)
        set_current_site(site)
        self.assertEqual(
            ':'.join(get_base_url(org=org2).split(':')[0:2]),
            'http://second.my.3yd'
        )
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://site.mindyour3.com'
        )

    def test_get_base_url_http(self):
        # On app
        set_current_site(self.site)
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://my.3yd'
        )

        # On site
        site = SiteFactory(domain='site.my.3yd')
        OrganizationFactory(site=site)
        set_current_site(site)
        self.assertEqual(
            ':'.join(get_base_url().split(':')[0:2]),
            'http://site.my.3yd'
        )
