from django.urls import reverse

from apps.b3_tests.factories import KeyManagerFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.testcases.org_testcases import \
    AuthenticatedOrganizationTestCase
from apps.basket.models import Basket


class DashboardTest(AuthenticatedTestCase):
    def test_open_empty_basket_retrieval(self):
        BasketFactory(status=Basket.OPEN, owner=self.user)
        response = self.client.get(reverse('b3_organization:dashboard'))
        context = response.context[0]
        baskets = context.get('projects')
        self.assertEqual(len(baskets), 0)

    def test_no_basket_retrieval(self):
        response = self.client.get(reverse('b3_organization:dashboard'))
        context = response.context[0]
        baskets = context.get('projects')
        self.assertEqual(len(baskets), 0)

    def test_basket_retrieval(self):
        BasketFactory(status=Basket.EDITABLE, owner=self.user)
        response = self.client.get(reverse('b3_organization:dashboard'))
        context = response.context[0]
        baskets = context.get('projects')
        self.assertEqual(len(baskets), 1)

    def test_basket_max_retrieval(self):
        BasketFactory(status=Basket.EDITABLE, owner=self.user)
        BasketFactory(status=Basket.EDITABLE, owner=self.user)
        BasketFactory(status=Basket.EDITABLE, owner=self.user)
        BasketFactory(status=Basket.EDITABLE, owner=self.user)

        response = self.client.get(reverse('b3_organization:dashboard'))
        context = response.context[0]
        baskets = context.get('projects')
        self.assertEqual(len(baskets), 3)

    def test_access_dashboard_anonymously(self):
        self.client.logout()

        response = self.client.get(
            reverse('b3_organization:dashboard'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('/accounts/login/' in response.request['PATH_INFO'])


class DashboardOrganizationTest(AuthenticatedOrganizationTestCase):
    def test_key_manager_without_picture(self):
        self.organization.key_manager = KeyManagerFactory(manager_photo=None)
        self.organization.save()

        response = self.client.get(
            reverse('b3_organization:dashboard'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(
            '- Dashboard</title>' in response.content.decode('utf-8')
        )
