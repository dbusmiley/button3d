from django.test import TestCase, RequestFactory

from apps.b3_core.models import StlFile
from apps.b3_organization.auth_backend import DomainEmailBackend
from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import UserFactory, \
    StlFileFactory, SiteFactory


class DomainAndOwnershipTest(TestCase):
    """Test for the checking domains and ownerships of uploaded stl files.
    """

    def test_login(self):
        """Function to test the login with the newly created user.
        This will create the user and check if the
        authentication is working properly.
        """
        site = SiteFactory()
        set_current_site(site)
        user = UserFactory.create(
            is_staff=False,
            is_active=True,
        )
        # user profile is created automatically, therefore I specified only
        # site
        user.userprofile.site = site
        user.userprofile.save()

        domain_backend = DomainEmailBackend()
        set_current_site(site)
        request = RequestFactory()
        user_logged_in = domain_backend.authenticate(  # nosec
            request=request,
            email=user.email, password="testpassword")
        user_not_logged_in = domain_backend.authenticate(  # nosec
            request=request,
            email=user.email, password="THIS1WILL1NOT1WORK")
        self.assertIsNotNone(user_logged_in)
        self.assertIsNone(user_not_logged_in)

    def test_login_fail(self):
        site = SiteFactory()
        set_current_site(site)
        user = UserFactory.create(
            is_staff=False,
            is_active=True,
        )
        # user profile is created automatically, therefore I specified only
        # site
        user.userprofile.site = site
        user.userprofile.save()

        domain_backend = DomainEmailBackend()
        set_current_site(None)
        request = RequestFactory()
        user_logged_in = domain_backend.authenticate(  # nosec
            request=request,
            email=user.email, password="testpassword")
        user_not_logged_in = domain_backend.authenticate(  # nosec
            request=request,
            email=user.email, password="THIS1WILL1NOT1WORK")
        self.assertIsNone(user_logged_in)
        self.assertIsNone(user_not_logged_in)

    def test_ownership_ok(self):
        """Function to test if an uploaded .stl file,
            which is created by an user, is associated with the owner himself
        and the site he is relying to.
        """
        site = SiteFactory()

        set_current_site(site)
        user = UserFactory.create(
            is_staff=False,
            is_active=True,
        )
        user.userprofile.site = site
        user.userprofile.save()

        domain_backend = DomainEmailBackend()
        request = RequestFactory()
        user_logged_in = domain_backend.authenticate(  # nosec
            request=request,
            email=user.email, password="testpassword")

        stl_file = StlFileFactory.create(
            uuid='13425b02-794b-4ce6-bc27-000000000001',
            owner=user_logged_in,
            site=site)
        stl_file_database = StlFile.all_objects.get(id=stl_file.id)

        self.assertEqual(stl_file_database.owner_id, user_logged_in.id)
        self.assertEqual(
            stl_file_database.site_id,
            user_logged_in.userprofile.site.id)
