from django.urls import reverse

from apps.b3_tests.factories import OrganizationCustomTemplateFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.testcases.org_testcases import OrganizationTestCase


class OrgLoaderTest(OrganizationTestCase):
    def test_load_custom_template(self):
        # saves a new cutom template to the test DB
        OrganizationCustomTemplateFactory(
            site=self.site,
            template_name='b3_core/nav.html',
            template='This is a test template'
        )
        response = self.client.get(reverse('home'))
        self.assertTrue(
            'This is a test template' in response.content.decode('utf-8')
        )


class AppLoaderTest(TestCase):
    def test_app_does_not_load_custom_template(self):
        OrganizationCustomTemplateFactory(
            template_name='b3_core/nav.html',
            template='This is a test template')
        response = self.client.get(reverse('home'))
        self.assertFalse(
            'This is a test template' in response.content.decode('utf-8')
        )
