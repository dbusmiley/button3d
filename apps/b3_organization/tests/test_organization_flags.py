from django.urls import reverse
from django.test import RequestFactory

from apps.b3_organization import utils
from apps.b3_tests.factories import StlFileFactory
import apps.b3_organization.templatetags.organization_flags as org_flags
from apps.b3_tests.testcases.org_testcases import OrganizationTestCase

from django.db.models.fields.files import ImageFieldFile, FileField
from django.template import Context, Template
from apps.b3_organization.utils import get_current_org


class OrganizationFlagTest(OrganizationTestCase):
    def test_attachment_upload_enabled(self):
        request = RequestFactory().get('/en/demo/')
        request.organization = self.organization

        # Current site is None
        utils.set_current_site(None)
        self.assertTrue(org_flags.attachment_upload_enabled(request))

        # Current site is set and attachment upload is enabled
        utils.set_current_site(self.site)
        self.assertTrue(org_flags.attachment_upload_enabled(request))

        # Current site is set and attachment upload is disabled
        self.organization.manual_request_attachment_upload_enabled = False
        self.organization.save()
        self.assertFalse(org_flags.attachment_upload_enabled(request))

    def test_single_upload_redirect_to_viewer(self):
        self.organization.single_upload_redirects_to_project_detail = False
        self.organization.save()

        s = StlFileFactory(showname='Blabla')
        response = self.client.get(
            reverse('upload', args=(s.uuid,)),
            follow=True
        )

        self.assertTrue(
            '<!-- Viewer Page -->' in response.content.decode('utf-8')
        )

    def test_single_upload_redirect_to_project_detail(self):
        self.organization.single_upload_redirects_to_project_detail = True
        self.organization.save()

        s = StlFileFactory(showname='Blabla')
        response = self.client.get(
            reverse('upload', args=(s.uuid,)),
            follow=True
        )

        self.assertTrue(
            '<!-- Project Detail Page -->' in response.content.decode('utf-8')
        )

    def test_get_organization_name(self):
        template = (
            '{% load organization_flags %}{% get_org_logo_tag_or_name %}'
        )
        output = (
            '<h1 class="blue-text" style="text-align:right;">3YOURMIND</h1>'
        )
        t = Template(template)
        self.assertEqual(t.render(Context({})), output)

    def test_get_organization_logo(self):
        organization = get_current_org()
        organization.theme.navbar_logo_image = ImageFieldFile(
            instance=None,
            field=FileField(),
            name='test.png'
        )
        organization.save()
        template = (
            '{% load organization_flags %}{% get_org_logo_tag_or_name %}'
        )
        template_instance = Template(template)
        rendered_template = template_instance.render(Context({}))
        self.assertTrue('src="/media/test.png"' in rendered_template)
        self.assertTrue('alt="3YOURMIND Logo"' in rendered_template)
