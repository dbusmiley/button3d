from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, SiteFactory, \
    UserFactory, KeyManagerFactory

from apps.b3_organization.utils import get_current_org
from apps.b3_organization.forms import OrganizationForm


class OrganizationAdminFormTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationAdminFormTest, self).setUp()
        self.org = get_current_org()
        self.user.is_staff = True

    def test_admin_form_submission_with_no_org_admins(self):
        test_site = SiteFactory()
        test_partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        test_partner.site = test_site
        test_partner.save()
        test_user = UserFactory()
        test_user.userprofile.site = test_site
        test_user.userprofile.save()
        test_user.save()
        test_key_manager = KeyManagerFactory()
        test_key_manager.save()
        post_data = {
            'printing_service_onboarding_user': None,
            'site': test_site.id,
            'anonymous_upload_enabled': True,
            'login_enabled': True,
            'is_http_auth_enabled': False,
            'showname': 'single',
            'file_validation_enabled': True,
            'manual_pricing_user': None,
            'http_auth_user': '',
            'partners_enabled': [test_partner.id],
            'is_company_information_in_payment_step_enabled': False,
            'is_yoda_active': True,
            'http_auth_password': '',
            'manual_request_attachment_upload_enabled': False,
            'automatic_redirect_to_oauth': False,
            'file_validation_message_en': '',
            'description': 'single supplier account configuration',
            'auth_plugins_enabled': None,
            'private_api_key': '79292f96-5c07-11e7-907b-a6006ad3dba0',
            'single_upload_redirects_to_project_detail': False,
            'filters_enabled': False,
            'force_verify_customer_address': True,
            'slug': 'single',
            'is_extended_registration_enabled': True,
            'disable_normal_login': False,
            'verify_customer_address_users': [test_user.id],
            'file_validation_message_de': '',
            'register_enabled': True,
            'organization_panel_admins': [],
            'link_sharing_enabled': False,
            'is_single_supplier_account': True,
            'key_manager': test_key_manager.id,
            'terms_conditions': 'Test Terms',
            'encryption_algorithm': 'AES',
            'hash_algorithm': 'SHA256',
            'customer_number_visibility': 'Optional',
            'company_information_visibility': 'Optional',
            'vat_id_label': 'VAT Number',
            'vat_id_label_en': 'VAT Number',
            'vat_id_label_de': 'VAT Number',
            'customer_number_label': 'Customer Number',
            'customer_number_label_de': 'Customer Number',
            'customer_number_label_en': 'Customer Number',

        }
        org_form = OrganizationForm(data=post_data)
        # This is easier to fix then form.is_valid(). Keeping the form test,
        #  in case we really do some extra work in there.
        self.assertDictEqual({}, org_form.errors)
        self.assertTrue(org_form.is_valid())
