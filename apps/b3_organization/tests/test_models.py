# -*- coding: utf-8 -*-

from apps.b3_organization.factories.ContactInformationFactory import \
    ContactInformationFactory
from apps.b3_tests.factories import UserFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class UserProfileTest(TestCase):
    def test_new_user_has_org(self):
        user = UserFactory()
        userprofile = user.userprofile

        self.assertIsNotNone(userprofile)
        self.assertTrue(userprofile.has_organization)
        self.organization = userprofile.organization

    def test_str_no_name(self):
        user = UserFactory(first_name='', last_name='')
        userprofile = user.userprofile
        self.assertEqual(userprofile.full_name, user.email)

    def test_str_with_name(self):
        user = UserFactory(first_name='First', last_name='Last')
        userprofile = user.userprofile
        self.assertEqual(userprofile.full_name, 'First Last')

    def test_unicode_contact_information(self):
        user = UserFactory(first_name='Bänjàmin', last_name='Blümchen')
        info = ContactInformationFactory.get_contact_information_for_user(user)
        self.assertEqual(info['contact']['email'], user.email)
        self.assertEqual(info['user'], 'Bänjàmin Blümchen')

    def test_contact_information(self):
        user = UserFactory(first_name='First', last_name='Tester')
        info = ContactInformationFactory.get_contact_information_for_user(user)
        self.assertEqual(info['contact']['email'], user.email)
        self.assertEqual(info['user'], 'First Tester')

    def test_already_unicode_contact_information(self):
        user = UserFactory(first_name='Bänjamin', last_name='Tester')
        info = ContactInformationFactory.get_contact_information_for_user(user)
        self.assertEqual(info['contact']['email'], user.email)
        self.assertEqual(info['user'], 'Bänjamin Tester')
