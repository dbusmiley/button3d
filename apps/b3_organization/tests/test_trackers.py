from datetime import timedelta

from actstream.models import actor_stream, model_stream
from django.contrib.auth.models import User
from django.utils import timezone

from apps.b3_order.factories import OrderFactory
from apps.b3_organization.tracking.utils import VERBS, REGISTRATION, ORDER
from apps.b3_organization.utils import get_current_org
from apps.b3_signup.models import Signup
from apps.b3_tests.factories import UserFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_order.models import Order


class TrackerTest(TestCase):

    def setUp(self):
        super().setUp()
        self.test_password = 'test'
        self.user = UserFactory(password=self.test_password)

    def test_user_interaction_tracking(self):
        get_current_org().organization_panel_admins.add(self.user)

        # for logging in, an interaction is tracked
        self.client.login(email=self.user.email, password=self.test_password)
        self.assertEqual(len(actor_stream(self.user)), 1)
        self.assertEqual(actor_stream(self.user).first().verb, VERBS.INTERACTS)

        # If user has not interacted for one day, interaction is tracked again
        self.user.last_login = timezone.now() - timedelta(days=1)
        self.user.save()
        self.client.get('/en/dashboard/', follow=True)
        self.assertEqual(len(actor_stream(self.user)), 2)
        self.assertEqual(actor_stream(self.user).first().verb, VERBS.INTERACTS)

    def test_signup_statechange_tracking(self):
        signup = Signup.create('test@test.test', origin_url='test')
        self.assertEqual(len(actor_stream(signup)), 1)
        self.assertEqual(
            actor_stream(signup).first().data['new_state'],
            REGISTRATION.FIRST_STEP
        )

        signup.create_user()
        self.assertEqual(len(actor_stream(signup)), 2)
        self.assertEqual(
            actor_stream(signup).first().data['old_state'],
            REGISTRATION.FIRST_STEP
        )
        self.assertEqual(
            actor_stream(signup).first().data['new_state'],
            REGISTRATION.REGISTERED
        )

    def test_order_statechange_tracking(self):
        order = OrderFactory()
        self.populate_current_request()

        self.assertEqual(len(actor_stream(order)), 1)
        self.assertEqual(
            actor_stream(order).first().data['new_state'],
            ORDER.PENDING
        )
        order.set_status('Printing')
        self.assertEqual(len(actor_stream(order)), 2)
        self.assertEqual(
            actor_stream(order).first().data['new_state'],
            ORDER.PRINTING
        )
        self.assertEqual(
            actor_stream(order).first().data['old_state'],
            ORDER.PENDING
        )


class ActionIntegrityTest(TestCase):
    def test_user_deletion_deletes_interactions(self):
        test_password = 'test'
        user = UserFactory(password=test_password)
        # for logging in, an interaction is tracked
        self.client.login(email=user.email, password=test_password)

        self.assertEqual(len(model_stream(User)), 1)
        user.delete()
        self.assertEqual(len(model_stream(User)), 0)

    def test_signup_deletion_deletes_state_changes(self):
        signup = Signup.create('test@test.test', origin_url='test')
        signup.create_user()
        self.assertEqual(len(model_stream(Signup)), 2)
        signup.delete()
        self.assertEqual(len(model_stream(Signup)), 0)

    def test_order_deletion_deletes_state_changes(self):
        order = OrderFactory()
        self.assertEqual(len(model_stream(Order)), 1)
        order.delete()
        self.assertEqual(len(model_stream(Order)), 0)
