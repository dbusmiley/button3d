from django.db import IntegrityError

from apps.b3_organization.models import Organization
from apps.b3_tests.factories import SiteFactory, KeyManagerFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class OrganizationTest(TestCase):
    def test_unique_slug(self):
        organization = Organization()
        organization.site = SiteFactory()
        organization.key_manager = KeyManagerFactory()
        organization.slug = 'organization-slug-1'
        organization.showname = 'Organization A'
        organization.save()

        organization_alt = Organization()
        organization_alt.site = SiteFactory()
        organization_alt.key_manager = KeyManagerFactory()
        organization_alt.slug = 'organization-slug-1'
        organization_alt.showname = 'Organization B'
        with self.assertRaises(IntegrityError):
            organization_alt.save()
