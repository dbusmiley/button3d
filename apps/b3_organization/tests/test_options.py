from django.core.cache import cache

from ..utils import set_current_site
from apps.b3_tests.factories import SiteFactory, \
    OrganizationPartnerOptionFactory, OrganizationFactory, \
    PartnerFactory, OrganizationProductOptionFactory, \
    ProductFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.partner.models import Partner


class PartnerOptionTest(TestCase):
    def setUp(self):
        super(PartnerOptionTest, self).setUp()
        self.partner = PartnerFactory()
        self.orgsite = SiteFactory()
        # Create vat_rate = 22.0 for main site
        # As asked by Felix, this can be used to override also main site
        # partners.
        self.option3 = OrganizationPartnerOptionFactory(
            partners=[self.partner],
            site=self.site,
            key='vat_rate',
            value='22.0',
        )

    def test_partner_options(self):
        # Custom Site/Org
        set_current_site(self.orgsite)
        self.assertEqual(
            self.partner.org_options.vat_rate,
            19.0)  # inherited from partner instance
        # Main Site
        set_current_site(self.site)
        self.assertEqual(
            self.partner.org_options.vat_rate,
            22.0)
        self.assertNotEqual(
            self.partner.org_options.vat_rate,
            self.partner.vat_rate)

    def test_partner_options_queries(self):
        # Main Site
        set_current_site(self.site)
        partner = PartnerFactory()
        with self.assertNumQueries(1):
            repr(partner.org_options.vat_rate)
            repr(partner.org_options.price_currency)

        # Custom Site/Org
        set_current_site(self.orgsite)
        with self.assertNumQueries(1):
            repr(partner.org_options.vat_rate)
            repr(partner.org_options.price_currency)

    def test_partner_cache(self):
        cache.set('partner-cache', [self.partner])
        self.assertEqual(cache.get('partner-cache'), [self.partner])


class OrganizationPartnerTest(TestCase):
    def setUp(self):
        super(OrganizationPartnerTest, self).setUp()
        self.orgsite = SiteFactory()
        self.partner_1 = PartnerFactory(site=None)
        self.partner_2 = PartnerFactory(
            site=self.orgsite,
            name='Test Partner 2',
            code='test-2'
        )
        self.org = OrganizationFactory.create(
            site=self.orgsite,
            partners_enabled=(self.partner_1, self.partner_2),
        )
        self.organization.partners_enabled.remove(self.partner_2)

    def test_partners_enabled(self):
        # Custom Site/Org
        set_current_site(self.orgsite)
        partners = Partner.objects.all()
        self.assertEqual(partners.count(), self.org.partners_enabled.count())
        self.assertEqual(
            partners.filter(
                code='test-2').first().site,
            self.orgsite)
        # Main Site
        set_current_site(self.site)
        partners = Partner.objects.all()
        self.assertEqual(partners.count(), 1)
        self.assertIsNone(partners.filter(code='test-2').first())


class ProductOptionTest(TestCase):
    def test_product_option(self):
        p = ProductFactory(title='Normal title')

        OrganizationProductOptionFactory(
            site=self.site,
            product=p,
            key='title',
            value='Overriden title')
        self.assertEqual(p.org_options.title, 'Overriden title')
        self.assertEqual(p.title, 'Normal title')

    def test_product_option_not_overriden(self):
        p = ProductFactory(title='Normal title')
        self.assertEqual(p.org_options.title, 'Normal title')
