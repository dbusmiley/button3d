from apps.b3_core.base import BaseMiddleware
from apps.b3_organization.utils import get_current_site


class IframeHeaderMiddleware(BaseMiddleware):
    """
    Adds the allowed Iframe Domains as HTTP Header into the response
    """

    def __call__(self, request):
        response = self.get_response(request)
        if 'Content-Security-Policy' in response:
            raise RuntimeError(
                "We are about to overwrite the csp-header."
                "We should use django-csp from now on."
            )

        org = get_current_site(request=request).organization
        if org.allowed_iframe_domains:
            # removes newlines and spaces from domain names
            # expecting domains to be newline separated strings.
            allowed_domains = " ".join(
                org.allowed_iframe_domains.split()
            )
            response['Content-Security-Policy'] = \
                f"frame-ancestors {allowed_domains} 'self';"
        else:
            response['Content-Security-Policy'] = \
                f"frame-ancestors 'self';"

        return response
