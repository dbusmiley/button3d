import logging

from django.core.exceptions import SuspiciousOperation

from apps.b3_core.base import BaseMiddleware
from apps.b3_organization.utils import get_current_site, set_current_site

logger = logging.getLogger(__name__)


class CurrentSiteMiddleware(BaseMiddleware):
    """Middleware that puts the site & organization
        object in current request object and threadlocals storage

    We're using it instead of django's default
        sites app middleware as Site.objects.get_current
    is using by default site instance matching
        with settings.SITE_ID, ignoring current request domain.
    """

    def __call__(self, request):

        site = get_current_site(request)
        if not site:
            raise SuspiciousOperation(
                ("The domain '%s' does not belong to any "
                 "organization in the database.") %
                request.get_host().split(":")[0])
        set_current_site(site)
        setattr(request, 'site', site)
        try:
            setattr(request, 'organization', site.organization)
        except Exception:
            raise SuspiciousOperation(
                (
                    "The domain '%s' is not connected to an organization in "
                    "the database."
                ) % site.domain
            )

        return self.get_response(request)
