
import base64
import logging

from django.http import HttpResponse

from apps.b3_core.base import BaseMiddleware
from apps.b3_organization.utils import get_current_org

logger = logging.getLogger(__name__)


class BasicAuthProtectionMiddleware(BaseMiddleware):
    """
    Some middleware to authenticate requests.
    Can be enabled per organization
    Derived from
    https://github.com/pbs/django-http-auth/blob/master/multisiteauth/middleware.py
    """

    def __call__(self, request):
        # adapted from
        # https://github.com/amrox/django-moat/blob/master/moat/middleware.py
        if get_current_org().is_http_auth_enabled:
            # check if we are already authenticated
            if request.session.get('basicauth_username'):
                logger.debug("Already authenticated as: %s",
                             request.session.get('basicauth_username'))
                return self.get_response(request)
            else:
                logger.debug("Could not find basic auth user in session")

            # Check for "cloud" HTTPS environments
            # adapted from http://djangosnippets.org/snippets/2472/
            if 'HTTP_X_FORWARDED_PROTO' in request.META:
                if request.META['HTTP_X_FORWARDED_PROTO'] == 'https':
                    request.is_secure = lambda: True

            return self._http_auth_helper(request)
        return self.get_response(request)

    def _http_auth_helper(self, request):
        # At this point, the user is either not logged in, or must log in using
        # http auth. If they have a header that indicates a login attempt, then
        # use this to try to login.
        if 'HTTP_AUTHORIZATION' in request.META:
            auth = request.META['HTTP_AUTHORIZATION'].split()
            if len(auth) == 2:
                if auth[0].lower() == 'basic':
                    # Currently, only basic http auth is used.
                    auth_content = auth[1].encode('ascii')
                    decoded_content = base64.b64decode(
                        auth_content).decode('ascii')
                    username, password = decoded_content.split(':')
                    if username == get_current_org().http_auth_user and \
                            password == get_current_org().http_auth_password:
                        request.session['basicauth_username'] = username
                        return self.get_response(request)

        # The username/password combo was incorrect, or not logprovided.
        # Challenge the user for a username/password.
        resp = HttpResponse()
        resp.status_code = 401

        resp['WWW-Authenticate'] = 'Basic'
        return resp
