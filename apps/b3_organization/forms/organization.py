from django import forms

from apps.b3_organization.models import Organization, \
    OrganizationTheme, KeyManager
from django.utils.translation import ugettext_lazy as _, \
    ugettext, activate, get_language
from ckeditor.widgets import CKEditorWidget


class OrganizationForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(OrganizationForm, self).__init__(*args, **kwargs)
        self.fields['partners_enabled'].required = False
        self.fields['terms_conditions_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields['terms_conditions_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_image')
        self.fields["terms_conditions_de"].required = False
        self.fields["terms_conditions_en"].required = False
        self.fields["customer_number_label_de"].required = False
        self.fields["customer_number_label_en"].required = False
        self.fields["vat_id_label_de"].required = False
        self.fields["vat_id_label_en"].required = False

    def clean(self):
        super(OrganizationForm, self).clean()
        is_single_supplier_account = self.cleaned_data.get(
            'is_single_supplier_account')
        partners = self.cleaned_data.get('partners_enabled')

        if is_single_supplier_account:
            if partners is None:
                raise forms.ValidationError(
                    _(('You should enable atleast one partner')))

        force_verify_customer_address = self.cleaned_data.get(
            'force_verify_customer_address')
        is_extended_registration_enabled = self.cleaned_data.get(
            'is_extended_registration_enabled')
        if force_verify_customer_address and \
                not is_extended_registration_enabled:
            raise forms.ValidationError(
                _(('You cannot force customer address '
                   'verification if extended registration is not enabled')))

        verify_customer_address_users = self.cleaned_data.get(
            'verify_customer_address_users')
        if force_verify_customer_address and len(
                verify_customer_address_users) <= 0:
            raise forms.ValidationError(
                _(('At least one customer address verification user '
                   'must be defined if customer address verification '
                   'forcing is enabled')))

        org_site = self.cleaned_data.get('site')
        if any([user.userprofile.site !=
                org_site for user in verify_customer_address_users]):
            raise forms.ValidationError(
                _(('Customer address verification users must '
                   'all belong to the same site as the organization')))

        organization_panel_admins = self.cleaned_data.get(
            'organization_panel_admins')
        if organization_panel_admins:
            if any([user.userprofile.site !=
                    org_site for user in organization_panel_admins]):
                raise forms.ValidationError(
                    _(('Organization panel administrators must '
                       'all belong to the same site as the organization')))

        if self.cleaned_data.get('disable_normal_login'):
            if not self.cleaned_data.get('auth_plugins_enabled'):
                raise forms.ValidationError(
                    {
                        'disable_normal_login': _(
                            'You should enable an auth plugin to use this '
                            'feature'
                        )
                    }
                )

        if self.cleaned_data.get('automatic_redirect_to_oauth'):
            if not self.cleaned_data.get('auth_plugins_enabled'):
                raise forms.ValidationError(
                    {
                        'automatic_redirect_to_oauth': _(
                            'You should enable an auth plugin to use this '
                            'feature'
                        )
                    }
                )

        return self.cleaned_data


class OrganizationThemeForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OrganizationThemeForm, self).__init__(*args, **kwargs)
        self.fields['footer_email_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom')
        self.fields['footer_email_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom')
        self.fields['contact_info_footer_en'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_font_size')
        self.fields['contact_info_footer_de'].widget = CKEditorWidget(
            config_name='ckeditor_custom_no_font_size')
        self.fields['request_user_confirmation_text_en'].widget = \
            CKEditorWidget(config_name='ckeditor_custom')
        self.fields['request_user_confirmation_text_de'].widget = \
            CKEditorWidget(config_name='ckeditor_custom')
        self.fields['supplier_additional_panel_text_de'].widget = \
            CKEditorWidget(config_name='ckeditor_custom')
        self.fields['supplier_additional_panel_text_en'].widget = \
            CKEditorWidget(config_name='ckeditor_custom')

        self._set_german_fields_default_values()

    def _set_german_fields_default_values(self):
        current_lang = get_language()
        activate('de')
        self.fields['signup_text_button_de'].initial = ugettext('Sign Up')
        self.fields['signup_text_de'].initial = ugettext(
            ('Sign up for a free account on 3YOURMIND and '
             'start managing your 3D printing projects online.')
        )
        self.fields['description_portal_de'].initial = ugettext(
            'Only the best 3D printing services')

        self.fields['request_page_title_de'].initial = ugettext(
            'Request for 3YOURMIND')
        self.fields['request_compare_step3_text_de'].initial = ugettext(
            'Ask our 3D printing experts by creating a manual request.'
            'Our in-house 3D Modelers can assist you editing the 3D model.'
        )
        self.fields['request_user_subject_mail_de'].initial = ugettext(
            '3YOURMIND Support')
        self.fields['request_user_confirmation_text_de'].initial = ugettext(
            ('Hello,<br>Thanks for contacting us! Please let us '
             'some time to have a look at your request. '
             'You will get an answer within one workday.'))
        activate(current_lang)

    class Meta:
        model = OrganizationTheme
        fields = '__all__'


class KeyManagerForm(forms.ModelForm):
    class Meta:
        model = KeyManager
        fields = '__all__'
