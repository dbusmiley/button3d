from django import forms

from apps.b3_organization.models import OrganizationPartnerOption, \
    OrganizationProductOption


class OrganizationOptionForm(forms.ModelForm):
    key = forms.ChoiceField(label="Key", choices=(),
                            widget=forms.Select(attrs={'class': 'selector'}))

    def __init__(self, *args, **kwargs):
        super(OrganizationOptionForm, self).__init__(*args, **kwargs)
        self.fields['key'].choices = self._key_choices

    def _key_choices(self):
        raise NotImplementedError()


class OrganizationPartnerOptionForm(OrganizationOptionForm):
    non_required_fields = [
        'bluebox_preview_step_en',
        'bluebox_preview_step_de',
        'bluebox_preview_step']
    key_is_excluded = False

    # Excluded partner option keys. No need to move it to project settings.
    _excluded_options = (
        'id',
        'code',
        'name',
        'shortname',
        'website',
        'email',
        'price_currency',
        'site',
        'address'
    )

    def _key_choices(self):
        # To prevent circular import, import Partner here
        from apps.partner.models import Partner
        choices = []
        fields = Partner._meta.fields
        for field in fields:
            if field.name in self._excluded_options:
                continue
            choices.append(
                (field.name,
                 "{} ({})".format(
                     field.verbose_name,
                     field.name)))
        return choices

    def is_valid(self):
        valid = super(OrganizationPartnerOptionForm, self).is_valid()
        if 'key' not in self.cleaned_data or \
                'site' not in self.cleaned_data or \
                'partners' not in self.cleaned_data:
            return False
        if not valid:
            key = str(self.cleaned_data['key'])
            value = self.cleaned_data['value'] \
                if 'value' in self.cleaned_data else None

            # Check fields that can have empty values with actual value
            if key in self.non_required_fields:
                self.key_is_excluded = True

            if self.key_is_excluded and not value:
                return True
        return valid

    """
    Overriding whole save method to be able to set instance.value
        as None avoiding to catch self.errors of validation
    """

    def save(self, commit=True):

        if self.errors:
            if 'value' in self.errors and self.key_is_excluded:
                self.instance.value = ""
            else:
                raise ValueError(
                    ("The %s could not be %s because "
                     "the data didn't validate.") %
                    (self.instance._meta.object_name,
                     'created' if self.instance._state.adding else 'changed',
                     ))
        if commit:
            self.instance.save()
            self._save_m2m()
        else:
            self.save_m2m = self._save_m2m
        return self.instance

    class Meta:
        model = OrganizationPartnerOption
        fields = '__all__'


class OrganizationProductOptionForm(OrganizationOptionForm):
    _excluded_options = (
        'id',
        'site',
        'parent',
        'date_created',
        'date_updated',
        'slug',
        'technology',
        'product_class',
        'is_discountable',
        'upc',
    )

    class Meta:
        model = OrganizationProductOption
        fields = '__all__'

    def _key_choices(self):
        from apps.catalogue.models.product import Product
        choices = []
        fields = Product._meta.fields
        for field in fields:
            if field.name in self._excluded_options:
                continue
            choices.append(
                (field.name, "{} ({})".format(field.verbose_name, field.name)))
        return choices
