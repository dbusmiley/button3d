from django.db import models
from django.db.models import Q
from django.conf import settings
from django.dispatch import receiver
from django.contrib.sites.models import Site
from django.db.models.signals import pre_save

from ..utils import get_current_site


class CurrentSiteManager(models.Manager):
    """
    Model manager to get queryset based on current request site.

    We're using it instead of django's default
        sites app middleware as Site.objects.get_current
    is using by default site instance
        matching with settings.SITE_ID, ignoring current request domain.
    """

    def get_queryset(self):
        qs = super(CurrentSiteManager, self).get_queryset()
        site = get_current_site()

        if hasattr(self.model(), 'uuid'):
            # hack to get stl demo files across all sites.
            # We should have separate demo
            # files for each site instead. TODO: decision needed
            return qs.filter(Q(site=site) | Q(uuid=settings.DEMO_UUID))
        else:
            return qs.filter(site=site)


class AbstractSiteModel(models.Model):
    """
    Subclasss from this Model for all Pro Account Specific (not-global) Models.
    """
    site = models.ForeignKey(Site, blank=True, null=True)

    # Use all_objects when you need to access all records.
    all_objects = models.Manager()

    # use objects when you need to access site records.
    objects = CurrentSiteManager()

    class Meta:
        abstract = True

    @property
    def organization(self):
        return self.site.organization


@receiver(pre_save)
def set_site_on_pre_save(sender, instance, **kwargs):
    """
    Set site automatically when a model instance
        using AbstractSiteModel is saved
    for the first time. We need it to add site to oscar model instances without
    having to change oscar models and views code.
    """
    if issubclass(sender, AbstractSiteModel):
        if not instance.pk and not instance.site:
            instance.site = get_current_site()
