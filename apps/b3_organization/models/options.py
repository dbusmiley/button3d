from django.core.exceptions import FieldDoesNotExist
from django.core.validators import RegexValidator, ValidationError
from django.db import models
from django.db.models.signals import m2m_changed
from django.utils.translation import ugettext_lazy as _, get_language

from apps.b3_organization.utils import get_current_site


class OptionsWrapper(object):
    """
    Options dict wrapper to fallback to model instance attribute value.
    """

    def __init__(self, instance, **options):
        self.instance = instance
        self.__dict__.update(options)

    def __getattr__(self, name):
        if name.startswith('__') and name.endswith('__'):
            return super(OptionsWrapper, self).__getattr__(name)

        if name in self.__dict__:
            return self.__dict__[name]
        else:
            return getattr(self.__dict__['instance'], name, None)


class OrganizationOptionModel(object):
    """When inheriting from this class, you must set
    org_option_class = Organization...Option
    to use the correct class that represents the options
    """
    @property
    def org_options(self):
        site = get_current_site()
        if not hasattr(self, '_options'):
            self._options = {}
        if site not in self._options:
            self._options[site] = \
                self.org_option_class.objects.get_options(self, site)
        return self._options[site]


class OrganizationOptionManager(models.Manager):
    def get_queryset(self):
        return super(
            OrganizationOptionManager,
            self).get_queryset().select_related('site')

    def _get_field(self, key):
        try:
            field = self.get_model_class()._meta.get_field(key)
        except FieldDoesNotExist:
            return
        else:
            return field

    def _get_option_value(self, key, value):
        field = self._get_field(key)
        if field:
            return field.to_python(value)
        else:
            return value

    def get_custom_queryset(self, instance, site):
        """This should implement how to retrive the
        corresponding options
        """
        raise NotImplementedError()

    def get_model_class(self):
        """Should be implement and return the
        basis model that we are overriding the
        attributes for"""
        raise NotImplementedError()

    def get_options(self, instance, site):
        # TODO: too many queries, add local class attribute caching
        options = {}
        current_lang = get_language()
        queryset = self.get_custom_queryset(instance, site)
        for key, value in queryset.values_list('key', 'value'):
            options[key] = self._get_option_value(key, value)
            # Replace translated fields
            if hasattr(instance, 'translated_fields'):
                for field in instance.translated_fields:
                    if key == '{}_{}'.format(field, current_lang):
                        options[field] = options[key]
        return OptionsWrapper(instance, **options)


class OrganizationPartnerOptionManager(OrganizationOptionManager):
    def get_custom_queryset(self, instance, site):
        return self.filter(partners__id=instance.id, site=site)

    def get_model_class(self):
        from apps.partner.models import Partner
        return Partner


class OrganizationProductOptionManager(OrganizationOptionManager):
    def get_custom_queryset(self, instance, site):
        return self.filter(product__id=instance.id, site=site)

    def get_model_class(self):
        from apps.catalogue.models.product import Product
        return Product


class AbstractOrganizationOption(models.Model):
    # Validate key to have key as python-safe attribute name.
    _key_regex = RegexValidator(
        regex='[a-zA-Z_][a-zA-Z0-9_]*',
        message=_('Key contains invalid characters!'))

    class Meta:
        abstract = True

    site = models.ForeignKey('sites.Site', blank=True, null=True)
    key = models.CharField(max_length=255, validators=[_key_regex])
    value = models.CharField(max_length=255)


class OrganizationPartnerOption(AbstractOrganizationOption):
    partners = models.ManyToManyField(
        'partner.Partner', verbose_name='Partners')
    objects = OrganizationPartnerOptionManager()

    def get_partners_string(self):
        return ', '.join(map(str, self.partners.all()))

    def clean(self):
        if self.key and self.value:
            try:
                OrganizationPartnerOption.objects._get_option_value(
                    self.key, self.value)
            except ValueError as e:
                raise ValidationError(e.message)

    def __str__(self):
        return '{}: {}'.format(self.get_partners_string(), self.key)


class OrganizationProductOption(AbstractOrganizationOption):
    product = models.ForeignKey('catalogue.Product')
    objects = OrganizationProductOptionManager()

    def __str__(self):
        return '{} - {}: {}'.format(self.site, self.product, self.key)


def delete_organization_partner_option_duplicates(
        sender, instance, action, **kwargs):
    if action == 'post_add':
        instance = OrganizationPartnerOption.objects.get(id=instance.id)
        for partner in instance.partners.all():
            duplicate_instances = OrganizationPartnerOption.objects.filter(
                site=instance.site,
                key=instance.key,
                partners=partner).exclude(id=instance.id)
            for duplicate_instance in duplicate_instances:
                duplicate_instance.partners.remove(partner)
                if OrganizationPartnerOption.objects.get(
                        id=duplicate_instance.id).partners.all().count() == 0:
                    duplicate_instance.delete()


m2m_changed.connect(
    delete_organization_partner_option_duplicates,
    sender=OrganizationPartnerOption.partners.through,
    dispatch_uid="delete_organization_partner_option_duplicates"
)
