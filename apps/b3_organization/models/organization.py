import typing as t

from colorfield.fields import ColorField
from django.contrib.auth import user_logged_in
from django.contrib.auth.models import User, Group
from django.contrib.sites.models import Site
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Template, Context
from django.template.loader import get_template
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _, \
    activate, get_language, ugettext

import button3d.type_declarations as td
from apps.b3_auth_plugins.models import SiteAuthPluginConfig
from apps.b3_core.models.deleted_date import AbstractDeletedDateModel
from apps.b3_misc.validators import FileValidator
from apps.b3_organization.tracking.trackers import UserInteractionTracker
from .site import AbstractSiteModel
from ..utils import get_current_site, get_current_org


class NaturalKeyOrganizationManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, site_name) -> 'Organization':
        return self.get(site__name=site_name)


class OrganizationManager(NaturalKeyOrganizationManager):
    def get_queryset(self) -> t.Sequence['Organization']:
        return super(
            OrganizationManager, self).get_queryset().select_related('site')


@python_2_unicode_compatible
class Organization(AbstractDeletedDateModel):
    HIDDEN, OPTIONAL, REQUIRED = 'Hidden', 'Optional', 'Required'
    VISIBILITY_CHOICES = (
        (HIDDEN, 'Hidden'),
        (OPTIONAL, 'Optional'),
        (REQUIRED, 'Required'),
    )

    site = models.OneToOneField(Site)
    slug = models.SlugField(blank=True, unique=True)
    showname = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    partners_enabled = models.ManyToManyField('partner.Partner')
    auth_plugins_enabled = models.ForeignKey(
        SiteAuthPluginConfig, blank=True, null=True,
        on_delete=models.SET_NULL
    )
    disable_normal_login = models.BooleanField(
        default=False, help_text=_(
            "Setting this would disable login via Email/Password methods"
        )
    )
    automatic_redirect_to_oauth = models.BooleanField(
        default=False, help_text=_(
            "If set, the user would be automatically redirected to OAUTH "
            "login page. Use it only after enabling some "
            "'auth_plugins_enabled' with valid configurations."
        )
    )

    private_api_key = models.CharField(max_length=255, blank=True)
    key_manager = models.ForeignKey('b3_organization.KeyManager', null=True)
    organization_panel_admins = models.ManyToManyField(User, blank=True)
    notify_organizational_panel_admins = models.BooleanField(
        default=True,
        verbose_name='Send order Emails to organization panel admins',
        help_text='When enabled, E-mails are sent out to organizational panel '
                  'admins on an order')

    # Deprecated.
    manual_pricing_user = models.ForeignKey(
        User, related_name='pricing_orgs',
        null=True,
        blank=True
    )
    printing_service_onboarding_user = models.ForeignKey(
        User, related_name='onboarding_orgs', null=True, blank=True)

    # Configuration flags
    is_single_supplier_account = models.BooleanField(
        default=False,
        verbose_name='Hide Printing Service Selection'
    )
    anonymous_upload_enabled = models.BooleanField(default=True)
    register_enabled = models.BooleanField(default=True)
    login_enabled = models.BooleanField(default=True)
    filters_enabled = models.BooleanField(default=True)

    link_sharing_enabled = models.BooleanField(default=True)

    file_validation_enabled = models.BooleanField(default=False)
    file_validation_message = models.TextField(
        null=True, blank=True, help_text=_(
            "Input fields of type checkbox or radio "
            "must define the binding attribute:"
            "data-bind=\"checkedOptions\".<br>The value of "
            "the input must contain 'disable'"
            "if the corresponding option should disable "
            "the upload or the value must contain 'enable'"
            "to enable the upload.<br>By default, when opening "
            "the pop-up, upload will be disabled."
            "To enable upload by default, add on the highest "
            "HTML tag the attribute:"
            "data-default-enable-upload=\"true\".")
    )

    force_verify_customer_address = models.BooleanField(default=False)
    verify_customer_address_users = models.ManyToManyField(
        User,
        related_name='verify_organizations',
        blank=True,
        verbose_name=_('Verifying customer address users')
    )
    is_extended_registration_enabled = models.BooleanField(default=False)
    manual_request_attachment_upload_enabled = models.BooleanField(
        default=True
    )
    single_upload_redirects_to_project_detail = models.BooleanField(
        default=False
    )
    enable_optimized_file_download = models.BooleanField(
        default=True,
        verbose_name=_('Enable users to download optimized 3D files')
    )

    is_http_auth_enabled = models.BooleanField(default=False)
    http_auth_user = models.CharField(max_length=255, blank=True, null=True)
    http_auth_password = models.CharField(
        max_length=255, blank=True, null=True)

    # Deprecated
    is_company_information_in_payment_step_enabled = models.BooleanField(
        default=True,
        help_text="This is deprecated and only used in the old Checkout"
    )
    show_net_prices = models.BooleanField(
        default=False,
        verbose_name=_('Show only Net Prices for customers'),
        help_text=_('Enabling this will also turn on Mandatory Company Name')
    )
    # Deprecated
    mandatory_company_name = models.BooleanField(
        default=False,
        help_text="This is deprecated and only used in the old Checkout"
    )

    company_information_visibility = models.CharField(
        max_length=10,
        choices=VISIBILITY_CHOICES,
        default=OPTIONAL,
        help_text='This setting configures, how the company related fields'
                  'behave on all address inputs (registration and checkout)'
                  ''
                  'Hidden: Company, VAT-Number and Department is hidden.'
                  ''
                  'Optional: Company, VAT-Number and Department'
                  'are shown but optional'
                  ''
                  'Required: Company and VAT-Number are shown and reuired'
                  'Depatment is shown and optional'
    )

    vat_id_label = models.CharField(
        max_length=100,
        default='VAT Number',
        help_text='How should the VAT-Number field be called?'
    )

    customer_number_visibility = models.CharField(
        max_length=10,
        choices=VISIBILITY_CHOICES,
        default=HIDDEN,
        help_text='This setting configures, how the customer number field'
                  'behaves in the extended registration'
    )

    customer_number_label = models.CharField(
        max_length=100,
        default='Customer Number',
        help_text='How should the Customer Number field be called?'
    )

    is_yoda_active = models.BooleanField(default=False)

    allow_insecure_download = models.BooleanField(
        default=False,
        help_text=_('Used for 3D Viewer SDK customers. '
                    'In doubt, leave this unchecked. '
                    'Tells the 3D Backend that the download endpoint can'
                    'be used with GET parametres instead of JWT.'
                    'See https://goo.gl/6ZaSdj for details.'
                    )
    )
    terms_conditions = models.TextField()

    allowed_iframe_domains = models.TextField(
        help_text='Domains on which this app can be displayed'
                  'inside an IFrame. Separated by new-line.'
                  'If set, the HTTP Header "Content-Security-Policy"'
                  'will be set accordingly.',
        null=True,
        blank=True
    )

    objects = OrganizationManager()
    all_objects = NaturalKeyOrganizationManager()

    def save(self, **kwargs):
        if self.site and not self.slug:
            self.slug = slugify(self.site.name)

        if not self.pk:
            self.generate_private_key()

        return super(Organization, self).save(**kwargs)

    def __str__(self):
        return self.showname or self.site.name

    def get_default_shipping_address(self) -> td.OrganizationShippingAddress:
        default_addr = self.shipping_addresses.filter(is_default=True)
        nb_default_addr = default_addr.count()
        if nb_default_addr == 0 or nb_default_addr > 1:
            raise AttributeError(
                ('An organization must have exactly one '
                 'default shipping address ({0} has {1})').format(
                    self, nb_default_addr))
        return default_addr[0]

    @property
    def is_single_supplier(self) -> bool:
        if self.is_single_supplier_account:
            return self.partners_enabled.first()
        return False

    @property
    def theme(self) -> t.Optional['OrganizationTheme']:
        try:
            return self.site.organizationtheme
        except OrganizationTheme.DoesNotExist:
            return

    def generate_private_key(self, force_replace: bool = False) -> None:
        import uuid

        if self.private_api_key and force_replace or not self.private_api_key:
            self.private_api_key = uuid.uuid4()

    # used by dump_site/load_site
    def natural_key(self) -> t.Tuple[str]:
        return (self.site.name,)

    @property
    def rendered_file_validation_message(self) -> str:
        t = Template(self.file_validation_message)
        return t.render(Context())


def _theme_upload_to(instance, filename) -> str:
    site = instance.site
    return "sites/{}/themes/static/{}".format(slugify(site.domain), filename)


class LinkManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, title, url) -> 'Link':
        return self.get(title=title, url=url)


@python_2_unicode_compatible
class Link(models.Model):
    objects = LinkManager()

    organization_theme = models.ForeignKey('b3_organization.OrganizationTheme')
    title = models.CharField(max_length=128)
    url = models.URLField(blank=True)
    published = models.BooleanField(default=True)
    position = models.PositiveIntegerField(
        _('Position'), default=0, blank=True, null=True)

    def __str__(self):
        return "%s" % self.title

    class Meta(object):
        ordering = ('position',)

    # used by dump_site/load_site
    def natural_key(self):
        return self.title, self.url


class KeyManagerManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, name, email):
        return self.get(name=name, email=email)


class KeyManager(models.Model):
    title = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    role = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)
    manager_photo = models.ImageField(
        blank=True, null=True, upload_to='key-managers/')

    objects = KeyManagerManager()

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.title)

    @property
    def referenced_by(self):
        return Organization.objects.filter(key_manager=self)

    # used by dump_site/load_site
    def natural_key(self):
        return self.name, self.email


class OrganizationThemeManager(models.Manager):
    # used by dump_site/load_site
    def get_by_natural_key(self, site_name):
        return self.get(site__name=site_name)


@python_2_unicode_compatible
class OrganizationTheme(models.Model):
    """Logo in Navbar (with link to their homepage)
    Change Colors of:
    Body BG
    Navbar-bg
    Navbar-Text-Color
    Button-Primary-bg
    primary-text-color
    Button-secondary-bg
    Button-secondary-text-color
    Change Viewer Background Image"""

    objects = OrganizationThemeManager()

    site = models.OneToOneField(Site)
    body_bkg_color = ColorField(
        _('Background color'),
        blank=True,
        default='#EEEEEE')

    navbar_bkg_color = ColorField(
        _('Topmost navigation bar color'),
        blank=True,
        default='#FFFFFF')
    navbar_txt_color = ColorField(
        _('Topmost navigation bar text color  '),
        blank=True,
        default='#555555')

    navbar_logo_image = models.ImageField(
        _('Logo'), blank=True, null=True, upload_to=_theme_upload_to)
    navbar_logo_url = models.URLField(
        _('Logo link'),
        blank=True
    )

    primary_bkg_color = ColorField(
        _('Main color'), blank=True, default='#2C66C4')
    primary_txt_color = ColorField(
        _('Main text color'),
        blank=True,
        default='#FFFFFF')
    primary_elements_color = ColorField(
        _('Main color for Visual elements'),
        blank=True,
        default='#2C66C4',
        help_text=_(('Cube, Sliders, highlights, calendar, '
                     'header-bar of uploaded 3D model, ...'))
    )

    btn_secondary_bkg_color = ColorField(
        _('Secondary button color'),
        blank=True,
        default='#FF7800')
    btn_secondary_txt_color = ColorField(
        _('Secondary button text color'),
        blank=True,
        default='#FFFFFF')

    link_color = ColorField(_('Link color'), blank=True, default='#2C66C4')

    checkout_navbar_bkg_color = ColorField(
        _('Second navigation bar color'),
        blank=True,
        default='#3D3D3D')
    checkout_navbar_txt_color = ColorField(
        _('Second navigation bar text color'),
        blank=True,
        default='#FFFFFF')

    viewer_bkg_image = models.ImageField(
        _('3D Viewer background image'),
        blank=True,
        null=True,
        upload_to=_theme_upload_to
    )
    viewer_bkg_color = ColorField(
        _('3D Viewer background Color'),
        blank=True,
        default='#2c66c4')
    viewer_txt_color = ColorField(
        _('3D Viewer text color'),
        blank=True,
        default='#ccc')
    viewer_bkg_repeat = models.CharField(
        _('3D Viewer background repeat'),
        choices=(('repeat', 'Repeat Background'),
                 ('cover', 'Display as cover')),
        default='repeat', max_length=255
    )

    error_color = ColorField(
        _('Custom color Error'),
        blank=True,
        default='#a7082a')
    warning_color = ColorField(
        _('Custom color Warning'),
        blank=True,
        default='#ff7800')
    success_color = ColorField(
        _('Custom color Success'),
        blank=True,
        default='#3c9d07')

    favicon = models.ImageField(
        _('Custom Favicon (.ico)'),
        blank=True,
        null=True,
        upload_to=_theme_upload_to,
        validators=[
            FileValidator(
                allowed_mimetypes=(
                    'image/vnd.microsoft.icon',
                    'image/x-icon'))])

    description_portal = models.CharField(
        _('Description Portal - '
          'this is shown when a link is posted in facebook or slack.'),
        max_length=255,
        default='Only the best 3D printing services'
    )

    customer_support_phone = models.CharField(
        _('Customer Support Telephone'),
        max_length=255,
        default='+49 (0)30 555 78748',
    )

    # Email data
    email_logo_image = models.ImageField(
        blank=True, null=True, upload_to=_theme_upload_to)
    footer_email = models.TextField(_('Email footer'), null=True, blank=True)
    from_email = models.CharField(
        _('Email From Address'),
        max_length=255,
        blank=True,
        default=''
    )

    # Footer information
    contact_info_footer = models.TextField(
        _('Website footer - contact information'), null=True, blank=True)
    powered_by_3yourmind = models.BooleanField(default=True)

    # Modal Signup
    signup_text_button = models.CharField(
        _('Text button Sign Up Modal'),
        max_length=24,
        default='Sign Up'
    )

    signup_text = models.CharField(
        _('Text content Sign Up Modal'),
        max_length=255,
        default=('Sign up for your account and start '
                 'managing your 3D printing projects online.')
    )

    # Manual request texts
    request_receiving_email = models.EmailField(
        _('Email address where the manual requests should be sent'),
        null=True,
        blank=True
    )
    request_page_title = models.CharField(
        _('Title of the manual request page'),
        max_length=256,
        null=True,
        blank=True)

    request_attachment_explanation_text = models.TextField(
        _('Explanation text under attachment on manual request page'),
        null=True,
        blank=True
    )

    request_compare_step3_text = models.TextField(
        _('Text on compare page step3, above the manual request button'),
        default='Ask our 3D printing experts by creating a manual request.'
                'Our in-house 3D Modelers can assist you editing the file.',
        null=True,
        blank=True
    )

    request_user_subject_mail = models.CharField(
        _('Subject of the request confirmation email for the user'),
        default='3YOURMIND Support',
        max_length=512,
        null=True,
        blank=True
    )

    request_user_confirmation_enabled = models.BooleanField(default=False)
    request_user_confirmation_text = models.TextField(
        _('The confirmation email content sent to the user'),
        null=True,
        blank=True,
        default=('Thank you for contacting our team.'
                 '<br>We are reviewing your request '
                 'and will provide feedback within '
                 'one working day.'))

    # Supplier link
    supplier_additional_panel_text = models.TextField(
        _('Text on addtional panel on supplier page'),
        null=True,
        blank=True,
        default=''
    )

    # Customize CSS
    customize_css = models.TextField(_('Customize CSS'), null=True, blank=True)

    project_default_material = models.ForeignKey(
        'catalogue.Product',
        verbose_name=_('3D Project default material'),
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    is_address_company_required = models.BooleanField(
        _('Is address company field required'),
        default=False
    )

    def render_css(self):
        template = get_template('b3_organization/theme_css.html')
        return template.render(context={'theme': self})

    @property
    def links_footer(self):
        return Link.objects.filter(organization_theme=self)

    # used by dump_site/load_site
    def natural_key(self):
        return self.site.name,

    def __str__(self):
        return str(self.site)


# Create an OrganizationTheme for each new Site
def create_organization_theme(sender, instance, created, **kwargs):
    if created:
        current_lang = get_language()
        # Default texts for german translations
        activate('de')
        OrganizationTheme.objects.get_or_create(
            site=instance,
            signup_text_button_de=ugettext('Sign Up'),
            signup_text_de=ugettext(
                'Sign up for a free account on 3YOURMIND'
                'and start managing your 3D printing projects online.'),
            description_portal_de=ugettext(
                'Only the best 3D printing services'),
            request_page_title_de=ugettext('Request for 3YOURMIND'),
            request_compare_step3_text_de=ugettext(
                'Ask our 3D printing experts by creating a manual request.'
                'Our in-house 3D Modelers can assist you editing the file.'),
            request_user_subject_mail_de=ugettext('3YOURMIND Support'),
            request_user_confirmation_text_de=ugettext(
                'Hello,<br>Thanks for contacting us!'
                'Please let us some time to have a look at your request.'
                'You will get an answer within one workday.'))

        activate(current_lang)


post_save.connect(create_organization_theme, sender=Site,
                  dispatch_uid="create_organization_theme")


@python_2_unicode_compatible
class UserProfile(AbstractSiteModel):
    """
    Maps a user OneToOne to a Userprofile.
    This is Django's recommended way of extending the user model.
    We use it to store a domain per user.
    See http://stackoverflow.com/questions/1404131/
    how-to-get-unique-users-across-multiple-django-sites-powered-by-the-sites-fram?lq=1
    """
    ROLE_USER = 'user'
    ROLE_ADMIN = 'admin'
    ROLE_ORGANIZATION_USER = 'organizationUser'
    ROLE_PARTNER_USER = 'partnerUser'

    user = models.OneToOneField(User)
    avatar = models.ImageField(blank=True,
                               null=True,
                               upload_to='user-avatars/')
    customer_number = models.CharField(
        max_length=255,
        blank=True,
        default=''
    )

    @property
    def default_address(self) -> t.Optional[td.Address]:
        """
        Since the default address is not used yet,
        we just return the first address found for this user.

        If you have the BillingAddress available (e.g. in an Order context),
        use that instead.

        TODO: Change this as soon as we save a default address per user.
        """
        return self.user.user_address.first()

    @property
    def full_name(self) -> str:
        """
        Return First Name + Last Name if given,
        otherwise return the mail adress
        """
        user = self.user
        if user.first_name and user.last_name:
            return f'{user.first_name} {user.last_name}'
        elif user.last_name:
            return user.last_name
        elif user.first_name:
            return user.first_name
        else:
            return user.email

    @property
    def avatar_url(self) -> t.Optional[str]:
        if not self.avatar:
            return None
        return self.avatar.url

    @property
    def has_organization(self) -> bool:
        return Organization.objects.filter(site=self.site).exists()

    @property
    def can_use_service_panel(self) -> bool:
        return self.user.partners.exists()

    @property
    def company_from_address(self) -> t.Optional[str]:
        if self.default_address:
            return self.default_address.company_name
        else:
            return None

    @property
    def country_from_address(self) -> t.Optional[td.Country]:
        if self.default_address:
            return self.default_address.country
        else:
            return None

    @property
    def phone_number_from_address(self) -> t.Optional[str]:
        if self.default_address:
            return self.default_address.phone_number
        else:
            return None

    def get_phone_numbers(self) -> t.Sequence[str]:
        """
        Function to get all phone numbers of
            an user in the international format.
        :return: An array containing the phone
            numbers in international format as strings.
        """
        result = []
        for address in self.user.user_address.all():
            if not address.phone_number:
                continue
            result.append(address.phone_number)
        return result

    @property
    def is_admin(self):
        return self.user.is_superuser

    @property
    def is_organization_user(self):
        return get_current_org().organization_panel_admins.filter(
            id=self.user_id).exists()

    @property
    def is_partner_user(self):
        from apps.partner.models import Partner

        return Partner.objects.filter(users__id=self.user_id).exists()

    @property
    def roles(self) -> t.Sequence[str]:
        _roles = [UserProfile.ROLE_USER]
        if self.is_admin:
            _roles.append(UserProfile.ROLE_ADMIN)
        if self.is_organization_user:
            _roles.append(UserProfile.ROLE_ORGANIZATION_USER)
        if self.is_partner_user:
            _roles.append(UserProfile.ROLE_PARTNER_USER)
        return _roles

    def __str__(self):
        return self.full_name


# Create a UserProfile for each new User
def create_user_profile(sender, instance, created, **kwargs):
    current_site = get_current_site()
    if not current_site:
        # If the user is imported from a fixture (sample data),
        #   don't create a user profile.
        # Instead it is expected that the profile is
        #   already set up in the fixture.
        return
    if created:
        UserProfile.objects.create(user=instance, site=current_site)


post_save.connect(
    create_user_profile,
    sender=User,
    dispatch_uid="create_user_profile")


@receiver(user_logged_in, dispatch_uid='user_interaction_on_login')
def track_user_interaction_on_login(sender, request, user, **kwargs):
    UserInteractionTracker.register_event(user)


class GroupProfile(AbstractSiteModel, Group):
    """
    DEPRECATED - NOT USED ANYMORE
    """

    # enforce max length of 50 because maxlength of Goup.name is 80.
    showname = models.CharField(max_length=50)

    class Meta:
        unique_together = ('site', 'showname',)


class OrganizationCustomTemplate(models.Model):
    site = models.ForeignKey(Site)
    template_name = models.CharField(
        max_length=255,
        help_text=_("A template path like 'b3_core/nav.html'"))
    template = models.TextField(
        blank=True,
        help_text=_('The HTML template')
    )

    class Meta:
        unique_together = ('site', 'template_name')

    def __str__(self):
        return "{0} - {1}".format(self.site.name, self.template_name)
