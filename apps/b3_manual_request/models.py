

import logging

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_attachement.models import AbstractAttachment,\
    upload_to_unique_folder
from apps.b3_core.mail import EmailSender
from apps.b3_organization.models import AbstractSiteModel
from apps.b3_organization.utils import getattr_theme


logger = logging.getLogger(__name__)


class ManualRequest(AbstractSiteModel):
    (QUESTION,
     MAKE_OFFER,
     MAKE_PRINTABLE,
     OTHER) = 'question', 'offer', 'printable', 'other'
    TYPE_CHOICES = (
        (MAKE_OFFER, _('Please make me an offer')),
        (MAKE_PRINTABLE, _('Please make my model printable')),
        (QUESTION, _('I have a question')),
        (OTHER, _('Other'))
    )

    type = models.CharField(
        _("What do you need?"),
        choices=TYPE_CHOICES,
        max_length=9,
        default=MAKE_OFFER
    )  # default value needed, or empty in form
    user = models.ForeignKey(User, null=True, blank=True)
    email = models.EmailField(help_text=_('E-Mail'))
    first_name = models.CharField(
        _('First name'),
        max_length=30,
        null=True,
        blank=True)
    last_name = models.CharField(
        _('Last name'),
        max_length=30,
        null=True,
        blank=True)
    company = models.CharField(
        _('Company'),
        max_length=200,
        null=True,
        blank=True,
        help_text=_('Company or Institution'))
    country = models.ForeignKey('b3_address.Country')
    phone_number = models.CharField(
        _('Telephone'),
        max_length=128,
        blank=True)
    message = models.CharField(
        _('Message'),
        max_length=2048,
        help_text=_('Please describe your 3D project and how we can help you.')
    )

    language = models.CharField(
        choices=settings.LANGUAGES,
        max_length=2
    )
    ip_address = models.GenericIPAddressField()

    referer_url = models.CharField(max_length=2048, null=True, blank=True)
    stl_file = models.ForeignKey(
        'b3_core.StlFile',
        to_field='uuid',
        null=True,
        blank=True
    )

    basket = models.ForeignKey('basket.Basket', null=True, blank=True)
    hubspot_data = models.CharField(max_length=512, null=True, blank=True)

    product = models.ForeignKey('catalogue.Product', null=True, blank=True)
    stockrecord = models.ForeignKey(
        'partner.StockRecord',
        null=True,
        blank=True
    )
    color = models.ForeignKey('catalogue.Color', null=True, blank=True)

    configuration = models.OneToOneField(
        'b3_core.Configuration',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def save(self, *args, **kwargs):
        if self.user is None and self.email is None:
            raise ValueError(
                'Cannot create ManualRequest without user nor email')
        return super(ManualRequest, self).save(*args, **kwargs)

    def send_emails(self):
        must_send_email_to_user = getattr_theme(
            'request_user_confirmation_enabled', default=True)

        # Send email to user
        if must_send_email_to_user:
            try:
                self._send_confirmation_email_to_user()
            except Exception:
                logger.exception(
                    'Error during sending manual request email to user')

        # Send email to support
        try:
            self._send_request_email_to_support()
        except Exception:
            logger.exception(
                'Error during sending manual request email to support')

    def _send_confirmation_email_to_user(self):
        email_sender = EmailSender()
        email_sender.send_email(
            to=self.email,
            subject_template_path='b3_manual_request/emails/'
                                  'email_to_user_subject.txt',
            body_txt_template_path='b3_manual_request/emails/'
                                   'email_to_user_body.txt',
            body_html_template_path='b3_manual_request/emails/'
                                    'email_to_user_body.html'
        )

    def _send_request_email_to_support(self):
        receiver = getattr_theme('request_receiving_email', default=None)
        if hasattr(settings, 'EMAIL_CONTACT') and not receiver:
            receiver = settings.EMAIL_CONTACT
        if not receiver:
            logger.warning(('Did not send support email on manual '
                            'request because settings.EMAIL_CONTACT '
                            'undefined'))
            return

        context = {
            'manual_request': self,
            'attachments': self.attachments.all()
        }

        email_sender = EmailSender()
        email_sender.send_email(
            to=receiver,
            subject_template_path='b3_manual_request/emails/'
                                  'email_to_support_subject.txt',
            body_txt_template_path='b3_manual_request/emails/'
                                   'email_to_support_body.txt',
            body_html_template_path='b3_manual_request/emails/'
                                    'email_to_support_body.html',
            extra_context=context
        )

    def __str__(self):
        return "Request from '{0}' and of type: '{1}'".format(
            self.email, self.type)


class Attachment(AbstractAttachment):
    MAX_SIZE_IN_MB = 500
    ATTACHMENT_FOLDER = "request_attachments"
    manual_request = models.ForeignKey(
        ManualRequest,
        null=True,
        on_delete=models.CASCADE,
        related_name="attachments")

    file = models.FileField(
        upload_to=upload_to_unique_folder,
        max_length=512,
    )
