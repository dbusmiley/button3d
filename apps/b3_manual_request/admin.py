from django.contrib import admin

from apps.b3_core.admin import DeletedDateAdmin
from apps.b3_manual_request.models import Attachment, ManualRequest


class AttachmentAdmin(DeletedDateAdmin):
    def get_queryset(self, request):
        return Attachment.all_objects.all()


class ManualRequestAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return ManualRequest.all_objects.all()


admin.site.register(ManualRequest, ManualRequestAdmin)
admin.site.register(Attachment, AttachmentAdmin)
