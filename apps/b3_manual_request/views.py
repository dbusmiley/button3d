import logging
import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from apps.b3_core.models import StlFile
from apps.b3_manual_request.models import Attachment
from apps.catalogue.models.product import Product
from apps.b3_manual_request.forms import ManualRequestFormFactory, \
    ManualRequestForm
from apps.b3_core import utils
from apps.b3_organization.utils import get_current_site

from apps.b3_attachement.views import AbstractAttachmentAjaxView

logger = logging.getLogger(__name__)


@require_http_methods(['GET', 'POST'])
def manual_request(request):
    return handle_request(
        request,
        template_name='b3_manual_request/request_page.html',
        thank_you_template_name='b3_manual_request/thank_you.html')


def handle_request(request, template_name, thank_you_template_name):
    if request.method == 'GET':
        return render(
            request,
            template_name,
            _create_manual_request_context(request))

    if request.method == 'POST':
        post_data = request.POST.copy()
        post_data['user'] = str(request.user.pk) \
            if request.user.is_active else None
        post_data['ip_address'] = utils.get_client_ip(request)
        post_data['country'] = utils.get_country(request)
        post_data['language'] = request.LANGUAGE_CODE
        post_data['site'] = str(get_current_site(request).pk) \
            if get_current_site(request) is not None else None

        if post_data['post_processings'] != '' and \
                post_data['post_processing_colors'] != '':
            post_data['post_processings'] = json.loads(
                post_data['post_processings']
            )
            post_data['post_processing_colors'] = json.loads(
                post_data['post_processing_colors']
            )

        requestform = ManualRequestForm(post_data)
        if requestform.is_valid():
            requestform.save()
            requestform.instance.send_emails()
            return render(request, thank_you_template_name, {})
        else:
            logger.warning(
                'A manual request form had a problem: {0}'.format(
                    requestform.errors))
            return render(
                request,
                template_name,
                _create_manual_request_context(form=requestform))

    raise RuntimeError('Should never reach this code')


def _create_manual_request_context(request=None, form=None):
    if form is None:
        form = ManualRequestFormFactory.make(request)
        form_data = form.initial
    else:
        form_data = form.data

    stl_file = StlFile.objects.filter(uuid=form_data['stl_file']).first() \
        if form_data.get('stl_file', False) \
        else None

    is_scale_defined = False
    product_title = ''
    printability = None
    if stl_file is not None:
        try:
            if form_data.get('measure_unit', False) and \
                    form_data.get('scale', False):
                dimensions = stl_file.get_scaled_dimensions(
                    'o', form_data['scale'])
                unit = form_data['measure_unit']
                is_scale_defined = True
            else:
                unit = stl_file.unit
                dimensions = stl_file.get_scaled_dimensions('o')
                is_scale_defined = True
        except Exception:
            is_scale_defined = False

        if form_data.get('product', False):
            product = Product.objects.get(pk=form_data['product'])
            product_title = product.title
        if product_title != '' and is_scale_defined:
            wall_min = product.attr_wall_min if \
                product.attr_wall_min is not None else \
                product.parent.attr_wall_min if \
                hasattr(product, 'parent') and product.parent is not None \
                else None
            if wall_min is not None:
                printability = stl_file.get_printability_info(
                    wall_min, float(form_data['scale']))["printability"]

    return {
        'form': form,
        'max_file_size_in_mb': Attachment.MAX_SIZE_IN_MB,
        'stl_file': stl_file,
        'stl_file_extension': '.{0}'.format(stl_file.get_extension())
        if stl_file is not None else None,
        'stl_file_config': {
            'size_str': '{1:.2f} {0} x {2:.2f} {0} x {3:.2f} {0}'.format(
                unit, dimensions['h'], dimensions['w'], dimensions['d'])
            if is_scale_defined is True else '',
            'product_title': product_title,
            'printability': printability
        }
    }


class ManualRequestAtachmentAjaxView(AbstractAttachmentAjaxView):
    attachment_class = Attachment
