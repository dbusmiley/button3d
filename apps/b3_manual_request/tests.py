# -*- coding: utf-8 -*-
import json

from django.core import mail
from django.urls import reverse
from django.test import RequestFactory, override_settings


from apps.b3_core.models import Configuration
from apps.b3_manual_request.forms import ManualRequestFormFactory
from apps.b3_manual_request.models import ManualRequest
from apps.b3_tests.factories import ProductFactory, StockRecordFactory, \
    ColorFactory, StlFileFactory, UserFactory, ManualRequestFactory, \
    ManualRequestAttachmentFactory, PostProcessingFactory, \
    ManualRequestPostProcessingOptionFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.testcases.org_testcases import OrganizationTestCase


class ManualRequestFactoryTestCase(TestCase):
    def test_with_hubspot_data(self):
        url = '{0}?hubspot={1}'.format(
            reverse('manual-request'), 'my-hubspot-test-data')
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertEqual(form.initial['hubspot_data'], 'my-hubspot-test-data')
        self.assertEqual(form.initial['type'], ManualRequest.MAKE_OFFER)

    def test_from_step3(self):
        stl_file = StlFileFactory()
        product = ProductFactory()
        stockrecord = StockRecordFactory(product=product)
        color = ColorFactory()
        unit = 'mm'
        scale = '0.9'
        post_processing = PostProcessingFactory(
            stock_record=stockrecord,
            colors=[color]
        )

        url = ('{0}?stl_file={1}&product={2}'
               '&supplier={3}&unit={5}&scale={6}&post_processing={7}')\
            .format(
                reverse('manual-request'), stl_file.uuid, product.slug,
                stockrecord.partner.code, color.pk, unit, scale,
                post_processing.id
        )
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertEqual(form.initial['stl_file'], stl_file.uuid)
        self.assertEqual(form.initial['product'], product.pk)
        self.assertEqual(form.initial['stockrecord'], stockrecord.pk)
        self.assertEqual(form.initial['measure_unit'], unit)
        self.assertEqual(form.initial['scale'], scale)
        self.assertEqual(form.initial['type'], ManualRequest.QUESTION)
        self.assertEqual(
            form.initial['post_processings'],
            json.dumps([str(post_processing.id)])
        )

    def test_compare_page_error(self):
        stl_file = StlFileFactory()

        url = '{0}?stl_file={1}&error=True'.format(
            reverse('manual-request'), stl_file.uuid)
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertEqual(form.initial['stl_file'], stl_file.uuid)
        self.assertEqual(form.initial['type'], ManualRequest.MAKE_PRINTABLE)

    def test_not_printable_error(self):
        stl_file = StlFileFactory()
        product = ProductFactory()
        stockrecord = StockRecordFactory(product=product)
        unit = 'in'
        scale = '1.2'

        url = ('{0}?stl_file={1}&not-printable=True'
               '&product={2}&supplier={3}&unit={4}&scale={5}')\
            .format(
                reverse('manual-request'), stl_file.uuid, product.slug,
                stockrecord.partner.code, unit, scale)
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertEqual(form.initial['stl_file'], stl_file.uuid)
        self.assertEqual(form.initial['product'], product.pk)
        self.assertEqual(form.initial['stockrecord'], stockrecord.pk)
        self.assertEqual(form.initial['measure_unit'], unit)
        self.assertEqual(form.initial['scale'], scale)
        self.assertEqual(form.initial['type'], ManualRequest.MAKE_PRINTABLE)
        self.assertFalse(hasattr(form.initial, 'color'))

    def test_from_basket(self):
        basket = BasketFactory(owner=UserFactory())

        url = '{0}?basket={1}'.format(reverse('manual-request'), basket.pk)
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertEqual(form.initial['basket'], str(basket.pk))
        self.assertEqual(form.initial['type'], ManualRequest.QUESTION)

    def test_from_empty_viewer(self):
        url = reverse('manual-request')
        request = RequestFactory().get(url)
        form = ManualRequestFormFactory.make(request)

        self.assertFalse(hasattr(form.initial, 'hubspot_data'))
        self.assertFalse(hasattr(form.initial, 'basket'))
        self.assertFalse(hasattr(form.initial, 'stl_file'))
        self.assertEqual(form.initial['type'], ManualRequest.MAKE_OFFER)


class ManualRequestEmailTest(OrganizationTestCase):
    @override_settings(EMAIL_CONTACT='support@my.3yd')
    def test_support_email_sent_with_empty_theme(self):
        self.populate_current_request()
        theme = self.organization.theme
        theme.request_receiving_email = ''
        theme.must_send_email_to_user = False
        theme.save()

        mr = ManualRequestFactory()
        mr.send_emails()

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, 'Manual Request')

    @override_settings(EMAIL_CONTACT='')
    def test_no_support_email_sent_empty_email_contact(self):
        self.populate_current_request()
        theme = self.organization.theme
        theme.request_receiving_email = ''
        theme.must_send_email_to_user = False
        theme.save()

        mr = ManualRequestFactory()
        mr.send_emails()

        self.assertEqual(len(mail.outbox), 0)

    @override_settings(EMAIL_CONTACT='support@my.3yd')
    def test_unicode_strings_work(self):
        self.populate_current_request()
        theme = self.organization.theme
        theme.must_send_email_to_user = False
        theme.save()

        mr = ManualRequestFactory(first_name='Ünicödé', last_name='Gùy')
        ManualRequestAttachmentFactory(manual_request=mr, filename='Fïlé')
        mr.send_emails()

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, 'Manual Request')

    @override_settings(EMAIL_CONTACT='support@my.3yd')
    def test_support_email_sent_with_post_processings(self):
        self.populate_current_request()
        theme = self.organization.theme
        theme.request_receiving_email = ''
        theme.must_send_email_to_user = False
        theme.save()

        mr = ManualRequestFactory()

        c1 = ColorFactory(rgb='#000000')
        stockrecord = StockRecordFactory()

        post_processing = PostProcessingFactory(
            stock_record=stockrecord,
            colors=[c1]
        )

        ManualRequestPostProcessingOptionFactory(
            manual_request_id=mr.id,
            post_processing=post_processing,
            color=c1
        )

        mr.send_emails()

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertIn(post_processing.title, str(email.message()))


class ManualRequestOpeningTest(TestCase):
    def test_not_printable_submission(self):
        url = reverse('manual-request')
        sr = StockRecordFactory()
        product = sr.product
        data = {
            'not-printable': True,
            'stl_file': StlFileFactory().uuid,
            'product': product.slug,
            'supplier': sr.partner.code,
            'unit': Configuration.MM,
            'scale': 1
        }
        response = self.client.get(url, data)
        self.assertTrue('Request for {0}'.format(
            self.organization.showname) in response.content.decode('utf-8'))

    def test_erroneous_stlfile_uuid(self):
        url = reverse('manual-request')
        data = {
            'error': True,
            'stl_file': '123456789'
        }
        response = self.client.get(url, data)
        self.assertTrue('Request for {0}'.format(
            self.organization.showname) in response.content.decode('utf-8'))
