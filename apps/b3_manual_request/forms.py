import ast
import json

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget

from apps.b3_core.models import StlFile, Configuration
from apps.b3_manual_request.models import ManualRequest, Attachment
from apps.b3_organization import utils as org_utils
from apps.b3_core.mixins import RequiredFieldsMixin
from apps.catalogue.models.product import Product
from apps.catalogue.models.color import Color
from apps.partner.models import StockRecord
from apps.partner.models import PostProcessing


class ManualRequestFormFactory(object):
    @staticmethod
    def make(request):
        user = request.user if \
            hasattr(request, 'user') and request.user.is_active else None
        org = org_utils.get_current_org()
        arguments = {
            'referer_url': request.META['HTTP_REFERER']
            if 'HTTP_REFERER' in request.META else None,
            'email': user.email if user else None,
            'first_name': user.first_name if user else None,
            'last_name': user.last_name if user else None,
            'company': None,
            'type': ManualRequest.MAKE_OFFER
        }
        try:
            arguments['phone_number'] = \
                org.get_default_shipping_address().phone_number
        except AttributeError:
            arguments['phone_number'] = None

        if 'post_processing' in request.GET:
            arguments['post_processings'] = json.dumps(
                request.GET.getlist('post_processing')
            )

        if 'post_processing_color' in request.GET:
            arguments['post_processing_colors'] = json.dumps(
                request.GET.getlist('post_processing_color')
            )

        if 'hubspot' in request.GET:
            arguments['type'] = ManualRequest.MAKE_OFFER
            arguments['hubspot_data'] = request.GET['hubspot']

        elif 'stl_file' in request.GET and 'error' in request.GET:
            arguments['type'] = ManualRequest.MAKE_PRINTABLE
            arguments['stl_file'] = request.GET['stl_file']

        elif 'stl_file' in request.GET and 'not-printable' in request.GET\
                and 'scale' in request.GET and 'unit' in request.GET:
            arguments['type'] = ManualRequest.MAKE_PRINTABLE
            arguments['stl_file'] = request.GET['stl_file']
            arguments['scale'] = request.GET['scale']
            arguments['measure_unit'] = request.GET['unit']
            arguments['product'] = Product.objects.get(
                slug=request.GET['product']).pk
            arguments['stockrecord'] = StockRecord.objects.filter(
                partner__code=request.GET['supplier'],
                product=arguments['product']).first().pk
            arguments['color'] = request.GET.get('color', None)

        elif 'stl_file' in request.GET and \
                'product' in request.GET and \
                'supplier' in request.GET and \
                'scale' in request.GET and \
                'unit' in request.GET:
            arguments['type'] = ManualRequest.QUESTION
            arguments['stl_file'] = request.GET['stl_file']
            arguments['scale'] = request.GET['scale']
            arguments['measure_unit'] = request.GET['unit']
            arguments['product'] = Product.objects.get(
                slug=request.GET['product']).pk
            arguments['stockrecord'] = StockRecord.objects.filter(
                partner__code=request.GET['supplier'],
                product=arguments['product']).first().pk
            arguments['color'] = request.GET.get('color', None)

        elif 'basket' in request.GET:
            arguments['type'] = ManualRequest.QUESTION
            arguments['basket'] = request.GET['basket']

        return ManualRequestForm(initial=arguments)


class ManualRequestForm(RequiredFieldsMixin, forms.ModelForm):
    stl_file = forms.CharField(widget=forms.HiddenInput(), required=False)
    attachment_ids = forms.CharField(
        max_length=32, widget=forms.HiddenInput(), required=False)
    scale = forms.FloatField(widget=forms.HiddenInput(), required=False)
    measure_unit = forms.CharField(
        max_length=2, widget=forms.HiddenInput(), required=False)
    post_processings = forms.MultipleChoiceField(
        widget=forms.HiddenInput(),
        choices=[],
        required=False
    )
    post_processing_colors = forms.MultipleChoiceField(
        widget=forms.HiddenInput(),
        choices=[],
        required=False
    )

    class Meta:
        model = ManualRequest
        fields = '__all__'
        widgets = {
            # Set values when opening form, but hidden
            'referer_url': forms.HiddenInput(),
            'basket': forms.HiddenInput(),
            'hubspot_data': forms.HiddenInput(),
            'product': forms.HiddenInput(),
            'stockrecord': forms.HiddenInput(),
            'configuration': forms.HiddenInput(),

            'message': forms.Textarea(),

            # Values that will be redefined when posted
            'ip_address': forms.HiddenInput(),
            'country': forms.HiddenInput(),
            'language': forms.HiddenInput(),
            'user': forms.HiddenInput(),
            'site': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['post_processings'].choices = \
            PostProcessing.objects.all().values_list('slug', 'stock_record_id')
        self.fields['post_processing_colors'].choices = \
            Color.objects.all().values_list('id', 'title_en')
        self.fields['post_processing_colors'].choices.insert(
            0, ('', "Empty choice")
        )
        if settings.RECAPTCHA_ENABLED:
            self.fields['captcha'] = ReCaptchaField(widget=ReCaptchaWidget())

    def clean_stl_file(self):
        not_found_error = _("The stl file could not be found.")

        if 'stl_file' not in self.cleaned_data:
            raise forms.ValidationError(not_found_error)

        uuid = self.cleaned_data['stl_file']

        if uuid is None or uuid == '':
            return None

        filtered_stl_file = StlFile.objects.filter(uuid=uuid)
        if not filtered_stl_file.exists():
            raise forms.ValidationError(not_found_error)

        return filtered_stl_file.first()

    def clean_attachment_ids(self):
        invalid_attachment_error = _("An attachment is invalid.")
        enter_list = _("Please enter a valid list for attachments.")

        if self.cleaned_data['attachment_ids'] is None or \
                self.cleaned_data['attachment_ids'] == '':
            return None

        try:
            ids = ast.literal_eval(self.cleaned_data['attachment_ids'])
            if Attachment.objects.filter(
                    pk__in=ids, manual_request=None).count() != len(ids):
                raise forms.ValidationError(invalid_attachment_error)
        except BaseException:
            raise forms.ValidationError(enter_list)

        return self.cleaned_data['attachment_ids']

    def clean(self):
        cleaned_data = super(ManualRequestForm, self).clean()
        scale = cleaned_data.get('scale', None)
        unit = cleaned_data.get('measure_unit', None)
        if scale and unit:
            if cleaned_data['configuration']:
                cleaned_data['configuration'].update(scale=scale, unit=unit)
            else:
                cleaned_data['configuration'] = \
                    Configuration.objects.create(scale=scale, unit=unit)
        return cleaned_data

    def save(self, *args, **kwargs):
        super(ManualRequestForm, self).save(*args, **kwargs)

        stockrecord = self.cleaned_data['stockrecord']
        for slug, color_id in zip(self.cleaned_data['post_processings'],
                                  self.cleaned_data['post_processing_colors']):
            try:
                post_processing = stockrecord.post_processings.get(slug=slug)
                color = Color.objects.get(id=color_id)
            except (Color.DoesNotExist, ValueError):
                color = None
            except PostProcessing.DoesNotExist:
                continue
            self.instance.post_processing_options.create(
                color=color,
                post_processing=post_processing
            )

        if self.cleaned_data['attachment_ids'] is not None:
            for attachment in Attachment.objects.filter(
                    pk__in=ast.literal_eval(
                        self.cleaned_data['attachment_ids']
                    )):
                attachment.manual_request = self.instance
                attachment.save()
