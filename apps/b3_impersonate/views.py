from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.shortcuts import render, redirect

from apps.b3_impersonate.view_decorators import \
    check_user_access_to_impersonate


@check_user_access_to_impersonate
def site_list(request):
    sites = Site.objects.all()

    return render(
        request,
        'b3_impersonate/site_list.html',
        {
            'sites': sites,
        }
    )


@check_user_access_to_impersonate
def user_list(request, site_id):
    users = User.objects.filter(userprofile__site=site_id) \
        .order_by('-is_superuser')
    site = Site.objects.get(id=site_id)

    return render(
        request,
        'b3_impersonate/user_list.html',
        {
            'users': users,
            'site': site,
        }
    )


@check_user_access_to_impersonate
def impersonate(request, user_id):
    user = User.objects.get(id=user_id)
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    auth.login(request, user)

    return redirect('/')
