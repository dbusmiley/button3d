from apps.b3_order.models import Order

from django import template


register = template.Library()


@register.simple_tag
def order_count(site):
    return Order.all_objects.filter(site=site).count()
