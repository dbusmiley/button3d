from django.conf.urls import url

from apps.b3_impersonate.views import site_list, user_list, impersonate

urlpatterns = [
    url(r'^$', site_list, name='index'),
    url(r'^users/(?P<site_id>\d+)/$', user_list, name='user_list'),
    url(r'^impersonate/(?P<user_id>\d+)/$',
        impersonate,
        name='impersonate'),
]
