import typing as t

from django.http import Http404

import button3d.type_declarations as td

from functools import wraps

from django.conf import settings

from apps.b3_impersonate.permissions import \
    user_is_superuser_and_can_impersonate

try:
    IMPERSONATE_ENABLED = settings.IMPERSONATE_ENABLED
except AttributeError:
    IMPERSONATE_ENABLED = False


def check_user_access_to_impersonate(view_func: t.Callable):
    """
    Allows access if DEBUG is active or when user is superuser and has
    explicit permission to access impersonate functionality
    """

    @wraps(view_func)
    def wrapper(request: td.Request, *args, **kwargs):
        if settings.DEBUG \
                or (IMPERSONATE_ENABLED
                    and user_is_superuser_and_can_impersonate(request.user)):
            return view_func(request, *args, **kwargs)
        raise Http404

    return wrapper
