import typing as t
import button3d.type_declarations as td

from django.contrib.sites.models import Site
from django.urls import reverse


def get_impersonate_page_url(request: td.Request) -> t.Optional[str]:
    """
    Returns url for /impersonate/ page using domain of random Site from the DB,
    that has Organization
    """

    site = Site.objects \
        .filter(organization__isnull=False) \
        .order_by('?') \
        .first()
    if site:
        domain = site.domain
        impersonate_url = reverse('impersonate:index')
        url = f'//{domain}:{request.get_port()}{impersonate_url}'
        return url
