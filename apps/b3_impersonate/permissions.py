import button3d.type_declarations as td
from apps.b3_impersonate.const import CAN_IMPERSONATE_PERMISSION_CODENAME


def user_is_superuser_and_can_impersonate(user: td.User) -> bool:
    return user.is_authenticated() \
        and user.is_superuser \
        and user.has_perm(CAN_IMPERSONATE_PERMISSION_CODENAME)
