from apps.b3_compare.api_v1_views.base import APIEndpointView, PricesView
from apps.b3_core.beautify import pretty_times
from apps.partner.templatetags.partner_tags import get_partner_logo_tag


class SuppliersView(APIEndpointView):
    required_params = ['country']
    optional_params = {'lang': 'en'}

    @property
    def partners(self):
        return self.organization.partners_enabled.exclude(address=None)

    def get(self, request, *args, **kwargs):
        results = [
            {
                'id': partner.id,
                'slug': partner.code,
                'title': partner.name,
                'city': partner.address.city,
                'country': partner.address.country.name,
                'website': partner.website,
                'address': partner.address.line1,
                'postcode': partner.address.zip_code,
                'payment_methods': self.get_payment_methods(partner),
                'rating_quality': partner.org_options.rating_quality,
                'rating_reliability': partner.org_options.rating_reliability,
                'rating_service': partner.org_options.rating_service,
                'rating_total': partner.org_options.rating_total,
                'is_forwarding': False,
                'email': partner.email,
                'phone': self.get_phone_number(partner),
                'logo_tag': get_partner_logo_tag(partner),
                'logo_url': self.get_logo_url(partner),
                'certifications': partner.get_certifications_url(
                    request.LANGUAGE_CODE
                ),
                'vat_rate': partner.vat_rate
            }
            for partner in self.partners
        ]
        return self.render_json_response({"results": results})

    def get_payment_methods(self, partner):
        payment_methods = partner.get_payment_methods()
        user_can_always_use_invoice = self.request.user.has_perm(
            'b3_core.invoice_for_all_ps_enabled'
        )
        if 'invoice' not in payment_methods and user_can_always_use_invoice:
            payment_methods.append('invoice')

        return payment_methods

    @staticmethod
    def get_phone_number(partner):
        if partner.address and partner.address.phone_number:
            return str(partner.address.phone_number)
        return None

    @staticmethod
    def get_logo_url(partner):
        if partner.logo and partner.logo.url:
            return partner.logo.url
        return None


class SupplierPricesView(PricesView):
    required_params = ['uuid', 'country', 'material']
    optional_params = {
        'scale': 1.0,
        'currency': 'EUR',
        'lang': 'en'
    }

    def get(self, request, *args, **kwargs):
        results = []

        all_stock_records, priced_stock_records = \
            self.get_priced_stock_records(self.stock_records)

        if self.organization.is_single_supplier_account:
            # Only the cheapest StockRecord for this material is returned
            # by the API if the printing service selection is hidden.
            if priced_stock_records:
                priced_stock_records = priced_stock_records[:1]
                all_stock_records = [priced_stock_records[0].stock_record]
            else:
                all_stock_records = all_stock_records[:1]

        price_dict = dict(priced_stock_records)
        for stock_record in all_stock_records:
            results.append(
                self.serialize_stock_record(
                    stock_record,
                    price_dict.get(stock_record)  # None instead of KeyError
                )
            )

        return self.render_json_response({'results': results})

    @property
    def stock_records(self):
        country_code = self.params['country']
        partners_enabled = self.organization.partners_enabled.all()
        return self._get_stock_records(country_code, partners_enabled)

    def _get_stock_records(self, country_code, partners_enabled):
        return self.get_relevant_stock_records_for_new_checkout_queryset(
            country_code, partners_enabled
        ).filter(product__slug=self.params['material'])\
            .distinct().select_related(
            'product', 'partner'
        ).prefetch_related(
            'post_processings',
            'partner__shipping_methods',
            'partner__shipping_methods__countries'
        )

    def serialize_stock_record(self, stock_record, price):
        delivery_min, delivery_max = self.get_fastest_delivery_time(
            stock_record
        )
        delivery_range = pretty_times([delivery_min, delivery_max])

        manual_pricing_required = price is None
        printability_status = self.get_printability_info(stock_record.product)

        colorable_post_processings_available = any(
            bool(post_processing.colors.all())
            for post_processing in stock_record.post_processings.all()
        )

        result = {
            'slug': '{0}-raw'.format(stock_record.product.slug),
            'supplier': stock_record.partner.code,
            'price': price.as_dict() if price else None,
            'manual_pricing_required': manual_pricing_required,
            'delivery_days_range': delivery_range,
            'delivery_min_days': delivery_min,
            'delivery_max_days': delivery_max,
            'shipping_methods': self.get_shipping_method_dicts(stock_record),
            'datasheet': stock_record.datasheet_url(self.params['lang']),
            'support': self.get_support_info_dict(stock_record),
            'has_color_finishes': colorable_post_processings_available,
            'finishes': self.get_post_processing_slugs(stock_record),
            'combinable_post_processings':
                stock_record.combinable_post_processings
        }
        result.update(printability_status)

        if 'error' not in printability_status:
            result['basket_url'] = self.create_basket_url(stock_record)

        return result
