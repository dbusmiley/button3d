import logging
from collections import defaultdict

from apps.b3_compare.api_v1_views.base import PricesView, APIEndpointView
from apps.b3_core.beautify import pretty_times
from apps.b3_core.utils import cache_result_on_instance
from apps.catalogue.models.product import Product
from apps.partner.models import StockRecord

logger = logging.getLogger(__name__)


class MaterialsView(APIEndpointView):
    required_params = []
    optional_params = {'lang': 'en'}

    @property
    def materials(self):
        partners_enabled = self.organization.partners_enabled.all()
        return Product.all_objects.filter(
            stockrecords__partner__in=partners_enabled,
            stockrecords__edit_status=StockRecord.PUBLISHED,
            stockrecords__deleted_date=None
        ).distinct()

    @property
    @cache_result_on_instance
    def post_processing_slugs(self):
        stock_records = StockRecord.objects.all().prefetch_related(
            'post_processings'
        ).select_related('product')

        post_processing_slugs = defaultdict(set)
        for stock_record in stock_records:
            for post_processing in stock_record.post_processings.all():
                post_processing_slugs[stock_record.product].add(
                    post_processing.full_slug
                )
        return post_processing_slugs

    def get(self, request, *args, **kwargs):
        results = []

        for m in self.materials:
            sub_category = m.category
            if sub_category is None:
                logger.error(f'Material with no subcategory: {m.pk}')
                continue
            category = sub_category.get_parent()
            if not category:
                logger.error(f'Material with category with no parent '
                             f'category: {m.pk}')
                continue

            post_processing_slugs = [f'{m.slug}-raw'] + [
                slug for slug in self.post_processing_slugs.get(m, [])
            ]
            results.append({
                "slug": m.slug,
                "title": m.full_product_title,
                "description": m.org_options.description,
                "category": category.name,
                "sub-category": sub_category.name,
                "is_multicolor": m.org_options.is_multicolor,
                "is_acid_resistant": m.org_options.is_acid_resistant,
                "is_corrosion_resistant": m.org_options.is_corrosion_resistant,
                "is_toxicity_resistant": m.org_options.is_toxicity_resistant,
                "is_uv_resistant": m.org_options.is_uv_resistant,
                "is_bio_compatible": m.org_options.is_bio_compatible,
                "finishes": post_processing_slugs,
                "attr_strength": m.org_options.attr_strength,
                "attr_detail": m.org_options.attr_detail,
                "technology": m.technology.title if m.technology else '',
                "attr_wall_min": m.org_options.attr_wall_min,
                "attr_wall_opt": m.org_options.attr_wall_opt,
                "attr_density_min": m.org_options.attr_density_min,
                "attr_density_max": m.org_options.attr_density_max,
                "attr_ultimate_tensile_strength_min":
                    m.org_options.attr_ultimate_tensile_strength_min,
                "attr_ultimate_tensile_strength_max":
                    m.org_options.attr_ultimate_tensile_strength_max,
                "attr_modulus_min": m.org_options.attr_modulus_min,
                "attr_modulus_max": m.org_options.attr_modulus_max,
                "attr_elongation_min": m.org_options.attr_elongation_min,
                "attr_elongation_max": m.org_options.attr_elongation_max,
                "attr_hardness_unit": m.org_options.attr_hardness_unit,
                "attr_hardness_min": m.org_options.attr_hardness_min,
                "attr_hardness_max": m.org_options.attr_hardness_max,
                "attr_flexural_strength_min":
                    m.org_options.attr_flexural_strength_min,
                "attr_flexural_strength_max":
                    m.org_options.attr_flexural_strength_max,
                "attr_flexural_modulus_min":
                    m.org_options.attr_flexural_modulus_min,
                "attr_flexural_modulus_max":
                    m.org_options.attr_flexural_modulus_max,
                "attr_hardness_shore_scale":
                    m.org_options.attr_hardness_shore_scale,
                "attr_hdt_min": m.org_options.attr_hdt_min,
                "attr_hdt_max": m.org_options.attr_hdt_max,
                "attr_glass_transition_min":
                    m.org_options.attr_glass_transition_min,
                "attr_glass_transition_max":
                    m.org_options.attr_glass_transition_max,
                "attr_part_density": m.org_options.attr_part_density,
                "attr_flammability": m.org_options.attr_flammability,
                "attr_usp_class_vi_certified":
                    m.org_options.attr_usp_class_vi_certified,
                "attr_yield_strength_max":
                    m.org_options.attr_yield_strength_max,
                "attr_yield_strength_min":
                    m.org_options.attr_yield_strength_min,
                "tradename": m.org_options.tradename if
                m.org_options.tradename else '',

                "score_density": m.score_density,
                "score_strength": m.score_strength,
                "score_detail": m.org_options.score_detail if
                m.org_options.score_detail else 1
            })

        return self.render_json_response({"results": results})


class MaterialPricesView(PricesView):
    required_params = ['uuid', 'country']
    optional_params = {
        'scale': 1.0,
        'currency': 'EUR',
        'supplier': None,
        'lang': 'en'
    }

    def get(self, request, *args, **kwargs):
        results = []
        for material, stock_records in self.materials.items():
            all_stock_records, priced_stock_records = \
                self.get_priced_stock_records(stock_records)
            if not all_stock_records:
                # No pricing possible for any of the StockRecords
                continue

            if self.organization.is_single_supplier_account:
                # Only the cheapest StockRecord for each material is returned
                # by the API if the printing service selection is hidden.
                if priced_stock_records:
                    priced_stock_records = priced_stock_records[:1]
                    all_stock_records = [priced_stock_records[0].stock_record]
                else:
                    all_stock_records = all_stock_records[:1]

            results.append(
                self.serialize_material(
                    material,
                    all_stock_records,
                    priced_stock_records
                )
            )

        return self.render_json_response({'results': results})

    @property
    def materials(self):
        country_code = self.params['country']
        partners_enabled = self.organization.partners_enabled.all()

        stock_records = self._get_stock_records(
            country_code,
            partners_enabled
        )

        if self.params['supplier']:
            stock_records = stock_records.filter(
                partner__code=self.params['supplier']
            )

        materials = defaultdict(list)
        for stock_record in stock_records:
            materials[stock_record.product].append(stock_record)
        return materials

    def _get_stock_records(self, country_code, partners_enabled):
        return self.get_relevant_stock_records_for_new_checkout_queryset(
            country_code, partners_enabled
        ).distinct().select_related(
            'product', 'partner'
        ).prefetch_related(
            'post_processings',
            'post_processings__colors',
            'partner__shipping_methods',
            'partner__shipping_methods__countries'
        )

    def serialize_material(
        self, material, stock_records, priced_stock_records
    ):
        manual_pricing_required = not priced_stock_records
        if manual_pricing_required:
            min_price_stock_record = stock_records[0]
            min_price, max_price = None, None
        else:
            # At least one StockRecord can be priced automatically
            min_price_stock_record, min_price = priced_stock_records[0]
            max_price = priced_stock_records[-1].price

        delivery_min, delivery_max = \
            self.get_fastest_delivery_time_among_stock_records(stock_records)
        delivery_days_range = pretty_times([delivery_min, delivery_max])

        amount_stock_records, amount_display = self.get_amount_stock_records(
            stock_records
        )

        colorable_post_processings_available = any(
            bool(post_processing.colors.all())
            for stock_record in stock_records
            for post_processing in stock_record.post_processings.all()
        )

        printability_info = self.get_printability_info(material)

        result = {
            'slug': material.slug,
            'popularity': material.popularity,
            'delivery_days_range': delivery_days_range,
            'delivery_min_days': delivery_min,
            'delivery_max_days': delivery_max,
            'min_price': min_price.as_dict() if min_price else None,
            'max_price': max_price.as_dict() if max_price else None,
            'has_color_finishes': colorable_post_processings_available,
            'amount_stockrecords': amount_stock_records,
            'amount_stockrecords_text': amount_display,
            'manual_pricing_required': manual_pricing_required,
            'suppliers': self.get_supplier_slugs(stock_records),
            'finishes': self.get_post_processing_slugs(min_price_stock_record),
            'has_stockrecords': True,
            'is_approximated': False
        }
        result.update(printability_info)

        if 'error' not in printability_info:
            result['basket_url'] = self.create_basket_url(
                min_price_stock_record
            )

        return result
