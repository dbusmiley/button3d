# flake8: noqa
from apps.b3_compare.api_v1_views.various import ColorsView, UploadView, \
    add_to_basket_view, get_custom_price_view

from apps.b3_compare.api_v1_views import materials, suppliers, finishes
