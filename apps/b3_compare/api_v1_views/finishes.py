from collections import namedtuple

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from apps.b3_compare.api_v1_views.base import APIEndpointView, PricesView
from apps.b3_core.beautify import pretty_times
from apps.b3_core.utils import ManualPricingRequiredException, \
    NoPricingPossibleException
from apps.partner.models import PostProcessing, StockRecord


class FinishesView(APIEndpointView):
    required_params = []
    optional_params = {'lang': 'en'}

    @property
    def post_processings(self):
        partners_enabled = self.organization.partners_enabled.all()
        return PostProcessing.objects.published().filter(
            stock_record__partner__in=partners_enabled,
        ).select_related(
            'stock_record',
            'stock_record__product',
            'stock_record__partner'
        )

    def get(self, request, *args, **kwargs):
        results = []
        for post_processing in self.post_processings:
            results.append({
                'slug': post_processing.full_slug,
                'title': post_processing.title,
                'description': post_processing.description,
                'material': post_processing.stock_record.product.slug,
                'partner': post_processing.stock_record.partner.code
            })

        return self.render_json_response({'results': results})


class FinishesPricesView(PricesView):
    required_params = ['uuid', 'country', 'material', 'supplier']
    optional_params = {
        'scale': 1.0,
        'currency': 'EUR',
        'color_id': None,
        'lang': 'en'
    }

    def get(self, request, *args, **kwargs):
        results = []
        for post_processing in self.post_processings:
            try:
                price = self.get_price(
                    post_processing.stock_record,
                    post_processings=[post_processing]
                )
            except ManualPricingRequiredException:
                price = None
            except NoPricingPossibleException:
                continue

            results.append(
                self.serialize_post_processing(post_processing, price)
            )

        return self.render_json_response({'results': results})

    @property
    def post_processings(self):
        country_code = self.params['country']
        partners_enabled = self.organization.partners_enabled.all()

        post_processings = self._get_post_processings(
            country_code,
            partners_enabled
        )

        # Due to backwards compatibility, the raw PostProcessing
        # (which is basically the StockRecord without any PostProcessings),
        # needs to be returned by the API too.
        try:
            return [self.raw_post_processing] + list(post_processings)
        except ObjectDoesNotExist:
            pass
        return post_processings

    def _get_post_processings(self, country_code, partners_enabled):
        return PostProcessing.objects.published().filter(
            Q(stock_record__partner__shipping_methods__countries__alpha2=country_code,  # noqa: E501
            stock_record__partner__shipping_methods__deleted_date=None,) |
            Q(stock_record__partner__pickuplocation__isnull=False,
              stock_record__partner__pickuplocation__deleted_date=None),
            stock_record__partner__in=partners_enabled,
            stock_record__partner__code=self.params['supplier'],
            stock_record__product__slug=self.params['material'],

        ).distinct().select_related(
            'stock_record', 'stock_record__product', 'stock_record__partner'
        ).prefetch_related(
            'colors',
            'stock_record__partner__shipping_methods',
            'stock_record__partner__shipping_methods__countries'
        )

    @property
    def raw_post_processing(self):
        partners_enabled = self.organization.partners_enabled.all()
        country_code = self.params['country']
        stock_record = self._get_stock_record(
            country_code,
            partners_enabled
        )
        return FakeRawPostProcessing(stock_record)

    def _get_stock_record(self, country_code, partners_enabled):
        return StockRecord.objects.select_related(
            'product', 'partner'
        ).prefetch_related(
            'partner__shipping_methods',
            'partner__shipping_methods__countries'
        ).distinct().get(
            edit_status=StockRecord.PUBLISHED,
            partner__shipping_methods__countries__alpha2=country_code,
            partner__shipping_methods__deleted_date=None,
            product__slug=self.params['material'],
            partner__code=self.params['supplier'],
            partner__in=partners_enabled
        )

    def serialize_post_processing(self, post_processing, price):
        stock_record = post_processing.stock_record
        manual_pricing_required = price is None

        delivery_min, delivery_max = self.get_fastest_delivery_time(
            stock_record, post_processings=[post_processing]
        )
        delivery_range = pretty_times([delivery_min, delivery_max])
        shipping_method_dicts = self.get_shipping_method_dicts(
            stock_record, post_processings=[post_processing]
        )

        printability_status = self.get_printability_info(stock_record.product)
        color_ids = [color.id for color in post_processing.colors.all()]

        result = {
            'slug': post_processing.full_slug,
            'supplier': stock_record.partner.code,
            'price': price.as_dict() if price else None,
            'manual_pricing_required': manual_pricing_required,
            'delivery_days_range': delivery_range,
            'delivery_min_days': delivery_min,
            'delivery_max_days': delivery_max,
            'shipping_methods': shipping_method_dicts,
            'datasheet': post_processing.datasheet_url(self.params['lang']),
            'support': self.get_support_info_dict(stock_record),
            'color_ids': color_ids
        }
        result.update(printability_status)

        param_color_id = self.params['color_id']
        if param_color_id and param_color_id not in color_ids:
            result['error'] = "E_COLOR_UNAVAILABLE"

        if 'error' not in result:
            result['basket_url'] = self.create_basket_url(
                stock_record, post_processing
            )
        return result


class FakeRawPostProcessing(object):
    """
    This class is used by to generate 'raw' post-processing, i.e.
    something that behaves like a PostProcessing instance, but represents the
    absence of any post-processing. The reason for this approach is to
    keep backwards-compatibility in FinishesPricesView.
    """
    colors = namedtuple('Colors', ['all'])(lambda: [])
    slug = 'raw'
    production_days_min = 0
    production_days_max = 0
    price_formula = '0'
    always_priced_manually = False

    def __init__(self, stock_record):
        super(FakeRawPostProcessing, self).__init__()
        self.stock_record = stock_record

    @property
    def full_slug(self):
        return '{0}-{1}'.format(self.stock_record.product.slug, self.slug)

    def datasheet_url(self, lang=None):
        return self.stock_record.datasheet_url(lang)

    def fits_in_bounds(self, stl_file, scale=1.0):
        return self.stock_record.fits_in_bounds(stl_file, scale)
