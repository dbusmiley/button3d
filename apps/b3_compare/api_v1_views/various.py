from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from apps.b3_compare.api_v1_views.base import APIEndpointView
from apps.b3_compare.utils import StockRecordPriceTester, \
    AnalyzePendingException
from apps.b3_core.exceptions import JSONBadRequest
from apps.b3_core.factories import ErrorResponseFactory
from apps.b3_core.models import StlFile, Configuration
from apps.b3_core.utils import PriceEvaluationError, \
    ManualPricingRequiredException, NoPricingPossibleException
from apps.catalogue.models.color import Color
from apps.partner.models import StockRecord

errorresponse_factory = ErrorResponseFactory('DJANGO_API')


class UploadView(APIEndpointView):
    required_params = []
    optional_params = {'lang': 'en'}

    def init_default_params(self):
        super(UploadView, self).init_default_params()
        if 'uuid' not in self.kwargs:
            raise JSONBadRequest('Value for key uuid is missing')
        self.uuid = self.kwargs['uuid']
        try:
            self.stl_file = StlFile.all_objects.get(uuid=self.uuid)
        except StlFile.DoesNotExist:
            raise Http404('StlFile does not exist')
        if self.stl_file.is_deleted and not self.stl_file.is_demo():
            raise Http404('StlFile is deleted')

    def get(self, request, *args, **kwargs):
        result = self.stl_file.get_job_status()

        result.update({
            'uuid ': self.stl_file.uuid,
            'creation_date': self.stl_file.creation_date,
            'name': self.stl_file.showname,
            'share_url': self.stl_file.share_url,
            'is_multicolor': self.stl_file.multicolor,
            'thumbnail_url': self.stl_file.thumbnail_url,
            'scale': 1,
            'unit': Configuration.MM
        })

        parameter = self.stl_file.get_parameter()

        result['parameter'] = {
            "max_scale": parameter.max_scale,
            "volume": parameter.volume,
            "d": parameter.d,
            "w": parameter.w,
            "h": parameter.h,
            "area": parameter.area,
            "faces": parameter.faces,
            "shells": parameter.shells,
            "holes": parameter.holes,
        } if parameter else None

        return self.render_json_response(result)


class ColorsView(APIEndpointView):
    required_params = []
    optional_params = {'lang': 'en'}

    def get(self, request, *args, **kwargs):
        colors = list(Color.objects.values('id', 'title', 'rgb'))
        return self.render_json_response({"results": colors})


def add_to_basket_view(request):
    uuid = request.GET['uuid']
    supplier = request.GET['supplier']
    finishing = request.GET['finishing']
    scale = request.GET['scale']
    unit = request.GET.get('unit', Configuration.MM)
    color_id = request.GET.get('color_id')
    url = reverse('generic_upload', kwargs={"uuid": uuid})

    return render(request, 'b3_compare/compare/add-to-basket.html', {
        "partner_slug": supplier,
        "material_slug": finishing,
        "scale": scale,
        "unit": unit,
        "color_id": color_id,
        "url": url,
    })


def get_custom_price_view(request):
    stlfile = get_object_or_404(
        StlFile,
        uuid=request.GET.get('uuid', None)
    )
    stockrecord = get_object_or_404(
        StockRecord,
        id=request.GET.get('stockrecord_id', None)
    )
    price_tester = StockRecordPriceTester(stockrecord)
    price_tester.set_custom_values(
        price_formula=request.GET.get('price_formula', None),
        support_enabled=request.GET.get('support_enabled', None),
        support_formula=request.GET.get('support_formula', None),
        support_angle=request.GET.get('support_angle', None),
        support_offset=request.GET.get('support_offset', None),
        orientation_formula=request.GET.get('orientation_formula', None)
    )

    try:
        price_info = price_tester.get_price(
            stlfile,
            quantity=request.GET.get('quantity', 1),
            scale=request.GET.get('scale', 1)
        )
        return JsonResponse(price_info)
    except PriceEvaluationError as ex:
        return errorresponse_factory.make_badrequest(
            error_code='PRICE_FORMULA_INVALID',
            message='Error while evaluating the price. '
                    'Make sure the formula is correct.',
            error_message=str(ex),
            formula=ex.formula_name
        )
    except AnalyzePendingException:
        return errorresponse_factory.make_badrequest(
            error_code='ANALYZE_PENDING',
            message='The file is still being analyzed. '
                    'Please wait a moment and retry'
        )
    except ManualPricingRequiredException:
        return errorresponse_factory.make_badrequest(
            error_code='MANUAL_PRICING_REQUIRED',
            message='Manual Pricing is required for this item'
        )
    except NoPricingPossibleException:
        return errorresponse_factory.make_badrequest(
            error_code='PRICE_EVALUATION_ERROR',
            message='An unexpected error ocurred '
            'while evaluating the price.'
        )
