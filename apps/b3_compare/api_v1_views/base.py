import logging
import operator
from collections import namedtuple, defaultdict

from braces.views import JSONResponseMixin
from django.conf import settings
from django.db.models import Q
from django.middleware.csrf import CsrfViewMiddleware
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import translation
from django.utils.http import urlencode
from django.views.generic.base import View

from apps.b3_core.beautify import pretty_times
from apps.b3_core.exceptions import JSONBadRequest
from apps.b3_core.models import StlFile, SupportStructure
from apps.b3_core.utils import cache_result_on_instance, \
    MissingParameterException, NoPricingPossibleException, \
    ManualPricingRequiredException
from apps.b3_organization.models import Organization
from apps.b3_organization.utils import get_base_url, set_current_site
from apps.partner.models import StockRecord
from apps.b3_shipping.models import ShippingMethod
from apps.partner.pricing.calculators import PartPriceCalculator
from apps.partner.pricing.price import Price

logger = logging.getLogger(__name__)


class APIEndpointView(JSONResponseMixin, View):
    content_type = 'application/json'
    http_method_names = ('get',)
    json_dumps_kwargs = {'indent': 2}
    organization = None
    params = {}
    required_params = []
    optional_params = {}

    def init_default_params(self):
        self.organization = self.get_organization_from_request()
        set_current_site(self.organization.site)

        params = {}
        for key, value in self.request.GET.items():
            if key not in self.required_params and \
                    key not in self.optional_params and \
                    key not in ["domain"]:
                raise JSONBadRequest(
                    "Key '%s' is not supported in this request" % key)

        for key in self.required_params:
            value = self.request.GET.get(key)
            if not value:
                raise JSONBadRequest("Value for key '%s' is missing" % key)
            params[key] = value

        for key, default in self.optional_params.items():
            value = self.request.GET.get(key, default)
            params[key] = value

        if 'lang' in params:
            if params['lang'] in dict(settings.LANGUAGES):
                lang = params['lang']
                translation.activate(language=lang)
                self.request.LANGUAGE_CODE = lang
            else:
                raise JSONBadRequest(
                    ("Language '%s' is not supported. "
                     "Supported languages are '%s'")
                    % (params['lang'],
                       "', '".join(dict(settings.LANGUAGES).keys())))

        if 'scale' in params:
            params['scale'] = float(params['scale'])

        if 'color_id' in params:
            if params['color_id']:
                params['color_id'] = int(params['color_id'])

        if 'uuid' in params:
            self.uuid = params['uuid']
            self.stl_file = get_object_or_404(
                StlFile.all_objects, uuid=self.uuid)
            self.stl_file.clean()

        self.params = params

    def get_organization_from_request(self):
        authorization_value = self.request.META.get('HTTP_AUTHORIZATION', None)
        if authorization_value is not None:
            try:
                label, api_key = authorization_value.strip().split(' ')
            except ValueError:
                raise JSONBadRequest('Unable to authorize.')

            if label != 'ApiKey':
                raise JSONBadRequest('Unable to authorize.')

            try:
                return Organization.objects.get(
                    private_api_key=api_key
                )
            except Organization.DoesNotExist:
                raise JSONBadRequest('Unable to authorize.')

        if CsrfViewMiddleware().process_view(self.request, None, (), {}):
            raise JSONBadRequest('Unable to authorize.')
        if 'domain' not in self.request.GET:
            raise JSONBadRequest('Unable to authorize.')

        """Right now you can bypass ApiKey Authorization by
        just providing an "domain" parameter.
        This parameter is not documented in the public available
        documentation. (developers.3yourmind.com)
        If somebody knows an easy solution how we can check if the
        request is coming from our own site, please tell Felix.
        CSRF validation is only working for POST requests. (GET
        requests are allowed)
        """
        try:
            organization = Organization.objects.prefetch_related(
                'partners_enabled'
            ).get(
                site__domain=self.request.GET['domain']
            )
        except Organization.DoesNotExist:
            raise JSONBadRequest("Unable to authorize.")
        return organization

    def dispatch(self, request, *args, **kwargs):
        self.init_default_params()
        return super(APIEndpointView, self).dispatch(request, *args, **kwargs)


PricedStockRecord = namedtuple('PricedStockRecord', ['stock_record', 'price'])


class PricesView(APIEndpointView):

    @property
    @cache_result_on_instance
    def shipping_methods(self):
        country_code = self.params['country']

        shipping_method_queryset = self._get_shipping_methods(country_code)

        shipping_methods = defaultdict(list)
        for shipping_method in shipping_method_queryset:
            shipping_methods[shipping_method.partner].append(shipping_method)

        return shipping_methods

    def _get_shipping_methods(self, country_code: str):
        shipping_methods = ShippingMethod.objects.distinct().filter(
            countries__alpha2=country_code,
            partner__in=self.organization.partners_enabled.all()
        ).select_related('partner').order_by(
            'shipping_days_min', 'shipping_days_min'
        )

        return shipping_methods

    def dispatch(self, request, *args, **kwargs):
        try:
            return super(PricesView, self).dispatch(
                request, *args, **kwargs
            )
        except (
            MissingParameterException,
            SupportStructure.DataException
        ) as e:
            logger.exception(f'Exception while retrieving prices: {str(e)}')
            return self.render_json_response({'results': []})

    def get_price(self, stock_record, post_processings=None):
        post_processings = post_processings or []

        price_calculator = PartPriceCalculator(
            stl_file=self.stl_file,
            stock_record=stock_record,
            post_processings=post_processings,
            scale=self.params['scale'],
            currency=self.params['currency']
        )
        return price_calculator.unit_price

    def get_priced_stock_records(self, stock_records):
        priceable_stock_records, priced_stock_records, = [], []
        for stock_record in stock_records:
            try:
                price = self.get_price(stock_record)
            except NoPricingPossibleException:
                continue
            except ManualPricingRequiredException:
                pass
            else:
                priced_stock_records.append(
                    PricedStockRecord(stock_record, price)
                )

            # automatically priced and manually priceable StockRecords
            priceable_stock_records.append(stock_record)

        priced_stock_records.sort(key=lambda x: x.price.incl_tax)
        return priceable_stock_records, priced_stock_records

    def get_fastest_delivery_time(self, stock_record, post_processings=None):
        """
        returns the delivery days for the fastest shipping method.
        This includes the production time.
        """
        if not self.shipping_methods[stock_record.partner]:
            return 0, 0
        fastest_shipping_method = min(
            self.shipping_methods[stock_record.partner],
            key=operator.attrgetter('shipping_time')
        )
        return fastest_shipping_method.delivery_time(
            stock_record, post_processings
        )

    def get_fastest_delivery_time_among_stock_records(self, stock_records):
        """
        returns the delivery days range for the StockRecord with the
        lowest minimum delivery time
        """
        return min(
            self.get_fastest_delivery_time(stock_record)
            for stock_record in stock_records
        )

    def get_shipping_method_dicts(self, stock_record, post_processings=None):
        shipping_methods = []
        for method in self.shipping_methods[stock_record.partner]:
            delivery_min, delivery_max = method.delivery_time(
                stock_record, post_processings
            )
            tax_amount = method.price_incl_tax - method.price_excl_tax
            shipping_methods.append({
                'name': method.name,
                'delivery_min_days': delivery_min,
                'delivery_max_days': delivery_max,
                'price': Price(
                    currency=stock_record.partner.price_currency,
                    excl_tax=method.price_excl_tax,
                    tax=tax_amount
                ).as_dict(currency=self.params['currency']),
                'delivery_days_range': pretty_times(
                    (delivery_min, delivery_max)
                )
            })
        return shipping_methods

    def get_support_info_dict(self, stock_record):
        if not stock_record.support_enabled:
            return {'enabled': False}

        support_values = self.stl_file.support_structure.get_values(
            stock_record.support_angle,
            stock_record.support_offset,
            self.params['scale']
        )
        support_volume, supported_area, supporting_area = support_values['h-']
        return {
            'enabled': True,
            'volume': support_volume,
            'supported_area': supported_area,
            'supporting_area': supporting_area,
            'angle_default': stock_record.support_angle,
            'offset_default': stock_record.support_offset
        }

    def get_printability_info(self, material):
        printability_info = self.stl_file.get_printability_status(
            material.attr_wall_min, self.params['scale']
        )
        del printability_info['status']
        return printability_info

    def create_basket_url(self, stock_record, post_processing=None):
        color_id = self.params.get('color_id', None)
        if post_processing:
            finish_slug = post_processing.slug
        else:
            finish_slug = 'raw'

        query_params = {
            'uuid': self.uuid,
            'scale': self.params['scale'],
            'supplier': stock_record.partner.code,
            'finishing': '{0}-{1}'.format(
                stock_record.product.slug, finish_slug
            )
        }
        if color_id is not None:
            query_params[color_id] = color_id

        return '{0}{1}?{2}'.format(
            get_base_url(),
            reverse('v1_add_to_basket'),
            urlencode(query_params)
        )

    @staticmethod
    def get_amount_stock_records(stock_records):
        amount = len(stock_records)
        if amount == 1:
            return amount, 'Offered by {0}'.format(
                stock_records[0].partner.name
            )
        return amount, 'Offered by {0} services'.format(amount)

    @staticmethod
    def get_supplier_slugs(stock_records):
        return [stock_record.partner.code for stock_record in stock_records]

    @staticmethod
    def get_post_processing_slugs(stock_record):
        material_slug = stock_record.product.slug
        return ['{0}-raw'.format(material_slug)] + [
            '{0}-{1}'.format(material_slug, post_processing.slug)
            for post_processing in stock_record.post_processings.all()
        ]

    def get_relevant_stock_records_for_new_checkout_queryset(
        self, country_code, partners_enabled
    ):
        return StockRecord.all_objects.distinct().filter(
            Q(partner__shipping_methods__countries__alpha2=country_code,
              partner__shipping_methods__deleted_date=None,
              ) |
            Q(partner__pickuplocation__isnull=False,
              partner__pickuplocation__deleted_date=None,),
            edit_status=StockRecord.PUBLISHED,
            partner__in=partners_enabled,
            partner__address__isnull=False,
            deleted_date=None
        )
