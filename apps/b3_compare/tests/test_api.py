from decimal import Decimal

from django.test import override_settings

from apps.b3_address.factories import CountryFactory
from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.clients import ApiKeyAuthorizedClient
from apps.b3_tests.factories import create_product, \
    ColorFactory, StlFileFactory, \
    PartnerFactory, StockRecordFactory, \
    create_post_processing, PostProcessingFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.utils import decode_url
from apps.partner.tests.test_supportstructure import set_support_structure_data

TEST_UUID = '00000000-0000-0000-0000-000000000000'


@override_settings(DEBUG=True, DEMO_UUID=TEST_UUID)
class BaseAPITest(TestCase, object):
    def setUp(self):
        super(BaseAPITest, self).setUp()
        self.client = ApiKeyAuthorizedClient(self.organization.private_api_key)
        self.demo_file = StlFileFactory(
            uuid=TEST_UUID,
            showname='demo')
        self.partner = PartnerFactory(code='test-partner')
        self.product = create_product()
        self.germany = CountryFactory(alpha2='DE')
        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
            name='UPS',
            countries=(self.germany,)
        )
        self.pickup_location = PickupLocationFactory(
            partner=self.partner
        )
        self.stockrecord = StockRecordFactory(
            partner=self.partner,
            product=self.product
        )
        self.post_processing = create_post_processing(
            self.stockrecord, has_colors=True, price_formula='1000'
        )


class UploadTest(BaseAPITest):
    def test_get_upload(self):
        response = self.client.get(
            f'/v1/uploads/{TEST_UUID}/'
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['status'], 'finished')
        self.assertEqual(response.json()['name'], 'demo')
        self.assertTrue('/demo/' in response.json()['share_url'])
        self.assertIsNotNone(response.json()['creation_date'])
        self.assertEqual(response.json()['parameter']['volume'], 23286.07)
        self.assertEqual(response.json()['parameter']['area'], 17672.93)
        self.assertEqual(response.json()['parameter']['faces'], 2308)

    def test_no_api_key_supplied(self):
        # Remove API key from client
        self.client.api_key = None
        response = self.client.get(
            f'/v1/uploads/{TEST_UUID}/',
        )
        self.assertEqual(response.json()['error'], 'Unable to authorize.')

    def test_get_upload_unexisting_file(self):
        response = self.client.get(
            '/v1/uploads/00000000-0000-0000-0000-INVALID00000/'
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.json()['error'],
            ('The requested resource '
             'is not found. Please read '
             'our api documentation here '
             'https://developers.3yourmind.com/'))


@override_settings(DEBUG=True)
class ColorViewTest(BaseAPITest):
    """
    Test the color endpoint
    """

    def test_color(self):
        color = ColorFactory()
        response = self.client.get('/v1/colors/')
        retrieved_colors = [
            (c['id'], c['title'], c['rgb'])
            for c in response.json()['results']
        ]

        self.assertIn(
            (color.id, color.title, color.rgb),
            retrieved_colors
        )


class MaterialViewTest(BaseAPITest):
    """
    Test the material endpoint on prod server
    """

    def test_material(self):
        response = self.client.get('/v1/materials/')
        material = response.json()['results'][0]

        p = self.product
        finish_slugs = [
            '{0}-raw'.format(p.slug),
            self.post_processing.full_slug
        ]
        self.assertEqual(material['slug'], p.slug)
        self.assertEqual(material['is_acid_resistant'], p.is_acid_resistant)
        self.assertEqual(material['finishes'], finish_slugs)
        self.assertEqual(material['title'], p.title)
        self.assertEqual(material['description'], p.description)
        self.assertEqual(material['sub-category'], self.product.category.name)
        self.assertEqual(material['category'],
                         self.product.category.get_parent().name)

        self.assertEqual(material['is_multicolor'], p.is_multicolor)
        self.assertEqual(material['is_bio_compatible'], p.is_bio_compatible)
        self.assertEqual(material['technology'], p.technology.title)
        self.assertEqual(material['attr_detail'], p.attr_detail)
        self.assertEqual(material['attr_strength'], p.attr_strength)
        self.assertEqual(material['attr_wall_min'], p.attr_wall_min)
        self.assertEqual(material['attr_wall_opt'], p.attr_wall_opt)
        self.assertEqual(material['attr_density_min'], p.attr_density_min)
        self.assertEqual(material['attr_density_max'], p.attr_density_max)

        self.assertEqual(material['score_detail'], p.score_detail)
        self.assertEqual(material['score_strength'], p.score_strength)
        self.assertEqual(material['score_density'], p.score_density)


class MaterialPricesTest(BaseAPITest):
    """
    Test the material prices endpoint
    """
    # Missing uuid in url

    def test_missing_key_uuid(self):
        response = self.client.get('/v1/material-prices/')

        self.assertEqual(
            response.json()['error'],
            "Value for key 'uuid' is missing"
        )

    # Missing country in url
    def test_missing_key_country(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'scale': 1.0
        }
        response = self.client.get('/v1/material-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'country' is missing"
        )

    # Key material is not supported
    def test_invalid_key_material(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'scale': 1.0,
            'material': 'test-product'
        }
        response = self.client.get('/v1/material-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Key 'material' is not supported in this request"
        )

    def test_material_prices(self):
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0
        }
        for test in ['Shipping', 'Pickup']:
            if test == 'Pickup':
                self.shipping_method.delete()
            response = self.client.get('/v1/material-prices/', query_params)
            stockrecord = response.json()['results'][0]
            raw_slug = '{0}-raw'.format(self.product.slug)
            expected_basket_url = (
                f'http://my.3yd/v1/add-to-basket/?supplier={self.partner.code}'
                f'&finishing={raw_slug}&scale=1.0&uuid={TEST_UUID}'
            )

            self.assertEqual(
                stockrecord['finishes'],
                [raw_slug, self.post_processing.full_slug]
            )
            self.assertEqual(stockrecord['suppliers'], ['test-partner'])
            self.assertEqual(stockrecord['popularity'], 500)
            self.assertEqual(
                decode_url(stockrecord['basket_url']),
                decode_url(expected_basket_url)
            )
            self.assertEqual(stockrecord['max_price']['currency'], 'EUR')
            self.assertEqual(stockrecord['max_price']['tax'], '45.42')
            self.assertEqual(stockrecord['max_price']['incl_tax'], '284.49')
            if test != 'Pickup':
                self.assertEqual(stockrecord['delivery_min_days'], 6)
                self.assertEqual(stockrecord['delivery_max_days'], 12)
            self.assertEqual(stockrecord['min_price']['currency'], 'EUR')
            self.assertEqual(stockrecord['min_price']['tax'], '45.42')
            self.assertEqual(stockrecord['min_price']['incl_tax'], '284.49')
            self.assertEqual(stockrecord['has_color_finishes'], True)
            self.assertEqual(stockrecord['slug'], self.product.slug)

    def test_material_prices_no_parameter(self):
        stlfile_without_parameters = StlFileFactory(parameter=None)
        query_params = {
            'uuid': stlfile_without_parameters.uuid,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0
        }
        response = self.client.get('/v1/material-prices/', query_params)
        self.assertEqual(len(response.json()['results']), 0)


class SupplierViewTest(BaseAPITest):
    """
    Test the supplier endpoint
    """

    def test_supplier(self):
        response = self.client.get('/v1/suppliers/?country=DE')

        stockrecord = response.json()['results'][0]
        partner = self.partner
        self.assertEqual(stockrecord['title'], partner.name)
        self.assertEqual(stockrecord['website'], partner.website)

        address = partner.address
        self.assertEqual(stockrecord['address'], address.line1)
        self.assertEqual(stockrecord['postcode'], address.zip_code)
        self.assertEqual(stockrecord['city'], address.city)
        self.assertEqual(stockrecord['country'], address.country.name)

        self.assertEqual(
            stockrecord['rating_quality'],
            partner.rating_quality
        )
        self.assertEqual(
            stockrecord['rating_service'],
            partner.rating_service
        )
        self.assertEqual(
            stockrecord['rating_reliability'],
            partner.rating_reliability
        )
        self.assertEqual(stockrecord['rating_total'], partner.rating_total)
        self.assertEqual(stockrecord['slug'], partner.code)


class SupplierPricesTest(BaseAPITest):
    """
    Test the supplier prices endpoint
    """
    # Missing material in url

    def test_missing_key_material(self):
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1
        }
        response = self.client.get('/v1/supplier-prices/', query_params)
        self.assertEqual(
            response.json()['error'],
            "Value for key 'material' is missing"
        )

    # Missing uuid in url
    def test_missing_key_uuid(self):
        query_params = {
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': 'test-product-'
        }
        response = self.client.get('/v1/supplier-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'uuid' is missing"
        )

    # Missing country in url
    def test_missing_key_country(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'scale': 1.0,
            'material': 'test-product-'
        }
        response = self.client.get('/v1/supplier-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'country' is missing"
        )

    def test_supplier_prices(self):
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug
        }
        response = self.client.get('/v1/supplier-prices/', query_params)
        stockrecord = response.json()['results'][0]

        raw_slug = '{0}-raw'.format(self.product.slug)
        expected_basket_url = (
            f'http://my.3yd/v1/add-to-basket/?supplier={self.partner.code}'
            f'&finishing={raw_slug}&scale=1.0&uuid={TEST_UUID}'
        ).format(self.partner.code, raw_slug)

        self.assertEqual(
            decode_url(stockrecord['basket_url']),
            decode_url(expected_basket_url)
        )
        self.assertEqual(stockrecord['supplier'], self.partner.code)
        self.assertEqual(stockrecord['has_color_finishes'], True)
        self.assertEqual(stockrecord['price']['excl_tax'], '239.07')
        self.assertEqual(stockrecord['price']['currency'], 'EUR')
        self.assertEqual(stockrecord['price']['tax'], '45.42')
        self.assertEqual(stockrecord['price']['incl_tax'], '284.49')
        self.assertEqual(stockrecord['slug'], raw_slug)
        self.assertEqual(
            stockrecord['shipping_methods'][0]['name'],
            'UPS'
        )
        self.assertEqual(
            stockrecord['shipping_methods'][0]['delivery_min_days'],
            6
        )
        self.assertEqual(
            stockrecord['shipping_methods'][0]['delivery_max_days'],
            12
        )
        self.assertEqual(
            stockrecord['shipping_methods'][0]['price']['incl_tax'],
            '11.90'
        )
        self.assertEqual(
            stockrecord['shipping_methods'][0]['price']['excl_tax'],
            '10.00'
        )
        self.assertEqual(
            stockrecord['finishes'],
            [raw_slug, self.post_processing.full_slug]
        )

    def test_supplier_shipping_methods_order(self):
        # Add shipping methods
        # there is self.shipping_method in germany only
        usa = CountryFactory(alpha2='US')
        ShippingMethodFactory(
            partner=self.partner,
            countries=(usa,),
            name='us'
        )
        shipping_method_slow = ShippingMethodFactory(
            partner=self.partner,
            countries=(self.germany, usa),
            shipping_days_min=60,
            shipping_days_max=90,
            name='slow'
        )
        ShippingMethodFactory(
            partner=PartnerFactory(code='other'),
            countries=(self.germany,),
            name='no-partner'
        )
        shipping_method_expensive = ShippingMethodFactory(
            partner=self.partner,
            countries=(self.germany, ),
            price_excl_tax=Decimal(5000.00),
            shipping_days_min=2,
            name='expensive'
        )

        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug
        }

        response = self.client.get('/v1/supplier-prices/', query_params)
        stockrecord = response.json()['results'][0]
        self.assertEqual(len(stockrecord['shipping_methods']), 3)
        self.assertEqual(
            stockrecord['shipping_methods'][0]['name'],
            self.shipping_method.name
        )
        self.assertEqual(
            stockrecord['shipping_methods'][1]['name'],
            shipping_method_expensive.name
        )
        self.assertEqual(
            stockrecord['shipping_methods'][2]['name'],
            shipping_method_slow.name
        )


class FinishesViewTest(BaseAPITest):
    """
    Test the finishes endpoint
    """

    def test_finishes(self):
        response = self.client.get('/v1/finishes/')
        finish = response.json()['results'][0]

        self.assertEqual(finish['title'], self.post_processing.title)
        self.assertEqual(finish['material'], self.product.slug)
        self.assertEqual(
            finish['slug'], self.post_processing.full_slug
        )
        self.assertEqual(
            finish['description'], self.post_processing.description
        )
        self.assertEqual(
            finish['partner'], self.partner.code
        )


class FinishesPriceTest(BaseAPITest):
    """
    Test the finishes prices endpoint
    """
    # Missing uuid in url

    def test_missing_key_uuid(self):
        query_params = {
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': 'pa12-pa2200'
        }
        response = self.client.get('/v1/finishes-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'uuid' is missing"
        )

    # Missing country in url
    def test_missing_key_country(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug
        }
        response = self.client.get('/v1/finishes-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'country' is missing"
        )

    # Missing material in url
    def test_missing_key_material(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'country': 'DE',
            'scale': 1.0
        }
        response = self.client.get('/v1/finishes-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'material' is missing"
        )

    # Missing supplier in url
    def test_missing_key_supplier(self):
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'country': 'DE',
            'scale': 1.0,
            'material': self.product.slug
        }
        response = self.client.get('/v1/finishes-prices/', query_params)

        self.assertEqual(
            response.json()['error'],
            "Value for key 'supplier' is missing"
        )

    def test_finishes_prices(self):
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug,
            'supplier': self.partner.code
        }
        response = self.client.get('/v1/finishes-prices/', query_params)
        # First entry is raw material, second is expected PostProcessing
        finish = response.json()['results'][1]

        expected_slug = self.post_processing.full_slug
        expected_basket_url = (
            f'http://my.3yd/v1/add-to-basket/?supplier={self.partner.code}'
            f'&finishing={expected_slug}&scale=1.0&uuid={TEST_UUID}'
        )

        self.assertEqual(finish['supplier'], 'test-partner')
        self.assertEqual(finish['price']['excl_tax'], '1239.07')
        self.assertEqual(finish['price']['currency'], 'EUR')
        self.assertEqual(finish['price']['tax'], '235.42')
        self.assertEqual(finish['price']['incl_tax'], '1474.49')
        self.assertEqual(finish['color_ids'], [1, 2])
        self.assertEqual(finish['slug'], expected_slug)
        self.assertEqual(
            decode_url(finish['basket_url']),
            decode_url(expected_basket_url)
        )

        self.assertEqual(
            finish['shipping_methods'][0]['delivery_min_days'],
            7
        )
        self.assertEqual(
            finish['shipping_methods'][0]['delivery_max_days'],
            15
        )
        self.assertEqual(
            finish['shipping_methods'][0]['price']['currency'],
            'EUR'
        )
        self.assertEqual(
            finish['shipping_methods'][0]['price']['tax'],
            '1.90'
        )
        self.assertEqual(
            finish['shipping_methods'][0]['price']['incl_tax'],
            '11.90'
        )
        self.assertEqual(
            finish['shipping_methods'][0]['name'],
            'UPS'
        )

    def test_finishes_prices_default_order(self):
        post_processing2 = PostProcessingFactory(stock_record=self.stockrecord)
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug,
            'supplier': self.partner.code,
            'color_id': 1
        }
        response = self.client.get('/v1/finishes-prices/', query_params)
        finishes = response.json()['results']
        slugs = sorted([finish['slug'] for finish in finishes])

        expected_slugs = sorted([
            self.post_processing.full_slug,
            post_processing2.full_slug,
            '{0}-raw'.format(self.product.slug)
        ])
        self.assertEqual(slugs, expected_slugs)

    def test_follow_add_to_basket_link(self):
        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug,
            'supplier': self.partner.code,
            'color_id': 1
        }
        response = self.client.get('/v1/finishes-prices/', query_params)
        basket_url = response.json()['results'][1]['basket_url']
        # Follow the add to basket link
        response = self.client.get(basket_url)
        self.assertEqual(response.status_code, 200)

    def test_bounds_filtering(self):
        post_processing2 = PostProcessingFactory(stock_record=self.stockrecord)

        query_params = {
            'uuid': TEST_UUID,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug,
            'supplier': self.partner.code
        }
        response = self.client.get('/v1/finishes-prices/', query_params)
        self.assertEqual(len(response.json()['results']), 3)

        # Now resrict the bounds
        post_processing2.bounds_x_max = 100
        post_processing2.bounds_y_max = 100
        post_processing2.bounds_z_max = 100
        post_processing2.save()

        # post-processing should be filtered out
        response = self.client.get('/v1/finishes-prices/', query_params)
        self.assertEqual(len(response.json()['results']), 2)


class CustomPriceTest(BaseAPITest):
    def test_custom_price(self):
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '1.25+root(volume)+pow(box, 0.33)',
            'scale': 1,
        }
        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['filename'], 'demo')
        self.assertEqual(response_json['price']['excl_tax'], '225.73')
        self.assertEqual(response_json['price']['tax'], '42.89')
        self.assertEqual(response_json['price']['incl_tax'], '268.62')

    def test_custom_price_scaled(self):
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '1.25+root(volume)+pow(box, 0.33)',
            'scale': 2,
        }
        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['filename'], 'demo')
        self.assertEqual(response_json['price']['excl_tax'], '575.63')
        self.assertEqual(response_json['price']['tax'], '109.37')
        self.assertEqual(response_json['price']['incl_tax'], '685.00')

    def test_custom_price_support(self):
        self.partner.metalprint_features_enabled = True
        self.partner.save()
        set_support_structure_data('support_bear.json', histogram=False)

        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '0',
            'scale': 2,
            'support_enabled': True,
            'support_formula': 'volume+area',
            'support_angle': 50,
            'support_offset': 5
        }
        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['filename'], 'demo')
        self.assertEqual(response_json['price']['excl_tax'], '82792.95')
        self.assertEqual(response_json['price']['tax'], '15730.66')
        self.assertEqual(response_json['price']['incl_tax'], '98523.61')

    def test_custom_price_no_vat(self):
        self.partner.vat_rate = 0
        self.partner.save()
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '1.25+root(volume)+pow(box, 0.33)',
            'scale': 1,
        }
        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['filename'], 'demo')
        self.assertEqual(response_json['price']['excl_tax'], '225.73')
        self.assertEqual(response_json['price']['tax'], '0.00')
        self.assertEqual(response_json['price']['incl_tax'], '225.73')

    def test_custom_price_price_formula_variables(self):
        self.partner.vat_rate = 0
        self.partner.save()
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '(area+((box-volume)+w)/h-d)/shells',
            'scale': 1,
        }

        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        response_json = response.json()

        self.assertEqual(response_json['filename'], 'demo')
        params = self.demo_file.parameter
        expected_price = round(
            (params.area + (
                (params.get_bb_volume(1) - params.volume) + params.w
            ) / params.h - params.d) / params.shells,
            2
        )
        price = response_json['price']
        self.assertEqual(price['excl_tax'], str(expected_price))
        self.assertEqual(price['tax'], '0.00')
        self.assertEqual(price['incl_tax'], str(expected_price))

    def test_custom_price_eval_return_error(self):
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '(bla ?./',
        }
        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 400)
        response_json = response.json()

        self.assertEqual(
            response_json['moreInfo']['errorMessage'],
            'Illegal character \'?\''
        )

        self.assertEqual(
            response_json['moreInfo']['formula'],
            'PRICE_FORMULA'
        )

    def test_custom_price_support_eval_return_error(self):
        set_support_structure_data('support_bear.json', histogram=False)
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'support_enabled': True,
            'support_formula': '(bla ?./',
            'support_angle': 45,
            'support_offset': 0
        }

        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 400)
        response_json = response.json()

        self.assertEqual(
            response_json['moreInfo']['errorMessage'],
            'Illegal character \'?\''
        )
        self.assertEqual(
            response_json['moreInfo']['formula'],
            'SUPPORT_FORMULA'
        )

    def test_custom_price_orientation_eval_return_error(self):
        set_support_structure_data('support_bear.json', histogram=False)
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'support_enabled': True,
            'support_formula': '1',
            'support_angle': 45,
            'support_offset': 0,
            'orientation_formula': '(bla ?./',
        }

        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 400)
        response_json = response.json()

        self.assertEqual(
            response_json['moreInfo']['errorMessage'],
            'Illegal character \'?\''
        )
        self.assertEqual(
            response_json['moreInfo']['formula'],
            'ORIENTATION_FORMULA'
        )

    def test_stockrecord_not_saved_to_db(self):
        query_params = {
            'uuid': self.demo_file.uuid,
            'stockrecord_id': self.stockrecord.pk,
            'price_formula': '1',
        }

        response = self.client.get('/v1/custom-price/', query_params)
        self.assertEqual(response.status_code, 200)
        self.stockrecord.refresh_from_db()
        self.assertEqual(
            self.stockrecord.price_custom,
            ('max(10.5, 1.25+root(volume)+'
             'pow(box, 0.33)+min(pow(box, 0.2), area)*shells)')
        )


class VirtualPrintingServicesTest(BaseAPITest):
    def test_adding_multiple_supplier_to_single_mode(self):
        self.organization.is_single_supplier_account = True
        self.organization.save()
        partner_2 = PartnerFactory(code='test-partner_2')
        partner_2.save()
        self.organization.partners_enabled.add(partner_2)
        self.organization.save()
        self.assertEqual(self.organization.partners_enabled.count(), 2)

        response = self.client.get('/v1/suppliers/?country=DE')
        self.assertEqual(len(response.json()['results']), 2)

    def add_supplier_with_cheaper_stock_record(self):
        partner = PartnerFactory(
            code='test-partner_2',
        )
        partner.save()
        self.organization.partners_enabled.add(partner)
        self.organization.save()
        StockRecordFactory(
            product=self.product,
            partner=partner,
            price_custom='1'
        )
        return partner

    def test_stock_records_from_multiple_partners(self):
        """
        We start by checking if the added finishing shows up - and then enable
        is_single_supplier_account and makes sure that only one - the cheapest
        offer is returned
        """
        # Now, try to check the material-prices API
        query_params = {
            'uuid': TEST_UUID,
            'currency': 'EUR',
            'scale': 1.0,
            'country': 'DE'
        }
        response = self.client.get('/v1/material-prices/', query_params)
        decoded_response = response.json()

        # This should return  the cheapeast of the avaialable prices for the
        # stockrecord
        self.assertEqual(
            len(decoded_response['results'][0]['suppliers']), 1
        )
        self.assertEqual(
            decoded_response['results'][0]['suppliers'][0], self.partner.code
        )

        partner2 = self.add_supplier_with_cheaper_stock_record()
        response = self.client.get('/v1/material-prices/', query_params)
        # This partner does not ship to this country yet
        self.assertEqual(len(response.json()['results'][0]['suppliers']), 1)

        # Now add shipping to Germany
        ShippingMethodFactory(
            partner=partner2, countries=(self.germany,),
            name='no-partner'
        )

        response = self.client.get('/v1/material-prices/', query_params)
        self.assertEqual(len(response.json()['results'][0]['suppliers']), 2)

        # Now enable single_supplier_account settings
        self.organization.is_single_supplier_account = True
        self.organization.save()

        response = self.client.get('/v1/material-prices/', query_params)
        # It should only show one now - the chepest
        response_decoded = response.json()['results'][0]
        self.assertEqual(len(response_decoded['suppliers']), 1)
        self.assertEqual(response_decoded['suppliers'][0], partner2.code)
