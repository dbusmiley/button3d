# -*- coding: utf-8 -*-

import urllib.parse

import jwt
from django.urls import reverse
from django.test import override_settings


from apps.b3_tests.factories import StlFileFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase, \
    AuthenticatedTestCase
from apps.basket.models import Basket


class BaseViewerTest(TestCase):
    def setUp(self):
        super(BaseViewerTest, self).setUp()


class AuthenticatedViewerTest(AuthenticatedTestCase):
    def test_open_new_file_as_user(self):
        stlfile = StlFileFactory(showname='MyStlFile')
        url = reverse('upload', kwargs={'uuid': stlfile.uuid})
        response = self.client.get(url, follow=True)
        self.assertTrue('<!-- Viewer Page -->' in response.content.decode())
        self.assertEqual(Basket.objects.count(), 1)
        self.assertEqual(Basket.objects.first().lines.count(), 1)
        self.assertEqual(
            Basket.objects.first().lines.first().uuid, stlfile.uuid)

    def test_open_existing_file_as_user(self):
        stlfile = StlFileFactory(showname='MyStlFile')

        b = BasketFactory(owner=self.user)
        b.add_empty_line(stlfile, quantity=1)

        url = reverse('upload', kwargs={'uuid': stlfile.uuid})
        response = self.client.get(url, follow=True)
        self.assertTrue('<!-- Viewer Page -->' in response.content.decode())
        self.assertEqual(Basket.objects.count(), 1)
        self.assertEqual(Basket.objects.first().lines.count(), 2)
        self.assertEqual(
            Basket.objects.first().lines.all()[0].uuid, stlfile.uuid)
        self.assertEqual(
            Basket.objects.first().lines.all()[1].uuid, stlfile.uuid)

    @override_settings(DEMO_UUID='00000000-0000-0000-0000-000000000000')
    def test_open_demo_as_user(self):
        StlFileFactory(
            showname='MyStlFile', uuid='00000000-0000-0000-0000-000000000000')

        url = reverse('demo')
        response = self.client.get(url, follow=True)
        self.assertTrue(
            '<title>3YOURMIND-MyStlFile</title>'
            in response.content.decode().replace(' ', '').replace('\n', ''))
        self.assertEqual(Basket.objects.first().lines.count(), 0)


class AnonymousViewerTest(TestCase):
    def test_open_new_file_as_anonymous(self):
        stlfile = StlFileFactory(showname='MyStlFile')
        url = reverse('upload', kwargs={'uuid': stlfile.uuid})
        self.assertEqual(Basket.objects.count(), 0)

        response = self.client.get(url, follow=True)
        self.assertTrue('<!-- Viewer Page -->' in response.content.decode())
        self.assertEqual(Basket.objects.count(), 1)
        self.assertIsNone(Basket.objects.first().owner)
        self.assertEqual(Basket.objects.first().lines.count(), 1)
        self.assertEqual(
            Basket.objects.first().lines.first().uuid, stlfile.uuid)

    def test_open_existing_file_as_anonymous(self):
        stlfile = StlFileFactory(showname='MyStlFile')
        url = reverse('upload', kwargs={'uuid': stlfile.uuid})

        # Upload file twice
        self.client.get(url, follow=True)
        response = self.client.get(url, follow=True)

        self.assertTrue('<!-- Viewer Page -->' in response.content.decode())
        self.assertEqual(Basket.objects.count(), 1)
        self.assertIsNone(Basket.objects.first().owner)
        self.assertEqual(Basket.objects.first().lines.count(), 2)
        self.assertEqual(
            Basket.objects.first().lines.all()[0].uuid, stlfile.uuid)
        self.assertEqual(
            Basket.objects.first().lines.all()[1].uuid, stlfile.uuid)

    @override_settings(DEMO_UUID='00000000-0000-0000-0000-000000000000')
    def test_open_demo_as_anonymous(self):
        StlFileFactory(
            showname='MyStlFile', uuid='00000000-0000-0000-0000-000000000000')

        url = reverse('demo')
        response = self.client.get(url, follow=True)
        self.assertTrue(
            '<title>3YOURMIND-MyStlFile</title>'
            in response.content.decode().replace(' ', '').replace('\n', ''))


@override_settings(SECRET_KEY='secret')
class SeccureDownloadTest(TestCase):
    def setUp(self):
        super(SeccureDownloadTest, self).setUp()
        self.stlfile = StlFileFactory(showname='MyStlFile')

    def test_download_optimized_url(self):
        url = reverse(
            'short_uploads_views:download_optimized',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'repaired.stl')

    def test_download_original_url(self):
        url = reverse(
            'short_uploads_views:download_original',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'originalFile.stl')

    def test_download_original_url_non_ascii(self):
        self.stlfile = StlFileFactory(showname='wöw')
        url = reverse(
            'short_uploads_views:download_original',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'originalFile.stl')

    def test_download_thumbnail_url(self):
        url = reverse(
            'short_uploads_views:download_thumbnail',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'thumbnail.png')

    def test_download_viewer_original_url(self):
        url = reverse(
            'short_uploads_views:download_viewer_original',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'viewer.ctm')

    def test_download_viewer_optimized_url(self):
        url = reverse(
            'short_uploads_views:download_viewer_optimized',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'repaired.ctm')

    def test_download_wta_url(self):
        url = reverse(
            'short_uploads_views:download_wta',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'wta.json')

    def test_download_support_url(self):
        url = reverse(
            'short_uploads_views:download_support',
            kwargs={"uuid": self.stlfile.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        file = jwt.decode(token, "secret")['file']
        self.assertEqual(file, 'support.json')

    def test_download_optimized_scaled_url(self):
        url = reverse(
            'short_uploads_views:download_optimized',
            kwargs={"uuid": self.stlfile.uuid})
        url += "?scale=2.5"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        url = response.url  # Redirect URL
        parsed = urllib.parse.urlparse(url)
        token = urllib.parse.parse_qs(parsed.query)['token'][0]
        scale = jwt.decode(token, "secret")['scale']
        self.assertEqual(scale, 2.5)
