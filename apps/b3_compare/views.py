import json
import logging
import urllib.parse
import requests as requestslib

from django.conf import settings

from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.http import Http404, JsonResponse, HttpResponseBadRequest, \
    HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.translation import ugettext_lazy as _, get_language
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.vary import vary_on_headers

from apps.b3_core import utils, permissions
from apps.b3_core.models import StlFile
from apps.b3_organization.utils import get_current_org, get_base_url
from apps.basket.models import Basket, Line
from apps.catalogue.models.category import Category
from apps.catalogue.models.color import Color
from apps.partner.models import StockRecord, PostProcessing

logger = logging.getLogger(__name__)


@vary_on_headers('X-Requested-With')
def compare(request, uuid=None):
    uuid = uuid or settings.DEMO_UUID
    if request.method == 'DELETE':
        return compare_delete(request, uuid)
    if request.method == 'POST':
        return compare_post(request, uuid)
    else:
        if request.is_ajax():
            return compare_get_ajax(request, uuid)
        else:
            return compare_get(request, uuid)


def compare_get_ajax(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    stl_file.clean()

    return JsonResponse(stl_file.to_dict(request), safe=False)


def compare_get(request, uuid, basket_id=None, line_id=None):
    org = get_current_org()
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)

    if not stl_file.can_view(request):
        raise Http404('No StlFile matches the given query.')

    upload_ownership_modal = False
    stl_file.assign_to_user_in_request(request)
    if not request.user.is_authenticated and \
            stl_file.is_mine and not stl_file.is_demo:
        upload_ownership_modal = True

    if (not basket_id or not line_id) and stl_file.is_demo:
        if not request.basket.pk:
            request.basket.save()
        basket_id = request.basket.id

    # Intercept uploaded file (not from project and not demo file)
    if (not basket_id or not line_id) and not stl_file.is_demo:
        # If anonymous user, persist basket to DB first if necessary
        if not request.basket.pk:
            request.basket.save()
        new_line = request.basket.add_empty_line(stl_file)
        if org.single_upload_redirects_to_project_detail:
            return HttpResponseRedirect(
                reverse(
                    'b3_user_panel:project-detail',
                    args=(new_line.basket_id, )
                )
            )
        return HttpResponseRedirect(
            reverse(
                'b3_user_panel:project-line-viewer',
                args=(new_line.basket_id, new_line.id, uuid)
            )
        )

    scale_unit = 'in' if request.GET.get('u', None) == 'in' else 'mm'

    # fetch materials
    curr_lang = get_language()
    compare_steps_key = 'compare_steps__' + curr_lang
    compare_steps = cache.get(compare_steps_key)
    if True:  # if not compare_steps:
        categories = json.dumps(
            [
                {
                    "id": c.id,
                    "depth": c.depth,
                    "path": c.path,
                    "name": c.name,
                } for c in Category.objects.all()
            ]
        )

        compare_steps = render(
            request, 'b3_compare/compare/components/compare_steps.html', {
                'categories': categories,
                'project_id': basket_id,
                'line_id': line_id
            }
        ).content
        cache.set(compare_steps_key, compare_steps, 86400)

    basket_is_ordered = False
    if basket_id:
        basket_is_ordered = Basket.objects.get(pk=basket_id).is_submitted

    context = {
        'stl_file': stl_file.to_dict(request),
        'show_net_prices': org.show_net_prices,
        'base_url': get_base_url(),
        'scale_unit': scale_unit,
        'compare_steps_prerendered': compare_steps,
        # for login/signup, to set ownership of the file (B3-864)
        'upload_uuid': uuid,
        'upload_ownership_modal': upload_ownership_modal,
        'view_name': request.resolver_match.url_name,
        'project_id': basket_id,
        'line_id': line_id,
        'line_name': Line.objects.get(pk=line_id).name if line_id else None,
        'basket_is_ordered': basket_is_ordered,
    }
    response = render(request, 'b3_compare/compare/index.html', context)

    if stl_file.is_demo and not request.user.is_authenticated:
        # Set a cookie for one hour to avoid show overlay
        if not request.COOKIES.get('hide_overlay'):
            response.set_cookie(
                'hide_overlay', False, max_age=3600, httponly=True
            )
        else:
            response.set_cookie(
                'hide_overlay', True, max_age=3600, httponly=True
            )

    return response


@permissions.require_ajax
def compare_post(request, uuid):
    stl_file = StlFile.all_objects.get(uuid=uuid)
    if not stl_file.can_edit(request):
        return JsonResponse({'success': False})

    if 'orientation_x' in request.POST and \
            'orientation_y' in request.POST and \
            'orientation_z' in request.POST:
        stl_file.set_rotation(
            request.POST['orientation_x'], request.POST['orientation_y'],
            request.POST['orientation_z']
        )

    return JsonResponse({'success': True})


@permissions.require_ajax
def compare_delete(request, uuid):
    stl_file = StlFile.all_objects.get(uuid=uuid)
    if not stl_file.can_delete(request):
        return JsonResponse({'success': False})
    stl_file.delete()
    return JsonResponse({'success': True})


# Uploads a file to an printing service and returns an URL
@permissions.require_ajax
@require_POST
@permissions.require_json_keys(
    ['partner_slug', 'material_slug', 'scale', 'unit', 'amount']
)
def generic_upload(request, uuid):
    json_data = json.loads(request.body.decode("utf-8"))
    r_partner_slug = json_data['partner_slug']
    r_material_slug = json_data['material_slug']
    r_scale = float(json_data['scale'])
    r_unit = json_data['unit']
    r_amount = int(json_data['amount'])
    r_post_processings = json_data['post_processings']

    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_order(request):
        return JsonResponse({
            'success': False,
            'message': str(_("You don't have access to order"))})

    stl_file.clean()
    if stl_file.parameter is None:
        return JsonResponse(
            {
                "code": 'GENERIC_UPLOAD_MISSING_PARAMETER',
                "message": 'Please wait until the file has been analyzed.',
                "moreInfo": None
            },
            status=409
        )

    stockrecord = StockRecord.objects.get(
        product__slug=r_material_slug, partner__code=r_partner_slug
    )
    post_processings = [
        {
            'post_processing': PostProcessing.objects.get(
                stock_record=stockrecord,
                slug=post_processing['slug'][len(stockrecord.product.slug)+1:]
            ),
            'color': Color.objects.get(id=post_processing['color_id'])
            if post_processing['color_id'] else None
        }
        for post_processing in r_post_processings
    ]

    # If a basket has previously been ordered as a anonymous user,
    # this basket is set as request.basket by the BasketMiddleware.
    # We need to replace this basket with a new Basket where we can add
    # the uploaded file.
    if not request.basket.can_be_edited:
        request.basket = Basket()
        request.session.pop('ordered_basket_id', None)

    line, created = request.basket.add_product(
        stl_file,
        stockrecord.product,
        scale=r_scale,
        unit=r_unit,
        quantity=r_amount,
        stockrecord=stockrecord,
        post_processings=post_processings,
    )

    request.session['last_basket_line_id'] = line.id
    data = {
        'success': True,
        'redirect_url': reverse(
            'b3_user_panel:project-detail',
            args=(line.basket_id, )
        )
    }
    return JsonResponse(data)


@require_GET
def download_optimized(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_download(request):
        return redirect(
            '{0}?{1}'.format(
                reverse('account_login'),
                urllib.parse.urlencode({'next': request.path}))
        )

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid,
        file=stl_file.get_optimized_filename(),
        filename='{0}_optimized.{1}'.format(
            stl_file.showname, stl_file.get_extension()
        ),
        scale=float(request.GET['scale']) if "scale" in request.GET else None
    )

    return redirect(url)


@require_GET
def download_original(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_download(request):
        return redirect(
            '{0}?{1}'.format(
                reverse('account_login'),
                urllib.parse.urlencode({'next': request.path}))
        )

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid,
        file=stl_file.get_original_filename(),
        filename='{0}_original.{1}'.format(
            stl_file.showname, stl_file.filetype.lower()
        ),
        scale=float(request.GET['scale']) if "scale" in request.GET else None
    )

    return redirect(url)


@require_GET
def download_thumbnail(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="thumbnail.png"
    )

    return redirect(url)


@require_GET
def download_viewer_original(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="viewer.ctm"
    )

    return redirect(url)


@require_GET
def download_viewer_optimized(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="repaired.ctm"
    )

    return redirect(url)


@require_GET
def download_wta(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(uuid=stl_file.uuid, file="wta.json")

    return redirect(url)


@require_GET
def download_support(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="support.json"
    )

    return redirect(url)


@require_GET
def download_texture_original(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="texture.png"
    )

    return redirect(url)


@require_GET
def download_texture_optimized(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="repaired_texture.png"
    )

    return redirect(url)


@require_GET
def download_viewer_simple(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="viewer_simple.ctm"
    )

    return redirect(url)


@require_GET
def download_repaired_simple(request, uuid):
    stl_file = get_object_or_404(StlFile.all_objects, uuid=uuid)
    if not stl_file.can_view(request):
        raise PermissionDenied("Permission denied")

    url = utils.build_secure_download_url(
        uuid=stl_file.uuid, file="repaired_simple.ctm"
    )

    return redirect(url)


@require_POST
def upload_file(request):
    origin = request.POST['origin']
    domain = request.POST['domain']
    if 'file' not in request.FILES:
        return HttpResponseBadRequest()

    in_memory_file = request.FILES['file']

    url = '{0}/upload'.format(get_base_url())

    data = {'origin': origin, 'domain': domain}

    files = {in_memory_file.name: in_memory_file.read()}
    response = requestslib.post(url, data=data, files=files)
    json_response = json.loads(response.content)

    if json_response['success'] is True:
        return HttpResponseRedirect(json_response['url'])
    else:
        raise RuntimeError(
            "Error during compatibility file upload: {0}".format(
                json_response['error']
            )
        )
