import logging

from apps.partner.models import StockRecord
from apps.partner.pricing.calculators import AdditionalInfoPartPriceCalculator

logger = logging.getLogger(__name__)


class StockRecordPriceTester(object):
    def __init__(self, stockrecord):
        self.stockrecord = StockRecord.objects.get(pk=stockrecord.pk)
        self.stockrecord.edit_status = 1

    def set_custom_values(
        self, price_formula, support_enabled, support_formula, support_angle,
        support_offset, orientation_formula
    ):
        if price_formula is not None:
            self.stockrecord.price_custom = price_formula
        if support_enabled is not None:
            self.stockrecord.support_enabled = bool(support_enabled)
        if support_formula is not None:
            self.stockrecord.price_support_custom = support_formula
        if support_angle is not None:
            self.stockrecord.support_angle = support_angle
        if support_offset is not None:
            self.stockrecord.support_offset = support_offset
        if orientation_formula is not None:
            self.stockrecord.price_orientation_custom = orientation_formula

    def get_price(self, stlfile, post_processings=None, quantity=1, scale=1.0):
        self._check_analyze_finished(stlfile)

        post_processings = post_processings or []
        price_calculator = AdditionalInfoPartPriceCalculator(
            stlfile,
            self.stockrecord,
            post_processings=post_processings,
            quantity=quantity,
            scale=scale,
        )

        price = price_calculator.unit_price
        additional_info = price_calculator.additional_info

        return {
            'filename': stlfile.showname,
            'price': {
                'excl_tax': price.excl_tax,
                'incl_tax': price.incl_tax,
                'tax': price.tax
            },
            'additional_info': additional_info
        }

    @staticmethod
    def _check_analyze_finished(stlfile):
        if stlfile.get_job_status()["status"] != "finished":
            raise AnalyzePendingException()
        stlfile.refresh_from_db()


class AnalyzePendingException(Exception):
    pass
