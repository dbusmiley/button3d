import subprocess  # nosec
from unittest import skipIf

from django.test import RequestFactory, override_settings, tag

from apps.b3_tests.testcases.common_testcases import TestCase
from apps.status_page.monitors.app_settings import ApplicationSettings
from apps.status_page.monitors.app_tables import ApplicationTables
from apps.status_page.monitors.app_version import ApplicationVersion
from apps.status_page.monitors.base import SUCCESS, ERROR
from apps.status_page.monitors.currency_api import CurrencyAPI
from apps.status_page.monitors.free_mem import FreeMemory
from apps.status_page.monitors.free_space import FreeSpace
from apps.status_page.monitors.migrations import Migrations
from apps.status_page.monitors.remote_server_is_accessible import \
    RemoteServerIsAccessible
from apps.status_page.monitors.server_ip import ServerIp
from apps.status_page.monitors.sites import Sites
from apps.status_page.monitors.uptime import Uptime


def is_free_available():
    try:
        subprocess.Popen(  # nosec
            ['free'],
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL
        )
    except FileNotFoundError:
        return False
    return True


class BaseMonitorTest(TestCase):
    def setUp(self):
        super().setUp()
        self.request = RequestFactory()


class TablesMonitorTest(BaseMonitorTest):
    def test_get_models_health(self):
        monitor = ApplicationTables()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Application tables created')
        self.assertEqual(result['status'], SUCCESS)

        self.assertTrue(all('- OK' in line
                            for line
                            in result['log']))


class AppVersionMonitorTest(BaseMonitorTest):
    def test_get_app_version(self):
        monitor = ApplicationVersion()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Application version')
        self.assertEqual(result['status'], SUCCESS)


class AppSettingsMonitorTest(BaseMonitorTest):
    def test_get_app_settings(self):
        monitor = ApplicationSettings('DEBUG', 'STATUS_PAGE_MONITORS')
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Application settings')
        self.assertEqual(result['status'], SUCCESS)


class CurrencyApiMonitorTest(BaseMonitorTest):
    @override_settings(ENABLE_CURRENCY_API=False)
    def test_currency_api_is_disabled(self):
        monitor = CurrencyAPI()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Currency API config')
        self.assertEqual(result['status'], ERROR)
        self.assertEqual(result['message'], 'Currency API disabled')


@skipIf(not is_free_available(), '`free` command not available')
class FreeMemoryMonitorTest(BaseMonitorTest):
    def test_free_memory_monitor(self):
        monitor = FreeMemory()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Free memory')
        self.assertEqual(result['status'], SUCCESS)


class FreeSpaceMonitorTest(BaseMonitorTest):
    def test_free_space_monitor(self):
        monitor = FreeSpace()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Free disk space')
        self.assertEqual(result['status'], SUCCESS)


class MigrationsMonitorTest(BaseMonitorTest):
    def test_migrations_monitor(self):
        monitor = Migrations()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Unapplied migrations')
        self.assertEqual(result['status'], SUCCESS)


class RemoteServerAccessibleMonitorTest(BaseMonitorTest):

    @tag('online')
    def test_remote_server_monitor(self):
        monitor = RemoteServerIsAccessible('https://google.com/')
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'https://google.com/ is responding')
        self.assertEqual(result['status'], SUCCESS)


class ServerIpMonitorTest(BaseMonitorTest):

    @tag('online')
    def test_server_ip_monitor(self):
        monitor = ServerIp()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Server IP')
        self.assertEqual(result['status'], SUCCESS)


class SitesMonitorTest(BaseMonitorTest):
    def test_sites_monitor(self):
        monitor = Sites()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Application sites')
        self.assertEqual(result['status'], SUCCESS)


class UptimeMonitorTest(BaseMonitorTest):
    def test_uptime_monitor(self):
        monitor = Uptime()
        result = monitor(self.request)
        self.assertEqual(monitor.title, 'Uptime')
        self.assertEqual(result['status'], SUCCESS)
