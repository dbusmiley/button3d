# Status page

Initial tech assignment: [https://jira.3yourmind.com/browse/B3-2891](https://jira.3yourmind.com/browse/B3-2891)

Purpose of this application is to provide "engineering/diagnostic menu" to system 
admins, who deploy/support button3d and accompanying services. It performs multiple 
checks of application's health.

Only superusers can open `/status/`.

## Configuration

Monitors can be added to `STATUS_PAGE_MONITORS` tuple:

    STATUS_PAGE_MONITORS = (
        STATUS_PAGE_MONITORS = (
            ApplicationVersion(),
            ApplicationSettings(
                'INSTALLED_APPS',
            ),
            CurrencyAPIConfig(),
            DomainExpiryDate('3yourmind.com'),
            GetMemorySpace(),
            GetFreeSpace(),
            IPInfoConfig(),
            RemoteServerIsAccessible('http://3yourmind.com/'),
            ServerIp(),
            Sites(),
            Uptime(),
            ApplicationTables(),
            Migrations(),
            ThreeDBackendUpload('127.0.0.1:8081'),
        )

    )

# Development

Checks/monitors are python classes inheriting `apps.status_page.monitors.base.BaseMonitor`.
Must implement `__call__(request)`, and override `title` with either field or property.

`__call__()` must return dict with required key `status` with value of either 
`main.monitors.base.SUCCESS` or `main.monitors.base.ERROR`. 
Other keys: `log` and `message` are optional.

`message` will be shown next to monitor name

`log` is for longer messages, for example, command logs, error messages. It will 
be shown in collapsible element

`title` is monitor name shown in the UI

For examples look at `apps.status_page.monitors.*`
