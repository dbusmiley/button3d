from django import template
from pprint import pformat

register = template.Library()


@register.filter
def prettify(value):
    if isinstance(value, (tuple, list)):
        return '\n'.join(value)
    elif isinstance(value, str):
        return value
    else:
        return pformat(value)
