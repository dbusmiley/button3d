from concurrent.futures.thread import ThreadPoolExecutor
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render
from importlib import import_module


class StatusPageError(Exception):
    pass


MONITORS = []

for line in settings.STATUS_PAGE_MONITORS:
    monitor_class_path = line[0]
    module_path, monitor_class_name = monitor_class_path.rsplit('.', 1)

    try:
        module = import_module(module_path)
    except ImportError:
        raise StatusPageError(f'Module {module_path} not found')

    try:
        monitor_class = getattr(module, monitor_class_name)
    except AttributeError:
        raise StatusPageError(f'Monitor class {monitor_class_path} not found')

    try:
        args = line[1]
    except IndexError:
        args = []

    if isinstance(args, dict):
        kwargs = args
        args = []
    else:
        try:
            kwargs = line[2]
        except IndexError:
            kwargs = {}

    try:
        MONITORS.append(monitor_class(*args, **kwargs))
    except TypeError as exc:
        raise StatusPageError(
            f'Monitor {monitor_class_path} is misconfigured', *exc.args)


@user_passes_test(lambda user: user.is_superuser)
def index(request):
    def run_monitor(monitor):
        return monitor.title, monitor(request)

    pool = ThreadPoolExecutor(max_workers=settings.STATUS_PAGE_THREADPOOL_SIZE)

    monitor_results = pool.map(
        run_monitor, MONITORS, timeout=10)

    monitor_results = sorted(
        monitor_results, key=lambda args: args[1]['status'])

    return render(
        request,
        'status_page/status.html',
        {'monitor_results': monitor_results}
    )
