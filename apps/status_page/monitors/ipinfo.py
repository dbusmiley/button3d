from django.conf import settings

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR


class IPInfoConfig(BaseMonitor):
    """
    Tests if ipinfo service is enabled and configured.

    If ipinfo is misconfigured, it returns default country for all IPs. If
    detected country differs from default, ipinfo is considered working
    """
    # this is Ukrainian IP, it is not default country
    DEFAULT_IP = '91.217.58.28'

    def __init__(self, ip: str = None):
        self.ip = ip or self.DEFAULT_IP

    @property
    def title(self) -> str:
        return f'IP Info config for {self.ip}'

    def __call__(self, _) -> dict:
        from apps.b3_core.utils import get_country_from_ip

        ipinfo_disabled = not settings.ENABLE_IPINFO_API

        if ipinfo_disabled:
            return {
                'status': ERROR,
                'message': 'IP Info disabled',
            }

        country = get_country_from_ip(self.ip)
        default_country = getattr(settings, 'DEFAULT_COUNTRY', 'DE')
        # if country from IP is not equal to default country, then ipinfo
        # is working

        if country != default_country:
            message = f'IP Info enabled and working (ip {self.ip} ' \
                      f'country is {country})'
            status = SUCCESS
        else:
            message = 'IP Info enabled and NOT working'
            status = ERROR

        return {
            'status': status,
            'message': message,
        }
