from django.conf import settings

from apps.status_page.monitors.base import BaseMonitor, SUCCESS


class ApplicationSettings(BaseMonitor):
    title = 'Application settings'

    def __init__(self, *settings_list):
        self.settings_list = settings_list

    def __call__(self, _) -> dict:
        settings_values = {}

        for setting_name in self.settings_list:
            setting_value = getattr(settings, setting_name)
            settings_values[setting_name] = setting_value

        return {'status': SUCCESS, 'log': settings_values}
