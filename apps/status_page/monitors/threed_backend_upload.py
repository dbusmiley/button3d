import time

import pathlib
import requests
from django.conf import settings

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR

ORIGIN = 'status_page'


class ThreeDBackendUpload(BaseMonitor):
    """
    Checks if 3D backend accepts uploaded file, processes it and updates the
    database and responds with correct redirect URL
    """

    @property
    def title(self) -> str:
        return f'3D backend upload.'

    def __call__(self, _) -> dict:
        try:
            host = settings.BACKEND_3D_HOST
        except AttributeError as exception:
            return {
                'status': ERROR,
                'message': 'BACKEND_3D_HOST not specified in setttings',
                'log': str(exception)
            }

        backend_url = f'{host}/upload'

        from apps.b3_core.models import StlFile
        file_path = pathlib.Path(settings.BASE_DIR) \
            / 'apps/status_page/files/cube.stl'
        with open(file_path, 'rb') as stl_file:
            files = {'file': stl_file}
            try:
                response = requests.post(
                    backend_url, data={'origin': ORIGIN}, files=files)
            except requests.ConnectionError as exception:
                return {
                    'status': ERROR,
                    'message': '3D backend response error',
                    'log': str(exception)
                }

        try:
            response_json = response.json()
            uuid = response_json['uuid']
            url = response_json['url']
        except ValueError:
            # response is not valid json
            return {
                'status': ERROR,
                'message': '3D backend response is not valid JSON',
                'log': response.content
            }
        except KeyError:
            return {
                'status': ERROR,
                'message': '3D backend response JSON does not have '
                           'required fields',
                'log': response.content
            }

        time.sleep(1)

        try:
            stl_file = StlFile.all_objects.get(uuid=uuid)
        except StlFile.DoesNotExist:
            return {
                'status': ERROR,
                'message': '3D backend did not save stl file in the DB'
            }
        else:
            if stl_file.origin != ORIGIN:
                return {
                    'status': ERROR,
                    'message': 'Uploaded file has wrong \'origin\'',
                    'log': stl_file.origin
                }
            else:
                return {'status': SUCCESS, 'message': url}
