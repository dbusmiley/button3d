import subprocess  # nosec

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR, \
    SUBPROCESS_COMMAND_DEFAULT_TIMEOUT


class FreeMemory(BaseMonitor):
    title = 'Free memory'

    def __call__(self, _) -> dict:
        try:
            process = subprocess.Popen(  # nosec
                ['free'], stdout=subprocess.PIPE
            )
            process.wait(SUBPROCESS_COMMAND_DEFAULT_TIMEOUT)
        except FileNotFoundError:
            return {
                'status': ERROR,
                'message': '`free` application is not available at host '
                           'system, but required for this check'
            }
        except subprocess.TimeoutExpired:
            return {
                'status': ERROR,
                'message': '`free` took too long to run'
            }

        return {
            'status': SUCCESS,
            'message': process.stdout.read().decode().split('\n')
        }
