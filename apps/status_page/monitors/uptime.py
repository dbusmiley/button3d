import subprocess  # nosec

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR, \
    SUBPROCESS_COMMAND_DEFAULT_TIMEOUT


class Uptime(BaseMonitor):
    title = 'Uptime'

    def __call__(self, _) -> dict:
        try:
            process = subprocess.Popen(  # nosec
                ['uptime'], stdout=subprocess.PIPE
            )
            process.wait(SUBPROCESS_COMMAND_DEFAULT_TIMEOUT)
        except FileNotFoundError:
            return {
                'status': ERROR,
                'message': '`uptime` application is not available at host '
                           'system, but required for this check'
            }
        except subprocess.TimeoutExpired:
            return {
                'status': ERROR,
                'message': '`uptime` took too long to run'
            }

        return {'status': SUCCESS,
                'message': process.stdout.read().decode().strip()}
