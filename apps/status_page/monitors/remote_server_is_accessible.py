import requests
from requests.exceptions import RequestException

from apps.status_page.monitors.base import BaseMonitor, ERROR, SUCCESS


class RemoteServerIsAccessible(BaseMonitor):
    def __init__(self, server_url: str):
        self.url = server_url

    @property
    def title(self) -> str:
        return f'{self.url} is responding'

    def __call__(self, _) -> dict:
        try:
            response = requests.get(self.url, timeout=10)
        except RequestException as exception:
            return {'status': ERROR, 'log': str(exception)}
        else:
            return {
                'status': SUCCESS,
                'message': f'Elapsed {response.elapsed}, '
                           f'status code {response.status_code}'
            }
