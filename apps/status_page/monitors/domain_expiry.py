from datetime import timedelta
from django.utils.timezone import is_naive, make_aware, now

from apps.status_page.monitors.base import BaseMonitor, ERROR, SUCCESS


class DomainExpiryDate(BaseMonitor):

    def __init__(
            self, domain: str, expiry_period_days: int = 30):
        self.domain = domain
        self.expiry_period = timedelta(days=expiry_period_days)

    @property
    def title(self) -> str:
        return f'Domain {self.domain} expiry period'

    def __call__(self, _) -> dict:
        import whois
        try:
            result = whois.query(self.domain)
        except FileNotFoundError:
            return {
                'status': ERROR,
                'message': '`whois` application is not available at host '
                           'system, but required for this check'
            }

        expiration_date = result.expiration_date
        if not expiration_date:
            return {
                'status': ERROR,
                'message': '`whois` Returned `None` Expiration Date',
                'log': str(result.__dict__)
            }
        if is_naive(expiration_date):
            expiration_date = make_aware(expiration_date)

        days_to_expire = expiration_date - now()
        msg = f'Expires in {days_to_expire.days} days'

        if days_to_expire < self.expiry_period:
            return {'status': ERROR, 'message': msg}
        else:
            return {
                'status': SUCCESS,
                'message': msg,
            }
