import pkg_resources

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR


class PythonPackages(BaseMonitor):
    title = 'Python packages'

    def __call__(self, _) -> dict:
        installed_packages = pkg_resources.working_set
        installed_packages_list = sorted([f"{i.key}=={i.version}"
                                          for i in installed_packages])
        if installed_packages:
            return {
                'status': SUCCESS,
                'log': '\n'.join(installed_packages_list)
            }
        else:
            return {
                'status': ERROR,
                'message': 'Something went wrong'
            }
