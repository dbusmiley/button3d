from apps.status_page.monitors.base import BaseMonitor, SUCCESS


class Sites(BaseMonitor):
    """
    Lists all Site objects with organizations
    """

    title = 'Application sites'

    def __call__(self, _) -> dict:
        from django.contrib.sites.models import Site
        sites = Site.objects\
            .select_related('organization')\
            .values_list('domain', 'organization__showname')
        log = []
        for site_domain, organization in sites:
            log.append(f'{site_domain} - {organization}')

        result = {'status': SUCCESS}
        message = '\n'.join(log)

        if len(log) > 10:
            result['log'] = message
        else:
            result['message'] = message

        return result
