
from apps.status_page.monitors.base import BaseMonitor, ERROR, SUCCESS


class Migrations(BaseMonitor):
    """
    Checks if all migrations are applied
    """

    title = 'Unapplied migrations'

    def __call__(self, _) -> dict:
        """
        Solution stolen from https://stackoverflow.com/questions/31838882/
                    check-for-pending-django-migrations
        """
        from django.db import connection
        from django.db.migrations.executor import MigrationExecutor

        connection.prepare_database()
        executor = MigrationExecutor(connection)
        targets = executor.loader.graph.leaf_nodes()

        unapplied_migrations = executor.migration_plan(targets)

        migrations = []
        for migration, _ in unapplied_migrations:
            migrations.append('{0}.{1}'.format(
                migration.app_label, migration.name))

        if migrations:
            status = ERROR
            log = '\n'.join(migrations)
        else:
            status = SUCCESS
            log = None

        return {
            'status': status,
            'log': log,
        }
