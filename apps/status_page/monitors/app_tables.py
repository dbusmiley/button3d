from django.apps import apps
from django.db import ProgrammingError

from apps.status_page.monitors.base import BaseMonitor, ERROR, SUCCESS


class ApplicationTables(BaseMonitor):
    """
    Checks if all models have corresponding tables created
    """

    title = 'Application tables created'

    def __call__(self, _) -> dict:
        log = []
        status = SUCCESS

        for model in apps.get_models():
            try:
                model.objects.first()
            except ProgrammingError as exception:
                msg = str(exception)
                status = ERROR
            else:
                msg = 'OK'
            log.append(f'{model._meta.label} - {msg}')

        return {
            'status': status,
            'log': log,
        }
