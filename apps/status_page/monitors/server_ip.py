import requests
from requests.exceptions import ConnectionError

from apps.status_page.monitors.base import BaseMonitor, ERROR, SUCCESS


class ServerIp(BaseMonitor):
    title = 'Server IP'

    def __call__(self, _) -> dict:
        try:
            response = requests.get('https://ifconfig.co/ip', timeout=10)
        except ConnectionError as exception:
            return {'status': ERROR, 'log': str(exception)}
        else:
            return {'status': SUCCESS, 'message': response.content.decode()}
