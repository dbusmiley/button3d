from django.conf import settings

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR


class CurrencyAPI(BaseMonitor):
    title = 'Currency API config'

    def __call__(self, _) -> dict:
        from apps.b3_core.exceptions import CurrencyAPIError
        from apps.b3_core.utils import get_exchange_rates

        currency_api_enabled = settings.ENABLE_CURRENCY_API

        if not currency_api_enabled:
            return {
                'status': ERROR,
                'message': 'Currency API disabled',
            }

        log = None
        status = SUCCESS
        currency_api_working = True

        try:
            exchange_rates = get_exchange_rates()
        except CurrencyAPIError as exception:
            currency_api_working = False
            log = exception.message
            status = ERROR

        else:
            if 'USDUSD' not in exchange_rates:
                currency_api_working = False
                log = exchange_rates
                status = ERROR

        if currency_api_working:
            message = 'Currency API enabled and working'
        else:
            message = 'Currency API enabled and NOT working'

        return {
            'status': status,
            'message': message,
            'log': log
        }
