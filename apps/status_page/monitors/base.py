import logging

SUCCESS = 'ok'
ERROR = 'error'
SUBPROCESS_COMMAND_DEFAULT_TIMEOUT = 4
logger = logging.getLogger(__name__)


class BaseMonitor:
    def __call__(self, request):
        raise NotImplementedError

    @property
    def title(self):
        raise NotImplementedError
