import subprocess  # nosec
from django.conf import settings

from apps.status_page.monitors.base import BaseMonitor, SUCCESS, ERROR, \
    SUBPROCESS_COMMAND_DEFAULT_TIMEOUT


class ApplicationVersion(BaseMonitor):
    """
    Returns git version
    """
    title = 'Application version'

    def __call__(self, _) -> dict:
        try:
            process = subprocess.Popen(  # nosec
                ['git', 'describe'],
                cwd=settings.BASE_DIR,
                stdout=subprocess.PIPE,
            )
            process.wait(SUBPROCESS_COMMAND_DEFAULT_TIMEOUT)
        except FileNotFoundError:
            return {
                'status': ERROR,
                'message': '`git` application is not available at host system,'
                           ' but required for this check'
            }
        except subprocess.TimeoutExpired:
            return {
                'status': ERROR,
                'message': '`git` took too long to run'
            }

        message = process.stdout.read().decode().strip()

        return {'status': SUCCESS, 'message': message}
