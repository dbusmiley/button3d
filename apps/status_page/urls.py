from django.conf.urls import url

from apps.status_page.views import index

urlpatterns = [
    url(r'^$', index),
]
