from django.conf.urls import url

from apps.b3_auth_plugins.methods.samlv2.views import execute_saml_login, \
    fetch_saml_metadata, execute_saml_logout
from apps.b3_auth_plugins.methods.oauth.views import get_oauth_callback
from apps.b3_auth_plugins.methods.jwt.views import execute_jwt_login

urlpatterns = [
    url(r'^oauth/callback/$', get_oauth_callback, name='oauth_callback'),
    url(r'^saml2/metadata/$', fetch_saml_metadata, name='saml_metadata'),
    url(r'^saml2/login', execute_saml_login, name='saml_login'),
    url(r'^saml2/logout/$', execute_saml_logout, name='saml_logout'),
    url(r'^jwt/login', execute_jwt_login, name='jwt_login'),
]
