from apps.b3_organization.utils import get_current_site
from django.contrib.auth.models import User


class SSOLoginBackend(object):
    """
    Login backend used by OAuth/SAML or other SSO.
    Allow login only on email address
    """
    def authenticate(self, email=None, is_external_login=False):
        if not is_external_login:
            return None

        try:
            current_site = get_current_site()
            return next((user
                         for user in User.objects.filter(email=email)
                         if user.userprofile.site == current_site),
                        None)
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
