from apps.b3_address.models import Address


def try_updating_user_address_for_this_user(
    user, attribute_value_user, attribute_value_address
):
    try:
        Address.objects.create(
            user=user,
            title=attribute_value_address['title'],
            first_name=attribute_value_user['first_name'],
            last_name=attribute_value_user['last_name'],
            company_name=attribute_value_address['company_name'],
            vat_id=attribute_value_address['vat_id'],
            department=attribute_value_address['department'],
            line1=attribute_value_address['line1'],
            line2=attribute_value_address['line2'],
            zip_code=attribute_value_address['zip_code'],
            city=attribute_value_address['city'],
            state=attribute_value_address['state'],
            country_id=attribute_value_address['country_id'],
            phone_number=attribute_value_address['phone_number']
        )
    except Exception as e:
        return False, e

    return True, None


def _fetch_mapping_for_attribute(mapping, attribute_name):
    mapping_obj = mapping.filter(user_field=attribute_name).first()
    if not mapping_obj:
        return attribute_name

    return mapping_obj.mapping


def _populate_mapping_fields(attr_mappings, attr_fields_dict):
    attr_maps_dict = {}
    for field in attr_fields_dict:
        mapping_for_field = _fetch_mapping_for_attribute(attr_mappings, field)
        attr_maps_dict.update({field: mapping_for_field})

    return attr_maps_dict


def parse_extra_attributes_for_auth_plugin(attr_mappings):
    """
    If there are extra attribute mappings present for this plugin, we should
    fetch it and update the model here
    :param saml_response:
    :return: dict with user and address filed mappings. Eg:
    {'user': {'first_name': 'FirstName', }, 'address': {}}
    """
    user_fields = ['first_name', 'last_name']
    address_fields = [
        'title', 'company_name', 'vat_id', 'department', 'line1', 'line2',
        'zip_code', 'city', 'state', 'country_id', 'phone_number',
    ]
    return {
        'user': _populate_mapping_fields(
            attr_mappings=attr_mappings, attr_fields_dict=user_fields
        ),
        'address': _populate_mapping_fields(
            attr_mappings=attr_mappings, attr_fields_dict=address_fields
        )
    }
