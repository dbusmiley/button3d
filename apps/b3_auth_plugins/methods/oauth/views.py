import requests

from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect

from apps.b3_auth_plugins.models import OauthClientConfig

from apps.b3_auth_plugins.utils.create_user_or_login import \
    create_user_or_login
from apps.b3_auth_plugins.utils.check_if_method_enabled_site import \
    fetch_auth_method
from apps.b3_auth_plugins.methods.oauth.utils import \
    fetch_details_header_generator, get_user_info_request_headers, \
    get_details_from_response, get_user_details_url, \
    get_user_email_from_response, get_token_url


def _extract_access_token(access_token_grant_response_raw):
    acc_token_grant_response = access_token_grant_response_raw.json()
    return acc_token_grant_response['access_token']


@csrf_exempt
def get_oauth_callback(request):
    if not request.method == 'GET':
        raise Http404('Bad request')

    auth_method = fetch_auth_method(request, 'oauth')
    if not auth_method:
        raise Http404('Bad request')

    try:
        oauth_service = OauthClientConfig.objects.get(plugin=auth_method)
    except OauthClientConfig.DoesNotExist:
        return Http404('Bad request, OAuth config does not exist')

    try:
        auth_token = request.GET[oauth_service.auth_token_param]
    except KeyError:
        raise Http404('Bad request, TOKEN does not exist')

    (headers, data) = fetch_details_header_generator(
        oauth_service, auth_token, oauth_service.client_id
    )
    # Fetching the token step
    access_token_grant_response_raw = requests.post(
        get_token_url(request, oauth_service),
        data=data, headers=headers, verify=True
    )
    try:
        access_token = _extract_access_token(access_token_grant_response_raw)
    except (ValueError, KeyError):
        raise Http404('Bad request, Failed to fetch access token')

    # Read information about the user step
    user_info_request_headers = get_user_info_request_headers(
        oauth_service, access_token
    )
    user_info_request_headers_response_raw = requests.get(
        get_user_details_url(request, oauth_service),
        headers=user_info_request_headers,
        verify=True
    )

    try:
        user_info_response = user_info_request_headers_response_raw.json()
    except ValueError:
        raise Http404('Bad request, Cannot parse user details')

    user_email = get_user_email_from_response(
        user_info_response, oauth_service, auth_method
    )

    if not user_email:
        raise Http404('Bad request, E-mail address not found on Auth response')

    # Log the user in step
    if user_email:
        (first_name, last_name) = get_details_from_response(
            user_info_response, auth_method
        )
        create_user_or_login(
            request, user_email=user_email, first_name=first_name,
            last_name=last_name
        )

    return HttpResponseRedirect('/')
