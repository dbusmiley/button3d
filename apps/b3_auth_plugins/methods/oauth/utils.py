import urllib.parse

from apps.b3_auth_plugins.models import AttributeMappings
from apps.b3_auth_plugins.methods.common import _fetch_mapping_for_attribute
from apps.b3_auth_plugins.models import OauthClientConfig
from apps.b3_auth_plugins.utils.replace_all import replace_all
from apps.b3_core.utils import get_country


def fetch_details_header_generator(oauth_service, auth_token, client_id):
    headers = {
        'Accept': 'application/json'
    }
    data = {
        'grant_type': 'authorization_code',
        'code': auth_token
    }
    if not oauth_service.use_implicit_token_grant:
        headers['Authorization'] = 'Basic {0}'.format(client_id)
        data['client_id'] = client_id
        data['client_secret'] = oauth_service.secret

    return headers, data


def get_user_info_request_headers(oauth_service, access_token=None):
    req_headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }

    if not oauth_service.get_details_auth_headers_has_modifications:
        auth_header = 'token {access_token}'
    else:
        auth_header = oauth_service.get_details_auth_headers_modifications

    replace_dict = {
        '{client_id}': oauth_service.client_id,
        '{access_token}': access_token
    }

    req_headers['Authorization'] = replace_all(auth_header, replace_dict)

    return req_headers


def get_mapping_for_auth_plugin(auth_method, key):
    attr_mappings_for_plugin = AttributeMappings.objects.filter(
        plugin=auth_method
    )
    return _fetch_mapping_for_attribute(attr_mappings_for_plugin, key)


def get_details_from_response(user_info_response, auth_method):
    # These can throw AttributeError, TypeError and whatever error
    try:
        first_name = user_info_response['firstName']
    except Exception:
        first_name = user_info_response[
            get_mapping_for_auth_plugin(auth_method, 'first_name')
        ] or ''

    try:
        last_name = user_info_response['lastName']
    except Exception:
        last_name = user_info_response[
            get_mapping_for_auth_plugin(auth_method, 'last_name')
        ] or ''

    return first_name, last_name


def get_user_details_url(request, oauth_service):
    """
    The function applies local modifications to get uer details url.
    :param request:
    :param oauth_service: the oauth service in question
    :return:
    """
    replace_dict = {
        '{country}': get_country(request).lower()
    }
    user_details_url = replace_all(
        oauth_service.get_details_url,
        replace_dict
    )
    return user_details_url


def get_user_email_from_response(user_info_response, oauth_service,
                                 auth_method):
    try:
        user_email = user_info_response['email']
    except KeyError:
        user_email = user_info_response[
            get_mapping_for_auth_plugin(auth_method, 'email')
        ] or False

    return user_email


def get_token_url(request, oauth_service):
    """
    The function applies local modifications to token url.
    :param request:
    :param oauth_service: the oauth service in question
    :return:
    """
    replace_dict = {
        '{country}': get_country(request).lower()
    }
    token_url = replace_all(
        oauth_service.get_token_url,
        replace_dict
    )
    return token_url


def oauth_generate_auth_url(auth_plugin, auth_url):
    try:
        oauth_client = OauthClientConfig.objects.get(
            plugin=auth_plugin
        )
    except OauthClientConfig.DoesNotExist:
        return None

    data = {
        'client_id': oauth_client.client_id,
        'scope': oauth_client.scope,
        'state': oauth_client.extra_param
    }
    return '{0}?{1}&redirect_uri={2}'.format(
        auth_url, urllib.parse.urlencode(data),
        oauth_client.callback_url)
