import factory
import os

from apps.b3_auth_plugins.tests.factories import SiteAuthPluginFactory

BASE_DIR = os.path.dirname(__file__)


class SAMLAuthPluginFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'b3_auth_plugins.SAMLClientConfig'

    plugin = factory.SubFactory(SiteAuthPluginFactory)
    single_sign_in_binding = 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
    sp_private_key = '',
    organization_url = 'http://app.3yourmind.com'
    is_signing_enabled = False
    attribute_consume_endpoint = 'http://yourapp.com/auth/saml2/login'
    single_logout_endpoint = 'http://yourapp.com/auth/saml2/logout/'
    single_logout_binding = 'urn:oasis:names:tc:SAML:2.0:bindings:' \
                            'HTTP-Redirect'
    saml_request_GET_param = 'SAMLRequest'
    name_id_format = 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient'
    saml_sign_in_response_POST_param = 'SAMLResponse'
    sp_entity_id = 'volskwagen'
    attribute_consumer_name = '3yourmind'
    is_pki_check_enabled = True
    authentication_class_ref = 'urn:oasis:names:tc:SAML:2.0:ac:classes:' \
                               'unspecified'
    idp_certificate = open(
        os.path.join(BASE_DIR, 'certs/idp_certificate.pem')).read()
    sp_certificate = open(
        os.path.join(BASE_DIR, 'certs/sp_certificate.pem')).read()
