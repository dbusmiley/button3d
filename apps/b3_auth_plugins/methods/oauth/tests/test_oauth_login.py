import os
import json

from unittest import mock
from requests.models import Response


from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import CountryFactory
from apps.b3_auth_plugins.models.site_auth_plugins import SiteAuthPluginConfig
from apps.b3_auth_plugins.models.oauth_client import OauthClientConfig
from apps.b3_auth_plugins.models.attribute_mappings import AttributeMappings

BASE_DIR = os.path.dirname(__file__)

AUTH_PLUGIN = os.path.join(BASE_DIR, 'data/auth_plugin.json')
OAUTH_CLIENT_CONFIG = os.path.join(BASE_DIR, 'data/oauth_client.json')
ATTRIBUTES_MAP = os.path.join(BASE_DIR, 'data/attribute_mappings.json')


class OauthLoginTest(TestCase):
    def setUp(self):
        super(OauthLoginTest, self).setUp()
        self.auth_plugin_conf = json.load(open(AUTH_PLUGIN))
        self.oauth_client_conf = json.load(open(OAUTH_CLIENT_CONFIG))
        self.attributes_map = json.load(open(ATTRIBUTES_MAP))

        country = CountryFactory(alpha2='DE')
        country.save()
        auth_plugin = SiteAuthPluginConfig(**self.auth_plugin_conf)
        auth_plugin.save()
        auth_plugin = SiteAuthPluginConfig.objects.all().first()

        oauth_client = OauthClientConfig(**self.oauth_client_conf)
        oauth_client.plugin_id = auth_plugin.id
        oauth_client.client_id = 'MYCLIENTID1234'
        oauth_client.save()

        # Create attributes here
        for item in self.attributes_map:
            attribute_map = AttributeMappings(**item)
            attribute_map.plugin_id = auth_plugin.id
            attribute_map.save()

        # Now activate this plugin for the current organization
        self.site.organization.auth_plugins_enabled = auth_plugin
        self.site.organization.save()

    def mocked_requests_post(*args, **kwargs):
        # First, need to make sure if the headers were set correct
        headers = kwargs['headers']
        assert headers['Authorization'] == 'Basic MYCLIENTID1234'  # nosec
        # Now check the POST body
        post_body = kwargs['data']
        assert str(post_body['code']) == 'mysecretcode'  # nosec
        assert str(post_body['client_id']) == 'MYCLIENTID1234'  # nosec
        assert str(post_body['grant_type']) == 'authorization_code'  # nosec
        assert str(post_body['client_secret']) == (  # nosec
            'thisisaverysecretclientsecret123123123'
        )

        def get_token_response():
            return json.dumps({
                'access_token': 'ACCESSTOKENGRANTED',
                'refresh_token': 'REFRESH_TOKEN',
                'expires_in': 4234
            }).encode()

        custom_token_response = Response()
        custom_token_response._content = get_token_response()
        return custom_token_response

    def mocked_requests_get(*args, **kwargs):
        # This needs to be called with the right auth headers
        headers = kwargs['headers']
        assert headers['Authorization'] == 'token ACCESSTOKENGRANTED'  # nosec

        def get_user_data_response():
            return json.dumps({
                'GivenName': 'FirstName',
                'SurName': 'LastName',
                'AddressLine1': 'Goerzalle',
                'CountryCode': 'DE',
                'PostCode': 12207,
                'State': 'Berlin',
                'AddressLine3': 'Berlin',
                'AddressLine2': '135',
                'AddressLine4': 'Berlin',
                'Title': 'Mr',
                'PhoneNumber': +4915908181012,
                'Mail': 'testinguser@3yourmind.com'
            }).encode()

        custom_response_user_data = Response()
        custom_response_user_data._content = get_user_data_response()
        custom_response_user_data.return_value = custom_response_user_data
        return custom_response_user_data

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_oauth_callback_response_check(self, mock_get, mock_post):
        response = self.client.get('/', follow=True)

        # Verify if the user is not logged in first
        self.assertFalse(response.context['user'].is_active)
        response = self.client.get(
            '/auth/oauth/callback?code=mysecretcode&state=mystate',
            follow=True
        )

        # Verify if login worked
        self.assertTrue(response.context['user'].is_active)

        # Now check if the user attributes were correct
        logged_in_user = response.context['user']

        self.assertEqual(logged_in_user.username, 'testinguser')
        self.assertEqual(logged_in_user.email,
                         'testinguser@3yourmind.com')
        self.assertEqual(logged_in_user.first_name, 'FirstName')
        self.assertEqual(logged_in_user.last_name, 'LastName')
