import os
import json
import jwt


from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_auth_plugins.factories import JWTClientConfigFactory, \
    SiteAuthPluginFactory, AttributeMappingsFactory
from apps.b3_address.models import Address
from apps.b3_tests.factories import CountryFactory


IDP_PRIVATE_KEY = os.path.join(
    os.path.dirname(__file__), 'certs/idp_privatekey.pem'
)
ATTRIBUTES_MAP = os.path.join(
    os.path.dirname(__file__), 'data/attribute_mappings.json'
)


class JWTLoginTest(TestCase):
    def setUp(self):
        super(JWTLoginTest, self).setUp()
        private_key = open(IDP_PRIVATE_KEY).read()
        payload = {
            "email": 'test@tester.com',
            "last_name": 'last name',
            "first_name": 'first name',
            "company": 'test company',
            "first_line_of_address": 'Goerzalle',
            "second_line_of_address": '135',
            "zip_code": '12207',
            "city": 'Berlin',
            "country": 'DE',
            "phone_number": '+4915908181012'
        }
        self.encoded_payload = jwt.encode(
            payload, private_key, algorithm='RS256'
        )
        self.attributes_map = json.load(open(ATTRIBUTES_MAP))

    def test_decoding_of_jwt(self):
        country = CountryFactory(alpha2='DE')
        country.save()

        auth_plugin = SiteAuthPluginFactory(
            auth_redirect_url='http://my-idp.com', type='jwt',
            name='jwt-unittest-sp'
        )
        auth_plugin.save()
        jwt_client = JWTClientConfigFactory(plugin=auth_plugin)
        jwt_client.save()

        # Create attributes here
        for item in self.attributes_map:
            attribute_map = AttributeMappingsFactory(
                plugin=auth_plugin, user_field=item['user_field'],
                mapping=item['mapping']
            )
            attribute_map.save()

        # Now activate this plugin for the current organization
        self.site.organization.auth_plugins_enabled = auth_plugin
        self.site.organization.save()

        response = self.client.get(
            '/auth/jwt/login',
            data={'token': self.encoded_payload},
            follow=True
        )

        # Verify if login worked
        self.assertTrue(response.context['user'].is_active)

        # Now check if the user attributes were correct
        logged_in_user = response.context['user']

        self.assertEqual(logged_in_user.first_name, 'first name')
        self.assertEqual(logged_in_user.last_name, 'last name')

        user_address = Address.objects.get(user=logged_in_user)
        self.assertIsNotNone(user_address)
        self.assertEqual(user_address.line1, 'Goerzalle')
        self.assertEqual(user_address.line2, '135')
        self.assertEqual(user_address.company_name, 'test company')
        self.assertEqual(user_address.city, 'Berlin')
        self.assertEqual(user_address.zip_code, '12207')
        self.assertEqual(
            user_address.phone_number, '+4915908181012'
        )
