import logging
import jwt

from django.http import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect


from apps.b3_auth_plugins.models.attribute_mappings import AttributeMappings
from apps.b3_auth_plugins.models.jwt_client import JWTClientConfig
from apps.b3_auth_plugins.utils.check_if_method_enabled_site import \
    fetch_auth_method
from apps.b3_auth_plugins.utils.create_user_or_login import \
    create_user_or_login
from apps.b3_auth_plugins.methods.common import \
    try_updating_user_address_for_this_user, \
    parse_extra_attributes_for_auth_plugin


logger = logging.getLogger(__name__)


def _parse_attribute_from_response(key, jwt_response):
    try:
        return jwt_response[key]
    except KeyError:
        return ''


def fetch_attribute_values_from_response(jwt_response, attr_mapping):
    attribute_maps = parse_extra_attributes_for_auth_plugin(attr_mapping)

    attribute_value_user = dict()
    attribute_value_address = dict()

    for attribute, mapping in attribute_maps['user'].items():
        attribute_value_user.update(
            {attribute: _parse_attribute_from_response(mapping, jwt_response)}
        )

    for attribute, mapping in attribute_maps['address'].items():
        attribute_value_address.update(
            {attribute: _parse_attribute_from_response(mapping, jwt_response)}
        )

    return attribute_value_user, attribute_value_address


@csrf_exempt
def execute_jwt_login(request):
    jwt_response = None
    decoded_jwt_response = None
    if not request.method == 'GET':
        logger.error(f'Got {request.method} at /login')
        return HttpResponseBadRequest('Bad request')

    auth_method = fetch_auth_method(request, 'jwt')

    if not auth_method:
        logger.error('Could not find an Auth plugin for the given site')
        return HttpResponseBadRequest('Bad request, plugin not found')

    try:
        jwt_plugin = JWTClientConfig.objects.get(plugin=auth_method)
    except JWTClientConfig.DoesNotExist:
        logger.error('Couldnt find a JWT client for the given site')
        return HttpResponseBadRequest('Bad request, client do not exist')

    try:
        jwt_response = request.GET[jwt_plugin.login_GET_param]
    except KeyError:
        logger.exception(
            f'Could not find any data from param: {jwt_plugin.login_GET_param}'
        )
    try:
        decoded_jwt_response = jwt.decode(
            jwt_response, jwt_plugin.idp_public_key, algorithms='RS256'
        )
    except Exception as e:
        logger.exception(
            f'Decoding JWT response {jwt_response} of type: '
            f'{type(jwt_response)} raised the exception: {e}'
        )

    attr_mappings_for_plugin = AttributeMappings.objects.filter(
        plugin=auth_method
    )
    if not attr_mappings_for_plugin.exists():
        logger.error('No attribute mappings found.')
        return HttpResponseBadRequest('Login failed for the user')

    email_map = attr_mappings_for_plugin.filter(user_field='email').first().\
        mapping

    if not email_map:
        logger.error(
            f'Failed to find email field from: {decoded_jwt_response}'
        )
        return HttpResponseBadRequest('Login failed for the user')

    try:
        email = _parse_attribute_from_response(email_map, decoded_jwt_response)
    except Exception as e:
        logger.error(
            f'Fetching: {email_map} field from {decoded_jwt_response} '
            f'resulted in: {e}'
        )
        return HttpResponseBadRequest('Login failed for the user')

    attribute_value_user = {}
    attribute_value_address = {}
    first_name = ''
    last_name = ''

    try:
        (attribute_value_user, attribute_value_address) = \
            fetch_attribute_values_from_response(
                decoded_jwt_response, attr_mappings_for_plugin
        )
    except Exception as e:
        logger.warning(f'Fetching attributes raised: {e}')

    if 'first_name' in attribute_value_user:
        first_name = attribute_value_user['first_name'] if len(
            attribute_value_user['first_name']
        ) > 0 else ''

    if 'last_name' in attribute_value_user:
        last_name = attribute_value_user['last_name'] if len(
            attribute_value_user['last_name']
        ) > 0 else ''

    created, user = create_user_or_login(
        request, user_email=email, first_name=first_name, last_name=last_name
    )
    if not created:
        return HttpResponseRedirect('/')

    # This was a new user, and hence update his address now
    (updated, exception) = try_updating_user_address_for_this_user(
        user, attribute_value_user, attribute_value_address
    )
    if not updated:
        logger.warning(
            f'Adding address details to user:{user} failed with {exception} '
            f'from response: {decoded_jwt_response}'
        )

    return HttpResponseRedirect('/')
