import logging
import defusedxml.ElementTree as ET
from signxml import XMLVerifier

from django.http import HttpResponseBadRequest, HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.contrib.auth import logout

from apps.b3_auth_plugins.models.saml2_client import SAMLClientConfig
from apps.b3_auth_plugins.models.attribute_mappings import AttributeMappings
from apps.b3_auth_plugins.utils.create_user_or_login import \
    create_user_or_login
from apps.b3_auth_plugins.utils.check_if_method_enabled_site import \
    fetch_auth_method
from apps.b3_auth_plugins.methods.samlv2.metadata.client_metadata import \
    ClientMetadata
from apps.b3_auth_plugins.methods.samlv2.metadata.constants import \
    SAMLConstants
from apps.b3_auth_plugins.methods.samlv2.authnrequest.utils import \
    decode_saml_response
from apps.b3_auth_plugins.methods.common import \
    try_updating_user_address_for_this_user, \
    parse_extra_attributes_for_auth_plugin


logger = logging.getLogger(__name__)


def check_if_pki_validation_exist(saml_response):
    return ET.fromstring(saml_response).findall(
        '*saml:AuthnStatement', namespaces=SAMLConstants.NSMAP
    )[0].findall(
        'saml:AuthnContext', namespaces=SAMLConstants.NSMAP
    )[0].findall(
        'saml:AuthnContextClassRef', namespaces=SAMLConstants.NSMAP
    )[0].text


def parse_pk_info_from_response(saml_response):
    """
    Find it from XML similar to
    <saml:Subject><saml:NameID Format="urn:oasis:names:
    tc:SAML:1.1:nameid-format:emailAddress">asdfasdf@asdfasdf.comasdfasdf
    </saml:NameID> </saml:Subject>
    :param saml_response:
    :return:
    """
    return ET.fromstring(saml_response).findall(
        '*saml:Subject', namespaces=SAMLConstants.NSMAP
    )[0].findall(
        'saml:NameID', namespaces=SAMLConstants.NSMAP
    )[0].text


def _parse_attribute_from_response(attribute, saml_response):
    """
    Find the attribute value from attributes section
    :param saml_response:
    :return: value, the value from SAMLResponse
    """
    value = ''
    if not attribute:
        return None

    try:
        value = ET.fromstring(saml_response).findall(
            '*saml:AttributeStatement', namespaces=SAMLConstants.NSMAP
        )[0].findall(
            'saml:Attribute[@Name=\'{0}\']'.format(attribute),
            namespaces=SAMLConstants.NSMAP
        )[0].findall(
            'saml:AttributeValue', namespaces=SAMLConstants.NSMAP
        )[0].text
    except Exception as e:
        logger.warning('Parsing XML for: {0} raised: {1}'.format(attribute, e))

    return value


def _check_if_mapping_exits_for_email_auth_plugin(mapping):
    return mapping.filter(user_field='email').first()


def fetch_attribute_values_from_response(saml_response, attr_mapping):
    attribute_maps = parse_extra_attributes_for_auth_plugin(attr_mapping)

    attribute_value_user = {}
    attribute_value_address = {}

    for attribute, mapping in attribute_maps['user'].items():
        attribute_value_user.update(
            {attribute: _parse_attribute_from_response(mapping, saml_response)}
        )

    for attribute, mapping in attribute_maps['address'].items():
        attribute_value_address.update(
            {attribute: _parse_attribute_from_response(mapping, saml_response)}
        )

    return attribute_value_user, attribute_value_address


@csrf_exempt
def execute_saml_login(request):
    saml_response = None
    verified_data = None

    if not request.method == 'POST':
        logger.error('Got {0} at /login'.format(request.method))
        return HttpResponseBadRequest('Bad request')

    auth_method = fetch_auth_method(request, 'saml2')
    if not auth_method:
        logger.error('Couldnt find an Auth plugin for the given site')
        return HttpResponseBadRequest('Bad request, plugin not found')

    try:
        saml_plugin = SAMLClientConfig.objects.get(plugin=auth_method)
    except SAMLClientConfig.DoesNotExist:
        logger.error('Couldnt find a SAML client for the given site')
        return HttpResponseBadRequest('Bad request, client do not exist')

    # Validate the assertion first
    try:
        saml_response = decode_saml_response(
            request.POST[saml_plugin.saml_sign_in_response_POST_param]
        )
    except Exception as e:
        logger.exception(
            'Decoding SAML response {0} raised the exception: {1}'.format(
                saml_response, e
            )
        )
    # Try to verify the XML data
    try:
        verified_data = XMLVerifier().verify(
            data=saml_response, x509_cert=saml_plugin.idp_certificate
        ).signed_xml
    except Exception as e:
        logger.exception(
            'Verifying SAML authenticity of {0} raised the following '
            'exception: {1}'.format(saml_response, e)
        )

    if verified_data is None:
        logger.error(
            'Verifying authenticity of {0} failed'.format(saml_response)
        )
        return HttpResponseBadRequest('SAML response validation failed')

    if saml_plugin.is_pki_check_enabled:
        auth_context_val = check_if_pki_validation_exist(saml_response)
        if not auth_context_val == saml_plugin.authentication_class_ref:
            logger.error(
                'Checking for PKI value failed for: {0}'
            )
            return HttpResponseBadRequest('PKI value validation failed')

    username = None
    email = None

    # Check if there exists a mapping for email in database
    attr_mappings_for_plugin = AttributeMappings.objects.filter(
        plugin=auth_method
    )
    email_map_exists = _check_if_mapping_exits_for_email_auth_plugin(
        mapping=attr_mappings_for_plugin
    )
    if not email_map_exists:
        email = parse_pk_info_from_response(saml_response)
    else:
        # We will get username from the NameId feild now. EmailId is elsewhere
        username = parse_pk_info_from_response(saml_response)
        email = _parse_attribute_from_response(
            attribute=email_map_exists.mapping, saml_response=saml_response
        )

    if not email:
        logger.error(
            'Failed to find email field from: {0}'.format(saml_response)
        )
        return HttpResponseBadRequest('Login failed for the user')

    attribute_value_user = {}
    attribute_value_address = {}
    first_name = email if not username else username
    last_name = email if not username else username

    try:
        (attribute_value_user, attribute_value_address) = \
            fetch_attribute_values_from_response(
                saml_response, attr_mappings_for_plugin
        )
    except Exception as e:
        logger.warning(
            'Fetching attributes raised: {0}'.format(e)
        )

    if 'first_name' in attribute_value_user:
        first_name = attribute_value_user['first_name'] if len(
            attribute_value_user['first_name']
        ) > 0 else email

    if 'last_name' in attribute_value_user:
        last_name = attribute_value_user['last_name'] if len(
            attribute_value_user['last_name']
        ) > 0 else email

    if not saml_plugin.is_redirection_post_login_enabled:
        redirect_url = '/'
    else:
        redirect_url = request.POST.get(
            saml_plugin.saml_relay_state_response_POST_param, '/'
        )
        if not redirect_url or redirect_url == '':
            redirect_url = '/'

    created, user = create_user_or_login(
        request, user_email=email, first_name=first_name, last_name=last_name,
        username=username
    )
    if not created:
        return HttpResponseRedirect(redirect_url)

    # This was a new user, and hence update his address now
    (updated, exception) = try_updating_user_address_for_this_user(
        user, attribute_value_user, attribute_value_address
    )
    if not updated:
        logger.warning(
            'Adding address details to user:{0} failed with {1} from '
            'response: {2}'.format(user, exception, saml_response)
        )

    return HttpResponseRedirect(redirect_url)


@csrf_exempt
def execute_saml_logout(request):
    """
    blackhole for saml logout responses
    :param request:
    :return:
    """
    logout(request)


def fetch_saml_metadata(request):
    if not request.method == 'GET':
        raise Http404

    auth_method = fetch_auth_method(request, 'saml2')
    metadata = None
    if not auth_method:
        return HttpResponseBadRequest('Bad request, no plugin found')

    try:
        saml_client = SAMLClientConfig.objects.get(plugin=auth_method)
    except SAMLClientConfig.DoesNotExist:
        return HttpResponseBadRequest(
            'Bad request, SAML config does not exist'
        )

    try:
        metadata = ClientMetadata.add_x509_key_descriptors(
            metadata=ClientMetadata.builder(
                saml_client=saml_client,
                authnsign=saml_client.is_signing_enabled,
                wsign=saml_client.is_signing_enabled
            ),
            cert=saml_client.sp_certificate
        )
    except Exception as e:
        logger.warn('Creating metadata returned: {0}'.format(e))

    return HttpResponse(metadata, content_type='text/xml')
