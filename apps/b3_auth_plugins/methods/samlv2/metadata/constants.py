"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""


class SAMLConstants:
    # Namespaces
    NS_SAML = 'urn:oasis:names:tc:SAML:2.0:assertion'
    NS_SAMLP = 'urn:oasis:names:tc:SAML:2.0:protocol'
    NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/'
    NS_MD = 'urn:oasis:names:tc:SAML:2.0:metadata'
    NS_XS = 'http://www.w3.org/2001/XMLSchema'
    NS_XSI = 'http://www.w3.org/2001/XMLSchema-instance'
    NS_XENC = 'http://www.w3.org/2001/04/xmlenc#'
    NS_DS = 'http://www.w3.org/2000/09/xmldsig#'

    # Namespaces
    NSMAP = {
        'samlp': NS_SAMLP,
        'saml': NS_SAML,
        'md': NS_MD,
        'ds': NS_DS,
        'xenc': NS_XENC
    }

    SHA1 = 'http://www.w3.org/2000/09/xmldsig#sha1'
    SHA256 = 'http://www.w3.org/2001/04/xmlenc#sha256'
    SHA384 = 'http://www.w3.org/2001/04/xmldsig-more#sha384'
    SHA512 = 'http://www.w3.org/2001/04/xmlenc#sha512'
    DSA_SHA1 = 'http://www.w3.org/2000/09/xmld/sig#dsa-sha1'
    RSA_SHA1 = 'http://www.w3.org/2000/09/xmldsig#rsa-sha1'
    RSA_SHA256 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256'
    RSA_SHA384 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384'
    RSA_SHA512 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512'

    NAMEID_ENCRYPTED = 'urn:oasis:names:tc:SAML:2.0:nameid-format:encrypted'
