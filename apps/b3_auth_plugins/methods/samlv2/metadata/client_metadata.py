"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""
from time import gmtime, strftime, time
from datetime import datetime
from defusedxml.minidom import parseString
from textwrap import wrap


class ClientMetadata(object):
    """
    A class that contains methods related to the metadata of the SP
    """

    TIME_VALID = 172800   # 2 days
    TIME_CACHED = 604800  # 1 week

    @staticmethod
    def builder(
        saml_client, authnsign=True, wsign=True, valid_until=None,
        cache_duration=None
    ):
        """
        Builds the metadata of the SP
        """
        valid_until_str = ''
        if valid_until is None:
            valid_until = int(time()) + ClientMetadata.TIME_VALID
        if not isinstance(valid_until, str):
            if isinstance(valid_until, datetime):
                valid_until_time = valid_until.timetuple()
            else:
                valid_until_time = gmtime(valid_until)
            valid_until_str = strftime(r'%Y-%m-%dT%H:%M:%SZ', valid_until_time)
        else:
            valid_until_str = valid_until

        cache_duration_str = ''
        if cache_duration is None:
            cache_duration = ClientMetadata.TIME_CACHED
        if not isinstance(cache_duration, str):
            cache_duration_str = 'PT%sS' % cache_duration
        else:
            cache_duration_str = cache_duration

        str_attribute_consuming_service = ''
        sls = """        <md:SingleLogoutService Binding="%(binding)s"
                            Location="%(location)s" />\n""" % \
            {
                'binding': saml_client.single_logout_binding,
                'location': saml_client.single_logout_endpoint,
            }

        str_authnsign = 'true' if authnsign else 'false'
        str_wsign = 'true' if wsign else 'false'

        organization_names = []
        organization_displaynames = []
        organization_urls = []
        organization_displaynames.append(
            """<md:OrganizationDisplayName xml:lang="en-US">%s
            </md:OrganizationDisplayName>""" %
            saml_client.attribute_consumer_name
        )
        organization_names.append("""<md:OrganizationName xml:lang="en-US">%s
        </md:OrganizationName>""" % (saml_client.attribute_consumer_name))
        organization_urls.append(
            """<md:OrganizationURL xml:lang="en-US">%s
            </md:OrganizationURL>""" % (saml_client.organization_url)
        )

        org_data = '\n'.join(organization_names) + '\n' + '\n'.join(
            organization_displaynames) + '\n' + '\n'.join(organization_urls)
        str_organization = """<md:Organization>%(org)s</md:Organization>
        \n""" % {'org': org_data}

        metadata = """<?xml version="1.0"?>
<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata"
                     %(valid)s
                     %(cache)s
                     entityID="%(entity_id)s">
    <md:SPSSODescriptor AuthnRequestsSigned="%(authnsign)s"
    WantAssertionsSigned="%(wsign)s" protocolSupportEnumeration=
    "urn:oasis:names:tc:SAML:2.0:protocol">
%(sls)s        <md:NameIDFormat>%(name_id_format)s</md:NameIDFormat>
        <md:AssertionConsumerService Binding="%(binding)s"
                                     Location="%(location)s"
                                     index="1" />
%(attribute_consuming_service)s    </md:SPSSODescriptor>
%(organization)s</md:EntityDescriptor>""" % \
            {
                'valid': ('validUntil="%s"' % valid_until_str),
                'cache': ('cacheDuration="%s"' % cache_duration_str),
                'entity_id': saml_client.sp_entity_id,
                'authnsign': str_authnsign,
                'wsign': str_wsign,
                'name_id_format': saml_client.name_id_format,
                'binding': saml_client.single_sign_in_binding,
                'location': saml_client.attribute_consume_endpoint,
                'sls': sls,
                'organization': str_organization,
                'attribute_consuming_service': str_attribute_consuming_service
            }
        return metadata

    @staticmethod
    def format_cert(cert, heads=True):
        """
        Returns a x509 cert (adding header & footer if required).
        :param cert: A x509 unformatted cert
        :type: string
        :param heads: True if we want to include head and footer
        :type: boolean
        :returns: Formatted cert
        :rtype: string
        """
        x509_cert = cert.replace('\x0D', '')
        x509_cert = x509_cert.replace('\r', '')
        x509_cert = x509_cert.replace('\n', '')
        if len(x509_cert) > 0:
            x509_cert = x509_cert.replace('-----BEGIN CERTIFICATE-----', '')
            x509_cert = x509_cert.replace('-----END CERTIFICATE-----', '')
            x509_cert = x509_cert.replace(' ', '')

            if heads:
                x509_cert = "-----BEGIN CERTIFICATE-----\n" + \
                            "\n".join(wrap(x509_cert, 64)) + \
                            "\n-----END CERTIFICATE-----\n"

        return x509_cert

    @staticmethod
    def add_x509_key_descriptors(metadata, cert=None, add_encryption=True):
        """
        Adds the x509 descriptors (sign/encryption) to the metadata
        The same cert will be used for sign/encrypt

        :param metadata: SAML Metadata XML
        :type metadata: string

        :param cert: x509 cert
        :type cert: string

        :param add_encryption: Determines if the KeyDescriptor
        [use="encryption"] should be added.
        :type add_encryption: boolean

        :returns: Metadata with KeyDescriptors
        :rtype: string
        """
        if cert is None or cert == '':
            return metadata
        try:
            xml = parseString(metadata.encode('utf-8'))
        except Exception as e:
            raise Exception('Error parsing metadata. ' + e.message)

        formatted_cert = ClientMetadata.format_cert(cert, False)
        x509_certificate = xml.createElementNS(
            'http://www.w3.org/2000/09/xmldsig#', 'ds:X509Certificate'
        )
        content = xml.createTextNode(formatted_cert)
        x509_certificate.appendChild(content)

        key_data = xml.createElementNS(
            'http://www.w3.org/2000/09/xmldsig#',
            'ds:X509Data'
        )
        key_data.appendChild(x509_certificate)

        key_info = xml.createElementNS(
            'http://www.w3.org/2000/09/xmldsig#',
            'ds:KeyInfo'
        )
        key_info.appendChild(key_data)

        key_descriptor = xml.createElementNS(
            'http://www.w3.org/2000/09/xmldsig#',
            'md:KeyDescriptor'
        )

        entity_descriptor = xml.getElementsByTagName('md:EntityDescriptor')[0]

        sp_sso_descriptor = entity_descriptor.\
            getElementsByTagName('md:SPSSODescriptor')[0]
        sp_sso_descriptor.insertBefore(
            key_descriptor.cloneNode(True), sp_sso_descriptor.firstChild
        )
        if add_encryption:
            sp_sso_descriptor.insertBefore(
                key_descriptor.cloneNode(True), sp_sso_descriptor.firstChild
            )

        signing = xml.getElementsByTagName('md:KeyDescriptor')[0]
        signing.setAttribute('use', 'signing')
        signing.appendChild(key_info)
        signing.setAttribute('xmlns:ds', 'http://www.w3.org/2000/09/xmldsig#')

        if add_encryption:
            encryption = xml.getElementsByTagName('md:KeyDescriptor')[1]
            encryption.setAttribute('use', 'encryption')
            encryption.appendChild(key_info.cloneNode(True))
            encryption.setAttribute(
                'xmlns:ds', 'http://www.w3.org/2000/09/xmldsig#'
            )

        return xml.toxml()
