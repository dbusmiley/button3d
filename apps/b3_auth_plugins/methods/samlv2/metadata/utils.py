"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""
from tempfile import NamedTemporaryFile
from apps.b3_auth_plugins.methods.samlv2.metadata.constants import \
    SAMLConstants


def print_xmlsec_errors(
    filename, line, func, error_object, error_subject, reason, msg
):
    """
    Auxiliary method. It overrides the default xmlsec debug message.
    """

    info = []
    if error_object != "unknown":
        info.append("obj=" + error_object)
    if error_subject != "unknown":
        info.append("subject=" + error_subject)
    if msg.strip():
        info.append("msg=" + msg)
    if reason != 1:
        info.append("errno=%d" % reason)
    if info:
        print(("%s:%d(%s)" % (filename, line, func), " ".join(info)))


def query(dom, query, context=None):
    """
    Extracts nodes that match the query from the Element
    :param dom: The root of the lxml objet
    :type: Element
    :param query: Xpath Expresion
    :type: string
    :param context: Context Node
    :type: DOMElement
    :returns: The queried nodes
    :rtype: list
    """
    if context is None:
        return dom.xpath(query, namespaces=SAMLConstants.NSMAP)
    else:
        return context.xpath(query, namespaces=SAMLConstants.NSMAP)


def write_temp_file(content):
    """
    Writes some content into a temporary file and returns it.
    :param content: The file content
    :type: string
    :returns: The temporary file
    :rtype: file-like object
    """
    f_temp = NamedTemporaryFile(delete=True)
    f_temp.file.write(content)
    f_temp.file.flush()
    return f_temp
