import requests
import logging

from django.contrib.auth.signals import user_logged_out
from django.dispatch import receiver
from apps.b3_auth_plugins.methods.samlv2.logoutrequest.create_logoutrequest \
    import SAMLLogoutRequest
from apps.b3_auth_plugins.utils.check_if_method_enabled_site import \
    fetch_auth_method
from apps.b3_auth_plugins.models import SAMLClientConfig
from apps.b3_auth_plugins.methods.samlv2.authnrequest.utils import \
    build_signature

logger = logging.getLogger(__name__)


@receiver(user_logged_out)
def sig_user_logged_out(sender, user, request, **kwargs):
    saml2_method = fetch_auth_method(request, 'saml2')
    if not saml2_method:
        return

    try:
        saml_client = SAMLClientConfig.objects.get(plugin=saml2_method)
    except SAMLClientConfig.DoesNotExist:
        return

    saml_logout_request = SAMLLogoutRequest(
        saml_client=saml_client, user_email=request.user.email
    ).get_request()

    signature = ''

    try:
        signature = build_signature(
            saml_client=saml_client,
            saml_data=saml_logout_request,
            relay_state=saml2_method.auth_redirect_url,
            saml_type='SAMLRequest'
        )
    except Exception as e:
        logger.exception(
            'Got the exception: {0} on trying to logout'.format(e)
        )
        return

    logout_request_url = saml2_method.auth_redirect_url + "?" + saml_client.\
        saml_request_GET_param + "=" + saml_logout_request + signature

    try:
        requests.get(logout_request_url, timeout=0.005)
    except Exception as e:
        logger.exception(
            'HTTP GET on logout failed at: {0} with: {1}'.format(
                logout_request_url, e
            )
        )

    return
