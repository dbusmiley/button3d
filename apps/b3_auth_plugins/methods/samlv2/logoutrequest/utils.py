"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""

from xml.dom.minidom import Document  # nosec: We don't read input with this
from apps.b3_auth_plugins.methods.samlv2.metadata.constants import \
    SAMLConstants


def generate_name_id(saml_client, value, debug=False):
    """
    Generates a nameID.
    :param value: fingerprint
    :type: string
    :param sp_nq: SP Name Qualifier
    :type: string
    :param sp_format: SP Format
    :type: string
    :param debug: Activate the xmlsec debug
    :type: bool
    :returns: DOMElement | XMLSec nameID
    :rtype: string
    """
    doc = Document()
    name_id_container = doc.createElementNS(SAMLConstants.NS_SAML,
                                            'container')
    name_id_container.setAttribute("xmlns:saml",
                                   SAMLConstants.NS_SAML)

    name_id = doc.createElement('saml:NameID')
    name_id.setAttribute('SPNameQualifier', saml_client.sp_entity_id)
    name_id.setAttribute('Format', saml_client.name_id_format)
    name_id.appendChild(doc.createTextNode(value))
    name_id_container.appendChild(name_id)

    return doc.saveXML(name_id)
