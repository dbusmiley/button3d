"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""
from base64 import b64encode
from lxml import etree  # nosec: We don't read input with this
from defusedxml.lxml import fromstring
from xml.dom.minidom import Document  # nosec: We don't read input with this
from apps.b3_auth_plugins.methods.samlv2.logoutrequest.utils import \
    generate_name_id
from apps.b3_auth_plugins.methods.samlv2.authnrequest.utils import \
    generate_unique_id, deflate_and_base64_encode, parse_time_to_SAML, now


class SAMLLogoutRequest(object):
    """
    This class handles a Logout Request.
    """

    def __init__(self, saml_client, user_email=None):
        """
        Constructs the Logout Request object.
        """

        uid = generate_unique_id()
        self.id = uid

        issue_instant = parse_time_to_SAML(now())
        name_id_obj = generate_name_id(
            saml_client=saml_client,
            value=user_email,
        )
        logout_request = """<samlp:LogoutRequest
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    ID="%(id)s"
    Version="2.0"
    IssueInstant="%(issue_instant)s"
    Destination="%(single_logout_url)s">
    <saml:Issuer>%(entity_id)s</saml:Issuer>
    %(name_id)s
    %(session_index)s
</samlp:LogoutRequest>""" % \
            {
                'id': uid,
                'issue_instant': issue_instant,
                'single_logout_url': saml_client.single_logout_endpoint,
                'entity_id': saml_client.sp_entity_id,
                'name_id': name_id_obj,
                'session_index': '',
            }
        self.__logout_request = logout_request

    def get_request(self, deflate=True):
        """
        Returns the Logout Request deflated, base64encoded
        :param deflate: It makes the deflate process optional
        :type: bool
        :return: Logout Request maybe deflated and base64 encoded
        :rtype: str object
        """
        if deflate:
            request = deflate_and_base64_encode(self.__logout_request)
        else:
            request = b64encode(self.__logout_request)
        return request

    def get_xml(self):
        """
        Returns the XML that will be sent as part of the request
        or that was received at the SP
        :return: XML request body
        :rtype: string
        """
        return self.__logout_request

    @staticmethod
    def get_id(request):
        """
        Returns the ID of the Logout Request
        :param request: Logout Request Message
        :type request: string|DOMDocument
        :return: string ID
        :rtype: str object
        """
        if isinstance(request, etree._Element):
            elem = request
        else:
            if isinstance(request, Document):
                request = request.toxml()
            elem = fromstring(request)
        return elem.get('ID', None)
