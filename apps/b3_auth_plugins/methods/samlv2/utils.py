import urllib.parse

from apps.b3_auth_plugins.models import SAMLClientConfig
from apps.b3_auth_plugins.methods.samlv2.authnrequest.create_authrequest \
    import SAMLAuthenticationRequest
from apps.b3_auth_plugins.methods.samlv2.authnrequest.utils import \
    build_signature


def saml_generate_auth_url(auth_plugin, auth_url, next_redirect=None):
    try:
        saml_client = SAMLClientConfig.objects.get(plugin=auth_plugin)
    except SAMLClientConfig.DoesNotExist:
        return None

    saml_request = SAMLAuthenticationRequest(
        saml_client=saml_client, plugin=auth_plugin
    )
    signature = ''
    if saml_client.is_signing_enabled:
        signature = build_signature(
            saml_client=saml_client,
            saml_data=saml_request.get_request(),
            relay_state=next_redirect or False,
            saml_type='SAMLRequest'
        )
    query_string = {
        saml_client.saml_request_GET_param: saml_request.get_request(),
    }

    if next_redirect and saml_client.is_redirection_post_login_enabled:
        query_string.update(
            {saml_client.saml_relay_state_response_POST_param: next_redirect}
        )

    return f'{auth_url}&' \
        f'{urllib.parse.urlencode(query_string)}{signature}'
