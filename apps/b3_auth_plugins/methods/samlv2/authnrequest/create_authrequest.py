"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""
from base64 import b64encode

from apps.b3_auth_plugins.methods.samlv2.authnrequest.utils import \
    generate_unique_id, parse_time_to_SAML, now, deflate_and_base64_encode


class SAMLAuthenticationRequest(object):
    """
    This class handles an AuthNRequest. It builds an
    AuthNRequest object.
    """

    def __init__(
        self, saml_client, plugin, force_authn=False, is_passive=False,
        set_nameid_policy=True,
    ):
        """
        Constructs the AuthnRequest object.
        :param settings: OSetting data
        :type return_to: OneLogin_Saml2_Settings
        :param force_authn: Optional argument. When true the AuthNRequest
        will set the ForceAuthn='true'.
        :type force_authn: bool
        :param is_passive: Optional argument. When true the AuthNRequest will
        set the Ispassive='true'.
        :type is_passive: bool
        :param set_nameid_policy: Optional argument. When true the
        AuthNRequest will set a nameIdPolicy element.
        :type set_nameid_policy: bool
        """
        uid = generate_unique_id()
        self.__id = uid
        issue_instant = parse_time_to_SAML(now())
        provider_name_str = \
            "\n" + \
            '    ProviderName="%s"' % saml_client.attribute_consumer_name

        force_authn_str = ''
        if force_authn is True:
            force_authn_str = "\n" + '    ForceAuthn="true"'

        is_passive_str = ''
        if is_passive is True:
            is_passive_str = "\n" + '    IsPassive="true"'

        nameid_policy_str = """
    <samlp:NameIDPolicy
        Format="%s"
        AllowCreate="true" />""" % saml_client.name_id_format
        requested_authn_context_str = \
            "\n" + """    <samlp:RequestedAuthnContext Comparison="exact">
        <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:\
        classes:PasswordProtectedTransport</saml:AuthnContextClassRef>
    </samlp:RequestedAuthnContext>"""

        attr_consuming_service_str = 'AttributeConsumingServiceIndex="1"'

        request = """<samlp:AuthnRequest
    xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"
    xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"
    ID="%(id)s"
    Version="2.0"%(provider_name)s%(force_authn_str)s%(is_passive_str)s
    IssueInstant="%(issue_instant)s"
    Destination="%(destination)s"
    ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
    AssertionConsumerServiceURL="%(assertion_url)s"
    %(attr_consuming_service_str)s>
    <saml:Issuer>
    %(entity_id)s
    </saml:Issuer>
    %(nameid_policy_str)s%(requested_authn_context_str)s
</samlp:AuthnRequest>""" % \
            {
                'id': uid,
                'provider_name': provider_name_str,
                'force_authn_str': force_authn_str,
                'is_passive_str': is_passive_str,
                'issue_instant': issue_instant,
                'destination': plugin.auth_redirect_url,
                'assertion_url': saml_client.attribute_consume_endpoint,
                'entity_id': saml_client.sp_entity_id,
                'nameid_policy_str': nameid_policy_str,
                'requested_authn_context_str': requested_authn_context_str,
                'attr_consuming_service_str': attr_consuming_service_str
            }

        self.__authn_request = request

    def get_request(self, deflate=True):
        """
        Returns unsigned AuthnRequest.
        :param deflate: It makes the deflate process optional
        :type: bool
        :return: AuthnRequest maybe deflated and base64 encoded
        :rtype: str object
        """
        if deflate:
            request = deflate_and_base64_encode(self.__authn_request)
        else:
            request = b64encode(self.__authn_request)
        return request

    def get_id(self):
        """
        Returns the AuthNRequest ID.
        :return: AuthNRequest ID
        :rtype: string
        """
        return self.__id

    def get_xml(self):
        """
        Returns the XML that will be sent as part of the request
        :return: XML request body
        :rtype: string
        """
        return self.__authn_request
