"""
Inspired from https://github.com/onelogin/python-saml/ V2.3.0

Copyright (c) 2014, OneLogin, Inc.
Released under the MIT License.
"""
from hashlib import sha256
from uuid import uuid4
from datetime import datetime
import calendar
import base64
import zlib
import xmlsec
from base64 import b64decode

from base64 import b64encode
from apps.b3_auth_plugins.methods.samlv2.metadata.utils import SAMLConstants
from urllib.parse import quote_plus


def decode_saml_response(saml_response):
    return b64decode(saml_response)


def generate_unique_id():
    """
    Generates an unique string (used for example as ID for assertions).
    :return: A unique string
    :rtype: string
    """
    return 'THREEYOURMIND_%s' % sha256(uuid4().hex.encode()).hexdigest()[:40]


def parse_time_to_SAML(time):
    """
    Converts a UNIX timestamp to SAML2 timestamp on the form
    yyyy-mm-ddThh:mm:ss(\.s+)?Z.
    :param time: The time we should convert (DateTime).
    :type: string
    :return: SAML2 timestamp.
    :rtype: string
    """
    data = datetime.utcfromtimestamp(float(time))
    return data.strftime('%Y-%m-%dT%H:%M:%SZ')


def now():
    """
    :return: unix timestamp of actual time.
    :rtype: int
    """
    return calendar.timegm(datetime.utcnow().utctimetuple())


def deflate_and_base64_encode(value):
    """
    Deflates and then base64 encodes a string
    :param value: The string to deflate and encode
    :type value: string
    :returns: The deflated and encoded string
    :rtype: string
    """
    return base64.b64encode(zlib.compress(value.encode('utf-8'))[2:-4])


def build_signature(
    saml_client, saml_data, relay_state, saml_type,
    sign_algorithm=SAMLConstants.RSA_SHA1
):
    """
    Builds the Signature
    :param relay_state: The target URL the user should be redirected to
    :type relay_state: string
    :param saml_type: The target URL the user should be redirected to
    :type saml_type: string  SAMLRequest | SAMLResponse
    :param sign_algorithm: Signature algorithm method
    :type sign_algorithm: string
    """
    # Load the key into the xmlsec context
    key = saml_client.sp_private_key

    dsig_ctx = xmlsec.DSigCtx()
    dsig_ctx.signKey = xmlsec.Key.loadMemory(
        key, xmlsec.KeyDataFormatPem, None
    )

    msg = '%s=%s' % (saml_type, quote_plus(saml_data))
    if relay_state is not None:
        msg += '&RelayState=%s' % quote_plus(relay_state)
    msg += '&SigAlg=%s' % quote_plus(sign_algorithm)

    # Sign the metadata with our private key.
    sign_algorithm_transform_map = {
        SAMLConstants.DSA_SHA1: xmlsec.TransformDsaSha1,
        SAMLConstants.RSA_SHA1: xmlsec.TransformRsaSha1,
        SAMLConstants.RSA_SHA256: xmlsec.TransformRsaSha256,
        SAMLConstants.RSA_SHA384: xmlsec.TransformRsaSha384,
        SAMLConstants.RSA_SHA512: xmlsec.TransformRsaSha512
    }
    sign_algorithm_transform = sign_algorithm_transform_map.get(
        sign_algorithm, xmlsec.TransformRsaSha1)

    signature = dsig_ctx.signBinary(str(msg), sign_algorithm_transform)
    return b64encode(signature)
