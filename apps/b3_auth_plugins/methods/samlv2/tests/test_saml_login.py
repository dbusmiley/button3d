import os
import json
import base64

from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import CountryFactory

from apps.b3_auth_plugins.factories import SiteAuthPluginFactory, \
    AttributeMappingsFactory, SAMLAuthPluginFactory
from apps.b3_address.models import Address


BASE_DIR = os.path.dirname(__file__)
SAML_AUTH_RESPONSE = os.path.join(BASE_DIR, 'data/saml_login_request.xml')
SAML_AUTH_RESPONSE_WITH_EMAIL = os.path.join(
    BASE_DIR, 'data/saml_login_request_with_email_map.xml'
)

ATTRIBUTES_MAP = os.path.join(BASE_DIR, 'data/attribute_mappings.json')
ATTRIBUTES_MAP_WITH_MAIL = os.path.join(
    BASE_DIR, 'data/attribute_mappings_with_mail.json'
)


class SAMLLoginTest(TestCase):
    def setUp(self):
        super(SAMLLoginTest, self).setUp()
        self.request_xml = open(SAML_AUTH_RESPONSE).read()
        self.request_xml_with_email = open(
            SAML_AUTH_RESPONSE_WITH_EMAIL
        ).read()
        self.attributes_map = json.load(open(ATTRIBUTES_MAP))
        self.attributes_map_with_mail = json.load(
            open(ATTRIBUTES_MAP_WITH_MAIL)
        )

    def test_post_saml_response(self):
        country = CountryFactory(alpha2='DE')
        country.save()

        auth_plugin = SiteAuthPluginFactory()
        auth_plugin.save()
        saml_client = SAMLAuthPluginFactory(plugin=auth_plugin)
        saml_client.save()

        for item in self.attributes_map:
            attribute_map = AttributeMappingsFactory(
                plugin=auth_plugin, user_field=item['user_field'],
                mapping=item['mapping'])
            attribute_map.save()

        # Now activate this plugin for the current organization
        self.site.organization.auth_plugins_enabled = auth_plugin
        self.site.organization.save()

        response = self.client.post(
            '/auth/saml2/login',
            data={'SAMLResponse': base64.b64encode(
                self.request_xml.encode('utf-8')).decode()},
            follow=True
        )
        # Verify if login worked
        self.assertTrue(response.context['user'].is_active)

        # Now check if the user attributes were correct
        logged_in_user = response.context['user']
        self.assertEqual(logged_in_user.first_name, 'FirstName')
        self.assertEqual(logged_in_user.last_name, 'LastName')

        user_address = Address.objects.get(user=logged_in_user)
        self.assertIsNotNone(user_address)
        self.assertEqual(user_address.line1, 'Goerzalle')
        self.assertEqual(user_address.line2, '135')
        self.assertEqual(user_address.company_name, 'Berlin')
        self.assertEqual(user_address.city, 'Berlin')
        self.assertEqual(user_address.state, 'Berlin')
        self.assertEqual(user_address.zip_code, '12207')
        self.assertEqual(user_address.phone_number, '+4915908181012')
        self.assertEqual(user_address.title, 'Mr')

    def test_with_email_in_map_field(self):
        country = CountryFactory(alpha2='DE')
        country.save()

        auth_plugin = SiteAuthPluginFactory()
        auth_plugin.save()
        saml_client = SAMLAuthPluginFactory(plugin=auth_plugin)
        saml_client.save()

        for item in self.attributes_map_with_mail:
            attribute_map = AttributeMappingsFactory(
                plugin=auth_plugin, user_field=item['user_field'],
                mapping=item['mapping'])
            attribute_map.save()

        # Now activate this plugin for the current organization
        self.site.organization.auth_plugins_enabled = auth_plugin
        self.site.organization.save()

        response = self.client.post(
            '/auth/saml2/login',
            data={'SAMLResponse': base64.b64encode(
                self.request_xml_with_email.encode('utf-8')).decode()},
            follow=True
        )
        # Verify if login worked
        self.assertTrue(response.context['user'].is_active)

        # Now check if the user attributes were correct
        logged_in_user = response.context['user']
        self.assertEqual(logged_in_user.username, 'testinguserforSAML')
        self.assertEqual(logged_in_user.email, 'testinguser@3yourmind.com')
        self.assertEqual(logged_in_user.first_name, 'FirstName')
        self.assertEqual(logged_in_user.last_name, 'LastName')

        user_address = Address.objects.get(user=logged_in_user)
        self.assertIsNotNone(user_address)
        self.assertEqual(user_address.line1, 'Goerzalle')
        self.assertEqual(user_address.line2, '135')
        self.assertEqual(user_address.company_name, 'Berlin')
        self.assertEqual(user_address.city, 'Berlin')
        self.assertEqual(user_address.state, 'Berlin')
        self.assertEqual(user_address.zip_code, '12207')
        self.assertEqual(user_address.phone_number, '+4915908181012')
        self.assertEqual(user_address.title, 'Mr')
