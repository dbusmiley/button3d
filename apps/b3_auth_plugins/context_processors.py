from apps.b3_organization.utils import get_current_org


def auth_context_processor(request):
    return {
        'auth_plugin': get_current_org().auth_plugins_enabled,
        'disable_normal_login': get_current_org().disable_normal_login
    }
