from apps.b3_auth_plugins.methods.oauth.utils import oauth_generate_auth_url
from apps.b3_auth_plugins.methods.samlv2.utils import saml_generate_auth_url
from apps.b3_auth_plugins.utils.replace_all import replace_all
from apps.b3_core.utils import get_country
from button3d import type_declarations as td
import typing as t


def generate_auth_url(
    request: td.HttpRequest,
    auth_plugin: td.SiteAuthPluginConfig,
    next_redirect: t.Optional[str] =None
) -> t.Optional[str]:
    replace_dict = {
        '{country}': get_country(request).lower()
    }
    auth_url = replace_all(
        auth_plugin.auth_redirect_url,
        replace_dict
    )

    if auth_plugin.type == 'oauth':
        return oauth_generate_auth_url(
            auth_plugin=auth_plugin,
            auth_url=auth_url
        )
    if auth_plugin.type == 'saml2':
        return saml_generate_auth_url(
            auth_plugin=auth_plugin,
            auth_url=auth_url,
            next_redirect=next_redirect
        )
    if auth_plugin.type == 'jwt':
        return auth_url

    return None
