from apps.b3_organization.models import Organization
from apps.b3_auth_plugins.models import SiteAuthPluginConfig
from button3d import type_declarations as td
import typing as t


def fetch_auth_method(
        request: td.HttpRequest,
        method: str) -> \
        t.Optional[td.SiteAuthPluginConfig]:
    try:
        curr_host = request.get_host()
    except KeyError:
        # pipelines can produce a keyerror here
        return False

    if curr_host.__contains__(':'):
        # for development purposes
        curr_host = curr_host.split(':')[0]

    # Find out what organization caused this, and check if there is a
    # method setup for the same.
    curr_org = Organization.objects.get(site__domain=curr_host)
    # There cannot be more than one method for a given site
    try:
        return SiteAuthPluginConfig.objects.get(
            organization=curr_org, type=method
        )
    except SiteAuthPluginConfig.DoesNotExist:
        return False
