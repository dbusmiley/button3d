import re

from django.contrib.auth.models import User


def generate_unique_username(email):
    """
    Create a unique username out of given email address. Username algorithm
    adapted from django-allauth.utils.generate_unique_username
    """
    max_length = User._meta.get_field('username').max_length
    username_from_email = email.split('@')[0].strip()
    username = re.sub('\s+', '_', username_from_email)
    i = 0
    while True:
        if i:
            pfx = str(i + 1)
        else:
            pfx = ''
        ret = username[0:max_length - len(pfx)] + pfx
        try:
            User.objects.get(username=ret)
        except User.DoesNotExist:
            return ret

        i += 1
