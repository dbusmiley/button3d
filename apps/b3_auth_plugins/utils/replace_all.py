def replace_all(text, dict):
    """
    Used as a substitution for f-string, because f-strings are not seccure
    for user provided string.
    """
    for i, j in dict.items():
        text = text.replace(i, j)
    return text
