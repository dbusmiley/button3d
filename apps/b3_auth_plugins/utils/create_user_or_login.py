from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User

from apps.b3_auth_plugins.utils.generate_unique_username import \
    generate_unique_username
import typing as t
from button3d import type_declarations as td


def create_user_or_login(
    request: td.HttpRequest,
    user_email: str,
    first_name: t.Optional[str] =None,
    last_name: t.Optional[str] =None,
    username: t.Optional[str] =None
) -> t.Tuple[bool, td.User]:
    created = False

    user = authenticate(email=user_email, is_external_login=True)
    if not user:
        username = generate_unique_username(user_email) if not username else \
            username
        # Create a new user and force login
        user = User.objects.create_user(
            username=username,
            email=user_email,
            first_name=first_name,
            last_name=last_name,
        )
        user.set_unusable_password()
        user = authenticate(email=user_email, is_external_login=True)
        created = True

    login(request, user)
    return created, user
