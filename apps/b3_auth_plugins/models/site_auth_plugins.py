from django.db import models
from django.utils.translation import ugettext_lazy as _


AUTH_PLUGINS = (
    ('oauth', 'oauth'),
    ('ldap', 'ldap'),
    ('saml2', 'saml2'),
    ('jwt', 'jwt')
)


class SiteAuthPluginConfig(models.Model):
    name = models.CharField(
        _('Plugin Name'),
        max_length=50,
        blank=True
    )
    type = models.CharField(
        _('Oauth Type'),
        max_length=5,
        choices=AUTH_PLUGINS,
        default='oauth'
    )
    template_button_name = models.CharField(
        _('Template button name'),
        max_length=15,
        default="Login"
    )
    auth_redirect_url = models.CharField(
        help_text='Template button redirect URL. '
        'For SAML, this can be IDP SSO url as well. '
        'You can use {country} here to inject a'
        '2-letter country code',
        max_length=500,
        blank=False,
        default="/accounts/login/"
    )
    is_active = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name
