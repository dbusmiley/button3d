from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_auth_plugins.models.site_auth_plugins import SiteAuthPluginConfig


class SAMLClientConfig(models.Model):
    plugin = models.ForeignKey(
        SiteAuthPluginConfig,
        related_name='saml_auth_plugin'
    )
    idp_certificate = models.TextField(
        help_text=_('X509 Certifcate of your IDP')
    )
    sp_private_key = models.TextField(
        blank=True,
        null=True,
        help_text=_('Private key to sign metadata')
    )
    sp_certificate = models.TextField(
        blank=True,
        null=True,
        help_text=_('X509 Certificate of your SP')
    )
    is_signing_enabled = models.BooleanField(
        default=False,
        help_text=_(
            'if enabled, the metadata will be appended with a signature with '
            'the private key above'
        )
    )
    is_pki_check_enabled = models.BooleanField(
        default=False,
        help_text=_(
            'If enabled, an additional check for PKI values also will be done'
        )
    )
    is_redirection_post_login_enabled = models.BooleanField(
        default=False,
        help_text=_(
            'If enabled, a user will be redirected to pre-login page after '
            'SAML authentication succeeds.'
        )
    )
    authentication_class_ref = models.CharField(
        max_length=100, blank=True, null=True,
        default='urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI',
        help_text=_('Authentication Context Class Ref')
    )
    sp_entity_id = models.CharField(
        max_length=100,
        help_text=_('SAML SP entity ID')
    )
    attribute_consume_endpoint = models.CharField(
        max_length=150,
        help_text=_(
            'Your login URL'
        ),
        default='http://yourapp.com/auth/saml2/login'
    )
    single_sign_in_binding = models.CharField(
        max_length=150,
        help_text=_('Sign in binding format, use default if not known'),
        default='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST'
    )
    saml_request_GET_param = models.CharField(
        max_length=30,
        help_text=_(
            'HTTP GET variable of SAML Requests to IDP during login/logout'
        ),
        default='SAMLRequest'
    )
    saml_sign_in_response_POST_param = models.CharField(
        max_length=30,
        help_text=_(
            'HTTP POST variable of SAML response in assertions from IdP after '
            'login'
        ),
        default='SAMLResponse'
    )
    saml_relay_state_response_POST_param = models.CharField(
        max_length=30,
        help_text=_(
            'HTTP POST variable of SAML response in assertions from IdP after '
            'login for RelayState'
        ),
        default='RelayState'
    )
    single_logout_endpoint = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text=_('Your Logout URL'),
        default='http://yourapp.com/auth/saml2/logout/'
    )
    single_logout_binding = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text=_('Sign out URL binding format, use default if not known'),
        default='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect'
    )
    attribute_consumer_name = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text=_('Your Organization name'),
        default='3yourmind'
    )
    organization_url = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text=_('Your organization url'),
        default='http://app.3yourmind.com'
    )
    name_id_format = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        help_text=_('Name ID format, use default if not known'),
        default='urn:oasis:names:tc:SAML:2.0:nameid-format:transient'
    )
