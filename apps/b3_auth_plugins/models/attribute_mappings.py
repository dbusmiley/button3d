from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_auth_plugins.models import SiteAuthPluginConfig

USER_DETAILS_ATTRIBUTES = (
    ('title', 'title'),
    ('first_name', 'first_name'),
    ('last_name', 'last_name'),
    ('company_name', 'company_name'),
    ('vat_id', 'vat_id'),
    ('department', 'department'),
    ('line1', 'line1'),
    ('line2', 'line2'),
    ('zip_code', 'zip_code'),
    ('city', 'city'),
    ('state', 'state'),
    ('country_id', 'country_id'),
    ('phone_number', 'phone_number'),
    ('email', 'email')
)


class AttributeMappings(models.Model):
    plugin = models.ForeignKey(
        SiteAuthPluginConfig,
        related_name='auth_plugin_mapping'
    )
    user_field = models.CharField(
        max_length=20,
        choices=USER_DETAILS_ATTRIBUTES,
        help_text=_('User attribute that is being mapped to')
    )
    mapping = models.CharField(
        max_length=150,
        help_text=_('Mapping for this attribute in the auth response')
    )
