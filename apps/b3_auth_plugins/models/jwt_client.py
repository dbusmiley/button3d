from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_auth_plugins.models.site_auth_plugins import SiteAuthPluginConfig


class JWTClientConfig(models.Model):
    plugin = models.ForeignKey(
        SiteAuthPluginConfig,
        related_name='jwt_auth_plugin'
    )
    idp_public_key = models.TextField(
        help_text=_('Public key of your JWT IDP')
    )
    login_GET_param = models.CharField(
        max_length=30,
        help_text=_('JWT response GET param'), default='token'
    )
