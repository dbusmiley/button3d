from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.b3_auth_plugins.models.site_auth_plugins import SiteAuthPluginConfig


class OauthClientConfig(models.Model):
    plugin = models.ForeignKey(
        SiteAuthPluginConfig,
        related_name='oauth_auth_plugin'
    )
    client_id = models.CharField(
        _('Client ID'),
        max_length=100,
        blank=True
    )
    secret = models.CharField(
        _('Client Secret'),
        max_length=100,
        blank=True
    )
    callback_url = models.CharField(
        _('Callback URL'),
        max_length=500,
        blank=True
    )
    scope = models.CharField(
        _('Scopes'),
        max_length=100,
        blank=True
    )
    auth_token_param = models.CharField(
        _('Token Param'),
        max_length=50,
        blank=True,
        default='code'
    )
    get_token_url = models.CharField(
        _('Get Accessd Token URL'),
        max_length=100,
        blank=True,
        help_text="The URL to get the token. You can use"
                  "{country} in the URL which will be replaced"
                  "with the 2-letter county code."
    )

    use_implicit_token_grant = models.BooleanField(
        default=False,
        help_text="If set to true, the client_id will not be sent in the POST "
                  "request to fetch param, and would be mentioned in the "
                  "Authorization headers instead"
    )
    get_details_auth_headers_has_modifications = models.BooleanField(
        default=False,
        help_text="Default value will be Authorization: token OAUTH-TOKEN"
    )
    get_details_auth_headers_modifications = models.TextField(
        _('Local modifications to get details Authorization header'),
        max_length=400,
        blank=True,
        help_text="The value of the Authorization Header. You can use"
                  "{client_id} and {access_token}"
    )
    get_details_url = models.CharField(
        _('Get Details URL'),
        max_length=100,
        blank=True,
        help_text="You can use {country} to insert the current country"
                  "in the URL"
    )
    extra_param = models.CharField(
        _('Extra param name'),
        max_length=100,
        blank=True
    )

    def __unicode__(self):
        return str(self.plugin)

    def __str__(self):
        return str(self.plugin)
