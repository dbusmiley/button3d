# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-09-07 16:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_auth_plugins', '0003_samlclientconfig_is_redirection_post_login_enabled'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_auth_headers_modifications',
            field=models.TextField(blank=True, help_text='The value of the Authorization Header. You can use{client_id} and {access_token}', max_length=400),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_auth_headers_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will bedirectly parsed to an exec() command.', max_length=100, null=True, verbose_name='Import statement for local modificationsto get details request Authorization headers'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url',
            field=models.CharField(blank=True, help_text='You can use {country} to insert the current countryin the URL', max_length=100, verbose_name='Get Details URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url_has_modifications',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available param: 'request' The current user request ", max_length=400, null=True, verbose_name='Local modifications to get details URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directlyparsed to an exec() command.', max_length=100, null=True, verbose_name='Import statement for local modifications to get details URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url',
            field=models.CharField(blank=True, help_text='The URL to get the token. You can use{country} in the URL which will be replacedwith the 2-letter county code.', max_length=100),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url_has_modifications',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available param: 'request' The current user request ", max_length=400, null=True, verbose_name='Local modifications to get token URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, null=True, verbose_name='Import statement for local modifications to get token URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='user_email_field_has_modifications',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='user_email_field_modifications',
            field=models.TextField(blank=True, help_text='Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Response available in user_info_request_headers_response', max_length=400, null=True, verbose_name='Code to parse Email address out of the response'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url',
            field=models.CharField(default='/accounts/login/', help_text='Template button redirect URL. For SAML, this can be IDP SSO url as well. You can use {country} here to inject a2-letter country code', max_length=500),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url_has_modifications',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available params: 'request': The current user request ", max_length=400, null=True, verbose_name='Local modifications to auth URL'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, null=True, verbose_name='Import statement for local modifications to auth URL'),
        ),
    ]
