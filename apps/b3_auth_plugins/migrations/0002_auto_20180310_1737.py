# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-03-10 17:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('b3_auth_plugins', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attributemappings',
            name='user_field',
            field=models.CharField(choices=[('first_name', 'first_name'), ('last_name', 'last_name'), ('title', 'title'), ('phone_number', 'phone_number'), ('line1', 'line1'), ('line2', 'line2'), ('line3', 'line3'), ('line4', 'line4'), ('state', 'state'), ('postcode', 'postcode'), ('country_id', 'country_id'), ('email', 'email')], help_text='User attribute that is being mapped to', max_length=20),
        ),
        migrations.AlterField(
            model_name='jwtclientconfig',
            name='login_GET_param',
            field=models.CharField(default='token', help_text='JWT response GET param', max_length=30),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='auth_token_param',
            field=models.CharField(blank=True, default='code', max_length=50, verbose_name='Token Param'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_auth_headers_has_modifications',
            field=models.BooleanField(default=False, help_text='Default value will be Authorization: token OAUTH-TOKEN'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_auth_headers_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available param: 'request' The current user request. This goes to Authorization: header. Available params: 'access_token': The Oauth2.0 access token. 'client_id': The current client id", max_length=400, verbose_name='Local modifications to get details Authorization header'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_auth_headers_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, verbose_name='Import statement for local modifications to get details request Authorization headers'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available param: 'request' The current user request ", max_length=400, verbose_name='Local modifications to get details URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_details_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, verbose_name='Import statement for local modifications to get details URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available param: 'request' The current user request ", max_length=400, verbose_name='Local modifications to get token URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='get_token_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, verbose_name='Import statement for local modifications to get token URL'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='use_implicit_token_grant',
            field=models.BooleanField(default=False, help_text='If set to true, the client_id will not be sent in the POST request to fetch param, and would be mentioned in the Authorization headers instead'),
        ),
        migrations.AlterField(
            model_name='oauthclientconfig',
            name='user_email_field_modifications',
            field=models.TextField(blank=True, help_text='Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Response available in user_info_request_headers_response', max_length=400, verbose_name='Code to parse Email address out of the response'),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='attribute_consume_endpoint',
            field=models.CharField(default='http://yourapp.com/auth/saml2/login', help_text='Your login URL', max_length=150),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='attribute_consumer_name',
            field=models.CharField(blank=True, default='3yourmind', help_text='Your Organization name', max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='authentication_class_ref',
            field=models.CharField(blank=True, default='urn:oasis:names:tc:SAML:2.0:ac:classes:SmartcardPKI', help_text='Authentication Context Class Ref', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='name_id_format',
            field=models.CharField(blank=True, default='urn:oasis:names:tc:SAML:2.0:nameid-format:transient', help_text='Name ID format, use default if not known', max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='organization_url',
            field=models.CharField(blank=True, default='http://app.3yourmind.com', help_text='Your organization url', max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='saml_request_GET_param',
            field=models.CharField(default='SAMLRequest', help_text='HTTP GET variable of SAML Requests to IDP during login/logout', max_length=30),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='saml_sign_in_response_POST_param',
            field=models.CharField(default='SAMLResponse', help_text='HTTP POST variable of SAML response in assertions from IdP after login', max_length=30),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='single_logout_binding',
            field=models.CharField(blank=True, default='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect', help_text='Sign out URL binding format, use default if not known', max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='single_logout_endpoint',
            field=models.CharField(blank=True, default='http://yourapp.com/auth/saml2/logout/', help_text='Your Logout URL', max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='samlclientconfig',
            name='single_sign_in_binding',
            field=models.CharField(default='urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST', help_text='Sign in binding format, use default if not known', max_length=150),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url',
            field=models.CharField(default='/accounts/login/', max_length=500, verbose_name='Template button redirect URL. For SAML, this can be IDP SSO url as well'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url_modifications',
            field=models.TextField(blank=True, help_text="Warning: This field will be directly parsed to an eval() command. Please insert only one single line. Available params: 'request': The current user request ", max_length=400, verbose_name='Local modifications to auth URL'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='auth_redirect_url_modifications_import',
            field=models.CharField(blank=True, help_text='Warning: This field will be directly parsed to an exec() command.', max_length=100, verbose_name='Import statement for local modifications to auth URL'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='template_button_name',
            field=models.CharField(default='Login', max_length=15, verbose_name='Template button name'),
        ),
        migrations.AlterField(
            model_name='siteauthpluginconfig',
            name='type',
            field=models.CharField(choices=[('oauth', 'oauth'), ('ldap', 'ldap'), ('saml2', 'saml2'), ('jwt', 'jwt')], default='oauth', max_length=5, verbose_name='Oauth Type'),
        ),
    ]
