from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_auth_plugins.factories import SiteAuthPluginFactory


class TestAuthPluginRedirection(TestCase):
    def setUp(self):
        super(TestAuthPluginRedirection, self).setUp()
        auth_plugin = SiteAuthPluginFactory()
        self.site.organization.auth_plugins_enabled = auth_plugin
        self.site.organization.save()

    def test_redirection_on_signup_links(self):
        response_without_sso = self.client.get(
            '/en/accounts/signup/', follow=True
        )
        self.assertNotEqual(
            response_without_sso.template_name[0], 'account/login.html'
        )
        # Now activate this plugin for the current organization
        self.site.organization.disable_normal_login = True
        self.site.organization.save()
        response = self.client.get(
            '/en/accounts/signup/', follow=True
        )
        self.assertEqual(response.template_name[0], 'account/login.html')

        response = self.client.get(
            '/en/accounts/ps_signup/', follow=True
        )
        self.assertEqual(response.template_name[0], 'account/login.html')
