import logging
from django import template

from apps.b3_auth_plugins.utils.generate_auth_url import generate_auth_url

register = template.Library()
logger = logging.getLogger(__name__)


@register.simple_tag(takes_context=True)
def modify_auth_redirect_url(context, value):
    request = context['request']
    auth_url = None
    next = request.GET.get('next', None)
    try:
        auth_url = generate_auth_url(
            request, auth_plugin=value, next_redirect=next)
    except Exception as e:
        logger.warning('Modifying urls in templatetags raised: {0}'.format(e))

    return auth_url
