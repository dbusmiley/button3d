from django.contrib import admin

from apps.b3_auth_plugins.models import SiteAuthPluginConfig, \
    OauthClientConfig, SAMLClientConfig, AttributeMappings, JWTClientConfig


class OauthClientConfigAdmin(admin.ModelAdmin):
    list_display = ('plugin', )


class SiteAuthPluginConfigAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', )


class SAML2ClientConfigAdmin(admin.ModelAdmin):
    list_display = ('plugin',)


class AttributeMappingsAdmin(admin.ModelAdmin):
    list_display = ('plugin', 'user_field', 'mapping',)


class JWTClientConfigAdmin(admin.ModelAdmin):
    list_display = ('plugin',)


admin.site.register(SAMLClientConfig, SAML2ClientConfigAdmin)
admin.site.register(OauthClientConfig, OauthClientConfigAdmin)
admin.site.register(SiteAuthPluginConfig, SiteAuthPluginConfigAdmin)
admin.site.register(AttributeMappings, AttributeMappingsAdmin)
admin.site.register(JWTClientConfig, JWTClientConfigAdmin)
