{% extends 'b3_modal/modal_base.html' %}
{% load i18n %}

{% block modal_id %}downloadUploadModal{% endblock %}

{% block modal_title %}
    {% blocktrans %}
        Download file as
        <span data-bind="if: multicolor">ZIP</span>
        <span data-bind="ifnot: multicolor">STL</span>
    {% endblocktrans %}
{% endblock %}

{% block modal_body %}
    {% blocktrans %}File to download: {% endblocktrans %}
    <span data-bind="text: showname"></span></br>
    {% if enable_optimized_downloads %}
        <a data-bind="click: download, if: showOptimizedDownload">
            {% blocktrans %}
                Download optimized and scaled 3D-Model as
                <span data-bind="if: multicolor">ZIP</span><span data-bind="ifnot: multicolor">STL</span>
            {% endblocktrans %}
        </a></br>
    {% endif %}
    <a data-bind="click: downloadOriginal">{% blocktrans %}Download original 3D-Model as <span data-bind="text: filetype" style="text-transform: uppercase">{% endblocktrans %}</span></a>
    <br>
{% endblock %}

{% block modal_footer %}
    <div class="btn b3-btn-black" id="confirm-download-button" data-dismiss="modal"> {% blocktrans %}Close{% endblocktrans %}</div>
{% endblock %}

{% block modal_logic %}
    {% comment %}
    Invisible IFrame for Downloads
    See http://stackoverflow.com/questions/3749231/download-file-using-javascript-jquery
    Download IFrame must be placed outside the modal, so it's still there when modal gets destroyed.
    {% endcomment %}
    <iframe id="magic_download_iframe" class="hide"></iframe>

    <script type="text/javascript">
        function DownloadUploadVM() {
            var self = this;
            self.rest_url = ko.observable();
            self.showname = ko.observable();
            self.status = ko.observable();
            self.download_optimized_url = ko.observable();
            self.filetype = ko.observable();
            self.download_original_url = ko.observable();
            self.multicolor = ko.observable();

            self.ShowDownloadModal = function(rest_url, scale) {
                console.log(scale);
                if((typeof show_signup == 'undefined') || show_signup === false ){
                    $.ajax({
                        url: rest_url,
                        success: function (data) {
                            self.showname(data.showname);
                            self.rest_url(rest_url);
                            self.status(data.status.status);
                            self.filetype(data.filetype);
                            self.download_optimized_url(data.download_optimized_url + "?scale=" + scale);
                            self.download_original_url(data.download_original_url);
                            self.multicolor(data.multicolor);
                            $("#downloadUploadModal").modal('show');
                        },
                        error: function(){
                            console.log('Something went wrong!!!');
                        }
                    });
                } else {
                    Button3d.modals.showSignup();
                }
            };

            self.showOptimizedDownload = ko.pureComputed(function () {
                return self.status() === "finished";
            }, self);

            self.download = function(){
                $("#magic_download_iframe")[0].src = self.download_optimized_url();
            };
            self.downloadOriginal = function(){
                $('#magic_download_iframe')[0].src = self.download_original_url();
            }

            // Public members
            self.publicMembers = {
                "show_download_upload_modal": self.ShowDownloadModal
            };
        }

        ko.applyBindings(new DownloadUploadVM(), $("#downloadUploadModal")[0]);
    </script>
{% endblock %}
