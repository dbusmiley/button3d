# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-09 10:58


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('b3_organization', '0001_initial'),
        ('address', '0002_auto_20180109_1058'),
        ('sites', '0002_alter_domain_unique'),
    ]

    operations = [
        migrations.AddField(
            model_name='organizationshippingaddress',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='shipping_addresses', to='b3_organization.Organization'),
        ),
        migrations.AddField(
            model_name='organizationshippingaddress',
            name='site',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site'),
        ),
        migrations.AddField(
            model_name='organizationbillingaddress',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='address.Country', verbose_name='Country'),
        ),
        migrations.AddField(
            model_name='organizationbillingaddress',
            name='organization',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='billing_address', to='b3_organization.Organization'),
        ),
        migrations.AddField(
            model_name='organizationbillingaddress',
            name='site',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='sites.Site'),
        ),
    ]
