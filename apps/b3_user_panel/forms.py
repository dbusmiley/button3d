from django.forms.models import ModelForm
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class ChangePasswordForm(ModelForm):
    oldpassword = forms.CharField(
        label=_("Current Password"),
        widget=forms.PasswordInput(
            attrs={'placeholder': _("Current Password")})
    )
    password1 = forms.CharField(
        label=_("New Password"),
        widget=forms.PasswordInput(
            attrs={'placeholder': _("New Password")})
    )
    password2 = forms.CharField(
        label=_("New Password (again)"),
        widget=forms.PasswordInput(
            attrs={'placeholder': _("New Password (again)")})
    )

    def clean_oldpassword(self):
        if not self.user.check_password(self.cleaned_data.get("oldpassword")):
            raise forms.ValidationError(_("Please type your current"
                                          " password."))
        return self.cleaned_data["oldpassword"]

    def clean_password2(self):
        if "password1" in self.cleaned_data and \
                "password2" in self.cleaned_data:
            if self.cleaned_data["password1"] != \
                    self.cleaned_data["password2"]:
                raise forms.ValidationError(_("You must type the same password"
                                              " each time."))
        return self.cleaned_data["password2"]

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['password2'])
        if commit:
            self.user.save()
        return self.user

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = ('oldpassword', 'password1', 'password2', )
