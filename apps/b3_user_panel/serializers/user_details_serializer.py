from django.contrib.auth import get_user_model
from drf_payload_customizer.mixins import PayloadConverterMixin
from rest_framework import serializers


class UserDetailSerializer(
        PayloadConverterMixin, serializers.ModelSerializer):
    avatar = serializers.URLField(source='userprofile.avatar_url')
    can_use_service_panel = serializers.BooleanField(
        source='userprofile.can_use_service_panel'
    )

    class Meta:
        model = get_user_model()
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'avatar',
            'can_use_service_panel',
        )
