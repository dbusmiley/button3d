from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.partner.models import Partner


class PrintingServiceUserPanelSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):

    class Meta:
        model = Partner
        fields = (
            'id',
            'name',
        )
