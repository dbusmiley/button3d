from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_order.models import Order


class OrderUserPanelListSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):

    number_of_lines = serializers.SerializerMethodField()
    number_of_comments = serializers.SerializerMethodField()
    status = serializers.CharField(source='status.type')

    def get_number_of_lines(self, obj) -> int:
        return obj.lines.count()

    def get_number_of_comments(self, obj) -> int:
        # TODO: Implement when comments are added to Order Model
        return 0

    class Meta:
        model = Order
        fields = (
            'id',
            'title',
            'number',
            'datetime_placed',
            'customer_reference',
            'partner_name',
            'currency',
            'total_value',
            'total_tax',
            'status',
            'payment_method_name',
            'number_of_lines',
            'number_of_comments',
            'delivery_days',
        )
