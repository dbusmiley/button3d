from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_address.serializers import AddressSerializer
from apps.b3_api.services.projects.attachments.serializers import \
    OrderAttachmentSerializer
from apps.b3_api.services.projects.lines.serializers import \
    LineDetailsWithAttachmentSerializer
from apps.b3_order.models import Order
from apps.b3_order.serializers.order_status_serialiser import \
    OrderStatusSerializer
from apps.b3_shipping.serializers.pickup_location import \
    PickupLocationSerializer
from apps.partner.serializers.partner_serializer import PartnerSerializer


class OrderUserPanelDetailSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    billing_address = AddressSerializer()
    shipping_address = AddressSerializer()

    status = serializers.CharField(source='status.type')

    shipping_method_name = serializers.CharField(
        required=False, source='shipping_method.name'
    )
    pickup_location = PickupLocationSerializer()

    lines = LineDetailsWithAttachmentSerializer(many=True)
    attachments = OrderAttachmentSerializer(many=True)

    invoice_prefix = serializers.CharField(source='latest_invoice.note_prefix')
    invoice_number = serializers.CharField(
        source='latest_invoice.invoice_number'
    )
    invoice_document = serializers.CharField(
        source='latest_invoice.document.url'
    )

    partner = PartnerSerializer()
    payment_method_name = serializers.CharField(
        source='payment.payment_method.name'
    )
    statuses = OrderStatusSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            'id',
            'title',
            'number',
            'datetime_placed',
            'customer_reference',
            'currency',
            'tax_rate',
            'min_price_diff_value',
            'min_price_diff_tax',
            'min_price_value',
            'min_price_tax',
            'fees_value',
            'fees_tax',
            'subtotal_value',
            'subtotal_tax',
            'shipping_value',
            'shipping_tax',
            'total_value',
            'total_tax',
            'voucher_discount_value',
            'voucher_discount_tax',
            'tracking_information',
            'delivery_instructions',
            'project',
            'billing_address',
            'shipping_address',
            'partner',
            'payment_method_name',
            'voucher',
            'shipping_method_name',
            'pickup_location',
            'delivery_days',
            'status',
            'lines',
            'attachments',
            'invoice_prefix',
            'invoice_number',
            'invoice_document',
            'statuses',
        )
