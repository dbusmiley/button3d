from django.http import Http404, JsonResponse, QueryDict
from django.shortcuts import render

from apps.b3_address.models import Address
from apps.b3_address.verification_mails import \
    update_email_verifiers_on_action_done

from apps.b3_organization.utils import get_current_org
import apps.b3_user_panel.permissions as user_permissions
from django.views.decorators.http import require_GET, require_POST
from apps.b3_user_panel.views import user_panel_pagination_context


@require_GET
@user_permissions.check_user_permission
def user_addresses_pending_verifications_view(request):
    org = get_current_org()
    if not org or not org.force_verify_customer_address:
        raise Http404()
    if not org.verify_customer_address_users.filter(
            pk=request.user.id).exists():
        raise Http404()

    address_class = Address
    template = 'b3_user_panel/user_addresses_pending_verifications.html'

    addresses = address_class.objects.filter(
        user__userprofile__site_id=org.site.id)

    filter_status = request.GET.get(
        "filter_verified_status", address_class.ADDRESS_PENDING)

    url_query_dict = QueryDict()
    if filter_status:
        if filter_status not in \
                dict(address_class.ADDRESS_VERIFIED_STATUSES).keys():
            raise Http404
        addresses = addresses.filter(verified_status=filter_status)
        url_query_dict = QueryDict("filter_verified_status=" + filter_status)

    # only for pagination
    context = user_panel_pagination_context(
        request, addresses.count(), url_query_dict)

    page_first_item_number = context["paging"]["page_first_item_number"]
    page_last_item_number = context["paging"]["page_last_item_number"]
    user_addresses_list = list(
        addresses.all()[page_first_item_number:page_last_item_number])

    context["user_addresses"] = user_addresses_list
    context["organization"] = org

    return render(
        request,
        template,
        context)


@require_POST
@user_permissions.check_user_permission
def user_addresses_pending_verifications_update_view(request):
    org = get_current_org()
    if not org or not org.force_verify_customer_address:
        raise Http404
    if not org.verify_customer_address_users.filter(
            pk=request.user.pk).exists():
        raise Http404()

    user_address_id = int(request.POST['id'])
    new_status = request.POST['new_status']

    if new_status not in dict(Address.ADDRESS_VERIFIED_STATUSES).keys():
        raise Http404

    user_address = Address.objects.filter(
        user__userprofile__site_id=org.site.id).get(id=user_address_id)

    user_address.verified_status = new_status
    user_address.save()

    # send an update about this event to all users
    update_email_verifiers_on_action_done(
        org=org, user_id=user_address.user.id,
        verified_by=request.user
    )
    return JsonResponse({"success": True, "new_status": new_status})
