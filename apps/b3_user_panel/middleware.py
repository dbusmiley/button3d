from django.shortcuts import redirect
from django.urls import reverse

from apps.b3_address.models import Address
from apps.b3_core.base import BaseMiddleware
from apps.b3_organization.utils import get_current_org


class ForceVerifyCustomerAddressMiddleware(BaseMiddleware):
    def __call__(self, request):
        org = get_current_org()
        if not org.force_verify_customer_address:
            return self.get_response(request)
        if request.user.is_anonymous or request.user.is_staff:
            return self.get_response(request)
        if org.verify_customer_address_users.filter(
                pk=request.user.pk).exists():
            return self.get_response(request)
        path_names = [
            "logout", "verification_pending",
            "account_logout",
            "account_email_signup_set_password",
            "account_email_extended_registration",
            "account_email_extended_registration_addresses"
        ]
        white_urls = [reverse(path_name) for path_name in path_names]

        # cannot reverse without id, cut id from ending
        white_urls += [
            reverse("account_email_extended_registration_addresses_delete",
                    kwargs={"user_address_id": 1})[:-2]]
        white_urls += [reverse("account_email_signup_confirm",
                               kwargs={"key": 1})[:-2]]

        if any([request.path.startswith(x) for x in white_urls]):
            return self.get_response(request)

        addresses = Address.objects.filter(user=request.user)
        if not addresses:
            # force_verify_customer_address was recently activated
            # Give user a chance to add his first address
            return redirect('account_email_extended_registration_addresses')

        if not addresses.filter(verified_status=Address.ADDRESS_VERIFIED):
            return redirect("verification_pending")

        return self.get_response(request)
