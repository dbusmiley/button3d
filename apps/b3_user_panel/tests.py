from django.urls import reverse

from apps.b3_address.factories import CountryFactory
from apps.b3_order.factories import OrderFactory
from apps.b3_order.models import Order
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.clients import ThreeydClient
from apps.b3_tests.factories import StlFileFactory, \
    UserFactory, StockRecordFactory, RequestMock, \
    PartnerFactory, PostProcessingFactory, ColorFactory, BasketFactory,\
    BasketLineFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket
from django.template import Context, Template


class SettingsPageTest(AuthenticatedTestCase):
    def test_change_password(self):
        # Open settings page
        response = self.client.get(
            reverse('b3_user_panel:settings'), follow=True
        )
        self.assertTrue(
            '<h4>Change Password</h4>' in response.content.decode('utf-8')
        )
        self.assertTrue(
            'Current Password:' in response.content.decode('utf-8')
        )
        self.assertTrue('New Password:' in response.content.decode('utf-8'))
        self.assertTrue(
            'New Password (again):' in response.content.decode('utf-8')
        )

        # Submit password change
        data = {
            'oldpassword': self.test_password,
            'password1': 'new_password',
            'password2': 'new_password'
        }
        response = self.client.post(
            '/en/accounts/password/change/', data=data, follow=True
        )
        self.assertEqual(response.status_code, 200)

        # Logout
        response = self.client.get('/en/accounts/logout/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.user.email in response.content.decode('utf-8'))

        # Login again
        data = {'login': self.user.email, 'password': 'new_password'}
        response = self.client.post(
            reverse('account_login'), data=data, follow=True
        )
        self.assertTrue(self.user.email in response.content.decode('utf-8'))
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode('utf-8')
        )


class ProjectDetailPageTest(AuthenticatedTestCase):
    def test_pricing_owner_accessibility(self):
        # Owner can access and edit
        basket = BasketFactory(owner=self.user)
        self.client.get(basket.url, follow=True)
        self.assertFalse(basket.has_manual_pricing_request_sent)
        self.assertFalse(basket.is_manually_priced)

        # Set for pricing
        UserFactory(username='pricer')
        basket.set_for_manual_pricing()

        # User cannot modify
        self.client.get(basket.url, follow=True)
        self.assertTrue(basket.has_manual_pricing_request_sent)
        self.assertFalse(basket.is_manually_priced)

    def test_redirect_project_detail(self):
        basket = BasketFactory(owner=self.user)

        # Not logged in, access basket
        self.client.logout()
        response = self.client.get(basket.url, follow=True)
        self.assertNotEqual(response.status_code, 404)
        self.assertTrue('login' in response.resolver_match.url_name)

    def test_upload_redirect_to_project_and_open_stlfile(self):
        self.organization.single_upload_redirects_to_project_detail = True
        self.organization.save()
        stlfile = StlFileFactory(owner=None, is_opened=False)

        url = reverse('upload', args=(stlfile.uuid,))
        self.client.get(url, follow=True)

        stlfile.refresh_from_db()
        self.assertTrue(stlfile.is_opened)
        self.assertEqual(stlfile.owner, self.user)

    def test_enabled_optimized_downloads(self):
        self.assertTrue('Download optimized and scaled 3D-Model'
                        in self.download_test(True).content.decode())

    def test_restricted_optimized_downloads(self):
        self.assertFalse('Download optimized and scaled 3D-Model'
                         in self.download_test(False).content.decode())

    def download_test(self, is_enabled):
        self.organization.enable_optimized_file_download = is_enabled
        self.organization.save()
        b = BasketFactory(owner=self.user)
        return self.client.get(
            reverse('b3_user_panel:project-detail', args=(b.id,)),
            follow=True
        )

    def test_open_stlfile_when_accessing_project(self):
        b = BasketFactory(owner=self.user)
        stlfile = StlFileFactory(owner=None, is_opened=False)

        b.add_empty_line(stlfile, quantity=1, scale=1)

        url = reverse('b3_user_panel:project-detail', args=(b.id,))
        self.client.get(url, follow=True)

        stlfile.refresh_from_db()
        self.assertTrue(stlfile.is_opened)
        self.assertEqual(stlfile.owner, self.user)


class PricingUserAccessTest(AuthenticatedTestCase):
    def setUp(self):
        super(PricingUserAccessTest, self).setUp()
        self.pricer = UserFactory(  # nosec
            email='pricer@3yourmind.com', password='pricerpass'
        )

    def test_pricer_access_basket(self):
        b = BasketFactory(owner=self.user)
        pricing_user = UserFactory(  # nosec
            email='pricer@3yourmind.com', password='testerpass'
        )
        partner = PartnerFactory()
        partner.users.add(pricing_user)
        partner.save()
        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.always_priced_manually = True
        stockrecord.save()

        new_line = b.add_empty_line(StlFileFactory(), quantity=1, scale=1)

        update_dictionary = {
            "product_slug": stockrecord.product.slug,
            "supplier_slug": partner.code,
            'stockrecord': stockrecord
        }
        new_line.update(update_dictionary)

        b.set_for_manual_pricing()
        b.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        b.save()
        b.refresh_from_db()

        self.client.logout()
        login_response = self.client.login(  # nosec
            email=pricing_user.email, password='testerpass'
        )
        self.assertTrue(login_response)
        response = self.client.get(b.url, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_pricer_cannot_access_basket_before_pricing(self):
        b = BasketFactory(owner=self.user)
        self.client.logout()
        self.assertTrue(
            self.client.login(  # nosec
                email=self.pricer.email, password='pricerpass'
            )
        )
        response = self.client.get(b.url, follow=True)
        self.assertEqual(response.status_code, 403)

    def test_pricer_access_line_stlfile(self):
        pricer_user = UserFactory(  # nosec
            email='pricer@3yourmind.com', password='testerpass'
        )
        sr = StockRecordFactory()
        sr.partner.users.add(pricer_user)
        sr.partner.save()

        b = BasketFactory(owner=self.user)
        l, _ = b.add_product(
            StlFileFactory(owner=self.user), sr.product, stockrecord=sr
        )

        b.set_for_manual_pricing()
        self.client.logout()
        self.assertTrue(
            self.client.login(  # nosec
                email=pricer_user.email, password='testerpass'
            )
        )
        response = self.client.get(l.configure_url, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_pricer_cannot_access_line_before_pricing(self):
        sr = StockRecordFactory()

        b = BasketFactory(owner=self.user)
        l, _ = b.add_product(
            StlFileFactory(owner=self.user), sr.product, stockrecord=sr
        )

        self.client.logout()
        self.assertTrue(
            self.client.login(  # nosec
                email=self.pricer.email, password='pricerpass'
            )
        )
        response = self.client.get(l.configure_url, follow=True)
        self.assertEqual(response.status_code, 403)

    def test_pricer_download_line_stlfile(self):
        sr = StockRecordFactory()
        pricing_user = UserFactory(  # nosec
            email='pricer@3yourmind.com', password='pricerpass'
        )
        sr.partner.users.add(pricing_user)
        sr.partner.save()
        stlfile = StlFileFactory(owner=self.user)

        b = BasketFactory(owner=self.user)
        l, _ = b.add_product(stlfile, sr.product, stockrecord=sr)

        b.set_for_manual_pricing()
        self.client.logout()
        self.assertTrue(
            self.client.login(  # nosec
                email=pricing_user.email, password='pricerpass'
            )
        )

        request = RequestMock().get(stlfile.download_optimized_url)
        request.user = pricing_user
        self.assertTrue(stlfile.can_download(request))

    def test_pricer_cannot_download_line(self):
        sr = StockRecordFactory()
        stlfile = StlFileFactory(owner=self.user)

        b = BasketFactory(owner=self.user)
        l, _ = b.add_product(stlfile, sr.product, stockrecord=sr)

        self.client.logout()
        self.assertTrue(
            self.client.login(  # nosec
                email=self.pricer.email, password='pricerpass'
            )
        )

        request = RequestMock().get(stlfile.download_optimized_url)
        request.user = self.pricer
        self.assertFalse(stlfile.can_download(request))


class OrderProjectTest(AuthenticatedTestCase):
    client_class = ThreeydClient

    def test_error_message_empty_basket(self):
        b = BasketFactory(owner=self.user)
        response = self.client.get(
            reverse('b3_user_panel:order-project', args=(b.id,)), follow=True
        )
        self.assertTrue(
            '<title>3YOURMIND - 3D Project' in
            response.content.decode('utf-8')
        )
        self.assertTrue(
            '3D Project must contain at least one model.' in
            response.content.decode('utf-8')
        )

    def test_error_message_lines_cannot_be_ordered(self):
        b = BasketFactory(owner=self.user)
        b.add_empty_line(StlFileFactory(owner=self.user))

        response = self.client.get(
            reverse('b3_user_panel:order-project', args=(b.id,)), follow=True
        )
        self.assertTrue(
            '<title>3YOURMIND - 3D Project' in
            response.content.decode('utf-8')
        )
        self.assertTrue(
            'Some models cannot be ordered.' in
            response.content.decode('utf-8')
        )


class ProjectQuotationTest(AuthenticatedTestCase):
    def test_fallback_not_enough_information(self):
        stock_record = self.create_stock_record()
        content = self.get_project_quotation(
            stock_record, post_processings=None
        )
        self.assertTrue(
            'Payment terms: Credit Card or 14 days after invoicing' in content
        )
        self.assertTrue('Quotation of: ' in content)

    def test_printing_service_without_vat(self):
        stock_record = self.create_stock_record()
        content = self.get_project_quotation(
            stock_record, post_processings=None
        )
        self.assertFalse('VAT ID: ' in content)

    def test_quotation_with_postprocessing(self):
        stock_record = self.create_stock_record()
        color_1 = ColorFactory(rgb='#000000')
        color_2 = ColorFactory(rgb='#111111')
        post_processing = PostProcessingFactory(
            stock_record=stock_record,
            colors=[color_1, color_2]
        )
        post_processings = {
            'post_processings': [{
                'post_processing': post_processing,
                'color': color_1
            }]
        }
        title = post_processing.title
        content = self.get_project_quotation(stock_record, post_processings)
        self.assertTrue(title in content)

    def test_not_all_lines_can_be_ordered(self):
        b = BasketFactory(owner=self.user)
        b.add_empty_line(StlFileFactory(owner=self.user))

        response = self.client.get(
            reverse('b3_user_panel:project-quotation', args=(b.id,)),
            follow=True
        )
        self.assertTrue(
            '<title>3YOURMIND - 3D Project' in
            response.content.decode('utf-8')
        )
        self.assertTrue(
            'Invalid 3D Project' in response.content.decode('utf-8')
        )

    def get_project_quotation(self, stock_record, post_processings):
        self.user.user_address.all().delete()
        self.project = BasketLineFactory(
            basket__owner=self.user,
            stockrecord__partner=stock_record.partner
        ).basket
        countries = (
            CountryFactory(
                alpha2='DE',
            ),
        )
        ShippingMethodFactory(
            partner=stock_record.partner, countries=countries
        )

        if post_processings:
            line = self.project.first_line
            line.update(post_processings)
            line.refresh_from_db()

        url = reverse(
            'b3_user_panel:project-quotation',
            args=(self.project.pk,)
        )
        response = self.client.get(url, follow=True)
        self.assertTrue(self.project.title in response.content.decode('utf-8'))
        self.assertEqual(
            str(response.context['total']['vat_rate']), '19.00000'
        )
        return response.content.decode('utf-8')

    @staticmethod
    def create_stock_record():
        test_partner = PartnerFactory()
        test_partner.save()
        stock_record = StockRecordFactory()
        stock_record.partner = test_partner
        stock_record.save()
        return stock_record

    def test_get_quotation_for_empty_basket(self):
        basket = BasketFactory(owner=self.user)
        url = reverse('b3_user_panel:project-quotation', args=(basket.id,))
        response = self.client.get(url, follow=True)
        self.assertTrue(response.status_code not in (404, 500))
        self.assertTrue(
            'Invalid 3D Project. Cannot create quotation.' in
            response.content.decode('utf-8')
        )

    def test_get_correct_shipping_method_in_quotation(self):
        stock_record = self.create_stock_record()
        project = BasketLineFactory(
            basket__owner=self.user,
            stockrecord__partner=stock_record.partner
        ).basket
        countries = (
            CountryFactory(
                alpha2='DE',
            ),
        )
        usa = CountryFactory(
            alpha2='US',
        )
        ShippingMethodFactory(
            partner=stock_record.partner,
        )
        ShippingMethodFactory(
            partner=stock_record.partner,
        )
        ShippingMethodFactory(countries=countries)
        ShippingMethodFactory(countries=(usa, ))
        ShippingMethodFactory(
            countries=countries,
            shipping_days_min=2,
            price_excl_tax=1,
        )
        ShippingMethodFactory(
            countries=countries,
            shipping_days_min=2,
            price_excl_tax=10000,
        )
        project.add_product(
            StlFileFactory(site=self.site),
            stock_record.product,
            stockrecord=stock_record
        )

        url = reverse('b3_user_panel:project-quotation', args=(project.id, ))
        response = self.client.get(url, follow=True)

        self.assertEqual(
            response.context.get('delivery_time_string'), '6 - 12 days'
        )
        self.assertEqual(
            str(response.context.get('shipping_method_price_incl_tax')),
            '11.90'
        )


class TemplateTest(AuthenticatedTestCase):
    def setUp(self):
        super(TemplateTest, self).setUp()

    def test_currency_symbol_tag(self):
        template = '{% load currency_tags %}' \
                   '{{active_currency|currency_with_symbol}}'
        output = u'kr NOK'
        self.tag_test(template, output)

    def tag_test(self, template, output):
        t = Template(template)
        c = Context({'active_currency': 'NOK'})
        self.assertEqual(t.render(c), output)


class ServiceListUserPanel(AuthenticatedTestCase):
    def test_service_list_empty_when_no_order_placed(self):
        response = self.client.get('/api/v2.0/user-panel/services/')
        self.assertEqual([], response.data)

    def test_service_list_has_item(self):
        OrderFactory(purchased_by=self.user)
        self.assertEqual(1, Order.objects.count())
        response = self.client.get('/api/v2.0/user-panel/services/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(
            [{'id': Order.objects.first().partner_id,
              'name': 'Test Partner'}],
            response.data
        )

    def test_single_supplier_organization(self):
        self.organization.is_single_supplier_account = True
        self.organization.save()
        OrderFactory(purchased_by=self.user)
        self.assertEqual(1, Order.objects.count())
        response = self.client.get('/api/v2.0/user-panel/services/')
        self.assertEqual(400, response.status_code)
        expected = {
            'code': 'SINGLE_SUPPLIER_ORGANIZATION',
            'message': 'This organization does not allow printing '
                       'service listing',
            'moreInfo': None}
        self.assertEqual(
            expected,
            response.data
        )
