import operator
import typing as t
from decimal import Decimal, DecimalException
from functools import reduce

from django.db import models
from django_filters import rest_framework as filters
from rest_framework.compat import distinct
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings

import button3d.type_declarations as td
from apps.b3_api.mixins import PaginatedViewMixin
from apps.b3_order.models import Order
from apps.b3_user_panel.serializers.order_user_panel_list_serializer \
    import OrderUserPanelListSerializer


class OrderFilter(filters.FilterSet):
    datetime_placed = filters.DateFromToRangeFilter()
    status = filters.CharFilter(field_name='current_status__type')
    partner_name = filters.CharFilter(field_name='partner__name')

    class Meta:
        model = Order
        fields = ['partner_name', 'datetime_placed', 'status']


def convert_decimal(candidate: str) -> t.Optional[Decimal]:
    try:
        return Decimal(candidate)
    except DecimalException:
        return None


class OrderListSearchBackend(SearchFilter):
    def filter_queryset(self,
                        request: td.HttpRequest,
                        queryset: td.DjangoQuerySet,
                        view: td.DjangoView):
        """
        If search terms are convertable to a numeric value, then we also
        filter on order total (both excl. and incl. matches)
        """
        search_fields = getattr(view, 'search_fields', None)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset

        orm_lookups = [
            self.construct_search(str(search_field))
            for search_field in search_fields
        ]

        base = queryset
        conditions = []
        add_total_incl_tax = False
        for search_term in search_terms:
            queries = [
                models.Q(**{orm_lookup: search_term})
                for orm_lookup in orm_lookups
            ]
            number = convert_decimal(search_term)
            if number is not None:
                add_total_incl_tax = True
                queries.append(
                    models.Q(total_value=number) |
                    models.Q(total_incl_tax=number)
                )

            conditions.append(reduce(operator.or_, queries))

        if add_total_incl_tax:
            queryset = queryset.annotate(
                total_incl_tax=models.F('total_value') + models.F('total_tax')
            )
        queryset = queryset.filter(reduce(operator.and_, conditions))
        if self.must_call_distinct(queryset, search_fields):
            # Filtering against a many-to-many field requires us to
            # call queryset.distinct() in order to avoid duplicate items
            # in the resulting queryset.
            # We try to avoid this if possible, for performance reasons.
            queryset = distinct(queryset, base)

        return queryset


class OrderUserPanelList(PaginatedViewMixin, ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderUserPanelListSerializer
    filter_class = OrderFilter
    filter_backends = api_settings.DEFAULT_FILTER_BACKENDS + [
        OrderListSearchBackend
    ]
    search_fields = (
        'number',
        'customer_reference',
        'partner__name',
    )
    ordering_field_mapping = {
        'status': 'current_status__type',
    }
    ordering_fields = (
        'status',
        'total_value',
        'datetime_placed',
        'customer_reference',
        'number',
    )

    def get_queryset(self) -> td.DjangoQuerySet:
        return Order.objects.filter(purchased_by=self.request.user)
