from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_user_panel.serializers.user_details_serializer import \
    UserDetailSerializer


class UserDetailView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserDetailSerializer
    allowed_methods = ('get',)

    def get_object(self):
        return self.request.user
