from django.http import Http404
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_order.models import Order
from apps.b3_user_panel.serializers.order_user_panel_detail_serializer \
    import OrderUserPanelDetailSerializer


class OrderUserPanelDetail(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderUserPanelDetailSerializer
    lookup_url_kwarg = 'order_id'

    def get_order(self) -> Order:
        try:
            return Order.objects.get(
                id=self.kwargs['order_id'], purchased_by=self.request.user
            )
        except Order.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='ORDER', object_id=self.kwargs['order_id']
                )
            )

    def get_object(self) -> Order:
        return self.get_order()
