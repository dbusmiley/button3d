import copy
import json
import logging
from collections import namedtuple

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, Http404, get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views import generic
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.http import require_GET, require_POST
from guardian.shortcuts import assign_perm

import apps.b3_user_panel.permissions as user_permissions
from apps.b3_address.forms import UserPanelAddressForm
from apps.b3_address.models import Address
from apps.b3_checkout.constants import PAYMENT_METHODS
from apps.b3_compare import views as compare_views
from apps.b3_core import permissions
from apps.b3_core.utils import get_country, currency_exchange, \
    determine_default_currency
from apps.b3_migration.models import Switch
from apps.b3_organization.models import OrganizationProductOption
from apps.b3_organization.utils import get_current_org
from apps.b3_user_panel.factories.ProjectQuotationUserDictionaryFactory \
    import ProjectQuotationUserDictionaryFactory
from apps.b3_user_panel.forms import ChangePasswordForm
from apps.basket.models import Basket, Line
from apps.basket.permissions import get_basket_access
from apps.catalogue.models.category import Category
from apps.catalogue.models.color import Color
from apps.catalogue.models.product import Product

logger = logging.getLogger(__name__)
Pair = namedtuple('Pair', ['label', 'value'])


@user_permissions.check_user_permission
@require_GET
def projects_view(request):
    switch = Switch.is_active(Switch.NEW_USER_PANEL)
    context = {
        'new_user_panel_switch': switch,
        'num_projects': Basket.search(owner=request.user)[1],
        'view_name': request.resolver_match.url_name
    }
    return render(
        request,
        'b3_user_panel/projects/projects.html',
        context=context
    )


@require_GET
@get_basket_access
def project_detail_view_quotation(request, basket, *args, **kwargs):
    by_quotation = \
        request.path == reverse(
            'b3_user_panel:project-quotation', args=(basket.id,))
    if not basket.can_be_edited and not by_quotation:
        raise Http404('No editable basket found')
    date = timezone.now()

    try:
        if basket.is_empty:
            raise ValueError(
                ("Basket should not be empty when "
                 "getting quote for a 3D project"))
        if not basket.all_lines_can_be_ordered():
            raise ValueError('Not all basket lines can be ordered')
    except BaseException:
        logger.warning('Cannot get project quotation', exc_info=True)
        return redirect(
            reverse(
                'b3_user_panel:project-detail', args=(basket.id,)) +
            '?error=' + _('Invalid 3D Project. Cannot create quotation.'))

    partner = basket.partner

    country = get_country(request)

    if hasattr(basket, 'order'):
        shipping_method = basket.order.get_shipping_method()
        vat_rate = basket.order.vat_rate
    else:
        shipping_method = partner.get_fastest_shipping_method(country)
        vat_rate = partner.org_options.vat_rate

    if shipping_method:
        delivery_time = basket.delivery_time_all_lines(shipping_method)
    else:
        delivery_time = None

    currency = request.GET.get('currency', basket.currency)
    try:
        currency_exchange(1.0, basket.currency, currency)
    except KeyError:  # Make sure currency exists, if not, fallback
        currency = basket.currency

    basket_price = basket.price_object_incl_discounts.as_dict(
        currency=currency
    )

    shipping_exclusive_tax_value, shipping_inclusive_tax_value = 0, 0

    if shipping_method:
        shipping_exclusive_tax_value = shipping_method.price_excl_tax
        shipping_inclusive_tax_value = shipping_method.price_incl_tax

    if hasattr(basket, 'order') and hasattr(basket.order, 'shipping_excl_tax'):
        shipping_exclusive_tax_value = basket.order.shipping_price.excl_tax
        shipping_inclusive_tax_value = basket.order.shipping_price.incl_tax

    shipping_excl_tax = currency_exchange(
        shipping_exclusive_tax_value,
        basket.currency,
        currency
    )
    shipping_incl_tax = currency_exchange(
        shipping_inclusive_tax_value,
        basket.currency,
        currency
    )

    total = {
        'vat': basket_price['tax'] + (shipping_incl_tax - shipping_excl_tax),
        'tax_type': partner.get_tax_type(),
        'vat_rate': vat_rate,
        'subtotal': basket.subtotal_price_object.as_dict(
            currency=currency
        ),
        'net_price': basket_price['excl_tax'] + shipping_excl_tax,
        'total_price': basket_price['incl_tax'] + shipping_incl_tax
    }

    attachments_file_names = [
        attachment.filename
        for attachment in basket.attachments.all()
    ]
    enclosure_text = ', '.join(attachments_file_names)

    lines = [{
        'quantity': l.quantity,
        'showname': l.showname,
        'product': l.product,
        'file_color': l.color,
        'scale': l.scale,
        'measure_unit': l.measure_unit,
        'dimensions_in_unit': l.dimensions_in_unit,
        'price_incl_tax': currency_exchange(
            l.price_incl_tax, l.partner.price_currency, currency),
        'price_excl_tax': currency_exchange(
            l.price_excl_tax, l.partner.price_currency, currency),
        'discount': l.discount_price_object.as_dict(currency=currency),
        'line_price_incl_tax': currency_exchange(
            l.line_price_incl_tax, l.partner.price_currency, currency),
        'line_price_excl_tax': currency_exchange(
            l.quantity * l.price_excl_tax, l.partner.price_currency, currency),
        'line_price_excl_tax_incl_discounts': currency_exchange(
            l.line_price_excl_tax_incl_discounts,
            l.partner.price_currency,
            currency
        ),
        'line_price_incl_tax_incl_discounts': currency_exchange(
            l.line_price_incl_tax_incl_discounts,
            l.partner.price_currency,
            currency
        ),
        'stockrecord': l.stockrecord,
        'post_processing_option_list': l.post_processing_options.all()
    } for l in basket.all_lines()]
    fees = basket.list_extra_fees(currency=currency)
    base_context = {
        'show_net_prices': get_current_org().show_net_prices,
        'partner': partner,
        'basket': basket,
        'fees': fees,
        'lines': lines,
        'date': date,
        'shipping_method_price_incl_tax': shipping_incl_tax,
        'shipping_method_price_excl_tax': shipping_excl_tax,
        'delivery_time_string': delivery_time,
        'total': total,
        'enclosuretext': enclosure_text,
        'project_id': basket.id,
        'currency': currency
    }
    user_dict = ProjectQuotationUserDictionaryFactory. \
        create_user_quotation_dictionary(request.user)
    base_context.update(user_dict)
    return render(
        request,
        'b3_user_panel/projects/project_quotation.html',
        base_context)


@require_GET
def order_basket(request, project_id):
    basket = get_object_or_404(Basket, pk=project_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise Http404()

    error_msg = None
    if basket.is_empty:
        error_msg = _('3D Project must contain at least one model.')
    elif not basket.all_lines_can_be_ordered():
        error_msg = _('Some models cannot be ordered.')
    elif basket.is_submitted:
        error_msg = _('Each 3D project can only be placed once.')
    if error_msg:
        logger.warning('Could not order basket because: ' + error_msg)
        return redirect(
            reverse('b3_user_panel:project-detail', args=(project_id,)) +
            '?error=' + error_msg)

    basket.prepare_for_checkout(request)

    return redirect('checkout:shipping-address')


@require_GET
@get_basket_access
def project_line_viewer(request, basket, line_id, uuid, *args, **kwargs):
    line = get_object_or_404(Line, pk=line_id, basket=basket)
    if line.uuid != uuid:
        raise Http404()

    return compare_views.compare_get(request, uuid, basket.id, line.id)


def assign_missing_permission_to_owner(basket):
    if not basket.owner:
        return
    if not basket.owner.has_perm(Basket.VIEW_PERMISSION):
        assign_perm(Basket.VIEW_PERMISSION, basket.owner, basket)
    if not basket.owner.has_perm(Basket.EDIT_PERMISSION):
        assign_perm(Basket.EDIT_PERMISSION, basket.owner, basket)
    if not basket.owner.has_perm(Basket.SHARE_PERMISSION):
        assign_perm(Basket.SHARE_PERMISSION, basket.owner, basket)
    if not basket.owner.has_perm(Basket.ORDER_PERMISSION):
        assign_perm(Basket.ORDER_PERMISSION, basket.owner, basket)


def get_vw_metadata_pairs(order):
    if not hasattr(order, 'payment'):
        return None
    vw_payment_slug = PAYMENT_METHODS.VW
    is_vw_payment = order.payment.payment_method.type_name == vw_payment_slug
    metadata = order.payment.metadata
    if metadata and is_vw_payment:
        pairs = [
            Pair(_('Creator'), metadata['creator_name']),
            Pair(_('E-Mail'), metadata['creator_email']),
            Pair(_('Service Provider'), metadata['provider']),
            Pair(_('Proposal Number'), metadata['proposal_number']),
            Pair(_('Proposal'), metadata['proposal']),
            Pair(_('Description'), metadata['description']),
            Pair(_('Delivery Date'), metadata['delivery_date']),
            Pair(_('Charge from'), metadata['charge_from']),
            Pair(_('Charge to'), metadata['charge_to'])
        ]
        return pairs
    return None


@require_GET
@get_basket_access
def project_detail_view(request, basket, *args, **kwargs):
    assign_missing_permission_to_owner(basket)
    for line in basket.lines.all():
        line.stl_file.assign_to_user_in_request(request)

    current_org = get_current_org()
    categories = json.dumps(get_categories())
    products_list = get_modified_products(current_org)

    products = json.dumps([{
        "id": p.id,
        "slug": p.slug,
        "title": p.title,
        "attr_strength": p.attr_strength,
        "attr_detail": p.attr_detail,
        "category": p.category.name,
        "technology": p.technology.title if p.technology else '',
        "attr_wall_min": p.attr_wall_min,
        "attr_wall_opt": p.attr_wall_opt,
        "description": p.description,
        "full_product_title": p.full_product_title,
        "is_multicolor": p.is_multicolor,
        "is_acid_resistant": p.is_acid_resistant,
        "is_corrosion_resistant": p.is_corrosion_resistant,
        "is_toxicity_resistant": p.is_toxicity_resistant,
        "is_uv_resistant": p.is_uv_resistant,
        "is_bio_compatible": p.is_bio_compatible,
        "attr_density_min": p.attr_density_min,
        "attr_density_max": p.attr_density_max,
        "attr_ultimate_tensile_strength_min":
            p.attr_ultimate_tensile_strength_min,
        "attr_ultimate_tensile_strength_max":
            p.attr_ultimate_tensile_strength_max,
        "attr_modulus_min": p.attr_modulus_min,
        "attr_modulus_max": p.attr_modulus_max,
        "attr_elongation_min": p.attr_elongation_min,
        "attr_elongation_max": p.attr_elongation_max,
        "attr_hardness_unit": p.attr_hardness_unit,
        "attr_hardness_min": p.attr_hardness_min,
        "attr_hardness_max": p.attr_hardness_max
    } for p in products_list])

    colors = json.dumps(
        list(Color.objects.values('id', 'title', 'rgb'))
    )
    order = getattr(basket, 'order', None)
    template = 'b3_user_panel/projects/project_detail.html'

    if order:
        status = order.get_status_email_text()
    else:
        status = None

    enable_optimized_downloads = current_org.enable_optimized_file_download

    context = {
        'should_redirect': request.user.is_anonymous,
        'vw_metadata': get_vw_metadata_pairs(order),
        'categories': categories,
        'products': products,
        'colors': colors,
        'project_id': basket.id,
        'service_id': basket.partner.id if basket.partner else None,
        'user_currency': request.session.get(
            'currency', determine_default_currency(request)
        ),
        'user_country': request.session.get('country', 'de'),
        'project_title': basket.title,
        'view_name': request.resolver_match.url_name,
        'basket': basket,
        'order': order,
        'status_message': status,
        'error': request.GET.get('error', None),
        'enable_optimized_downloads': enable_optimized_downloads
    }

    return render(
        request,
        template,
        context)


def get_categories():
    return list(Category.objects.values('id', 'depth', 'path', 'name'))


def get_modified_products(current_org):
    raw_product_list = Product.objects.select_related('technology').in_bulk()
    organization_product_options = list(
        OrganizationProductOption.objects.filter(
            site=current_org.site
        ).values('product_id', 'key', 'value')
    )

    for custom_mapping in organization_product_options:
        setattr(
            raw_product_list[custom_mapping['product_id']],
            custom_mapping['key'], custom_mapping['value']
        )
    return list(raw_product_list.values())


@user_permissions.check_user_permission
@require_GET
def settings_view(request):
    template = 'b3_user_panel/settings/settings.html'
    form = UserPanelAddressForm()

    # User address update here uses shipping form
    return render(
        request,
        template,
        {
            'address_form': form
        })


@user_permissions.check_user_permission
@require_POST
def user_address_view(request):
    form = UserPanelAddressForm(data=request.POST)
    if form.is_valid():
        address = form.save(commit=False)
        address.user = User.objects.get(id=request.user.id)
        address.save()
        return redirect(reverse('b3_user_panel:settings'))
    return render(
        request,
        'b3_user_panel/settings/settings.html',
        {'address_form': form}
    )


@user_permissions.check_user_permission
@permissions.require_DELETE
def delete_user_address_view(request, user_address_id):
    user_addr = get_object_or_404(Address, pk=user_address_id)
    user_addr.delete()
    return JsonResponse(
        {'success': True, 'url': reverse('b3_user_panel:settings')})


class NewUserAddressSettingsUpdateView(generic.UpdateView):
    template_name = 'b3_user_panel/settings/edit_user_address.html'
    form_class = UserPanelAddressForm
    success_url = reverse_lazy('b3_user_panel:settings')

    @method_decorator(user_permissions.check_user_permission)
    def dispatch(self, request, *args, **kwargs):
        return super(
            NewUserAddressSettingsUpdateView, self).dispatch(
            request, *args, **kwargs
        )

    def get_queryset(self):
        return self.request.user.user_address.filter(
            deleted_date__isnull=True
        )

    def form_valid(self, form):
        address = form.save(commit=False)
        address.user = User.objects.get(id=self.request.user.id)
        address.save()
        return HttpResponseRedirect(self.get_success_url())


# Decided to not to make template tag, it's easier to convert to ajax from this
# QueryDict is for search and filter support
def user_panel_pagination_context(
    request, objects_count, url_query_dict,
    items_per_page=10, links_per_page=5
):
    objects_max_pages = int((objects_count - 1) / items_per_page + 1)

    current_page_number = request.GET.get('page', '1')

    cur_page_index = max(1, min(int(current_page_number), objects_max_pages))

    range_min_pages = int(max(1, cur_page_index - links_per_page / 2))
    range_max_pages = int(
        min(objects_max_pages, cur_page_index + links_per_page / 2))

    page_first_item_number = ((cur_page_index - 1) * items_per_page)
    page_last_item_number = (cur_page_index * items_per_page)

    context = dict()

    context["page_first_item_number"] = page_first_item_number
    context["page_last_item_number"] = page_last_item_number

    context["first_url"] = "?" + url_query_dict.urlencode()

    context["visible"] = objects_max_pages > 1

    range_pages = []
    for page_number in range(range_min_pages, range_max_pages + 1):
        copy_url_query_dict = copy.deepcopy(url_query_dict)
        copy_url_query_dict["page"] = str(page_number)
        range_pages.append({"title": str(page_number),
                            "is_current": page_number == cur_page_index,
                            "url": "?" + copy_url_query_dict.urlencode()})

    context["range_urls"] = range_pages

    copy_url_query_dict = copy.deepcopy(url_query_dict)
    copy_url_query_dict["page"] = str(objects_max_pages)
    context["last_url"] = "?" + copy_url_query_dict.urlencode()

    return {"paging": context}


@login_required
@sensitive_post_parameters()
@csrf_protect
def change_password(request):
    if request.is_ajax() or request.method == "POST":
        form = ChangePasswordForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return JsonResponse(status=200, data={"success": True})
        else:
            return JsonResponse(status=500, data={"form_errors": form.errors})

    else:
        raise Http404()
