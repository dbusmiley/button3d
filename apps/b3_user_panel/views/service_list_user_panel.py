from django.db.models import Subquery
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.errors.custom_exceptions import ErrorResponse
from apps.b3_order.models import Order
from apps.b3_organization.utils import get_current_org
from apps.b3_user_panel.serializers.printing_service_user_panel_serializer \
    import PrintingServiceUserPanelSerializer
from apps.partner.models import Partner


class ServiceListUserPanel(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PrintingServiceUserPanelSerializer

    def get(self, request, *args, **kwargs):
        organization = get_current_org()
        if organization.is_single_supplier_account:
            raise ErrorResponse(
                'SINGLE_SUPPLIER_ORGANIZATION',
                'This organization does not allow printing service listing',
            )
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user_orders = Order.objects.filter(purchased_by=self.request.user)
        result = Partner.objects.filter(
            pk__in=Subquery(user_orders.values('partner_id'))
        )
        return result
