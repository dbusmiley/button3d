class ProjectQuotationUserDictionaryFactory:
    def __init__(self):
        pass

    @classmethod
    def create_user_quotation_dictionary(cls, user):
        """
        Method to create user specific dictionary containing
            information for the project quotation. Pure function to
        enable reusability.
        :param user: User of which the dictionary for the
            project quotation should be created.
        :return: A dictionary containing information about the
            user. If there are no information or the user is not
        authenticated it will return an empty dictionary rather than None.
        """
        user_dict = {}
        if not user.is_anonymous:
            user_dict['userprofile'] = user.userprofile
            user_dict['userlastname'] = user.last_name
            if user.user_address.first():
                user_dict['useraddress'] = user.user_address.first()
        return user_dict
