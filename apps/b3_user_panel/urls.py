from django.conf.urls import url
import apps.b3_user_panel.views as views
from apps.b3_user_panel.verification_views import \
    user_addresses_pending_verifications_view, \
    user_addresses_pending_verifications_update_view

urlpatterns = [
    url((r'projects/(?P<project_id>[0-9]+)/lines/'
         '(?P<line_id>[0-9]+)/u/(?P<uuid>[a-f0-9\-]+)'),
        views.project_line_viewer, name='project-line-viewer'),
    url(r'^projects/(?P<project_id>[0-9]+)/order/$',
        views.order_basket,
        name='order-project'),

    url(r'^projects/(?P<project_id>[0-9]+)/',
        views.project_detail_view,
        name='project-detail'),

    url(r'^projects/$',
        views.projects_view,
        name='projects'),

    url(r'^project-quotation/(?P<project_id>[0-9]+)/$',
        views.project_detail_view_quotation,
        name='project-quotation'),

    # ---- Below urls used for setting new address models in old UP ----
    url(
        r'^settings/user-address/$',
        views.user_address_view,
        name='user-address'),
    url(
        r'^settings/user-address/edit/(?P<pk>[0-9]+)/$',
        views.NewUserAddressSettingsUpdateView.as_view(),
        name='edit-user-address'),
    url(
        r'^settings/user-address/delete/(?P<user_address_id>[0-9]+)/$',
        views.delete_user_address_view,
        name='delete-user-address'),

    # --------------------------------------------------------------------
    url(r'^settings/', views.settings_view, name='settings'),

    url(r'^pending-verifications/$',
        user_addresses_pending_verifications_view,
        name='pending-verifications'),
    url(r'^pending-verifications/update/$',
        user_addresses_pending_verifications_update_view,
        name='pending-verifications-update'),
]
