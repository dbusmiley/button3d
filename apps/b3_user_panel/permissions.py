from django.urls import reverse
from django.shortcuts import redirect


def check_user_permission(function):
    def _check_user_permission(request, *args, **kwargs):
        if not request.user.is_active:
            login_url = reverse('account_login')
            return redirect('{0}?next={1}'.format(login_url, request.path))
        return function(request, *args, **kwargs)
    return _check_user_permission
