from decimal import Decimal as D

from django import template

from apps.b3_core.utils import get_timezone_from_ip, get_timezone_from_country

# Django template custom math filters
# Ref : https://code.djangoproject.com/ticket/361

register = template.Library()


def mult(value, arg):
    return D(value) * D(arg)


def sub(value, arg):
    return D(value) - D(arg)


def div(value, arg):
    return D(value) / D(arg)


def adddecimal(value, arg):
    return D(value) + D(arg)


@register.assignment_tag
def resolve_timezone_of_user_from_order(order):
    """
    Fetch timezone of a user from a given order. Try to fetch it from the user
    profile or later fallback to use IP
    :param order:
    :return:
    """
    try:
        user_country_code = \
            order.purchased_by.userprofile.country_from_address.alpha2
        return get_timezone_from_country(user_country_code)
    except AttributeError:
        return get_timezone_from_ip(order.customer_ip)


register.filter('mult', mult)
register.filter('sub', sub)
register.filter('div', div)
register.filter('adddecimal', adddecimal)
