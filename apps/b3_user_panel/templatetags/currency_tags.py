from django.template import Library
from apps.b3_core import utils
from decimal import Decimal as D
from decimal import InvalidOperation
from django.conf import settings
from apps.b3_core.utils import currency_exchange, determine_default_currency
from django.utils.translation import get_language, to_locale
from babel.numbers import format_currency


register = Library()


@register.filter(name='currency')
def currency(value, currency=None):
    """
    Format decimal value as currency
    """
    try:
        value = D(value)
    except (TypeError, InvalidOperation):
        return u""
    currency_format = getattr(settings, 'currency_format', None)
    kwargs = {
        'currency': currency or settings.DEFAULT_CURRENCY,
        'locale': to_locale(get_language() or settings.LANGUAGE_CODE)
    }
    if isinstance(currency_format, dict):
        kwargs.update(currency_format.get(currency, {}))
    else:
        kwargs['format'] = currency_format
    return format_currency(value, **kwargs)


@register.simple_tag
def convert_currency_from(val, from_c, request):
    to_currency = determine_default_currency(request)
    return currency_exchange(from_amount=val, from_c=from_c, to_c=to_currency)


@register.filter('currency_with_symbol')
def currency_with_symbol(currency):
    result = '{0} {1}'.format(
        utils.get_currency_symbol(currency),
        currency
    )
    return result
