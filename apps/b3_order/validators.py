from django.core.exceptions import ValidationError

from apps.b3_address.models import Address


def validate_address_does_not_belong_to_user_or_partner(value):
    address = Address.objects.get(id=value)
    if address.user or address.partner:
        raise ValidationError(
            f'Address with id:{value} should be a copy of an address and not '
            f'assigned to a user or a partner',
            params={'value': value},
        )
