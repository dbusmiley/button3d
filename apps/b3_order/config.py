from django.apps import AppConfig


class B3OrderConfig(AppConfig):
    name = 'apps.b3_order'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('Order'))
