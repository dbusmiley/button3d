from django.core.management import BaseCommand
from django.urls import reverse

from apps.b3_order.models import Order
from apps.b3_order.utils import diagnose_order


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-d', help='Override domain for order links')

    def handle(self, *args, **options):
        _domain = options['d']
        for order in Order.all_objects.iterator():
            result = diagnose_order(order)
            if result is not None:
                order, errors = result
                domain = _domain or order.site.domain
                path = reverse(
                    'admin:b3_order_order_change',
                    args=(order.id,)
                )

                url = f'http://{domain}{path}'

                errors_string = ', '.join(errors)
                print(f'{url} | {errors_string}')
