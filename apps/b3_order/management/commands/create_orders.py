import logging
import os
import random
import sys
from datetime import timedelta
from uuid import uuid4

import inquirer
import requests
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone

from apps.b3_checkout.models import PartnerPaymentMethod, Payment
from apps.b3_core.models import StlFile
from apps.b3_order.factories import OrderFactory, NO_PAYMENT
from apps.b3_order.utils import OrderNumberGenerator
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import BasketLineFactory, \
    VoucherFactory, BasketFactory, ParameterFactory
from apps.partner.models import Partner, StockRecord

logger = logging.getLogger(__name__)

DEFAULT_UPLOAD_URL = 'http://3dbackend:8080/upload'
DEFAULT_STL_FILE = 'apps/status_page/files/cube.stl'
RANDOMIZE_CHOICES = ['dates', 'stockrecords']
MINIMUM_REQUIRED_ARGS = ['qty', 'site', 'partner_id', 'owner_id']


class Command(BaseCommand):
    def __init__(self, stdout=None, stderr=None, no_color=False):
        super().__init__(stdout, stderr, no_color)
        self.options = {}
        self.site = None
        self.user = None
        self.partner = None
        self.site_string = None
        self.file_path = None
        self.upload_url = None

    def add_arguments(self, parser):
        parser.add_argument(
            '--qty',
            type=int,
            help='Number of orders to create.'
        )
        parser.add_argument(
            '--site',
            type=str,
            help='Name of the site that the order should be placed on'
        )
        parser.add_argument(
            '--partner_id',
            type=str,
            help='ID of the partner to use'
        )
        parser.add_argument(
            '--owner_id',
            type=str,
            help='ID of the user to assign the order to'
        )
        parser.add_argument(
            '--randomize',
            nargs='+',
            help='Which values to randomize',
            choices=RANDOMIZE_CHOICES
        )
        parser.add_argument(
            '--uploadurl',
            type=str,
            default=DEFAULT_UPLOAD_URL,
            help=f'Absolute url for 3d backend upload.'
                 f' Default: {DEFAULT_UPLOAD_URL}'
        )
        parser.add_argument(
            '--filepath',
            type=str,
            default=DEFAULT_STL_FILE,
            help=f'Stl file to use. Default: {DEFAULT_STL_FILE}'
        )

    def _validate_parameters(self, kwargs):
        if kwargs.get('no_input'):
            invalid_args = []
            for arg in MINIMUM_REQUIRED_ARGS:
                if kwargs.get(arg) is None:
                    invalid_args.append(arg)

            if len(invalid_args) > 0:
                print(f'Interactive mode was turned off but command line'
                      f' is missing the these args: {invalid_args}')
                exit(1)

    def handle(self, *args, **kwargs):
        self._validate_parameters(kwargs)
        self.options = self._update_kwargs(**kwargs)
        self.site_string = self.options.get('site', None)
        # Setting Environment var to stop bugs with get_current_org()
        os.environ['SITE_DOMAIN'] = self.site_string

        qty = int(self.options.get('qty'))
        self.site = Site.objects.get(domain=self.site_string)
        self.user = User.objects.get(id=self.options['owner_id'])
        self.partner = Partner.objects.get(id=self.options['partner_id'])
        self.file_path = self.options.get('filepath')
        self.upload_url = self.options.get('uploadurl')

        for iteration in range(qty):
            with transaction.atomic():
                order = self.create_order()
                if order:
                    print(f'Created Order #{order.id} Successfully')
                else:
                    print(f'Failed to create order.')

    def create_order(self):
        # --------
        # STL FILE
        # --------

        files = {'file': (self.file_path, open(self.file_path, 'rb'))}
        response = requests.post(self.upload_url, files=files)
        if response.status_code != 200:
            print('Could not upload an stl file, check 3d backend is running.')
            sys.exit(1)

        payload = response.json()
        if 'uuid' not in payload:
            if 'errormessage' in payload:
                print(payload['errormessage'])
            else:
                print('Unknown error uploading file')

            sys.exit(1)

        params = ParameterFactory()
        stl = StlFile.all_objects.get(
            uuid=payload['uuid']
        )
        stl.parameter = params
        stl.save()

        # ------------
        # BASKET/ORDER
        # ------------
        randomize = self.options.get('randomize')
        if 'stockrecords' in randomize:
            qs = StockRecord.objects.filter(partner=self.partner)
            stockrecord = qs[random.randint(0, len(qs) - 1)]  # nosec
        else:
            stockrecord = StockRecord.objects.filter(
                partner=self.partner
            ).first()

        date = timezone.now()
        if 'dates' in randomize:
            date = date - timedelta(days=random.randint(0, 30))  # nosec

        voucher = VoucherFactory(
            code=uuid4(),
            partner=self.partner
        )
        shipping_method = ShippingMethodFactory(
            partner=self.partner
        )

        basket = self._create_basket(stockrecord, stl)

        order = OrderFactory(
            datetime_placed=date,
            number=basket.quotation_number,
            site=self.site,
            partner=self.partner,
            project=basket,
            voucher=voucher,
            shipping_method=shipping_method,
            generate_payment=NO_PAYMENT
        )
        self._create_payment(order=order)
        return order

    def _create_payment(self, order):
        method = PartnerPaymentMethod.objects \
            .filter(partner=self.partner).first()
        payment = Payment.objects.create(
            user=self.user,
            order=order,
            payment_method=method,
            currency='EUR',
            amount=order.total_value
        )
        return payment

    def _create_basket(self, stockrecord, stl):
        basket = BasketFactory(
            owner=self.user,
            quotation_number=None,
            site_id=self.site.id
        )
        basket.quotation_number = OrderNumberGenerator().order_number(basket)
        basket.deleted_date = None
        basket.save()

        BasketLineFactory(
            stockrecord=stockrecord,
            stl_file=stl,
            basket=basket
        )
        BasketFactory.save_basket(basket)
        return basket

    def _update_kwargs(self, **kwargs):
        """
        Fills in the gaps left out from command line for easier selection
        without needing to know id's of partner, owner etc
        """
        answers = {}
        if not kwargs.get('qty'):
            qstn_qty = inquirer.Text(
                'qty', message='Quantity of desired orders?', default='1'
            )
            answers['qty'] = inquirer.prompt([qstn_qty])['qty']
            kwargs.update(answers)
        if not kwargs.get('site'):
            site_qs = Site.objects.all().values('name', 'id')
            sites = [site['name'] for site in site_qs]
            qstn_site = inquirer.List(
                'site',
                message='Which site to Place the orders at?',
                choices=sites
            )
            answer = inquirer.prompt([qstn_site])['site']
            answers['site'] = answer
            kwargs.update(answers)

        if not kwargs.get('partner_id'):
            partner_qs = Partner.objects.filter(
                site__name=kwargs['site']).values('name', 'id')
            partners = [{partner['name']: partner['id']}
                        for partner in partner_qs]
            qstn_partner = inquirer.List(
                'partner',
                message='Which partner to Place the orders with?',
                choices=partners
            )
            answer = inquirer.prompt([qstn_partner])['partner']
            answers['partner_id'] = next(iter(answer.values()))
            kwargs.update(answers)

        if not kwargs.get('owner_id'):
            owner_qs = User.objects.filter(
                userprofile__site__name=kwargs['site']).values('email', 'id')
            owners = [{owner['email']: owner['id']} for owner in owner_qs]
            qstn_owner = inquirer.List(
                'owner',
                message='Which User should have these orders assigned?',
                choices=owners
            )
            answer = inquirer.prompt([qstn_owner])['owner']
            answers['owner_id'] = next(iter(answer.values()))
            kwargs.update(answers)
        if not kwargs.get('randomize'):
            qstn_randomize = inquirer.Checkbox(
                'randomize',
                message='Values to Randomize',
                choices=RANDOMIZE_CHOICES
            )
            answer = inquirer.prompt([qstn_randomize])['randomize']
            answers['randomize'] = answer
            kwargs.update(answers)
        return kwargs
