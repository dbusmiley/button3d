from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import BasketLineFactory, Decimal, \
    PartnerExtraFeeFactory, PostProcessingFactory, ColorFactory, \
    BasketAttachmentFactory, VoucherFactory, Voucher
from apps.b3_tests.testcases.common_testcases import TestCase


class OrderFactoryTest(TestCase):
    def test_order_factory_with_project(self):
        line = BasketLineFactory()
        project = line.basket

        fee = PartnerExtraFeeFactory(partner=project.partner)
        voucher = VoucherFactory(
            action_type=Voucher.FIXED,
            partner=project.partner
        )

        color = ColorFactory()
        post_processing = PostProcessingFactory(
            stock_record=line.stockrecord,
            colors=[color]
        )
        line.post_processing_options.create(
            post_processing=post_processing,
            color=color
        )

        filename = 'test.file'
        content_basket_attachment = b'test1'
        content_basket_line_attachment = b'test1'
        BasketAttachmentFactory(
            basket=project,
            content=content_basket_attachment,
            filename=filename,
            uploader=project.owner
        )
        BasketAttachmentFactory(
            basket=project,
            basket_line=line,
            content=content_basket_line_attachment,
            filename=filename,
            uploader=project.owner
        )

        order = OrderFactory(project=project, voucher=voucher)

        self.assertEqual(order.total.excl_tax, Decimal('463.55'))
        self.assertEqual(order.applied_fees.first().price.excl_tax, fee.value)
        self.assertEqual(order.voucher_discount_value, voucher.action_value)
        self.assertEqual(
            order.attachments.first().file.read(),
            content_basket_attachment
        )
        self.assertEqual(
            order.lines.first().attachments.first().file.read(),
            content_basket_line_attachment
        )
        self.assertEqual(order.payment.payment_method.type_name, 'invoice')
        self.assertEqual(order.payment.amount, Decimal('463.55'))

    def test_order_factory_plain(self):
        order = OrderFactory()
        self.assertEqual(order.total.excl_tax, Decimal('215.16'))
