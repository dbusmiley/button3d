from button3d import type_declarations as td
from apps.b3_order.models.order_notes import InvoiceSequence
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import PartnerFactory


class InvoiceTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.site = get_current_site()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.save()

        self.other_partner = PartnerFactory(
            name='Otherwerk', logo='meltwerk-logo.png'
        )
        self.other_partner.site = self.site
        self.other_partner.save()

    def reset_sequence(self, partner: td.Partner):
        self.assertTrue(InvoiceSequence.objects.reset_sequence(partner))

    def test_sequence(self):
        self.reset_sequence(self.partner)
        first = InvoiceSequence.objects.get_next(partner=self.partner)
        second = InvoiceSequence.objects.get_next(partner=self.partner)
        self.assertTrue(second - first == 1)

    def test_sequence_different_partners(self):
        self.reset_sequence(self.partner)
        self.reset_sequence(self.other_partner)
        first = InvoiceSequence.objects.get_next(partner=self.partner)
        first_other = InvoiceSequence.objects.get_next(
            partner=self.other_partner
        )
        self.assertEqual(first, 1)
        self.assertEqual(first_other, 1)
        second = InvoiceSequence.objects.get_next(partner=self.partner)
        second_other = InvoiceSequence.objects.get(partner=self.other_partner)
        self.assertTrue(second - second_other.sequence == 1)
