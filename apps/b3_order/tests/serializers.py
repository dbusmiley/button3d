from datetime import date

from django.db.models import Q

from apps.b3_order.serializers.order_csv_export_filter_serializer import \
    OrderCSVExportFilterSerializer
from apps.b3_tests.testcases.common_testcases import TestCase


class OrderCSVExportFilterSerializerTest(TestCase):
    def test_serializer_with_from_date(self):
        data = {'date_from': '2016-06-06'}

        serializer = OrderCSVExportFilterSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        expected_query = Q(datetime_placed__date__gte=date(2016, 6, 6))
        self.assertEqual(str(expected_query), str(serializer.save()))

    def test_serializer_with_to_date(self):
        data = {'date_to': '2016-06-06'}

        serializer = OrderCSVExportFilterSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        expected_query = Q(datetime_placed__date__lt=date(2016, 6, 6))
        self.assertEqual(str(expected_query), str(serializer.save()))

    def test_serializer_with_one_day(self):
        data = {'date_from': '2016-06-06', 'date_to': '2016-06-06'}

        serializer = OrderCSVExportFilterSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        expected_query = Q(datetime_placed__date=date(2016, 6, 6))
        self.assertEqual(str(expected_query), str(serializer.save()))

    def test_serliazer_with_two_dates(self):
        data = {'date_from': '2016-06-06', 'date_to': '2016-06-08'}

        serializer = OrderCSVExportFilterSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        expected_query = Q(datetime_placed__date__gte=date(2016, 6, 6)) & \
            Q(datetime_placed__date__lt=date(2016, 6, 8))
        self.assertEqual(str(expected_query), str(serializer.save()))

    def test_serializer_with_statuses(self):
        data = {'shipped': 1, 'printing': 'yes'}

        serializer = OrderCSVExportFilterSerializer(data=data)
        self.assertTrue(serializer.is_valid())

        expected_query = Q(current_status__type__in=['Shipped', 'Printing'])
        self.assertEqual(str(expected_query), str(serializer.save()))
