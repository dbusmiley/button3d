import json
import unittest
from datetime import datetime
from unittest import mock

from django.utils.timezone import make_aware

import button3d.type_declarations as td
from apps.b3_address.factories import AddressFactory
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_order.factories import OrderFactory
from apps.b3_order.models import OrderLine, \
    OrderLinePostProcessingOption
from apps.b3_order.webhook_serializers import OrderWebhookSerializer
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import BasketFactory, PartnerFactory, \
    BasketLineFactory, StockRecordFactory, \
    PostProcessingFactory, ColorFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Comment


# Currently fails due to autogeneration of order line on the order. Jakob /
# Serheii will fix OrderFactory in another ticket at which point this test
# can be re-enabled.
@unittest.expectedFailure
class TestOrderWebhookSerializer(AuthenticatedTestCase):
    def test_serializer(self):
        self.user.first_name = 'Mickey'
        self.user.last_name = 'Mouse'
        self.user.email = 'mickey_mouse@disney.com'
        self.user.save()

        datetime_created = make_aware(datetime(2018, 5, 1, 10, 0))

        partner = PartnerFactory(id=9)
        shipping_method = ShippingMethodFactory(
            name='TestMethod',
            partner=partner,
        )
        PartnerPaymentMethodFactory(partner=partner)

        project = BasketFactory(
            id=50,
            title='test_project',
            owner=self.user,
            date_created=datetime_created,
        )

        stock_record1: td.StockRecord = StockRecordFactory(
            partner=partner,
            id=78,
        )
        project_line1: td.BasketLine = BasketLineFactory(
            basket=project,
            stockrecord=stock_record1,
        )

        stock_record2: td.StockRecord = StockRecordFactory(partner=partner)
        project_line2: td.BasketLine = BasketLineFactory(
            basket=project,
            stockrecord=stock_record2,
        )

        billing_address = AddressFactory(user=None, partner=None, title='Mr.')
        shipping_address = AddressFactory(user=None, partner=None, title='Mr.')

        order = OrderFactory(
            id=5,
            datetime_placed=datetime_created,
            project=project,
            purchased_by=self.user,
            billing_address=billing_address,
            shipping_address=shipping_address,
            shipping_method=shipping_method,
            site=self.site,
        )

        # order lines
        order_line1 = OrderLine.objects.create(
            order=order,
            stl_file=project_line1.stl_file,
            stock_record=stock_record1,
            configuration=project_line1.configuration,
            unit_price_value=project_line1.unit_price_excl_tax,
            unit_price_tax=project_line1.unit_price_incl_tax
            - project_line1.unit_price_excl_tax,
            quantity=project_line1.quantity,
            item_discount_value=project_line1.discount_excl_tax,
            item_discount_tax=project_line1.discount_incl_tax
            - project_line1.discount_excl_tax,
            item_total_value=project_line1.line_price_excl_tax,
            item_total_tax=project_line1.line_price_incl_tax
            - project_line1.line_price_excl_tax,
        )

        post_processing = PostProcessingFactory(
            stock_record=stock_record1,
            id=99,
        )
        color = ColorFactory(id=3)

        OrderLinePostProcessingOption.objects.create(
            line=order_line1,
            post_processing=post_processing,
            color=color
        )

        order_line2 = OrderLine.objects.create(
            order=order,
            stl_file=project_line2.stl_file,
            stock_record=stock_record2,
            configuration=project_line2.configuration,
            unit_price_value=project_line2.unit_price_excl_tax,
            unit_price_tax=project_line2.unit_price_incl_tax
            - project_line2.unit_price_excl_tax,
            quantity=project_line2.quantity,
            item_discount_value=project_line2.discount_excl_tax,
            item_discount_tax=project_line2.discount_incl_tax
            - project_line2.discount_excl_tax,
            item_total_value=project_line2.line_price_excl_tax,
            item_total_tax=project_line2.line_price_incl_tax
            - project_line2.line_price_excl_tax,
        )

        # comments
        with mock.patch(
                'apps.basket.models.comment.now',
                new=lambda: datetime_created,
        ):
            comment1 = Comment.objects.create(
                uuid='165a0d48-e970-4893-8da7-ffc21eb224e9',
                text='I want it painted',
                user=self.user,
                basket=project,
                created=datetime_created,
                edited=datetime_created,
            )
            Comment.objects.create(
                uuid='632bb0b2-c87a-47cc-8aae-215ddda2d413',
                parent=comment1,
                text='And polished',
                user=self.user,
                basket=project,
                created=datetime_created,
                edited=datetime_created,
            )

        data = OrderWebhookSerializer(instance=order).data
        data = json.loads(json.dumps(data))
        expected = {
            'api_version': '2018-09-26',
            'created': '2018-05-01T10:00:00Z',
            'data': {
                'object': {
                    'billing_address': {'city': 'Berlin',
                                        'country': 'GB',
                                        'first_name':
                                            billing_address.first_name,
                                        'last_name':
                                            billing_address.last_name,
                                        'line1': 'Bismerkstr. 12-14',
                                        'line2': '2 G.',
                                        'line3': '3YOURMIND',
                                        'postcode': '10625',
                                        'state': 'Berlin',
                                        'title': 'Mr.'},
                    'comments': [{
                        'created_pretty_date': '2018-05-01T10:00:00Z',
                        'edited_pretty_date': '2018-05-01T10:00:00Z',
                        'id': '165a0d48-e970-4893-8da7-ffc21eb224e9',
                        'parent': None,
                        'text': 'I want it painted',
                        'user': {'id': self.user.id,
                                 'name': 'Mickey Mouse',
                                 'service': 'Test Partner'}},
                                 {
                        'created_pretty_date': '2018-05-01T10:00:00Z',
                        'edited_pretty_date': '2018-05-01T10:00:00Z',
                        'id': '632bb0b2-c87a-47cc-8aae-215ddda2d413',
                        'parent': '165a0d48-e970-4893-8da7-ffc21eb224e9',
                        'text': 'And polished',
                        'user': {'id': self.user.id,
                                 'name': 'Mickey Mouse',
                                 'service': 'Test Partner'}}],
                    'currency': 'EUR',
                    'domain': 'domain.com',
                    'instructions': '',
                    'items': [{'line_discount': {
                        'currency': 'EUR',
                        'excl_tax': '0.00',
                        'incl_tax': '0.00',
                        'is_tax_known': True,
                        'tax': '0.00'},
                        'line_price': {
                            'currency': 'EUR',
                            'excl_tax': '239.07',
                            'incl_tax': '284.49',
                            'is_tax_known': True,
                            'tax': '45.42'},
                        'partner_name': 'Test Partner',
                        'partner_sku': stock_record1.partner_sku,
                        'post_processings': [
                            {'color': str(color),
                             'name': post_processing.title}],
                        'product': 'Test product',
                        'quantity': 1,
                        'scale': 1.0,
                        'service_id': 9,
                        'stock_record': stock_record1.id,
                        'support_volume': None,
                        'unit_price': {
                            'currency': 'EUR',
                            'excl_tax': '239.07',
                            'incl_tax': '284.49',
                            'is_tax_known': True,
                            'tax': '45.42'},
                        'uuid': order_line1.stl_file.uuid,
                        'volume': 23286.07},
                        {'line_discount': {
                            'currency': 'EUR',
                            'excl_tax': '0.00',
                            'incl_tax': '0.00',
                            'is_tax_known': True,
                            'tax': '0.00'},
                            'line_price': {
                                'currency': 'EUR',
                                'excl_tax': '239.07',
                                'incl_tax': '284.49',
                                'is_tax_known': True,
                                'tax': '45.42'},
                            'partner_name': 'Test Partner',
                            'partner_sku': stock_record2.partner_sku,
                            'post_processings': [],
                            'product': 'Test product',
                            'quantity': 1,
                            'scale': 1.0,
                            'service_id': 9,
                            'stock_record': stock_record2.id,
                            'support_volume': None,
                            'unit_price': {
                                'currency': 'EUR',
                                'excl_tax': '239.07',
                                'incl_tax': '284.49',
                                'is_tax_known': True,
                                'tax': '45.42'},
                            'uuid': order_line2.stl_file.uuid,
                            'volume': 23286.07}],
                    'order_date': '2018-05-01T10:00:00Z',
                    'order_id': 5,
                    'order_number': order.number,
                    'pickup_location': None,
                    'project_id': 50,
                    'project_name': 'test_project',
                    'shipping_address': {'city': 'Berlin',
                                         'country': 'GB',
                                         'first_name':
                                             shipping_address.first_name,
                                         'last_name':
                                             shipping_address.last_name,
                                         'line1': 'Bismerkstr. 12-14',
                                         'line2': '2 G.',
                                         'line3': '3YOURMIND',
                                         'postcode': '10625',
                                         'state': 'Berlin',
                                         'title': 'Mr.'},
                    'shipping_method': 'TestMethod',
                    'shipping_price': {'currency': 'EUR',
                                       'excl_tax': '10.00',
                                       'incl_tax': '10.00',
                                       'is_tax_known': True,
                                       'tax': '0.00'},
                    'total_price': {'currency': 'EUR',
                                    'excl_tax': '488.14',
                                    'incl_tax': '488.14',
                                    'is_tax_known': True,
                                    'tax': '0.00'},
                    'user': {
                        'email': 'mickey_mouse@disney.com',
                        'first_name': 'Mickey',
                        'last_name': 'Mouse'},
                    'vat_number': 'AS-123-R'}},
            'type': 'order.created'}

        self.assertEqual(data, expected)
