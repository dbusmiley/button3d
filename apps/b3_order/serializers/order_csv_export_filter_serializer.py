from rest_framework import serializers
from django.db.models import Q
import typing as t


class OrderCSVExportFilterSerializer(serializers.Serializer):
    date_from = serializers.DateField(required=False, format='%Y-%m-%d')
    date_to = serializers.DateField(required=False, format='%Y-%m-%d')
    pending = serializers.CharField(required=False)
    shipped = serializers.CharField(required=False)
    printing = serializers.CharField(required=False)
    cancelled = serializers.CharField(required=False)

    statuses = ('pending', 'shipped', 'printing', 'cancelled')

    def save(self) -> Q:
        query = Q()
        for date_filter in self.get_date_filters():
            query &= date_filter

        statuses = self.get_applied_statuses()

        if statuses:
            query &= Q(current_status__type__in=statuses)
        return query

    def get_date_filters(self) -> t.List[Q]:
        queries = []
        date_from = self.validated_data.get('date_from')
        date_to = self.validated_data.get('date_to')
        if date_from and date_to and date_from == date_to:
            return [Q(datetime_placed__date=date_from)]

        if date_from:
            queries.append(Q(datetime_placed__date__gte=date_from))

        if date_to:
            queries.append(Q(datetime_placed__date__lt=date_to))

        return queries

    def get_applied_statuses(self) -> t.List[str]:
        return [
            status.title()
            for status in self.statuses
            if status in self.validated_data
        ]

    def filter_query_pretty_printed(self) -> str:
        data = self.validated_data
        date_from = data.get('date_from')
        date_to = data.get('date_to')

        default_no_date = ''
        date_formatting = '%Y.%m.%d'

        if any((date_from, date_to)):
            if date_from:
                date_from_string = \
                    f'from {date_from.strftime(date_formatting)}'
            else:
                date_from_string = default_no_date

            if date_to:
                date_to_string = f'to {date_to.strftime(date_formatting)}'
            else:
                date_to_string = default_no_date

            date_string = ' '.join((date_from_string, date_to_string))
        else:
            date_string = ''

        statuses: str = ', '.join(self.get_applied_statuses())
        pretty_printed = f' '.join((date_string, statuses))

        return pretty_printed.strip()
