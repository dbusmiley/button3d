from rest_framework import serializers

from apps.b3_order.models import OrderStatus


class OrderStatusSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('type', 'created')
        model = OrderStatus
