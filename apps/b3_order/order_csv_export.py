import logging
import io
import typing as t
from collections import defaultdict, namedtuple
from json import dumps

from django.conf import settings

import button3d.type_declarations as td
from apps.b3_core.utils import CSVUnicodeWriter

logger = logging.getLogger(__name__)


class OrderCSVExport:
    CSVOrderColumns = namedtuple('CSVOrderColumns', [
        'number',
        'date',
        'status',
        'vat_rate',
        'min_price',
        'min_price_tax',
        'fees',
        'fees_tax',
        'subtotal',
        'subtotal_tax',
        'shipping_price',
        'shipping_tax',
        'total_price',
        'total_tax',
        'voucher_discount_value',
        'voucher_discount_tax',
        'payment_method',

        'buyer',
        'customer_number',
        'customer_email',
        'customer_message',
        'company_information',

        'billing_addess_title',
        'billing_addess_first_name',
        'billing_addess_last_name',
        'billing_addess_street',
        'billing_addess_line2',
        'billing_addess_company',
        'billing_addess_department',
        'billing_addess_city',
        'billing_addess_state',
        'billing_addess_post_code',
        'billing_addess_country',

        'shipping_addess_title',
        'shipping_addess_first_name',
        'shipping_addess_last_name',
        'shipping_addess_street',
        'shipping_addess_line2',
        'shipping_addess_company',
        'shipping_addess_department',
        'shipping_addess_city',
        'shipping_addess_state',
        'shipping_addess_post_code',
        'shipping_addess_country',

        'pickup_location',
        'pickup_location_instructions',

        'shipping_method',
        'delivery_time',
        'line_count',
        'attachments',
        'comment',
    ])

    CSVOrderLineColumns = namedtuple('CSVOrderLineColumns', [
        'name',
        'material',
        'post_processing_and_color',
        'dimensions',
        'scale',
        'quantity',
        'unit_price',
        'line_price_tax_excl',
        'line_price_tax',
        'download_link_original',
        'download_link_repaired',
        'attachments',
    ])

    CSV_ORDER_HEADERS = CSVOrderColumns(
        'Order Number',
        'Order Date',
        'Status',
        'VAT rate %',
        'Minimal price, {}',
        'Minimal price tax, {}',
        'Fees, {}',
        'Fees tax, {}',
        'Subtotal, {}',
        'Subtotal tax, {}',
        'Shipping price, {}',
        'Shipping price tax, {}',
        'Total price, {}',
        'Total price tax, {}',
        'Voucher discount amount, {}',
        'Voucher discount tax, {}',
        'Payment Method',

        'Buyer',
        'Customer Number',
        'Email',
        'Message from Buyer',
        'Company Information',

        'Billing Address - Title',
        'Billing Address - First Name',
        'Billing Address - Last Name',
        'Billing Address - Street Name',
        'Billing Address - Line 2',
        'Billing Address - Company',
        'Billing Address - Department',
        'Billing Address - City',
        'Billing Address - State',
        'Billing Address - Postcode',
        'Billing Address - Country',

        'Shipping Address - Title',
        'Shipping Address - First Name',
        'Shipping Address - Last Name',
        'Shipping Address - Street Name',
        'Shipping Address - Line 2',
        'Shipping Address - Company',
        'Shipping Address - Department',
        'Shipping Address - City',
        'Shipping Address - State',
        'Shipping Address - Postcode',
        'Shipping Address - Country',

        'Pickup Location',
        'Pickup Location Instructions',

        'Shipping Method',
        'Delivery Time',
        'Print Items',
        'Attachments',
        'Comment',
    )

    CSV_ORDER_LINE_HEADERS = CSVOrderLineColumns(
        'Name',
        'Material',
        'Post-Processings, Color',
        'Dimensions (mm)',
        'Scale (%)',
        'Quantity',
        'Unit Price (%s)',
        'Line Price (%s)',
        'Line Tax (%s)',
        'Download Original File',
        'Download Repaired File',
        'Attachments',
    )

    CSV_LINE_HEADERS = CSV_ORDER_HEADERS + CSV_ORDER_LINE_HEADERS

    def __init__(
            self,
            orders_queryset: t.Sequence[td.Order],
            request: td.HttpRequest,
    ):
        self.orders_queryset = orders_queryset
        self.request = request
        self.include_attachments: bool = \
            settings.PRIVATE_FILE_STORAGE == settings.DEFAULT_FILE_STORAGE
        logger.info(
            f'Order and line attachments CSV export '
            f'enabled?={self.include_attachments}.'
            f' DEFAULT_FILE_STORAGE={settings.DEFAULT_FILE_STORAGE}, '
            f'PRIVATE_FILE_STORAGE={settings.PRIVATE_FILE_STORAGE}'
        )

    def as_raw_csv(self):
        response_payload = io.StringIO()
        writer = CSVUnicodeWriter(response_payload)
        for row in self._csv_lines_generator():
            writer.writerow(row)

        return response_payload.getvalue()

    def as_values_list(self):
        return list(self._csv_lines_generator())

    def _csv_lines_generator(
            self,
    ) -> t.Generator:
        partitioned_orders = self._partition_orders_by_currency()

        for currency, orders in partitioned_orders.items():
            title_row = tuple(
                column_title.format(currency)
                for column_title
                in self.CSV_LINE_HEADERS
            )
            yield title_row

            for order in orders:
                order_values = self._get_order_csv_row_values_list(order)
                for line in order.lines.all():
                    line_values = self._get_order_line_csv_row_values_list(
                        line,
                    )
                    yield order_values + line_values

    def _partition_orders_by_currency(self) \
            -> t.DefaultDict[str, t.Sequence[td.Order]]:
        partitioned = defaultdict(list)

        for order in self.orders_queryset:
            currency: str = order.currency
            partitioned[currency].append(order)
        return partitioned

    def _get_order_csv_row_values_list(self, order: td.OrderNew) \
            -> CSVOrderColumns:
        user: td.User = order.purchased_by
        billing_address: td.Address = order.billing_address
        shipping_address: t.Optional[td.Address] = order.shipping_address
        pickup_location: t.Optional[td.PickupLocation] = order.pickup_location
        payment = order.payment

        if order.shipping_method:
            payment_description = order.shipping_method.name
        else:
            payment_description = f'Pickup: {order.pickup_location.location}'

        if self.include_attachments:
            attachments = order.basket.attachments.\
                filter(basket_line=None).only('file')

            attachment_column_value: str = \
                self._get_attachment_list_string(attachments)
        else:
            attachment_column_value: str = ''

        first_comment = order.get_first_comment()
        if first_comment is not None:
            comment_text = first_comment.text
        else:
            comment_text = ''

        return self.CSVOrderColumns(
            order.number,
            order.datetime_placed.strftime('%Y-%m-%d %H:%M:%S'),
            order.status.type,
            order.tax_rate,

            order.min_price_value,
            order.min_price_tax,
            order.fees_value,
            order.fees_tax,
            order.subtotal_value,
            order.subtotal_tax,
            order.shipping_value,
            order.shipping_tax,
            order.total_value,
            order.total_tax,
            order.voucher_discount_value,
            order.voucher_discount_tax,
            payment.payment_method.name,
            user.userprofile.full_name,
            user.userprofile.customer_number,
            user.email,
            order.delivery_instructions,
            order.partner.name,

            billing_address.title if billing_address else '',
            billing_address.first_name if billing_address else '',
            billing_address.last_name if billing_address else '',
            billing_address.line1 if billing_address else '',
            billing_address.line2 if billing_address else '',
            billing_address.company_name if billing_address else '',
            billing_address.department if billing_address else '',
            billing_address.city if billing_address else '',
            billing_address.state if billing_address else '',
            billing_address.zip_code if billing_address else '',
            billing_address.country.name if billing_address else '',

            shipping_address.title if shipping_address else '',
            shipping_address.first_name if shipping_address else '',
            shipping_address.last_name if shipping_address else '',
            shipping_address.line1 if shipping_address else '',
            shipping_address.line2 if shipping_address else '',
            shipping_address.company_name if shipping_address else '',
            shipping_address.department if shipping_address else '',
            shipping_address.city if shipping_address else '',
            shipping_address.state if shipping_address else '',
            shipping_address.zip_code if shipping_address else '',
            shipping_address.country.name if shipping_address else '',

            pickup_location.location if pickup_location else '',
            pickup_location.instructions if pickup_location else '',

            payment_description,
            order.delivery_days_range_pretty_printed,
            order.lines.count(),
            attachment_column_value,
            comment_text,
        )

    def _get_order_line_csv_row_values_list(
            self,
            order_line: td.OrderLine,
    ) -> CSVOrderLineColumns:
        product = order_line.stock_record.product
        stl_file = order_line.stl_file

        post_processing_options = order_line.post_processing_options.all()
        post_processings = []
        for post_processing_option in post_processing_options:
            if post_processing_option.color:
                post_processing_formatted = \
                    f'{post_processing_option.post_processing.title} ' \
                    f'({post_processing_option.color.title})'
            else:
                post_processing_formatted = \
                    post_processing_option.post_processing.title
            post_processings.append(post_processing_formatted)

        post_processings_str = ', '.join(post_processings)

        if self.include_attachments:
            attachments = order_line.attachments.only('file')
            attachment_column_value: str = \
                self._get_attachment_list_string(attachments)
        else:
            attachment_column_value: str = ''

        return self.CSVOrderLineColumns(
            order_line.name,
            product.title,
            post_processings_str,
            order_line.dimensions_formatted,
            order_line.scale,
            order_line.quantity,
            order_line.unit_price_value,
            order_line.item_total_value,
            order_line.item_total_tax,
            self.request.build_absolute_uri(stl_file.download_optimized_url),
            self.request.build_absolute_uri(stl_file.download_original_url),
            attachment_column_value,
        )

    def _get_attachment_list_string(
            self,
            attachments: t.Sequence[td.OrderAttachment]
    ) -> str:
        attachment_list = [
            self.request.build_absolute_uri(attachment.file.url)
            for attachment
            in attachments
        ]
        if attachment_list:
            return dumps(attachment_list)
        else:
            return ''
