from actstream.models import Action
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from apps.b3_order.models import Order,  OrderStatus, OrderAttachment,  \
    OrderLine, OrderFee, OrderLinePostProcessingOption


@admin.register(OrderLine)
class OrderLineAdmin(admin.ModelAdmin):
    class OrderLinePostProcessingOptionInline(admin.TabularInline):
        model = OrderLinePostProcessingOption
        raw_id_fields = ('color', 'post_processing')

    inlines = (OrderLinePostProcessingOptionInline,)
    raw_id_fields = ('order', 'stl_file', 'stock_record', 'configuration')


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):

    class OrderStatusInline(admin.TabularInline):
        model = OrderStatus
        extra = 0

    class OrderLineInline(admin.TabularInline):
        model = OrderLine
        raw_id_fields = ('stl_file', 'stock_record', 'configuration')
        extra = 0

    class OrderFeeInline(admin.TabularInline):
        model = OrderFee
        extra = 0

    class OrderAttachmentInline(admin.TabularInline):
        model = OrderAttachment
        raw_id_fields = ('uploader', 'order_line')
        extra = 0

    class StateChangesInline(GenericTabularInline):
        ct_field = "actor_content_type"
        ct_fk_field = "actor_object_id"
        model = Action
        extra = 0

    def attachment_count(self, order):
        return order.attachments.count()

    def get_queryset(self, request):
        return Order.all_objects.all()

    raw_id_fields = (
        'site', 'project', 'partner',
        'voucher', 'shipping_method', 'pickup_location',
        'billing_address', 'shipping_address', 'purchased_by',
    )
    list_display = (
        'title',
        'number',
        'site',
        'partner',
        'attachment_count',
    )
    inlines = (
        OrderLineInline,
        OrderStatusInline,
        OrderAttachmentInline,
        OrderFeeInline,
        StateChangesInline,
    )
    search_fields = [
        'number',
        'site__name',
        'purchased_by__first_name',
        'purchased_by__last_name',
        'purchased_by__username'
    ]
