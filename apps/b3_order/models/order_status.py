from django.db import models
from django.utils import timezone

from apps.b3_core.models import AbstractDeletedDateModel, DeletedDateManager


class OrderStatusManager(DeletedDateManager):
    def current(self):
        return self.get_queryset().order_by('-created').first()


class OrderStatus(AbstractDeletedDateModel):
    PENDING, PRINTING, SHIPPED, CANCELLED = \
        'Pending', 'Printing', 'Shipped', 'Cancelled'
    STATUSES = (
        (PENDING, 'pending'),
        (PRINTING, 'printing'),
        (SHIPPED, 'shipped'),
        (CANCELLED, 'cancelled')
    )

    order = models.ForeignKey(
        'b3_order.Order',
        related_name='statuses',
        on_delete=models.CASCADE
    )

    type = models.CharField(
        max_length=10,
        choices=STATUSES,
    )

    created = models.DateTimeField(default=timezone.now)

    objects = OrderStatusManager()
    all_objects = models.Manager()

    def __str__(self) -> str:
        return self.type

    class Meta:
        ordering = ('created', )
