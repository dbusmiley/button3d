import datetime
import logging
import typing as t
from decimal import Decimal

from actstream.models import Action
from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.utils import timezone
from guardian.shortcuts import get_users_with_perms

import button3d.type_declarations as td
from apps.b3_address.models import Address
from apps.b3_core.fields import CurrencyField
from apps.b3_order.exceptions import InvalidOrderStatus
from apps.b3_order.models.order_status import OrderStatus
from apps.b3_order.validators \
    import validate_address_does_not_belong_to_user_or_partner
from apps.b3_organization.tracking.trackers import OrderStateTracker
from apps.b3_organization.utils import get_current_site, get_current_org, \
    get_current_request
from apps.b3_shipping.models import ShippingMethod, PickupLocation
from apps.b3_shipping.utils.get_delivery_range import \
    get_delivery_range_pretty_printed, get_delivery_range
from apps.b3_voucher.models import Voucher
from apps.basket.models import Basket
from apps.partner.models import Partner, AbstractDeletedDateModel
from apps.partner.pricing.price import Price

logger = logging.getLogger(__name__)


class OrderManager(models.Manager):
    def get_queryset(self):
        qs = super().get_queryset()
        site = get_current_site()
        return qs.filter(
            deleted_date=None,
            site=site
        )

    def with_partner(self, partner_id):
        if not partner_id:
            return Order.objects.all()

        # This would allow order_by() to be operated on the queryset later, see
        # https://docs.djangoproject.com/en/1.9/ref/models/querysets/#distinct
        return Order.all_objects.filter(
            id__in=Order.all_objects.filter(
                partner_id=int(partner_id)
            ).distinct()
        )


class Order(AbstractDeletedDateModel):

    # ORDER INFORMATION
    # -----------------

    title = models.CharField(
        max_length=254, help_text='Order Title'
    )
    number = models.CharField(max_length=128, db_index=True, unique=True)
    site = models.ForeignKey(Site, related_name='orders')
    datetime_placed = models.DateTimeField(
        db_index=True, default=timezone.now
    )

    project = models.OneToOneField(
        Basket,
        on_delete=models.PROTECT,
        related_name='order',
    )

    # CUSTOMER INFORMATION
    # ----------------

    purchased_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        related_name='orders'
    )

    customer_language = models.CharField(max_length=5, blank=True)
    customer_ip = models.CharField(max_length=45, blank=True)
    customer_reference = models.CharField(max_length=254)

    billing_address = models.ForeignKey(
        Address,
        null=True,
        blank=True,
        related_name='as_billing_address_on_orders',
        validators=[validate_address_does_not_belong_to_user_or_partner]
    )

    shipping_address = models.ForeignKey(
        Address,
        related_name='as_shipping_address_on_orders',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        validators=[validate_address_does_not_belong_to_user_or_partner]
    )

    # PARTNER INFORMATION
    # -------------------

    partner = models.ForeignKey(
        Partner,
        on_delete=models.PROTECT,
        related_name='orders'
    )

    # PRICING INFORMATION (EXCL TAX)
    # ------------------------------
    currency = models.CharField(max_length=12)
    tax_rate = models.DecimalField(decimal_places=5, max_digits=7)

    min_price_diff_value = CurrencyField()
    min_price_diff_tax = CurrencyField()

    min_price_value = CurrencyField()
    min_price_tax = CurrencyField()

    fees_value = CurrencyField()
    fees_tax = CurrencyField()

    subtotal_value = CurrencyField()
    subtotal_tax = CurrencyField()

    shipping_value = CurrencyField()
    shipping_tax = CurrencyField()

    total_value = CurrencyField()
    total_tax = CurrencyField()

    voucher = models.ForeignKey(
        Voucher,
        related_name='orders',
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )
    voucher_discount_value = CurrencyField(default=0)
    voucher_discount_tax = CurrencyField(default=0)

    # SHIPPING INFORMATION
    # --------------------

    shipping_method = models.ForeignKey(
        ShippingMethod,
        related_name='orders',
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    pickup_location = models.ForeignKey(
        PickupLocation,
        related_name='orders',
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    tracking_information = models.CharField(
        max_length=128,
        blank=True,
        default='',
    )

    delivery_instructions = models.TextField(blank=True, default='')

    # ADDITIONAL INFORMATION
    # ----------------------

    state_changes = GenericRelation(
        Action,
        object_id_field='actor_object_id',
        content_type_field='actor_content_type',
        related_query_name='order'
    )

    current_status = models.ForeignKey(
        'b3_order.OrderStatus',
        on_delete=models.SET_NULL,
        related_name='current_order',
        null=True,
        blank=True
    )

    objects = OrderManager()
    all_objects = models.Manager()

    class Meta:
        ordering = ('pk',)

    def __str__(self) -> str:
        return f'Order #{self.id}'

    def save(self, *args, **kwargs):
        self.clean()

        super().save(*args, **kwargs)

        if not self.statuses.exists():
            self.set_status(OrderStatus.PENDING)

    def delete(self, **kwargs) -> None:
        for state_change in self.state_changes.all():
            state_change.delete()
        super().delete()
        self.project.delete_submitted()

    @property
    def status(self) -> t.Optional[td.OrderStatus]:
        if not self.current_status and self.statuses.exists():
            self.current_status = self.statuses.current()
            self.save()
        return self.current_status

    def set_status(self, new_status: str) -> OrderStatus:
        status_translations = {
            'Pending': OrderStatus.PENDING,
            'Printing': OrderStatus.PRINTING,
            'Cancelled': OrderStatus.CANCELLED,
            'Shipped': OrderStatus.SHIPPED,
        }
        old_status = None
        if self.status is not None:
            old_status = self.status.type

        OrderStateTracker.register_state_change(
            self,
            old_state=status_translations.get(old_status),
            new_state=status_translations[new_status]
        )
        status = self.statuses.create(type=new_status)
        self.current_status = status
        self.save()
        return status

    def set_status_with_email_message(
            self,
            new_status: str,
            custom_message: str,
    ) -> None:
        """
        Overriding method to send message after change status by partner/admin

        Set a new status for this New Order Model.

        If the requested status is not valid, then ``InvalidOrderStatus`` is
        raised.
        """

        old_status: str = self.status.type
        if new_status == old_status:
            return

        allowed_statuses: t.List[str] = \
            [status for status, _ in OrderStatus.STATUSES]

        if new_status not in allowed_statuses:
            raise InvalidOrderStatus(
                f"'{new_status}' is not a valid status for order {self.number}"
                f" (current status: '{self.status.type}')")
        self.set_status(new_status)

        self.send_status_changed_email(new_status, custom_message)

    def update_tracking_information(self, value: str) -> None:
        self.tracking_information = value
        self.save()

    @property
    def delivery_days_range_pretty_printed(self) -> str:
        return get_delivery_range_pretty_printed(
            self.lines.all(), self.shipping_method
        )

    @property
    def delivery_days(self) -> t.Optional[t.Tuple[int, int]]:
        delivery_days = get_delivery_range(
            self.lines.all(), self.shipping_method
        )
        return delivery_days if None not in delivery_days else None

    def _price_object_from(self, price: Decimal, tax: Decimal) -> Price:
        return Price(
            excl_tax=price,
            currency=self.currency,
            tax=tax
        )

    @property
    def subtotal(self) -> Price:
        return self._price_object_from(self.subtotal_value, self.subtotal_tax)

    @property
    def total(self) -> Price:
        return self._price_object_from(self.total_value, self.total_tax)

    @property
    def shipping_price(self) -> Price:
        return self._price_object_from(self.shipping_value, self.shipping_tax)

    @property
    def total_without_shipping(self) -> Price:
        return self.total - self.shipping_price

    @property
    def minimum_price_fee(self) -> Price:
        return self._price_object_from(
            self.min_price_diff_value, self.min_price_diff_tax
        )

    @property
    def payment_method_name(self) -> str:
        return self.payment.payment_method.name

    @property
    def fees_total(self) -> Price:
        return self._price_object_from(
            self.fees_value, self.fees_tax
        )

    @property
    def voucher_discount(self) -> Price:
        return self._price_object_from(
            self.voucher_discount_value, self.voucher_discount_tax
        )

    @property
    def basket(self) -> td.Project:
        """
        DEPRECATED: use .project instead.
        This property is only here for backwards compatibility.
        """
        return self.project

    @property
    def order_is_confirmed(self) -> bool:
        try:
            return self.order_confirmation_notes.exists()
        except ObjectDoesNotExist:
            return False

    @property
    def has_deliveries(self) -> bool:
        try:
            return self.delivery_notes.exists()
        except ObjectDoesNotExist:
            return False

    @property
    def confirmed_delivery_date(self) -> t.Union[datetime.date, None]:
        if not self.order_is_confirmed:
            return None

        return self.order_confirmation_notes.latest('created_at').shipping_date

    @property
    def can_create_invoice(self) -> bool:
        if not self.order_is_confirmed:
            return False

        for delivery_note in self.delivery_notes.all():
            if delivery_note.total_delivered != 0:
                return True

        return False

    @property
    def latest_invoice(self) -> td.InvoiceNote:
        return self.invoice_notes.filter(
            is_cancelled=False
        ).latest('created_at')

    def cancelled_invoices(self, sort_order: str = '-created_at') \
            -> td.DjangoQuerySet:
        return self.invoice_notes.filter(
            is_cancelled=True
        ).order_by(sort_order)

    @property
    def partner_name(self) -> str:
        return self.partner.name

    def get_email_context(
            self,
            extra_context: t.Optional[td.StringKeyDict] = None,
    ) -> td.StringKeyDict:
        context = (extra_context or {}).copy()
        context.update({
            'request': get_current_request(),
            'order': self,
            'partner': self.partner,
            'lines': self.lines.all(),
            'delivery_time_string': self.delivery_days_range_pretty_printed,
        })
        return context

    def send_status_changed_email(
            self,
            new_status: str,
            custom_message: t.Optional[str],
    ) -> None:
        from apps.b3_core.mail import EmailSender
        email_sender = EmailSender()
        recipient_list = [self.purchased_by] + \
            self.get_list_of_users_with_share_permission()
        new_status = new_status.lower()

        for recipient in recipient_list:
            context = self.get_email_context({
                'custom_message': custom_message,
                'name': recipient.userprofile.full_name,
            })
            email_sender.send_email(
                to=recipient.email,
                subject_template_path=f'b3_core/emails/order_status/'
                f'order_status_{new_status}_subject.txt',
                body_txt_template_path=f'b3_core/emails/order_status/'
                                       f'order_status_{new_status}_body.txt',
                body_html_template_path=f'b3_core/emails/order_status/'
                                        f'order_status_{new_status}_body.html',
                extra_context=context,
            )

    def send_order_placed_email(self) -> None:
        self.send_order_placed_email_to_managers()
        self.send_order_placed_email_to_customer()
        self.send_order_placed_email_to_users_with_share_permission()

    def send_order_placed_email_to_managers(self) -> None:
        from apps.b3_core.mail import EmailSender
        email_sender = EmailSender()
        context = self.get_email_context()
        recipient_list = self.get_admin_email_list()
        email_sender.send_email(
            to=recipient_list,
            subject_template_path=f'b3_core/emails/'
            f'admin_partner_order_placed_subject.txt',
            body_txt_template_path=f'b3_core/emails/'
                                   f'admin_partner_order_placed_body.txt',
            body_html_template_path=f'b3_core/emails/'
                                    f'admin_partner_order_placed_body.html',
            extra_context=context,
        )

    def send_order_placed_email_to_customer(self) -> None:
        from apps.b3_core.mail import EmailSender
        email_sender = EmailSender()
        context = self.get_email_context()
        email_sender.send_email(
            to=self.purchased_by.email,
            subject_template_path=f'b3_core/emails/'
            f'user_order_placed_subject.txt',
            body_txt_template_path=f'b3_core/emails/'
                                   f'user_order_placed_body.txt',
            body_html_template_path=f'b3_core/emails/'
                                    f'user_order_placed_body.html',
            extra_context=context,
        )

    def send_order_placed_email_to_users_with_share_permission(self) -> None:
        from apps.b3_core.mail import EmailSender
        email_sender = EmailSender()
        recipient_list = self.get_list_of_users_with_share_permission()
        for recipient in recipient_list:
            context = self.get_email_context(
                {'name': recipient.userprofile.full_name}
            )
            email_sender.send_email(
                to=recipient.email,
                subject_template_path=f'b3_core/emails/user_shared_'
                f'order_placed_subject.txt',
                body_txt_template_path=f'b3_core/emails/user_shared_'
                                       f'order_placed_body.txt',
                body_html_template_path=f'b3_core/emails/user_shared_'
                                        f'order_placed_body.html',
                extra_context=context,
            )

    def get_list_of_users_with_share_permission(self) -> t.Sequence[td.User]:
        """
        Purchased by and others who have share permission
        :return:
        """
        users_with_access = []
        any_permission = get_users_with_perms(self.basket, attach_perms=True)
        for user, _ in any_permission.items():
            if self.purchased_by != user:
                users_with_access.append(user)
        return users_with_access

    def get_admin_email_list(self) -> t.Sequence[str]:
        """
        Printing service users and organization admin
        :return:
        """
        addresses = {email_address for _, email_address in settings.MANAGERS}

        # Add Email Address for the first fullfillment partner found for this
        # order ATTENTION: This doesn't support multiple fullfillment partners
        try:
            partner = self.partner
            for user in partner.users.all():
                if user.email:
                    addresses.add(user.email)
        except Partner.DoesNotExist:
            logger.warning("Order has no fulfillment partner")

        current_organization = get_current_org()
        if current_organization.notify_organizational_panel_admins:
            # If enabled, notify organization panel admins as well
            for admin in current_organization.organization_panel_admins.all():
                addresses.add(admin.email)
        return list(addresses)

    def get_status_email_text(self) -> str:
        from apps.b3_core.mail import EmailSender
        email_sender = EmailSender()
        context = self.get_email_context({
            'custom_message': '',
            'name': self.purchased_by.userprofile.full_name,
        })
        status = self.status.type.lower()
        _, body_text, _ = email_sender.render_email(
            subject_template_path=f'b3_core/emails/order_status/'
            f'order_status_{status}_subject.txt',
            body_txt_template_path=f'b3_core/emails/order_status/'
                                   f'order_status_{status}_body.txt',
            context=context,
        )
        return body_text

    def validate(self):
        """
        Currently only used in a management command (clean_orders).
        """
        if not self.basket:
            raise ValidationError("Order must have a Basket.")
        if not self.lines.all():
            raise ValidationError("Order must have Lines.")
        if not self.basket.lines.all():
            raise ValidationError("Orders Basket must have Lines.")
        if not self.partner:
            raise ValidationError("Orders must have a Partner.")

    def get_first_comment(self) -> t.Optional[td.Comment]:
        return self.basket.comments.first()
