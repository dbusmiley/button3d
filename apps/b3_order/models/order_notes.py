from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models, transaction
from django.db.models import Sum
from django.utils.module_loading import import_string

from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_core.utils import today
from apps.b3_order.models.order import Order
from apps.b3_order.models.order_line import OrderLine
from apps.partner.models.partner import Partner

__all__ = (
    'BaseOrderNote',
    'InvoiceNote',
    'InvoiceSequence',
    'OrderConfirmationNote',
    'DeliveryNote',
    'DeliveryNoteLine',
    'can_create_invoice',
)


class InvoiceSequenceManager(models.Manager):
    @transaction.atomic
    def get_next(self, partner: Partner, update=True):
        sequence, __ = self.get_or_create(partner=partner)
        reserved = sequence.sequence + 1
        if update:
            sequence.sequence = reserved
            sequence.save(update_fields=['sequence'])

        return reserved

    get_next.alters_data = True

    @transaction.atomic
    def reset_sequence(self, partner: Partner) -> bool:
        sequence, __ = self.update_or_create(
            partner=partner, defaults={'sequence': 0}
        )
        return sequence.sequence == 0

    reset_sequence.alters_data = True


class InvoiceSequence(models.Model):
    """
    Sequence pattern for invoices: unique serials per printing service

    We disallow access to the sequence from the partner to prevent
    accidental manipulation. The invoice should be the only one that can
    increment it.

    While we cannot 100% guarantee that, we can make footshooting harder.
    """
    objects = InvoiceSequenceManager()
    partner = models.ForeignKey(
        Partner, on_delete=models.CASCADE,
        related_name='partner_sequence_disabled+'
    )
    sequence = models.BigIntegerField(default=0, editable=False)


class BaseOrderNote(AbstractDeletedDateModel):
    issue_date = models.DateField(default=today)
    shipping_date = models.DateField(default=today)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    document = models.FileField(
        verbose_name='pdf document', editable=False,
        upload_to='pdf/order_notes', null=True,
        storage=import_string(settings.PRIVATE_FILE_STORAGE)()
    )
    comment = models.TextField(blank=True, null=True)
    notify_customer = models.BooleanField(default=False)
    note_prefix = models.CharField(editable=False, default='', max_length=10)
    prefix_field = None

    def __str__(self):
        return f'{self.note_prefix}{self.id}: {self.issue_date}'

    def set_note_prefix(self, prefix_provider: Partner):
        """
        The only way to set the `note_prefix` field.

        :param prefix_provider: A model that is authoritative for the note's
                                prefix. This is currently limited to service
                                partners.
        """
        if self.prefix_field is not None:
            self.note_prefix = getattr(prefix_provider, self.prefix_field)
        else:
            raise NotImplementedError('Either override or set prefix_field')

    class Meta:
        abstract = True


def can_create_invoice(order: Order):
    # This should never fail, but we check, since we need to grab the prefix
    # from the partner.
    if not order.partner:
        raise ValidationError(
            'No partner for order', code='NO_PARTNER'
        )


class InvoiceNote(BaseOrderNote):
    invoice_number = models.BigIntegerField(default=0, editable=False)
    is_cancelled = models.BooleanField(default=False)
    cancelled_by = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True,
        related_name='cancelling_user'
    )
    cancelled_at = models.DateTimeField(null=True)
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='invoice_notes'
    )
    prefix_field = 'invoice_prefix'

    def __str__(self):
        return f'{self.note_prefix}{self.invoice_number}: {self.issue_date}'

    def save(self, force_insert=False, *args, **kwargs):
        create = force_insert or not self.pk
        if not create:
            return super().save(force_insert=force_insert, *args, **kwargs)

        service = self.order.partner
        if service is None:
            raise RuntimeError(f"No partner for given order {self.order}")
        self.invoice_number = InvoiceSequence.objects.get_next(service)
        super().save(force_insert=force_insert, *args, **kwargs)

    def order_is_confirmed(self) -> bool:
        try:
            return self.order.order_confirmation_notes.exists()
        except ObjectDoesNotExist:
            return False


class OrderConfirmationNote(BaseOrderNote):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE,
        related_name='order_confirmation_notes'
    )
    prefix_field = 'order_confirmation_prefix'

    @property
    def order_confirmation_number(self):
        return f'{self.note_prefix}{self.pk}'

    def __str__(self):
        return f'{self.order_confirmation_number}: {self.issue_date} / ' \
               f'OrderID #{self.order.number}'


class DeliveryNote(BaseOrderNote):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='delivery_notes'
    )
    shipping_date = models.DateField(verbose_name='Shipping Date')
    show_thumbnails = models.BooleanField(default=False)
    prefix_field = 'delivery_note_prefix'

    @property
    def delivery_note_number(self):
        return f'{self.note_prefix}{self.pk}'

    @property
    def total_delivered(self):
        return self.processed_lines.aggregate(Sum('delivered')).delivered__sum

    def __str__(self):
        return f'{self.delivery_note_number}: {self.issue_date} / Order ID ' \
               f'#{self.order.number} / total delivered: ' \
               f'{self.total_delivered}'


class DeliveryNoteLine(AbstractDeletedDateModel):
    delivery_note = models.ForeignKey(
        DeliveryNote, on_delete=models.CASCADE, related_name='processed_lines'
    )
    order_line = models.ForeignKey(
        OrderLine, on_delete=models.CASCADE, related_name='delivered_lines'
    )
    delivered = models.PositiveIntegerField(
        verbose_name='quantity delivered', default=0,
    )

    def __str__(self):
        return f'Note #{self.delivery_note.delivery_note_number} / ' \
               f'OrderLine: {self.order_line.pk} / Delivered: {self.delivered}'
