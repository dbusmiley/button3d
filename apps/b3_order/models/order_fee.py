from decimal import Decimal

from django.db import models

from apps.b3_core.fields import CurrencyField
from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_order.models import Order
from apps.partner.pricing.price import Price
from button3d import type_declarations as td


class OrderFee(AbstractDeletedDateModel):
    """
    For fees applied to Order during checkout.
    Note:
    Minimum order fee's should be saved to the fields on :model: Order
    """
    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='applied_fees'
    )
    name = models.CharField(max_length=128)

    excl_tax = CurrencyField(default=Decimal(0.0))
    tax = CurrencyField()

    def __str__(self) -> str:
        return self.name

    @property
    def price(self) -> td.Price:
        return Price(
            currency=self.order.currency,
            excl_tax=self.excl_tax,
            tax=self.tax
        )
