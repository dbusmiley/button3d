from django.db import models

from apps.b3_attachement.models import AbstractAttachment


class OrderAttachment(AbstractAttachment):
    """
    An attachment either belongs directly to an order line or
    or to no specific line (general order attachment).
    In the latter case the order_line is null.
    """

    ATTACHMENT_FOLDER = 'order_attachments'
    order = models.ForeignKey(
        'b3_order.Order',
        related_name='attachments',
    )
    order_line = models.ForeignKey(
        'b3_order.OrderLine',
        null=True,
        blank=True,
        related_name='attachments',
    )
