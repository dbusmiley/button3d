from django.db import models

from apps.b3_order.models import OrderLine
from apps.partner.models import PostProcessingOption, PostProcessing, Color


class OrderLinePostProcessingOption(PostProcessingOption):
    line = models.ForeignKey(
        OrderLine,
        related_name='post_processing_options'
    )

    post_processing = models.ForeignKey(
        PostProcessing,
        related_name='order_lines'
    )
    color = models.ForeignKey(
        Color,
        related_name='order_lines',
        null=True,
        blank=True
    )

    class Meta:
        unique_together = (("line", "post_processing"),)

    def __str__(self) -> str:
        return f'{self.id}: {self.line} ' \
            f'| {self.post_processing} | {self.color}'
