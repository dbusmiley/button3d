import typing as t
import datetime
import button3d.type_declarations as td
from django.db import models, transaction
from django.db.models import Sum
from django.conf import settings

from apps.b3_core.fields import CurrencyField
from apps.b3_core.models import StlFile, Configuration, \
    AbstractDeletedDateModel
from apps.b3_mes.models import Workflow, Sequence
from apps.b3_order.models.order import Order
from apps.partner.models import StockRecord
from apps.partner.pricing.price import Price


class OrderLine(AbstractDeletedDateModel):

    # LINE INFORMATION
    # ----------------

    order = models.ForeignKey(
        Order,
        on_delete=models.CASCADE,
        related_name='lines'
    )
    name = models.CharField(max_length=254, default='', blank=True)

    # MODEL INFORMATION
    # -----------------

    stl_file = models.ForeignKey(
        StlFile,
        on_delete=models.PROTECT,
        to_field='uuid',
        related_name='order_lines'
    )
    stock_record = models.ForeignKey(
        StockRecord,
        on_delete=models.PROTECT,
        related_name='order_lines'
    )
    configuration = models.OneToOneField(
        Configuration,
        on_delete=models.PROTECT,
        related_name='order_line',
    )

    # PRICING INFORMATION
    # -------------------

    unit_price_value = CurrencyField()
    unit_price_tax = CurrencyField()

    quantity = models.PositiveIntegerField(default=1)

    item_discount_value = CurrencyField()
    item_discount_tax = CurrencyField()

    item_total_value = CurrencyField()
    item_total_tax = CurrencyField()

    # ADDITIONAL INFORMATION
    # ----------------------

    # instructions: MES Requirement
    instructions = models.TextField(
        help_text='Instructions related to processing this line',
        blank=True,
        default='',
    )
    # employees: MES Requirement
    employees = models.ManyToManyField(settings.AUTH_USER_MODEL)

    @property
    def partner(self) -> td.Partner:
        return self.stock_record.partner

    @property
    def post_processings(self) -> t.List[td.PostProcessing]:
        return [
            option.post_processing
            for option in self.post_processing_options.all()
        ]

    @property
    def stockrecord(self) -> td.StockRecord:
        """
        self.stock_record is the preferred usage and correct spelling -
        this is only for backwards compat with old basket functions
        """
        return self.stock_record

    @property
    def item_total(self) -> Price:
        return Price(
            excl_tax=self.item_total_value,
            currency=self.order.currency,
            tax=self.item_total_tax
        )

    @property
    def unit_price(self) -> Price:
        return Price(
            excl_tax=self.unit_price_value,
            currency=self.order.currency,
            tax=self.unit_price_tax
        )

    @property
    def item_discount(self) -> Price:
        return Price(
            excl_tax=self.item_discount_value,
            currency=self.order.currency,
            tax=self.item_discount_tax
        )

    @property
    def dimensions(self) -> t.Dict[str, float]:
        dimensions = self.stl_file.get_scaled_dimensions(self.scale)
        return dimensions

    @property
    def dimensions_formatted(self) -> str:
        dimensions = self.dimensions
        return f'{dimensions["h"]:.2f} ' \
            f'x {dimensions["w"]:.2f} ' \
            f'x {dimensions["d"]:.2f}'

    @property
    def scale(self) -> float:
        return float(self.configuration.scale)

    @property
    def unit(self) -> str:
        return self.configuration.unit

    @property
    def thumbnail_url(self) -> str:
        return self.stl_file.thumbnail_url

    @property
    def product(self) -> td.Product:
        return self.stock_record.product

    def __str__(self) -> str:
        return f'Order: #{self.order.number} | Line: #{self.id}, ' \
            f'{self.quantity} x {self.stock_record}'

    @property
    def delivered_quantity(self) -> int:
        return self.delivered_lines.aggregate(Sum('delivered')).delivered__sum

    @transaction.atomic
    def delete_old_workflow(self) -> None:
        try:
            workflow = Workflow.objects.get(part=self)
        except Workflow.DoesNotExist:
            return
        else:
            for status in workflow.statuses.all():
                status.delete()
            workflow.delete()

    def delete_old_sequences(self) -> None:
        self.sequences.all().delete()

    @property
    def status_id(self) -> t.Optional[int]:
        sequences = self.sequences.all()
        if not (hasattr(self, 'workflow') or sequences):
            return None

        sequences_status_ids = [sequence.status.id for sequence in sequences]
        statuses_ordered_ids = [
            status.id
            for status in self.workflow.get_statuses_ordered()]
        for status_id in statuses_ordered_ids:
            if status_id in sequences_status_ids:
                return status_id

        raise AttributeError(
            'Part has workflow and sequences but sequence '
            'statuses do not match workflow statuses')

    def status(self) -> str:
        status_id = self.status_id
        if not status_id:
            return ''

        status_name = self.workflow.statuses.get(id=status_id).name
        if self.sequences.exclude(status_id=status_id):
            return f'{status_name} - Mixed'

        return status_name

    def next_status(self) -> str:
        status_id = self.status_id
        if not status_id:
            return ''

        status = self.workflow.statuses.get(id=status_id)
        if status.next:
            return status.next.name
        return ''

    def get_assignable_sequences(self, workstation_id: int) -> t.List[int]:
        """
        Get sequences that can be assigned in a :model: `Job` for for the
        :model: `Workstation` identified by `workstation_id`
        :param workstation_id: id of workstation
        :return: list of :model: `Sequence` ids

        Although :param: `workstation_id` is not currently used, it is still
        passed because once the logic of how the sequences are determined is
        set, we would most likely need the workstation_id
        """
        # TODO: assert correct logic after call with EOS
        if not Workflow.objects.filter(part=self).exists():
            return []
        statuses = self.workflow.get_statuses_ordered()
        index = len(statuses) - 1
        while index > -1:
            status = statuses[index]
            if status.workstation and status.workstation.is_printing:
                break
            index -= 1

        pre_last_printing_statuses = statuses[:index]
        pre_last_printing_statuses_ids = [
            s.id for s in pre_last_printing_statuses]

        sequences_qs = Sequence.objects.filter(
            status_id__in=pre_last_printing_statuses_ids,
            job=None
        )
        sequences = sequences_qs.values_list('id', flat=True)
        return list(sequences)

    def get_estimated_delivery_date(self) -> datetime.date:
        """
        Get an estimation of the delivery date based
        on (ordered in terms of accuracy):
            1. Order confirmation -> confirmed_delivery_date
            2. DeliveryNote -> shipping_date
            3. ShippingMethod -> shipping_days_max

        This is used in MES for suggesting which parts to be printed first
        :return: date
        """
        if self.order.confirmed_delivery_date:
            return self.order.confirmed_delivery_date

        if self.order.delivery_notes.all().exists():
            return self.order.delivery_notes.first().shipping_date

        if self.order.shipping_method:
            return self.order.datetime_placed.date() + datetime.timedelta(
                self.order.shipping_method.shipping_days_max)

        # TODO: update after checking what are all the
        # possible ways to figure out delivery date
        raise AttributeError('Cannot figure out delivery date')

    def is_fitting_in(self, workstation: td.Workstation, used_space=0) -> bool:
        """
        Check if part can fit with in :param: `workstation`
        :return: bool
        """
        if(
            self.dimensions['w'] > workstation.dimension_x or
            self.dimensions['h'] > workstation.dimension_y or
            self.dimensions['d'] > workstation.dimension_z
        ):
            return False

        required_space = self.required_space(workstation)
        if workstation.bounding_box3d:
            available_volume = workstation.usable_volume - used_space
            return (available_volume - required_space) > 0

        # TODO: add logic for possible rotation of part to fit
        available_area = workstation.usable_area - used_space
        return (available_area - required_space) > 0

    def required_space(self, workstation: td.Workstation) -> float:
        """
        Get volume/area required for this part to be printed
        in :param: `workstation`
        :param: :model: `Workstation` instance
        :return: The volume or 2d-space required
        """
        if workstation.bounding_box3d:
            return self.dimensions['w'] * self.dimensions['h'] * \
                self.dimensions['d']
        return self.dimensions['w'] * self.dimensions['d']

    def move_all_sequences_to_next_status(self) -> None:
        for sequence in self.sequences.all():
            if sequence.status.next:
                sequence.status = sequence.status.next
                sequence.save()
            else:
                raise ValueError(f'Sequence {sequence} is at the last status'
                                 f' of it\'s workflow')

    def check_all_sequences_at_same_status(self) -> bool:
        statuses = self.sequences.all().values_list('status_id', flat=True)
        return len(set(statuses)) == 1
