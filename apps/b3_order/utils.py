import button3d.type_declarations as td

__all__ = (
    'OrderNumberGenerator',
)


class OrderNumberGenerator:
    """
    Simple object for generating order numbers.

    We need this as the order number is often required for payment
    which takes place before the order model has been created.
    """

    @staticmethod
    def order_number(basket: td.Project):
        """
        Return an order number for a given basket
        """
        return 100000 + basket.id
