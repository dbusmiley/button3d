from django.contrib.auth.models import User
from rest_framework import serializers

from apps.b3_address.models import Address
from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_order.models import Order, OrderLine, \
    OrderLinePostProcessingOption
from apps.basket.models import Comment
from apps.partner.pricing.price import Price


class PriceSerializer(serializers.Serializer):
    excl_tax = CurrencyField(coerce_to_string=True)
    incl_tax = CurrencyField(coerce_to_string=True)
    tax = CurrencyField(coerce_to_string=True)
    currency = serializers.CharField()
    is_tax_known = serializers.SerializerMethodField()

    def get_is_tax_known(self, price):
        return price.tax is not None


class PostProcessingSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='post_processing.title')
    color = serializers.SerializerMethodField()

    class Meta:
        model = OrderLinePostProcessingOption
        fields = (
            'name',
            'color',
        )

    def get_color(self, post_processing_option):
        if post_processing_option.color:
            return str(post_processing_option.color)
        else:
            return None


class OrderItemSerializer(serializers.ModelSerializer):
    service_id = serializers.IntegerField(source='partner.id')
    product = serializers.CharField(source='stock_record.product.title')
    volume = serializers.FloatField(source='stl_file.parameter.volume')
    scale = serializers.FloatField(source='configuration.scale')
    partner_sku = serializers.CharField(source='stock_record.partner_sku')
    uuid = serializers.CharField(source='stl_file.uuid')
    partner_name = serializers.CharField(source='partner.name')
    line_price = serializers.SerializerMethodField()
    unit_price = serializers.SerializerMethodField()
    line_discount = serializers.SerializerMethodField()
    support_volume = serializers.SerializerMethodField()
    post_processings = PostProcessingSerializer(
        many=True,
        source='post_processing_options',
    )

    class Meta:
        model = OrderLine
        fields = (
            'service_id',
            'stock_record',
            'product',
            'volume',
            'quantity',
            'scale',
            'support_volume',
            'partner_sku',
            'uuid',
            'partner_name',
            'line_price',
            'unit_price',
            'line_discount',
            'post_processings',
        )

    def get_unit_price(self, order_line):
        return PriceSerializer(order_line.unit_price).data

    def get_line_price(self, order_line):
        return PriceSerializer(order_line.item_total).data

    def get_line_discount(self, order_line):
        return PriceSerializer(order_line.item_discount).data

    def get_support_volume(self, order_line):
        if order_line.stockrecord.support_enabled:
            return order_line.stl_file.support_structure.get_values(
                order_line.stockrecord.support_angle,
                order_line.stockrecord.support_offset,
                order_line.configuration.scale
            )['h-'][0]
        else:
            return None


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
        )


class AddressSerializer(serializers.ModelSerializer):
    line3 = serializers.CharField(source='company_name')
    postcode = serializers.CharField(source='zip_code')

    class Meta:
        model = Address
        fields = (
            'first_name',
            'last_name',
            'title',
            'line1',
            'line2',
            'line3',
            'state',
            'city',
            'postcode',
            'country',
        )


class CommentUserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='user_id')
    name = serializers.SerializerMethodField()
    service = serializers.CharField(source='basket.partner.name')

    class Meta:
        model = User
        fields = (
            'id',
            'service',
            'name',
        )

    def get_name(self, comment):
        user = comment.user
        return user.userprofile.full_name

    def get_service(self, comment):
        return comment.basket.partner.name


class CommentSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='uuid')
    parent = serializers.SerializerMethodField()
    user = CommentUserSerializer(source='*')
    created_pretty_date = serializers.DateTimeField(source='created')
    edited_pretty_date = serializers.DateTimeField(source='edited')

    class Meta:
        model = Comment
        fields = (
            'id',
            'parent',
            'text',
            'user',
            'edited_pretty_date',
            'created_pretty_date',
        )

    def get_parent(self, comment):
        if comment.parent:
            return str(comment.parent.uuid)
        else:
            return None


class OrderObjectSerializer(serializers.ModelSerializer):
    domain = serializers.SerializerMethodField()
    order_id = serializers.IntegerField(source='id')
    project_name = serializers.CharField(source='basket.title')
    project_id = serializers.IntegerField(source='basket.id')
    vat_number = serializers.CharField(source='billing_address.vat_id')
    order_date = serializers.DateTimeField(source='datetime_placed')
    order_number = serializers.CharField(source='number')
    instructions = serializers.CharField(source='delivery_instructions')
    shipping_method = serializers.CharField(source='shipping_method.name')
    items = OrderItemSerializer(source='lines', many=True)

    comments = CommentSerializer(source='basket.comments', many=True)
    user = UserSerializer(source='purchased_by')
    shipping_address = AddressSerializer()
    billing_address = AddressSerializer()
    total_price = serializers.SerializerMethodField()
    shipping_price = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'domain',
            'order_id',
            'project_name',
            'project_id',
            'vat_number',
            'order_date',
            'order_number',
            'instructions',
            'pickup_location',
            'shipping_method',
            'currency',
            'items',
            'comments',
            'user',
            'shipping_address',
            'billing_address',
            'total_price',
            'shipping_price',
        )

    def get_domain(self, order):
        return order.site.domain

    def get_total_price(self, order):
        return PriceSerializer(
            Price(
                currency=order.currency,
                excl_tax=order.total_value,
                tax=order.tax_rate
            )
        ).data

    def get_shipping_price(self, order):
        return PriceSerializer(
            Price(
                currency=order.currency,
                excl_tax=order.shipping_value,
                tax=order.tax_rate
            )
        ).data


class WebhookDataSerializer(serializers.ModelSerializer):
    object = OrderObjectSerializer(source='*')

    class Meta:
        model = Order
        fields = (
            'object',
        )


class OrderWebhookSerializer(serializers.ModelSerializer):
    data = WebhookDataSerializer(source='*')
    created = serializers.DateTimeField(source='datetime_placed')
    type = serializers.SerializerMethodField()
    api_version = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'created',
            'api_version',
            'type',
            'data',
        )

    def get_type(self, order):
        return 'order.created'

    def get_api_version(self, order):
        return '2018-09-26'
