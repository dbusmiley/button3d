
from decimal import Decimal
from factory.django import DjangoModelFactory
import factory

from apps.b3_address.factories import AddressFactory
from apps.b3_checkout.factories import PaymentFactory
from apps.b3_order.models import Order, OrderLine, \
    OrderLinePostProcessingOption, OrderAttachment, OrderFee
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import VoucherFactory, PostProcessingFactory, \
    BasketLineFactory, AbstractAttachmentFactory
from apps.partner.pricing.calculators.base import \
    get_billing_and_shipping_country, get_is_b2b_transaction
from apps.partner.pricing.calculators.project import ProjectPriceCalculator, \
    LinePriceCalculator

NO_PAYMENT = 'no-payment'


class OrderFactory(DjangoModelFactory):

    class Meta:
        model = Order

    project = factory.LazyFunction(lambda: BasketLineFactory().basket)

    title = 'MyOrder'
    number = factory.Sequence(lambda n: str(100000 + n))
    site = factory.LazyFunction(get_current_site)
    customer_language = 'en'
    customer_ip = '127.0.0.1'
    customer_reference = 'MyCustomerReference'

    billing_address = factory.SubFactory(AddressFactory, user=None)
    shipping_address = factory.SubFactory(AddressFactory, user=None)
    partner = factory.SelfAttribute('project.partner')
    purchased_by = factory.SelfAttribute('project.owner')
    shipping_method = factory.LazyAttribute(
        lambda self: self.partner.shipping_methods.first()
    )

    voucher = factory.SubFactory(VoucherFactory)
    currency = factory.SelfAttribute('price.currency')
    tax_rate = factory.SelfAttribute('price.tax_rate')
    min_price_value = factory.SelfAttribute('price.min_price.excl_tax')
    min_price_tax = factory.SelfAttribute('price.min_price.tax')
    subtotal_value = factory.SelfAttribute('price.subtotal.excl_tax')
    subtotal_tax = factory.SelfAttribute('price.subtotal.tax')
    shipping_value = factory.SelfAttribute('price.shipping.excl_tax')
    shipping_tax = factory.SelfAttribute('price.shipping.tax')
    fees_value = factory.SelfAttribute('price.fees.excl_tax')
    fees_tax = factory.SelfAttribute('price.fees.tax')
    min_price_diff_value = factory.SelfAttribute(
        'price.min_price_diff.excl_tax'
    )
    min_price_diff_tax = factory.SelfAttribute('price.min_price_diff.tax')
    voucher_discount_value = factory.SelfAttribute(
        'price.voucher_discount.excl_tax'
    )
    voucher_discount_tax = factory.SelfAttribute('price.voucher_discount.tax')
    total_value = factory.SelfAttribute('price.total.excl_tax')
    total_tax = factory.SelfAttribute('price.total.tax')

    price = factory.LazyAttribute(
        lambda self: OrderFactory._create_price_calculator(self)
    )

    @classmethod
    def _create(cls, model_class, *args,  **kwargs):
        price_calculator = kwargs.pop('price', None)
        order = super()._create(model_class, *args, **kwargs)
        cls._create_lines(order, price_calculator)
        cls._create_fees(order, price_calculator)
        cls._create_attachments(order)

        return order

    @classmethod
    def _create_price_calculator(cls, order):
        shipping_country, billing_country = get_billing_and_shipping_country(
            order.billing_address,
            order.shipping_address
        )
        is_b2b_transaction = get_is_b2b_transaction(order.billing_address)

        return ProjectPriceCalculator(
            project=order.project,
            shipping_method=order.shipping_method,
            voucher=order.voucher,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

    @classmethod
    def _create_lines(cls, order, price_calculator):
        if price_calculator is None:
            return
        for line_price_calculator in price_calculator.lines:
            OrderLineFactory(
                order=order,
                project_line=line_price_calculator.line,
                price=line_price_calculator
            )

    @classmethod
    def _create_fees(cls, order, price_calculator):
        if price_calculator is None:
            return
        for fee in price_calculator.fee_objects:
            OrderFeeFactory(
                order=order,
                name=fee.name,
                excl_tax=fee.amount.excl_tax,
                tax=fee.amount.tax
            )

    @classmethod
    def _create_attachments(cls, order):
        project_attachments = order.project.attachments.filter(
            basket_line__isnull=True
        )
        for attachment in project_attachments:
            OrderAttachmentFactory(
                order=order,
                filename=attachment.filename,
                content=attachment.file.read(),
                uploader=attachment.uploader,
            )

    @factory.post_generation
    def generate_payment(obj, create, extracted, **kwargs):
        if create:
            if extracted:
                if extracted == NO_PAYMENT:
                    return
                obj.payment = extracted
            else:
                obj.payment = PaymentFactory(
                    order=obj,
                    currency=obj.currency,
                    amount=obj.total_value,
                    payment_method=obj.partner.payment_methods.first()
                )


class OrderFeeFactory(DjangoModelFactory):
    class Meta:
        model = OrderFee

    order = factory.SubFactory(OrderFactory)
    name = 'MyFee'
    excl_tax = Decimal('10.00')
    tax = Decimal('1.90')


class OrderAttachmentFactory(AbstractAttachmentFactory):
    class Meta:
        model = OrderAttachment

    order = factory.SubFactory(OrderFactory)


class OrderLineFactory(DjangoModelFactory):
    class Meta:
        model = OrderLine

    project_line = factory.LazyAttribute(
        lambda line_stub: line_stub.order.project.lines.first()
    )

    order = factory.SubFactory(OrderFactory)
    name = factory.LazyAttribute(lambda self: self.project_line.name or '')
    stl_file = factory.SelfAttribute('project_line.stl_file')
    stock_record = factory.SelfAttribute('project_line.stockrecord')
    configuration = factory.SelfAttribute('project_line.configuration')
    quantity = factory.SelfAttribute('project_line.quantity')

    unit_price_value = factory.SelfAttribute('price.unit_price.excl_tax')
    unit_price_tax = factory.SelfAttribute('price.unit_price.tax')
    item_discount_value = factory.SelfAttribute('price.item_discount.excl_tax')
    item_discount_tax = factory.SelfAttribute('price.item_discount.tax')
    item_total_value = factory.SelfAttribute('price.item_total.excl_tax')
    item_total_tax = factory.SelfAttribute('price.item_total.tax')

    price = factory.LazyAttribute(
        lambda self: OrderLineFactory._create_price_calculator(self)
    )

    @classmethod
    def _create(cls, model_class, *args,  **kwargs):
        project_line = kwargs.pop('project_line')
        kwargs.pop('price')

        order_line = super()._create(model_class, *args, **kwargs)
        cls._create_post_processing_options(order_line, project_line)
        cls._create_attachments(order_line, project_line)

        return order_line

    @classmethod
    def _create_price_calculator(cls, order_line):
        billing_address = order_line.order.billing_address
        shipping_address = order_line.order.shipping_address
        shipping_country, billing_country = get_billing_and_shipping_country(
            billing_address,
            shipping_address
        )
        is_b2b_transaction = get_is_b2b_transaction(billing_address)
        currency = order_line.order.currency

        return LinePriceCalculator(
            line=order_line.line,
            currency=currency,
            shipping_country=shipping_country,
            billing_country=billing_country,
            is_b2b_transaction=is_b2b_transaction
        )

    @classmethod
    def _create_post_processing_options(cls, order_line, project_line):
        for option in project_line.post_processing_options.all():
            OrderLinePostProcessingOptionFactory(
                line=order_line,
                post_processing=option.post_processing,
                color=option.color
            )

    @classmethod
    def _create_attachments(cls, order_line, project_line):
        for attachment in project_line.attachments.all():
            OrderAttachmentFactory(
                order=order_line.order,
                order_line=order_line,
                filename=attachment.filename,
                content=attachment.file.read(),
                uploader=attachment.uploader,
            )


class OrderLinePostProcessingOptionFactory(DjangoModelFactory):
    class Meta:
        model = OrderLinePostProcessingOption

    line = factory.SubFactory(OrderLineFactory)
    post_processing = factory.SubFactory(PostProcessingFactory)
    color = factory.LazyAttribute(
        lambda option: option.post_processing.colors.first()
    )
