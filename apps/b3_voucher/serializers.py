from decimal import Decimal as D

from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_voucher.models import Voucher


class ServiceVoucherSerializer(
        PayloadConverterMixin,
        serializers.ModelSerializer):

    times_used = serializers.SerializerMethodField()
    total_price_used = serializers.SerializerMethodField()

    def get_times_used(self, obj: Voucher) -> int:
        return obj.get_global_usage()

    def get_total_price_used(self, obj: Voucher) -> D:
        return obj.get_total_price().excl_tax

    def validate(self, attrs: dict) -> dict:
        if attrs['action_type'] == Voucher.PERCENTAGE:
            if attrs['action_value'] < 0 or attrs['action_value'] > 100:
                raise serializers.ValidationError(
                    {'action_value':
                     'action_value must be between 0 and 100'
                     'when action_type is "percentage".'
                     }
                )
        return attrs

    def validate_code(self, value: str) -> str:
        code = value.strip().upper()
        if not code:
            raise serializers.ValidationError(
                {'code': 'Code must not be empty'}
            )
        return code

    class Meta:
        model = Voucher
        fields = ('id',
                  'creation_date',
                  'code',
                  'action_type',
                  'action_value',
                  'start_date',
                  'end_date',
                  'limit_type',
                  'limit_amount',
                  'times_used',
                  'total_price_used'
                  )
