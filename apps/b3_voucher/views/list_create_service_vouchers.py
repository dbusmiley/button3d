import typing as t
from rest_framework.generics import ListCreateAPIView

from apps.b3_voucher.serializers import ServiceVoucherSerializer
from apps.b3_api.services.views import BaseServiceView
from apps.b3_voucher.models import Voucher


class ListCreateServiceVouchers(BaseServiceView, ListCreateAPIView):
    serializer_class = ServiceVoucherSerializer

    def get_queryset(self) -> t.Type[Voucher]:
        return Voucher.objects.filter(
            partner=self.get_object()
        )

    def perform_create(
        self,
        serializer: ServiceVoucherSerializer
    ) -> None:

        serializer.save(partner=self.get_object())
