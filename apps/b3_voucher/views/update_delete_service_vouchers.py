from django.http import Http404
from rest_framework.generics import RetrieveUpdateDestroyAPIView, \
    get_object_or_404

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.views import BaseServiceView
from apps.b3_voucher.serializers import ServiceVoucherSerializer
from apps.b3_voucher.models import Voucher


class UpdateDeleteServiceVouchers(
    BaseServiceView, RetrieveUpdateDestroyAPIView
):
    serializer_class = ServiceVoucherSerializer

    def get_object(self) -> Voucher:
        try:
            return get_object_or_404(
                Voucher,
                pk=self.kwargs['voucher_id'],
                partner__id=self.kwargs['service_id'],
            )
        except Voucher.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='Voucher',
                    object_id=self.kwargs['voucher_id']
                )
            )
