from django.conf.urls import url

from .views import ListCreateServiceVouchers,\
    UpdateDeleteServiceVouchers

urlpatterns = [
    url(r'^$', ListCreateServiceVouchers.as_view()),
    url(r'^(?P<voucher_id>[0-9]+)/$', UpdateDeleteServiceVouchers.as_view())
]
