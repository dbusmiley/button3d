import button3d.type_declarations as td

from decimal import Decimal as D

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from apps.b3_core.models import AbstractDeletedDateModel


class Voucher(AbstractDeletedDateModel):

    PERCENTAGE = 'percentage'
    FIXED = 'fixed'
    ACTION_TYPES = (
        (PERCENTAGE, "Percentage"),
        (FIXED, "Fixed Amount")
    )

    PER_USER = 'per-user'
    GLOBAL = 'global'
    LIMIT_TYPES = (
        (PER_USER, 'Per User'),
        (GLOBAL, 'Global')
    )

    creation_date = models.DateTimeField(
        auto_now_add=True
    )

    partner = models.ForeignKey(
        'partner.Partner',
        related_name='vouchers',
        on_delete=models.CASCADE
    )

    code = models.CharField(max_length=255)

    action_type = models.CharField(max_length=255, choices=ACTION_TYPES)

    action_value = models.DecimalField(max_digits=9, decimal_places=2)

    start_date = models.DateTimeField(null=True, blank=True)

    end_date = models.DateTimeField(null=True, blank=True)

    limit_type = models.CharField(
        max_length=255,
        choices=LIMIT_TYPES,
        null=True,
        blank=True
    )

    limit_amount = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        unique_together = (("partner", "code"),)

    def __str__(self) -> str:
        if self.action_type == self.PERCENTAGE:
            return f'{self.action_value:.2f}%'
        else:
            return f'{self.action_value}{self.partner.price_currency}'

    def is_applicable(self, user: User) -> bool:
        if self.is_deleted:
            return False

        if self.start_date and self.start_date > timezone.now():
            return False

        if self.end_date and self.end_date < timezone.now():
            return False

        if self.limit_type == self.GLOBAL and \
                self.get_global_usage() >= self.limit_amount:
            return False

        if self.limit_type == self.PER_USER and \
                self.get_user_usage(user) >= self.limit_amount:
            return False

        return True

    def get_global_usage(self) -> int:
        return self.orders.count()

    def get_user_usage(self, user: User) -> int:
        return self.orders.filter(purchased_by=user).count()

    def get_total_price(self) -> td.Price:
        total = self.partner.create_price(excl_tax=D('0'))
        for order in self.orders.all():
            total += order.voucher_discount
        return total
