from django.contrib import admin

from apps.b3_voucher.models import Voucher


@admin.register(Voucher)
class VoucherAdmin(admin.ModelAdmin):
    raw_id_fields = ('partner',)
