import json
from decimal import Decimal as D

from apps.b3_address.factories import AddressFactory
from apps.b3_organization.utils import get_current_site
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, VoucherFactory, \
    BasketFactory, StockRecordFactory, StlFileFactory, Voucher, \
    BasketLineFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket
from apps.partner.pricing.price import Price


class VoucherApplicationTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        # Create Partner
        self.site = get_current_site()
        self.partner = PartnerFactory(name='Meltwerk', site=self.site)
        self.partner.users.add(self.user)
        self.partner.save()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = get_current_site()
        self.basket.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)
        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()
        self.line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line2.save()
        self.basket.save()

        self.voucher_fixed = VoucherFactory(
            partner=self.partner,
            code='FIXED',
            action_type=Voucher.FIXED,
            action_value="11.11"
        )

        self.voucher_huge = VoucherFactory(
            partner=self.partner,
            code='HUGE',
            action_type=Voucher.FIXED,
            action_value="9999999"
        )

        self.voucher_percentage = VoucherFactory(
            partner=self.partner,
            code='PERCENTAGE',
            action_type=Voucher.PERCENTAGE,
            action_value="1"
        )

        self.voucher_global = VoucherFactory(
            partner=self.partner,
            code='GLOBAL',
            limit_type='global',
            limit_amount=2,
            action_type=Voucher.FIXED,
            action_value="11.11"
        )

        self.voucher_per_user = VoucherFactory(
            partner=self.partner,
            code='PER-USER',
            limit_type='per-user',
            limit_amount=2,
            action_type=Voucher.FIXED,
            action_value="11.11"
        )

        self.start_date = VoucherFactory(
            partner=self.partner,
            code='STARTDATE',
            start_date='3000-07-21T17:32:28Z',
        )

        self.start_date = VoucherFactory(
            partner=self.partner,
            code='ENDDATE',
            end_date='2000-07-21T17:32:28Z',
        )

    def _create_order(self, user, voucher, total):
        # Temporary login with another user
        self.client.login(email=user.email, password=self.test_password)

        self.project = BasketLineFactory(
            basket__owner=user,
            stockrecord__partner=self.partner
        ).basket

        self.shipping_method = ShippingMethodFactory(partner=self.partner)
        self.shipping_method.countries.add(self.partner.address.country)

        self.billing_address = AddressFactory(user=user)
        self.shipping_address = AddressFactory(user=user)
        self.payment_method = self.partner.payment_methods.first()
        payload = {
            'billingAddressId': self.billing_address.id,
            'pickupLocationId': None,
            'shipping': {
                'methodId': self.shipping_method.id,
                'addressId': self.shipping_address.id,
                'deliveryInstructions': 'plz deliver.'
            },
            'payment': {
                'methodId': self.payment_method.id,
                'authorizedAmount': total,
                'currency': 'EUR',
                'details': {}
            },
            'voucherCode': voucher.code,
            'additionalInformation': {
                'reference': 'XYZ',
                'customerNumber': '123'
            }
        }
        response = self.client.post(
            f'/api/v2.0/user-panel/projects/{self.project.id}/orders/',
            data=json.dumps(payload),
            content_type="application/json"
        )

        self.client.login(email=self.user.email, password=self.test_password)

        return response

    def test_fixed_voucher_applies_on_order(self):
        global_usage = self.voucher_fixed.get_global_usage()
        user_usage = self.voucher_fixed.get_user_usage(self.user)
        total_price = self.voucher_fixed.get_total_price()

        response = self._create_order(self.user, self.voucher_fixed, '283.17')
        self.assertEqual(response.status_code, 201)

        self.assertEqual(
            self.voucher_fixed.get_global_usage(),
            global_usage + 1
        )
        self.assertEqual(
            self.voucher_fixed.get_user_usage(self.user),
            user_usage + 1
        )

        self.assertEqual(
            self.voucher_fixed.get_total_price(),
            total_price + Price(
                excl_tax=D('11.11'),
                currency='EUR',
                tax=D('2.11')
            )
        )

    def test_percentage_voucher_applies_on_order(self):
        response = self._create_order(
            self.user,
            self.voucher_percentage,
            '293.43'
        )
        self.assertEqual(response.status_code, 201)

    def test_failes_on_order_global_limit(self):
        response = self._create_order(
            self.user,
            self.voucher_global,
            '283.17'
        )
        user2 = UserFactory(password=self.test_password)
        self.assertEqual(response.status_code, 201)
        response = self._create_order(
            user2,
            self.voucher_global,
            '283.17'
        )
        self.assertEqual(response.status_code, 201)
        response = self._create_order(
            self.user,
            self.voucher_global,
            '283.17'
        )
        self.assertEqual(response.status_code, 400)

    def test_failes_on_order_user_limit(self):
        response = self._create_order(
            self.user,
            self.voucher_per_user,
            '283.17'
        )
        self.assertEqual(response.status_code, 201)
        response = self._create_order(
            self.user,
            self.voucher_per_user,
            '283.17'
        )
        self.assertEqual(response.status_code, 201)
        response = self._create_order(
            self.user,
            self.voucher_per_user,
            '283.17'
        )
        self.assertEqual(response.status_code, 400)

        user2 = UserFactory(password=self.test_password)
        response = self._create_order(
            user2,
            self.voucher_per_user,
            '283.17'
        )
        self.assertEqual(response.status_code, 201)

    def test_fixed_voucher(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': " FIXED  "}
        )
        dict = json.loads(response.content)

        self.assertEqual(
            dict['priceSummary']['voucherDiscount']['exclusiveTax'],
            '11.11'
        )

        self.assertEqual(
            dict['priceSummary']['voucherDiscount']['inclusiveTax'],
            '13.22'
        )

        self.assertEqual(
            dict['priceSummary']['total']['exclusiveTax'],
            '467.03'
        )
        self.assertEqual(
            dict['priceSummary']['total']['inclusiveTax'],
            '555.76'
        )

    def test_fixed_voucher_negative(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "HUGE"}
        )
        dict = json.loads(response.content)

        self.assertEqual(
            dict['priceSummary']['voucherDiscount']['exclusiveTax'],
            '478.14'
        )

        self.assertEqual(
            dict['priceSummary']['total']['exclusiveTax'],
            '0.00'
        )

        self.assertEqual(
            dict['priceSummary']['total']['inclusiveTax'],
            '0.00'
        )

    def test_percentage_voucher(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "PERCENTAGE"}
        )
        dict = json.loads(response.content)

        self.assertEqual(
            dict['priceSummary']['voucherDiscount']['exclusiveTax'],
            '4.78'
        )

        self.assertEqual(
            dict['priceSummary']['total']['exclusiveTax'],
            '473.36'
        )

    def test_voucher_error_on_start_date(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "STARTDATE"}
        )

        self.assertEqual(response.status_code, 400)

    def test_voucher_error_on_end_date(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "ENDDATE"}
        )

        self.assertEqual(response.status_code, 400)

    def test_non_existing_voucher_code(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "OSCAR"}
        )

        self.assertEqual(response.status_code, 400)

    def test_voucher_code_spaces(self):
        response = self.client.get(
            f'/api/v2.0/user-panel/projects/{self.basket.pk}/price/',
            {'voucherCode': "  "}
        )
        dict = json.loads(response.content)

        self.assertEqual(
            dict['priceSummary']['total']['exclusiveTax'],
            '478.14'
        )
