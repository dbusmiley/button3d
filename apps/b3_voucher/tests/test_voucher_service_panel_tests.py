import json

from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import PartnerFactory, VoucherFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class VoucherServicePanelTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        # Create Partner
        self.site = get_current_site()
        self.partner = PartnerFactory(name='Meltwerk', site=self.site)
        self.partner.users.add(self.user)
        self.partner.save()

        # Create a Voucher
        self.voucher = VoucherFactory(
            partner=self.partner,
            code='THIS-IS-UNIQUE'
        )

        self.vouchers_url = \
            f'/api/v2.0/service-panel/services/{self.partner.id}/vouchers/'

    def test_voucher_creation(self):
        data = {
            'code': 'TESTME',
            'actionType': 'percentage',
            'actionValue': 10,
            'startDate': '2017-07-21T17:32:28Z',
            'endDate': '2018-07-21T17:32:28Z',
            'limitType': 'per-user',
            'limitAmount': 1
        }
        response = self.client.post(
            self.vouchers_url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)

    def test_voucher_invalid_percentage(self):
        data = {
            'code': 'TESTME',
            'actionType': 'percentage',
            'actionValue': 1000,
        }
        response = self.client.post(
            self.vouchers_url,
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)

    def test_voucher_creation_no_permission(self):
        no_access = PartnerFactory(name='P2', site=self.site)
        data = {
            'code': 'TESTME',
            'actionType': 'percentage',
            'actionValue': 10,
        }

        response = self.client.post(
            f'/api/v2.0/service-panel/services/{no_access.id}/vouchers/',
            json.dumps(data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 403)

    def test_voucher_access_no_permission(self):
        no_access = PartnerFactory(name='P2', site=self.site)
        voucher = VoucherFactory(partner=no_access, code='NOACCESS')
        vouchers_url = \
            f'/api/v2.0/service-panel/services/{no_access.id}/vouchers/'
        response = self.client.get(f'{vouchers_url}{voucher.id}/')
        self.assertEqual(response.status_code, 403)

    def test_voucher_update(self):
        data = {
            'code': 'UPDATEME',
            'actionType': 'percentage',
            'actionValue': 10,
        }
        response = self.client.post(
            self.vouchers_url,
            json.dumps(data),
            content_type='application/json'
        )
        voucher_id = json.loads(response.content)['id']
        update = {
            'code': 'UPDATEME',
            'actionType': 'percentage',
            'actionValue': 20
        }
        response = self.client.put(
            f'{self.vouchers_url}{voucher_id}/',
            json.dumps(update),
            content_type='application/json'
        )
        self.assertEqual(
            json.loads(response.content)['actionValue'],
            '20.00'
        )

    def test_voucher_list(self):
        data = {
            'code': 'LISTME',
            'actionType': 'percentage',
            'actionValue': 10,
            'limitType': 'per-user',
            'limitAmount': 1
        }
        self.client.post(
            self.vouchers_url,
            json.dumps(data),
            content_type='application/json'
        )
        response = self.client.get(self.vouchers_url)
        self.assertTrue(len(json.loads(response.content)))

    def test_voucher_delete(self):
        data = {
            'code': 'DELETEME',
            'actionType': 'percentage',
            'actionValue': 10,
            'startDate': '2017-07-21T17:32:28Z',
            'endDate': '2018-07-21T17:32:28Z',
            'limitType': 'per-user',
            'limitAmount': 1
        }
        response = self.client.post(
            self.vouchers_url,
            json.dumps(data),
            content_type='application/json'
        )
        voucher_id = json.loads(response.content)['id']

        response = self.client.delete(
            f'{self.vouchers_url}{voucher_id}/',
        )

        self.assertEqual(response.status_code, 204)
