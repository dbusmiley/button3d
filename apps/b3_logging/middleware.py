import uuid
import logging

from apps.b3_core.base import BaseMiddleware

logger = logging.getLogger(__name__)


class RequestUUIDMiddleware(BaseMiddleware):
    """
    Assigns unique identifier (UUID) to every request. This UUID is added to
    every log record to differentiate them between requests
    """

    def __call__(self, request):
        request.uuid = uuid.uuid4()

        response = self.get_response(request)

        if hasattr(request, 'uuid'):
            del request.uuid
        return response


class RequestLoggingMiddleware(BaseMiddleware):
    """
    Emits log message about current request method, path, status code,
    user IP and User-Agent
    """

    def __call__(self, request):
        response = self.get_response(request)

        method = request.method.upper()
        path = request.get_full_path()
        status = response.status_code
        ip = request.META.get('REMOTE_ADDR', '')
        user_agent = request.META.get('HTTP_USER_AGENT', '')
        logger.info(f'{method} {path} {status} {ip} {user_agent}')
        return response
