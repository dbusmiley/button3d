import logging


class ExtendedLoggingHandler(logging.StreamHandler):
    """
    This logging handler adds extra info to log records:
     - user_id
     - session_key
     - request_uuid (unique number assigned to each request)
    in order to differentiate between different users and requests
    """

    def emit(self, record):
        from apps.b3_core.middleware import THREAD_LOCALS

        if hasattr(THREAD_LOCALS, 'current_request'):
            request = THREAD_LOCALS.current_request

            record.request_uuid = getattr(request, 'uuid', None)

            session = getattr(request, 'session', None)
            if session is not None:
                record.session_key = getattr(session, 'session_key', None)
            else:
                record.session_key = None

            user = getattr(request, 'user', None)
            if user is not None and user.is_authenticated():
                record.user_id = user.id
            else:
                record.user_id = None
        else:
            record.user_id = 'NoRequestContext'
            record.session_key = None
            record.request_uuid = None
        super().emit(record)
