from modeltranslation.admin import TranslationAdmin

from django import forms
from django.contrib import admin

from apps.catalogue.admin.views import view_product_index
from apps.catalogue.models.category import Category
from apps.catalogue.models.product import Product
from apps.catalogue.models.color import Color
from apps.catalogue.models.technology import Technology
from apps.partner.models import StockRecord


class StockRecordForm(forms.ModelForm):
    class Meta:
        model = StockRecord
        exclude = []

    def add_error(self, field, error):
        if hasattr(error, 'error_dict'):
            err_dict = error.error_dict
            for field_err, error_list in err_dict.items():
                if field_err not in self.fields:
                    if '__all__' not in err_dict:
                        err_dict['__all__'] = []
                    err_dict['__all__'].append(error_list)
                    err_dict.pop(field_err, None)
            error = forms.ValidationError(err_dict)

        super(StockRecordForm, self).add_error(field, error)


class StockRecordInline(admin.TabularInline):
    model = StockRecord
    form = StockRecordForm
    show_change_link = True
    fields = ['partner', 'max_x_mm', 'max_y_mm', 'max_z_mm', 'partner_sku', ]
    readonly_fields = [
        'partner', 'max_x_mm', 'max_y_mm', 'max_z_mm', 'partner_sku', ]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        exclude = []


@admin.register(Category)
class CategoryAdmin(TranslationAdmin):
    pass


@admin.register(Product)
class ProductAdmin(TranslationAdmin):
    form = ProductForm
    fieldsets = (
        (None, {
            'fields':  (
                (
                    'title_en',
                    'title_de'
                ),
                'slug',
                'site',
                'category'
            )
        }),
        ('Basic Properties', {
            'classes': ('collapse',),
            'fields':  (
                'is_multicolor',
                'technology',
                'tradename',
                'is_colorable',
                'internal_comment',
                'popularity',
                'material_form',
                ('attr_wall_min', 'attr_wall_opt'),
                ('attr_gap_min', 'attr_gap_opt'),
                'score_detail'),
        }),
        ('Material Properties', {
            'classes': ('collapse',),
            'fields':  (
                'is_acid_resistant',
                'is_corrosion_resistant',
                'is_toxicity_resistant',
                'is_uv_resistant',
                'is_bio_compatible',
                (
                    'attr_ultimate_tensile_strength_min',
                    'attr_ultimate_tensile_strength_max'
                ),
                (
                    'attr_modulus_min',
                    'attr_modulus_max'
                ),
                (
                    'attr_elongation_min',
                    'attr_elongation_max'
                ),
                (
                    'attr_flexural_strength_min',
                    'attr_flexural_strength_max'
                ),
                (
                    'attr_flexural_modulus_min',
                    'attr_flexural_modulus_max'
                ),
                (
                    'attr_hardness_min',
                    'attr_hardness_max'
                ),
                'attr_hardness_unit',
                (
                    'attr_density_min',
                    'attr_density_max'
                ),
                (
                    'attr_yield_strength_min',
                    'attr_yield_strength_max'
                ),
            ),
        })
    )
    inlines = [StockRecordInline]
    prepopulated_fields = {"slug": ("title",)}

    def changelist_view(self, request, extra_context=None):
        # this is very bad, but how then render
        super_context = super(
            ProductAdmin, self).changelist_view(request).context_data
        return view_product_index(request, super_context)

    def get_queryset(self, request):
        return Product.all_objects.all()

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Color)
class ColorAdmin(TranslationAdmin):
    pass


@admin.register(Technology)
class TechnologyAdmin(TranslationAdmin):
    pass
