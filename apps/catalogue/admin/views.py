from django.shortcuts import render
from apps.catalogue.models.product import Product
from apps.catalogue.models.category import Category
from apps.partner.models import StockRecord, Partner
import json


def view_product_index(request, extra_context):
    partners = list(Partner.objects.all())
    partners_dict = {}
    for partner in partners:
        partners_dict[partner.id] = partner
    stockrecords = list(StockRecord.objects.all())

    def get_partners_by_product_id(product_id):
        for sr in stockrecords:
            if sr.product_id == product_id and sr.partner_id in partners_dict:
                yield partners_dict[sr.partner_id]

    def get_stockrecord_amount_by_product_id(product_id):
        amount = 0
        for sr in stockrecords:
            if sr.product_id == product_id:
                amount += 1
        return amount

    categories = list(Category.objects.all())
    categories_dict = {}
    for category in categories:
        categories_dict[category.id] = category

    products_list = [{
        'id': p.id,
        'title': p.title,
        'technology': p.technology.title if p.technology else "",
        'partners': ', '.join(
            ['"' + t.name + '"'
             for t in get_partners_by_product_id(p.id)]),
        'categories_id': p.category,
        'stockrecord_amount': get_stockrecord_amount_by_product_id(p.id),
        'attr_usp_class_vi_certified': p.attr_usp_class_vi_certified
    } for p in Product.all_objects.all()]
    categories_list = [{"id": c.id, "name": c.name} for c in categories]
    context = {
        'products_json': json.dumps(products_list),
        'categories_json': json.dumps(categories_list)}
    extra_context.update(context)
    return render(
        request,
        'admin/catalogue/product/index.html',
        context=extra_context,
    )
