import json

from apps.b3_address.factories import CountryFactory
from apps.b3_tests.factories import ProductFactory, ColorFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class CatalogueDownloadTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        CountryFactory()
        ProductFactory()
        ColorFactory()

    def test_download_catalogue_no_permission(self):
        self.user.is_staff = False
        self.user.save()
        response = self.client.get('/api/v2.0/system/catalogue/')
        self.assertEqual(response.status_code, 403)

    def test_download_catalogue(self):
        self.user.is_staff = True
        self.user.save()
        response = self.client.get('/api/v2.0/system/catalogue/')
        self.assertEqual(response.status_code, 200)
        response_json = response.content.decode()
        catalogue = json.loads(response_json)
        self.assertEqual(len(catalogue), 6)
