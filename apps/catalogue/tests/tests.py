from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import ProductFactory, SiteFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.catalogue.models.product import Product


class SiteTest(TestCase):
    def setUp(self):
        super(SiteTest, self).setUp()
        self.product = ProductFactory(site=self.site)
        self.site2 = SiteFactory(domain="org2.my.3yd")

    def test_product_accessible(self):
        set_current_site(self.site)
        self.assertIn(self.product, Product.objects.all())

        set_current_site(self.site2)
        self.assertNotIn(self.product, Product.objects.all())

        set_current_site(None)
        self.assertNotIn(self.product, Product.objects.all())
