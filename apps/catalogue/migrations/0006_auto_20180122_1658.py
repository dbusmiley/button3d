# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-01-22 16:58


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0005_auto_20180117_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='color',
            name='title',
            field=models.CharField(max_length=128, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='color',
            name='title_de',
            field=models.CharField(max_length=128, null=True, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='color',
            name='title_en',
            field=models.CharField(max_length=128, null=True, verbose_name='Title'),
        ),
    ]
