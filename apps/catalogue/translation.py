from modeltranslation.translator import translator, TranslationOptions
from apps.catalogue.models.category import Category
from apps.catalogue.models.product import Product
from apps.catalogue.models.color import Color
from apps.catalogue.models.technology import Technology


class CategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)


class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'tradename')


class ColorTranslationOptions(TranslationOptions):
    fields = ('title',)


class TechnologyTranslationOptions(TranslationOptions):
    fields = ('title',)


translator.register(Category, CategoryTranslationOptions)
translator.register(Product, ProductTranslationOptions)
translator.register(Color, ColorTranslationOptions)
translator.register(Technology, TechnologyTranslationOptions)

# Temporary fix for a bug in modeltranslation. See
# https://github.com/deschler/django-modeltranslation/issues/455
# Should be removed as soon as bug is resolved
Category._meta.base_manager_name = None
Product._meta.base_manager_name = None
Color._meta.base_manager_name = None
Technology._meta.base_manager_name = None
