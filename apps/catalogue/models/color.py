from django.core.validators import RegexValidator
from django.db import models


class Color(models.Model):
    title = models.CharField('Title', max_length=128, unique=False)
    rgb = models.CharField('RGB', max_length=7, unique=False, validators=[
        RegexValidator(
            regex='^#(?:[0-9a-fA-F]{6})$',
            message='Wrong format. Sample: #094fed',
        ),
    ])

    def __str__(self):
        return "<'%s' '%s'>" % (self.rgb, self.title)
