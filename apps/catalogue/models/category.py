from django.db import models
from treebeard.mp_tree import MP_Node


class Category(MP_Node):
    """
    A product category. Merely used for navigational purposes; has no
    effects on business logic.

    Uses django-treebeard.
    """
    name = models.CharField('Name', max_length=255, db_index=True)
    description = models.TextField('Description', blank=True)

    def __str__(self):
        return self.full_name

    @property
    def full_name(self) -> str:
        """
        Returns a string representation of the category and it's ancestors,
        e.g. 'Books > Non-fiction > Essential programming'.
        """
        names = [category.name for category in self.get_ancestors_and_self()]
        return ' > '.join(names)

    def get_ancestors_and_self(self):
        return list(self.get_ancestors()) + [self]
