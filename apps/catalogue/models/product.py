from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Q
from django.utils.text import slugify

from apps.b3_organization.models import OrganizationProductOption, \
    get_current_site, OrganizationOptionModel
from apps.catalogue.models.category import Category
from apps.catalogue.models.technology import Technology


class ProductManager(models.Manager):
    def get_queryset(self):
        qs = super(ProductManager, self).\
            get_queryset().select_related().prefetch_related('stockrecords')
        site = get_current_site()
        return qs.filter(Q(site=site) | Q(site__isnull=True))


class Product(models.Model, OrganizationOptionModel):
    """
    The 3D Printing Material
    For historical reasons called "Product"
    """

    title = models.CharField(max_length=255, blank=True)
    slug = models.SlugField('Slug', max_length=255, unique=False)
    description = models.TextField('Description', blank=True)
    date_created = models.DateTimeField('Date created', auto_now_add=True)

    # This field is used by Haystack to reindex search
    date_updated = models.DateTimeField(
        'Date updated', auto_now=True, db_index=True
    )

    category = models.ForeignKey(
        Category,
        verbose_name='Category',
        on_delete=models.PROTECT,
        null=True
    )

    site = models.ForeignKey(Site, blank=True, null=True, default=None)
    is_multicolor = models.BooleanField('Is multicolor', default=False)
    technology = models.ForeignKey(
        Technology, verbose_name='Technology', blank=True, null=True)
    tradename = models.CharField(
        'Tradename', max_length=100, null=True, blank=True)
    is_colorable = models.BooleanField('Is colorable', default=True)
    internal_comment = models.TextField(
        'Internal 3yd comment', blank=True, null=True)
    popularity = models.PositiveIntegerField(
        'Popularity', blank=True, null=True)
    material_form = models.CharField('Material Form', max_length=30,
                                     choices=(('powder', 'Powder'),
                                              ('liquid', 'Liquid'),
                                              ('filament', 'Filament'),
                                              ('other', 'Other')),
                                     blank=True, null=True)

    score_detail = models.PositiveIntegerField(
        'A integer between 1 and 100 describing the detail richness.',
        blank=True,
        null=True
    )

    attr_strength = models.PositiveIntegerField(
        'Deprecated. score_stregth will be calculated from attributes.',
        blank=True,
        null=True,
        choices=((0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)))

    attr_detail = models.PositiveIntegerField(
        'Deprecated. Use score_detail instead.',
        blank=True,
        null=True,
        choices=((0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5)))
    attr_ultimate_tensile_strength_min = models.FloatField(
        'Ultimate tensile strength min (MPa)', blank=True, null=True)
    attr_ultimate_tensile_strength_max = models.FloatField(
        'Ultimate tensile strength max (MPa)', blank=True, null=True)
    attr_modulus_min = models.FloatField(
        'Tensile modulus min (MPa)', blank=True, null=True)
    attr_modulus_max = models.FloatField(
        'Tensile modulus max (MPa)', blank=True, null=True)
    attr_elongation_min = models.FloatField(
        'Elongation at break min (%)', blank=True, null=True)
    attr_elongation_max = models.FloatField(
        'Elongation at break max (%)', blank=True, null=True)
    attr_flexural_strength_min = models.FloatField(
        'Flexural strength min (MPa)', blank=True, null=True)
    attr_flexural_strength_max = models.FloatField(
        'Flexural strength max (MPa)', blank=True, null=True)
    attr_flexural_modulus_min = models.FloatField(
        'Flexural modulus min (MPa)', blank=True, null=True)
    attr_flexural_modulus_max = models.FloatField(
        'Flexural modulus max (MPa)', blank=True, null=True)
    attr_hardness_shore_scale = models.FloatField(
        'Hardness shore scale', blank=True, null=True)
    attr_hardness_min = models.FloatField(
        'Hardness min', blank=True, null=True)
    attr_hardness_max = models.FloatField(
        'Hardness max', blank=True, null=True)
    attr_hardness_unit = models.CharField(
        'Hardness unit', blank=True, null=True, max_length=100,
        choices=(('Rockwell B', 'Rockwell B'),
                 ('Rockwell C', 'Rockwell C'),
                 ('Vickers', 'Vickers'),
                 ('Shore A', 'Shore A'),
                 ('Shore D', 'Shore D'),
                 ('Brinell', 'Brinell')))

    attr_hdt_min = models.FloatField(
        'HDT min (C)', blank=True, null=True)
    attr_hdt_max = models.FloatField(
        'HDT max (C)', blank=True, null=True)
    attr_glass_transition_min = models.FloatField(
        'Glass Transition Temp min (Tg) (Celsius)', blank=True, null=True)
    attr_glass_transition_max = models.FloatField(
        'Glass Transition Temp max (Tg) (Celsius)', blank=True, null=True)
    attr_part_density = models.FloatField(
        'Part density (g/cm3)', blank=True, null=True)
    attr_flammability = models.FloatField(
        'Flammability', blank=True, null=True)
    attr_usp_class_vi_certified = models.NullBooleanField(
        'USP Class VI Certified', blank=True, null=True)
    attr_density_min = models.FloatField(
        'Density min (g/cm3)', blank=True, null=True)
    attr_density_max = models.FloatField(
        'Density max (g/cm3)', blank=True, null=True)
    attr_ultimate_tensile_strength_x_direction_min = models.FloatField(
        'Ultimate tensile strength x direction min (MPa)',
        blank=True, null=True)
    attr_ultimate_tensile_strength_x_direction_max = models.FloatField(
        'Ultimate tensile strength x direction max (MPa)',
        blank=True, null=True)
    attr_ultimate_tensile_strength_z_direction_min = models.FloatField(
        'Ultimate tensile strength z direction min (MPa)',
        blank=True, null=True)
    attr_ultimate_tensile_strength_z_direction_max = models.FloatField(
        'Ultimate tensile strength z direction max (MPa)',
        blank=True, null=True)
    attr_strain_break_x_direction_min = models.FloatField(
        'Strain at break x direction min (%)', blank=True, null=True)
    attr_strain_break_x_direction_max = models.FloatField(
        'Strain at break x direction max (%)', blank=True, null=True)
    attr_strain_break_z_direction_min = models.FloatField(
        'Strain at break z direction min (%)', blank=True, null=True)
    attr_strain_break_z_direction_max = models.FloatField(
        'Strain at break z direction max (%)', blank=True, null=True)
    attr_izod_impact_strength_min = models.FloatField(
        'Izod notched impact strength min (J/m)', blank=True, null=True)
    attr_izod_impact_strength_max = models.FloatField(
        'Izod notched impact strength max (J/m)', blank=True, null=True)
    attr_charpy_impact_strength_min = models.FloatField(
        'Charpy notched impact strength min (kJ/m2)', blank=True, null=True)
    attr_charpy_impact_strength_max = models.FloatField(
        'Charpy notched impact strength max (kJ/m2)', blank=True, null=True)
    attr_charpy_v_impact_strength_min = models.FloatField(
        'Charpy V notched impact strength min (J)', blank=True, null=True)
    attr_charpy_v_impact_strength_max = models.FloatField(
        'Charpy V notched impact strength max (J)', blank=True, null=True)
    attr_service_temperature_max = models.FloatField(
        'Service temperature max (Celsius)', blank=True, null=True)

    attr_wall_min = models.FloatField(
        'Minimum wall thickness (critical)', blank=False, null=True)
    attr_wall_opt = models.FloatField(
        'Minimum wall thickness (optimal)', blank=False, null=True)
    attr_gap_min = models.FloatField(
        'Minimum gap (critical)', blank=True, null=True)
    attr_gap_opt = models.FloatField(
        'Minimum gap (optimal)', blank=True, null=True)

    attr_melting_point_min = models.FloatField(
        'Melting Point min (Celsius)', blank=True, null=True)
    attr_melting_point_max = models.FloatField(
        'Melting Point max (Celsius)', blank=True, null=True)
    attr_thermal_conductivity = models.FloatField(
        'Thermal conductivity (Celsius)', blank=True, null=True)
    attr_electric_conductivity = models.FloatField(
        'Electric conductivity (Celsius)', blank=True, null=True)
    attr_coefficient_thermal_expansion = models.FloatField(
        'Coefficient of thermal expansion (K^-1)', blank=True, null=True)
    attr_tensile_strength_annealed = models.FloatField(
        'Tensile strength (annealed) (MPa)', blank=True, null=True)
    attr_elongation_annealed = models.FloatField(
        'Elongation (annealed) (%)', blank=True, null=True)
    attr_tensile_strength_wire_annealed = models.FloatField(
        'Tensile strength (wire, annealed) (MPa)', blank=True, null=True)
    attr_elongation_wire_annealed = models.FloatField(
        'Elongation (wire, annealed) (%)', blank=True, null=True)
    attr_hardness_wire_annealed = models.FloatField(
        'Hardness (wire, annealed) (HV)', blank=True, null=True)
    attr_tensile_strength_wire_half_hard = models.FloatField(
        'Tensile strength (wire, 1/2 hard) (MPa)', blank=True, null=True)
    attr_hardness_wire_half_hard = models.FloatField(
        'Hardness (wire, 1/2 hard) (HV)', blank=True, null=True)
    attr_tensile_strength_wire_fullhard = models.FloatField(
        'Tensile strength (wire, full hard) (MPa)', blank=True, null=True)
    attr_hardness_wire_fullhard = models.FloatField(
        'Hardness (wire, full hard) (HV)', blank=True, null=True)
    attr_tensile_strength_wire_springhard = models.FloatField(
        'Tensile strength (wire, spring hard) (%)', blank=True, null=True)
    attr_hardness_wire_springhard = models.FloatField(
        'Hardness (wire, spring hard) (HV)', blank=True, null=True)
    attr_yield_strength_max = models.FloatField(
        'Yield Strength max (MPa)', blank=True, null=True)
    attr_yield_strength_min = models.FloatField(
        'Yield Strength min (MPa)', blank=True, null=True)

    is_acid_resistant = models.BooleanField(
        'Acid resistant', default=False)
    is_corrosion_resistant = models.BooleanField(
        'Corrosion resistant', default=False)
    is_toxicity_resistant = models.BooleanField(
        'Toxicity resistant', default=False)
    is_uv_resistant = models.BooleanField(
        'UV resistant', default=False)
    is_bio_compatible = models.BooleanField(
        'Bio compatible', default=False)

    # Organization options
    org_option_class = OrganizationProductOption

    objects = ProductManager()
    all_objects = models.Manager()

    def __str__(self):
        if self.title:
            return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.get_title())
        super(Product, self).save(*args, **kwargs)

    @property
    def has_stockrecords(self):
        """
        Test if this product has any stockrecords
        """
        return self.stockrecords.exists()

    @property
    def num_stockrecords(self):
        return self.stockrecords.count()

    @property
    def full_product_title(self):
        """
        Returns something like "Polyamide Natural White"
        :return:
        """
        return self.org_options.title

    @property
    def score_strength(self):
        max_value = 2050.00
        ts_max = self.org_options.attr_ultimate_tensile_strength_max
        ts_min = self.org_options.attr_ultimate_tensile_strength_min

        return self._log_score(ts_min, ts_max, max_value)

    @property
    def score_density(self):
        max_value = 21.45
        ds_max = self.org_options.attr_density_max
        ds_min = self.org_options.attr_density_min

        return self._log_score(ds_min, ds_max, max_value)

    def _log_score(self, min, max, max_value):
        """
        Returns a nice logarithmized value between 1 and 100
        """
        if not max or max < 0 or not min or min < 0:
            # invalid
            return 1

        average = (min + max) / 2.0

        if average > max_value:
            return 100

        if average == 0:
            return 1

        score = (average / max_value)  # between 0 and 1
        # still between 0 and 1 but logarithmized
        score **= (1.0 / 3.0)
        score = int(round(score * 100))  # between 0 and 100

        return score
