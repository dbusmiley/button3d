import json

from django.core import serializers

from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.b3_address.models import Country
from apps.catalogue.models.category import Category
from apps.catalogue.models.color import Color
from apps.catalogue.models.product import Product
from apps.catalogue.models.technology import Technology


class CatalogueDownloadView(APIView):

    permission_classes = (IsAdminUser, )

    def get(self, request, *args, **kwargs):
        objects = []
        objects.extend(Country.objects.all())
        objects.extend(Category.objects.all())
        objects.extend(Color.objects.all())
        objects.extend(Technology.objects.all())
        objects.extend(Product.objects.all())

        # Use non DRF serializer to serialize to a loaddata-compatible format
        serialized_objects = serializers.serialize(
            'json',
            objects,
            indent=4,
        )

        # Parse JSON and use DRF Response make the response consistant to the
        # rest of the API

        unserialized_objects = json.loads(serialized_objects)

        return Response(unserialized_objects)
