import os
import re

from django.conf import settings
from django.core.urlresolvers import reverse, NoReverseMatch
from django.template import Library
from django.utils.translation import ugettext as _

register = Library()

supported_languages = tuple(lang_code for lang_code, _ in settings.LANGUAGES)
regexp_substring = '|'.join(supported_languages)
localized_url_regexp = r'^/(?P<lang>{0})(?P<url>/.*)$'.format(regexp_substring)
localized_url_re = re.compile(localized_url_regexp)


@register.simple_tag(takes_context=True)
def change_lang(context, lang=None):
    """
    Get active page's url by a specified language. For example, if we are here:
    /en/url/path/, change_lang('de') will return /de/url/path/

    Usage: {% change_lang 'en' %}

    {% change_lang 'x-default' %} or {% change_lang %} mean url for current
    language
    """

    path = context['request'].path

    match = localized_url_re.match(path)

    # if url has language prefix in it. If URL part is not language-prefixed,
    # for example: /api/v2/url/path/, do nothing
    if match:
        url_groups = match.groupdict()

        if lang in supported_languages:
            # strip only existing language codes
            path = '/{0}{1}'.format(lang, url_groups['url'])
        elif lang is None or lang == 'x-default':
            # strip language prefix to get language-agnostic URL:
            # /en/url/path/ to /url/path/
            path = url_groups['url']

    return path


@register.filter
def filesize(value):
    """Returns the filesize of the filename given in value"""
    return os.path.getsize(settings.MEDIA_ROOT + value)


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname)
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''


@register.filter("inline_truncate")
def inline_truncate(value, size):
    """Truncates a string to the given size
        placing the ellipsis at the middle of the string"""
    if len(value) > size > 3:
        start = (size - 3) // 2
        end = (size - 3) - start
        return value[0:start] + '...' + value[-end:]
    else:
        return value[0:size]


@register.filter("delivery_time")
def delivery_time(basket, method_code):
    return basket.delivery_time_all_lines(basket.shipping_method(method_code))


@register.filter("get_orderable_project_line_at_index")
def get_orderable_project_line_at_index(project_lines, position):
    count = 0
    for line in project_lines:
        if line.is_orderable():
            if count == position:
                return line
            else:
                count += 1
    raise Exception("Could not find orderable line at the indicated position")


@register.filter("get_project_line_from_basket_line")
def get_project_line_from_basket_line(project_lines, basket_line):
    for line in project_lines:
        if line.stockrecord == basket_line.stockrecord and \
                line.product == basket_line.product:
            return line
    return None


@register.filter_function
def order_by(queryset, args):
    args = [x.strip() for x in args.split(',')]
    return queryset.order_by(*args)


@register.filter('manipulate_color')
def color_variant(hex_color, brightness_offset=1):
    """ takes a color like #87c95f and produces
        a lighter or darker variant """
    if len(hex_color) != 7:
        raise Exception(
            ("Passed %s into color_variant(), "
             "needs to be in #87c95f format.") % hex_color)
    rgb_hex = [hex_color[x:x+2] for x in [1, 3, 5]]
    new_rgb_int = [int(hex_value, 16) + brightness_offset
                   for hex_value in rgb_hex]
    # Make sure new values are between 0 and 255
    new_rgb_int = [min([255, max([0, i])]) for i in new_rgb_int]

    # hex() produces "0x88", we want just "88"
    return "#" + "".join([hex(i)[2:] for i in new_rgb_int])


@register.filter('brightness_color')
def brightness_color(hex_color):
    if len(hex_color) != 7:
        raise Exception(
            ("Passed %s into brightness_color(), "
             "needs to be in #87c95f format.") % hex_color)
    rgb_red = int(hex_color[1:3], 16)
    rgb_green = int(hex_color[3:5], 16)
    rgb_blue = int(hex_color[5:7], 16)
    brightness = (rgb_red / 255.0) * 0.3 + \
        (rgb_green / 255.0) * 0.59 + \
        (rgb_blue / 255.0) * 0.11
    return brightness


@register.filter(name='get_range')
def get_range(number):
    return list(range(number))


# Some strings are not translated because they are stored in DB
# To make gettext recognize them, they are tagged here
_("Printing")
_("Shipped")
_("Cancelled")
_("Pending")
