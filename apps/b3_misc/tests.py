from django.core.management import call_command
from django.test import TestCase, RequestFactory

from apps.b3_misc.templatetags.b3_utils import change_lang


class TestChangeLangTemplateTag(TestCase):
    def test_change_lang_tag(self):
        request_factory = RequestFactory()

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/api/v2/url/path/')}, 'de'),
            '/api/v2/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/api/v2/url/path/')}),
            '/api/v2/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/en/url/path/')}, 'de'),
            '/de/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/en/url/path/')}, 'es'),
            '/en/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/en/url/path/')}, 'en'),
            '/en/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/en/url/path/')}),
            '/url/path/')

        self.assertEqual(
            change_lang(
                {'request': request_factory.get('/en/url/path/')},
                'x-default'),
            '/url/path/')


class TestLoadSampleData():
    def test_load_sample_data(self):
        call_command(  # nosec
            'load_sample_data',
            multi_domain='multi.my.3yd',
            single_domain='single.my.3yd',
            mail='test+admin@3yd.de',
            password='3yourminD'
        )


class TestBootstrap():
    def test_bootstrap(self):
        call_command(  # nosec
            'bootstrap',
            domain='multi.my.3yd',
            mail='test+admin@3yd.de',
            admin_user='adminadmin',
            admin_password='3yourminD'
        )
