import getpass

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.management import BaseCommand
from django.db import transaction

import button3d.type_declarations as td
from apps.b3_migration.factories.switch_factory import SwitchFactory
from apps.b3_migration.models import Switch
from apps.b3_misc.management.commands.load_materials import load_materials
from apps.b3_misc.management.commands.utils import query
from apps.b3_organization.models import UserProfile
from apps.b3_tests.factories import SiteFactory, OrganizationFactory


def create_admin_user(
    username: str,
    mail: str,
    password: str,
    site: td.Site
) -> td.User:

    try:
        User.objects.get(
            username=username
        )
    except User.DoesNotExist:
        admin = User.objects.create_superuser(
            username=username,
            password=password,
            email=mail
        )

        UserProfile.objects.create(
            user=admin,
            site=site
        )

        print(f'Admin {username} created.')

        return admin


def create_site_and_org(
    domain: str
) -> td.Site:
    site = SiteFactory(
        domain=domain,
        name=domain
    )

    OrganizationFactory(site=site)
    for feature in Switch.FEATURE_CHOICES:
        SwitchFactory(
            feature=feature[0],
            organization=site.organization,
            active=True,
        )

    return site


def bootstrap(
    domain: str,
    admin_username: str,
    admin_mail: str,
    admin_password: str,
    once: bool
) -> td.Site:
    if once and Site.objects.filter(domain=domain):
        print('This domain already exists.'
              'Use another domain or omit --once to overwrite.'
              'Nothing was done.')
        return Site.objects.filter(domain=domain)

    # Make sure that example.com is deleted. (example.com is automatically
    # created by the django Sites framework. We don't need that.)
    try:
        example = Site.objects.get(domain='example.com')
        example.delete()
    except Site.DoesNotExist:
        pass

    site = create_site_and_org(domain)

    create_admin_user(admin_username, admin_mail, admin_password, site)

    return site


class Command(BaseCommand):

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '-d',
            '--domain',
            action='store',
            dest='domain',
            default=False,
            help='Domain (Host) without Port'
        )

        parser.add_argument(
            '-u',
            '--username',
            action='store',
            dest='username',
            default=False,
            help='Admin username'
        )

        parser.add_argument(
            '-p',
            '--password',
            action='store',
            dest='password',
            default=False,
            help='Admin password'
        )

        parser.add_argument(
            '-m',
            '--mail',
            action='store',
            dest='mail',
            default=False,
            help='Admin email address'
        )

        parser.add_argument(
            '--once',
            action='store_true',
            help='Don\'t overwrite existing data'
        )

    def handle(self, *args, **options):
        with transaction.atomic():
            load_materials()
        domain = options['domain'] or query('Domain (without port)')
        username = options['username'] or query('Admin Username')
        mail = options['mail'] or query('Admin Email')
        password = options['password'] or getpass.getpass()
        once = options['once']

        bootstrap(domain, username, mail, password, once)
