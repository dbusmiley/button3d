from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.contrib.sites.models import Site
from apps.partner.models.partner import Partner
from apps.b3_organization.models.organization import UserProfile


class Command(BaseCommand):

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            'site',
            nargs=1,
            action='store',
            default=False,
            help='site, to which the users should be added'
        )

    def handle(self, *args, **options):
        site_domain = options["site"][0]
        site = Site.objects.get(domain=site_domain)

        # Get User profiles first

        if UserProfile.all_objects.filter(
                user__email="test+admin@3yd.de",
                user__username="test_admin",
                site=site
        ).count() == 0:
            admin = User.objects.create_superuser(  # nosec
                email="test+admin@3yd.de",
                username="test_admin",
                password="3yourminD"
            )
            admin_profile = UserProfile.all_objects.create(
                user=admin,
                site=site
            )
        else:
            admin_profile = UserProfile.all_objects.get(
                user__username="test_admin",
                user__email="test+admin@3yd.de",
                site=site
            )
            admin = admin_profile.user

        if UserProfile.all_objects.filter(
                user__email="test+user@3yd.de",
                user__username="test_user",
                site=site
        ).count() == 0:
            user = User.objects.create_user(  # nosec
                email="test+user@3yd.de",
                username="test_user",
                password="3yourminD"
            )
            user_profile = UserProfile.all_objects.create(
                user=user,
                site=site
            )
        else:
            user_profile = UserProfile.all_objects.get(
                user__email="test+user@3yd.de",
                user__username="test_user",
                site=site)
            user = user_profile.user

        if UserProfile.all_objects.filter(
                user__email="test+service@3yd.de",
                user__username="test_service",
                site=site
        ).count() == 0:
            service = User.objects.create_user(  # nosec
                email="test+service@3yd.de",
                username="test_service",
                password="3yourminD"
            )
            service_profile = UserProfile.all_objects.create(
                user=service,
                site=site
            )
        else:
            service_profile = UserProfile.all_objects.get(
                user__email="test+service@3yd.de",
                user__username="test_service",
                site=site
            )
            service = service_profile.user

        for partner in Partner.objects.all():
            if service not in partner.users.all():
                partner.users.add(service)
                try:
                    partner.save()
                except ValueError:
                    print(("Couldn't add User to {}"
                           .format(partner.name.encode('utf-8'))))
            else:
                print(("User already Added to {}"
                       .format(partner.name.encode('utf-8'))))
