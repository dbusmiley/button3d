def query(label, default=''):
    data = input(  # nosec
        f"{label} (current: {default.encode('utf-8')}): "
    ).strip()
    if not data:
        return default
    return data
