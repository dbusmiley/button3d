import os

from django.conf import settings
from django.core.management import BaseCommand, call_command


MATERIALS_JSON = os.path.join(settings.BASE_DIR, "materials.json")


def load_materials() -> None:
    call_command("loaddata", MATERIALS_JSON)


class Command(BaseCommand):
    def handle(self, *args, **options):
        load_materials()
