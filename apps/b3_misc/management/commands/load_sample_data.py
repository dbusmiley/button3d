import getpass
import os
import random
import re

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction
from faker import Factory

from apps.b3_address.models import Country
from apps.b3_checkout.constants import PAYMENT_METHODS, PAYMENT_METHOD_CHOICES
from apps.b3_checkout.factories import PartnerPaymentMethodFactory
from apps.b3_checkout.models import SupportedPaymentMethod, \
    PartnerPaymentMethod
from apps.b3_misc.management.commands.bootstrap import bootstrap
from apps.b3_misc.management.commands.load_materials import load_materials
from apps.b3_misc.management.commands.utils import query
from apps.b3_organization.models import UserProfile, Site
from apps.b3_shipping.factories.pickup_location import PickupLocationFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_signup.models import Signup
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    KeyManagerFactory
from apps.partner.models import Partner, Product

faker = Factory.create()


class LoadSampleDataException(Exception):
    pass


class Command(BaseCommand):
    def __init__(self, stdout=None, stderr=None, no_color=False):
        super().__init__(stdout, stderr, no_color)
        self.product_ids = []

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '-s',
            '--single-domain',
            action='store',
            dest='single_domain',
            default=False,
            help='Domain for the single site'
        )

        parser.add_argument(
            '-M',
            '--multi-domain',
            action='store',
            dest='multi_domain',
            default=False,
            help='Domain for the multi site'
        )

        parser.add_argument(
            '-p',
            '--password',
            action='store',
            dest='password',
            default=False,
            help='Admin password'
        )

        parser.add_argument(
            '-m',
            '--mail',
            action='store',
            dest='mail',
            default=False,
            help='Admin email address'
        )

        parser.add_argument(
            '--once',
            action='store_true',
            help='Don\'t overwrite existing data'
        )

    def handle(self, *args, **options):
        single_domain = options['single_domain'] or query(
            'The domain for the Sample Organization called "single"'
            '(without port)', 'single.my.3yd'
        )
        multi_domain = options['multi_domain'] or query(
            'The domain for the Sample Organization called "multi"'
            '(without port)', 'multi.my.3yd'
        )
        mail = options['mail'] or query('Admin Email')
        password = options['password'] or getpass.getpass()
        once = options['once']

        if once and User.objects.filter(is_superuser=True):
            print(f'We already have admins. Aborting.')
            return

        with transaction.atomic():
            load_materials()
        with transaction.atomic():
            self.single_site, self.multi_site = self._create_sites(
                single_domain,
                multi_domain,
                mail,
                password,
                once
            )

            single_users = [
                'test+singleuser@3yd.de', 'test+singleservice@3yd.de'
            ]
            multi_users = [
                'test+multiuser@3yd.de', 'test+multiservice@3yd.de'
            ]

            self._create_users_for_site(single_users, self.single_site)
            self._create_users_for_site(multi_users, self.multi_site)
            self.product_ids = self._get_product_ids()
            self._create_partners(
                sites_qs=Site.objects.all(),
                user_qs=User.objects.filter(email__icontains='service')
            )
            return self.stdout.write(
                self.style.SUCCESS(f'All data added successfully')
            )

    def _create_sites(self, single_domain, multi_domain, mail, password, once):

        single_site = bootstrap(
            domain=single_domain,
            admin_username='single_admin',
            admin_mail=mail,
            admin_password=password,
            once=once
        )
        single_site.organization.key_manager = KeyManagerFactory()
        single_site.organization.save()
        multi_site = bootstrap(
            domain=multi_domain,
            admin_username='multi_admin',
            admin_mail=mail,
            admin_password=password,
            once=once
        )
        multi_site.organization.key_manager = KeyManagerFactory()
        multi_site.organization.save()
        return single_site, multi_site

    def _create_users_for_site(self, users, site):
        for user in users:
            split = re.split(r'[\W]', user)
            first_name = split[0]
            last_name = split[1]
            user_obj = User.objects.create_user(  # nosec
                username=Signup.generate_unique_username(user),
                email=user,
                password='3yourminD',
                first_name=first_name,
                last_name=last_name,
            )

            if 'service' in user:
                site.organization.organization_panel_admins.add(user_obj)
                print(
                    f'{user} is a service user, added to {site.name}'
                )

            UserProfile.objects.create(
                user=user_obj,
                site=site
            )

    def _create_partners(self, sites_qs, user_qs, qty=4) -> None:
        partners_created = []
        for site in sites_qs:
            for number_of_partners in range(qty):
                partner = self._populate_partners(site=site, users=user_qs)
                partner.logo_temp = None
                partner.save()
                partners_created.append(partner)
                self.stdout.write(
                    self.style.SUCCESS(
                        f'Partner: {partner} was populated successfully!'
                    ))

        if len(partners_created) == qty * len(sites_qs):
            self.stdout.write(
                self.style.SUCCESS(
                    f'Partners added successfully!'
                ))
            return None
        else:
            raise LoadSampleDataException('Error creating partners')

    def _populate_partners(self, site, users):
        os.environ['SITE_DOMAIN'] = site.domain
        partner = PartnerFactory(
            name=faker.company(),
            site=site
        )

        multi_users = users.filter(email__contains='multi')
        single_users = users.filter(email__contains='single')

        if site.domain == str(self.multi_site.domain):
            for user in multi_users:
                Partner.objects.get(name=partner.name).users.add(user)

        elif site.domain == str(self.single_site.domain):
            for user in single_users:
                Partner.objects.get(name=partner.name).users.add(user)

        site.organization.partners_enabled.add(partner)

        self._create_custom_payment_method(partner=partner)
        self._create_shipping_method(partner=partner)
        self._create_pickup_location(partner=partner)
        self._enable_payments(partner=partner)
        self._create_stock_records(partner=partner)
        return partner

    def _create_custom_payment_method(self, partner):
        PartnerPaymentMethodFactory(
            partner=partner,
            type_name=PAYMENT_METHODS.CUSTOM,
            public_config_arguments={'name': 'Internal Payment'},
            is_enabled=True,
            is_billing_address_required=False
        )

    def _create_shipping_method(self, partner):
        ShippingMethodFactory(  # nosec
            name='DHL Cheap',
            price_excl_tax=10,
            partner=partner,
            countries=Country.objects.all(),
            shipping_days_min=random.randint(5, 8),
            shipping_days_max=random.randint(9, 12)
        )
        ShippingMethodFactory(  # nosec
            name='UPS Expensive',
            price_excl_tax=35,
            partner=partner,
            countries=Country.objects.all(),
            shipping_days_min=random.randint(2, 4),
            shipping_days_max=random.randint(4, 7)
        )
        ShippingMethodFactory(  # nosec
            name='FedEx TURBO',
            price_excl_tax=20,
            partner=partner,
            countries=Country.objects.all(),
            shipping_days_min=1,
            shipping_days_max=2
        )

    def _create_pickup_location(self, partner):
        PickupLocationFactory(
            partner=partner,
            instructions=f'Room number: {faker.random_number()}',
            location=faker.street_address()
        )

    def _create_stock_records(self, partner, **kwargs):
        for product_id in self.product_ids:
            StockRecordFactory(  # nosec
                price_custom=str(random.randint(10, 500)),
                partner=partner,
                product=Product.objects.get(id=product_id),
                min_production_days=random.randint(1, 3),
                max_production_days=random.randint(3, 6),
                **kwargs
            )

    def _get_product_ids(self):
        products_to_use = []
        products = list(Product.objects.all().values_list('id', flat=True))

        for i in range(4):
            random_index = random.randint(0, len(products) - 1)  # nosec
            choice = products[random_index]
            products_to_use.append(choice)
            products.pop(random_index)

        return products_to_use

    def _enable_payments(self, partner):
        payment_methods = PAYMENT_METHOD_CHOICES
        try:
            for payment_method in payment_methods:
                if payment_method[0] != PAYMENT_METHODS.INVOICE \
                        and payment_method[0] != PAYMENT_METHODS.CUSTOM:
                    SupportedPaymentMethod.objects.create(
                        partner=partner,
                        type_name=payment_method[0]
                    )
                    PartnerPaymentMethod.objects.create(
                        partner=partner,
                        type_name=payment_method[0]
                    )
        except Exception as e:
            raise e
        return self.stdout.write(
            self.style.SUCCESS(
                f'All payments methods enabled for {partner.name}')
        )
