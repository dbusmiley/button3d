from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from apps.b3_organization.utils import get_base_url


def error(request, error):
    context = {'base_url': get_base_url()}
    if error == "file-too-large":
        context['errormessage'] = \
            _("The 3D model exceeds the maximum file size.") + " (max. 60 MB)"
    elif error == "file-invalid":
        context['errormessage'] = _(
            ("The file is invalid or the format was not recognized."
             "You can upload any "
             "obj, dae, wrl, ply, 3ds, step, iges, stl or x3d file.")
        )
    elif error == "file-empty":
        context['errormessage'] = _("The file is missing or empty.")
    else:
        context['errormessage'] = _("Something went wrong.")

    return render(request, 'b3_misc/error.html', context)
