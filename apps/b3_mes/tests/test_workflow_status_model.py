from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory)


class WorkflowStatusTest(TestCase):
    def setUp(self):
        super().setUp()

        service = PartnerFactory()
        self.order = OrderFactory(partner=service)
        part = self.order.lines.first()
        workflow_template = WorkflowTemplateWithStatusesFactory()
        self.workflow = workflow_template.generate_workflow_and_sequences(
            part.id)

        self.workflow_status = self.workflow.statuses.get(next=None)

        for sequence in self.order.sequences.all():
            sequence.status = self.workflow_status
            sequence.save()

    def test_handle_deleting_status(self):
        """
        Assert that deleting status updates any sequences related to it
        """
        self.workflow_status.delete()

        expected_workflow_status = self.workflow.get_first_status()

        for sequence in self.order.sequences.all():
            self.assertEqual(expected_workflow_status.id, sequence.status.id)
