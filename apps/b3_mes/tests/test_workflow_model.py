from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory


class WorkflowTest(TestCase):
    def setUp(self):
        super().setUp()

        service = PartnerFactory()
        order = OrderFactory(partner=service)
        part = order.lines.first()
        workflow_template = WorkflowTemplateWithStatusesFactory()
        self.workflow = workflow_template.generate_workflow_and_sequences(
            part.id)

    def test_string_representation(self):
        self.assertEqual(
            str(self.workflow),
            f'{self.workflow.partner.name}: {self.workflow.name}',
        )
