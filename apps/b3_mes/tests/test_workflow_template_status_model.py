from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import WorkflowTemplateStatusFactory


class WorkflowTemplateStatusTest(TestCase):
    def setUp(self):
        super().setUp()
        self.status = WorkflowTemplateStatusFactory()

    def test_string_representation(self):
        self.assertEqual(
            str(self.status),
            f'{self.status.workflow.name}: {self.status.name}',
        )
