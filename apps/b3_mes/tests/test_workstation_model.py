from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import WorkstationFactory


class WorkstationTest(TestCase):
    def setUp(self):
        super().setUp()
        self.workstation = WorkstationFactory()

    def test_string_representation(self):
        self.assertEqual(str(self.workstation), f'{self.workstation.name}')
