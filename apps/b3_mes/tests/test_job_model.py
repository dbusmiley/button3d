from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory, ProductFactory
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory, WorkstationFactory, JobFactory

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class JobTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        service = PartnerFactory()
        order = OrderFactory(partner=service)
        part = order.lines.first()
        workflow_template = WorkflowTemplateWithStatusesFactory()
        workflow_template.generate_workflow_and_sequences(part.id)
        sequence = order.sequences.all().first()

        workstation = WorkstationFactory(partner=service)
        material = ProductFactory()
        workstation.materials.add(material)

        self.job = JobFactory(
            workstation=workstation,
            material=material,
            created_by=self.user
        )
        self.job.sequences.add(sequence)

    def test_string_representation(self):
        self.assertEqual(
            str(self.job),
            f'{self.job.workstation.name}: {self.job.start_time} '
            f'- {self.job.finish_time}'
        )
