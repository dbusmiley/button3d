from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory,
)
from apps.b3_tests.testcases.common_testcases import TestCase


class SequenceTest(TestCase):
    def setUp(self):
        super().setUp()

        service = PartnerFactory()
        order = OrderFactory(partner=service)
        part = order.lines.first()
        workflow_template = WorkflowTemplateWithStatusesFactory()
        workflow_template.generate_workflow_and_sequences(part.id)
        self.sequence = order.sequences.all().first()

    def test_string_representation(self):
        self.assertEqual(
            str(self.sequence),
            f'{self.sequence.part.name}: {self.sequence.id}',
        )
