from apps.b3_mes.models import Sequence
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory)


class WorkflowTemplateTest(TestCase):
    def setUp(self):
        super().setUp()
        service = PartnerFactory()
        self.order = OrderFactory(partner=service)
        self.part = self.order.lines.first()
        self.workflow_template = WorkflowTemplateWithStatusesFactory()
        self.workflow = self.workflow_template.generate_workflow_and_sequences(
            self.part.id)

    def test_string_representation(self):
        self.assertEqual(
            str(self.workflow_template),
            f'{self.workflow_template.partner.name}:'
            f' {self.workflow_template.name}',
        )

    def test_generate_workflow_and_sequences_count(self):
        self.assertCountEqual(
            [
                self.workflow_template.name,
                self.workflow_template.description,
                self.workflow_template.partner,
            ],
            [
                self.workflow.name,
                self.workflow.description,
                self.workflow.partner,
            ],
        )

    def test_generate_workflow_and_sequences_field_values(self):
        for status in self.workflow_template.statuses.all():
            self.assertEqual(
                self.workflow.statuses.filter(name=status.name).count(), 1
            )

    def test_generate_workflow_and_sequences_status_count(self):
        self.assertEqual(
            self.workflow.statuses.count(),
            self.workflow_template.statuses.count(),
        )

    def test_generate_workflow_and_sequences_sequence_count(self):
        self.assertEqual(
            self.part.quantity, Sequence.objects.filter(part=self.part).count()
        )
