from apps.b3_mes.models.workflow import Workflow
from apps.b3_mes.models.abstract_workflow import AbstractWorkflow
from apps.b3_mes.utils import get_workflow_statuses_order
import button3d.type_declarations as td


class WorkflowTemplate(AbstractWorkflow):
    """
    Workflow Template for MES
    """

    def generate_workflow_and_sequences(self, part_id: int) -> td.Workflow:
        """
        Generates an instance of :model: `Workflow` and related instances of
        :model: `WorkflowStatus` based on `self` and related instances
        of `self.statuses.all()`

        Then generates sequences. Sequences are generated here to enforce the
        order of (1) workflow create (2) status create (3) sequences create
        -- sequences are created last because sequence.status may not be null
        and is therefore auto set to the first status in the current workflow

        :return workflow: instace of :model: Workflow
        """
        WorkflowTemplate._delete_old_workflow(part_id)
        workflow = self._perform_generate_workflow(part_id)
        self._perform_generate_workflow_statuses(workflow)

        workflow.create_sequences()

        return workflow

    @staticmethod
    def _delete_old_workflow(part_id: int) -> None:
        from apps.b3_order.models import OrderLine
        part = OrderLine.objects.get(id=part_id)
        part.sequences.all().delete()
        if hasattr(part, 'workflow'):
            for status in part.workflow.statuses.all():
                status.delete()
            part.workflow.delete()

    def _perform_generate_workflow(self, part_id: int) -> td.Workflow:
        workflow = Workflow(
            name=self.name,
            description=self.description,
            partner=self.partner,
            part_id=part_id,
        )
        workflow.save()
        return workflow

    def _perform_generate_workflow_statuses(
        self, workflow: td.Workflow
    ) -> None:
        workflow_template_statuses = get_workflow_statuses_order(self)
        template_current_status = workflow_template_statuses[-1]
        next_status = None

        while template_current_status:
            current_status = template_current_status.generate_workflow_status(
                next_status, workflow
            )

            if hasattr(template_current_status, 'previous'):
                next_status = current_status
                template_current_status = template_current_status.previous
            else:
                template_current_status = None
