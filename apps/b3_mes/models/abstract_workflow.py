from django.db import models

from apps.b3_organization.models.site import AbstractSiteModel
from apps.partner.models.partner import Partner
import button3d.type_declarations as td


class AbstractWorkflow(AbstractSiteModel):
    """
    Workflow for MES Parts (Lines)
    """

    partner = models.ForeignKey(Partner)
    name = models.CharField(max_length=256)
    description = models.TextField(blank=True)

    def get_statuses_ordered(self):
        current_status = self.get_first_status()
        statuses = []
        while current_status:
            statuses.append(current_status)
            current_status = current_status.next
        return statuses

    def get_first_status(self) -> td.AbstractWorkflowStatus:
        current = self.statuses.first()
        if not current:
            raise Exception('Workflow has no statuses')

        while hasattr(current, 'previous'):
            current = current.previous
        return current

    def __str__(self) -> str:
        """
        String representation for workflow
        """
        return f'{self.partner.name}: {self.name}'

    class Meta:
        abstract = True
