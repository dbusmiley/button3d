from django.db import models, transaction

from apps.b3_mes.models.abstract_workflow import AbstractWorkflow
from apps.b3_mes.models import Sequence


class Workflow(AbstractWorkflow):
    """
    Workflow for MES Parts (Lines)

    -- this is a duplication of WorkflowTemplate to allow for
        customization on the part level
    """

    part = models.OneToOneField(
        'b3_order.OrderLine', related_name='workflow', on_delete=models.CASCADE
    )

    def statuses_str(self) -> str:
        current = self.get_first_status()
        statuses_str = ''
        while current:
            statuses_str += f'{current.id} {current.name}'
            if current.next:
                statuses_str += ' -> '
            current = current.next
        return statuses_str

    @transaction.atomic
    def create_sequences(self) -> None:
        """
        Creates sequences for part and assigns them to first status.
        -- If part already has sequences, delete them first.
        """
        order = self.part.order
        for i in range(self.part.quantity):
            Sequence(
                order=order,
                part=self.part,
                status=self.get_first_status(),
            ).save()
