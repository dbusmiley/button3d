from django.db import models
from django.core.exceptions import ValidationError
from django.utils.timezone import now

from apps.b3_core.models.deleted_date import AbstractDeletedDateModel
from apps.b3_organization.models.site import AbstractSiteModel
import button3d.type_declarations as td

import typing as t


class Sequence(AbstractDeletedDateModel, AbstractSiteModel):
    order = models.ForeignKey(
        'b3_order.Order', related_name='sequences', on_delete=models.PROTECT
    )
    part = models.ForeignKey(
        'b3_order.OrderLine', related_name='sequences',
        on_delete=models.PROTECT
    )
    status = models.ForeignKey(
        'b3_mes.WorkflowStatus',
        related_name='sequences',
        on_delete=models.PROTECT,
    )

    modified = models.DateTimeField(auto_now=True)

    @property
    def next_status(self) -> t.Optional[td.AbstractWorkflowStatus]:
        if self.status:
            return self.status.next
        return None

    @property
    def basket(self) -> td.Basket:
        return self.part.order.project

    @property
    def job_number(self) -> int:
        job = self.job
        return job.id if job else -1

    @property
    def job_status(self) -> str:
        job = self.job
        return job.status if job else 'invalid'

    @property
    def job(self) -> td.MESJob:
        """
        Get first upcoming job or last job is all started
        :return:
        """
        first_upcoming_job = self.job_set.filter(
            start_time__gte=now()).order_by('start_time').first()
        if first_upcoming_job:
            return first_upcoming_job
        return self.job_set.last()

    def __str__(self) -> str:
        return f'{self.part.name}: {self.id}'

    def save(self, *args, **kwargs) -> None:
        self._validate_correct_part()

        super().save(*args, **kwargs)

    def _validate_correct_part(self) -> None:
        """Validate that part reference the same order as self"""
        if self.part.order.id != self.order.id:
            raise ValidationError(
                f'Cannot save Sequence with part and self belong to different'
                f' orders. Part order id: {self.part.order.id}, '
                f'self order id: {self.order.id}'
            )
