from django.db import models

from apps.b3_mes.models.abstract_workflow_status import AbstractWorkflowStatus
from apps.b3_mes.models import Workflow


class WorkflowStatus(AbstractWorkflowStatus):
    """
    Workflow status
    """

    workflow = models.ForeignKey(
        Workflow, related_name='statuses', on_delete=models.CASCADE
    )

    def delete(self, *args, **kwargs) -> None:
        self._set_sequences_to_first_status()

        super().delete(*args, **kwargs)

    def _set_sequences_to_first_status(self) -> None:
        """
        Check if any instances of :model: `Sequence` are related to this
        status, if so, set them to the first status in the current workflow
        """
        self.refresh_from_db()

        sequences = self.sequences.all()
        if not sequences:
            return

        first_status = self.workflow.get_first_status()
        if self.id == first_status.id:
            first_status = first_status.next
        if first_status.id == self.id:
            first_status = self.next

        for sequence in sequences:
            sequence.status = first_status
            sequence.save()
