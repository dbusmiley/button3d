from django.db import models

from apps.b3_attachement.models import AbstractAttachment
from apps.b3_mes.models.job import Job


class JobAttachment(AbstractAttachment):
    """
    A Job attachment, uploaded through MES
    """

    ATTACHMENT_FOLDER = 'job_attachments'
    job = models.ForeignKey(
        Job,
        related_name='attachments',
    )
