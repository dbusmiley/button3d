from django.db import models

from apps.b3_mes.models import WorkflowStatus
from apps.b3_mes.models.abstract_workflow_status import AbstractWorkflowStatus
from apps.b3_mes.models.workflow_template import WorkflowTemplate
import button3d.type_declarations as td


class WorkflowTemplateStatus(AbstractWorkflowStatus):
    """
    WorkflowTemplate status
    """

    workflow = models.ForeignKey(
        WorkflowTemplate, related_name='statuses', on_delete=models.CASCADE
    )

    def generate_workflow_status(
        self,
        next_status: td.WorkflowTemplateStatus,
        workflow: td.Workflow
    ) -> td.WorkflowStatus:
        status = WorkflowStatus(
            name=self.name,
            next=next_status,
            workflow=workflow,
            workstation=self.workstation,
        )
        status.save()
        return status
