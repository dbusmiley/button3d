from django.core.exceptions import ValidationError
from django.db import models
from django.conf import settings

from apps.b3_core.models.deleted_date import AbstractDeletedDateModel
from apps.b3_mes.models.workstation import Workstation
from apps.b3_mes.models.sequence import Sequence
from apps.b3_organization.models.site import AbstractSiteModel
from apps.catalogue.models.product import Product


class Job(AbstractDeletedDateModel, AbstractSiteModel):
    STATUS_STARTED = 'started'
    STATUS_SCHEDULED = 'scheduled'
    STATUS_FINISHED = 'finished'
    STATUS_FAILED = 'failed'
    STATUS_CHOICES = (
        (STATUS_STARTED, 'Started'),
        (STATUS_SCHEDULED, 'Scheduled'),
        (STATUS_FINISHED, 'Finished'),
        (STATUS_FAILED, 'Failed'),
    )

    TYPE_PRINTING = 'printing'
    TYPE_MAINTENANCE = 'maintenance'
    TYPE_CHOICES = (
        (TYPE_PRINTING, 'Printing'),
        (TYPE_MAINTENANCE, 'Maintenance'),
    )

    workstation = models.ForeignKey(
        Workstation,
        on_delete=models.CASCADE
    )
    material = models.ForeignKey(
        Product,
        on_delete=models.PROTECT
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT
    )

    name = models.CharField(
        max_length=128,
        blank=True,
        default=''
    )

    status = models.CharField(
        choices=STATUS_CHOICES,
        default=STATUS_SCHEDULED,
        max_length=32
    )
    type = models.CharField(
        choices=TYPE_CHOICES,
        default=TYPE_PRINTING,
        max_length=32)
    instructions = models.CharField(
        max_length=2048,
        blank=True,
    )

    sequences = models.ManyToManyField(Sequence)
    start_time = models.DateTimeField()
    finish_time = models.DateTimeField()
    process_parameters = models.CharField(max_length=1024, blank=True)
    material_batch_number = models.CharField(max_length=1024, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs) -> None:
        self._validate_material()

        return super().save(*args, **kwargs)

    def _validate_material(self) -> None:
        if not self.workstation.can_print_material(self.material):
            raise ValidationError(f'The workstation {self.workstation} does '
                                  f'not support material {self.material}')

    def __str__(self) -> str:
        return f'{self.workstation.name}: {self.start_time} - ' \
            f'{self.finish_time}'
