from django.db import models
from django.utils import timezone

from apps.b3_core.models.deleted_date import AbstractDeletedDateModel
from apps.b3_organization.models.site import AbstractSiteModel
from apps.partner.models.partner import Partner
from apps.catalogue.models.product import Product
from apps.partner.models.post_processing import PostProcessing
import button3d.type_declarations as td

import typing as t
import datetime

from ordered_model.models import OrderedModel


class Workstation(AbstractDeletedDateModel, AbstractSiteModel, OrderedModel):
    partner = models.ForeignKey(
        Partner, related_name='workstations', on_delete=models.CASCADE
    )
    materials = models.ManyToManyField(Product)
    post_processings = models.ManyToManyField(PostProcessing)

    name = models.CharField(max_length=128)
    description = models.CharField(max_length=1024, blank=True)
    type = models.CharField(max_length=128)

    is_printing = models.BooleanField(default=True)

    dimension_x = models.IntegerField()
    dimension_y = models.IntegerField()
    dimension_z = models.IntegerField()

    bounding_box3d = models.BooleanField(default=True)

    load_factor = models.FloatField(default=100)

    def can_print_material(self, material: td.Product) -> bool:
        return self.materials.filter(
            id=material.id
        ).exists()

    @property
    def usable_volume(self) -> float:
        if self.bounding_box3d:
            total_volume = self.dimension_x * self.dimension_y * \
                self.dimension_z
            return self.load_factor * total_volume / 100
        raise Exception('Bounding box is 2D, consider '
                        'using property usable_area instead')

    @property
    def usable_area(self) -> float:
        if not self.bounding_box3d:
            total_volume = self.dimension_x * self.dimension_y
            return self.load_factor * total_volume / 100
        raise Exception('Bounding box is 3D, consider '
                        'using property usable_volume instead')

    def get_assignable_parts(self) -> td.DjangoQuerySet:
        """
        Get list of parts that can have assignable sequences to the current
        :model: `Workstation`
        :return:
        """
        from apps.b3_order.models import OrderLine

        parts = OrderLine.objects.select_related(
            'order',
            'stock_record',
            'stock_record__product',
            'stock_record__partner',
            'stl_file',
            'configuration'
        ).exclude(
            workflow=None
        )

        return parts

    def get_suggested_parts_sequences(
        self
    ) -> t.Sequence[t.Tuple[td.OrderLineNew, t.Sequence[int]]]:
        """
        Get list of parts and their perspective sequences that are suggested
        to be printed first based on their delivery date and size
        :return: array of tuples of part and array of sequences e.g.
        [(<Part>, [<Sequence>]),]
        """
        def delivery_date(obj: td.OrderLineNew) -> datetime.date:
            # We cannot return None here, cause we can't do comparisons
            # with None values. And since sorted calls it, this will fail.
            # Instead we return a value far in the future so the unknowns are
            # sorted last.
            try:
                return obj.get_estimated_delivery_date()
            except AttributeError:
                far_future = timezone.now() + datetime.timedelta(days=365)
                return far_future.date()

        sorted_assignable_parts = sorted(
            self.get_assignable_parts(),
            key=delivery_date
        )

        suggested_parts_sequences = []
        used_space = 0

        for part in sorted_assignable_parts:
            sequences = []
            for sequence in part.get_assignable_sequences(self.id):
                if part.is_fitting_in(self, used_space=used_space):
                    sequences.append(sequence)
                    used_space += part.required_space(self)
                else:
                    break
            if sequences:
                suggested_parts_sequences.append((part, sequences))

        return suggested_parts_sequences

    def __str__(self) -> str:
        return self.name
