from django.db import models

from apps.b3_attachement.models import AbstractAttachment


class PartAttachment(AbstractAttachment):
    """
    A part attachment, uploaded through MES
    """

    ATTACHMENT_FOLDER = 'part_attachments'
    part = models.ForeignKey(
        'b3_order.OrderLine',
        related_name='part_attachments',
    )
