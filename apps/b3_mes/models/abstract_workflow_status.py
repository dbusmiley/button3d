from django.db import models
from django.core.exceptions import ValidationError

from apps.b3_mes.models.workstation import Workstation
from apps.b3_organization.models.site import AbstractSiteModel
import button3d.type_declarations as td

import typing as t


class AbstractWorkflowStatus(AbstractSiteModel):
    """
    Status for Workflow for MES parts (Linesa)
    """

    next = models.OneToOneField(
        'self',
        null=True,
        blank=True,
        on_delete=models.PROTECT,
        related_name='previous',
    )
    workstation = models.ForeignKey(
        Workstation,
        related_name='%(class)s',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    name = models.CharField(max_length=256)

    def __str__(self) -> str:
        """
        String representation for status
        """
        return f'{self.workflow.name}: {self.name}'

    def save(self, *args, **kwargs) -> None:
        if self.next:
            if self.next.workflow.id != self.workflow.id:
                raise ValidationError(
                    'It is not possible to store WorkflowTemplateStatus '
                    'with the value for next is referencing a '
                    'status from another workflow'
                )
            elif hasattr(self, 'pk') and self.pk == self.next.pk:
                raise ValidationError(
                    'It is not possible to store WorkflowTemplateStatus '
                    'with the value for next is itself'
                )
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs) -> None:
        previous_status, next_status = \
            self._update_references_to_self_pre_delete()
        super().delete(*args, **kwargs)
        AbstractWorkflowStatus._update_references_to_self_post_delete(
            previous_status, next_status)

    def _update_references_to_self_pre_delete(
        self
    ) -> t.Tuple[td.AbstractWorkflowStatus, td.AbstractWorkflowStatus]:
        """
        If another status references `self` set it to reference `self.next`
        """
        self.refresh_from_db()
        next_status = self.next
        previous_status = None
        if hasattr(self, 'previous'):
            previous_status = self.previous
            previous_status.next = None
            previous_status.save()
        return previous_status, next_status

    @staticmethod
    def _update_references_to_self_post_delete(
        previous_status: td.AbstractWorkflowStatus,
        next_status: td.AbstractWorkflowStatus
    ) -> None:
        if previous_status:
            previous_status.next = next_status
            previous_status.save()

    class Meta:
        abstract = True
