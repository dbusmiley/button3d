import button3d.type_declarations as td

import typing as t


def get_workflow_statuses_order(
    workflow: td.AbstractWorkflow
) -> t.Sequence[td.AbstractWorkflowStatus]:
    """
    Get list of ordered status for :param: `workflow`
    """
    last_status = workflow.statuses.get(next=None)
    ordered_statuses = [last_status]

    current = last_status
    while hasattr(current, 'previous'):
        ordered_statuses = [current] + ordered_statuses
        current = current.previous
    return ordered_statuses
