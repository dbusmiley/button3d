# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('offer', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='benefit',
            name='proxy_class',
            field=models.CharField(default=None, max_length=255, verbose_name='Custom class'),
            preserve_default=True,
        ),
    ]
