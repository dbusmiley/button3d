import time
from urllib.parse import urljoin

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.module_loading import import_string
from django.utils.translation import ugettext_lazy as _

from apps.b3_core.beautify import pretty_filesize_with_unit
from apps.b3_core.models import AbstractDeletedDateModel
from apps.b3_organization.utils import get_base_url

from apps.b3_attachement.utils import generate_sha1


AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


def upload_to_unique_folder(instance, filename):
    """
    Uploads a file to an unique generated Path to keep the original filename
    """

    salt, hash = generate_sha1('{}{}'.format(filename, time.time()))

    return '%(path)s/%(hash_path)s/%(filename)s' % {
        'path': instance.ATTACHMENT_FOLDER,
        'hash_path': hash[:16],
        'filename': filename
    }


@python_2_unicode_compatible
class AbstractAttachment(AbstractDeletedDateModel):
    """
    This class provides an abstract Attachment and is meant to be
    subclassed from within other apps.

    A Subclass of this AbstractAttachment should
    contain at least a foreign key to the attachee.
    """
    MAX_SIZE_IN_MB = 500
    ATTACHMENT_FOLDER = "attachments"

    file = models.FileField(
        upload_to=upload_to_unique_folder,
        max_length=512,
        storage=import_string(settings.PRIVATE_FILE_STORAGE)()
    )
    filename = models.CharField(max_length=256)
    filesize = models.IntegerField()

    creation_date = models.DateTimeField(default=timezone.now)

    uploader = models.ForeignKey(
        AUTH_USER_MODEL,
        null=True,
        blank=True,
        verbose_name=_("Uploader")
    )

    @property
    def pretty_size(self):
        return pretty_filesize_with_unit(self.filesize)

    def __str__(self):
        return '{0} (id: {1}) (size: {2})'.format(
            self.filename, self.pk, pretty_filesize_with_unit(self.filesize))

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.filename,
            'size': self.pretty_size,
            'url': self.file.url,
            'uploader':
                self.uploader.userprofile.full_name if self.uploader else None,
            'uploader_pk': self.uploader.pk if self.uploader else None,
            'creation_date': self.creation_date,
            'pretty_creation_date': time.mktime(self.creation_date.timetuple())
        }

    @property
    def get_absolute_url(self):
        return urljoin(get_base_url(), self.file.url)

    class Meta:
        abstract = True
        verbose_name = _('Attachment')
        verbose_name_plural = _('Attachments')
        ordering = ('pk',)
