from django.http import JsonResponse

from apps.b3_attachement.models import AbstractAttachment
from django.views.generic.base import View

from apps.b3_core.permissions import require_ajax
from django.utils.decorators import method_decorator


class AbstractAttachmentAjaxView(View):
    """
    This view is for creating, deleting and
    getting attachments via ajax and is used by dropzone.js
    The view is meant to be subclassed by other apps.
    """

    attachment_class = AbstractAttachment
    http_method_names = ['get', 'post', 'delete']

    @method_decorator(require_ajax)
    def dispatch(self, request, *args, **kwargs):
        return super(AbstractAttachmentAjaxView, self).\
            dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.check_user_allowed_to_post(request)
        attachments = []
        user = request.user if not request.user.is_anonymous else None

        for f, val in request.FILES.items():
            attachment_model = self.attachment_class.objects.create(
                file=val,
                filename=val.name,
                filesize=val.size,
                uploader=user)
            attachments.append(attachment_model.to_dict())

        return JsonResponse({'success': True, 'attachments': attachments})

    def delete(self, request, *args, **kwargs):
        try:
            id = request.GET.get('id')
            attachment = self.attachment_class.objects.get(pk=id)
            self.check_user_allowed_to_delete(request, attachment)
            attachment.delete()
            return JsonResponse({'success': True})
        except Exception:
            return JsonResponse({
                'success': False,
                'message': 'Attachment not found'})

    def get(self, request, *args, **kwargs):
        try:
            id = request.GET.get('id')
            attachment = self.attachment_class.objects.get(pk=id)
            self.check_user_allowed_to_get(request, attachment)
            return JsonResponse({
                'success': True,
                'attachment': self.attachment_class.objects.get(
                    pk=id).to_dict()})
        except Exception as e:
            return JsonResponse({'success': False, 'message': str(e)})

    def check_user_allowed_to_get(self, request, attachment):
        """
        This method is supposed to be overidden by the sub class.
        It should check if the requester is allowed to see and
        modify the attachment.

        This method is supposed to raise PermissionDenied.
        :return:
        """
        return True

    def check_user_allowed_to_delete(self, request, attachment):
        """
        This method is supposed to be overidden by the sub class.
        It should check if the requester is allowed to delete the attachment.

        This method is supposed to raise PermissionDenied.
        :return:
        """
        return True

    def check_user_allowed_to_post(self, request):
        """
        This method is supposed to be overidden by the sub class.
        It should check if the requester is allowed to create a new attachment.

        This method is supposed to raise PermissionDenied.
        :return:
        """
        return True
