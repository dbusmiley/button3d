import base64
from uuid import uuid4

try:
    from hashlib import sha1 as sha_constructor
except ImportError:
    from django.utils.hashcompat import sha_constructor


def generate_sha1(string, salt=None):
    """
    Generates a sha1 hash for supplied string.

    :param string:
        The string that needs to be encrypted.

    :param salt:
        Optionally define your own salt. If none is supplied, will use a random
        string of 5 characters.

    :return: Tuple containing the salt and hash.

    """
    if not salt:
        salt = sha_constructor(
            str(uuid4()).encode('utf-8')).hexdigest()[:5]

    hash = base64.b64encode(
        sha_constructor(
            '{0}{1}'.format(salt, string).encode('utf-8')
        ).hexdigest().encode('utf-8')
    ).decode()

    return salt, hash
