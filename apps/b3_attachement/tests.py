# -*- coding: utf-8 -*-

from apps.b3_attachement.models import AbstractAttachment, \
    upload_to_unique_folder
from apps.b3_tests.testcases.common_testcases import TestCase


class AttachmentTest(TestCase):
    def test_upload_to_unique_folder(self):
        instance = AbstractAttachment
        filename = 'test.pdf'
        self.assertRegex(
            upload_to_unique_folder(instance, filename),
            'attachments\/(\w)+\/test\.pdf')

    def test_upload_to_unique_folder_unicode(self):
        instance = AbstractAttachment
        filename = 'déjà-vu.pdf'
        self.assertRegex(
            upload_to_unique_folder(instance, filename),
            'attachments\/(\w)+\/déjà-vu\.pdf')
