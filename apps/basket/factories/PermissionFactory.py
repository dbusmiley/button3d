from apps.b3_organization.factories.ContactInformationFactory import \
    ContactInformationFactory
from apps.basket.models import Basket


class PermissionFactory:
    @staticmethod
    def create_by_partner(partner):
        """
        Function to create permission instances for suppliers.
        :param partner: Partner the permission
            information should be created for.
        :return: A serializable dictionary containing
            all information about the supplier.
        """
        contact_dict = {}
        if partner.email:
            contact_dict['email'] = partner.email
        if partner.address.phone_number:
            contact_dict['phone'] = str(partner.address.phone_number)
        if partner.address.country:
            contact_dict['country'] = partner.address.country.name

        supplier_with_perm = {
            'name': partner.display_name,
            'email': partner.email,
            'role': 'supplier',
            'permission':
                PermissionFactory.get_supplier_permission_dictionary(),
            'contact': contact_dict
        }
        return supplier_with_perm

    @staticmethod
    def create_by_user(user, permissions, user_is_superuser_owner=False):
        contact_information = \
            ContactInformationFactory.get_contact_information_for_user(user)
        contact_data = contact_information['contact']
        role = 'owner' if user_is_superuser_owner \
            else PermissionFactory.get_role_by_permissions(permissions)
        user_with_perm = {
            'name': user.userprofile.full_name,
            'email': user.email,
            'role': role,
            'permission':
                PermissionFactory.create_permission_instance(permissions),
            'contact': contact_data
        }
        return user_with_perm

    @classmethod
    def create_permission_instance(cls, permissions):
        permission_dict = {
            'can_view_project': Basket.VIEW_PERMISSION in permissions,
            'can_edit_project': Basket.EDIT_PERMISSION in permissions,
            'can_share_project': Basket.SHARE_PERMISSION in permissions,
            'can_price_project': Basket.PRICING_PERMISSION in permissions,
            'can_order_project': Basket.ORDER_PERMISSION in permissions
        }
        return permission_dict

    @classmethod
    def get_owner_permission_dictionary(cls):
        return {
            'can_view_project': True,
            'can_edit_project': True,
            'can_share_project': True,
            'can_price_project': False,
            'can_order_project': True
        }

    @classmethod
    def get_viewer_permission_dictionary(cls):
        return {
            'can_order_project': True,
            'can_share_project': False,
            'can_edit_project': True,
            'can_view_project': True,
            'can_price_project': False
        }

    @classmethod
    def get_supplier_permission_dictionary(cls):
        return {
            'can_order_project': False,
            'can_share_project': False,
            'can_edit_project': False,
            'can_view_project': True,
            'can_price_project': False
        }

    @classmethod
    def get_role_by_permissions(cls, permissions):
        user_permission_dict = PermissionFactory.\
            create_permission_instance(permissions)
        if user_permission_dict == PermissionFactory.\
                get_owner_permission_dictionary():
            return 'owner'
        if user_permission_dict == PermissionFactory.\
                get_viewer_permission_dictionary():
            return 'viewer'
        if user_permission_dict == PermissionFactory.\
                get_supplier_permission_dictionary():
            return 'supplier'
        return None
