from guardian.shortcuts import get_users_with_perms

from apps.b3_organization.utils import getattr_theme
from apps.basket.factories.PermissionFactory import PermissionFactory
from apps.partner.models import StockRecord, Partner


def craft_update_dictionary_default_material(is_single_supplier):
    """
    Function to get the update dictionary for
        organization sites to set the initial state of the model.
    :param is_single_supplier: Boolean which describes if
        the current partner is a single supplier or not. This is
        needed because when the organization is a
        single supplier then we need to select a supplier slug.
    :return: A dictionary containing all the information
        needed to set the initial state in the project line view.
    """
    default_product = getattr_theme('project_default_material')
    if default_product is None:
        return None
    update_dictionary = {
        "product": default_product
    }
    # Fallback for single supplier
    if is_single_supplier:
        partner = Partner.objects.first()
        update_dictionary['stockrecord'] = StockRecord.objects.get(
            product=default_product,
            partner=partner
        )
    return update_dictionary


def generate_basket_user_permissions(basket):
    any_permission = get_users_with_perms(basket, attach_perms=True)

    users_with_perm = []
    if basket.is_submitted or \
        basket.has_manual_pricing_request_sent or \
            basket.is_manually_priced:
        supplier = basket.lines.first().partner
        supplier_permission = PermissionFactory.create_by_partner(
            partner=supplier
        )
        users_with_perm.append(supplier_permission)
    for user, perms in any_permission.items():
        user_is_superuser_owner = user == basket.owner and \
            basket.owner.is_superuser
        user_with_perm = PermissionFactory.create_by_user(
            user, perms, user_is_superuser_owner=user_is_superuser_owner
        )
        users_with_perm.append(user_with_perm)

    return users_with_perm
