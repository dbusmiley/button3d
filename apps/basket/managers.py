from apps.b3_organization.models import CurrentSiteManager
from apps.b3_core.managers import DeletedDateManager


class StatusFilterMixin:
    def get_status_filter(self) -> str:
        raise NotImplementedError('Subclasses must return a status filter')

    def get_queryset(self):
        return super().get_queryset().filter(status=self.get_status_filter())


class BasketManager(CurrentSiteManager, DeletedDateManager):
    pass


class CurrentSiteOpenBasketManager(StatusFilterMixin, BasketManager):
    def get_status_filter(self) -> str:
        return self.model.OPEN


class CurrentSiteSavedBasketManager(StatusFilterMixin, BasketManager):
    def get_status_filter(self) -> str:
        return self.model.SAVED
