import logging
import typing as t

from django.conf import settings
from django.core.signing import BadSignature, Signer
from django.utils.functional import SimpleLazyObject, empty

from apps.b3_core.base import BaseMiddleware
from apps.basket.models import Basket
from apps.partner.strategy import Selector
import button3d.type_declarations as td

logger = logging.getLogger(__name__)


selector = Selector()


class BasketMiddleware(BaseMiddleware):
    """ Adapted version of the oscar BasketMiddleware.
    This version does not merge the anonymous basket to the user one.
    """

    def init_basket(self, request: td.RequestWithBasket):
        # Load stock/price strategy and assign to request (it will later be
        # assigned to the basket too).
        strategy = selector.strategy(request=request, user=request.user)
        request.strategy = strategy

        # We lazily load the basket so use a private variable to hold the
        # cached instance.
        request._basket_cache = None

        def load_full_basket():
            """
            Return the basket after setting strategy
            """
            basket = self.get_basket(request)
            basket.strategy = request.strategy

            return basket

        def load_basket_hash():
            """
            Load the basket and return the basket hash

            Note that we don't apply offers or check that every line has a
            stockrecord here.
            """
            basket = self.get_basket(request)
            if basket.id:
                return self.get_basket_hash(basket.id)

        # Use Django's SimpleLazyObject to only perform the loading work
        # when the attribute is accessed.
        request.basket = SimpleLazyObject(load_full_basket)
        request.basket_hash = SimpleLazyObject(load_basket_hash)

    def __call__(self, request: td.RequestWithBasket):
        request.cookies_to_delete = []
        self.init_basket(request)

        response = self.get_response(request)
        # Always do cookie maintenance
        cookies_to_delete = getattr(request, 'cookies_to_delete', [])
        for cookie_key in cookies_to_delete:
            response.delete_cookie(cookie_key)

        if not hasattr(request, 'basket'):
            return response

        # noinspection PyProtectedMember
        if (
            isinstance(request.basket, SimpleLazyObject) and
            request.basket._wrapped is empty
        ):
            return response

        cookie_key = self.get_cookie_key(request)
        # Check if we need to set a cookie. If the cookies is already available
        # but is set in the cookies_to_delete list then we need to re-set it.
        has_basket_cookie = (
            cookie_key in request.COOKIES
            and cookie_key not in cookies_to_delete)

        # If a basket has had products added to it, but the user is anonymous
        # then we need to assign it to a cookie
        if (
            request.basket.id and not request.user.is_authenticated
            and not has_basket_cookie
        ):
            cookie = self.get_basket_hash(request.basket.id)
            response.set_cookie(
                cookie_key, cookie,
                max_age=settings.BASKET_COOKIE_LIFETIME,
                secure=settings.USE_SECURE_COOKIES, httponly=True
            )
        return response

    @staticmethod
    def get_cookie_key(request: td.Request) -> str:
        """
        DEPRECATED

        This method has a request argument, so people can return different
        cookie names based on for example, the hostname.

        Ironically, cookies can by nature be tied to request parameters,
        including hostname and path, so this isn't very useful for the real
        world. This is why it's deprecated.
        """
        return settings.BASKET_COOKIE_NAME

    @staticmethod
    def get_cookie_basket(
        cookie_key: str, request: td.RequestWithBasket,
        manager: td.BasketManager
    ) -> t.Optional[td.Basket]:
        """
        Looks for a basket which is referenced by a cookie.

        If a cookie key is found with no matching basket, then we add
        this cookie to the list to be deleted. This list is maintained on
        the request object, so that views / serializers / templates and what
        have you, can add cookies to this list.

        Clearly the basket oversteps it's boundaries here and this should be
        refactored with a whitelist of cookies that may be found in this
        deletion list.
        """
        basket = None
        if cookie_key in request.COOKIES:
            basket_hash = request.COOKIES[cookie_key]
            try:
                basket_id = Signer().unsign(basket_hash)
                basket = manager.get(pk=basket_id, owner=None)
            except (BadSignature, Basket.DoesNotExist):
                request.cookies_to_delete.append(cookie_key)
        return basket

    def get_basket(self, request: td.RequestWithBasket):
        """
        Return the open basket for this request
        """
        if request._basket_cache is not None:
            return request._basket_cache

        manager = Basket.open
        # TODO: This is probably not needed as API with a request argument
        cookie_key = self.get_cookie_key(request)
        cookie_basket = self.get_cookie_basket(cookie_key, request, manager)
        ordered_basket_id = request.session.get('ordered_basket_id', -1)

        # FIXME: Does this generate duplicate baskets?
        if ordered_basket_id is not -1:
            # ordered_basket_id will be set by basket.prepare_for_checkout()
            basket = Basket.objects.filter(
                id=ordered_basket_id
            ).first()
            if not basket:
                basket = Basket()
                del request.session['ordered_basket_id']
            return basket

        # FIXME: This reachability claim seems bogus.
        # The below code is only reachable for the _tests
        # in which baskets get crafted out of the normal order process.
        if hasattr(request, 'user') and request.user.is_authenticated:
            # Signed-in user: if they have a cookie basket too, it means
            # that they have just signed in and we need to merge their cookie
            # basket into their user basket, then delete the cookie.
            try:
                basket, __ = manager.get_or_create(owner=request.user)
            except Basket.MultipleObjectsReturned:
                # Not sure quite how we end up here with multiple baskets.
                # We merge them and create a fresh one
                old_baskets = list(manager.filter(owner=request.user))
                basket = old_baskets[0]
                for other_basket in old_baskets[1:]:
                    basket.merge(other_basket, add_quantities=False)

            # Assign user onto basket to prevent further SQL queries when
            # basket.owner is accessed.
            basket.owner = request.user

            if cookie_basket:
                basket.status = Basket.EDITABLE
                basket.save()

                basket = cookie_basket
                basket.owner = request.user  # Assign user basket
                basket.save()

        elif cookie_basket:  # Anonymous user with a basket tied to the cookie
            basket = cookie_basket
        else:
            # Anonymous user with no basket - instantiate a new basket
            # instance.  No need to save yet.
            basket = Basket()

        # Cache basket instance for the during of this request
        request._basket_cache = basket

        return basket

    @staticmethod
    def get_basket_hash(basket_id):
        return Signer().sign(basket_id)

    # FIXME: Very fishy and may be needed elsewhere (!template_response)
    def process_template_response(self, request, response):
        if hasattr(response, 'context_data'):
            if response.context_data is None:
                response.context_data = {}
            if 'basket' not in response.context_data:
                response.context_data['basket'] = request.basket
            else:
                # Occasionally, a view will want to pass an alternative basket
                # to be rendered.  This can happen as part of checkout
                # processes where the submitted basket is frozen when the
                # customer is redirected to another site (eg PayPal).  When the
                # customer returns and we want to show the order preview
                # template, we need to ensure that the frozen basket gets
                # rendered (not request.basket).  We still keep a reference to
                # the request basket (just in case).
                response.context_data['request_basket'] = request.basket
        return response
