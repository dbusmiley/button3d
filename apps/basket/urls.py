from django.conf.urls import url

import apps.basket.views as views

urlpatterns = [
    url(r'^empty_basket/$', views.empty_basket, name='empty-basket'),
    url(r'^search/', views.baskets_search, name='search'),
    url(r'^create/', views.create_basket, name='create'),
    url(
        r'^(?P<basket_id>[0-9]+)/$',
        views.BasketAjaxView.as_view(),
        name='basket-details'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/add/$',
        views.add_file_to_basket,
        name='add-file'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/clone/$',
        views.basket_clone,
        name='clone'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/manual_price/$',
        views.request_manual_price,
        name='manual-price'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/set_prices/$',
        views.set_prices,
        name='set-prices'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/set_as_priced/$',
        views.set_basket_as_priced,
        name='set-basket-as-priced'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/url/$',
        views.basket_url,
        name='basket-url'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/lines/(?P<line_id>[0-9]+)/$',
        views.BasketLineAjaxView.as_view(),
        name='basket-line-details'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/lines/(?P<line_id>[0-9]+)/clone/$',
        views.basket_line_clone,
        name='clone-line'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/permission/$',
        views.basket_permission,
        name='basket-permission'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/users/$',
        views.BasketUsersView.as_view(),
        name='basket-users'
    ),
    url(
        r'^(?P<basket_id>[0-9]+)/comments/$',
        views.BasketCommentsView.as_view(),
        name='basket-comments'
    ),

    url(
        r'^attachment/ajax/$',
        views.BasketAttachmentAjaxView.as_view(),
        name='basket-attachments'
    )
]
