__all__ = (
    'InvalidBasketLineError',
    'BasketUpdateException',
    'NotAllLinesPricedException',
)


class InvalidBasketLineError(Exception):
    pass


class BasketUpdateException(Exception):
    pass


class NotAllLinesPricedException(Exception):
    pass
