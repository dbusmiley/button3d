import json
import logging
from decimal import Decimal
from uuid import UUID

from braces.views import JSONRequestResponseMixin, AjaxResponseMixin
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied, ValidationError, \
    SuspiciousOperation
from django.core.serializers import serialize
from django.http import HttpResponseBadRequest
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.http import require_GET, require_POST
from django.views.generic.base import View
from guardian.shortcuts import remove_perm, assign_perm

from apps.b3_attachement.views import AbstractAttachmentAjaxView
from apps.b3_core import permissions
from apps.b3_core.mail import EmailSender
from apps.b3_core.models import StlFile
from apps.b3_core.utils import get_country
from apps.b3_organization import utils as org_utils
from apps.b3_organization.factories.ContactInformationFactory import \
    ContactInformationFactory
from apps.b3_organization.utils import get_current_org
from apps.b3_user_panel import permissions as user_permissions
from apps.basket.models import Basket, Line, BasketAttachment
from apps.basket.models import Comment
from apps.basket.exceptions import NotAllLinesPricedException, \
    BasketUpdateException
from apps.basket.utils import craft_update_dictionary_default_material, \
    generate_basket_user_permissions
from apps.catalogue.models.product import Product
from apps.catalogue.models.color import Color
from apps.partner.models import StockRecord, Partner, PostProcessing, Price

logger = logging.getLogger(__name__)


@permissions.require_DELETE
def empty_basket(request):
    request.basket.flush()
    return JsonResponse({'success': True})


# ======= Ajax views ========


class BasketAjaxView(JSONRequestResponseMixin, AjaxResponseMixin, View):
    http_method_names = ['get', 'post', 'delete']

    @method_decorator(permissions.require_ajax)
    def dispatch(self, request, *args, **kwargs):
        self.session_currency = request.GET.get('currency', None)
        self.basket = get_object_or_404(Basket, pk=kwargs.pop('basket_id'))
        self.organization = get_current_org()
        if not self.basket.can_user_view(user=request.user, request=request):
            raise PermissionDenied

        return super(BasketAjaxView, self).dispatch(request, *args, **kwargs)

    def show_net_prices(self):
        if self.organization.show_net_prices:
            return True
        if self.basket.status == self.basket.SUBMITTED:
            is_net_priced = self.basket.price_object.tax == 0
            return is_net_priced
        return False

    def get_ajax(self, request, *args, **kwargs):
        basket_lines = list(self.basket.lines.order_by('date_created'))
        total_nb_lines = len(basket_lines)

        basket_json = json.loads(
            serialize('json', (self.basket,)))[0]['fields']
        basket_json['success'] = True

        # Add basket information
        basket_json['owner_email'] = self.basket.owner.email \
            if self.basket.owner else None
        basket_json['url'] = self.basket.url

        basket_json['nb_lines'] = total_nb_lines
        basket_json['lines'] = []
        for l in basket_lines:
            line_dict = {
                'id': l.id
            }
            basket_json['lines'].append(line_dict)

        # Attachments
        basket_json['attachments'] = []
        for attachment in self.basket.attachments.order_by('pk'):
            basket_json['attachments'].append(attachment.to_dict())

        # Pricing information
        basket_json['is_manual_pricing_required'] = \
            self.basket.is_manual_pricing_required
        basket_json['is_manually_priced'] = self.basket.is_manually_priced
        basket_json['is_frozen'] = self.basket.is_frozen
        basket_json['is_manual_pricing_user'] = \
            self.basket.can_user_price(request.user)
        basket_json['manual_pricing_currency'] = self.basket.currency
        basket_json['tax_type'] = self.basket.tax_type_partner()

        basket_price = self.basket.price_object
        # Recalculate prices, in case a Line has no price set.
        if not (basket_price and basket_price.currency):
            for l in self.basket.lines.filter(is_manual_pricing_required=False,
                                              product__isnull=False,
                                              stockrecord__isnull=False,
                                              price_excl_tax__isnull=True,
                                              price_incl_tax__isnull=True):
                self.basket.price_line(l, False)

        basket_price = self.basket.price_object_incl_discounts
        # Checking for partner before adding these to context to avoid problems
        if basket_price and self.basket.partner:
            basket_json['extra_fees'] = self.basket.list_extra_fees(
                currency=self.session_currency
            )
            basket_json['min_price'] = self.basket.min_order_price_object\
                .as_dict(
                    currency=self.session_currency
            )
            basket_json['diff_to_min_price'] = \
                self.basket.difference_to_minimum_price.as_dict(
                    currency=self.session_currency
            )
            basket_json['subtotal'] = self.basket.subtotal_price_object\
                .as_dict(
                currency=self.session_currency
            )
        if basket_price and basket_price.currency:
            basket_json['price'] = basket_price.as_dict(
                currency=request.GET.get("currency", None)
            )
            basket_json['price']['tax_rate'] = self.get_tax_rate()
        else:
            basket_json['price'] = None
        basket_json['show_net_prices'] = self.show_net_prices()
        if hasattr(self.basket, 'order') and hasattr(
            self.basket.order, 'voucher_discount'
        ):
            basket_json['voucher_discount'] = \
                self.basket.order.voucher_discount.as_dict()
        else:
            basket_json['voucher_discount'] = Price(
                excl_tax=0,
                currency=self.session_currency,
                tax=0
            ).as_dict()
        return self.render_json_response(basket_json)

    def get_tax_rate(self) -> Decimal:
        tax_rate = Decimal(0.00)
        if hasattr(self.basket, 'order'):
            tax_rate = self.basket.order.tax_rate
        elif self.basket.partner:
            tax_rate = self.basket.partner.org_options.vat_rate
        return tax_rate.normalize()

    def post_ajax(self, request, *args, **kwargs):
        try:
            if self.basket.update_fields(self.request_json):
                return self.render_json_response({'success': True})
            else:
                return self.render_json_response({
                    'success': True,
                    'message': 'Nothing was changed'
                })
        except Exception as ex:
            logger.warning(
                'Exception while updating basket fields (title/reference)')
            return self.render_json_response({
                'success': False, 'message': str(ex)})

    def delete_ajax(self, request, *args, **kwargs):
        try:
            self.basket.delete()
            response_dict = {
                'success': True,
                'redirect_url': reverse('b3_user_panel:projects')
            }
        except ValueError:
            response_dict = {
                'success': False
            }
        return self.render_json_response(response_dict)


class BasketLineAjaxView(JSONRequestResponseMixin, AjaxResponseMixin, View):
    http_method_names = ['get', 'post', 'delete']

    @method_decorator(permissions.require_ajax)
    def dispatch(self, request, *args, **kwargs):
        self.basket = get_object_or_404(Basket, pk=kwargs.pop('basket_id'))
        if not self.basket.can_user_view(user=request.user, request=request):
            raise PermissionDenied

        self.line = get_object_or_404(
            Line, pk=kwargs.pop('line_id'), basket=self.basket)
        return super(BasketLineAjaxView, self).dispatch(
            request, *args, **kwargs)

    def get_ajax(self, request, *args, **kwargs):
        try:
            line_dict = self.line.to_dict(
                currency=request.GET.get('currency', None))
            organization = get_current_org()
            line_dict['show_net_prices'] = organization.show_net_prices
            line_dict['success'] = True
            return self.render_json_response(line_dict)
        except BaseException:
            import traceback
            traceback.print_exc()
            logger.exception(
                'Exception while retrieving basket line information')
            return self.render_json_response({
                'success': False,
                'message': 'Error while retrieving information'
            })

    def post_ajax(self, request, *args, **kwargs):
        """ Contains the logic when calling from 3D projects.
            (selecting a default finish when supplier)
        It prepares the input dictionary for line.update()
        """
        if not self.line.basket.can_be_edited:
            return JsonResponse({
                'success': False,
                'message': "line can't be updated: basket is not editable"
            }, status=400)
        try:
            body_json = self.request_json

            product_slug = body_json.pop('product_slug', None)
            if product_slug:
                product = Product.objects.get(slug=product_slug)
            else:
                product = self.line.product

            partner_code = body_json.pop('supplier_slug', None)
            if partner_code and product:
                try:
                    partner = Partner.objects.get(code=partner_code)
                    stock_record = StockRecord.objects.get(
                        product=product, partner=partner
                    )
                except Partner.DoesNotExist:
                    raise ValidationError(
                        f'Invalid supplier_slug: {partner_code}'
                    )
                except StockRecord.DoesNotExist:
                    product_slug = product.slug
                    error_message = (
                        f'No Offer available for product_slug {product_slug}'
                        f' and supplier_slug {partner_code}'
                    )
                    raise ValidationError(error_message)
            elif product != self.line.product:
                stock_record = None
            else:
                stock_record = self.line.stockrecord

            post_processing_info_dict = body_json.pop('post_processings', None)
            if post_processing_info_dict is not None and stock_record:
                try:
                    post_processings = []
                    for info_dict in post_processing_info_dict:
                        slug = info_dict['slug'][len(product.slug)+1:]
                        color_id = info_dict['color_id']
                        post_processing = stock_record.post_processings.get(
                            slug=slug
                        )
                        color = post_processing.colors.get(
                            id=color_id
                        ) if color_id else None
                        if post_processing.colors.exists() and color is None:
                            raise ValidationError(
                                'Invalid color_id for post_processing '
                                f'{slug}: {color_id}'
                            )
                        post_processings.append({
                            'post_processing': post_processing,
                            'color': color
                        })
                except (TypeError, KeyError, IndexError):
                    raise ValidationError('Invalid format of post_processings')
                except PostProcessing.DoesNotExist:
                    raise ValidationError(
                        f'Invalid slug in post_processings: {slug}'
                    )
                except Color.DoesNotExist:
                    raise ValidationError(
                        f'Invalid color_id in post_processings: {color_id}'
                    )
            elif product != self.line.product \
                    or stock_record != self.line.stockrecord:
                post_processings = []
            else:
                post_processings = None

            if 'quantity' in body_json:
                if body_json['quantity'] is None:
                    body_json['quantity'] = 0
                try:
                    qt = int(body_json['quantity'])
                except (ValueError, TypeError):
                    qt_str = body_json['quantity']
                    raise SuspiciousOperation(
                        f'Invalid integer value for quantity "{qt_str}"'
                    )
                else:
                    body_json['quantity'] = qt

            body_json.update({
                'product': product,
                'stockrecord': stock_record,
                'post_processings': post_processings
            })
            self.line.update(body_json)
            line_dict = self.line.to_dict(
                currency=request.GET.get('currency', None)
            )
            line_dict['success'] = True
            return self.render_json_response(line_dict)
        except ValidationError as e:
            return self.render_json_response({
                'success': False,
                'message': str(e)
            })
        except SuspiciousOperation as ex:
            logger.exception(f'Attempt to mess with quantities: {str(ex)}')
            return self.render_json_response({
                'success': False,
                'message': ex.args[0]
            })
        except Exception as ex:
            logger.exception(
                f'Exception while updating basket line: {str(ex)}'
            )
            return self.render_json_response({
                'success': False,
                'message': 'unexpected error'
            })

    @staticmethod
    def fetch_stock_record_with_finish_slug(finish_slug, supplier_slug):
        stock_records = StockRecord.objects.filter(
            partner__code=supplier_slug,
            product__slug=finish_slug
        )
        if not len(stock_records) == 1:
            amount_stock_records = len(stock_records)
            raise BasketUpdateException(
                f'{amount_stock_records} StockRecord returned '
                f'for partner {supplier_slug} '
                f'and product {finish_slug}.'
            )

        return stock_records[0]

    def delete_ajax(self, request, *args, **kwargs):
        self.line.delete()
        return self.render_json_response({'success': True})


@user_permissions.check_user_permission
@permissions.require_ajax
@require_GET
def baskets_search(request):
    currency = request.GET.get('currency', request.POST.get('currency', None))
    country = get_country(request)
    baskets, nb_baskets = Basket.search(
        owner=request.user,
        status=request.GET.get('status', None),
        query=request.GET.get('name', None),
        date_0=request.GET.get('date_0', None),
        date_1=request.GET.get('date_1', None),
        page=request.GET.get('page', '1'),
        items_per_page=request.GET.get('items_per_page', '20'),
        currency=currency,
        country=country
    )
    # Gather information to return
    baskets_details = []
    for b in baskets:
        try:
            price_obj_with_shipping = b.price_object_with_shipping(country)
            price = price_obj_with_shipping.as_dict(currency=currency) if \
                price_obj_with_shipping and \
                price_obj_with_shipping.currency else None
            baskets_details.append(
                {
                    'project_id': b.id,
                    'title': b.title if b.title else '',
                    'url': b.url,
                    'date_created': b.date_created,
                    'nb_lines': b.lines.count(),
                    'price': price,
                    'is_ordered': b.is_submitted,
                    'quotation_number': b.quotation_number,
                    'lines': [
                        {'thumbnail_url': l.thumbnail_url}
                        for l in b.lines.all()[:min(
                            b.lines.count(),
                            3)]],
                    'is_shared': b.owner != request.user})
        except Exception:
            logger.exception(
                'Could not return this basket when searching for projects')

    response = {
        'success': True,
        'num_results': nb_baskets,
        'list': baskets_details
    }

    return JsonResponse(response)


@permissions.require_ajax
@permissions.require_PUT
@permissions.require_json_keys(['title'])
def create_basket(request):
    """
    API endpoint to create new baskets.
    This will also assign permissions to the basket
        if the user is authenticated.
    This is done here because guardian permissions are
        not easily accessible via the django orm model methods.
    :param request: Request which was sent.
    :return: A JSON response containing the result of the operation.
    """
    body_json = json.loads(request.body.decode('utf-8'))
    title = body_json['title']

    basket_owner = request.user if request.user.is_authenticated() else None
    basket = Basket.objects.create(
        owner=basket_owner, title=title, status=Basket.EDITABLE
    )
    basket.prepare_for_checkout(request)

    if request.user.is_authenticated:
        assign_perm(Basket.VIEW_PERMISSION, request.user, basket)
        assign_perm(Basket.EDIT_PERMISSION, request.user, basket)
        assign_perm(Basket.SHARE_PERMISSION, request.user, basket)
        assign_perm(Basket.ORDER_PERMISSION, request.user, basket)

    url = reverse(
        'b3_user_panel:project-detail',
        kwargs={
            'project_id': str(basket.id)
        }
    )
    return JsonResponse(
        {
            'success': True,
            'redirect_url': url,
            'id': basket.id
        }
    )


@permissions.require_ajax
@require_GET
def basket_url(request, basket_id):
    """ This should only be called on basket creation.
    * If the basket contains multiple lines, this function
        will return the basket detail url
    * If it contains only one file, the url depends on
        the organization flag 'single upload redirect to
        project detail'
    """
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise PermissionDenied
    num_lines = basket.num_lines
    if num_lines == 0:
        return HttpResponseBadRequest()

    # Multiple file upload => redirect to project detail
    if num_lines > 1:
        url = basket.url
    else:
        organization = get_current_org()
        if organization.single_upload_redirects_to_project_detail:
            url = basket.url
        else:
            url = basket.first_line.configure_url
    return JsonResponse({'success': True, 'url': url})


@permissions.require_ajax
@permissions.require_PUT
@permissions.require_json_keys(['uuid'])
def add_file_to_basket(request, basket_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise PermissionDenied

    body_json = json.loads(request.body.decode('utf-8'))
    file_uuid = body_json['uuid']
    # Currently ignored, but still asked in javascript ajax call.
    # Should be handled on the backend (with a queue i.e)
    # file_number = body_json['number'] if 'number' in body_json else None

    stlfile = StlFile.objects.filter(uuid=file_uuid).first()
    if not stlfile:
        return JsonResponse({'success': False})

    stlfile.assign_to_user_in_request(request)

    new_line = basket.add_empty_line(stlfile, quantity=1, scale=1)

    if request.organization is not None:
        is_single_supplier = request.organization.is_single_supplier
        update_dict = craft_update_dictionary_default_material(
            is_single_supplier)
        new_line.update(update_dict)

    return JsonResponse({'success': True, 'new_line_id': new_line.id})


@permissions.require_ajax
@require_POST
@permissions.require_json_keys(['project_name'])
def basket_clone(request, basket_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(request.user):
        return JsonResponse({
            "code": 'PROJECT_CLONE_PERMISSION',
            "message": 'You are now allowed to clone this basket because you'
                       ' do not have access to it',
            "moreInfo": None
        }, status=403)

    new_project_name = json.loads(request.body.decode('utf-8'))['project_name']
    new_basket = basket.clone(new_project_name, owner=request.user)

    detail_url = reverse('b3_user_panel:project-detail', args=(new_basket.id,))
    return JsonResponse({'success': True, 'redirect_url': detail_url})


@permissions.require_ajax
@require_POST
def basket_line_clone(request, basket_id, line_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise PermissionDenied

    line = get_object_or_404(Line, id=line_id, basket=basket)
    new_line = line.clone()
    return JsonResponse({'success': True, 'pk': new_line.pk})


@permissions.require_ajax
@require_POST
def request_manual_price(request, basket_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise PermissionDenied
    basket.set_for_manual_pricing()
    # Send email
    try:
        current_organization = get_current_org()
        current_organization_base_url = org_utils.get_base_url()

        if current_organization.is_yoda_active:
            base_url = (
                f'{current_organization_base_url}'
                f'{settings.SERVICE_PANEL_PATH}'
            )
            basket_url = f'{base_url}projects/{basket_id}'
        else:
            basket_url = f'{current_organization_base_url}{basket.url}'

        context = {
            'basket_url': basket_url,
            'basket_quotation': basket.quotation_number,
        }

        emails = [user.email for user in basket.first_line.partner.users.all()]

        email_sender = EmailSender()
        email_sender.send_email(
            to=emails,
            subject_template_path='customer/emails/manual_price/'
                                  'email_manual_requested_subject.txt',
            body_txt_template_path='customer/emails/manual_price/'
                                   'email_manual_requested_body.txt',
            body_html_template_path='customer/emails/manual_price/'
                                    'email_manual_requested_body.html',
            extra_context=context
        )

    except Exception:
        logger.exception(
            'Error during sending manual pricing email to pricer user and ps')

    return JsonResponse({'success': True})


@user_permissions.check_user_permission
@permissions.require_ajax
@require_POST
def set_prices(request, basket_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_price(request.user):
        raise PermissionDenied

    # Set prices
    for line_id, price_excl_tax_str in json.loads(
        request.body.decode('utf-8')
    ).items():
        # TODO may fail, need to be tested better
        line = basket.lines.get(id=line_id)
        if not line.is_manual_pricing_required:
            continue
        try:
            price_excl_tax = Decimal(price_excl_tax_str)
            if price_excl_tax < 0:
                raise ValueError('Cannot set a negative price')
            # TODO test if price_excl_tax has an appropriate value
            line.set_manual_price(price_excl_tax)
        except ValueError:
            warning = (
                f'Could not manually set a price of {price_excl_tax_str}'
                f'for line #{line_id} '
                f'(basket #{basket.id}) '
                f'by user {request.user.email}.'
            )
            logger.warning(warning)

    return JsonResponse({'success': True})


@user_permissions.check_user_permission
@permissions.require_ajax
@require_POST
def set_basket_as_priced(request, basket_id):
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_price(request.user):
        raise PermissionDenied

    try:
        basket.set_as_manually_priced()
    except NotAllLinesPricedException:
        return JsonResponse({
            'success': False,
            'message': 'Not all lines are priced yet.'
        })
    return JsonResponse({'success': True})


@permissions.require_ajax
@require_GET
def basket_permission(request, basket_id):
    """
    GET resource to get permissions of a basket.
    :param request: Request parameter given through middleware django.
        It has a lot of information about the user
    sending the request and the request itself.
    :param basket_id: Id of the basket where the permission should be get.
    :return: Array of dictionaries containing user information.
    """
    basket = get_object_or_404(Basket, pk=basket_id)
    if not basket.can_user_view(user=request.user, request=request):
        raise PermissionDenied

    contact_info = ContactInformationFactory. \
        get_contact_information_for_basket(basket)
    return JsonResponse(contact_info, safe=False)


class BasketUsersView(AjaxResponseMixin, View):
    """
    View which gives control to the permissions.
    This is use as class based view because of seperation of concerns.
    """
    http_method_names = ['get', 'post', 'delete']

    def get_ajax(self, request, basket_id, *args, **kwargs):
        basket = get_object_or_404(Basket, pk=basket_id)
        return JsonResponse(
            generate_basket_user_permissions(basket),
            safe=False
        )

    def post_ajax(self, request, basket_id, *args, **kwargs):
        basket = get_object_or_404(Basket, pk=basket_id)
        body_json = json.loads(request.body.decode('utf-8'))
        email = body_json['email'].strip()
        user_check = self.get_user_to_check(email, basket, request.user)
        if user_check['error']:
            return JsonResponse(
                user_check['error'],
                safe=False,
                content_type="application/json")

        user_to_add = user_check['user']
        assign_perm(Basket.VIEW_PERMISSION, user_to_add, basket)
        assign_perm(Basket.EDIT_PERMISSION, user_to_add, basket)
        assign_perm(Basket.ORDER_PERMISSION, user_to_add, basket)

        contact_information = \
            ContactInformationFactory.get_contact_information_for_user(
                user_to_add)
        contact_data = contact_information['contact']
        data = {
            'success': True,
            'name': str(
                user_to_add.userprofile),
            'email': user_to_add.email,
            'role': 'viewer',
            'permission': {
                'can_view_project': user_to_add.has_perm(
                    Basket.VIEW_PERMISSION,
                    basket),
                'can_edit_project': user_to_add.has_perm(
                    Basket.EDIT_PERMISSION,
                    basket),
                'can_share_project': user_to_add.has_perm(
                    Basket.SHARE_PERMISSION,
                    basket),
                'can_price_project': basket.can_user_price(user_to_add),
                'can_order_project': user_to_add.has_perm(
                    Basket.ORDER_PERMISSION,
                    basket),
            },
            'contact': contact_data}
        self.send_email_for_viewing_permission(basket, user_to_add)
        return JsonResponse(data, safe=False, content_type="application/json")

    def delete_ajax(self, request, basket_id, *args, **kwargs):
        body_json = json.loads(request.body.decode('utf-8'))
        email = body_json['email']

        first_partner = Partner.objects.filter(email=email).first()
        if first_partner is not None and email == first_partner.email:
            data = self.create_error_response(
                _(('It is not possible to revoke access '
                   'from a printing service.')))
            return JsonResponse(
                data, safe=False, content_type="application/json")

        user_to_delete = User.objects.filter(email=email).first()
        if user_to_delete is None:
            data = self.create_error_response(_('User not found.'))
            return JsonResponse(
                data, safe=False, content_type="application/json")
        basket = get_object_or_404(Basket, pk=basket_id)
        if user_to_delete == basket.owner:
            data = self.create_error_response(
                _(('Project owner is not able to delete '
                   'himself from a project.')))
            return JsonResponse(
                data, safe=False, content_type="application/json")
        remove_perm(Basket.VIEW_PERMISSION, user_to_delete, basket)
        remove_perm(Basket.EDIT_PERMISSION, user_to_delete, basket)
        remove_perm(Basket.SHARE_PERMISSION, user_to_delete, basket)
        remove_perm(Basket.PRICING_PERMISSION, user_to_delete, basket)
        remove_perm(Basket.ORDER_PERMISSION, user_to_delete, basket)
        data = {
            'success': True,
            'message': None
        }
        return JsonResponse(data, safe=False, content_type="application/json")

    @staticmethod
    def handle_invalid_project_share_attempt(error_message=None):
        if not error_message:
            error_message = 'The current basket cannot be shared. ' \
                            'Please try later.'
        return {
            'user': None,
            'error': BasketUsersView.create_error_response(_(error_message))
        }

    def get_user_to_check(self, email, basket, share_user):
        """
        Method to check if the payload was valid and to check for other
        errors because of the business process.
        :param email: email of the user which should be added.
        :param basket: Basket the user should be added to.
        :param share_user: User who want to share the basket.
        :return: A dictionary containing the user which should be added under
        the key 'user' and an error which has the key 'error'. Just one key
        can be given in the dictionary. The other one will be None.
        """
        if email == '':
            return self.handle_invalid_project_share_attempt(
                error_message='Please insert an email of the user you want to '
                              'grant permission'
            )

        if not basket.owner:
            return self.handle_invalid_project_share_attempt()

        user_to_add = User.objects.filter(
            userprofile__site=basket.owner.userprofile.site, email=email
        ).first()
        if not user_to_add:
            return self.handle_invalid_project_share_attempt()

        error_data = BasketUsersView.check_permission_error(
            share_user, basket, user_to_add
        )

        return {
            'user': None if error_data else user_to_add,
            'error': error_data
        }

    @staticmethod
    def check_permission_error(user, basket, user_to_add):
        """
        Method which will return any error which
            occurs when adding permissions.
        :param user: User which wants to make the changes.
        :param basket: Basket which should be changed.
        :param user_to_add: User which should get added
            the permission to share.
        :return: A string containing the error or a None instance.
        """
        if user_to_add is None:
            return BasketUsersView.create_error_response(
                _('The user was not found in the system. '
                    'The account needs to be created before '
                    '3D Projects can be shared with it.')
            )
        if not user.has_perm(Basket.SHARE_PERMISSION, basket):
            return BasketUsersView.create_error_response(
                _('You do not have permission to share this basket.'))
        if user_to_add == basket.owner:
            return BasketUsersView.create_error_response(
                _('You are already the project owner.'))
        if user_to_add.userprofile.site != basket.owner.userprofile.site:
            return BasketUsersView.create_error_response(
                _('The user is active on a different platform. '
                    'Please ask them to register on your site.'))
        if user_to_add.has_perm(Basket.VIEW_PERMISSION, basket):
            return BasketUsersView.create_error_response(
                _('The user already has access to that 3D Project.'))
        return None

    @staticmethod
    def create_error_response(message):
        """
        Method to create an error message dictionary which
            will count as a response. Method is created to reduce
        boilerplate code of creating dictionaries.
        :param message: Message which should be appended to error dictionary.
        :return: A dictionary containing success false and the error message.
        """
        return {
            'success': False,
            'message': str(message),
        }

    @staticmethod
    def send_email_for_viewing_permission(basket, user_to_add):
        try:
            base_url = org_utils.get_base_url()
            context = {
                "basket_title": basket.title,
                "basket_owner": basket.owner.userprofile.full_name,
                "basket_url": f'{base_url}{basket.url}'
            }
            email_sender = EmailSender()
            email_sender.send_email(
                to=user_to_add.email,
                subject_template_path='customer/emails/permissions/'
                                      'email_permission_'
                                      'notification_subject.txt',
                body_txt_template_path='customer/emails/permissions/'
                                       'email_permission_'
                                       'notification_body.txt',
                body_html_template_path='customer/emails/permissions/'
                                        'email_permission_'
                                        'notification_body.html',
                extra_context=context
            )

        except Exception:
            logger.exception(
                ('Error sending email to user to notify '
                 'him about viewing permission.'))


class BasketAttachmentAjaxView(AbstractAttachmentAjaxView):
    attachment_class = BasketAttachment

    def post(self, request, *args, **kwargs):
        basket_id = request.POST.get('basket_id', None)
        if basket_id == 'None' or basket_id == 'null' or not basket_id:
            return JsonResponse({
                'success': False,
                'message': "basket_id must be set",
            })

        self.check_user_allowed_to_post(request)

        basket = get_object_or_404(Basket, pk=request.POST['basket_id'])

        attachments = []

        try:
            line = basket.lines.get(pk=request.POST['basket_line_id'])
        except BaseException:
            line = None

        for f, val in request.FILES.items():
            attachment_model = self.attachment_class.objects.create(
                file=val,
                filename=val.name,
                filesize=val.size,
                uploader=request.user
                if request.user.is_authenticated else None,
                basket=basket,
                basket_line=line)
            attachments.append(attachment_model.to_dict())

        return JsonResponse({'success': True, 'attachments': attachments})

    def check_user_allowed_to_post(self, request):
        basket = get_object_or_404(Basket, pk=request.POST['basket_id'])
        if not basket.can_user_view(user=request.user, request=request):
            raise PermissionDenied

    def check_user_allowed_to_get(self, request, attachment):
        if not attachment.basket.can_user_view(
            user=request.user, request=request
        ):
            raise PermissionDenied

    def check_user_allowed_to_delete(self, request, attachment):
        self.check_user_allowed_to_get(request, attachment)


class BasketCommentsView(AjaxResponseMixin, View):
    """
    Basket comment view.
    This view will control the basket comments. It has methods to add, edit,
    delete comments. It is also possible to get all comments. See API spec in
    documentation to get more information.
    """
    http_method_names = ['get', 'put', 'post', 'delete']

    def get_ajax(self, request, basket_id, *args, **kwargs):
        """
        GET HTTP Call to get all comments of the current project.
        :param request: Request instance provided by django.
        :param basket_id: Id of the project.
        :param args: Additional arguments.
        :param kwargs: Additional arguments.
        :return: A HttpResponse or JsonResponse depending on the type of
        response. Lookup API specification inside the wiki.
        """
        error = BasketCommentsView.get_user_access_error(
            basket_id=basket_id,
            user=request.user
        )
        if error:
            return JsonResponse(error, status=401)

        # Get all comments of basket.
        basket_comments = Comment.objects.filter(
            basket_id=basket_id
        )
        # Transform queryset to dictionary list of comments.
        comments = [comment.to_dict() for comment in basket_comments]

        return JsonResponse(
            {'value': comments},
            safe=False,
            content_type="application/json"
        )

    def put_ajax(self, request, basket_id, *args, **kwargs):
        """
        PUT HTTP Method to add a comment.
        :param request: Request instance provided by django.
        :param basket_id: Id of the project.
        :param args: Additional arguments.
        :param kwargs: Additional arguments.
        :return: A JsonResponse with different status codes for errors or no
        errors. If the addition was successful it will return 201 and the new
        uuid. This is done since the frontend should know about the other data
        already.
        """
        error = BasketCommentsView.get_user_access_error(
            basket_id=basket_id,
            user=request.user
        )
        if error:
            return JsonResponse(error, status=401)

        basket_reference = Basket.objects.filter(id=basket_id).first()
        comment_user = User.objects.filter(id=request.user.id).first()
        comment = Comment(user=comment_user, basket=basket_reference)

        body_json = json.loads(request.body.decode('utf-8'))
        validation_response = BasketCommentsView.validate_payload(body_json)
        if not validation_response['is_valid']:
            return JsonResponse(
                validation_response['error'],
                status=[validation_response['status']]
            )
        comment.text = body_json['text']
        parsed_parent = body_json['parent']
        parent_comment = BasketCommentsView.get_parent(parsed_parent)
        if parent_comment:
            comment.parent = parent_comment
        comment.save()

        self.send_email_notification(
            basket_id=basket_id,
            user=request.user,
            comment=comment
        )

        return JsonResponse(
            {'id': comment.pretty_uuid},
            status=201,
            safe=False,
            content_type="application/json"
        )

    def post_ajax(self, request, basket_id, *args, **kwargs):
        """
        HTTP POST method to update a comment.
        :param request: Request instance provided by django.
        :param basket_id: Id of the project.
        :param args: Additional arguments.
        :param kwargs: Additional arguments.
        :return: A JsonResponse which will either give back an error based on
        the error structure defined in the documentation. If the edit was
        successful this method will return a HttpResponse with status code
        200.
        """
        error = BasketCommentsView.get_user_access_error(
            basket_id=basket_id,
            user=request.user
        )
        if error:
            return JsonResponse(error, status=401)

        body_json = json.loads(request.body.decode('utf-8'))

        validation_response = BasketCommentsView.validate_payload(body_json)
        if not validation_response['is_valid']:
            return JsonResponse(
                validation_response['error'],
                status=[validation_response['status']]
            )

        comment_to_edit = BasketCommentsView.get_comment(
            uuid=body_json['id'],
            user_id=request.user.id,
            basket_id=basket_id
        )
        if comment_to_edit:
            comment_to_edit.text = body_json['text']
            comment_to_edit.edited = timezone.now()
            comment_to_edit.save()
            return HttpResponse(status=200)

        error_dict = {
            'code': 'COMMENTS_UNDEFINED',
            'message': 'No error message defined.',
            'moreInfo': None,
        }

        return JsonResponse(error_dict, status=404)

    def delete_ajax(self, request, basket_id, *args, **kwargs):
        """
        HTTP DELETE method to delete comments.
        :param request: Request instance provided by django.
        :param basket_id: Id of the project.
        :param args: Additional arguments.
        :param kwargs: Additional arguments.
        :return: A JsonResponse if an error appears. Based on the error
        specification in the documentation the caller will get an error if the
        call failed. Otherwise this method will return a HttpResponse with
        status code 204.
        """
        error = BasketCommentsView.get_user_access_error(
            basket_id=basket_id,
            user=request.user
        )
        if error:
            return JsonResponse(error, status=401)

        comment_to_delete = BasketCommentsView.get_comment(
            uuid=request.GET.get('id', None),
            user_id=request.user.id,
            basket_id=basket_id
        )
        if comment_to_delete:
            comment_to_delete.delete()
            return HttpResponse(status=204)

        error_dict = {
            'code': 'COMMENTS_DELETE_ERROR',
            'message': 'Comment could not be deleted because it was not'
                       'available.',
            'moreInfo': None,
        }
        return JsonResponse(error_dict, status=404)

    @staticmethod
    def get_user_access_error(basket_id, user):
        """
        Method to check if the user is able to modify, add comments.
        This method is written to have a central method to catch errors.
        :param basket_id: Id of the project which should be checking the
        user's eligibility to see comments.
        :param user: User which sent the request and will be checked for
        eligibility to see comments.
        :return: An error dictionary or None.
        """
        basket = Basket.objects.filter(id=basket_id).first()
        if not basket:
            error_dict = {
                'code': 'COMMENTS_PROJECT_NOT_EXISTING',
                'message': 'This project is not existing anymore.',
                'moreInfo': None,
            }
            return error_dict
        if not basket.can_user_view(user):
            error_dict = {
                'code': 'COMMENTS_NOT_AUTHORIZED',
                'message': 'You are not able to modify or add comments.',
                'moreInfo': None,
            }
            return error_dict
        return None

    @staticmethod
    def convert_to_uuid(text):
        """
        Simple helper method to convert the prefixed comment specific UUID to
        a normal UUID.
        :param text: UUID with the prefix 'comments-'.
        :return: A simple UUID instance.
        """
        return UUID(text.replace('comments-', ''))

    @staticmethod
    def get_parent(parsed_parent):
        if parsed_parent:
            parent_comment = Comment.objects.filter(
                uuid=BasketCommentsView.convert_to_uuid(parsed_parent)
            ).first()
            return parent_comment
        return None

    @staticmethod
    def get_comment(uuid, user_id, basket_id):
        uuid_to_delete = BasketCommentsView.convert_to_uuid(uuid)
        filtered_comment = Comment.objects.filter(
            user_id=user_id,
            uuid=uuid_to_delete,
            basket_id=basket_id,
        ).first()
        return filtered_comment

    @staticmethod
    def get_all_users_with_access(basket_id, user_id):
        basket = Basket.all_objects.get(id=basket_id)
        users_with_access = basket.get_users_with_access()
        comment_poster = User.objects.filter(id=user_id).first()
        users_with_access = [
            user for user in users_with_access if
            user.email != comment_poster.email and not user.is_superuser]
        emails = [user.email for user in users_with_access]
        return emails

    @staticmethod
    def create_context_for_email(comment, basket_id, user):
        basket = Basket.all_objects.get(id=basket_id)
        base_url = org_utils.get_base_url()
        context = {
            'basket_title': basket.title,
            'comment_user': user.userprofile.full_name,
            'comment': comment.text,
            'basket_url': f'{base_url}{basket.url}'
        }
        return context

    @staticmethod
    def send_email_notification(basket_id, user, comment):
        email_receivers = BasketCommentsView.get_all_users_with_access(
            basket_id=basket_id,
            user_id=user.id
        )
        for email_receiver in email_receivers:
            context = BasketCommentsView.create_context_for_email(
                comment=comment,
                basket_id=basket_id,
                user=user
            )

            email_sender = EmailSender()
            email_sender.send_email(
                to=email_receiver,
                subject_template_path='customer/emails/comments/'
                                      'email_comment_notification_subject.txt',
                body_txt_template_path='customer/emails/comments/'
                                       'email_comment_notification_body.txt',
                body_html_template_path='customer/emails/comments/'
                                        'email_comment_notification_body.html',
                extra_context=context
            )

    @staticmethod
    def validate_payload(payload):
        if payload['text'] == '':
            response = {
                'is_valid': False,
                'error': {
                    'code': 'COMMENTS_NO_PAYLOAD_TEXT',
                    'message': 'You need to have a text in payload to add a '
                               'comment.',
                    'more_info': None,
                },
                'status': 400,
            }
            return response
        return {
            'is_valid': True,
            'error': None,
            'more_info': None,
        }
