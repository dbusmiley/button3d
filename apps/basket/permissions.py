from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.shortcuts import get_object_or_404, redirect

from apps.basket.models import Basket


def get_basket_access(function):
    """This function can works currently if the url associated
    to the view contains a named parameter called project_id.
    And the view must accept as second parameter the basket.
    """

    def _check_basket_access(request, *args, **kwargs):
        basket_id = kwargs.get('project_id', None)

        basket = get_object_or_404(Basket, pk=basket_id)
        if not basket.can_user_view(user=request.user, request=request):
            if not request.user.is_authenticated:
                account_login = reverse('account_login')
                basket_url = basket.url
                return redirect(f'{account_login}?next={basket_url}')
            else:
                raise PermissionDenied
        return function(request, basket, *args, **kwargs)

    return _check_basket_access
