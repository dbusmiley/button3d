from django.contrib import admin

from apps.basket.models.basket import Basket, Line
from apps.b3_core.admin import DeletedDateAdmin


class LineInline(admin.TabularInline):
    model = Line
    raw_id_fields = [
        'color', 'configuration', 'stl_file', 'stockrecord', 'product',
        'basket',
    ]
    readonly_fields = (
        'line_reference', 'price_excl_tax', 'price_incl_tax', 'price_currency',
        'stockrecord', 'product', 'stl_file',
    )


class LineAdmin(DeletedDateAdmin):
    def get_queryset(self, request):
        return Line.all_objects.all()

    def post_processings(self, instance):
        return ', '.join([
            f'{option.post_processing.title}' +
            (
                f' (Color: {option.color.title}[{option.color.rgb}])'
                if option.color else ''
            )
            for option in instance.post_processing_options.all()
        ])

    raw_id_fields = [
        'color', 'configuration', 'stl_file', 'stockrecord', 'product',
        'basket',
    ]
    list_display = (
        'id', 'basket', 'product', 'partner', 'post_processings',  'quantity',
        'price_excl_tax', 'price_currency', 'date_created'
    )
    readonly_fields = (
        'stl_file', 'partner', 'product', 'post_processings',
        'price_currency', 'price_incl_tax', 'price_excl_tax', 'quantity',
        'configuration', 'basket', 'line_reference',
        'is_manual_pricing_required'
    )

    exclude = ['stockrecord', 'color', 'basket']


class BasketAdmin(DeletedDateAdmin):
    def get_queryset(self, request):
        return Basket.all_objects.all()

    list_display = ('id', 'site', 'owner', 'status', 'num_lines',
                    'contains_a_voucher', 'date_created', 'date_submitted',
                    'time_before_submit')
    readonly_fields = ('date_merged', 'date_submitted',)
    inlines = [LineInline]
    search_fields = [
        'id',
        'site__name',
        'owner__first_name',
        'owner__last_name',
        'owner__username'
    ]


admin.site.register(Basket, BasketAdmin)
admin.site.register(Line, LineAdmin)
