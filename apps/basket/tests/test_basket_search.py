# -*- coding: utf-8 -*-
from datetime import timedelta

from django.urls import reverse
from django.utils import timezone

from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import UserFactory, \
    StlFileFactory, StockRecordFactory, PartnerFactory, \
    BasketLineFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase, \
    AuthenticatedAjaxTestCase
from apps.basket.models import Basket


class BasketSearchTest(TestCase):
    def setUp(self):
        super(BasketSearchTest, self).setUp()
        self.user = UserFactory()

    def test_search_by_name(self):
        b1 = BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            title='Lorem ipsum')
        b2 = BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            title='Lorem ipsum dolor')

        BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            title='Test')

        baskets, nb = Basket.search(owner=self.user, query='ipsum')
        self.assertEqual(nb, 2)
        for basket in [b1, b2]:
            self.assertIn(basket, baskets)

        # this used to have query=123 (numerical) which happened to be the
        # ID of one of the baskets on my machine. So let's not do that again.
        baskets, nb = Basket.search(owner=self.user, query='bla bla di bla')
        self.assertEqual(nb, 0)
        self.assertEqual(len(baskets), nb)

    def test_search_all_baskets(self):
        b1 = BasketFactory(owner=self.user, status=Basket.SAVED)
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b4 = BasketFactory(owner=self.user, status=Basket.SUBMITTED)

        baskets, nb = Basket.search(owner=self.user)
        # Does not return empty OPEN basket
        self.assertEqual(nb, 4)
        for basket in [b1, b2, b3, b4]:
            self.assertIn(basket, baskets)

    def test_search_open_baskets(self):
        b1 = BasketFactory(owner=self.user, status=Basket.SAVED)
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)

        BasketFactory(owner=self.user, status=Basket.SUBMITTED)

        baskets, nb = Basket.search(owner=self.user, status='open')
        # Does not return empty OPEN basket
        self.assertEqual(nb, 3)
        for basket in [b1, b2, b3]:
            self.assertIn(basket, baskets)

    def test_search_not_return_empty_open_basket(self):
        b1 = BasketFactory(owner=self.user, status=Basket.OPEN)

        baskets, nb = Basket.search(owner=self.user)
        self.assertEqual(nb, 0)
        self.assertEqual(len(baskets), 0)

        # Add line
        b1.add_empty_line(StlFileFactory())
        baskets, nb = Basket.search(owner=self.user)
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], b1)

    def test_search_start_date_baskets(self):
        b1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b1.date_created -= timedelta(days=1)
        b1.save()
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)

        now = timezone.now()
        baskets, nb = Basket.search(
            owner=self.user, date_0=now.strftime('%Y-%m-%d'))
        self.assertEqual(nb, 1)
        self.assertEqual(len(baskets), nb)
        self.assertEqual(baskets[0], b2)

    def test_search_end_date_baskets(self):
        b1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b2.date_created += timedelta(days=2)
        b2.save()

        now = timezone.now()
        baskets, nb = Basket.search(owner=self.user, date_1=(
            now + timedelta(days=1)).strftime('%Y-%m-%d'))
        self.assertEqual(nb, 1)
        self.assertEqual(len(baskets), nb)
        self.assertEqual(baskets[0], b1)

    def test_search_range_date_baskets(self):
        b1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b1.date_created -= timedelta(days=1)
        b1.save()
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b4 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b4.date_created += timedelta(days=2)
        b4.save()

        now = timezone.now()
        baskets, nb = Basket.search(owner=self.user, date_0=now.strftime(
            '%Y-%m-%d'), date_1=(now + timedelta(days=1)).strftime('%Y-%m-%d'))
        self.assertEqual(nb, 2)
        self.assertEqual(len(baskets), nb)
        for basket in [b2, b3]:
            self.assertIn(basket, baskets)

    def test_search_pagination_baskets(self):
        created_baskets = []
        for i in range(9):
            created_baskets.append(
                BasketFactory(owner=self.user, status=Basket.EDITABLE)
            )

        baskets1, nb = Basket.search(owner=self.user, items_per_page=3)
        self.assertEqual(nb, 9)
        self.assertEqual(len(baskets1), 3)

        baskets2, nb = Basket.search(owner=self.user, items_per_page=3, page=2)
        self.assertEqual(nb, 9)
        self.assertEqual(len(baskets2), 3)

        baskets3, nb = Basket.search(owner=self.user, items_per_page=3, page=3)
        self.assertEqual(nb, 9)
        self.assertEqual(len(baskets3), 3)

        baskets4, nb = Basket.search(owner=self.user, items_per_page=3, page=4)
        self.assertEqual(nb, 9)
        self.assertEqual(len(baskets4), 0)

        baskets = baskets1 + baskets2 + baskets3
        for basket in baskets:
            self.assertIn(basket, created_baskets)

        for basket in created_baskets:
            self.assertIn(basket, baskets)

    def test_search_invalid_date(self):
        with self.assertRaises(ValueError):
            Basket.search(owner=self.user, date_0='invalid')

        with self.assertRaises(ValueError):
            Basket.search(owner=self.user, date_1='invalid')

    def test_search_invalid_nb_items(self):
        with self.assertRaises(ValueError):
            Basket.search(owner=self.user, items_per_page='invalid')

    def test_search_invalid_page_nb(self):
        with self.assertRaises(ValueError):
            Basket.search(owner=self.user, items_per_page='2', page='invalid')

    def test_search_for_reference(self):
        BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            reference='this is a test')
        b2 = BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            reference='this lorem ipsum')

        baskets, nb = Basket.search(owner=self.user, query='lorem')
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], b2)

    def test_search_for_quotation_number(self):
        BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            quotation_number='000123',
            title='test')
        b2 = BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            quotation_number='000678',
            title='test')

        baskets, nb = Basket.search(owner=self.user, query='678')
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], b2)

    def test_search_for_line_name(self):
        stlfile = StlFileFactory()
        b1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        line1 = b1.add_empty_line(stlfile)
        line2 = b2.add_empty_line(stlfile)
        line1.name = 'Test line'
        line1.save()
        line2.name = 'Lorem test line'
        line2.save()

        baskets, nb = Basket.search(owner=self.user, query='lorem')
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], b2)

    def test_search_for_line_stlfile_name(self):
        file1 = StlFileFactory(showname='Stlfile')
        file2 = StlFileFactory(showname='Lorem file')
        b1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        b2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        line1 = b1.add_empty_line(file1)
        line2 = b2.add_empty_line(file2)
        line1.name = 'Test line'
        line1.save()
        line2.name = 'Test line'
        line2.save()

        baskets, nb = Basket.search(owner=self.user, query='lorem')
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], b2)

    def test_search_line_without_name(self):
        b = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        line = b.add_empty_line(StlFileFactory())
        line.name = None
        line.save()

        baskets, nb = Basket.search(owner=self.user, query='test')
        self.assertEqual(nb, 0)

    def test_search_basket_without_name(self):
        BasketFactory(owner=self.user, status=Basket.EDITABLE, title=None)

        baskets, nb = Basket.search(owner=self.user, query='test')
        self.assertEqual(nb, 0)

    def test_search_unicode_query(self):
        BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE,
            title='ùnicöde'
        )

        baskets, nb = Basket.search(owner=self.user, query='de')
        self.assertEqual(nb, 1)

        baskets, nb = Basket.search(owner=self.user, query='ùni')
        self.assertEqual(nb, 1)


class BasketSearchAjaxCallsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(BasketSearchAjaxCallsTest, self).setUp()
        self.basket = BasketFactory(owner=self.user)

    def test_search_empty_open_basket(self):
        self.basket.status = Basket.OPEN
        self.basket.save()
        self.assertEqual(self.basket.num_items, 0)

        url = reverse('basket-ajax:search')
        data = {
            'status': 'open'
        }
        # Empty open basket doesn't appear
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 0)
        self.assertEqual(len(json_response['list']), 0)
        # And also doesn't appear when all asked
        json_response = self.client.get(url, {})
        self.assertEqual(json_response['num_results'], 0)
        self.assertEqual(len(json_response['list']), 0)

    def test_baskets_search(self):
        url = reverse('basket-ajax:search')
        # Make open basket non empty (so it appears in the retrieved baskets)
        stlfile = StlFileFactory()
        self.basket.add_empty_line(stlfile)

        # Create test baskets
        b_froz1 = BasketFactory(
            status=Basket.EDITABLE,
            owner=self.user,
            title='Bla')
        b_froz1.date_created -= timedelta(days=14)
        b_froz1.save()
        b_froz2 = BasketFactory(
            status=Basket.EDITABLE,
            owner=self.user,
            title='TestBla')
        b_saved = BasketFactory(status=Basket.SAVED, owner=self.user)
        b_sub = BasketFactory(
            status=Basket.SUBMITTED,
            owner=self.user,
            title='BlaBlaBla')

        baskets = [self.basket, b_saved, b_froz1, b_froz2]
        # Test status
        data = {
            'status': 'open'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 4)
        self.assertEqual(len(json_response['list']), 4)
        project_ids = [
            json_response['list'][i]['project_id'] for i in range(4)
        ]
        for basket in baskets:
            self.assertIn(basket.id, project_ids)

        data = {
            'status': 'submitted'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 1)
        self.assertEqual(len(json_response['list']), 1)
        self.assertEqual(json_response['list'][0]['project_id'], b_sub.id)

        data = {
            'status': 'all'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 5)
        self.assertEqual(len(json_response['list']), 5)

        # Test name
        data = {
            'name': 'Bla',
            'status': 'All'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 3)
        self.assertEqual(len(json_response['list']), 3)
        project_ids = [
            json_response['list'][i]['project_id'] for i in range(3)
        ]
        for basket in [b_sub, b_froz1, b_froz2]:
            self.assertIn(basket.id, project_ids)

        data = {
            'name': 'Bla',
            'status': 'open'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 2)
        self.assertEqual(len(json_response['list']), 2)
        project_ids = [
            json_response['list'][i]['project_id'] for i in range(2)
        ]
        for basket in [b_froz1, b_froz2]:
            self.assertIn(basket.id, project_ids)

        # Test dates
        now = timezone.now()
        data = {
            'date_0': (now - timedelta(days=5)).strftime('%Y-%m-%d'),
            'date_1': now.strftime('%Y-%m-%d'),
            'status': 'open'
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 3)
        self.assertEqual(len(json_response['list']), 3)
        project_ids = [
            json_response['list'][i]['project_id'] for i in range(3)
        ]
        for basket in [b_saved, b_froz2, self.basket]:
            self.assertIn(basket.id, project_ids)

        # Test pagination
        data = {
            'items_per_page': 3
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 5)
        self.assertEqual(len(json_response['list']), 3)
        page_one_project_ids = [
            json_response['list'][i]['project_id'] for i in range(3)
        ]

        data = {
            'items_per_page': 3,
            'page': 2
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 5)
        self.assertEqual(len(json_response['list']), 2)
        page_two_project_ids = [
            json_response['list'][i]['project_id'] for i in range(2)
        ]

        self.assertEqual(
            sorted(
                page_one_project_ids + page_two_project_ids
            ),
            sorted(
                [self.basket.id, b_saved.id, b_sub.id, b_froz1.id, b_froz2.id]
            )
        )

        data = {
            'items_per_page': 3,
            'page': 3
        }
        json_response = self.client.get(url, data)
        self.assertEqual(json_response['num_results'], 5)
        self.assertEqual(len(json_response['list']), 0)

    def test_returned_search_content(self):
        stlfile = StlFileFactory()
        sr = StockRecordFactory()
        line, _ = self.basket.add_product(
            stlfile, product=sr.product, stockrecord=sr)

        url = reverse('basket-ajax:search')
        json_response = self.client.get(url)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['num_results'], 1)
        self.assertEqual(len(json_response['list']), 1)

        r_basket = json_response['list'][0]
        self.assertEqual(r_basket['project_id'], self.basket.id)
        self.assertEqual(r_basket['title'], self.basket.title)
        self.assertEqual(r_basket['url'], self.basket.url)
        self.assertEqual(r_basket['nb_lines'], 1)
        self.assertIsNotNone(r_basket['price'])
        self.assertIsNotNone(r_basket['price']['excl_tax'])
        self.assertEqual(r_basket['price']['excl_tax'], '239.07')
        self.assertEqual(r_basket['price']['incl_tax'], '284.49')
        self.assertFalse(r_basket['is_ordered'])
        self.assertEqual(
            r_basket['quotation_number'], str(
                self.basket.quotation_number))

        self.assertEqual(len(r_basket['lines']), 1)
        self.assertEqual(
            r_basket['lines'][0]['thumbnail_url'],
            line.thumbnail_url)


class BasketSearchByPriceTest(TestCase):
    def setUp(self):
        super(BasketSearchByPriceTest, self).setUp()
        self.user = UserFactory()

        self.b = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.partner = PartnerFactory()
        sr = StockRecordFactory(partner=self.partner)
        ShippingMethodFactory(
            partner=self.partner,
            shipping_days_min=3,
            price_excl_tax=15
        )
        ShippingMethodFactory(
            partner=self.partner,
            shipping_days_min=1,
            price_excl_tax=5
        )
        self.line = BasketLineFactory(
            basket=self.b,
            stockrecord=sr
        )
        self.b.refresh_price()
        self.b.refresh_from_db()
        self.prices = self.b.price_object_with_shipping(None)

    def test_search_for_exact_excl_price(self):
        baskets, nb = Basket.search(
            owner=self.user, query=self.prices.excl_tax)
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_int_excl_price(self):
        baskets, nb = Basket.search(
            owner=self.user, query=int(
                self.prices.excl_tax))
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_str_int_excl_price(self):
        baskets, nb = Basket.search(
            owner=self.user, query=str(int(self.prices.excl_tax)))
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_int_incl_price(self):
        baskets, nb = Basket.search(
            owner=self.user, query=int(
                self.prices.incl_tax))
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_incorrect_price(self):
        baskets, nb = Basket.search(owner=self.user, query='123456')
        self.assertEqual(nb, 0)

    def test_search_for_invalid_price(self):
        baskets, nb = Basket.search(owner=self.user, query='toto')
        self.assertEqual(nb, 0)

    def test_search_for_exact_prices_incoherent_currency(self):
        baskets, nb = Basket.search(
            owner=self.user, query=str(
                self.prices.excl_tax), currency='USD')
        self.assertEqual(nb, 0)
        baskets, nb = Basket.search(
            owner=self.user, query=str(
                self.prices.incl_tax), currency='USD')
        self.assertEqual(nb, 0)

    def test_search_for_exact_excl_price_with_quantity(self):
        self.line.update({'quantity': 3})
        self.b.refresh_price()
        self.prices = self.b.price_object_with_shipping(None)
        baskets, nb = Basket.search(
            owner=self.user, query=self.prices.excl_tax)
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_exact_excl_price_multiple_lines(self):
        sr = StockRecordFactory(partner=self.partner)
        BasketLineFactory(
            basket=self.b,
            stockrecord=sr
        )
        self.b.refresh_price()
        self.prices = self.b.price_object_with_shipping(None)

        baskets, nb = Basket.search(
            owner=self.user, query=self.prices.excl_tax)
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)

    def test_search_for_basket_use_commas(self):
        baskets, nb = Basket.search(
            owner=self.user, query=str(
                self.prices.incl_tax).replace(
                '.', ','))
        self.assertEqual(nb, 1)
        self.assertEqual(baskets[0], self.b)
