import json
from decimal import Decimal

from django.conf import settings
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import override_settings
from django.test.client import MULTIPART_CONTENT
from django.urls import reverse

from apps.b3_core.models import StlFile
from apps.b3_tests.factories import \
    StlFileFactory, StockRecordFactory, ProductFactory, PartnerFactory, \
    ColorFactory, PostProcessingFactory, PartnerExtraFeeFactory,\
    BasketFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.b3_tests.testcases.org_testcases import \
    AuthenticatedAjaxOrganizationTestCase
from apps.basket.models import Basket, Line, BasketAttachment
from apps.partner.conditions import PriceGreaterCondition


class BasketAjaxCallsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(BasketAjaxCallsTest, self).setUp()
        self.basket = BasketFactory(owner=self.user)

    def test_basket_not_found(self):
        url = reverse(
            'basket-ajax:basket-details',
            args=[
                self.basket.pk + 1,
            ])
        response = self.client.get(url, return_json=False)
        self.assertEqual(response.status_code, 404)

    def test_update_basket(self):
        url = reverse('basket-ajax:basket-details', args=[self.basket.pk, ])

        # Update
        data = json.dumps({'title': 'foo', 'reference': 'bar'})
        json_response = self.client.post(url, data)
        self.assertIsInstance(json_response, dict)
        self.assertTrue(json_response['success'])
        b = Basket.objects.get(pk=self.basket.pk)
        self.assertEqual(b.title, 'foo')
        self.assertEqual(b.reference, 'bar')

        # Nothing was updated
        data = json.dumps({})
        json_response = self.client.post(url, data)
        self.assertIsInstance(json_response, dict)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['message'], 'Nothing was changed')

        # Error
        data = json.dumps({'title': ''})
        json_response = self.client.post(url, data)
        self.assertIsInstance(json_response, dict)
        self.assertFalse(json_response['success'])
        self.assertEqual(
            json_response['message'],
            'Basket title cannot be empty')

    def test_delete_basket(self):
        url = reverse('basket-ajax:basket-details', args=[self.basket.pk, ])

        # Delete
        json_response = self.client.delete(url)
        self.assertIsInstance(json_response, dict)
        self.assertTrue(json_response['success'])
        self.assertEqual(
            json_response['redirect_url'],
            reverse('b3_user_panel:projects'))

        # Not found since it is deleted
        response = self.client.delete(url, return_json=False)
        self.assertEqual(response.status_code, 404)

    def test_delete_ordered_basket(self):
        self.basket.submit()
        url = reverse('basket-ajax:basket-details', args=[self.basket.pk, ])
        json_response = self.client.delete(url)
        self.assertFalse(json_response['success'])

    def test_update_basket_line(self):
        stlfile = StlFileFactory()
        line = self.basket.add_empty_line(stlfile)
        self.assertTrue(line.is_empty)
        self.assertFalse(self.basket.all_lines_can_be_ordered())

        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.pk, line.pk]
        )

        # Update both
        partner = PartnerFactory(price_currency="USD")
        product = ProductFactory()
        stockrecord = StockRecordFactory(
            product=product,
            partner=partner
        )

        data = json.dumps({'product_slug': product.slug})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['stl_file'], stlfile.uuid)
        self.assertEqual(json_response['product_slug'], product.slug)

        self.basket.refresh_from_db()
        self.assertFalse(self.basket.all_lines_can_be_ordered())

        data = json.dumps({'supplier_slug': partner.code})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['status'], 'finished')
        self.assertEqual(json_response['product_slug'], product.slug)
        self.assertEqual(json_response['supplier_slug'], partner.code)

        line.refresh_from_db()
        self.basket.refresh_from_db()
        self.assertTrue(self.basket.all_lines_can_be_ordered())
        self.assertEqual(line.stockrecord, stockrecord)
        self.assertFalse(line.is_manual_pricing_required)
        self.assertEqual(line.price_object.as_dict()['currency'], "USD")

    def test_update_basket_line2(self):
        stlfile = StlFileFactory()
        line = self.basket.add_empty_line(stlfile, quantity=2)
        self.assertEqual(line.quantity, 2)
        self.assertFalse(self.basket.all_lines_can_be_ordered())

        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.pk, line.pk]
        )

        product = ProductFactory(title='Test Product')
        data = json.dumps({'product_slug': product.slug})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['status'], 'finished')
        self.assertEqual(json_response['quantity'], 2)
        self.assertEqual(json_response['product_slug'], product.slug)
        line.refresh_from_db()
        self.assertEqual(line.product, product)
        self.assertTrue(line.is_empty)

        partner = PartnerFactory()
        stock_record = StockRecordFactory(
            partner=partner,
            product=product
        )
        data = json.dumps({'supplier_slug': partner.code})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['product_slug'], product.slug)
        self.assertEqual(json_response['supplier_slug'], partner.code)
        line.refresh_from_db()
        self.assertEqual(line.stockrecord, stock_record)
        self.assertFalse(line.is_empty)
        self.assertEqual(line.quantity, 2)

        post_processing = PostProcessingFactory(
            stock_record=stock_record
        )
        post_processing_payload = {
            'post_processings': [{
                'slug': post_processing.full_slug,
                'color_id': None
            }]
        }
        json_response = self.client.post(
            url,
            json.dumps(post_processing_payload)
        )
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['product_slug'], product.slug)
        self.assertEqual(json_response['supplier_slug'], partner.code)
        self.assertEqual(
            json_response['post_processings'],
            post_processing_payload['post_processings']
        )
        line.refresh_from_db()
        self.assertEqual(line.stockrecord, stock_record)
        self.assertEqual(line.product, product)
        self.assertEqual(
            [post_processing],
            line.post_processings
        )
        self.assertFalse(line.is_empty)

        product2 = ProductFactory(title='Test Product 2')
        data = json.dumps({'product_slug': product2.slug})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.product, product2)
        self.assertIsNone(line.stockrecord)
        self.assertTrue(line.is_empty)
        self.assertEqual(line.quantity, 2)

    def test_update_line_colors(self):
        c1 = ColorFactory(rgb='#000000')
        c2 = ColorFactory(rgb='#111111')

        stlfile = StlFileFactory()
        stockrecord = StockRecordFactory()
        post_processing = PostProcessingFactory(
            stock_record=stockrecord,
            colors=[c1, c2]
        )
        line, _ = self.basket.add_product(
            stlfile,
            product=stockrecord.product,
            stockrecord=stockrecord
        )

        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.pk, line.pk]
        )

        data = json.dumps({
            'post_processings': [{
                'slug': post_processing.full_slug,
                'color_id': c1.id
            }]
        })
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.post_processing_options.first().color, c1)

        data = json.dumps({
            'post_processings': [{
                'slug': post_processing.full_slug,
                'color_id': c2.id
            }]
        })
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.post_processing_options.first().color, c2)

    def test_line_invalid_input(self):
        stlfile = StlFileFactory()
        line = self.basket.add_empty_line(stlfile, quantity=2)
        self.assertEqual(line.quantity, 2)
        self.assertFalse(self.basket.all_lines_can_be_ordered())
        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.pk, line.pk]
        )

        # Hacking!
        data = json.dumps({'quantity': 'imma hack yo ass'})
        json_response = self.client.post(url, data)
        self.assertFalse(json_response['success'])

    def test_basket_stockrecord_updating_manual_pricing(self):
        stlfile = StlFileFactory()
        line = self.basket.add_empty_line(stlfile)

        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.pk, line.pk]
        )

        product = ProductFactory(title='Test Product')
        partner = PartnerFactory()
        stockrecord = StockRecordFactory(
            partner=partner,
            product=product,
        )
        color = ColorFactory()
        post_processing1 = PostProcessingFactory(
            stock_record=stockrecord,
            colors=[color],
            always_priced_manually=True
        )
        post_processing2 = PostProcessingFactory(
            stock_record=stockrecord
        )

        # Set product & partner
        data = json.dumps({
            'product_slug': product.slug,
            'supplier_slug': partner.code})
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.product, product)
        self.assertEqual(line.stockrecord, stockrecord)
        self.assertFalse(line.is_manual_pricing_required)

        data = json.dumps({
            'product_slug': product.slug,
            'supplier_slug': partner.code,
            'post_processings': [{
                'slug': post_processing1.full_slug,
                'color_id': color.id
            }]
        })
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.product, product)
        self.assertEqual(line.stockrecord, stockrecord)
        self.assertEqual(
            [(post_processing1, color)],
            [
                (option.post_processing, option.color)
                for option in line.post_processing_options.all()
            ]
        )
        self.assertTrue(line.is_manual_pricing_required)
        self.assertFalse(line.has_manual_price_set)

        data = json.dumps({
            'product_slug': product.slug,
            'supplier_slug': partner.code,
            'post_processings': [{
                'slug': post_processing2.full_slug,
                'color_id': None
            }]
        })
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        line.refresh_from_db()
        self.assertEqual(line.product, product)
        self.assertEqual(line.stockrecord, stockrecord)
        self.assertEqual(
            [(post_processing2, None)],
            [
                (option.post_processing, option.color)
                for option in line.post_processing_options.all()
            ]
        )
        self.assertFalse(line.is_manual_pricing_required)
        self.assertFalse(line.has_manual_price_set)

    def test_get_basket_details(self):
        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))

        json_response = self.client.get(url)
        self.assertEqual(json_response['title'], self.basket.title)
        self.assertEqual(json_response['url'], self.basket.url)
        self.assertEqual(
            json_response['url'],
            reverse(
                'b3_user_panel:project-detail',
                args=(
                    self.basket.id,
                )))
        self.assertEqual(len(json_response['lines']), 0)
        # ...

        # Add files
        line1 = self.basket.add_empty_line(StlFileFactory())
        line2 = self.basket.add_empty_line(StlFileFactory())

        json_response = self.client.get(url)
        self.assertEqual(len(json_response['lines']), 2)
        self.assertEqual(len(json_response['lines'][0]), 1)
        self.assertEqual(json_response['lines'][0]['id'], line1.id)
        self.assertEqual(json_response['lines'][1]['id'], line2.id)

    def test_create_basket_without_name(self):
        url = reverse('basket-ajax:create')

        data = json.dumps({})
        response = self.client.put(url, data, return_json=False)
        self.assertEqual(response.status_code, 400)

    def test_create_basket(self):
        url = reverse('basket-ajax:create')
        data = json.dumps({
            'title': 'TestName'
        })
        json_response = self.client.put(url, data)

        self.assertTrue(json_response['success'])
        self.assertIsNotNone(json_response['id'])
        basket = Basket.objects.get(id=json_response['id'])
        self.assertEqual(basket.title, 'TestName')
        self.assertEqual(json_response['redirect_url'], reverse(
            'b3_user_panel:project-detail', args=(basket.id,)))

    def test_create_basket_anonymous(self):
        url = reverse('basket-ajax:create')

        data = json.dumps({
            'title': 'TestNameAnonymous'
        })
        self.client.logout()
        json_response = self.client.put(url, data)

        self.assertTrue(json_response['success'])
        self.assertIsNotNone(json_response['id'])
        basket = Basket.objects.get(id=json_response['id'])
        self.assertEqual(basket.title, 'TestNameAnonymous')
        self.assertEqual(json_response['redirect_url'], reverse(
            'b3_user_panel:project-detail', args=(basket.id,)))

    def test_add_file_to_basket(self):
        url = reverse('basket-ajax:add-file', args=(self.basket.id,))

        stlfile = StlFileFactory()
        self.assertIsNone(stlfile.owner)
        self.assertEqual(self.basket.num_items, 0)

        data = json.dumps({
            'uuid': stlfile.uuid
        })
        json_response = self.client.put(url, data)
        stlfile = StlFile.objects.get(pk=stlfile.pk)
        self.assertTrue(json_response['success'])
        self.assertEqual(self.basket.num_items, 1)
        self.assertEqual(
            json_response['new_line_id'],
            self.basket.first_line.id)

        self.assertEqual(self.basket.first_line.stl_file.uuid, stlfile.uuid)
        self.assertEqual(stlfile.owner.id, self.user.id)

        json_response = self.client.put(url, data)
        self.assertTrue(json_response['success'])
        self.assertEqual(self.basket.num_items, 2)

    def test_get_basket_line_details(self):
        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()

        line, created = self.basket.add_product(
            stlfile,
            stockrecord.product,
            quantity=2,
            scale=1.2,
            stockrecord=stockrecord
        )

        url = reverse(
            'basket-ajax:basket-line-details',
            args=[self.basket.id, line.id]
        )
        json_response = self.client.get(url)
        self.assertEqual(json_response['stl_file'], stlfile.uuid)
        self.assertEqual(json_response['status'], 'finished')
        self.assertEqual(json_response['showname'], line.name)
        self.assertEqual(
            json_response['stl_file_creation_date'],
            stlfile.creation_date.strftime('%Y-%m-%dT%H:%M:%S'))

        self.assertEqual(json_response['quantity'], 2)
        self.assertEqual(json_response['scale'], 1.2)
        self.assertEqual(json_response['measure_unit'], line.measure_unit)
        self.assertEqual(
            json_response['dimensions']['d'],
            line.dimensions_in_unit['d'])
        self.assertEqual(
            json_response['dimensions']['w'],
            line.dimensions_in_unit['w'])
        self.assertEqual(
            json_response['dimensions']['h'],
            line.dimensions_in_unit['h'])

        self.assertEqual(json_response['configure_url'], line.configure_url)
        self.assertEqual(json_response['thumbnail_url'], line.thumbnail_url)

        self.assertEqual(
            json_response['product_slug'],
            stockrecord.product.slug
        )
        self.assertEqual(
            json_response['supplier_slug'],
            stockrecord.partner.code)
        self.assertEqual(json_response['post_processings'], [])

    @override_settings(DEMO_UUID='abcdef12-0000-0000-1234-000000000001')
    def test_delete_line(self):
        stlfile = StlFileFactory(uuid='abcdef12-0000-0000-1234-000000000001')
        self.assertTrue(stlfile.is_demo)
        line = self.basket.add_empty_line(stlfile)

        stlfile2 = StlFileFactory()
        self.assertFalse(stlfile2.is_demo)
        line2 = self.basket.add_empty_line(stlfile2)
        self.assertTrue(self.basket.num_items, 2)

        url = reverse(
            'basket-ajax:basket-line-details',
            args=(
                self.basket.id,
                line.id))
        json_response = self.client.delete(url)
        self.assertTrue(json_response['success'])
        self.assertEqual(self.basket.num_items, 1)

        url = reverse(
            'basket-ajax:basket-line-details',
            args=(
                self.basket.id,
                line2.id))
        json_response = self.client.delete(url)
        self.assertTrue(json_response['success'])
        self.assertEqual(self.basket.num_items, 0)

        self.assertEqual(Line.objects.count(), 0)
        # TODO this should not work when we have the 1-n relationship
        self.assertIsNone(stlfile.deleted_date)

    def test_clone_basket(self):
        url = reverse('basket-ajax:clone', args=(self.basket.id,))
        response = self.client.post(url, return_json=False)
        self.assertEqual(response.status_code, 400)

        data = json.dumps({'project_name': 'NewName'})
        json_response = self.client.post(url, data)

        self.assertTrue(json_response['success'])
        new_basket = Basket.objects.exclude(id=self.basket.id).get()
        self.assertEqual(json_response['redirect_url'], reverse(
            'b3_user_panel:project-detail', args=(new_basket.id,)))
        self.assertNotEqual(new_basket.id, self.basket.id)
        self.assertEqual(new_basket.title, 'NewName')

    def test_clone_basket_line(self):
        stlfile = StlFileFactory()
        stockrecord = StockRecordFactory()
        line, created = self.basket.add_product(
            stlfile,
            product=stockrecord.product,
            stockrecord=stockrecord)
        self.assertEqual(self.basket.num_items, 1)

        url = reverse('basket-ajax:clone-line', args=(self.basket.id, line.id))
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])
        self.assertNotEqual(json_response['pk'], line.pk)
        new_line = Line.objects.get(pk=json_response['pk'])
        self.assertNotEqual(new_line.name, line.name)
        self.assertEqual(new_line.product, line.product)
        self.assertEqual(new_line.stockrecord, line.stockrecord)
        self.assertEqual(self.basket.num_items, 2)

    def test_basket_none_price(self):
        """Test for correct behaviour if line prices are set to 0.00"""
        stockrecord = StockRecordFactory()

        line, created = self.basket.add_product(
            StlFileFactory(),
            product=stockrecord.product,
            stockrecord=stockrecord)
        self.assertNotEqual(line.price_excl_tax, Decimal(0.00))
        self.assertNotEqual(line.price_incl_tax, Decimal(0.00))
        line.price_excl_tax = Decimal(0.00)
        line.price_incl_tax = Decimal(0.00)
        line.save()
        line.refresh_from_db()

        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))
        json_response = self.client.get(url)
        self.assertEqual(json_response['price']['incl_tax'], '0.00')
        line.refresh_from_db()
        self.assertEqual(line.price_excl_tax, Decimal(0.00))
        self.assertEqual(line.price_incl_tax, Decimal(0.00))

    def test_basket_url_no_line(self):
        self.assertEqual(self.basket.num_lines, 0)

        url = reverse('basket-ajax:basket-url', args=(self.basket.id,))
        response = self.client.get(url, return_json=False)
        self.assertEqual(response.status_code, 400)

    def test_basket_url_one_line_to_project(self):
        self.basket.add_empty_line(StlFileFactory())
        self.assertEqual(self.basket.num_lines, 1)
        self.organization.single_upload_redirects_to_project_detail = True
        self.organization.save()

        url = reverse('basket-ajax:basket-url', args=(self.basket.id,))
        json_response = self.client.get(url)
        self.assertEqual(json_response['url'], self.basket.url)

    def test_basket_url_one_line_to_viewer(self):
        line = self.basket.add_empty_line(StlFileFactory())
        self.assertEqual(self.basket.num_lines, 1)
        self.organization.single_upload_redirects_to_project_detail = False
        self.organization.save()

        url = reverse('basket-ajax:basket-url', args=(self.basket.id,))
        json_response = self.client.get(url)
        self.assertEqual(json_response['url'], line.configure_url)

    def test_basket_url_multiple_lines(self):
        self.basket.add_empty_line(StlFileFactory())
        self.basket.add_empty_line(StlFileFactory())
        self.assertEqual(self.basket.num_lines, 2)

        url = reverse('basket-ajax:basket-url', args=(self.basket.id,))
        json_response = self.client.get(url)
        self.assertEqual(json_response['url'], self.basket.url)

    def test_basket_ajax_api_on_submitted_basket(self):
        stl_file = StlFileFactory(showname='MyStlFile')

        basket = BasketFactory(owner=self.user)
        line = basket.add_empty_line(stl_file, quantity=1)

        # The POST to basket-ajax URL
        basket_ajax_url = reverse(
            'basket-ajax:basket-line-details', args=(basket.id, line.id)
        )

        response = self.client.post(
            basket_ajax_url, data=json.dumps({"scale": 1, "unit": "mm"}),
            return_json=False
        )
        self.assertNotEqual(response.status_code, 400)

        # Now order this basket
        basket.submit()
        response = self.client.post(
            basket_ajax_url, data=json.dumps({"scale": 1, "unit": "mm"}),
            return_json=False
        )
        # This should return a 400
        self.assertEqual(response.status_code, 400)


class BasketAttachmentAjaxCallsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(BasketAttachmentAjaxCallsTest, self).setUp()
        self.basket = BasketFactory(owner=self.user)

        stlfile = StlFileFactory()
        stockrecord = StockRecordFactory()
        self.line, created = self.basket.add_product(
            stlfile,
            product=stockrecord.product,
            stockrecord=stockrecord)
        self.assertEqual(self.basket.num_items, 1)

    def test_upload_basket_attachment(self):
        # Upload a file to the BASKET
        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'attachment.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf')

        data = {
            'attachment': attachment,
            'basket_id': self.basket.pk
        }

        json_response = self.client.post(
            url, data, return_json=True, content_type=MULTIPART_CONTENT)
        self.assertTrue(json_response['success'])
        self.assertEqual(len(json_response['attachments']), 1)
        self.assertEqual(
            json_response['attachments'][0]['name'],
            'attachment.pdf')
        self.assertEqual(
            json_response['attachments'][0]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][0]['basket_id'],
            self.basket.id)
        self.assertIsNone(json_response['attachments'][0]['basket_line_id'])

        attachment = BasketAttachment.objects.get(
            pk=json_response['attachments'][0]['id'])
        self.assertEqual(attachment.filename, 'attachment.pdf')
        self.assertEqual(attachment.uploader, self.user)
        self.assertEqual(attachment.basket, self.basket)
        self.assertIsNone(attachment.basket_line)

        # Upload a file to the Line
        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'attachment.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type="application/pdf")

        data = {
            'attachment': attachment,
            'basket_id': self.basket.pk,
            'basket_line_id': self.line.pk
        }

        json_response = self.client.post(
            url, data, return_json=True, content_type=MULTIPART_CONTENT)
        self.assertTrue(json_response['success'])
        self.assertEqual(len(json_response['attachments']), 1)
        self.assertEqual(
            json_response['attachments'][0]['name'],
            'attachment.pdf')
        self.assertEqual(
            json_response['attachments'][0]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][0]['basket_id'],
            self.basket.id)
        self.assertEqual(
            json_response['attachments'][0]['basket_line_id'],
            self.line.id)

        attachment = BasketAttachment.objects.get(
            pk=json_response['attachments'][0]['id'])
        self.assertEqual(attachment.filename, 'attachment.pdf')
        self.assertEqual(attachment.uploader, self.user)
        self.assertEqual(attachment.basket, self.basket)
        self.assertEqual(attachment.basket_line, self.line)

        # Now check the basket
        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))
        json_response = self.client.get(url)

        self.assertEqual(json_response['title'], self.basket.title)
        self.assertEqual(len(json_response['attachments']), 2)
        self.assertIsNone(json_response['attachments'][0]['basket_line_id'])
        self.assertEqual(
            json_response['attachments'][0]['basket_id'],
            self.basket.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][0]['name'],
            "attachment.pdf")

        self.assertEqual(
            json_response['attachments'][1]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][1]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][1]['basket_id'],
            self.basket.id)
        self.assertEqual(
            json_response['attachments'][1]['name'],
            "attachment.pdf")
        self.assertEqual(
            json_response['attachments'][1]['basket_line_id'],
            self.line.id)

        my_attachment_id = json_response['attachments'][1]['id']

        # And GET a single attachment
        data = {
            'id': my_attachment_id,
            'basket_id': self.basket.pk,
        }

        url = reverse('basket-ajax:basket-attachments')
        json_response = self.client.get(url, data, return_json=True)

        self.assertTrue(json_response['success'])
        self.assertEqual(
            json_response['attachment']['basket_line_id'],
            self.line.pk)

        # Check if basket line is fine
        url = reverse(
            'basket-ajax:basket-line-details',
            args=(
                self.basket.id,
                self.line.pk))
        json_response = self.client.get(url, data, return_json=True)

        self.assertTrue(len(json_response['attachments']), 1)

        # Change the user
        other_user = UserFactory(password=self.test_password)
        self.assertTrue(
            self.client.login(
                email=other_user.email,
                password=self.test_password))

        # And GET a single attachment
        data = {
            'id': my_attachment_id,
            'basket_id': self.basket.pk,
        }

        url = reverse('basket-ajax:basket-attachments')
        json_response = self.client.get(url, data, return_json=True)

        # Permission should be denied
        self.assertFalse(json_response['success'])

        # Change user back (just in case...)
        self.assertTrue(
            self.client.login(
                email=self.user.email,
                password=self.test_password))

        # DELETE Attachment
        basket_attachments_url = reverse('basket-ajax:basket-attachments')
        url = (
            f'{basket_attachments_url}'
            f'?id={my_attachment_id}'
            f'&basket_id={self.basket.pk}'
        )
        json_response = self.client.delete(
            url, return_json=True, content_type="application/json")

        self.assertTrue(json_response['success'])

        # Now check the basket
        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))
        json_response = self.client.get(url)

        self.assertEqual(len(json_response['attachments']), 1)


class BasketPricingAjaxCallsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(BasketPricingAjaxCallsTest, self).setUp()
        self.basket = BasketFactory(owner=self.user)
        # TODO add line to basket that cannot be priced automatically
        self.pricer_user = UserFactory(
            email='pricer@3yourmind.com',
            password=self.test_password)
        self.organization.manual_pricing_user = self.pricer_user
        self.organization.save()

    def test_manual_price_request(self):
        self.assertFalse(self.basket.has_manual_pricing_request_sent)
        self.assertFalse(self.basket.can_user_view(self.pricer_user))
        self.assertFalse(self.basket.can_user_price(self.pricer_user))

        partner = PartnerFactory()
        partner.users.add(self.pricer_user)
        partner.save()
        stock_record = StockRecordFactory(partner=partner)
        stock_record.add_manual_pricing_rule(
            PriceGreaterCondition(max_price=1))

        stl_file = StlFileFactory()
        self.basket.add_product(
            stl_file,
            stock_record.product,
            quantity=1,
            stockrecord=stock_record)

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])

        self.basket.refresh_from_db()
        self.pricer_user.refresh_from_db()
        self.assertTrue(self.basket.has_manual_pricing_request_sent)
        self.assertTrue(self.basket.can_user_price(self.pricer_user))
        self.assertTrue(self.basket.can_user_view(self.pricer_user))

        # Assert pricer got an email
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        mail_subject = (
            f'Manual pricing request on '
            f'3D project # {self.basket.quotation_number}'
        )
        self.assertEqual(email.subject, mail_subject)
        self.assertTrue(self.basket.url in email.body)

    def test_post_manual_priced(self):
        stockrecord = StockRecordFactory()
        stockrecord.add_manual_pricing_rule(PriceGreaterCondition(max_price=0))
        stockrecord.refresh_from_db()

        line = self.basket.add_empty_line(StlFileFactory())
        self.assertFalse(line.is_manual_pricing_required)

        url = reverse(
            'basket-ajax:basket-line-details',
            args=(
                self.basket.id,
                line.id))
        data = json.dumps({
            'product_slug': stockrecord.product.slug,
            'supplier_slug': stockrecord.partner.code
        })
        json_response = self.client.post(url, data)
        self.assertTrue(json_response['success'])
        self.assertTrue(json_response['is_manual_pricing_required'])
        line.refresh_from_db()
        self.assertTrue(line.is_manual_pricing_required)

    def test_set_prices(self):
        stockrecord = StockRecordFactory()
        stockrecord.always_priced_manually = True
        stockrecord.save()
        stockrecord.partner.users.add(self.pricer_user)
        line, created = self.basket.add_product(
            StlFileFactory(),
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        self.client.post(url)

        url = reverse('basket-ajax:set-prices', args=(self.basket.id,))

        # Basket owner connected, should not work
        not_allowed_response = self.client.post(url, return_json=False)
        self.assertEqual(not_allowed_response.status_code, 403)

        self.client.logout()
        logged_in = self.client.login(
            email=self.pricer_user.email,
            password=self.test_password)
        self.assertTrue(logged_in, 'should be logged in')

        url = reverse('basket-ajax:set-prices', args=(self.basket.id,))
        data = {
            line.id: 1000
        }
        json_response = self.client.post(url, json.dumps(data))

        self.assertTrue(json_response['success'])
        self.basket.refresh_from_db()
        line.refresh_from_db()
        self.assertTrue(line.has_manual_price_set)
        self.assertTrue(line.price_excl_tax, 1000)
        self.assertTrue(line.can_be_ordered)
        self.assertTrue(self.basket.are_all_lines_priced)
        self.assertFalse(self.basket.is_manually_priced)

        url = reverse(
            'basket-ajax:set-basket-as-priced',
            args=(
                self.basket.id,
            )
        )
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])
        self.basket.refresh_from_db()
        self.assertTrue(self.basket.is_manually_priced)

    def test_minimum_order_price(self):
        """Test that basket.difference_to_minimum_price calculated correctly"""
        fee_value = Decimal('2000.00')
        basket_price = '100.00'
        partner = PartnerFactory(
            minimum_order_price_enabled=True,
            minimum_order_price_value=fee_value
        )
        stockrecord = StockRecordFactory(
            partner=partner,
            price_custom=basket_price,
        )
        self.basket.add_product(
            StlFileFactory(),
            stockrecord.product,
            quantity=1,
            scale=1,
            stockrecord=stockrecord)

        self.assertEqual(
            self.basket.difference_to_minimum_price.excl_tax,
            fee_value - Decimal(basket_price)
        )

    def test_extra_fees(self):
        """
        Test that extra fees are displayed correctly and total price takes
        them into account
        """
        stock_price = Decimal(100.00)
        fee1 = Decimal(5.00)
        fee2 = Decimal(10.00)
        partner = PartnerFactory()
        stockrecord = StockRecordFactory(
            partner=partner,
            price_custom=str(stock_price),
        )
        PartnerExtraFeeFactory(
            partner=partner,
            type='Setup',
            value=fee1
        )
        PartnerExtraFeeFactory(
            partner=partner,
            type='Testing',
            value=fee2
        )
        self.basket.add_product(
            StlFileFactory(),
            stockrecord.product,
            quantity=1,
            scale=1,
            stockrecord=stockrecord)

        def test_showing_correctly():
            fees = self.basket.list_extra_fees()
            fee_names = [fee['name'] for fee in fees]
            self.assertIn('Setup', fee_names)
            # Generator to run over list of dicts until finds value
            setup_fee = next(
                (fee for fee in fees if fee["name"] == "Setup")
            )
            self.assertEqual(setup_fee['excl_tax'], fee1)
            self.assertIn('Testing', fee_names)

        def test_correct_total():
            price_obj = self.basket.price_object_incl_discounts
            self.assertEqual(
                price_obj.excl_tax,
                stock_price + fee1 + fee2
            )

        test_showing_correctly()
        test_correct_total()


class BasketPricingOrganizationAjaxCallsTest(
        AuthenticatedAjaxOrganizationTestCase):
    def setUp(self):
        super(BasketPricingOrganizationAjaxCallsTest, self).setUp()
        self.user.userprofile.site = self.site
        self.user.save()

        self.basket = BasketFactory(owner=self.user)
        self.basket.site = self.site
        self.basket.save()

        self.stockrecord = StockRecordFactory()
        line, _ = self.basket.add_product(
            StlFileFactory(),
            product=self.stockrecord.product,
            stockrecord=self.stockrecord)

    def test_manual_price_request(self):
        pricer_user = UserFactory(email='pricer@3yourmind.com')
        self.organization.save()
        partner = self.basket.first_line.stockrecord.partner
        partner.users.add(pricer_user)
        partner.save()

        self.assertFalse(self.basket.has_manual_pricing_request_sent)
        self.assertFalse(self.basket.can_user_view(pricer_user))
        self.assertFalse(self.basket.can_user_price(pricer_user))

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])

        self.basket.refresh_from_db()
        self.organization.refresh_from_db()

        self.assertTrue(self.basket.has_manual_pricing_request_sent)
        self.assertTrue(self.basket.can_user_view(pricer_user))
        self.assertTrue(self.basket.can_user_price(pricer_user))

        # Assert pricer got an email
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(len(email.recipients()), 1)
        mail_subject = (
            f'Manual pricing request on 3D project '
            f'# {self.basket.quotation_number}'
        )
        self.assertEqual(email.subject, mail_subject)
        self.assertTrue(self.basket.url in email.body)

    def test_yoda_manual_price_request(self):
        pricer_user = UserFactory(email='pricer@3yourmind.com')
        self.organization.is_yoda_active = True
        self.organization.save()
        partner = self.basket.first_line.stockrecord.partner
        partner.users.add(pricer_user)
        partner.save()

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        self.client.post(url)

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(len(email.recipients()), 1)
        self.assertTrue(settings.SERVICE_PANEL_PATH in email.body)

    def test_printing_service_and_pricer_gets_mail(self):
        pricer_user = UserFactory(email='pricer@3yourmind.com')

        ps_user = UserFactory(email='ps@3yourmind.com')
        self.stockrecord.partner.users.add(ps_user)
        self.stockrecord.partner.users.add(pricer_user)

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(len(email.recipients()), 2)
        mail_subject = (
            f'Manual pricing request on 3D project '
            f'# {self.basket.quotation_number}'
        )
        self.assertEqual(email.subject, mail_subject)
        self.assertTrue(self.basket.url in email.body)

    def test_printing_service_gets_mail_no_pricer(self):
        self.organization.manual_pricing_user = None
        self.organization.save()

        ps_user = UserFactory(email='ps@3yourmind.com')
        self.stockrecord.partner.users.add(ps_user)

        url = reverse('basket-ajax:manual-price', args=(self.basket.id,))
        json_response = self.client.post(url)
        self.assertTrue(json_response['success'])

        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(len(email.recipients()), 1)
        mail_subject = (
            f'Manual pricing request on 3D project '
            f'# {self.basket.quotation_number}'
        )
        self.assertEqual(email.subject, mail_subject)
        self.assertTrue(self.basket.url in email.body)
