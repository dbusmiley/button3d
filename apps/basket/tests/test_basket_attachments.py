# -*- coding: utf-8 -*-

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from django.test.client import MULTIPART_CONTENT

from apps.b3_tests.factories import StlFileFactory, \
    StockRecordFactory, BasketFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.basket.models import BasketAttachment


class BasketAttachmentAjaxCallsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(BasketAttachmentAjaxCallsTest, self).setUp()
        self.basket = BasketFactory(owner=self.user)

        stlfile = StlFileFactory()
        stockrecord = StockRecordFactory()
        self.line, created = self.basket.add_product(
            stlfile,
            product=stockrecord.product,
            stockrecord=stockrecord)
        self.assertEqual(self.basket.num_items, 1)

    def test_upload_basket_attachment(self):
        """
        Upload a file to the Basket.
        """
        pricing_user = UserFactory(
            email='supercoolpricer@3yourmind.com',
            password=self.test_password,
        )
        self.basket.first_line.stockrecord.partner.users.add(pricing_user)
        self.basket.first_line.stockrecord.partner.save()

        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'attachment.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf')

        data = {
            'attachment': attachment,
            'basket_id': self.basket.pk
        }

        json_response = self.client.post(
            url, data, return_json=True, content_type=MULTIPART_CONTENT
        )
        self.assertTrue(json_response['success'])
        self.assertEqual(len(json_response['attachments']), 1)
        self.assertEqual(
            json_response['attachments'][0]['name'],
            'attachment.pdf')
        self.assertEqual(
            json_response['attachments'][0]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][0]['basket_id'],
            self.basket.id)
        self.assertIsNone(json_response['attachments'][0]['basket_line_id'])

        attachment = BasketAttachment.objects.get(
            pk=json_response['attachments'][0]['id'])
        self.assertEqual(attachment.filename, 'attachment.pdf')
        self.assertEqual(attachment.uploader, self.user)
        self.assertEqual(attachment.basket, self.basket)
        self.assertIsNone(attachment.basket_line)

        # Upload a file to the Line
        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'attachment.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type="application/pdf")

        data = {
            'attachment': attachment,
            'basket_id': self.basket.pk,
            'basket_line_id': self.line.pk
        }

        json_response = self.client.post(
            url, data, return_json=True, content_type=MULTIPART_CONTENT)
        self.assertTrue(json_response['success'])
        self.assertEqual(len(json_response['attachments']), 1)

        self.assertEqual(
            json_response['attachments'][0]['name'],
            'attachment.pdf')
        self.assertEqual(
            json_response['attachments'][0]['uploader_pk'],
            self.user.id)
        self.assertEqual(
            json_response['attachments'][0]['uploader'],
            self.user.email)
        self.assertEqual(
            json_response['attachments'][0]['basket_id'],
            self.basket.id)
        self.assertEqual(
            json_response['attachments'][0]['basket_line_id'],
            self.line.id)

        attachment = BasketAttachment.objects.get(
            pk=json_response['attachments'][0]['id'])
        self.assertEqual(attachment.filename, 'attachment.pdf')
        self.assertEqual(attachment.uploader, self.user)
        self.assertEqual(attachment.basket, self.basket)
        self.assertEqual(attachment.basket_line, self.line)

        # Now check the basket
        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))
        json_response = self.client.get(url)

        self.assertEqual(json_response['title'], self.basket.title)
        self.assertEqual(len(json_response['attachments']), 2)

        first_attachement = json_response['attachments'][0]
        self.assertIsNone(first_attachement['basket_line_id'])
        self.assertEqual(first_attachement['basket_id'], self.basket.id)
        self.assertEqual(first_attachement['uploader_pk'], self.user.id)
        self.assertEqual(first_attachement['uploader'], self.user.email)
        self.assertEqual(first_attachement['name'], 'attachment.pdf')

        second_attachement = json_response['attachments'][1]
        self.assertEqual(second_attachement['uploader_pk'], self.user.id)
        self.assertEqual(second_attachement['uploader'], self.user.email)
        self.assertEqual(second_attachement['basket_id'], self.basket.id)
        self.assertEqual(second_attachement['name'], 'attachment.pdf')
        self.assertEqual(second_attachement['basket_line_id'], self.line.id)

        my_attachment_id = second_attachement['id']

        # And GET a single attachment
        data = {
            'id': my_attachment_id,
            'basket_id': self.basket.pk,
        }

        url = reverse('basket-ajax:basket-attachments')
        json_response = self.client.get(url, data, return_json=True)

        self.assertTrue(json_response['success'])
        self.assertEqual(
            json_response['attachment']['basket_line_id'],
            self.line.pk)

        # Check if basket line is fine
        url = reverse(
            'basket-ajax:basket-line-details',
            args=(
                self.basket.id,
                self.line.pk))
        json_response = self.client.get(url, data, return_json=True)

        self.assertTrue(len(json_response['attachments']), 1)

        self.assertTrue(
            self.client.login(
                email=pricing_user.email,
                password=self.test_password
            )
        )

        # And GET a single attachment
        data = {
            'id': my_attachment_id,
            'basket_id': self.basket.pk,
        }

        url = reverse('basket-ajax:basket-attachments')
        json_response = self.client.get(url, data, return_json=True)
        # Permission should be denied
        self.assertFalse(json_response['success'])

        # Give other user access to the basket
        self.basket.set_for_manual_pricing()

        # And GET a single attachment
        data = {
            'id': my_attachment_id,
            'basket_id': self.basket.pk,
        }

        url = reverse('basket-ajax:basket-attachments')

        json_response = self.client.get(url, data, return_json=True)

        # Permission should be granted
        self.assertTrue(json_response['success'])

        # Change user back (just in case...)
        self.assertTrue(
            self.client.login(
                email=self.user.email,
                password=self.test_password))

        # DELETE Attachment
        basket_attachments_url = reverse('basket-ajax:basket-attachments')
        url = (
            f'{basket_attachments_url}'
            f'?id={my_attachment_id}'
            f'&basket_id={self.basket.pk}'
        )
        json_response = self.client.delete(
            url, return_json=True, content_type="application/json")
        self.assertTrue(json_response['success'])

        # Now check the basket
        url = reverse('basket-ajax:basket-details', args=(self.basket.id,))
        json_response = self.client.get(url)

        self.assertEqual(len(json_response['attachments']), 1)

    def test_attachment_with_unicode_name(self):
        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'déjà-vu.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type="application/pdf")

        data = {
            'attachment': attachment,
            'basket_id': self.basket.pk
        }
        json_response = self.client.post(
            url, data, content_type=MULTIPART_CONTENT)
        self.assertTrue(json_response['success'])
        self.assertEqual(len(json_response['attachments']), 1)
        self.assertEqual(
            json_response['attachments'][0]['name'],
            'déjà-vu.pdf')

    def test_post_attachment_empty_basket_id(self):
        url = reverse('basket-ajax:basket-attachments')
        attachment = SimpleUploadedFile(
            'bla.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf')

        data = {
            'attachment': attachment,
            'basket_id': None
        }
        json_response = self.client.post(
            url, data, content_type=MULTIPART_CONTENT)
        self.assertFalse(json_response['success'])

        data = {
            'attachment': attachment,
            'basket_id': 'null'
        }
        json_response = self.client.post(
            url, data, content_type=MULTIPART_CONTENT)
        self.assertFalse(json_response['success'])
