from apps.basket.models import Basket
from apps.b3_tests.factories import StlFileFactory, \
    PartnerFactory, BasketFactory, StockRecordFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class BasketAccessChecks(AuthenticatedTestCase):
    def setUp(self):
        super(BasketAccessChecks, self).setUp()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.save()
        self.stockrecord = StockRecordFactory(partner=self.partner)
        self.stockrecord.save()

    def test_access_with_no_owner(self):
        """
        Basket.get_users_with_access() can 500 when no partner users exist.
        Check if this is still the case
        """
        # Owner is null here
        basket = BasketFactory(status=Basket.EDITABLE)
        basket.site = self.site
        line1, created = basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        basket.status = Basket.SUBMITTED
        basket.save()

        self.assertEqual(len(basket.get_users_with_access()), 0)

    def test_access_with_owner_no_partner_user(self):
        basket = BasketFactory(status=Basket.EDITABLE, owner=self.user)
        basket.site = self.site
        line1, created = basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        basket.status = Basket.SUBMITTED
        basket.save()

        self.assertEqual(len(basket.get_users_with_access()), 1)

    def test_access_with_owner_with_partner_user(self):
        new_user = UserFactory()
        self.partner.users.add(new_user)
        self.partner.save()
        basket = BasketFactory(status=Basket.EDITABLE, owner=self.user)
        basket.site = self.site
        line1, created = basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        basket.status = Basket.SUBMITTED
        basket.save()

        self.assertEqual(len(basket.get_users_with_access()), 2)
