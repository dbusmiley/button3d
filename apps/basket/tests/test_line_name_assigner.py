# -*- coding: utf-8 -*-


from apps.b3_tests.factories import StlFileFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class BasketLineNameAssignerTest(TestCase):
    def test_unicode_line_name(self):
        b = BasketFactory()

        stl_file = StlFileFactory(showname='Törö_éèàà')
        line = b.add_empty_line(stl_file)
        self.assertIn('Törö_éèàà', line.name)
