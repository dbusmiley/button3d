import json

from django.urls import reverse
from apps.b3_tests.factories import UserFactory
from apps.b3_tests.testcases.common_testcases import \
    AuthenticatedAjaxTestCase


class BasketPermissionsAjaxCallsTest(AuthenticatedAjaxTestCase):
    def test_put_single_user(self):
        """
        Test which will create a new 3D project and
            add permission to one user to share project.
        It expects the right response.
        """
        basket_id = self.create_new_basket('Project Put Single User')

        other_user = UserFactory(password=self.test_password)
        basket_add_response = self.add_as_viewer(basket_id, other_user)

        self.assertEqual(basket_add_response['role'], 'viewer')
        self.assertEqual(basket_add_response['success'], True)
        self.assertEqual(basket_add_response['email'], other_user.email)
        self.assertEqual(
            basket_add_response['name'], str(
                other_user.userprofile))
        self.assertTrue('email' in basket_add_response['contact'])

    def test_get_users(self):
        """
        Test which adds two users to the project and after each
            addition will check the amount of owners and users.
        """
        basket_id = self.create_new_basket('Project Get Single User')

        first_user = UserFactory(password=self.test_password)
        self.add_as_viewer(basket_id, first_user)

        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        first_response = self.client.get(url)
        self.assertEqual(2, len(first_response))
        owners_after_first_add = [
            permission for permission in first_response if
            permission['role'] == 'owner']
        viewers_after_first_add = [
            permission for permission in first_response if
            permission['role'] == 'viewer']
        self.assertEqual(1, len(owners_after_first_add))
        self.assertEqual(1, len(viewers_after_first_add))

        second_user = UserFactory(password=self.test_password)
        self.add_as_viewer(basket_id, second_user)
        second_response = self.client.get(url)

        self.assertEqual(3, len(second_response))
        all_owners = [
            permission for permission in second_response if
            permission['role'] == 'owner']
        all_viewers = [
            permission for permission in second_response if
            permission['role'] == 'viewer']
        self.assertEqual(1, len(all_owners))
        self.assertEqual(2, len(all_viewers))

    def test_delete_users(self):
        """
        This test will add permission for three users and
            will delete one and checks the amount of viewers after the
        addition and after the deletion.
        """
        basket_id = self.create_new_basket('Project Delete Users')
        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        first_user = UserFactory(password=self.test_password)
        second_user = UserFactory(password=self.test_password)
        third_user = UserFactory(password=self.test_password)

        self.add_as_viewer(basket_id, first_user)
        self.add_as_viewer(basket_id, second_user)
        self.add_as_viewer(basket_id, third_user)

        first_response = self.client.get(url)
        self.assertEqual(4, len(first_response))

        self.delete(basket_id, second_user)

        second_response = self.client.get(url)
        self.assertEqual(3, len(second_response))

    def test_add_delete_non_existing_user(self):
        """
        Test which tries to add a non existing user and
            delete a non existing user.
        """
        basket_id = self.create_new_basket('Project Delete Users')
        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        data = json.dumps({
            'email': 'WillNotExistInDb@3yourmind.com'
        })
        response = self.client.post(url, data)
        self.assertEqual(False, response['success'])

        delete_response = self.client.delete(url, data)
        self.assertEqual(False, delete_response['success'])

    def test_user_without_permission(self):
        """
        This test will create a new basket and then change the user
            and try to add permission to a user as a non basket
        owner.
        """
        basket_id = self.create_new_basket('Project Delete Users')
        self.client.logout()

        new_auth_user = UserFactory(password=self.test_password)
        self.client.login(
            email=new_auth_user.email,
            password=self.test_password)

        user_to_add = UserFactory(password=self.test_password)
        response_1 = self.add_as_viewer(basket_id, user_to_add)
        self.assertEqual(False, response_1['success'])

        self.client.logout()
        self.client.login(email=self.user.email, password=self.test_password)

    def test_adding_user_multiple_times(self):
        """
        Test which will first add a user to a basket and then will
            try to add the user again. This should result in an
        error.
        """
        basket_id = self.create_new_basket('Adding User multiple times')
        first_user = UserFactory(password=self.test_password)
        self.add_as_viewer(basket_id, first_user)
        response = self.add_as_viewer(basket_id, first_user)
        self.assertEqual(False, response['success'])

    def add_as_viewer(self, basket_id, user):
        """
        Helper method to add a viewer in a one liner.
        :param basket_id: Id of the basket where the user should be added.
        :param user: User which should be added for share permission.
        :return: The response of the call.
        """
        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        data = json.dumps({
            'email': user.email
        })
        return self.client.post(url, data)

    def delete(self, basket_id, user):
        """
        Helper method to delete a viewer in a one liner.
        :param basket_id: Id of the basket where the user should be deleted.
        :param user: User which should get deleted.
        :return: The response of the call.
        """
        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        data = json.dumps({
            'email': user.email
        })
        return self.client.delete(url, data)

    def create_new_basket(self, title):
        """
        Method to create a basket in a one liner.
        :param title: Title of the basket.
        :return: The id of the created basket.
        """
        basket_create_url = reverse('basket-ajax:create')
        data = json.dumps({
            'title': title
        })
        basket_creation_json_response = self.client.put(
            basket_create_url, data)
        basket_id = basket_creation_json_response['id']
        return basket_id


class SharedBasketClone(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(SharedBasketClone, self).setUp()
        self.basket_id = self.create_new_basket(title='Test 3D Project')
        self.other_user = UserFactory(password=self.test_password)

    def test_cloning_shared_basket_permitted(self):
        self.add_as_viewer(self.basket_id, self.other_user)
        basket_clone_response = self.clone_as_user(other_user=self.other_user,
                                                   basket_id=self.basket_id)
        self.assertEqual(basket_clone_response['success'], True)
        self.reset_ajax_client()

    def test_cloning_shared_basket_not_permitted(self):
        basket_clone_response = self.clone_as_user(other_user=self.other_user,
                                                   basket_id=self.basket_id)
        self.assertEqual(basket_clone_response['code'],
                         'PROJECT_CLONE_PERMISSION')
        self.reset_ajax_client()

    def create_new_basket(self, title):
        """
        Method to create a basket in a one liner.
        :param title: Title of the basket.
        :return: The id of the created basket.
        """
        basket_create_url = reverse('basket-ajax:create')
        data = json.dumps({
            'title': title
        })
        basket_creation_json_response = self.client.put(
            basket_create_url, data)
        basket_id = basket_creation_json_response['id']
        return basket_id

    def add_as_viewer(self, basket_id, user):
        """
        Helper method to add a viewer in a one liner.
        :param basket_id: Id of the basket where the user should be added.
        :param user: User which should be added for share permission.
        :return: The response of the call.
        """
        url = reverse('basket-ajax:basket-users', args=(basket_id,))
        data = json.dumps({
            'email': user.email
        })
        return self.client.post(url, data)

    def clone_as_user(self, other_user, basket_id):
        self.client.login(email=other_user.email, password=self.test_password)
        basket_clone_url = reverse('basket-ajax:clone', args=(basket_id,))
        data = json.dumps({
            'project_name': 'cloned 3d project'
        })
        return self.client.post(basket_clone_url, data)

    def reset_ajax_client(self):
        self.client.login(email=self.user.email, password=self.test_password)
