from decimal import Decimal, ROUND_DOWN


from apps.b3_core.utils import quantize
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import StlFileFactory, PartnerFactory, \
    create_product, create_stockrecord, \
    CountryFactory, BasketFactory, AddressFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket
from apps.partner.conclusionactions import FixedDiscountAction, \
    PercentageDiscountAction
from apps.partner.conditions import QuantityGreaterCondition


class BasketDiscountTestCase(AuthenticatedTestCase):
    def setUp(self):
        """
        Method to set up all the mocking data needed for the test.
        """
        super(BasketDiscountTestCase, self).setUp()

        self.stl_file = StlFileFactory(showname='The Stl Model')
        self.partner = PartnerFactory(name='The Company')
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish,
            partner=self.partner,
            is_default=True
        )

        germany = CountryFactory(
            alpha2='DE'
        )
        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
            name='My First Shipping Method'
        )
        self.user_address = AddressFactory(
            first_name='The Tester',
            last_name='Testing',
            user=self.user,
            country=germany
        )

        self.basket_four_items = self.create_quantity_basket(4)
        self.basket_six_items = self.create_quantity_basket(6)
        self.basket_single_item = self.create_quantity_basket(1)
        self.basket_1 = self.create_quantity_basket(1)
        self.basket_2 = self.create_quantity_basket(2)
        self.basket_3 = self.create_quantity_basket(3)

    def create_quantity_basket(self, quantity):
        basket = BasketFactory(status=Basket.OPEN, owner=self.user)
        basket.add_product(
            self.stl_file,
            product=self.product,
            stockrecord=self.stock_record,
            quantity=quantity
        )
        return basket

    def test_fixed_material_discount(self):
        # 10 money-units discount on Lines with more than 2 items
        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=2)
        )

        # Should apply
        self.basket_3.refresh_price()
        self.assertEqual(
            self.basket_3.total_excl_tax,
            self.basket_3.total_excl_tax_excl_discounts - Decimal(10)
        )
        # Should not apply
        self.basket_1.refresh_price()
        self.assertEqual(
            self.basket_1.total_excl_tax,
            self.basket_1.total_excl_tax_excl_discounts
        )

    def test_percentage_material_discount(self):
        """
        This test will create a discount for a stockrecord. If there is a line
        with this specific stock record and the amount of this item is above
        two a ten percent discount will be applied.
        """
        self.stock_record.add_discount_rule(
            PercentageDiscountAction(rate=0.10),
            QuantityGreaterCondition(max_quantity=2)
        )

        self.basket_3.refresh_price()
        self.assertEqual(
            self.basket_3.total_excl_tax,
            quantize(
                self.basket_3.total_excl_tax_excl_discounts * Decimal(0.90))
        )
        self.assertEqual(
            self.basket_3.total_incl_tax,
            quantize(
                self.basket_3.total_incl_tax_excl_discounts * Decimal(0.90))
        )
        self.basket_1.refresh_price()
        self.assertEqual(
            self.basket_1.total_excl_tax,
            self.basket_1.total_excl_tax_excl_discounts
        )

    def test_only_best_material_discount_applies(self):
        # 10% discount on Baskets with more than 1 item
        self.stock_record.add_discount_rule(
            PercentageDiscountAction(rate=0.10),
            QuantityGreaterCondition(max_quantity=1)
        )
        # 20% discount on Baskets with more than 2 items
        self.stock_record.add_discount_rule(
            PercentageDiscountAction(rate=0.20),
            QuantityGreaterCondition(max_quantity=2)
        )

        self.basket_3.refresh_price()
        # Only 20% discount should apply on basket
        self.assertEqual(
            self.basket_3.total_excl_tax,
            quantize(
                self.basket_3.total_excl_tax_excl_discounts * Decimal(0.80))
        )

    def test_discount_applies_on_tax_incl_price(self):
        # 10 money-units discount on Baskets with more than 2 items
        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=2)
        )

        # Should apply
        self.basket_3.refresh_price()
        # When a discount of 10 is applied to the tax exclusive price,
        # the tax has to be reduced, too.
        # The total discount is then 10 + 10*tax_rate
        # (e.g. tax_rate = 0.19 for 19% tax)
        expected_price_incl_tax_incl_discount = quantize(
            (self.basket_3.total_excl_tax_excl_discounts -
             Decimal(10)) * (1 + self.partner.vat_rate / 100),
            rounding=ROUND_DOWN
        )
        self.assertEqual(
            self.basket_3.total_incl_tax,
            expected_price_incl_tax_incl_discount
        )
