import json
from uuid import UUID

from django.urls import reverse

from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.basket.models import Comment
from apps.basket.views import BasketCommentsView


class BasketCommentsAjaxCallsTest(AuthenticatedAjaxTestCase):
    """
    Test cases for the Project Comments.
    """

    def test_uuid_converter(self):
        """
        Test the static converter method of the view for converting comment
        specific UUIDs.
        """
        test_first_uuid = 'comments-c744a5bfc6114e249c06ad833bea5648'
        self.assertEqual(
            UUID('c744a5bfc6114e249c06ad833bea5648'),
            BasketCommentsView.convert_to_uuid(test_first_uuid)
        )

        test_first_uuid = 'comments-e81e0bc649ab439a884a5cca5cc736dd'
        self.assertEqual(
            UUID('e81e0bc649ab439a884a5cca5cc736dd'),
            BasketCommentsView.convert_to_uuid(test_first_uuid)
        )

    def test_add_comment(self):
        """
        Test which will simply create a project and add a comment to it after.
        """
        basket_id = self.create_new_basket('Project Comment Single User')

        url = reverse('basket-ajax:basket-comments', args=(basket_id,))

        data = json.dumps({
            'parent': None,
            'text': 'This is a new Main Comment'
        })
        response = self.client.put(url, data)
        self.assertTrue('comments-' in response['id'])

    def test_add_nested_comments(self):
        """
        Test which will create a basket. After the ceation the basket owner
        will add one first layer comment and one second layer comment.
        """
        basket_id = self.create_new_basket('Project Nested Comments')

        url = reverse('basket-ajax:basket-comments', args=(basket_id,))

        # Add first comment.
        data = json.dumps({
            'parent': None,
            'text': 'First Layer Comment 1'
        })
        parent_id = self.client.put(url, data)['id']

        # Add second comment.
        data = json.dumps({
            'parent': parent_id,
            'text': 'First Layer Comment 1'
        })
        second_id = self.client.put(url, data)['id']

        self.assertEqual(2, Comment.objects.count())
        self.assertEqual(1, Comment.objects.filter(parent=None).count())
        uuid_to_filter = UUID(second_id.replace('comments-', ''))
        self.assertFalse(
            Comment.objects.filter(uuid=uuid_to_filter).first() is None
        )

    def create_new_basket(self, title):
        """
        Method to create a basket in a one liner.
        :param title: Title of the basket.
        :return: The id of the created basket.
        """
        basket_create_url = reverse('basket-ajax:create')
        data = json.dumps({
            'title': title
        })
        basket_creation_json_response = self.client.put(
            basket_create_url,
            data
        )
        basket_id = basket_creation_json_response['id']
        return basket_id
