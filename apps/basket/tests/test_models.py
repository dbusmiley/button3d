from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from django.utils import timezone
from django.utils.translation import activate

from apps.b3_core.models import Configuration
from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import UserFactory, StlFileFactory, \
    StockRecordFactory, PartnerFactory, ColorFactory, ProductFactory, \
    SiteFactory, OrganizationFactory, PostProcessingFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.basket.models import Basket, Line
from apps.partner.conditions import PriceGreaterCondition


class BasketModelTest(TestCase):
    def setUp(self):
        super(BasketModelTest, self).setUp()

    def test_multiple_saved_baskets(self):
        user = UserFactory()
        self.assertEqual(Basket.saved.filter(owner=user).count(), 0)

        basket1 = BasketFactory(owner=user, status=Basket.SAVED)
        self.assertEqual(Basket.saved.get(owner=user), basket1)
        self.assertEqual(Basket.saved.filter(owner=user).count(), 1)

        basket2 = BasketFactory(owner=user, status=Basket.SAVED)
        self.assertEqual(Basket.saved.get(owner=user), basket2)
        self.assertEqual(Basket.saved.filter(owner=user).count(), 1)

        b = BasketFactory.build(owner=user, status=Basket.SAVED)
        self.assertEqual(Basket.saved.get(owner=user), basket2)
        self.assertEqual(Basket.saved.filter(owner=user).count(), 1)
        b.save()
        self.assertEqual(Basket.saved.get(owner=user), b)
        self.assertEqual(Basket.saved.filter(owner=user).count(), 1)

    def test_default_title(self):
        activate('en')
        b = BasketFactory()
        self.assertEqual(b.title, f'3D Project {b.quotation_number}')
        activate('en')  # Set back to english for tests

    def test_quotation_number(self):
        b = BasketFactory()
        self.assertIsNotNone(b.quotation_number)
        self.assertEqual(b.quotation_number, 100000 + b.id)

        b2 = BasketFactory.build(quotation_number=None)
        self.assertIsNone(b2.quotation_number)
        b2.save()
        b2.refresh_from_db()
        self.assertIsNotNone(b2.quotation_number)

    def test_update_title_and_reference(self):
        b = BasketFactory()
        self.assertIsNotNone(b.title)
        self.assertIsNone(b.reference)

        # Update title
        changed = b.update_fields({'title': 'Test'})
        self.assertTrue(changed)
        self.assertEqual(b.title, 'Test')
        with self.assertRaises(ValueError):
            b.update_fields({'title': None})
        with self.assertRaises(ValueError):
            b.update_fields({'title': ''})
        self.assertEqual(b.title, 'Test')

        # Update reference
        changed = b.update_fields({'reference': 'Ref'})
        self.assertTrue(changed)
        self.assertEqual(b.reference, 'Ref')

        changed = b.update_fields({'reference': ''})
        self.assertTrue(changed)
        self.assertIsNone(b.reference)

        b.update_fields({'reference': 'Ref'})
        changed = b.update_fields({'reference': None})
        self.assertTrue(changed)
        self.assertIsNone(b.reference)

        b.update_fields({'reference': 'Ref'})
        changed = b.update_fields({'reference': -1})
        self.assertTrue(changed)
        self.assertIsNone(b.reference)

        # Update both
        changed = b.update_fields({'title': 'Foo', 'reference': 'Bar'})
        self.assertTrue(changed)
        self.assertEqual(b.title, 'Foo')
        self.assertEqual(b.reference, 'Bar')

        # Update none
        changed = b.update_fields(None)
        self.assertFalse(changed)
        self.assertEqual(b.title, 'Foo')
        self.assertEqual(b.reference, 'Bar')
        changed = b.update_fields({})
        self.assertFalse(changed)
        self.assertEqual(b.title, 'Foo')
        self.assertEqual(b.reference, 'Bar')

    def test_delete_basket(self):
        partner = PartnerFactory()
        stockrecord = StockRecordFactory(partner=partner)
        stlfile = StlFileFactory()

        b = BasketFactory()
        line, created = b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)
        self.assertIsNone(b.deleted_date)
        self.assertFalse(b.is_deleted)
        self.assertTrue(Line.objects.filter(pk=line.pk).exists())
        self.assertFalse(stlfile.is_deleted)

        # Delete and test everything is deleted
        b.delete()
        now = timezone.now()
        self.assertEqual(b.deleted_date.day, now.day)
        self.assertEqual(b.deleted_date.month, now.month)
        self.assertEqual(b.deleted_date.year, now.year)
        self.assertTrue(b.is_deleted)

        # Cannot alter basket
        with self.assertRaises(ValueError):
            b.add_product(
                stlfile,
                stockrecord.product,
                quantity=1,
                stockrecord=stockrecord)
        with self.assertRaises(ValueError):
            b.update_fields({'title': 'NewTitle'})

        # Test manager
        self.assertEqual(Basket.objects.count(), 0)
        self.assertEqual(Basket.open.count(), 0)
        self.assertEqual(Basket.saved.count(), 0)
        self.assertEqual(Basket.all_objects.count(), 1)

    def test_delete_submitted_basket(self):
        b = BasketFactory()
        b.submit()
        with self.assertRaises(ValueError):
            b.delete()

    def test_basket_has_only_one_supplier_empty_basket(self):
        b = BasketFactory()
        self.assertTrue(b.has_only_one_supplier())

    def test_basket_has_only_one_supplier_one_line(self):
        b = BasketFactory()
        stockrecord = StockRecordFactory()
        line, created = b.add_product(
            StlFileFactory(),
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)

        b.refresh_from_db()
        self.assertTrue(b.has_only_one_supplier())

    def test_basket_has_only_one_supplier_one_empty_line(self):
        b = BasketFactory()
        b.add_empty_line(StlFileFactory())

        b.refresh_from_db()
        self.assertTrue(b.has_only_one_supplier())

    def test_basket_has_only_one_supplier_multiple_lines_same_supplier(self):
        b = BasketFactory()
        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()
        b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)
        b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)

        b.refresh_from_db()
        self.assertTrue(b.has_only_one_supplier())

    def test_basket_has_only_one_supplier_multiple_lines_different_supplier(
            self):
        b = BasketFactory()
        stlfile = StlFileFactory()
        partner = PartnerFactory(code='p1')
        stockrecord = StockRecordFactory(partner=partner)
        b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)
        partner2 = PartnerFactory(code='p2')
        stockrecord2 = StockRecordFactory(partner=partner2)
        b.add_product(
            stlfile,
            stockrecord2.product,
            quantity=1,
            stockrecord=stockrecord2)

        b.refresh_from_db()
        self.assertFalse(b.has_only_one_supplier())

    def test_basket_has_only_one_supplier_multiple_lines_some_empty(self):
        b = BasketFactory()
        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()
        b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)
        b.add_empty_line(stlfile)

        b.refresh_from_db()
        self.assertFalse(b.has_only_one_supplier())

    def test_basket_can_be_ordered(self):
        b = BasketFactory()
        self.assertTrue(b.is_empty)
        self.assertTrue(b.all_lines_can_be_ordered())

        # Add a full line
        partner = PartnerFactory()
        stockrecord = StockRecordFactory(partner=partner)
        stlfile = StlFileFactory()

        line, created = b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)

        self.assertFalse(line.is_empty)
        self.assertFalse(b.has_empty_lines())
        self.assertTrue(b.all_lines_can_be_ordered())

        # Add empty line
        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile, quantity=1)
        self.assertTrue(line.is_empty)
        self.assertTrue(b.has_empty_lines())
        self.assertFalse(b.all_lines_can_be_ordered())

        # Set product
        line.product = stockrecord.product
        line.save()
        self.assertTrue(line.is_empty)
        self.assertTrue(b.has_empty_lines())
        self.assertFalse(b.all_lines_can_be_ordered())

        # Set stockrecord
        line.stockrecord = stockrecord
        line.save()
        self.assertFalse(line.is_empty)
        self.assertFalse(b.has_empty_lines())
        # Not all lines can be ordered here because no price has been
        # calculated for the empty line

    def test_empty_line(self):
        b = BasketFactory()
        partner = PartnerFactory()
        product = ProductFactory()
        child_product = ProductFactory(
            title='Child')
        stockrecord = StockRecordFactory(
            product=child_product,
            is_default=True,
            partner=partner)

        stlfile = StlFileFactory()

        with self.assertRaises(ValueError):
            b.add_empty_line(None)

        # Create empty line
        line = b.add_empty_line(stlfile, quantity=1)
        self.assertTrue(line.is_empty)

        # This should not raise a TypeError, but show 0 as price
        self.assertEqual(line.unit_price_excl_tax, 0)

        # Update product
        self.assertTrue(line.update({'product': product}))
        line.refresh_from_db()
        self.assertTrue(line.is_empty)
        self.assertEqual(line.product, product)

        # This should not raise a TypeError
        self.assertEqual(line.unit_price_incl_tax, 0)

        # Update stockrecord
        self.assertTrue(line.update({'stockrecord': stockrecord}))
        line.refresh_from_db()
        self.assertFalse(line.is_empty)
        self.assertEqual(line.stockrecord, stockrecord)
        self.assertEqual(b.lines.count(), 1)
        self.assertIsNotNone(line.unit_price_incl_tax)

        # Compare with 'normal' line
        stlfile = StlFileFactory()
        line2, created = b.add_product(
            stlfile,
            product=product,
            quantity=1,
            stockrecord=stockrecord)
        self.assertEqual(line.price_incl_tax, line2.price_incl_tax)
        self.assertEqual(line.price_excl_tax, line2.price_excl_tax)
        # TODO test all other things that can be updated

    def test_default_line_name(self):
        b = BasketFactory()
        StockRecordFactory()
        stlfile = StlFileFactory()
        # Create empty line
        line = b.add_empty_line(stlfile, quantity=1)
        self.assertEqual(
            line.name,
            f'{b.quotation_number}-{stlfile.showname}-1'
        )
        # TODO need to test more (thread safety, etc.)

    def test_clone(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        b.add_empty_line(stlfile, quantity=1)

        cloned_b = b.clone('ClonedBasket')
        self.assertNotEqual(cloned_b.id, b.id)
        self.assertEqual(cloned_b.title, 'ClonedBasket')
        self.assertNotEqual(cloned_b.quotation_number, b.quotation_number)
        self.assertEqual(cloned_b.num_items, 1)
        self.assertEqual(b.num_items, 1)
        self.assertEqual(cloned_b.reference, b.reference)

        self.assertEqual(
            b.first_line.name,
            f'{b.quotation_number}-{b.first_line.stl_file.showname}-1'
        )
        first_line_name = (
            f'{cloned_b.quotation_number}-'
            f'{cloned_b.first_line.stl_file.showname}-1'
        )
        self.assertEqual(cloned_b.first_line.name, first_line_name)
        self.assertEqual(cloned_b.first_line.stl_file, stlfile)

        b.submit()
        cloned_b2 = b.clone('BasketClonedSubmit')
        self.assertNotEqual(cloned_b2.id, b.id)
        self.assertEqual(cloned_b2.title, 'BasketClonedSubmit')
        self.assertNotEqual(cloned_b2.quotation_number, b.quotation_number)
        self.assertEqual(cloned_b2.num_items, 1)
        self.assertEqual(cloned_b2.reference, b.reference)

    def test_manual_pricing_stockrecordrule(self):
        partner = PartnerFactory()
        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.add_manual_pricing_rule(PriceGreaterCondition(max_price=1))

        stlfile = StlFileFactory()
        b = BasketFactory()
        line, created = b.add_product(
            stlfile,
            stockrecord.product,
            quantity=1,
            stockrecord=stockrecord)
        self.assertFalse(line.is_empty)
        self.assertTrue(line.is_manual_pricing_required)
        self.assertFalse(line.has_manual_price_set)
        self.assertFalse(line.can_be_ordered)

    def test_can_user_view_owner(self):
        user = UserFactory()
        b = BasketFactory(owner=user)
        self.assertTrue(b.can_user_view(user))

    def test_can_user_view_pricer(self):
        pricer = UserFactory(  # nosec
            email='testpricer@3yourmind.com',
            password='Test1234'
        )
        test_site = SiteFactory()
        OrganizationFactory(site=test_site)
        set_current_site(test_site)

        b = BasketFactory()

        self.assertFalse(b.can_user_view(pricer))
        partner = PartnerFactory()
        partner.users.add(pricer)
        partner.save()

        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.always_priced_manually = True
        stockrecord.save()

        new_line = b.add_empty_line(StlFileFactory(), quantity=1, scale=1)

        update_dictionary = {
            "product_slug": stockrecord.product.slug,
            "supplier_slug": partner.code,
            'stockrecord': stockrecord
        }
        new_line.update(update_dictionary)
        b.set_for_manual_pricing()
        b.status = Basket.SUBMITTED
        b.save()

        b.refresh_from_db()
        self.assertTrue(b.can_user_view(pricer))
        set_current_site(None)

    def test_can_user_view_anonymous(self):
        anonymous = AnonymousUser()
        b = BasketFactory(owner=None)
        request = RequestFactory().get('/')
        self.assertFalse(b.can_user_view(anonymous))
        self.assertFalse(b.can_user_view(anonymous, request))
        request.basket = b
        self.assertTrue(b.can_user_view(anonymous, request))

    def test_correct_users_with_access_for_anonymous(self):
        basket = BasketFactory(owner=None)
        users_with_access = basket.get_users_with_access()
        self.assertNotIn(None, users_with_access)

    def test_multiple_currencies(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        p1 = PartnerFactory(code='p1', price_currency='USD')
        sr1 = StockRecordFactory(partner=p1)
        l1, _ = b.add_product(
            stlfile, product=sr1.product, stockrecord=sr1)

        stlfile = StlFileFactory()
        p2 = PartnerFactory(code='p2', price_currency='EUR')
        sr2 = StockRecordFactory(partner=p2)
        l2, _ = b.add_product(
            stlfile, product=sr2.product, stockrecord=sr2)

        self.assertEqual(b.num_lines, 2)
        self.assertNotEqual(l1.price_currency, l2.price_currency)
        self.assertFalse(b.has_only_one_currency())
        self.assertFalse(b.has_only_one_supplier())

        # Set them to the same stockrecord
        self.assertTrue(l2.update({'stockrecord': sr1}))
        l2.refresh_from_db()
        self.assertEqual(b.num_lines, 2)
        self.assertEqual(l1.price_currency, l2.price_currency)
        self.assertTrue(b.has_only_one_currency())
        self.assertTrue(b.has_only_one_supplier())

    def test_add_product_to_manually_priced_basket(self):
        b = BasketFactory(pricing_status=Basket.MANUALLY_PRICED)
        stlfile = StlFileFactory()

        with self.assertRaises(ValueError):
            sr = StockRecordFactory()
            b.add_product(stlfile, product=sr.product, stockrecord=sr)

        with self.assertRaises(ValueError):
            b.add_empty_line(stlfile)

    def test_add_product_to_waiting_for_pricing_basket(self):
        b = BasketFactory(pricing_status=Basket.WAITING_FOR_MANUAL_PRICING)
        stlfile = StlFileFactory()

        with self.assertRaises(ValueError):
            sr = StockRecordFactory()
            b.add_product(stlfile, product=sr.product, stockrecord=sr)

        with self.assertRaises(ValueError):
            b.add_empty_line(stlfile)

    def test_basket_purchase_info(self):
        basket = BasketFactory()
        partner = PartnerFactory(vat_rate=-0.11)
        stockrecord = StockRecordFactory(partner=partner)

        line, _ = basket.add_product(
            StlFileFactory(),
            product=stockrecord.product,
            stockrecord=stockrecord,
            scale=0.3,
            quantity=10,
            unit=Configuration.INCH,
        )
        line.price_excl_tax = None
        line.price_incl_tax = None

        try:
            # blind call, which can raiase an error due to NoneType prices
            line.purchase_info
        except TypeError:
            self.fail('Line.purchase_info() raised TypeError')


class BasketLineModelTest(TestCase):
    def setUp(self):
        super(BasketLineModelTest, self).setUp()

    def test_update_color(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)

        color1 = ColorFactory()
        color2 = ColorFactory()
        post_processing = PostProcessingFactory(colors=[color1, color2])

        post_processings = {
            'post_processings': [{
                'post_processing': post_processing,
                'color': color1
            }]
        }
        line.update(post_processings)
        line.refresh_from_db()
        self.assertEqual(line.post_processing_options.first().color, color1)

        post_processings = {
            'post_processings': [{
                'post_processing': post_processing,
                'color': color2
            }]
        }
        line.update(post_processings)
        line.refresh_from_db()
        self.assertEqual(line.post_processing_options.first().color, color2)

    def test_update_quantity(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)
        self.assertEqual(line.quantity, 1)

        self.assertTrue(line.update({'quantity': 5}))
        line.refresh_from_db()
        self.assertEqual(line.quantity, 5)

    def test_update_filename(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)
        self.assertEqual(
            line.name,
            f'{b.quotation_number}-{line.stl_file.showname}-1'
        )

        self.assertTrue(line.update({'filename': 'BlaBlaTest'}))
        line.refresh_from_db()
        self.assertEqual(line.name, 'BlaBlaTest')

    def test_update_scale(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)

        self.assertTrue(line.update({'scale': 1.3}))
        line.refresh_from_db()
        self.assertEqual(line.scale, 1.3)

        self.assertTrue(line.update({'scale': 2}))
        line.refresh_from_db()
        self.assertEqual(line.scale, 2)

    def test_update_measure_unit(self):
        b = BasketFactory()

        stlfile = StlFileFactory()
        line = b.add_empty_line(stlfile)
        self.assertEqual(line.measure_unit, 'mm')

        self.assertTrue(line.update({'unit': 'in'}))
        line.refresh_from_db()
        self.assertEqual(line.measure_unit, 'in')

    def test_clone(self):
        b = BasketFactory()

        stlfile = StlFileFactory()

        line = b.add_empty_line(stlfile, scale=1.3)

        cloned_line = line.clone()
        self.assertNotEqual(cloned_line.id, line.id)
        self.assertNotEqual(cloned_line.name, line.name)
        self.assertEqual(
            cloned_line.name,
            f'{b.quotation_number}-{line.stl_file.showname}-2'
        )
        self.assertEqual(cloned_line.stl_file, line.stl_file)
        self.assertEqual(cloned_line.scale, line.scale)

        # Test attribute independency
        self.assertEqual(line.scale, 1.3)
        self.assertTrue(line.update({'scale': 1.8}))
        line.refresh_from_db()
        cloned_line.refresh_from_db()
        self.assertEqual(line.scale, 1.8)
        self.assertEqual(cloned_line.scale, 1.3)

    def test_pricing_to_manual_pricing(self):
        b = BasketFactory()
        line = b.add_empty_line(StlFileFactory())

        sr = StockRecordFactory()
        line.update({
            'product': sr.product,
            'stockrecord': sr
        })
        self.assertFalse(line.is_manual_pricing_required)

        # Add condition
        sr.add_manual_pricing_rule(PriceGreaterCondition(max_price=1))
        line.update({
            'product': sr.product,
            'stockrecord': sr
        })
        self.assertTrue(line.is_manual_pricing_required)

        # Other stockrecord
        sr_2 = StockRecordFactory()
        line.update({
            'product': sr_2.product,
            'stockrecord': sr_2
        })
        self.assertFalse(line.is_manual_pricing_required)

    def test_dimensions_in_unit_returns_correctly(self):
        b = BasketFactory()
        stlfile = StlFileFactory()
        line = b.add_empty_line(
            stlfile,
            scale=2.3,
            unit=Configuration.INCH)
        dim = line.dimensions_in_unit
        self.assertAlmostEqual(
            dim['h'],
            stlfile.parameter.h *
            Configuration.MM_TO_INCH *
            2.3)
        self.assertAlmostEqual(
            dim['w'],
            stlfile.parameter.w *
            Configuration.MM_TO_INCH *
            2.3)
        self.assertAlmostEqual(
            dim['d'],
            stlfile.parameter.d *
            Configuration.MM_TO_INCH *
            2.3)

    def test_dimensions_in_unit_fails(self):
        b = BasketFactory()
        line = b.add_empty_line(
            StlFileFactory(parameter=None),
            scale=2.3,
            unit=Configuration.INCH)
        dim = line.dimensions_in_unit
        self.assertIsNone(dim)


class BasketMergeTest(TestCase):
    def setUp(self):
        super(BasketMergeTest, self).setUp()
        self.stlfile = StlFileFactory()
        self.basket1 = BasketFactory()
        self.basket2 = BasketFactory()

    def test_merge_same_supplier_baskets(self):
        p1 = PartnerFactory()
        sr1 = StockRecordFactory(partner=p1)
        line1, created = self.basket1.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)

        line2, created = self.basket2.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)

        self.basket1.merge(self.basket2, add_quantities=False)
        self.assertEqual(self.basket1.num_lines, 2)
        self.assertEqual(self.basket2.num_lines, 0)
        self.assertEqual(self.basket1.lines.order_by('pk')[0].id, line1.id)
        self.assertEqual(self.basket1.lines.order_by('pk')[1].id, line2.id)

    def test_merge_no_supplier_basket(self):
        line1 = self.basket1.add_empty_line(self.stlfile, quantity=1)
        line2 = self.basket2.add_empty_line(self.stlfile, quantity=1)

        basket1_lines = self.basket1.lines.order_by('pk')
        self.basket1.merge(self.basket2, add_quantities=False)
        self.assertEqual(self.basket1.num_lines, 2)
        self.assertEqual(self.basket2.num_lines, 0)
        self.assertEqual(basket1_lines[0].id, line1.id)
        self.assertEqual(basket1_lines[1].id, line2.id)
        self.assertIsNone(basket1_lines[0].product)
        self.assertIsNone(basket1_lines[0].stockrecord)
        self.assertIsNone(basket1_lines[1].product)
        self.assertIsNone(basket1_lines[1].stockrecord)

    def test_merge_only_one_has_supplier(self):
        p1 = PartnerFactory()
        sr1 = StockRecordFactory(partner=p1)
        line1, created = self.basket1.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)

        line2 = self.basket2.add_empty_line(self.stlfile, quantity=1)

        basket1_lines = self.basket1.lines.order_by('pk')
        self.basket1.merge(self.basket2, add_quantities=False)
        self.assertEqual(self.basket1.num_lines, 2)
        self.assertEqual(self.basket2.num_lines, 0)
        self.assertEqual(basket1_lines[0].id, line1.id)
        self.assertEqual(basket1_lines[1].id, line2.id)
        self.assertEqual(basket1_lines[0].stockrecord.id, sr1.id)
        self.assertIsNone(basket1_lines[1].stockrecord)

    def test_merge_different_suppliers_basket(self):
        p1 = PartnerFactory()
        sr1 = StockRecordFactory(partner=p1)
        line1, created = self.basket1.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)

        p2 = PartnerFactory(code='second-parter')
        sr2 = StockRecordFactory(partner=p2)
        line2, created = self.basket2.add_product(
            self.stlfile,
            product=sr2.product,
            quantity=1,
            stockrecord=sr2)

        basket1_lines = self.basket1.lines.order_by('pk')
        self.basket1.merge(self.basket2, add_quantities=False)
        self.assertEqual(self.basket1.num_lines, 2)
        self.assertEqual(self.basket2.num_lines, 0)
        self.assertEqual(basket1_lines[0].id, line1.id)
        self.assertEqual(basket1_lines[1].id, line2.id)
        self.assertNotEqual(
            self.basket1.lines.all()[0].stockrecord.id,
            self.basket1.lines.all()[1].stockrecord.id)

    def test_merge_empty_basket_into_basket_with_items(self):
        p1 = PartnerFactory()
        sr1 = StockRecordFactory(partner=p1)
        line1, created = self.basket1.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)
        line2 = self.basket1.add_empty_line(self.stlfile, quantity=1)

        self.basket1.merge(self.basket2, add_quantities=False)
        self.assertEqual(self.basket1.num_lines, 2)
        self.assertEqual(self.basket2.num_lines, 0)
        self.assertEqual(self.basket1.lines.order_by('pk')[0].id, line1.id)
        self.assertEqual(self.basket1.lines.order_by('pk')[1].id, line2.id)

    def test_merge_basket_into_empty_basket(self):
        p1 = PartnerFactory()
        sr1 = StockRecordFactory(partner=p1)
        line1, created = self.basket1.add_product(
            self.stlfile,
            product=sr1.product,
            quantity=1,
            stockrecord=sr1)
        line2 = self.basket1.add_empty_line(self.stlfile, quantity=1)

        self.basket2.merge(self.basket1, add_quantities=False)
        self.assertEqual(self.basket2.num_lines, 2)
        self.assertEqual(self.basket1.num_lines, 0)
        self.assertEqual(self.basket2.lines.all()[0].id, line1.id)
        self.assertEqual(self.basket2.lines.all()[1].id, line2.id)
