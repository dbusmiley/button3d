from django.core.management.base import BaseCommand
from apps.basket.models import Basket, Line

import zlib


class Command(BaseCommand):
    help = 'Updates the basket line references'

    def handle(self, *args, **kwargs):
        for line in Line.objects.all():
            if line.basket.status in \
                    (Basket.OPEN,
                     Basket.SAVED,
                     Basket.EDITABLE,
                     Basket.FROZEN):
                if line.basket.status == Basket.FROZEN:
                    line.basket.make_editable()
                options = [{
                    'option': attr.option,
                    'value': attr.value
                } for attr in line.attributes.all()]

                new_ref = line.basket._create_line_reference(
                    product=line.product,
                    stockrecord=line.stockrecord,
                    scale=line.scale,
                    options=options,
                    line_id=line.id)
                line.line_reference = new_ref
                line.save()
