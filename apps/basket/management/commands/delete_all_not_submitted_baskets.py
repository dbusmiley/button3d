from django.core.management.base import BaseCommand
from apps.basket.models import Basket


class Command(BaseCommand):
    help = ('Deletes all baskets (by setting a delete_date) '
            'which have not the status "Submitted"')

    def handle(self, *args, **options):
        baskets = Basket.all_objects.exclude(status="Submitted")
        for basket in baskets:
            if basket.status != "Submitted":
                print(("Deleting %s" % basket))
                basket.delete()
