import typing as t
import zlib
from decimal import Decimal
from random import randint

from django.conf import settings
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.db import models
from django.db.models import Sum
from django.utils import timezone

from apps.b3_core.models import Configuration
from apps.b3_core.utils import quantize, get_default_currency, \
    NoPricingPossibleException, ManualPricingRequiredException
from apps.partner.pricing.calculators import PartPriceCalculator
from apps.partner.pricing.price import Price
from apps.partner.strategy import PurchaseInfo
from apps.partner import availability
import button3d.type_declarations as td

__all__ = (
    'AbstractBasket',
    'AbstractLine',
    'random_line_ref',
)


class AbstractBasket(models.Model):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        related_name='baskets',
        on_delete=models.CASCADE,
        verbose_name="Owner"
    )

    date_created = models.DateTimeField("Date created", auto_now_add=True)
    date_merged = models.DateTimeField("Date merged", null=True, blank=True)
    date_submitted = models.DateTimeField("Date submitted", null=True,
                                          blank=True)

    # Statuses
    OPEN, MERGED, SAVED, FROZEN, SUBMITTED, EDITABLE = (
        "Open", "Merged", "Saved", "Frozen", "Submitted", "Editable")
    STATUS_CHOICES = (
        (OPEN, "Open - currently active"),
        (MERGED, "Merged - superceded by another basket"),
        (SAVED, "Saved - for items to be purchased later"),
        (FROZEN, "Frozen - the basket cannot be modified"),
        (SUBMITTED, "Submitted - has been ordered at the checkout"),
        (EDITABLE, "Editable - the basket can be altered"),
    )
    status = models.CharField(
        "Status", max_length=128, default=OPEN, choices=STATUS_CHOICES
    )
    editable_statuses = (OPEN, SAVED, EDITABLE)

    # NOTE: Managers not copied from oscar as they are all overridden

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(AbstractBasket, self).__init__(*args, **kwargs)

        # We keep a cached copy of the basket lines as we refer to them often
        # within the same request cycle.  Also, applying offers will append
        # discount data to the basket lines which isn't persisted to the DB and
        # so we want to avoid reloading them as this would drop the discount
        # information.
        self._lines: t.List['AbstractLine'] = None
        self.offer_applications = []  # Empty list since we don't use this

    # ========
    # Strategy
    # ========

    @property
    def has_strategy(self):
        return hasattr(self, '_strategy')

    def _get_strategy(self):
        if not self.has_strategy:
            raise RuntimeError(
                "No strategy class has been assigned to this basket. "
                "This is normally assigned to the incoming request in "
                "oscar.apps.basket.middleware.BasketMiddleware. "
                "Since it is missing, you must be doing something different. "
                "Ensure that a strategy instance is assigned to the basket!"
            )
        return self._strategy

    def _set_strategy(self, strategy):
        self._strategy = strategy

    strategy = property(_get_strategy, _set_strategy)

    def all_lines(self):
        """
        Return a cached set of basket lines.

        This is important for offers as they alter the line models and you
        don't want to reload them from the DB as that information would be
        lost.
        """
        if self.id is None:
            return self.lines.none()
        if self._lines is None:
            self._lines = (
                self.lines
                    .select_related('product', 'stockrecord')
                    .order_by(self._meta.pk.name)
            )
        return self._lines

    def flush(self):
        """
        Remove all lines from basket.
        """
        if self.status == self.FROZEN:
            raise PermissionDenied("A frozen basket cannot be flushed")
        self.lines.all().delete()
        self._lines = None

    def add_empty_line(self,
                       stlfile: td.StlFile, quantity: int = 1,
                       scale: float = 1,
                       unit: str = Configuration.MM) -> 'AbstractLine':
        if self.is_deleted:
            raise ValueError('Cannot add products to a deleted basket')
        if self.has_manual_pricing_request_sent or self.is_manually_priced:
            raise ValueError('Cannot add products to a manually priced basket')
        if not stlfile:
            raise ValueError('Cannot add products without stlfile')

        configuration = Configuration.objects.create(scale=scale, unit=unit)
        line = self.lines.create(
            stl_file=stlfile,
            product=None,
            stockrecord=None,
            configuration=configuration,
            quantity=quantity)
        line.name = self.line_name_assigner.get_name(line.stl_file)
        line.save()

        return line

    # FIXME: This is a weird API and 2 parameters aren't used.
    @staticmethod
    def price_line(line: 'AbstractLine',
                   created: bool, request: td.HttpRequest = None):
        """
        Price a line.

        The product and stock record must already be present on the line.

        :param line: The line to price
        :param created: No clue
        :param request: Unused
        """

        try:
            price_calculator = PartPriceCalculator(
                stl_file=line.stl_file,
                stock_record=line.stockrecord,
                post_processings=line.post_processings,
                scale=line.scale,
                quantity=line.quantity
            )
            price = price_calculator.unit_price
        except ManualPricingRequiredException:
            line.is_manual_pricing_required = True
            line.price_excl_tax = None
            line.price_incl_tax = None
            line.discount_excl_tax = 0
            line.discount_incl_tax = 0
        except NoPricingPossibleException:
            line.is_manual_pricing_required = False
            line.price_excl_tax = None
            line.price_incl_tax = None
            line.discount_excl_tax = 0
            line.discount_incl_tax = 0
        else:
            discounts = price_calculator.item_discount
            line.discount_excl_tax = discounts.excl_tax
            line.discount_incl_tax = discounts.incl_tax

            line.is_manual_pricing_required = False
            line.price_excl_tax = price.excl_tax
            line.price_incl_tax = price.incl_tax

        line.price_currency = line.partner.price_currency
        line.save()

        # Returning the line is useful when overriding this method.
        return line, created

    # FIXME: Clean this API now that we do not need to adhere to Oscar protocol
    def add_product(self,
                    stlfile: td.StlFile, product: td.Product,
                    quantity: int = 1, scale: float = 1,
                    unit: str = Configuration.MM,
                    stockrecord: td.StockRecord = None,
                    post_processings: t.Union[
                        td.PostProcessings, t.Sequence[td.StringKeyDict]
                    ] = None):
        """
        Add a product to the basket

        Creates a new line using the product and related information and
        adds it to this basket.

        :returns: A 2-tuple with the newly created line and a boolean saying it
                  was created. It's unclear why this exists, as it's always
                  created and the value is never checked.
        line: the matching basket line
        created: whether the line was created or updated
        """
        if not self.id:
            self.save()

        new_line = self.add_empty_line(stlfile, quantity, scale, unit)
        new_line.update({
            'product': product,
            'stockrecord': stockrecord,
            'post_processings': post_processings
        })
        return self.price_line(new_line, created=True)

    add_product.alters_data = True
    add = add_product

    def applied_offers(self):
        raise ReferenceError('Offers should not be used')

    def reset_offer_applications(self):
        raise ReferenceError('Offers should not be used')

    def merge_line(self, line: 'AbstractLine', add_quantities: bool = True):
        """
        Transfer a line from another basket to this one.

        This is used with the "Saved" basket functionality and when a user
        logs in with a (partially) setup basket.
        """
        try:
            existing_line = self.lines.get(line_reference=line.line_reference)
        except ObjectDoesNotExist:
            # Line does not already exist - reassign its basket
            line.basket = self
            line.save()
        else:
            # Line already exists - assume the max quantity is correct and
            # delete the old
            if add_quantities:
                existing_line.quantity += line.quantity
            else:
                existing_line.quantity = max(existing_line.quantity,
                                             line.quantity)
            existing_line.save()
            line.delete()
        finally:
            self._lines = None

    merge_line.alters_data = True

    def merge(self, basket: 'AbstractBasket', add_quantities: bool = True):
        """
        Merge another basket with this one

        Mostly used when an anonymous user created an account or logs in.
        We then want to merge the active anonymous basket into the active
        basket of the user.

        :param basket: The (new) basket to merge into this one. This new
        basket is deleted at the end.
        :param add_quantities: Deprecated: is not used since merging
            lines actually not happen due to random line_references
        """
        if not add_quantities:
            import warnings
            warnings.warn(
                'Deprecated: add_quantities is forced to true and support '
                'will be removed', DeprecationWarning
            )
        for line_to_merge in basket.lines.all():
            self.merge_line(line_to_merge, add_quantities=True)
        basket.delete()  # Basket not required anymore

    merge.alters_data = True

    def freeze(self):
        """
        Freezes the basket so it cannot be modified.
        """
        self.status = self.FROZEN
        self.save()

    freeze.alters_data = True

    def thaw(self):
        """
        Unfreezes a basket so it can be modified again
        """
        self.status = self.OPEN
        self.save()

    thaw.alters_data = True

    def submit(self):
        """
        Mark this basket as submitted
        """
        self.status = self.SUBMITTED
        self.date_submitted = timezone.now()
        self.save()

    submit.alters_data = True

    # Kept for backwards compatibility
    set_as_submitted = submit

    # FIXME: This is used in one place, but we don't really need it
    @staticmethod
    def _create_line_reference(product, stockrecord, options):
        """
        Returns a reference string for a line based on the item
        and its options.
        """
        base = '%s_%s' % (product.id, stockrecord.id)
        if not options:
            return base
        repr_options = [{
            'option': repr(option['option']),
            'value': repr(option['value'])
        } for option in options]
        return "%s_%s" % (base, zlib.crc32(repr(repr_options).encode('utf8')))

    def _get_total(self, fieldname: str):
        """
        For executing a named method on each line of the basket
        and returning the total.
        """
        total = Decimal('0.00')
        for line in self.all_lines():
            try:
                total += getattr(line, fieldname)
            except ObjectDoesNotExist:
                # Handle situation where the product may have been deleted
                pass
            except TypeError:
                # Handle Unavailable products with no known price
                raise
        return total

    # ==========
    # Properties
    # ==========

    @property
    def is_empty(self):
        """
        Test if this basket is empty
        """
        return self.id is None or self.num_lines == 0

    @property
    def is_tax_known(self):
        """
        Test if tax values are known for this basket
        """
        return all([line.is_tax_known for line in self.all_lines()])

    @property
    def total_excl_tax(self):
        """
        Return total line price excluding tax
        """
        return self._get_total('line_price_excl_tax_incl_discounts')

    @property
    def total_tax(self):
        """Return total tax for a line"""
        return self._get_total('line_tax')

    @property
    def total_incl_tax(self):
        """
        Return total price inclusive of tax and discounts
        """
        return self._get_total('line_price_incl_tax_incl_discounts')

    @property
    def total_incl_tax_excl_discounts(self):
        """
        Return total price inclusive of tax but exclusive discounts
        """
        return self._get_total('line_price_incl_tax')

    @property
    def total_discount(self):
        return self._get_total('discount_value')

    @property
    def offer_discounts(self):
        """
        Return basket discounts from non-voucher sources.  Does not include
        shipping discounts.
        """
        return []

    @property
    def voucher_discounts(self):
        """
        Return discounts from vouchers
        """
        return []

    @property
    def has_shipping_discounts(self):
        return len(self.shipping_discounts) > 0

    @property
    def shipping_discounts(self):
        """
        DEPRECATED: offer applications are not used by us and replaced by
        b3_voucher, which has it's own API.
        Oscar does call  them, so return stuff that adheres to the API but
        is "not there".
        """
        return []

    @property
    def post_order_actions(self):
        """
        Return discounts from vouchers
        """
        return []

    @property
    def grouped_voucher_discounts(self):
        """
        Return discounts from vouchers but grouped so that a voucher which
        links to multiple offers is aggregated into one object.
        """
        return []

    @property
    def total_excl_tax_excl_discounts(self):
        """
        Return total price excluding tax and discounts
        """
        return self._get_total('line_price_excl_tax')

    @property
    def num_lines(self):
        """Return number of lines"""
        return self.all_lines().count()

    @property
    def num_items(self):
        """Return number of items"""
        return sum(line.quantity for line in self.lines.all())

    @property
    def num_items_without_discount(self):
        num = 0
        for line in self.all_lines():
            num += line.quantity_without_discount
        return num

    @property
    def num_items_with_discount(self):
        num = 0
        for line in self.all_lines():
            num += line.quantity_with_discount
        return num

    @property
    def time_before_submit(self):
        if not self.date_submitted:
            return None
        return self.date_submitted - self.date_created

    @property
    def time_since_creation(self, test_datetime=None):
        if not test_datetime:
            test_datetime = timezone.now()
        return test_datetime - self.date_created

    @property
    def contains_a_voucher(self):
        return False

    @property
    def is_submitted(self):
        return self.status == self.SUBMITTED

    @property
    def can_be_edited(self):
        """
        Test if a basket can be edited
        """
        return self.status in self.editable_statuses

    @property
    def currency(self):
        # Since all lines should have the same currency, return the currency of
        # the first one found.
        for line in self.all_lines():
            return line.price_currency

    # =============
    # Query methods
    # =============

    def contains_voucher(self, code):
        return False

    def product_quantity(self, product):
        """
        Return the quantity of a product in the basket

        The basket can contain multiple lines with the same product, but
        different options and stockrecords. Those quantities are summed up.
        """
        matching_lines = self.lines.filter(product=product)
        quantity = matching_lines.aggregate(Sum('quantity'))['quantity__sum']
        return quantity or 0

    # FIXME: This is most likely unused, and the only usage of line refs
    def line_quantity(self, product, stockrecord, options=None):
        """
        Return the current quantity of a specific product and options
        """
        ref = self._create_line_reference(product, stockrecord, options)
        try:
            return self.lines.get(line_reference=ref).quantity
        except ObjectDoesNotExist:
            return 0


def random_line_ref():
    return str(randint(1 << 64, 1 << 128))  # nosec: Not security relevant


class AbstractLine(models.Model):
    basket = models.ForeignKey(
        'basket.Basket',
        on_delete=models.CASCADE,
        related_name='lines',
        verbose_name="Basket"
    )
    # This is to determine which products belong to the same line
    # We can't just use product.id as you can have customised products
    # which should be treated as separate lines.  Set as a
    # SlugField as it is included in the path for certain views.
    line_reference = models.SlugField(
        "Line Reference",
        max_length=128,
        db_index=True,
        allow_unicode=False,
        default=random_line_ref,
    )

    product = models.ForeignKey(
        'catalogue.Product',
        on_delete=models.CASCADE,
        related_name='basket_lines',
        verbose_name='Product',
        null=True,
    )

    # We store the stockrecord that should be used to fulfil this line.
    stockrecord = models.ForeignKey(
        'partner.StockRecord',
        on_delete=models.CASCADE,
        related_name='basket_lines',
        null=True,
    )

    quantity = models.PositiveIntegerField('Quantity', default=1)

    # We store the unit price incl tax of the product when it is first added to
    # the basket.  This allows us to tell if a product has changed price since
    # a person first added it to their basket.
    price_currency = models.CharField(
        "Currency", max_length=12, default=get_default_currency
    )
    price_excl_tax = models.DecimalField(
        'Price excl. Tax', decimal_places=2, max_digits=12,
        null=True)
    price_incl_tax = models.DecimalField(
        'Price incl. Tax', decimal_places=2, max_digits=12, null=True)

    # Track date of first addition
    date_created = models.DateTimeField("Date Created", auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(AbstractLine, self).__init__(*args, **kwargs)
        # Instance variables used to persist discount information
        self._discount_excl_tax = Decimal('0.00')
        self._discount_incl_tax = Decimal('0.00')
        self._affected_quantity = 0

    class Meta:
        abstract = True
        app_label = 'basket'
        verbose_name = 'Basket line'
        verbose_name_plural = 'Basket lines'

    def __str__(self):
        return f'Basket #{self.basket.pk}, Product #{self.product.pk}' \
               f', quantity {self.quantity}'

    def save(self, *args, **kwargs):
        if not self.basket.can_be_edited:
            status = self.basket.status.lower()
            raise PermissionDenied(
                f"You cannot modify a {status} basket"
            )
        return super().save(*args, **kwargs)

    # =============
    # Offer methods
    # =============

    def clear_discount(self):
        """
        Remove any discounts from this line.
        """
        self._discount_excl_tax = Decimal('0.00')
        self._discount_incl_tax = Decimal('0.00')
        self._affected_quantity = 0

    def discount(self, discount_value, affected_quantity, incl_tax=True):
        """
        Apply a discount to this line
        """
        if incl_tax:
            if self._discount_excl_tax > 0:
                raise RuntimeError(
                    "Attempting to discount the tax-inclusive price of a line "
                    "when tax-exclusive discounts are already applied")
            self._discount_incl_tax += discount_value
        else:
            if self._discount_incl_tax > 0:
                raise RuntimeError(
                    "Attempting to discount the tax-exclusive price of a line "
                    "when tax-inclusive discounts are already applied")
            self._discount_excl_tax += discount_value
        self._affected_quantity += int(affected_quantity)

    def consume(self, quantity):
        """
        Mark all or part of the line as 'consumed'

        Consumed items are no longer available to be used in offers.
        """
        if quantity > self.quantity - self._affected_quantity:
            inc = self.quantity - self._affected_quantity
        else:
            inc = quantity
        self._affected_quantity += int(inc)

    def get_price_breakdown(self):
        """
        Return a breakdown of line prices after discounts have been applied.

        Returns a list of (unit_price_incl_tax, unit_price_excl_tax, quantity)
        tuples.
        """
        if not self.is_tax_known:
            raise RuntimeError("A price breakdown can only be determined "
                               "when taxes are known")
        prices = []
        if not self.discount_value:
            prices.append((self.unit_price_incl_tax, self.unit_price_excl_tax,
                           self.quantity))
        else:
            # Need to split the discount among the affected quantity
            # of products.
            item_incl_tax_discount = (
                self.discount_value / int(self._affected_quantity))
            item_excl_tax_discount = item_incl_tax_discount * self._tax_ratio
            item_excl_tax_discount = quantize(item_excl_tax_discount)
            prices.append((self.unit_price_incl_tax - item_incl_tax_discount,
                           self.unit_price_excl_tax - item_excl_tax_discount,
                           self._affected_quantity))
            if self.quantity_without_discount:
                prices.append((self.unit_price_incl_tax,
                               self.unit_price_excl_tax,
                               self.quantity_without_discount))
        return prices

    # =======
    # Helpers
    # =======

    @property
    def _tax_ratio(self):
        if not self.unit_price_incl_tax:
            return 0
        return self.unit_price_excl_tax / self.unit_price_incl_tax

    # ==========
    # Properties
    # ==========

    @property
    def has_discount(self):
        return self.quantity > self.quantity_without_discount

    @property
    def quantity_with_discount(self):
        return self._affected_quantity

    @property
    def quantity_without_discount(self):
        return int(self.quantity - self._affected_quantity)

    @property
    def is_available_for_discount(self):
        return self.quantity_without_discount > 0

    @property
    def discount_value(self):
        # Only one of the incl- and excl- discounts should be non-zero
        return max(self._discount_incl_tax, self._discount_excl_tax)

    @property
    def purchase_info(self):
        """
        Return the stock/price info
        """
        if not hasattr(self, '_info'):
            # Cache the PurchaseInfo instance.
            excl_tax = self.price_excl_tax or Decimal(0)
            incl_tax = self.price_incl_tax or Decimal(0)
            tax = max(incl_tax - excl_tax, Decimal(0))
            self._info = PurchaseInfo(
                price=Price(
                    currency=self.price_currency,
                    excl_tax=excl_tax,
                    tax=tax
                ),
                availability=availability.Available,
                stockrecord=self.stockrecord
            )

        return self._info

    @property
    def is_tax_known(self):
        return self.purchase_info.price.is_tax_known

    @property
    def unit_effective_price(self):
        """
        The price to use for offer calculations
        """
        return self.purchase_info.price.effective_price

    @property
    def unit_price_excl_tax(self):
        return self.purchase_info.price.excl_tax

    @property
    def unit_price_incl_tax(self):
        return self.purchase_info.price.incl_tax

    @property
    def unit_tax(self):
        return self.purchase_info.price.tax

    @property
    def line_price_excl_tax(self):
        if self.unit_price_excl_tax is not None:
            return self.quantity * self.unit_price_excl_tax

    @property
    def line_price_excl_tax_incl_discounts(self):
        if self._discount_excl_tax and self.line_price_excl_tax is not None:
            return self.line_price_excl_tax - self._discount_excl_tax
        if self._discount_incl_tax and self.line_price_incl_tax is not None:
            # This is a tricky situation.  We know the discount as calculated
            # against tax inclusive prices but we need to guess how much of the
            # discount applies to tax-exclusive prices.  We do this by
            # assuming a linear tax and scaling down the original discount.
            return self.line_price_excl_tax \
                - self._tax_ratio * self._discount_incl_tax
        return self.line_price_excl_tax

    @property
    def line_price_incl_tax_incl_discounts(self):
        # We use whichever discount value is set.  If the discount value was
        # calculated against the tax-exclusive prices, then the line price
        # including tax
        if self.line_price_incl_tax is not None:
            return self.line_price_incl_tax - self.discount_value

    @property
    def line_tax(self):
        if self.is_tax_known:
            return self.quantity * self.unit_tax

    @property
    def line_price_incl_tax(self):
        if self.unit_price_incl_tax is not None:
            return self.quantity * self.unit_price_incl_tax

    @property
    def description(self) -> str:
        d = str(self.product)
        ops = []
        for attribute in self.attributes.all():
            ops.append("%s = '%s'" % (attribute.option.name, attribute.value))
        if ops:
            d = "%s (%s)" % (d, ", ".join(ops))
        return d

    def get_warning(self):
        raise ReferenceError('This method should not be used')
