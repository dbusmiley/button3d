from django.db import models

from apps.b3_attachement.models import AbstractAttachment
from apps.basket.models import Basket, Line


class BasketAttachment(AbstractAttachment):
    """
    An to a basket.
    An attachment either belongs directly to a basket line or
    or to no specific line (general basket attachment).
    In the latter case the basket_line is null.
    """

    ATTACHMENT_FOLDER = "basket_attachments"
    basket = models.ForeignKey(Basket, related_name="attachments")
    basket_line = models.ForeignKey(
        Line,
        null=True,
        blank=True,
        related_name="attachments"
    )

    def to_dict(self):
        dict = super(BasketAttachment, self).to_dict()
        dict['basket_id'] = self.basket.pk
        dict['basket_line_id'] = self.basket_line.pk if self.basket_line \
            else None
        return dict
