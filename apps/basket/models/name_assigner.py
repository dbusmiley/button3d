class BasketLineNameAssigner:
    """ Name structure: quotationNumber-stlfileName-number
    """

    def __init__(self, basket):
        self.basket = basket
        self.current_number = 1

        self._calculate_current_number()

    def _calculate_current_number(self):
        # Get all numbers distributed at the end of files
        numbers_in_filenames = []
        for line in self.basket.lines.all():
            if line.name and '-' in line.name:
                pos = line.name.rfind('-')
                end_of_name = line.name[pos + 1:]
                if end_of_name.isdigit():
                    numbers_in_filenames.append(int(end_of_name))
        # Find first number that is not taken
        i = 1
        while i in numbers_in_filenames:
            i += 1
        self.current_number = i

    def _get_next_number(self):
        self._calculate_current_number()
        return self.current_number

    def get_name(self, stlfile, number=None):
        file_number = number if number is not None else self._get_next_number()

        return (
            f'{self.basket.quotation_number}'
            f'-{stlfile.showname}-{file_number}'
        )
