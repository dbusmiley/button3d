import uuid

import time
from django.db import models
from django.utils.timezone import now

from apps.b3_attachement.models import AUTH_USER_MODEL
from apps.b3_core.models import AbstractDeletedDateModel
from apps.basket.models import Basket


class Comment(AbstractDeletedDateModel):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    parent = models.ForeignKey('basket.Comment', null=True)
    text = models.CharField(max_length=1024, default='')
    created = models.DateTimeField(default=now, editable=False)
    edited = models.DateTimeField(default=now, editable=False)
    user = models.ForeignKey(
        AUTH_USER_MODEL,
        related_name='comments',
        null=True,
        verbose_name='User'
    )
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE,
                               related_name='comments')

    def save(self, *args, **kwargs):
        self.edited = now()
        super().save(*args, **kwargs)

    @property
    def pretty_uuid(self):
        return 'comments-' + str(self.uuid)

    def to_dict(self):
        # Determine supplier
        service = None
        basket = self.basket
        if basket.is_submitted or basket.has_manual_pricing_request_sent:
            users_to_check = list(basket.first_line.partner.users.all())
            if self.user in users_to_check:
                service = basket.first_line.partner.display_name

        edit_date = None
        difference = abs((self.created - self.edited).total_seconds())
        if difference > 1:
            edit_date = time.mktime(self.edited.timetuple())
        name = self.user.userprofile.full_name \
            if self.user.userprofile \
            else self.user.email
        parent_uuid = self.parent.pretty_uuid \
            if self.parent \
            else None
        ctx = {
            'id': self.pretty_uuid,
            'parent': parent_uuid,
            'text': self.text,
            'user': {
                'id': self.user_id,
                'name': name,
                'service': service,
            },
            'createdPrettyDate': time.mktime(self.created.timetuple()),
            'editedPrettyDate': edit_date if edit_date else None,
        }
        return ctx

    @property
    def get_dictionary(self):
        return self.to_dict()
