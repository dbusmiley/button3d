import json
import logging
import typing as t
from datetime import timedelta, datetime
from decimal import Decimal

from django.core.exceptions import PermissionDenied
from django.core.serializers import serialize
from django.db import models, transaction
from django.urls import reverse
from django.utils import timezone
from guardian.shortcuts import get_objects_for_user, get_users_with_perms

import button3d.type_declarations as td
from apps.b3_core.mail import EmailSender
from apps.b3_core.models import StlFile, Configuration, \
    AbstractDeletedDateModel
from apps.b3_core.utils import quantize, get_default_currency
from apps.b3_organization import utils as org_utils
from apps.b3_organization.models import AbstractSiteModel
from apps.b3_shipping.models import ShippingMethod
from apps.b3_shipping.utils.get_delivery_range import get_delivery_range, \
    get_delivery_range_pretty_printed
from apps.basket.exceptions import NotAllLinesPricedException
from apps.basket.managers import BasketManager, \
    CurrentSiteOpenBasketManager, CurrentSiteSavedBasketManager
from apps.basket.models import BasketLineNameAssigner
from apps.catalogue.models.color import Color
from apps.partner.pricing.price import Price
from .base import AbstractBasket, AbstractLine, random_line_ref

logger = logging.getLogger(__name__)

__all__ = (
    'Basket',
    'Line',
)

""" Manual pricing workflow:
- A line will be set as 'is_manual_pricing_required'
    when priced if it satisfies certain conditions
- These conditions are in partner.conditions.
    Each partner can have a set of rules which can
    be activated with 'partner.manual_pricing_rules_enabled'
- The owner of the basket can then release it to be priced
    manually: basket.set_for_manual_pricing().
    This sets the pricing status of the basket and allows
    a pricing user to price the basket
- The pricing user can now access the basket and set the
    price using: line.set_manual_price()
- Once all prices are set, the pricing user can
    call basket_view.set_basket_as_priced() in order
    to change the basket pricing status to priced
- The basket can now ordered by the owner, but not changed
"""

""" Basket line:
- Can be created empty or with a product/stockrecord
- Updating the basket line, if the product and
    stockrecord are set, will trigger a price recalculation
- Pricing can determine whether it can be done
    automatically, or if a manual pricing is required
    line.is_manual_pricing_required is then set appropriately
- In order to be ordered, the line must have product & stockrecord
    set, and if it need manual pricing, the price should be set
- The price can be set by the pricing user
    through line.set_manual_price(price)
"""


class Basket(AbstractBasket, AbstractSiteModel, AbstractDeletedDateModel):
    title = models.CharField('Project title', max_length=254, null=True)
    reference = models.CharField(
        'Project reference', max_length=254, null=True)
    quotation_number = models.CharField(
        'Quotation number', max_length=50, null=True)

    # Permissions
    VIEW_PERMISSION = 'view_basket'
    EDIT_PERMISSION = 'edit_basket'
    SHARE_PERMISSION = 'share_basket'
    PRICING_PERMISSION = 'price_basket'
    ORDER_PERMISSION = 'order_basket'

    class Meta:
        permissions = (
            ('view_basket', 'Is authorized to open a basket'),
            ('edit_basket', 'Is authorized to edit a basket'),
            ('share_basket', 'Is authorized to share a basket'),
            ('price_basket',
             'Is authorized to set a manual price for a basket'),
            ('order_basket', 'Is authorized to order a basket')
        )
        ordering = ['id']

    NO_REQUEST, WAITING_FOR_MANUAL_PRICING, MANUALLY_PRICED = (
        "No Request", "Waiting For Pricing", "Manually Priced")
    PRICING_STATUS_CHOICES = (
        (NO_REQUEST,
         "No Request - there was no manual pricing requested yet"),
        (WAITING_FOR_MANUAL_PRICING,
         "Waiting For Pricing - the basket needs someone to set a manual price"
         " for one or multiple lines"
         ),
        (MANUALLY_PRICED,
         "Manually Priced - the basket has been priced manually"),
    )

    pricing_status = models.CharField(
        max_length=128, default=NO_REQUEST, choices=PRICING_STATUS_CHOICES)

    # Managers
    objects = BasketManager()
    all_objects = models.Manager()

    open = CurrentSiteOpenBasketManager()
    saved = CurrentSiteSavedBasketManager()

    @property
    def currency(self):
        # Overriden from Oscar to fix bugs with using line.price_currency
        if not self.partner:
            return super().currency
        return self.partner.price_currency

    def refresh_price(self):
        """Reprice all lines to check for updates on discounts"""
        self._lines = self.lines.all()
        for line in self.all_lines():
            self.price_line(line, created=False)

    @property
    def line_name_assigner(self):
        if not hasattr(self, '_line_name_assigner'):
            self._line_name_assigner = BasketLineNameAssigner(self)
        return self._line_name_assigner

    def shipping_method(
        self, method_code, include_deleted_methods=False
    ) -> td.ShippingMethod:
        method_manager = ShippingMethod.objects \
            if not include_deleted_methods else ShippingMethod.all_objects
        return method_manager.get(code=method_code)

    def delivery_time_all_lines(
        self,
        method: td.ShippingMethod,
    ) -> str:
        return get_delivery_range_pretty_printed(self.all_lines(), method)

    def delivery_time_all_lines_time(
        self,
        method: td.ShippingMethod,
    ) -> t.Union[td.TupleOfTwoInts, td.TupleOfTwoNones]:
        return get_delivery_range(self.all_lines(), method)

    def tax_type_partner(self):
        if not self.first_line or not self.first_line.partner:
            return None
        return self.first_line.partner.get_tax_type()

    def vat_rate_partner(self):
        if self.first_line:
            return self.first_line.partner.org_options.vat_rate

    @property
    def first_line(self):
        try:
            return self.all_lines()[0]
        except IndexError:
            return

    def make_editable(self):
        self.status = Basket.EDITABLE
        self.save()

    def clone(self, new_name, owner=None):
        if not new_name:
            raise ValueError('Basket new name must be set')

        cloned_basket = Basket.objects.get(pk=self.pk)
        if owner:
            cloned_basket.owner = owner
        cloned_basket.id = None
        cloned_basket.quotation_number = None
        cloned_basket.title = new_name
        cloned_basket.status = Basket.EDITABLE
        cloned_basket.pricing_status = Basket.NO_REQUEST
        cloned_basket.flush()
        cloned_basket.save()

        for line in self.all_lines():
            line.clone(cloned_basket)

        return cloned_basket

    def flush(self):
        if not self.can_be_edited:
            raise PermissionDenied('Cannot flush basket: cannot be modified')
        self.lines.all().delete()
        self._lines = None

    @property
    def is_frozen(self):
        return self.has_manual_pricing_request_sent or self.is_manually_priced

    def save(self, *args, **kwargs):
        # Do not let more than one basket be saved
        if self.status == self.__class__.SAVED:
            for b in Basket.saved.filter(owner=self.owner):
                b.make_editable()

        return_val = super(Basket, self).save(*args, **kwargs)

        # We need the id to determine the quotation number
        #  (and the quotation number for the title).
        # So we first save, then, if no quotation number exists,
        #   we create it and recursively call save again to
        #   update the instance (and the same procedure for the title)
        if not self.quotation_number:
            from apps.b3_order.utils import OrderNumberGenerator
            generator = OrderNumberGenerator()
            self.quotation_number = generator.order_number(self)
            return_val = self.save()

        if not self.title:
            project = '3D Project'
            self.title = f'{project} {self.quotation_number}'
            return_val = self.save()

        return return_val

    def delete(self, **kwargs):
        if self.is_submitted:
            raise ValueError('Cannot delete submitted basket')
        return super(Basket, self).delete()

    def delete_submitted(self, **kwargs):
        self.status = self.OPEN
        return self.delete()

    def has_empty_lines(self):
        return any([line.is_empty for line in self.lines.all()])

    def all_lines_can_be_ordered(self):
        # TODO: make sure that if all prices are set,
        #   but set_as_priced was not called
        # yet, it must return False.
        # During checkout, basket always changes its status to OPEN,
        # so we cant use basket.is_manually_priced
        # if self.is_manual_pricing_required and not self.is_manually_priced:
        #    return False
        return all([line.can_be_ordered for line in self.lines.all()])

    def has_only_one_supplier(self):
        lines = self.lines.all()
        if len(lines) == 0:
            return True

        supplier = lines[0].partner
        for l in lines:
            if l.partner != supplier:
                return False
        return True

    def has_only_one_currency(self):
        first_currency = self.currency
        return all(line.price_currency == first_currency
                   for line in self.lines.all())

    def update_fields(self, data):
        if self.is_deleted:
            raise ValueError('Cannot update fields of deleted basket')

        if data is None:
            return False

        updated_something = False
        if 'title' in data:
            self._update_title(data['title'])
            updated_something = True
        if 'reference' in data:
            self._update_reference(data['reference'])
            updated_something = True

        if updated_something:
            self.save()
        return updated_something

    def _update_title(self, new_title):
        if not new_title or new_title == "":
            raise ValueError("Basket title cannot be empty")
        self.title = new_title

    def _update_reference(self, new_reference):
        if not new_reference or new_reference == -1 or new_reference == '':
            self.reference = None
        else:
            self.reference = new_reference
        self.save()

    @property
    def partner(self):
        if self.all_lines() and self.all_lines()[0].stockrecord:
            return self.all_lines()[0].stockrecord.partner
        return None

    @property
    def url(self):
        return reverse('b3_user_panel:project-detail', args=(self.id,))

    @property
    def is_manual_pricing_required(self):
        return any(line.is_manual_pricing_required
                   for line in self.all_lines())

    @property
    def is_no_pricing_possible(self):
        return any(line.price_excl_tax is None and
                   not line.is_manual_pricing_required
                   for line in self.all_lines())

    @property
    def are_all_lines_priced(self):
        return all([(not line.is_manual_pricing_required) or
                    line.has_manual_price_set for line in self.lines.all()])

    @property
    def subtotal_lines_excl_tax(self):
        return sum(line.line_price_excl_tax_incl_discounts
                   for line in self.all_lines())

    @property
    def subtotal_lines_incl_tax(self):
        return sum(line.line_price_incl_tax_incl_discounts
                   for line in self.all_lines())

    @property
    def subtotal_price_object(self) -> Price:
        tax_amount = (
            self.subtotal_lines_incl_tax - self.subtotal_lines_excl_tax
        )
        return Price(
            excl_tax=self.subtotal_lines_excl_tax,
            tax=tax_amount,
            currency=self.currency
        )

    def list_extra_fees(self, currency=None) -> t.List[dict]:
        list_of_fees = []
        if hasattr(self, 'order'):
            query = self.order.applied_fees.all()
        else:
            query = self.partner.extra_fees.all()
        for fee in query:
            representation = fee.price_object.as_dict(
                currency or self.currency
            )
            representation['name'] = fee.type
            list_of_fees.append(representation)
        return list_of_fees

    @property
    def total_extra_fee_price_object(self) -> Price:
        if hasattr(self, 'order'):
            return self.order.total
        elif self.partner:
            return self.partner.total_extra_fee_price_object
        else:
            return Price.zero(currency=self.currency)

    @property
    def min_order_price_object(self) -> Price:
        if not self.partner:
            return Price.zero(currency=self.currency)
        return self.partner.min_order_price_object

    def calculate_diff_to_min_price(self) -> Price:
        """
        Creates a new price object based on min_price_obj - (total + fees)
        to show the amount left until (total + fees) > min_price_obj
        """
        tax_amount = self.total_incl_tax - self.total_excl_tax
        total = Price(
            currency=self.currency,
            excl_tax=self.total_excl_tax,
            tax=tax_amount
        )
        total_with_fees = total + self.total_extra_fee_price_object
        min_price_obj = self.min_order_price_object
        if total_with_fees.incl_tax < min_price_obj.incl_tax:
            return min_price_obj - total_with_fees
        return Price.zero(currency=self.currency)

    @property
    def difference_to_minimum_price(self) -> Price:
        if hasattr(self, 'order'):
            return Price.zero(currency=self.currency)
        return self.calculate_diff_to_min_price()

    @property
    def price_object(self) -> Price:
        """
        Price object excluding discounts, excluding shipping but including
        min fee check and total extra fees
        """
        if self.is_empty or not self.all_lines_can_be_ordered():
            return Price.zero(currency=self.currency)
        else:
            min_price_obj = self.min_order_price_object
            tax_amount = (
                self.total_incl_tax_excl_discounts
                - self.total_excl_tax_excl_discounts
            )
            price_obj = Price(
                currency=self.currency,
                excl_tax=self.total_excl_tax_excl_discounts,
                tax=tax_amount
            )
            price_obj = price_obj + self.total_extra_fee_price_object
            if price_obj < min_price_obj:
                return min_price_obj
        return price_obj

    @property
    def price_object_incl_discounts(self) -> Price:
        """
        Price object incl discounts, min fee check and total extra fees,
        excluding shipping
        """
        if self.is_empty or not self.all_lines_can_be_ordered():
            return Price.zero(currency=self.currency)
        else:
            min_price_obj = self.min_order_price_object
            tax_amount = self.total_incl_tax - self.total_excl_tax
            price_obj = Price(
                currency=self.currency,
                excl_tax=self.total_excl_tax,
                tax=tax_amount
            )
            price_obj = price_obj + self.total_extra_fee_price_object
            if price_obj < min_price_obj:
                return min_price_obj
        return price_obj

    def shipping_price_object(self, country) -> Price:
        if self.is_empty or not self.all_lines_can_be_ordered():
            return Price.zero(currency=self.currency)
        shipping_incl_tax, shipping_excl_tax = 0, 0
        shipping_method = self.partner.get_fastest_shipping_method(country)
        if shipping_method:
            shipping_incl_tax = shipping_method.price_incl_tax
            shipping_excl_tax = shipping_method.price_excl_tax
        tax_amount = shipping_incl_tax - shipping_excl_tax
        return Price(
            excl_tax=shipping_excl_tax,
            tax=tax_amount,
            currency=self.currency
        )

    def price_object_with_shipping(self, country) -> Price:
        if self.is_empty or not self.all_lines_can_be_ordered():
            return Price.zero(currency=self.currency)
        shipping_price_obj = self.shipping_price_object(country)
        price_obj = self.price_object_incl_discounts
        return price_obj + shipping_price_obj

    @property
    def discount_price_object(self) -> Price:
        incl_tax = self.total_incl_tax_excl_discounts - self.total_incl_tax
        excl_tax = self.total_excl_tax_excl_discounts - self.total_excl_tax
        tax_amount = incl_tax - excl_tax
        return Price(
            excl_tax=excl_tax,
            tax=tax_amount,
            currency=self.currency
        )

    def can_user_view(self, user, request=None):
        can_view = self.owner == user \
            if user and user.is_authenticated \
            else False
        if not can_view:
            can_view = self.can_user_price(user)
        if not can_view:
            can_view = user.has_perm(Basket.VIEW_PERMISSION, self)
        if not can_view:
            partner_available = self.first_line and self.first_line.partner
            basket_submitted = self.status == Basket.SUBMITTED
            basket_manually_priced = self.is_manually_priced
            basket_orderable = basket_submitted or basket_manually_priced
            if partner_available and basket_orderable:
                partner_users = self.first_line.partner.users.all()
                if partner_users.filter(id=user.id).exists():
                    return True

        if request and hasattr(request, 'basket') and not can_view:
            return request.basket == self
        return can_view

    def can_user_price(self, user):
        """
        Method to check if a user can price the current basket.
        :param user: User instance which will be checked for pricing
        permissions.
        :return: A boolean describing if he/she has access or not.
        """
        user_verified = user and user.is_authenticated
        basket_verified = self.has_manual_pricing_request_sent and \
            self.first_line and self.first_line.partner
        if user_verified and basket_verified:
            return self.first_line.partner.users.filter(id=user.id).count() > 0
        return False

    def can_user_edit(self, user, request=None):
        return self.can_user_view(user=user, request=request)

    def set_for_manual_pricing(self):
        self.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        self.save()

    @property
    def has_manual_pricing_request_sent(self):
        return self.pricing_status == self.WAITING_FOR_MANUAL_PRICING

    @property
    def is_manually_priced(self):
        return self.pricing_status == self.MANUALLY_PRICED

    @classmethod
    def search(cls, owner, **kwargs):
        """Filters baskets on multiple criteria.
        The baskets are returned in descreasing creation date order.
        kwargs parameters:
        - status: open/submitted/None (None means all baskets, and is default)
        - query: string that can be contained by basket name,
            reference, quotation_number or line names
        - date_0: YYYY-mm-dd, baskets created after this date, string
        - date_1: YYYY-mm-dd, baskets created before this date, string
        - page: starting at 1 (and 1 by default), for pagination
        - items_per_page: int, 20 by default
        :return a tuple (list of baskets,
            total number of baskets-without pagination)
        """

        baskets = Basket.objects.filter(
            owner=owner, order__isnull=True
        ).exclude(status=Basket.OPEN, lines=None)

        # Join Baskets which are shared with user.
        if not owner.is_superuser:
            baskets = baskets | get_objects_for_user(
                owner, 'basket.view_basket').exclude(owner=owner)
        status = kwargs.pop('status', None)

        if status:
            if status == 'open':
                baskets = baskets.filter(
                    status__in=[Basket.OPEN, Basket.SAVED, Basket.EDITABLE])
            elif status == 'submitted':
                baskets = baskets.filter(status=Basket.SUBMITTED)

        date_0 = kwargs.pop('date_0', None)
        if date_0:
            # Add timezone to this
            start_date = datetime.strptime(date_0, '%Y-%m-%d').replace(
                tzinfo=timezone.get_current_timezone()
            )
            start_date = start_date
            baskets = baskets.filter(date_created__gte=start_date)

        date_1 = kwargs.pop('date_1', None)

        if date_1:
            # Inclusive this date, so +1 day
            end_date = timezone.datetime.strptime(
                date_1, '%Y-%m-%d').replace(
                tzinfo=timezone.get_current_timezone()
            ) + timedelta(days=1)
            baskets = baskets.filter(date_created__lte=end_date)

        # Evaluation of the query in the correct
        #   order to make the query searches
        baskets = list(baskets.order_by('-date_created'))

        currency = kwargs.pop('currency', None)
        country = kwargs.pop('country', None)
        query = kwargs.pop('query', None)
        if query:
            """Keep baskets where the 'query' is in the
                project title, reference, quotation number
            Or the line name, stl file showname or one
                the basket sums (excl or incl tax)
            Note: It is important for the query that the
                line related parts appear first, else
                it doesn't work somehow
            """

            def baskets_search_filter(basket, query, currency, country):
                query = str(query).lower()

                # Basket attributes
                title_match = basket.title and query in basket.title.lower()
                reference_match = basket.reference and \
                    query in basket.reference.lower()
                quotation_number_match = basket.quotation_number and \
                    query in basket.quotation_number
                # Line name and stlfile showname
                contains_line_name_or_stlfile_showname = any(
                    (l.name and query in l.name.lower()) or
                    query in l.stl_file.showname.lower()
                    for l in basket.lines.all())

                # Prices
                price_obj = basket.price_object_with_shipping(country)
                price_obj = price_obj.as_dict(currency=currency) \
                    if price_obj and price_obj.currency else None
                contains_price = (
                    query.replace(',', '.')
                    in str(price_obj['excl_tax']).replace(',', '.') or
                    query.replace(',', '.')
                    in str(price_obj['incl_tax']).replace(',', '.')) \
                    if price_obj else False
                return any((
                    title_match,
                    reference_match,
                    quotation_number_match,
                    contains_line_name_or_stlfile_showname,
                    contains_price))

            baskets = [
                b for b in baskets if baskets_search_filter(
                    b, query, currency, country)]

        # Total number of baskets, without pagination
        nb_baskets = len(baskets)

        # Handle pages
        items_per_page = kwargs.pop('items_per_page', None)
        if items_per_page:
            items_per_page = int(items_per_page)
            page_number = max(1, int(kwargs.pop('page', 1)))

            first_item_number = (page_number - 1) * items_per_page
            last_item_number = page_number * items_per_page
            baskets = baskets[first_item_number:last_item_number]

        return list(baskets), nb_baskets

    def prepare_for_checkout(self, request):
        if self.status != self.OPEN:
            # Freeze currently open basket
            for b in Basket.objects.filter(
                status=Basket.OPEN,
                owner=self.owner
            ):
                b.make_editable()
            # Update new open basket
            self.thaw()
            request.basket = self

        if hasattr(request, 'session'):
            request.session['ordered_basket_id'] = self.id

    def _check_basket_eligible_for_external_access(self):
        """
        Check if basket might have external users with access
        """
        if (
            self.status is not Basket.SUBMITTED and
            not self.has_manual_pricing_request_sent and
            not self.is_manually_priced
        ):
            return False
        return True

    def _partner_users_exist(self):
        """
        If a basket has at-least a line with a proper partner with partner
        users, this should return True
        """
        if not self.first_line or not self.first_line.partner:
            return False

        return True

    def get_users_with_access(self):
        users = [self.owner] if self.owner else []
        basket_eligible = self.status == Basket.SUBMITTED or \
            self.has_manual_pricing_request_sent or self.is_manually_priced
        if basket_eligible and self.first_line and self.first_line.partner:
            for partner_user in self.first_line.partner.users.all():
                users.append(partner_user)
        for user in get_users_with_perms(self):
            if self.can_user_view(user):
                users.append(user)

        # Return only unique values
        return list(set(users))

    def set_as_manually_priced(self):
        if not (self.are_all_lines_priced and self.is_manual_pricing_required):
            raise NotAllLinesPricedException

        self.pricing_status = Basket.MANUALLY_PRICED
        self.save()
        self.send_manual_pricing_set_confirmation_email()

    def send_manual_pricing_set_confirmation_email(self):
        try:
            self.try_send_manual_pricing_set_confirmation_email()
        except Exception:
            logger.exception(
                (
                    'Error during sending notify email to owner of basket '
                    'that has been priced.'
                )
            )

    def try_send_manual_pricing_set_confirmation_email(self):

        basket_url = f'{org_utils.get_base_url()}{self.url}'
        context = {
            "basket_quotation": self.quotation_number,
            "basket_url": basket_url,
        }

        email_sender = EmailSender()
        email_sender.send_email(
            to=self.owner.email,
            subject_template_path='customer/emails/manual_price/'
                                  'email_manual_priced_subject.txt',
            body_txt_template_path='customer/emails/manual_price/'
                                   'email_manual_priced_body.txt',
            body_html_template_path='customer/emails/manual_price/'
                                    'email_manual_priced_body.html',
            extra_context=context
        )


class Line(AbstractLine, AbstractDeletedDateModel):
    name = models.CharField(max_length=254, null=True)
    configuration = models.OneToOneField(
        Configuration,
        on_delete=models.CASCADE,
        related_name='basket_line',
    )
    is_manual_pricing_required = models.BooleanField(default=False)

    stl_file = models.ForeignKey(
        StlFile,
        on_delete=models.CASCADE,
        to_field='uuid',
        related_name='basket_lines',
    )
    color = models.ForeignKey(
        Color,
        on_delete=models.CASCADE,
        null=True,
        related_name='basket_lines',
    )

    discount_incl_tax = models.DecimalField(
        'Discount incl. Tax', decimal_places=2, max_digits=12, default=0.00,
    )
    discount_excl_tax = models.DecimalField(
        'Discount excl. Tax', decimal_places=2, max_digits=12, default=0.00,
    )

    class Meta:
        # Enforce sorting by order of creation.
        ordering = ['date_created', 'pk']

    def __str__(self):
        product_id = self.product.pk if self.product else '/'
        stockrec_id = self.stockrecord.pk if self.stockrecord else '/'
        return f'Basket line id: #{self.pk}, Basket id: {self.basket.pk}, ' \
               f'Product id: {product_id}, Stockrecord id: {stockrec_id}, ' \
               f'Uuid: {self.uuid}'

    @property
    def discount_price_object(self) -> Price:
        tax_amount = self.discount_incl_tax - self.discount_excl_tax
        return Price(
            excl_tax=self.discount_excl_tax,
            tax=tax_amount,
            currency=self.price_currency
        )

    def line_taxes(self):
        return self.price_incl_tax - self.price_excl_tax

    @property
    def is_empty(self):
        return self.product is None or self.stockrecord is None

    @property
    def uuid(self):
        return self.stl_file.uuid

    @property
    def showname(self):
        if self.name:
            return self.name
        return self.stl_file.showname

    @property
    def configure_url(self):
        return reverse('b3_user_panel:project-line-viewer',
                       args=(self.basket.id, self.id, self.uuid))

    @property
    def thumbnail_url(self):
        return self.stl_file.thumbnail_url

    @property
    def scale(self):
        return float(self.configuration.scale)

    @property
    def measure_unit(self):
        return self.configuration.unit

    @property
    def line_tax(self):
        return self.line_price_incl_tax_incl_discounts \
            - self.line_price_excl_tax_incl_discounts

    @transaction.atomic
    def _post_processing_update(self, post_processings: dict) -> None:
        self.post_processing_options.all().delete()

        for post_processing_option in post_processings:
            post_processing = post_processing_option['post_processing']
            default_color = post_processing.colors.first()
            color = post_processing_option['color'] or default_color
            self.post_processing_options.create(
                post_processing=post_processing,
                color=color,
            )

    def update(self, update_dict):
        """
        Update the line using a dictionary prepared by the frontend

        The view is responsible for providing the correct fields in the
        correct type and everything that blows up here, shows where the hole is
        in that assumption.

        .. Note:
           this whole dict construction in the view is unnecessary and even
           more unnecessary is passing it as dict to a custom method. We can
           simply untangle the dict using the double star operator and pass
           it to save(). Of course, then we need to use correct field names
           (`dict.filename` versus `self.name` for example), which once
           again, would be a task of the view.

        This note is dedicated to the poor soul that has to write the new
        implementation. I wish you much strength.
        """
        if update_dict is None:
            return False

        if self.basket.is_manually_priced or \
                self.basket.has_manual_pricing_request_sent:
            return False

        updated = False

        if 'product' in update_dict:
            self.product = update_dict['product']
            updated = True

        if 'stockrecord' in update_dict:
            self.stockrecord = update_dict['stockrecord']
            if self.stockrecord:
                self.price_currency = self.stockrecord.price_currency
            else:
                self.price_currency = get_default_currency()
            updated = True

        post_processings = update_dict.get('post_processings', None)
        if post_processings is not None:
            self._post_processing_update(post_processings)
            updated = True

        if 'quantity' in update_dict:
            self.quantity = update_dict['quantity']
            updated = True

        if 'filename' in update_dict:
            self.name = update_dict['filename']
            updated = True

        if 'unit' in update_dict:
            self.configuration.update(unit=update_dict['unit'])
            updated = True

        if 'scale' in update_dict:
            self.configuration.update(scale=update_dict['scale'])
            updated = True

        if updated:
            self.save()
            if not self.is_empty:
                self.basket.price_line(self, created=False)

                if hasattr(self, '_info'):
                    del self._info  # Delete old cached information (bad)
            self.save()
        return updated

    @property
    def dimensions(self):
        dimensions = self.stl_file.get_scaled_dimensions(self.scale)
        return dimensions

    @property
    def dimensions_in_unit(self):
        try:
            dimensions = self.stl_file.get_scaled_dimensions(
                self.configuration.scale_in_unit)
        except AttributeError:
            dimensions = None
        return dimensions

    @property
    def download_link(self):
        return self.stl_file.download_optimized_url

    def apply_discount(self, amount):
        if amount == 0:
            self.discount_incl_tax, self.discount_excl_tax = 0, 0
            self.save()
            return amount
        if amount > self.line_price_excl_tax_incl_discounts:
            amount = self.line_price_excl_tax_incl_discounts
        self.discount_excl_tax = amount
        tax_ratio = Decimal(1)
        if self.tax_ratio:
            tax_ratio = self.tax_ratio
        self.discount_incl_tax = quantize(amount / tax_ratio)
        self.save()
        return amount

    @property
    def tax_ratio(self):
        if not self.unit_price_incl_tax:
            return 0
        return self.unit_price_excl_tax / self.unit_price_incl_tax

    @property
    def line_price_incl_tax_incl_discounts(self):
        if self.discount_incl_tax:
            return self.line_price_incl_tax - self.discount_incl_tax
        if self.discount_excl_tax:
            return quantize(self.line_price_incl_tax - self.discount_excl_tax /
                            self._tax_ratio)
        return self.line_price_incl_tax

    @property
    def line_price_excl_tax_incl_discounts(self):
        if self.discount_excl_tax:
            return self.line_price_excl_tax - self.discount_excl_tax
        if self.discount_incl_tax:
            return self.line_price_excl_tax \
                - self._tax_ratio * self.discount_incl_tax
        return self.line_price_excl_tax

    def clone(self, new_basket=None):
        if new_basket is None and self.basket.is_submitted:
            raise ValueError('Cannot clone line into submitted basket')

        cloned_line = Line.objects.get(pk=self.pk)
        cloned_line.id = None
        cloned_line.name = None
        cloned_line.line_reference = random_line_ref()

        if new_basket:
            cloned_line.basket = new_basket
        cloned_line.configuration = self.configuration.clone()
        cloned_line.save()

        cloned_line.name = cloned_line.basket.line_name_assigner.get_name(
            cloned_line.stl_file)
        cloned_line.save()

        for option in self.post_processing_options.all():
            cloned_line.post_processing_options.create(
                post_processing=option.post_processing,
                color=option.color
            )

        return cloned_line

    @property
    def has_manual_price_set(self):
        return self.is_manual_pricing_required and \
            self.price_excl_tax is not None

    @property
    def is_no_pricing_possible(self):
        return not self.is_manual_pricing_required and \
            self.price_excl_tax is None

    @property
    def can_be_ordered(self):
        if self.is_empty:
            return False
        if self.is_no_pricing_possible:
            return False
        if self.is_manual_pricing_required and \
                not self.has_manual_price_set:
            return False
        return True

    def set_manual_price(self, price_excl_tax):
        price = self.partner.create_price(price_excl_tax)
        self.price_excl_tax = price.excl_tax
        self.price_incl_tax = price.incl_tax
        self.save()

    @property
    def price_object(self):
        if self.can_be_ordered:
            price_obj = self.purchase_info.price
            return Price(
                currency=price_obj.currency,
                excl_tax=price_obj.excl_tax,
                tax=price_obj.tax
            )
        return None

    @property
    def partner(self):
        return self.stockrecord.partner if self.stockrecord else None

    @property
    def post_processings(self):
        return [
            option.post_processing
            for option in self.post_processing_options.all()
        ]

    def to_dict(self, currency=None):
        line_json = json.loads(serialize('json', (self,)))[0]['fields']
        # Override information
        if currency and \
                self.partner and \
                line_json.get('price_excl_tax', False) and \
                line_json.get('price_incl_tax', False):
            line_json['price'] = self.price_object.as_dict(currency=currency)
            line_json['discount'] = self.discount_price_object \
                .as_dict(currency=currency)

        # Add stlfile information
        line_json.update(self.stl_file.get_job_status())

        # Add line information
        line_json['showname'] = self.showname
        line_json['stl_file'] = self.uuid
        line_json['scale'] = self.scale
        line_json['measure_unit'] = self.measure_unit
        line_json['configure_url'] = self.configure_url
        line_json['thumbnail_url'] = self.thumbnail_url
        line_json['stl_file_creation_date'] = self.stl_file.creation_date. \
            strftime('%Y-%m-%dT%H:%M:%S')
        line_json['attachments'] = list(
            attachment.to_dict() for attachment in self.attachments.all())
        line_json['is_manual_pricing_required'] = \
            self.is_manual_pricing_required
        line_json['has_manual_price_set'] = self.has_manual_price_set
        line_json['price'] = self.price_object.as_dict(currency=currency) \
            if self.price_object else None
        line_json['dimensions'] = self.dimensions_in_unit
        line_json['project_title'] = self.basket.title
        line_json['project_url'] = self.basket.url

        del line_json['product']
        del line_json['stockrecord']
        if self.product:
            line_json['product_slug'] = self.product.slug
        if self.stockrecord:
            line_json['supplier_slug'] = self.stockrecord.partner.code

        line_json['post_processings'] = [
            {
                'slug': option.post_processing.full_slug,
                'color_id': option.color.id if option.color else None
            }
            for option in self.post_processing_options.all()
        ]

        return line_json


# FIXME: 0xDEADC0DE
def test_rule_applies_to_line(line):
    return lambda rule: rule.is_applying(
        price=line.unit_price_excl_tax,
        quantity=line.quantity
    )


# FIXME: 0xDEADC0DE
def apply_and_get_line_discount(line):
    applying_rules = filter(
        test_rule_applies_to_line(line),
        line.stockrecord.get_discount_rules()
    )
    applicable_discounts = [
        discount
        for rule in applying_rules
        for discount in rule.get_conclusion_actions()
    ]
    # Only the discount with the highest benefit is applied
    if applicable_discounts:
        applicable_discounts.sort(
            key=lambda d: d.get_discountable_amount(line),
            reverse=True
        )
    else:
        return line.apply_discount(Decimal(0))
    return applicable_discounts[0].apply_to(line)
