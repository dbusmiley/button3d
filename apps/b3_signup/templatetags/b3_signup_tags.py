from django import template
from apps.b3_signup.forms import EmailSignupForm, LoginForm
from apps.b3_organization.utils import get_current_org


register = template.Library()


@register.inclusion_tag(
    'account/partials/email_signup_form.html', takes_context=True)
def email_signup_form(context):
    return {
        'form': EmailSignupForm(),
        'upload_uuid': context.get('upload_uuid'),
        'organization': get_current_org()
    }


@register.inclusion_tag('account/partials/login_form.html', takes_context=True)
def login_form(context):
    return {
        'form': LoginForm(),
        'upload_uuid': context.get('upload_uuid'),
        'next': context.request.GET.get('next') or context.request.path
    }
