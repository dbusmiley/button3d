import re

from actstream.models import Action
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from apps.b3_organization.tracking.trackers import RegistrationStateTracker
from apps.b3_organization.tracking.utils import REGISTRATION
from apps.b3_signup.util import send_signup_email
from apps.b3_organization.models import AbstractSiteModel


@python_2_unicode_compatible
class Signup(AbstractSiteModel):
    email = models.EmailField(unique=False, verbose_name=_('E-mail address'))
    confirmed = models.BooleanField(verbose_name=_('Confirmed'), default=False)
    user = models.ForeignKey(User, null=True)
    created = models.DateTimeField(verbose_name=_('Created'),
                                   default=timezone.now)
    key = models.CharField(
        verbose_name=_('Secret Key'),
        max_length=64, unique=True
    )
    sent = models.DateTimeField(verbose_name=_('E-mail sent'), null=True)
    origin_url = models.CharField(
        verbose_name=_("URL where the user pressed 'SignUp'"),
        max_length=1024,
        default=settings.LOGIN_REDIRECT_URL
    )
    order_id_anonymous = models.IntegerField(
        verbose_name=_("Order id when user is anonymous"),
        null=True,
        default=None
    )
    basket_id_anonymous = models.IntegerField(
        verbose_name=_('Basket id when user is anonymous'),
        null=True
    )
    is_printing_service = models.BooleanField(default=False)

    state_changes = GenericRelation(
        Action,
        object_id_field='actor_object_id',
        content_type_field='actor_content_type',
        related_query_name='signup'
    )

    class Meta:
        unique_together = ('email', 'site')

    @classmethod
    def create(cls, email, origin_url=None):
        key = get_random_string(64).lower()
        instance = cls._default_manager.create(
            email=email,
            origin_url=origin_url,
            key=key
        )

        return instance

    @classmethod
    def generate_unique_username(cls, email):
        """
        Create a unique username out of given email address. Username algorithm
        adapted from django-allauth.utils.generate_unique_username
        """
        max_length = User._meta.get_field('username').max_length
        username_from_email = email.split('@')[0].strip()
        username = re.sub('\s+', '_', username_from_email)
        i = 0
        while True:
            if i:
                pfx = str(i + 1)
            else:
                pfx = ''
            ret = username[0:max_length - len(pfx)] + pfx
            try:
                User.objects.get(username=ret)
            except User.DoesNotExist:
                return ret

            i += 1

    def create_user(self):
        self.user = User.objects.create_user(
            username=self.generate_unique_username(self.email),
            email=self.email
        )
        self.user.is_active = True
        self.user.save()
        self.save()

        RegistrationStateTracker.register_state_change(
            self,
            new_state=REGISTRATION.REGISTERED,
            old_state=REGISTRATION.FIRST_STEP
        )

        return self.user

    def send_signup_email(self, request):
        send_signup_email(self, request)

    def __str__(self):
        return "Signup: {0}".format(self.email)


@receiver(post_save, sender=Signup, dispatch_uid='initial_signup_state_change')
def register_initial_signup_state_change(
    sender, instance, created, raw, **kwargs
):
    if created and not raw:
        RegistrationStateTracker.register_state_change(
            instance,
            new_state=REGISTRATION.FIRST_STEP
        )
