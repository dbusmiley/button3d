import logging
import typing as t

from braces.views import LoginRequiredMixin
from django.conf import settings
from django.contrib.auth import update_session_auth_hash, login as django_login
from django.http import Http404, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, View
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectMixin

import apps.b3_user_panel.permissions as user_permissions
from apps.b3_address.forms import UserPanelAddressForm
from apps.b3_address.models import Address
from apps.b3_auth_plugins.utils.generate_auth_url import generate_auth_url
from apps.b3_core.mail import EmailSender
from apps.b3_organization.utils import get_current_org
from apps.b3_signup.forms import \
    EmailSignupForm, CustomSetPasswordForm, ExtendedRegistrationForm, \
    PrintingServiceWelcomeForm
from apps.b3_signup.models import Signup
from apps.b3_signup.signals import user_signed_up
from apps.basket.models import Basket
from .exceptions import ImmediateHttpResponse
from .forms import LoginForm

logger = logging.getLogger(__name__)

sensitive_post_parameters_m = method_decorator(
    sensitive_post_parameters('password', 'password1', 'password2'))


class PopupViewMixin(object):
    """
    Mixin for popups/iframe (hubspot login/signup)
    """
    popup_template_name = None

    def is_popup(self):
        return self.request.GET.get('popup')

    def get_template_names(self):
        if self.is_popup() and self.popup_template_name:
            return [self.popup_template_name]
        return super(PopupViewMixin, self).get_template_names()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['is_popup'] = self.is_popup()
        return context

    def get_success_url(self):
        success_url = super().get_success_url()
        if success_url is None:
            success_url = reverse("b3_organization:dashboard")
        if self.is_popup():
            return "{}?next={}".format(
                reverse('account_popup_success'),
                success_url or settings.LOGIN_REDIRECT_URL
            )
        return success_url


class UploadOwnershipViewMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(UploadOwnershipViewMixin, self) \
            .get_context_data(*args, **kwargs)
        context['upload_uuid'] = self.request.GET.get(
            'upload_uuid',
            self.request.POST.get('upload_uuid', None)
        )
        return context


class LoginView(PopupViewMixin, UploadOwnershipViewMixin, FormView):
    """
    Custom popup-enabled login view
    """
    form_class = LoginForm
    template_name = 'account/login.html'
    popup_template_name = 'account/popup_login.html'
    success_url = reverse_lazy('b3_organization:dashboard')
    redirect_field_name = 'next'

    def get_context_data(self, *args: list, **kwargs: dict) \
            -> t.Dict[str, t.Any]:
        context = super().get_context_data(*args, **kwargs)
        context['next'] = self.request.GET.get(self.redirect_field_name, None)
        return context

    def get(self, request, *args, **kwargs):
        """
        Redirect to oauth login page, if settings found
        """
        curr_org = get_current_org()
        auth_plugin = curr_org.auth_plugins_enabled
        if auth_plugin and curr_org.disable_normal_login and \
                curr_org.automatic_redirect_to_oauth:
            return HttpResponseRedirect(
                generate_auth_url(request, auth_plugin)
            )
        else:
            # Leave the view untouched
            return super(LoginView, self).get(request, *args, **kwargs)

    @sensitive_post_parameters_m
    def dispatch(self, request, *args, **kwargs):
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        try:
            form.login(self.request)
            return super(LoginView, self).form_valid(form)
        except ImmediateHttpResponse as e:
            return e.response

    def get_success_url(self):
        redirect_to = self.request.POST.get(self.redirect_field_name) \
            or self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to


class EmailSignupView(PopupViewMixin, UploadOwnershipViewMixin, FormView):
    """
    Custom popup-enabled signup view
    """
    template_name = 'account/email_signup.html'
    popup_template_name = 'account/popup_email_signup.html'
    form_class = EmailSignupForm

    def dispatch(self, request, *args, **kwargs):
        current_org = get_current_org()
        auth_plugin = current_org.auth_plugins_enabled
        if auth_plugin:
            # Not returning early, as there can be 3 logical combinations
            # possible
            if current_org.automatic_redirect_to_oauth:
                # Redirect directly to the auth page
                return HttpResponseRedirect(
                    generate_auth_url(request, auth_plugin)
                )
            if current_org.disable_normal_login:
                # Just redirect user to the app login page
                return HttpResponseRedirect(reverse('account_login'))

        return super(EmailSignupView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        email = form.cleaned_data['email']
        origin_url = form.cleaned_data['origin_url']
        signup = form.save(email, origin_url)

        # assign current order to Signup
        order_id = self.request.session.get('checkout_order_id', None)
        if order_id and signup.order_id_anonymous is None:
            signup.order_id_anonymous = order_id

        # assign current basket to Signup
        basket = self.request.basket if hasattr(self.request, 'basket') \
            else None
        basket_id = self.request.basket.id if basket and basket.id else None
        if basket_id and signup.basket_id_anonymous is None:
            signup.basket_id_anonymous = basket_id

        # mark Signup as a Printing Service Signup if it is one
        signup.is_printing_service = self.is_printing_service_signup()

        # send verification mail
        signup.send_signup_email(self.request)
        signup.sent = timezone.now()
        signup.save()

        return self.render_to_response(self.get_context_data(email_sent=True))

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def is_printing_service_signup(self):
        return "is_ps" in self.kwargs and self.kwargs["is_ps"]

    def get_context_data(self, *args, **kwargs):
        context = super(EmailSignupView, self) \
            .get_context_data(*args, **kwargs)
        context['is_printing_service_signup'] = \
            self.is_printing_service_signup()
        return context


class EmailSignupConfirmView(
    SingleObjectMixin, UploadOwnershipViewMixin, View
):
    def get_object(self, queryset=None):
        try:
            signup = Signup.objects.get(key=self.kwargs['key'].lower())
        except Signup.DoesNotExist:
            raise Http404()

        # If for this Signup, a user has already been created and a custom
        # password set, the verification key is invalid
        if signup.confirmed:
            raise Http404(_("Your E-Mail address has already been verified."))

        return signup

    def get(self, *args, **kwargs):
        self.object = signup = self.get_object()
        if not signup.user:
            user = signup.create_user()

            # transfer Basket from Signup to user
            if signup.basket_id_anonymous:
                try:
                    basket = Basket.objects.get(
                        id=signup.basket_id_anonymous, owner=None
                    )
                    basket.owner = user
                    basket.save()
                except BaseException:
                    logger.warning(
                        'Basket transfer was not successful after signup'
                    )
        else:
            user = signup.user

        # authenticate (login) the user
        user.backend = 'apps.b3_organization.auth_backend.DomainEmailBackend'
        django_login(self.request, user)

        org = get_current_org()
        if org.is_extended_registration_enabled:
            return redirect('account_email_extended_registration')
        return redirect('account_email_signup_set_password')


class EmailSignupExtendedRegistrationView(LoginRequiredMixin, FormView):
    form_class = ExtendedRegistrationForm
    template_name = 'account/email_signup_extended_registration.html'

    def get_form_kwargs(self):
        kwargs = super(EmailSignupExtendedRegistrationView, self) \
            .get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)

        # mark the Signup as confirmed, since the signup process is finished
        # after setting the password - if the organization does not have force
        # verification enabled. A check for self.request.organization is
        # unnecessary at this point - so skipping
        if not self.request.organization.force_verify_customer_address:
            signup = self.request.user.signup_set.first()
            signup.confirmed = True
            signup.save()

        return super(EmailSignupExtendedRegistrationView, self). \
            form_valid(form)

    def get_success_url(self):
        return reverse('account_email_extended_registration_addresses')


class EmailSignupSetPasswordView(LoginRequiredMixin, FormView):
    form_class = CustomSetPasswordForm
    template_name = 'account/email_signup_set_password.html'

    def get(self, request, *args, **kwargs):
        user_signed_up.send(
            sender=self.__class__, request=request,
            user=request.user
        )
        return super(EmailSignupSetPasswordView, self).get(request=request)

    def get_form_kwargs(self):
        kwargs = super(EmailSignupSetPasswordView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        update_session_auth_hash(self.request, form.user)

        # mark the Signup as confirmed, since the signup process is finished
        # after setting the password
        signup = Signup.objects.get(user=self.request.user)
        signup.confirmed = True
        signup.save()

        return super(EmailSignupSetPasswordView, self).form_valid(form)

    def get_success_url(self):
        signup = Signup.objects.get(email=self.request.user.email)
        if signup.is_printing_service:
            return reverse('account_ps_welcome')
        return signup.origin_url or settings.LOGIN_REDIRECT_URL


class PrintingServiceWelcomeView(LoginRequiredMixin, FormView):
    form_class = PrintingServiceWelcomeForm
    template_name = 'account/printing_service_welcome.html'

    def form_valid(self, form):
        self.send_mail_to_onboarding_user(form.cleaned_data)
        return super(PrintingServiceWelcomeView, self).form_valid(form)

    def send_mail_to_onboarding_user(self, form_context):
        onboarding_user = get_current_org().printing_service_onboarding_user \
            if get_current_org() else None
        context = {"ps": form_context}
        context["ps"]["username"] = self.request.user.username
        if onboarding_user:
            context["first_name"] = onboarding_user.first_name
            context["last_name"] = onboarding_user.last_name
            email = onboarding_user.email
        else:
            email = "onboard@3yourmind.com"  # TODO do not hardcode

        email_sender = EmailSender()
        email_sender.send_email(
            to=email,
            subject_template_path='customer/emails/'
                                  'printing_service_onboarding/'
                                  'email_onboarding_subject.txt',
            body_txt_template_path='customer/emails/'
                                   'printing_service_onboarding/'
                                   'email_onboarding_body.txt',
            body_html_template_path='customer/emails/'
                                    'printing_service_onboarding/'
                                    'email_onboarding_body.html',
            extra_context=context
        )

    def get_success_url(self):
        return reverse('account_ps_welcome_confirm')


class PrintingServiceWelcomeConfirmView(LoginRequiredMixin, TemplateView):
    template_name = 'account/printing_service_welcome_confirm.html'


def popup_success(request):
    """
    Renders a custom template with Javascript code that is closing popup and
    reload/redirect parent browser window
    """
    next = request.GET.get('next')
    return render(request, 'account/popup_success.html', {'next': next})


@user_permissions.check_user_permission
def extended_registration_addresses_view(request):
    form_class = UserPanelAddressForm
    if request.method == "POST":
        form = form_class(data=request.POST)
        if form.is_valid():
            address = form.save(commit=False)
            address.user = request.user
            address.save()

            # Signup for this user is complete at this point
            signup = request.user.signup_set.first()
            signup.confirmed = True
            signup.save()

            if get_current_org().force_verify_customer_address:
                return redirect('verification_pending')
            return redirect('b3_organization:dashboard')
        # Invalid forms get rendered again
    else:
        form = form_class(
            initial={
                'first_name': request.user.first_name,
                'last_name': request.user.last_name
            }
        )

    return render(
        request,
        'account/email_signup_extended_registration_addresses.html',
        {'address_form': form}
    )


@user_permissions.check_user_permission
def extended_registration_addresses_delete_view(request, user_address_id):
    user_addr = get_object_or_404(Address, pk=user_address_id)

    user_addr.delete()
    return JsonResponse(
        {
            'success': True,
            'url': reverse('account_email_extended_registration_addresses')
        }
    )


@user_permissions.check_user_permission
def extended_registration_verification_pending_view(request):
    return render(request, 'account/verification-pending.html', {})
