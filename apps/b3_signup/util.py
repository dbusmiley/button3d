from django.urls import reverse
from apps.b3_core.mail import EmailSender


def send_signup_email(signup, request):
    confirm_url = request.build_absolute_uri(
        reverse('account_email_signup_confirm', args=[signup.key]))
    upload_uuid = request.GET.get(
        'upload_uuid', request.POST.get(
            'upload_uuid', None))
    if upload_uuid:
        confirm_url = '{}?upload_uuid={}'.format(confirm_url, upload_uuid)

    context = {
        'signup_confirm_url': confirm_url,
    }

    email_sender = EmailSender()
    email_sender.send_email(
        to=signup.email,
        subject_template_path='account/email/email_signup_sent_subject.txt',
        body_txt_template_path='account/email/email_signup_sent_body.txt',
        body_html_template_path='account/email/email_signup_sent_body.html',
        extra_context=context,
    )
