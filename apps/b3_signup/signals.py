from django.contrib.auth.signals import user_logged_in
from django.dispatch import Signal


# Typically followed by `user_logged_in` (unless, e-mail verification kicks in)
user_signed_up = Signal(providing_args=["request", "user"])


def user_upload_ownership_handler(sender, request, user, **kwargs):
    from apps.b3_core.models import StlFile

    uuid = request.GET.get(
        'upload_uuid', request.POST.get(
            'upload_uuid', None))
    if not uuid:
        return

    try:
        stl_file = StlFile.objects.get(uuid=uuid)
    except StlFile.DoesNotExist:
        return

    if stl_file.is_demo:
        # Can't set ownership of demo file
        return

    if stl_file.is_mine(request):
        stl_file.owner = user
        stl_file.save()


user_logged_in.connect(user_upload_ownership_handler)
