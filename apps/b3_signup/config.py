from django.apps import AppConfig


class SignupConfig(AppConfig):
    name = 'apps.b3_signup'

    def ready(self):
        from actstream import registry
        Signup = self.get_model('Signup')
        registry.register(Signup)
