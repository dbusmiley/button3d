# flake8: noqa
from .signals import *

default_app_config = 'apps.b3_signup.config.SignupConfig'
