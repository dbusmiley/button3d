from django.contrib import admin

from apps.b3_signup.models import Signup


class SignupAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return Signup.all_objects.all()


admin.site.register(Signup, SignupAdmin)
