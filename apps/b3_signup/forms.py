import urllib.parse

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.urls import reverse
from django.forms.models import ModelForm
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _, \
    pgettext_lazy as pgettext

from .exceptions import UserRegisteredEmail
from .models import Signup
from apps.b3_core.mail import EmailSender
from apps.b3_organization.utils import get_current_site, get_base_url,\
    get_current_org


class LoginForm(forms.Form):

    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(
            attrs={
                'placeholder': _("Password")}))
    remember = forms.BooleanField(label=_("Remember Me"),
                                  required=False)

    user = None
    error_messages = {
        'account_inactive':
            _("This account is currently inactive."),

        'email_password_mismatch':
            _(("The e-mail address and/or password "
               "you specified are not correct.")),

        'username_password_mismatch':
            _("The username and/or password you specified are not correct."),

        'username_email_password_mismatch':
            _("The login and/or password you specified are not correct.")
    }

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        login_widget = forms.TextInput(attrs={'type': 'email',
                                              'placeholder':
                                                  _('E-mail address'),
                                              'autofocus': 'autofocus'})
        login_field = forms.EmailField(label=_("E-mail"),
                                       widget=login_widget)
        self.fields["login"] = login_field
        if settings.SESSION_REMEMBER is not None:
            del self.fields['remember']

    def user_credentials(self):
        """
        Provides the credentials required to authenticate the user for
        login.
        """
        credentials = {}
        login = self.cleaned_data["login"]
        credentials["email"] = login
        credentials["password"] = self.cleaned_data["password"]
        return credentials

    def clean_login(self):
        login = self.cleaned_data['login']
        return login.strip()

    def clean(self):
        if self._errors:
            return
        user = authenticate(**self.user_credentials())
        if user:
            self.user = user
        else:
            raise forms.ValidationError(
                self.error_messages[
                    'email_password_mismatch'])
        return self.cleaned_data

    def login(self, request):
        ret = login(request, self.user)
        remember = settings.SESSION_REMEMBER
        if remember is None:
            remember = self.cleaned_data['remember']
        if remember:
            request.session.set_expiry(settings.SESSION_COOKIE_AGE)
        else:
            request.session.set_expiry(0)
        return ret


class EmailSignupForm(forms.Form):
    email = forms.EmailField(label=_("E-mail"), required=True)
    origin_url = forms.CharField(required=False, widget=forms.HiddenInput)

    def validate_signup(self, email):
        """
        Here we check if a user already exists.

        A email must be unique depending on the organization.

        Example:
        A user user@abc.com registers at foo.3yourmind.com
        Then user@abc.com can also register on bar.3yourmind.com or
        on app.3yourmind.com

        Finally, user@abc.com can also still register at druck.dick-dick.de.
        """
        user_with_email_on_site = User.objects.filter(
            email__iexact=email,
            userprofile__site=get_current_site()
        ).first()
        signup = user_with_email_on_site.signup_set.first() \
            if user_with_email_on_site else None

        # Signup is only possible if either:
        # a) There is no User with this E-Mail or
        # b) There is a User, but it's signup process was not finished yet
        # (then it is the same user signing up again, that's ok,
        # just send the verification mail again)
        if user_with_email_on_site and not (signup and not signup.confirmed):
            raise UserRegisteredEmail

        return True

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            self.validate_signup(email)
        except UserRegisteredEmail:
            raise forms.ValidationError(_("An active user is already using "
                                          "this e-mail address."))
        return email

    def save(self, email, origin_url):
        signup = Signup.objects.filter(email__iexact=email).first()
        return signup or Signup.create(email=email, origin_url=origin_url)


class ResetPasswordForm(PasswordResetForm):

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        email = self.cleaned_data["email"]
        for user in self.get_users(email):
            temp_key = token_generator.make_token(user)

            # save it to the password reset model
            # password_reset = PasswordReset(user=user, temp_key=temp_key)
            # password_reset.save()
            path = reverse(
                "account_reset_password_from_key",
                kwargs=dict(
                    uidb64=urlsafe_base64_encode(force_bytes(user.pk)),
                    token=temp_key
                )
            )
            url = urllib.parse.urljoin(get_base_url(), path)
            context = {
                "user": user,
                "password_reset_url": url,
                "request": request,
                "username": user.username
            }

            email_sender = EmailSender()
            email_sender.send_email(
                to=email,
                subject_template_path='account/email/'
                                      'password_reset_key_subject.txt',
                body_txt_template_path='account/email/'
                                       'password_reset_key_body.txt',
                body_html_template_path='account/email/'
                                        'password_reset_key_body.html',
                extra_context=context
            )

        return self.cleaned_data["email"]


class ExtendedRegistrationForm(ModelForm):
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={'placeholder': _("Password")})
    )
    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput(
            attrs={
                'placeholder': _("Password (again)")}))

    first_name = forms.CharField(
        min_length=1,
        max_length=30,
        label=_("First name"),
        widget=forms.TextInput(
            attrs={
                'placeholder': _("First name")}))
    last_name = forms.CharField(
        min_length=1,
        max_length=30,
        label=_("Last name"),
        widget=forms.TextInput(
            attrs={
                'placeholder': _("Last name")}))

    customer_number = forms.CharField()

    def clean_password2(self):
        if "password1" in self.cleaned_data and\
                "password2" in self.cleaned_data:
            if (self.cleaned_data["password1"] !=
                    self.cleaned_data["password2"]):
                raise forms.ValidationError(_("You must type the same password"
                                              " each time."))
        return self.cleaned_data["password2"]

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data["password1"])
        self.user.first_name = self.cleaned_data["first_name"]
        self.user.last_name = self.cleaned_data["last_name"]
        self.user.userprofile.customer_number = \
            self.cleaned_data["customer_number"]

        if commit:
            self.user.save()
            self.user.userprofile.save()
        return self.user

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(ExtendedRegistrationForm, self).__init__(*args, **kwargs)

        org = get_current_org()

        if org.customer_number_visibility == org.HIDDEN:
            self.fields['customer_number'].widget = forms.HiddenInput()
        else:
            self.fields['customer_number'].widget = forms.TextInput(
                attrs={
                    'placeholder': org.customer_number_label}
            )

        self.fields['customer_number'].label = org.customer_number_label
        self.fields['customer_number'].required = \
            org.customer_number_visibility == org.REQUIRED

    class Meta:
        model = User
        fields = ('password1', 'password2',
                  'first_name', 'last_name', 'customer_number', )


class CustomSetPasswordForm(ModelForm):
    """
    Class which is created to extend the set password form.
    Here custom filters for passwords are defined.
    This custom form will be referenced in EmailSignupSetPasswordView.
    """
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(attrs={'placeholder': _("Password")})
    )
    password2 = forms.CharField(
        label=_("Password (again)"),
        widget=forms.PasswordInput(
            attrs={
                'placeholder': _("Password (again)")}))

    @staticmethod
    def has_numbers(input_string):
        return any(char.isdigit() for char in input_string)

    def clean_password1(self):
        if "password1" in self.cleaned_data:
            if len(self.cleaned_data["password1"]
                   ) < settings.ACCOUNT_PASSWORD_MIN_LENGTH:
                raise forms.ValidationError(
                    _("Password must be a minimum of %d characters.") %
                    settings.ACCOUNT_PASSWORD_MIN_LENGTH)
            if not self.has_numbers(self.cleaned_data["password1"]):
                raise forms.ValidationError(
                    _("Password need at least one number.")
                )
        return self.cleaned_data["password1"]

    def clean_password2(self):
        if ("password1" in self.cleaned_data and
                "password2" in self.cleaned_data):
            if (self.cleaned_data["password1"] !=
                    self.cleaned_data["password2"]):
                raise forms.ValidationError(_("You must type the same password"
                                              " each time."))
        return self.cleaned_data["password2"]

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(CustomSetPasswordForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data["password1"])
        if commit:
            self.user.save()
        return self.user

    class Meta:
        model = User
        fields = ('password1', 'password2', )


class PrintingServiceWelcomeForm(forms.Form):
    company = forms.CharField(
        max_length=100,
        label=pgettext(
            "name of the company", "Company Name"
        )
    )
    website = forms.CharField(max_length=100, label=_("Website"))
    email = forms.EmailField(label=_("Business email"))
    phone = forms.CharField(max_length=30, label=_("Phone Number"))
    title = forms.ChoiceField(
        choices=[("Mr", _("Mr.")), ("Mrs.", _("Mrs."))],
        label=_("Contact Person")
    )
    first_name = forms.CharField(max_length=30, label=_("First Name"))
    last_name = forms.CharField(max_length=30, label=_("Last Name"))
    questions = forms.CharField(
        widget=forms.Textarea,
        max_length=1000,
        required=False,
        label=_("Questions"))
