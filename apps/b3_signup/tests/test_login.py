import time
import re

from django.conf import settings
from django.core import mail
from django.core.signing import Signer
from django.urls import reverse
from django.http.cookie import SimpleCookie

from apps.b3_organization.utils import set_current_site
from apps.b3_tests.clients import ThreeydClient, ProAccountClient
from apps.b3_tests.factories import UserFactory, \
    StockRecordFactory, StlFileFactory, \
    SiteFactory, OrganizationFactory, BasketFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.basket.models import Basket


class LoginTest(TestCase):
    def setUp(self):
        super(LoginTest, self).setUp()
        self.user = UserFactory(password='testpassword')  # nosec
        self.login_url = reverse('account_login')

    def is_login_page(self, page_content):
        return '<title>3YOURMIND - Login</title>' in page_content and \
            '<h4>Login</h4>' in page_content

    def test_login_ok(self):
        data = {
            'login': self.user.email,
            'password': 'testpassword'
        }
        response = self.client.post(self.login_url, data=data, follow=True)
        self.assertFalse(self.is_login_page(response.content.decode()))
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode())

    def test_login_no_data(self):
        response = self.client.post(self.login_url, data={}, follow=True)
        self.assertTrue(self.is_login_page(response.content.decode()))
        self.assertTrue('This field is required.' in
                        response.content.decode())

    def test_login_wrong_mail(self):
        data = {
            'login': 'test@3yourmind.c',
            'password': 'testpassword'
        }
        response = self.client.post(self.login_url, data=data, follow=True)
        self.assertTrue(self.is_login_page(response.content.decode()))
        self.assertTrue('Enter a valid email address.' in
                        response.content.decode())

    def test_login_not_existing_mail(self):
        data = {
            'login': 'test@gmail.com',
            'password': 'testpassword'
        }
        response = self.client.post(self.login_url, data=data, follow=True)
        self.assertTrue(self.is_login_page(response.content.decode()))
        self.assertTrue(
            ('The e-mail address and/or password '
             'you specified are not correct.') in response.content.decode())

    def test_login_wrong_password(self):
        data = {
            'login': 'tester@3yourmind.com',
            'password': 'passwrd'
        }
        response = self.client.post(self.login_url, data=data, follow=True)
        self.assertTrue(self.is_login_page(response.content.decode()))
        self.assertTrue(
            ('The e-mail address and/or password you '
             'specified are not correct.') in response.content.decode())

    def test_forgotten_password(self):
        # Ask for reset email
        data = {'email': self.user.email}
        response = self.client.post(
            reverse('account_reset_password'),
            data=data,
            follow=True)
        self.assertTrue('<h4>Password Reset</h4>' in response.content.decode())
        self.assertTrue(
            ('We have sent you an e-mail. '
             'Please contact us if you do not '
             'receive it within a few minutes.') in response.content.decode())

        # Read email
        time.sleep(5)  # Wait for email to arrive
        self.assertEqual(len(mail.outbox), 1)

        subject = mail.outbox[0].subject
        self.assertEqual(subject, 'Password Reset E-mail')

        body = mail.outbox[0].body
        reset_url = None
        for url in re.findall(
            ('http[s]?://(?:[a-zA-Z]|[0-9]'
             '|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'),
                body):
            if "/en/accounts/password/reset/key/" in url:
                reset_url = url
        self.assertIsNotNone(reset_url)
        reset_url = reset_url.replace('https', 'http')

        # Click reset link
        response = self.client.get(reset_url)
        self.assertTrue(
            '<h4>Change Password</h4>' in response.content.decode()
        )

        # Submit reset request
        data = {
            'password1': 'new_password_123',
            'password2': 'new_password_123'
        }
        response = self.client.post(reset_url, data=data, follow=True)
        self.assertTrue('<h4>Change Password</h4>' in
                        response.content.decode())
        self.assertTrue('Your password is now changed.' in
                        response.content.decode())

        # Connect again with new password
        data = {
            'login': self.user.email,
            'password': 'new_password_123'
        }
        response = self.client.post(self.login_url, data=data, follow=True)
        self.assertFalse(self.is_login_page(response.content.decode()))
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode())


class LoginWithAnonymousBasketTest(TestCase):
    def setUp(self):
        super(LoginWithAnonymousBasketTest, self).setUp()
        # Have an anonymous user with basket (with a line)
        # Create the basket through middleware
        response = self.client.get(reverse('demo'), follow=True)
        response.wsgi_request.basket.save()
        # Simulate basket cookie
        self.client.cookies = SimpleCookie({
            settings.BASKET_COOKIE_NAME:
            Signer().sign(response.wsgi_request.basket.id)})

        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()
        line, created = response.wsgi_request.basket.add_product(
            stlfile, stockrecord.product, stockrecord=stockrecord)
        self.anonymous_basket = response.wsgi_request.basket

    def test_login_to_user_with_open_basket(self):
        user = UserFactory(password='testpassword')  # nosec
        # Add open basket to user
        user_basket = BasketFactory(owner=user, status=Basket.OPEN)
        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()
        line, created = user_basket.add_product(
            stlfile, stockrecord.product, stockrecord=stockrecord)
        # Login test
        data = {
            'login': user.email,
            'password': 'testpassword'
        }
        response = self.client.post(
            '{0}?next={1}'.format(
                reverse('account_login'),
                reverse(
                    'b3_user_panel:project-detail',
                    args=(
                        self.anonymous_basket.id,
                    ))),
            data=data,
            follow=True)
        self.assertEqual(
            reverse(
                'b3_user_panel:project-detail',
                args=(
                    self.anonymous_basket.id,
                )),
            response.wsgi_request.path)

        self.anonymous_basket.refresh_from_db()
        user_basket.refresh_from_db()
        self.assertEqual(self.anonymous_basket.owner, user)
        self.assertEqual(self.anonymous_basket.status, Basket.OPEN)
        self.assertEqual(user_basket.owner, user)
        self.assertEqual(user_basket.status, Basket.EDITABLE)


class MultipleSitesLoginTest(TestCase):
    def setUp(self):
        super(MultipleSitesLoginTest, self).setUp()
        self.user = UserFactory(  # nosec
            email='test@3yourmind.com',
            password='testpassword')
        self.user.userprofile

        self.orgsite = SiteFactory()
        OrganizationFactory(site=self.orgsite)
        self.orguser = UserFactory(  # nosec
            email='test@3yourmind.com',
            password='testpassword2')
        self.orguser.userprofile.site = self.orgsite
        self.orguser.userprofile.save()

    def test_login_app(self):
        self.client = ThreeydClient()
        data = {
            'login': self.user.email,
            'password': 'testpassword'
        }
        response = self.client.post(
            reverse('account_login'), data=data, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode())

    def test_login_site(self):
        self.client = ProAccountClient()
        set_current_site(self.site)
        data = {
            'login': self.orguser.email,
            'password': 'testpassword2'
        }
        response = self.client.post(
            reverse('account_login'), data=data, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode())
