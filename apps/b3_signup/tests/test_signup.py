import re

from django.conf import settings
from django.contrib.auth.models import User
from django.core import mail
from django.core.signing import Signer
from django.urls import reverse
from django.http.cookie import SimpleCookie

from apps.b3_tests.factories import UserFactory, \
    StockRecordFactory, StlFileFactory
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.basket.models import Basket


def find_signup_url(body):
    for url in re.findall(
        ('http[s]?://(?:[a-zA-Z]|[0-9]'
         '|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'),
            body):
        if "/en/accounts/signup/confirm" in url:
            return url
    return None


class SignupTest(TestCase):
    def test_signup(self):
        # Sign up page
        response = self.client.get(
            reverse('account_email_signup'), follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Sign Up</title>' in response.content.decode())
        self.assertTrue('<h4>Sign Up</h4>' in response.content.decode())

        # Sign up
        data = {'email': 'test@3yourmind.com'}
        response = self.client.post(
            reverse('account_email_signup'),
            data=data,
            follow=True)
        self.assertTrue('<h4>Sign Up confirmation</h4>' in
                        response.content.decode())
        self.assertTrue(
            ('A confirmation e-mail was sent '
             'to your address.') in response.content.decode())

        # Read email
        self.assertEqual(len(mail.outbox), 1)

        subject = mail.outbox[0].subject
        self.assertEqual(subject, 'Sign up confirmation')

        body = mail.outbox[0].body
        signup_url = find_signup_url(body)
        self.assertIsNotNone(signup_url)
        signup_url = signup_url.replace('https', 'http')

        # Open mail link
        response = self.client.get(signup_url, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Set Password</title>' in
            response.content.decode())
        self.assertTrue('<h4>Set Password</h4>' in response.content.decode())

        set_password_url = reverse('account_email_signup_set_password')

        # Set invalid password (too short)
        data = {
            'login': 'test@3yourmind.com',
            'password1': 'test',
            'password2': 'test'
        }
        response = self.client.post(set_password_url, data=data, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Set Password</title>' in
            response.content.decode())
        self.assertTrue('<h4>Set Password</h4>' in
                        response.content.decode())
        self.assertTrue(
            'Password must be a minimum of 8 characters.' in
            response.content.decode())

        # Set invalid password (no number)
        data = {
            'login': 'test@3yourmind.com',
            'password1': 'testpass',
            'password2': 'testpass'
        }
        response = self.client.post(set_password_url, data=data, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Set Password</title>' in
            response.content.decode())
        self.assertTrue('<h4>Set Password</h4>' in
                        response.content.decode())
        self.assertTrue(
            'Password need at least one number.' in
            response.content.decode())

        # Set invalid password (not matching)
        data = {
            'login': 'test@3yourmind.com',
            'password1': 'testpass123',
            'password2': 'testpass456'
        }
        response = self.client.post(set_password_url, data=data, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Set Password</title>' in
            response.content.decode())
        self.assertTrue('<h4>Set Password</h4>' in
                        response.content.decode())
        self.assertTrue(
            'You must type the same password each time.' in
            response.content.decode())

        # Set password
        data = {
            'login': 'test@3yourmind.com',
            'password1': 'testpass1',
            'password2': 'testpass1'
        }
        response = self.client.post(set_password_url, data=data, follow=True)
        self.assertTrue('test@3yourmind.com' in response.content.decode())
        self.assertTrue(
            "You haven't created any 3D projects so far." in
            response.content.decode())
        self.assertTrue("You haven't ordered anything." in
                        response.content.decode())
        self.assertTrue(
            '<title>3YOURMIND - Dashboard</title>' in
            response.content.decode())

    def test_signup_no_data(self):
        response = self.client.post(
            reverse('account_email_signup'), follow=True)
        self.assertTrue('<h4>Sign Up</h4>' in response.content.decode())
        self.assertTrue('This field is required.' in response.content.decode())

    def test_signup_already_signed_up(self):
        user = UserFactory()

        data = {'email': user.email}
        response = self.client.post(
            reverse('account_email_signup'),
            data=data,
            follow=True)
        self.assertTrue('<h4>Sign Up</h4>' in response.content.decode())
        self.assertTrue(
            ('An active user is already using '
             'this e-mail address.') in response.content.decode())

    def test_signup_wrong_mail(self):
        data = {'email': 'test@3yourmind.c'}
        response = self.client.post(
            reverse('account_email_signup'),
            data=data,
            follow=True)
        self.assertTrue('<h4>Sign Up</h4>' in response.content.decode())
        self.assertTrue('Enter a valid email address.' in
                        response.content.decode())


class SignupWithBasketTest(TestCase):
    def setUp(self):
        super(SignupWithBasketTest, self).setUp()
        # Have an anonymous user with basket (with a line)
        # Create the basket through middleware
        response = self.client.get(reverse('demo'), follow=True)
        response.wsgi_request.basket.save()
        self.client.cookies = SimpleCookie({
            settings.BASKET_COOKIE_NAME:
            Signer().sign(response.wsgi_request.basket.id)})

        stockrecord = StockRecordFactory()
        stlfile = StlFileFactory()
        line, created = response.wsgi_request.basket.add_product(
            stlfile, stockrecord.product, stockrecord=stockrecord)

    def test_signup_assign_basket(self):
        self.assertIsNone(Basket.objects.first().owner)
        data = {'email': 'test@3yourmind.com'}
        response = self.client.post(
            reverse('account_email_signup'),
            data=data,
            follow=True)
        self.assertTrue('<h4>Sign Up confirmation</h4>' in
                        response.content.decode())
        self.assertTrue(
            ('A confirmation e-mail was sent '
             'to your address.') in response.content.decode())

        signup_url = find_signup_url(
            mail.outbox[0].body).replace(
            'https', 'http')
        # New client (session...)
        self.client = self.client_class()
        response = self.client.get(signup_url, follow=True)
        self.assertTrue(
            '<title>3YOURMIND - Set Password</title>' in
            response.content.decode())
        self.assertTrue('<h4>Set Password</h4>' in response.content.decode())

        set_password_url = reverse('account_email_signup_set_password')
        # Set password
        data = {
            'login': 'test@3yourmind.com',
            'password1': 'testpass1',
            'password2': 'testpass1'
        }
        response = self.client.post(set_password_url, data=data, follow=True)
        self.assertTrue('test@3yourmind.com' in response.content.decode())

        self.assertEqual(Basket.objects.count(), 1)
        self.assertEqual(
            Basket.objects.first().owner,
            User.objects.get(
                email='test@3yourmind.com'))
