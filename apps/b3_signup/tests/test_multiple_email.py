from apps.b3_organization.utils import set_current_site
from apps.b3_signup.exceptions import UserRegisteredEmail
from apps.b3_signup.forms import EmailSignupForm
from apps.b3_tests.factories import SiteFactory, RequestMock, \
    OrganizationFactory, UserAnyEmailFactory, SignupFactory
from apps.b3_tests.testcases.common_testcases import TestCase


class MultipleEmailTest(TestCase):
    def setUp(self):
        super(MultipleEmailTest, self).setUp()
        self.site_subdomain = SiteFactory()
        self.organization_site_subdomain = OrganizationFactory(
            site=self.site_subdomain)
        self.site_external_domain = SiteFactory(
            name="external.domain1", domain="external.domain1")
        self.organization_external_domain = OrganizationFactory(
            site=self.site_external_domain)
        self.factory = RequestMock()

    def test_email_can_create_user(self):
        email = 'test-email@example.com'
        form = EmailSignupForm(data={'email': email})

        set_current_site(self.site)
        form.validate_signup(email)

        set_current_site(self.site_subdomain)
        form.validate_signup(email)

        set_current_site(self.site_external_domain)
        form.validate_signup(email)

    def test_user_on_main_domain(self):
        set_current_site(self.site)

        # create main site user
        user = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user.email)

        # try to create new user with the same email
        form = EmailSignupForm(data={'email': user.email})

        set_current_site(self.site)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

        set_current_site(self.site_subdomain)
        form.validate_signup(user.email)

        set_current_site(self.site_external_domain)
        form.validate_signup(user.email)

    def test_user_on_subdomain(self):
        set_current_site(self.site_subdomain)

        # create subdomain user
        user = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user.email)

        # try to create new user with the same email

        form = EmailSignupForm(data={'email': user.email})

        set_current_site(self.site)
        form.validate_signup(user.email)

        set_current_site(self.site_subdomain)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

        set_current_site(self.site_external_domain)
        form.validate_signup(user.email)

    def test_user_on_external_domain(self):
        set_current_site(self.site_external_domain)

        # create external domain user
        user = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user.email)

        # try to create new user with the same email

        form = EmailSignupForm(data={'email': user.email})

        set_current_site(self.site)
        form.validate_signup(user.email)

        set_current_site(self.site_subdomain)
        form.validate_signup(user.email)

        set_current_site(self.site_external_domain)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

    def test_cannot_create_user(self):
        set_current_site(self.site)
        user = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user.email)

        set_current_site(self.site_external_domain)
        user2 = UserAnyEmailFactory.create(username='tester2', is_staff=False)
        SignupFactory.create(email=user2.email)

        # try to create new user with the same email

        form = EmailSignupForm(data={'email': user.email})

        set_current_site(self.site)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

        set_current_site(self.site_subdomain)
        form.validate_signup(user.email)

        set_current_site(self.site_external_domain)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

    def test_cannot_create_user2(self):
        set_current_site(self.site_subdomain)
        user = UserAnyEmailFactory.create(is_staff=False)
        SignupFactory.create(email=user.email)

        set_current_site(self.site_external_domain)
        user2 = UserAnyEmailFactory.create(username='tester2', is_staff=False)
        SignupFactory.create(email=user2.email)

        # try to create new user with the same email

        form = EmailSignupForm(data={'email': user.email})

        set_current_site(self.site)
        form.validate_signup(user.email)

        set_current_site(self.site_subdomain)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)

        set_current_site(self.site_external_domain)
        with self.assertRaises(UserRegisteredEmail):
            form.validate_signup(user.email)
