from django.test import TestCase

from apps.b3_api.errors.api_responses import error_object_not_found, \
    error_object_update_failed, error_object_access_denied_user, \
    error_bad_db_operation


class ApiResponsesTest(TestCase):
    def setUp(self):
        super(ApiResponsesTest, self).setUp()

    def test_object_not_found(self):
        object_error_not_found_error = error_object_not_found('ModelX', 1)
        expected_error = {
            "code": 'MODELX_NOT_FOUND',
            "message": 'MODELX not found for the given id',
            "moreInfo": {
                "objectName": 'ModelX',
                "id": 1
            }
        }
        self.assertEqual(object_error_not_found_error, expected_error)

    def test_error_object_update_failed(self):
        object_update_failed_error = error_object_update_failed('ModelX', 1)
        expected_error = {
            "code": 'MODELX_UPDATE_FAILED',
            "message": None,
            "moreInfo": {
                "objectName": 'ModelX',
                "id": 1,
                "exception": None
            }
        }
        self.assertEqual(object_update_failed_error, expected_error)

    def test_error_object_access_denied_user(self):
        object_access_denied_error = error_object_access_denied_user(
            'ModelX', 1, 29)
        expected_error = {
            "code": 'MODELX_ACCESS_DENIED',
            "message": 'Access denied for the requesting user',
            "moreInfo": {
                "objectName": 'ModelX',
                "objectId": 1,
                "userId": 29,
            }
        }
        self.assertEqual(object_access_denied_error, expected_error)

    def test_error_bad_db_operation(self):
        db_error_text = 'ERROR 1146 (42S02): Table test.no_such_table does ' \
                        'not exist'
        bad_db_operation_error = error_bad_db_operation('ModelX',
                                                        db_error_text)

        expected_error = {
            "code": 'Invalid db operation on accessing: ModelX',
            "message": 'Database operation raised an exception',
            "moreInfo": {
                "exception": 'ERROR 1146 (42S02): Table test.no_such_table '
                             'does not exist',
            }
        }
        self.assertEqual(bad_db_operation_error, expected_error)
