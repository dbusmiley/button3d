def error_object_not_found(object_name, object_id):
    return {
        "code": f'{object_name.upper()}_NOT_FOUND',
        "message": f'{object_name.upper()} not found for the given id',
        "moreInfo": {
            "objectName": object_name,
            "id": object_id
        }
    }


def error_object_update_failed(
    object_name=None, object_id=None, message=None, exception=None
):
    return {
        "code": f'{object_name.upper()}_UPDATE_FAILED',
        "message": message,
        "moreInfo": {
            "objectName": object_name,
            "id": object_id,
            "exception": exception
        }
    }


def error_got_wrong_value(key, value, value_expected, message=None):
    return {
        "code": 'INVALID_VALUE',
        "message": message,
        "moreInfo": {
            "key": key,
            "value": value,
            "valueExpected": value_expected,
        }
    }


def error_object_access_denied_user(object_name, object_id, user_id):
    return {
        "code": f'{object_name.upper()}_ACCESS_DENIED',
        "message": 'Access denied for the requesting user',
        "moreInfo": {
            "objectName": object_name,
            "objectId": object_id,
            "userId": user_id,
        }
    }


def error_bad_db_operation(object_name, exception):
    return {
        "code": f'Invalid db operation on accessing: {object_name}',
        "message": 'Database operation raised an exception',
        "moreInfo": {
            "exception": f'{exception}',
        }
    }
