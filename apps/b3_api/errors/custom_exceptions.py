import logging

from django.http.response import Http404
from rest_framework.exceptions import PermissionDenied

from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.views import exception_handler, set_rollback

logger = logging.getLogger(__name__)


def _custom_exception_handler(exc, data=None):
    # Just to handle APIExceptions
    headers = {}
    if getattr(exc, 'auth_header', None):
        headers['WWW-Authenticate'] = exc.auth_header
    if getattr(exc, 'wait', None):
        headers['Retry-After'] = f'{exc.wait}'

    set_rollback()
    return Response(data=data or {}, status=exc.status_code, headers=headers)


def _handle_permission_denied(exc, context):
    return exception_handler(exc, context)


def _handle_http_404(exc, context):
    response = exception_handler(exc, context)
    response.data = exc.args[0]
    return response


def _handle_validation_error(exc, context):
    """
    exc will have key value pair of failed fields,
    like: https://gist.github.com/anonymous/2158de1185c7c62bf1263a477f50c8d4
    """
    response = _custom_exception_handler(exc)
    more_info = [
        {
            'field': item,
            'message': exc.detail[item]
        } for item in exc.detail
    ]

    try:
        err_object = type(context['view'].get_object()).__name__
    except Exception as e:  # nosec
        logger.warning("Errorcode could not be generated", e)
        err_object = ''

    response.data['code'] = f"{err_object}_VALIDATION_ERROR"
    response.data['message'] = "Model field validation error."
    response.data['moreInfo'] = more_info
    return response


def _handle_error_response(exc, context):
    return _custom_exception_handler(exc, data=exc.as_dict())


class ErrorResponse(Exception):
    def __init__(self, code, message, more_info=None, status_code=400):
        self.code = code
        self.message = message
        self.more_info = more_info
        self.status_code = status_code

    def as_dict(self):
        return {
            'code': self.code,
            'message': self.message,
            'moreInfo': self.more_info
        }


def custom_exception_handler(exc, context):
    if isinstance(exc, PermissionDenied):
        return _handle_permission_denied(exc, context)

    if isinstance(exc, Http404):
        return _handle_http_404(exc, context)

    if isinstance(exc, exceptions.ValidationError):
        return _handle_validation_error(exc, context)

    if isinstance(exc, ErrorResponse):
        return _handle_error_response(exc, context)

    response = exception_handler(exc, context)
    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code
    return response
