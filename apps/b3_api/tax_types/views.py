from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.partner.models.partner import TAX_TYPES


class ListTaxTypes(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        return Response(
            list(dict(TAX_TYPES).keys()), status=status.HTTP_200_OK
        )
