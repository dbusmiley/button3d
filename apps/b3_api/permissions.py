from django.conf import settings
from rest_framework import permissions

from apps.b3_api.errors.api_responses import error_object_access_denied_user


class IsServiceUser(permissions.BasePermission):
    message = 'You do not have the permission to access this API'

    def has_permission(self, request, view):
        pk_arg = view.lookup_url_kwarg or 'service_id'
        if not request.user.partners.exists():
            # Use our custom message instead
            self.message = error_object_access_denied_user(
                object_name='SERVICE',
                object_id=view.kwargs.get(pk_arg),
                user_id=request.user.id
            )
            return False

        return True


class IsLocalRequest(permissions.BasePermission):
    """
    Allow requests from INTERNAL_IPS
    Such views will be used by status_page monitor processes
    """
    message = 'Only local requests allowed'

    def has_object_permission(self, request, view, obj):
        return request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS


class IsMESUser(permissions.BasePermission):
    message = 'You do not have the permission to access this API'

    def has_permission(self, request, view):
        pk_arg = view.lookup_url_kwarg or 'id'
        if not request.user.partners.filter(mes_activated=True).exists():
            # Use our custom message instead
            self.message = error_object_access_denied_user(
                object_name='MES',
                object_id=view.kwargs.get(pk_arg),
                user_id=request.user.id
            )
            return False

        return True
