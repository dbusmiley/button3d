from django.conf.urls import url, include

from apps.b3_api.colors.views import ListColors
from apps.b3_api.countries.views import ListCountries
from apps.b3_api.currencies.views import ListCurrencies
from apps.b3_api.materials.views import ListMaterials
from apps.b3_api.organizations.settings.views.organization_settings import (
    OrganizationSettingsView
)
from apps.b3_api.tax_types.views import ListTaxTypes
from apps.b3_api.technologies.views import ListTechnologies
from apps.b3_api.timezones.views import Timezones
from apps.catalogue.views import CatalogueDownloadView

urlpatterns = [
    url(r'^technologies/$', ListTechnologies.as_view()),
    url(r'^colors/$', ListColors.as_view()),
    url(r'^countries/$', ListCountries.as_view()),
    url(r'^currencies/$', ListCurrencies.as_view()),
    url(r'^materials/$', ListMaterials.as_view()),
    url(r'^tax-types/$', ListTaxTypes.as_view()),
    url(r'^timezones/$', Timezones.as_view()),
    url(r'^services/', include('apps.b3_api.services.urls')),
    url(r'^projects/', include('apps.b3_api.projects.urls')),
    url(r'^auth/', include('apps.b3_api.auth.urls')),
    url(r'^users/', include('apps.b3_api.users.urls')),
    url(r'^settings/$', OrganizationSettingsView.as_view()),
    url(r'^service-panel/', include('apps.b3_checkout.service_panel_urls')),
    url(r'^user-panel/', include('apps.b3_checkout.user_panel_urls')),
    url(r'^organization-panel/', include('apps.b3_api.organizations.urls'),),
    url(r'^system/catalogue/$', CatalogueDownloadView.as_view())

]
