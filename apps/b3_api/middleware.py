import logging
import re
import typing as t

from django.core.exceptions import MiddlewareNotUsed
from django.http import (HttpRequest, HttpResponse, HttpResponseBadRequest)
from django.urls.resolvers import ResolverMatch
from openapi_core.shortcuts import RequestValidator, ResponseValidator
from openapi_core.wrappers.base import BaseOpenAPIRequest, BaseOpenAPIResponse

import button3d.type_declarations as td  # noqa
from .utils.openapi import ReloadableSpecMixin
from .utils.urlpatterns import PatternCache
from apps.b3_core.middleware import BaseMiddleware

__all__ = (
    'OpenApiSpecMiddleware',
)

logger = logging.getLogger(__name__)


# When should you add a new middleware to this file:
# - Your middleware relates to the API and the API only AND:
#   - You can reuse many of the imports already here OR
#   - You want to directly use some of the classes defined here OR
#   - The file is still small enough after you added your code
#
# In all other cases please right-click on the file in PyCharm and select
# Refactor > Convert to Python package. Then add your code in a seperate file.


def get_path_parameters(match: ResolverMatch) -> t.Dict[str, str]:
    params = {}
    for i, arg in enumerate(match.args):
        params[str(i)] = arg

    params.update(match.kwargs)
    logger.debug('===> Parameters: {params!r}'.format(params=params))
    return params


class DjangoOpenApiRequest(BaseOpenAPIRequest):
    pattern_cache = PatternCache(base='/api/v2.0/')

    def __init__(self, request: HttpRequest):
        self.request = request

    @property
    def host_url(self) -> str:
        host = self.request.get_host()
        scheme = self.request.scheme
        host_url = '{}://{}'.format(scheme, host)
        logger.debug('===> Host url: {0:s}'.format(host_url))
        return host_url

    @property
    def path(self) -> str:
        return self.request.path

    @property
    def path_pattern(self) -> str:
        return self.pattern_cache.get_pattern(self.path) or self.path

    @property
    def method(self) -> str:
        return self.request.method.lower()

    @property
    def sanitized_cookies(self) -> 'td.StringKeyDict':
        """
        Remove any cookies that may contain sensitive information

        :return: the sanitized cookies, but still a great taste!
        """
        return self.request.COOKIES

    @property
    def parameters(self) -> 'td.StringKeyDict':
        """
        Return the parameters that formed the request. These consist of
        dictionaries with:
        - path variables (/user/{Id})
        - query string
        - headers
        - cookies

        .. TODO::
            Cookies should probably be sanitized in the future but are not at
            the moment.

        .. note::
            If `self.request.resolver_match` is None, then we cannot obtain
            the information for the path variables. This means this method is
            called too early with a request that is not yet fully populated.
            The earliest this method can be called is in `process_view()` for a
            Middleware component, which is the point in the pipeline that is
            called when the URL is resolved and the view is about to be called.

        :return: A dictionary of dictionaries with the relevant information
        """
        path_params = {}
        if self.request.resolver_match is not None:
            path_params = get_path_parameters(self.request.resolver_match)

        return {
            'path': path_params,
            'query': self.request.GET.dict(),
            'headers': self.request.META,
            'cookies': self.sanitized_cookies,
        }

    @property
    def body(self) -> bytes:
        return self.request.body

    @property
    def mimetype(self) -> str:
        return self.request.content_type


class DjangoOpenApiResponse(BaseOpenAPIResponse):
    def __init__(self, response: HttpResponse):
        self.response = response

    @property
    def data(self):
        return self.response.content

    @property
    def status_code(self):
        return self.response.status_code

    @property
    def mimetype(self):
        return self.response.get('Content-Type')


def validation_error_response(msg: str, errors: t.Sequence[Exception],
                              **kwargs) -> HttpResponse:
    exception_msgs = '\n'.join([err.args[0] for err in errors])
    content = msg.format(errors=exception_msgs, **kwargs)
    return HttpResponseBadRequest(content=content)


class OpenApiSpecMiddleware(ReloadableSpecMixin, BaseMiddleware):
    """
    Validates each request and response against an OpenAPI 3.0 spec.
    """
    path_match = r'^/api/v2.0/'
    REQ_FAILURE_MSG = 'Request does not match spec:\n\n{errors:s}'
    RESP_FAILURE_MSG = 'Response does not match spec:\n\n' \
                       '{errors:s}\nOriginal:\n{orig:s}'

    def __init__(self, get_response: 'td.MiddlewareOrView' = None):
        from django.conf import settings
        if not settings.ENABLE_OPENAPI_SPEC_VALIDATION:
            raise MiddlewareNotUsed

        super().__init__(
            get_response=get_response, spec_path=settings.OPENAPI_SPEC_FILE
        )

        self.req_validator, self.res_validator = None, None
        self._path_match = re.compile(self.path_match)
        self.request_validation_failed = False
        self.url_matched = False

        # Populate the specification related objects
        self.regenerate_spec_if_updated()

    def regenerate_spec_if_updated(self) -> bool:
        updated = super().regenerate_spec_if_updated()
        if updated:
            self.req_validator = RequestValidator(self.spec)
            self.res_validator = ResponseValidator(self.spec)

        return updated

    def match_api_base(self, request: HttpRequest) -> bool:
        path = request.get_full_path()
        logger.debug('Matching request uri: {:s}'.format(path))
        return re.search(self._path_match, path) is not None

    def process_view(self,
                     request: HttpRequest,
                     *args, **kwargs) -> t.Optional['td.DjangoResponse']:
        """
        Hook that does the request validation

        :param request: The request we are processing
        :return: None if all is ok. HttpResponseBadRequest if not ok.
        """
        self.request_validation_failed = False
        self.regenerate_spec_if_updated()
        self.url_matched = self.match_api_base(request)
        if not self.url_matched:
            # This is preceded by Matching request uri:, hence the indent.
            logger.debug('===> Url not matched')
            return None

        wrapped_request = DjangoOpenApiRequest(request)
        result = self.req_validator.validate(request=wrapped_request)
        if result.errors:
            self.request_validation_failed = True
            return validation_error_response(self.REQ_FAILURE_MSG,
                                             result.errors)

        logger.info("OpenAPI: Request validated.")

        return None

    def __call__(self, request: HttpRequest) -> 'td.DjangoResponse':
        """
        Middleware implementation

        We only do the response part here. We need url parameters so we
        cannot do the processing here, but only in process_view(),
        with roughly the same semantics.

        The main difference being, that we cannot alter the URL, which is a
        non-issue for us.

        :param request: The request being processed
        :return: The response
        """
        response: td.DjangoResponse = self.get_response(request)
        return self.process_response(request, response)

    def process_response(self,
                         request: HttpRequest,
                         response: HttpResponse) -> 'td.DjangoResponse':
        if self.request_validation_failed or not self.url_matched:
            logger.debug('===> Invalid request or url not matched')
            return response

        wrapped_request = DjangoOpenApiRequest(request)
        wrapped_response = DjangoOpenApiResponse(response)
        result = self.res_validator.validate(
            request=wrapped_request, response=wrapped_response
        )
        if result.errors:
            orig = response.content
            return validation_error_response(self.REQ_FAILURE_MSG,
                                             result.errors, orig=orig)

        logger.info('OpenAPI: response validated')

        return response
