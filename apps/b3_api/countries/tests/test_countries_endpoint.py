import json

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_address.models import Country


class CountriesAPIEndpointTest(AuthenticatedTestCase):
    def setUp(self):
        super(CountriesAPIEndpointTest, self).setUp()

    def test_multiple_countries(self):
        """
        Create a new country and see it came back.
        """
        CountriesAPIEndpointTest.create_country(['germany', 'india'])
        response = self.client.get('/api/v2.0/countries/')
        countries_returned = json.loads(response.content)
        self.assertEqual(len(countries_returned), 2)

    def test_single_country(self):
        """
        Create a new country and see it came back.
        """
        CountriesAPIEndpointTest.create_country(['germany'])
        response = self.client.get('/api/v2.0/countries/')
        countries_returned = json.loads(response.content)
        self.assertEqual(len(countries_returned), 1)
        self.assertEqual(countries_returned[0]['alphaTwoCode'], 'DE')

    @staticmethod
    def create_country(country_names):
        if 'germany' in country_names:
            country = Country.objects.create(
                alpha2='DE',
                alpha3='DEU',
                name='Germany'
            )
            country.save()
        if 'india' in country_names:
            country = Country.objects.create(
                alpha2='IN',
                alpha3='IND',
                name='India'
            )
            country.save()
