from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView

from apps.b3_address.models import Country

from apps.b3_api.countries.serializers import CountrySerializer


class ListCountries(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = CountrySerializer

    def get_queryset(self):
        return Country.objects.all()
