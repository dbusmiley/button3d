from rest_framework import serializers
from apps.b3_address.models import Country

from drf_payload_customizer.mixins import PayloadConverterMixin


class CountrySerializer(PayloadConverterMixin, serializers.ModelSerializer):
    class Meta:
        model = Country
        field_mappings = {
            'alpha2': 'alpha_two_code',
            'alpha3': 'alpha_three_code',
        }
        fields = (
            'name',
            'alpha2',
            'alpha3',
        )
