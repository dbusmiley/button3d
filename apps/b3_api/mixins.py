from rest_framework import pagination, response
from collections import OrderedDict

import button3d.type_declarations as td

__all__ = (
    'PaginatedViewMixin',
)


class PageCountPagination(pagination.PageNumberPagination):
    page_query_param: str = 'page'
    page_size_query_param: str = 'pageSize'
    page_size = 25
    max_page_size: int = 100

    def get_paginated_response(self,
                               data: td.StringKeyDict) -> response.Response:
        """
        Optimized for showing the number of pages and not result count.

        Representation does not contain URLs as page state is kept by the
        frontend, so it can send new Ajax requests, instead of full page
        reloads.

        .. note:: The page size is set in :data:`settings.REST_FRAMEWORK`.

        :param data: serializer data
        """
        return response.Response(OrderedDict([
            ('currentPage', self.page.number),
            ('totalPages', self.page.paginator.num_pages),
            ('pageSize', self.get_page_size(self.request)),
            ('results', data),
        ]))


class PaginatedViewMixin:
    pagination_class = PageCountPagination
