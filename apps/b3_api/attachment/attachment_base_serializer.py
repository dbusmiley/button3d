from rest_framework import serializers

from apps.b3_api.users.serializers.user import UserSerializer


class AttachmentBaseSerializer(serializers.ModelSerializer):
    uploader = UserSerializer()
    name = serializers.CharField(source='filename')
    added = serializers.CharField(source='creation_date')
    url = serializers.SerializerMethodField()

    def get_url(self, obj):
        request_context = self.context.get('request')
        attachment_url = request_context.build_absolute_uri(obj.file.url)
        return attachment_url

    class Meta:
        fields = (
            'id',
            'name',
            'added',
            'url',
            'uploader',
        )
