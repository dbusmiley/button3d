from decimal import Decimal

import json

from rest_framework.renderers import JSONRenderer


class StrigifyingJSONEncoder(json.JSONEncoder):
    """
    Custom JSON encoder to gracefuly encode non-standard python objects by
    converting them to str
    """

    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)

        elif not isinstance(o, (str, list, tuple, dict, int, float)):
            return str(o)

        return super(StrigifyingJSONEncoder, self).default(o)


class StringifyingJSONRenderer(JSONRenderer):
    """
    DRF renderer using custom StrigifyingJSONEncoder encoder
    """
    encoder_class = StrigifyingJSONEncoder
