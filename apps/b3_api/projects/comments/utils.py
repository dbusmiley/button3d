from apps.b3_api.errors.api_responses import error_object_update_failed


def handle_project_comment_update_fail(object_id, validation_response):
    return error_object_update_failed(
        object_name='PROJECT_COMMENT',
        object_id=object_id,
        message=validation_response,
    )
