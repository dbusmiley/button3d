# flake8: noqa
from .list_project_comments import ListProjectCommentsView
from .project_comments_details import ProjectCommentsDetailsView
