from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.projects.comments.serializers import \
    ListProjectCommentsSerializer
from apps.b3_api.projects.comments.utils import \
    handle_project_comment_update_fail
from apps.b3_api.projects.views import BaseProjectsView
from apps.basket.models import Comment
from apps.basket.views import BasketCommentsView


class ListProjectCommentsView(BaseProjectsView, ListCreateAPIView):
    permission_classes = (IsAuthenticated, IsServiceUser)
    serializer_class = ListProjectCommentsSerializer

    def get_queryset(self):
        return Comment.objects.filter(basket_id=self.get_project().id)

    def list(self, request, *args, **kwargs):
        serialized_response = super(ListProjectCommentsView, self).list(
            request, *args, **kwargs)
        comment_list = [
            comment["get_dictionary"]
            for comment in serialized_response.data
        ]
        return Response({
            "value": comment_list
        })

    def post(self, request, *args, **kwargs):
        basket = self.get_project()
        comment = Comment(user=request.user, basket=basket)
        try:
            validation_response = BasketCommentsView.validate_payload(
                request.data)
        except (KeyError, MultiValueDictKeyError):
            return Response(
                handle_project_comment_update_fail(basket.id,
                                                   'Got invalid JSON'),
                status=status.HTTP_404_NOT_FOUND)

        if not validation_response['is_valid']:
            return Response(
                handle_project_comment_update_fail(
                    basket.id, validation_response['error']),
                status=status.HTTP_404_NOT_FOUND)

        comment.text = request.data['text']
        parent_comment = BasketCommentsView.get_parent(request.data['parent'])
        if parent_comment:
            comment.parent = parent_comment

        comment.save()

        BasketCommentsView.send_email_notification(
            basket_id=basket.id, user=request.user, comment=comment)
        return Response({'id': comment.pretty_uuid})
