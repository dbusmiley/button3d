from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import status
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.projects.comments.serializers \
    import ProjectCommentsDetailsSerializer
from apps.b3_api.projects.comments.utils import \
    handle_project_comment_update_fail
from apps.b3_api.projects.views import BaseProjectsView
from apps.basket.views import BasketCommentsView


class ProjectCommentsDetailsView(BaseProjectsView,
                                 RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated, IsServiceUser)
    serializer_class = ProjectCommentsDetailsSerializer

    def get_object(self):
        return BasketCommentsView.get_comment(
            uuid=self.kwargs['comment_id'],
            user_id=self.request.user.id,
            basket_id=self.get_project().id
        )

    def put(self, request, *args, **kwargs):
        comment_data = request.data
        try:
            validation_response = BasketCommentsView.validate_payload(
                comment_data
            )
        except (KeyError, MultiValueDictKeyError):
            return Response(
                handle_project_comment_update_fail(
                    self.kwargs['comment_id'], 'Got invalid JSON'
                ),
                status=status.HTTP_404_NOT_FOUND
            )

        if not validation_response['is_valid']:
            return Response(
                handle_project_comment_update_fail(
                    self.kwargs['comment_id'], validation_response['error']
                ),
                status=status.HTTP_404_NOT_FOUND
            )

        comment = self.get_object()
        if not comment:
            return Response(
                error_object_not_found(
                    object_name='PROJECT_COMMENT',
                    object_id=comment_data['id'],
                ),
                status=status.HTTP_404_NOT_FOUND
            )

        comment.text = comment_data['text']
        comment.edited = timezone.now()
        comment.save()
        return Response(None, status=status.HTTP_200_OK)
