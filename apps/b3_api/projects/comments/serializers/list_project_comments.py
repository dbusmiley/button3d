from rest_framework import serializers

from apps.basket.models import Comment


class ListProjectCommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('get_dictionary', )
