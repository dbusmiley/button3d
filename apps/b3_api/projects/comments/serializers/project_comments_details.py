from rest_framework import serializers

from apps.basket.models import Comment


class ProjectCommentsDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('get_dictionary', )
