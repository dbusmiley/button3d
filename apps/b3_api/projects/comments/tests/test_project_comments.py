import json

from django.urls import reverse
from rest_framework.test import APIClient

from apps.b3_organization.utils import set_current_site
from apps.b3_tests.clients import ThreeydClient
from apps.b3_tests.factories import PartnerFactory, BasketFactory, \
    SiteFactory, OrganizationFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.basket.models import Basket


class ProjectCommentsTest(AuthenticatedAjaxTestCase):
    alternative_site_host = 'alternative.my.3yd'

    def setUp(self):
        super(ProjectCommentsTest, self).setUp()
        self.threeydclient = ThreeydClient()
        self.threeydclient.login(
            email=self.user.email, password=self.test_password
        )
        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.comment_data = json.dumps(
            {
                'parent': None,
                'text': 'This is a new Main Comment'
            }
        )
        self.basket.save()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

    def test_invalid_comment_access_throughout_organizations(self):
        site = SiteFactory(
            domain=self.alternative_site_host,
            name='Alternative Organization'
        )
        user, basket = self.create_user_and_basket_on_site(site)

        client = self.create_and_login_api_client(user=user)
        add_comment_response = self.add_organization_comment(
            client=client, basket_id=basket.id, host=self.alternative_site_host
        )
        self.assertEqual(add_comment_response['code'], 'SERVICE_ACCESS_DENIED')

    def test_valid_comment_access_throughout_organizations(self):
        site = SiteFactory(
            domain=self.alternative_site_host,
            name='Alternative Organization'
        )
        user, basket = self.create_user_and_basket_on_site(site)
        self.partner.users.add(user)

        client = self.create_and_login_api_client(user=user)
        alternative_comment = self.add_organization_comment(
            client=client, basket_id=basket.id, host=self.alternative_site_host
        )
        alternative_comment_id = alternative_comment['id']

        main_organization_comment_id = self.client.get(
            f'/api/v2.0/projects/{basket.id}/comments/'
        )['value'][0]['id']
        self.assertEqual(main_organization_comment_id, alternative_comment_id)

    def create_user_and_basket_on_site(self, site):
        self.create_organization_with_site(site)
        set_current_site(site)
        user = UserFactory(password=self.test_password)
        basket = BasketFactory(owner=user, status=Basket.EDITABLE)
        basket.save()
        return user, basket

    def create_organization_with_site(self, site):
        organization = OrganizationFactory(
            site=site, showname='Alternative Organization')
        organization.partners_enabled.add(self.partner)
        organization.save()
        return organization

    def create_and_login_api_client(self, user):
        client = APIClient()
        client.login(username=user.username,
                     password=self.test_password)
        return client

    @staticmethod
    def add_organization_comment(client, basket_id, host):
        url = f'/api/v2.0/projects/{basket_id}/comments/'
        comment_payload = {
            'parent': None,
            'text': 'This is a new Main Comment'
        }
        response = client.post(url, comment_payload,
                               format='json',
                               HTTP_HOST=host)
        return response.data

    def test_comments_access(self):
        add_comment_response = self.client.put(
            reverse('basket-ajax:basket-comments', args=(self.basket.id, )),
            self.comment_data
        )
        comment_id = add_comment_response['id']
        project_comments_response = self.client.get(
            f'/api/v2.0/projects/{self.basket.id}/comments/'
        )
        self.assertEqual(
            project_comments_response['value'][0]['id'], comment_id
        )

    def test_comment_crud_operations(self):
        add_comment_response = self.client.put(
            reverse('basket-ajax:basket-comments', args=(self.basket.id, )),
            self.comment_data
        )
        comment_id = add_comment_response['id']

        # Now try updating one comment (PUT)
        addition_response = self.threeydclient.put(
            f'/api/v2.0/projects/{self.basket.id}/comments/{comment_id}/',
            json.dumps({
                'text': 'This is an updated Main Comment'
            }),
            content_type='application/json',
            return_json=True
        )
        self.assertEqual(addition_response.status_code, 200)

        # Now try fetching this one
        project_comments_response = self.client.get(
            f'/api/v2.0/projects/{self.basket.id}/comments/'
        )
        self.assertEqual(
            project_comments_response['value'][0]['text'],
            'This is an updated Main Comment'
        )

        deletion_response = self.threeydclient.delete(
            f'/api/v2.0/projects/{self.basket.id}/comments/{comment_id}/'
        )
        self.assertEqual(deletion_response.status_code, 204)

        # now lets try to get this back
        project_comments_response = self.client.get(
            f'/api/v2.0/projects/{self.basket.id}/comments/'
        )
        self.assertEqual(len(project_comments_response['value']), 0)
