from django.conf.urls import url

from apps.b3_api.projects.comments.views import \
    ListProjectCommentsView, ProjectCommentsDetailsView

urlpatterns = [
    url(r'^$', ListProjectCommentsView.as_view()),
    url(
        r'^(?P<comment_id>comments-[0-9A-Za-z-]*)/$',
        ProjectCommentsDetailsView.as_view()
    ),
]
