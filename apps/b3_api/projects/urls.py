from django.conf.urls import url, include

urlpatterns = [
    url(
        r'^(?P<project_id>\d+)/comments/',
        include('apps.b3_api.projects.comments.urls')
    ),
]
