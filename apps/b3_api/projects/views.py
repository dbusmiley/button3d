from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.services.permissions import IsServiceRelated

from apps.basket.models import Basket


class BaseProjectsView(APIView):
    """
    Base class that can fetch the right Service for
    /services/<id>/ functions
    """
    permission_classes = (IsAuthenticated, IsServiceUser, IsServiceRelated)

    def get_project(self):
        return Basket.all_objects.filter(id=self.kwargs['project_id']).first()
