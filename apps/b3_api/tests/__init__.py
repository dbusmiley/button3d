from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory
import unittest
from django.conf import settings


@unittest.skipIf(not settings.ENABLE_OPENAPI_SPEC_VALIDATION,
                 "OpenAPI specifcation validation not enabled")
class OpenApiTest(AuthenticatedTestCase):
    def setUp(self):
        super(OpenApiTest, self).setUp()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png', site=self.site
        )
        # self.partner.site = self.site
        self.partner.save()
        self.partner.users.add(self.user)

    def test_services(self):
        r = self.client.get('/api/v2.0/services/')
        self.assertEqual(r.status_code, 200)
        print(r.content)
        # [{"id":81,"name":"Meltwerk","logo":"http://my.3yd/media/meltwerk-logo.png","currency":"EUR","enabled":true}]

    def test_service_payment_methods(self):
        r = self.client.get(
            '/api/v2.0/services/{:d}/payment-methods/'.format(self.partner.id)
        )
        print(r.content)
        self.assertEqual(r.status_code, 200)
