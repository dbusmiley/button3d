from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.users.serializers.user_profile import UserProfileSerializer
from apps.b3_api.users.views.base import UserBaseView


class UserProfileView(UserBaseView, RetrieveUpdateAPIView):
    serializer_class = UserProfileSerializer
