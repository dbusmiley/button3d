from django.conf import settings
from rest_framework import status
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response

from apps.b3_api.users.serializers.user_preference import \
    UserPreferenceSerializer
from apps.b3_api.users.views.base import UserBaseView


class UserPreferenceView(UserBaseView, RetrieveUpdateAPIView):
    serializer_class = UserPreferenceSerializer

    def put(self, request, *args, **kwargs):
        requested_currency = request.data['currency']
        currencies = settings.SUPPORTED_CURRENCY_SET

        if requested_currency not in currencies:
            return Response(None, status=status.HTTP_406_NOT_ACCEPTABLE)

        request.session['currency'] = requested_currency
        return Response(None, status=status.HTTP_200_OK)
