from django.contrib.auth.models import User
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView


class UserBaseView(APIView):
    permission_classes = (
        IsAuthenticated,
    )

    def get_object(self):
        return get_object_or_404(User, id=self.request.user.id)
