# flake8: noqa
from .user_preference import UserPreferenceView
from .user_profile import UserProfileView
from .base import UserBaseView
