from django.contrib.auth.models import User
from rest_framework import serializers

from apps.b3_api.utils.serializer_fields import Base64ImageField, \
    StrOrNoneField
from apps.b3_organization.utils import get_current_org


class BaseRelatedAttributeField(serializers.Field):
    """
    Base class for fields that serialize attribute values of related objects.
    Its purpose is to allow read/write access to serialized attributess
    """

    def get_attribute(self, instance):
        return instance

    def to_internal_value(self, data):
        return data


class AvatarField(Base64ImageField):
    def get_attribute(self, instance):
        return instance

    def to_representation(self, instance):
        # if user has avatar, return its absolute url
        if instance.userprofile.avatar:
            request = self.context.get('request')
            if request:
                return request.build_absolute_uri(
                    instance.userprofile.avatar_url
                )
            else:
                return instance.userprofile.avatar_url


class NameSerializer(serializers.Serializer):
    """
    Manages nested JSON object {"first": 'first_name', "last": 'last_name'}
    """
    first = StrOrNoneField(source='first_name', allow_null=True)
    last = StrOrNoneField(source='last_name', allow_null=True)


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.userprofile.full_name

    class Meta:
        fields = (
            'id',
            'name',
            'email',
        )
        model = User


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'avatar',
            'email',
            'name',
            'roles',
            'organization',
        )
        read_only_fields = ('email',)

    # unfolds its fields to global validated_data
    name = NameSerializer(source='*')

    roles = serializers.ListField(source='userprofile.roles', read_only=True)
    avatar = AvatarField(allow_null=True)
    organization = serializers.SerializerMethodField()

    def get_organization(self, obj):
        current_organization = get_current_org()
        return current_organization.showname

    def update(self, instance, validated_data):
        """
        Gets executed when serialiser.save() is called. Since we are only
        updating existing users, only update() is implemented

        :param instance:
        :param validated_data:
        :return:
        """
        instance.first_name = validated_data["first_name"] or ''
        instance.last_name = validated_data["last_name"] or ''
        instance.save()

        profile = instance.userprofile

        avatar = validated_data["avatar"]
        if avatar:
            profile.avatar.save(avatar.name, avatar, save=False)
        profile.save()
        return instance
