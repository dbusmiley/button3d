from django.contrib.auth.models import User
from rest_framework import serializers

from apps.b3_core.utils import determine_default_currency


class UserPreferenceSerializer(serializers.ModelSerializer):
    def determine_request_session_values_for(self, key):
        request = self.context['request']
        return request.session.get(key, None)

    class Meta:
        model = User
        fields = (
            'language',
            'country',
            'currency'
        )

    language = serializers.SerializerMethodField()
    country = serializers.SerializerMethodField()
    currency = serializers.SerializerMethodField()

    def get_language(self, obj):
        language = self.determine_request_session_values_for('_language')
        return language

    def get_country(self, obj):
        country = self.determine_request_session_values_for('country')
        return country

    def get_currency(self, obj):
        return determine_default_currency(self.context['request'])
