from django.contrib.auth.models import User
from rest_framework import serializers

from drf_payload_customizer.mixins import PayloadConverterMixin


class UserPartSerializer(PayloadConverterMixin, serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    avatar_url = serializers.URLField(
        source='userprofile.avatar_url')

    def get_name(self, obj):
        return str(obj)

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'avatar_url',
        )
        read_only_fields = fields
