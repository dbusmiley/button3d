from django.contrib.auth.models import User
from rest_framework import serializers

from drf_payload_customizer.mixins import PayloadConverterMixin


class UserSerializer(PayloadConverterMixin, serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    company = serializers.CharField(
        source='userprofile.company_from_address'
    )
    customer_number = serializers.CharField(
        source='userprofile.customer_number'
    )

    def get_name(self, obj):
        return obj.userprofile.full_name

    class Meta:
        fields = (
            'id',
            'name',
            'first_name',
            'last_name',
            'email',
            'company',
            'customer_number'
        )
        model = User
