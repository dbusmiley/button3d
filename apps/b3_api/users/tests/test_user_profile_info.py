import json
from django.contrib.auth.models import User

from apps.b3_api.users.tests.data.files import valid_avatar, stl_file
from apps.b3_organization.utils import get_current_org
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase, \
    UserFactory

USER_PROFILE_ENDPOINT = '/api/v2.0/users/profile/'


class LoggedInUserProfileDetailsTest(AuthenticatedTestCase):
    def test_logged_in_user_id(self):
        logged_in_user_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertEqual(
            logged_in_user_response.data['email'], self.user.email
        )
        self.assertEqual(
            logged_in_user_response.data['name']['first'], None
        )
        self.assertEqual(
            logged_in_user_response.data['name']['last'], None
        )

    def test_user_name_not_set(self):
        no_name_user = UserFactory(password=self.test_password)
        no_name_user.save()
        self.client.login(email=no_name_user.email,
                          password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertEqual(user_profile_response.data['name']['first'], None)
        self.assertEqual(user_profile_response.data['name']['last'], None)

    def test_user_name_set(self):
        named_user = UserFactory(password=self.test_password)
        named_user.first_name = 'Judith'
        named_user.last_name = 'Schuette'
        named_user.save()
        self.client.login(email=named_user.email, password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        first_name = user_profile_response.data['name']['first']
        last_name = user_profile_response.data['name']['last']
        self.assertEqual(first_name, 'Judith')
        self.assertEqual(last_name, 'Schuette')

    def test_default_user_role(self):
        user = UserFactory(password=self.test_password)
        user.save()
        self.client.login(email=user.email, password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertTrue('user' in user_profile_response.data['roles'])

    def test_admin_role_user(self):
        admin_user = UserFactory(password=self.test_password)
        admin_user.is_superuser = True
        admin_user.save()
        self.client.login(email=admin_user.email, password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertTrue('admin' in user_profile_response.data['roles'])

    def test_organization_role_user(self):
        organization_user = UserFactory(password=self.test_password)
        organization_user.save()
        current_organization = get_current_org()
        current_organization.organization_panel_admins.add(organization_user)
        current_organization.save()
        self.client.login(email=organization_user.email,
                          password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertTrue(
            'organizationUser' in user_profile_response.data['roles'])

    def test_partner_role_user(self):
        partner_user = UserFactory(password=self.test_password)
        partner = PartnerFactory(name='The Company')
        partner.users.add(partner_user)
        partner.save()
        self.client.login(email=partner_user.email,
                          password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertTrue(
            'partnerUser' in user_profile_response.data['roles'])

    def test_user_avatar_empty_by_default(self):
        user = UserFactory(password=self.test_password)
        self.client.login(email=user.email,
                          password=self.test_password)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        self.assertEqual(None, user_profile_response.data['avatar'])

    def test_setting_user_avatar(self):
        user = UserFactory(password=self.test_password)
        self.client.login(email=user.email,
                          password=self.test_password)

        payload = {
            "avatar": valid_avatar,
            "country": None,
            "phone": None,
            "company": None,
            "name": {
                "last": None,
                "first": None
            }
        }
        user_profile_response = self.client.put(
            USER_PROFILE_ENDPOINT,
            data=json.dumps(payload),
            content_type="application/json"
        )
        self.assertEqual(user_profile_response.status_code, 200)
        user_profile_response = self.client.get(
            USER_PROFILE_ENDPOINT
        )
        avatar_field = user_profile_response.data['avatar']
        self.assertTrue('http://my.3yd/media/user-avatars/' in avatar_field)
        self.assertTrue('.png' in avatar_field)

    def test_setting_user_name_via_api(self):
        user = UserFactory(password=self.test_password)
        self.client.login(email=user.email,
                          password=self.test_password)
        payload = {
            "avatar": None,
            "name": {
                "last": 'Mustermann',
                "first": 'Max'
            }
        }
        user_profile_response = self.client.put(
            USER_PROFILE_ENDPOINT,
            data=json.dumps(payload),
            content_type="application/json"
        )
        self.assertTrue(user_profile_response.status_code, 200)
        user = User.objects.get(pk=user.id)
        self.assertEqual(user.first_name, 'Max')
        self.assertEqual(user.last_name, 'Mustermann')

    def test_setting_pdf_as_avatar(self):
        user = UserFactory(password=self.test_password)
        self.client.login(email=user.email,
                          password=self.test_password)
        payload = {
            "avatar": stl_file,
            "name": {
                "last": None,
                "first": None
            }
        }
        user_profile_response = self.client.put(
            USER_PROFILE_ENDPOINT,
            data=json.dumps(payload),
            content_type="application/json"
        )
        json_response = json.loads(user_profile_response.content)
        expected = {u'code': u'User_VALIDATION_ERROR',
                    u'message': u'Model field validation error.',
                    u'moreInfo': [
                        {u'field': u'avatar',
                         u'message': [
                             u'Upload a valid image. The file you uploaded was'
                             u' either not an image or a corrupted image.']}]}
        self.assertEqual(json_response, expected)
        self.assertEqual(user_profile_response.status_code, 400)

    def test_empty_payload(self):
        user = UserFactory(password=self.test_password)
        self.client.login(email=user.email,
                          password=self.test_password)
        payload = {"name": {}}
        user_profile_response = self.client.put(
            USER_PROFILE_ENDPOINT,
            data=json.dumps(payload),
            content_type="application/json"
        )
        json_response = json.loads(user_profile_response.content)
        expected = {
            "code": "User_VALIDATION_ERROR",
            "message": "Model field validation error.",
            "moreInfo": [{"field": "avatar",
                          "message": ["No file was submitted."]},
                         {"field": "name",
                          "message": {
                              "first": ["This field is required."],
                              "last": ["This field is required."]}}]
        }
        self.assertEqual(json_response, expected)
        self.assertTrue(user_profile_response.status_code, 400)
