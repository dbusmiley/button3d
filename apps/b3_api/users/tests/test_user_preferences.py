import json

from apps.b3_tests.factories import UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class LoggedInUserPreferencesTest(AuthenticatedTestCase):
    def setUp(self):
        self.user = UserFactory(password=self.test_password)
        super(LoggedInUserPreferencesTest, self).setUp()

    def test_update_currencies(self):
        self.client.login(email=self.user.email,
                          password=self.test_password)

        user_preference_response_raw = self.client.get(
            f'/api/v2.0/users/{self.user.id}/preference/'
        )

        # In fact, the returned is EUR - but one cannot be sure so not
        # checking it here as internal magic determines host country
        self.assertIsNotNone(user_preference_response_raw.data['currency'])

        # Now update currency
        payload = {"currency": 'USD'}
        self.client.put(
            f'/api/v2.0/users/{self.user.id}/preference/',
            data=json.dumps(payload),
            content_type="application/json"
        )

        # Now get it (should get it from session)
        user_preference_response_raw = self.client.get(
            f'/api/v2.0/users/{self.user.id}/preference/',
        )
        self.assertEqual(user_preference_response_raw.data['currency'], 'USD')
