import base64
from mimetypes import guess_extension, guess_type

from django.core.files.base import ContentFile


def convert_to_content_file(base64text):
    extension = guess_extension(guess_type(base64text)[0])
    is_base_64 = ';base64,' in base64text
    accepted_extensions = ['.png', '.jpe', '.jpg']
    extension_valid = extension in accepted_extensions
    if not extension_valid or not is_base_64:
        raise TypeError
    file_format, image_string = base64text.split(';base64,')
    ext = file_format.split('/')[-1]
    data = ContentFile(base64.b64decode(image_string))
    file_name = "'avatar." + ext
    return file_name, data
