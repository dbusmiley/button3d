from django.conf.urls import url

from apps.b3_api.users.views import UserPreferenceView, UserProfileView

urlpatterns = [
    url(r'preference/$', UserPreferenceView.as_view()),
    url(r'profile/$', UserProfileView.as_view()),
]
