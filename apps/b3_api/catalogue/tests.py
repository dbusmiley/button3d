from apps.b3_api.catalogue.serializers import ProductSerializer, \
    ProductWithTechnologySerializer
from apps.b3_tests.factories import ProductFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase


class SerializerTests(TestCase):
    def setUp(self):
        super().setUp()

        self.product = ProductFactory()

    def test_product_serializer_structure(self):
        structure_expected = {
            'id': 0,
            'title': 'string',
        }
        serializer_populated = ProductSerializer(
            instance=self.product)
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_product_with_technology_serializer_structure(self):
        structure_expected = {
            'id': 0,
            'title': 'string',
            'technology': {
                'id': 0,
                'name': 'string',
            }
        }
        serializer_populated = ProductWithTechnologySerializer(
            instance=self.product)
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
