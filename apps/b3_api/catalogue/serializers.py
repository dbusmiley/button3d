from apps.b3_api.technologies.serializers import TechnologySerializer
from drf_payload_customizer.mixins import PayloadConverterMixin
from apps.catalogue.models.product import Product

from rest_framework import serializers


class ProductSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer,
):

    class Meta:
        model = Product
        fields = (
            'id',
            'title',
        )


class ProductWithTechnologySerializer(ProductSerializer):
    technology = TechnologySerializer()

    class Meta:
        model = ProductSerializer.Meta.model
        fields = ProductSerializer.Meta.fields + ('technology', )
