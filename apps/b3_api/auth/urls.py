from django.conf.urls import url

from apps.b3_api.auth.views import LoginView

urlpatterns = [
    url(r'^login/$', LoginView.as_view()),
]
