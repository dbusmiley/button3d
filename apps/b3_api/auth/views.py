from rest_framework import status
from rest_framework.authentication import SessionAuthentication, \
    BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView


class LoginView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated, )

    @staticmethod
    def get_user_authentication_dict(request):
        return {'userId': request.user.id}

    def get(self, request, *args, **kwargs):
        user_auth_dict = self.get_user_authentication_dict(request)
        return Response(user_auth_dict, status=status.HTTP_200_OK)
