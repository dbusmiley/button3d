from rest_framework.authtoken.models import Token
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory

from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories import UserFactory, PartnerFactory
from apps.b3_organization.utils import get_current_site

from apps.b3_api.services.views import ListServices


class TokenAuthServicePanelAccess(TestCase):
    def setUp(self):
        super(TokenAuthServicePanelAccess, self).setUp()
        self.user = UserFactory()
        self.user.save()
        self.factory = APIRequestFactory()
        self.token = Token.objects.create(user=self.user)
        self.token.save()

    def test_token_auth_unsuccessful_login(self):
        projects_response_raw = self.client.get('/api/v2.0/services/')
        self.assertEqual(projects_response_raw.status_code, 403)
        self.assertEqual(
            projects_response_raw.data, {
                "status_code": 403,
                "detail": 'Authentication credentials were not provided.'
            }
        )

    def test_token_auth_successful_login_no_permission(self):
        service_request = self.factory.get('/api/v2.0/services/')
        force_authenticate(
            service_request, user=self.user, token=self.token.key
        )
        response = ListServices.as_view()(service_request)
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(response.data['code'], 'SERVICE_ACCESS_DENIED')

    def test_token_auth_successful_login_with_permission(self):
        # Now add this user as a partner user and see if he can access
        partner = PartnerFactory(name='Meltwerk', logo='meltwerk-logo.png')
        partner.users.add(self.user)
        partner.site = get_current_site()
        partner.save()

        service_request = self.factory.get('/api/v2.0/services/')
        force_authenticate(
            service_request, user=self.user, token=self.token.key
        )
        response = ListServices.as_view()(service_request)
        self.assertNotEqual(response.status_code, 401)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['id'], partner.id)
        self.assertEqual(response.data[0]['name'], partner.name)
