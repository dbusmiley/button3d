from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class LoggedInUserDetailsTest(AuthenticatedTestCase):
    def setUp(self):
        super(LoggedInUserDetailsTest, self).setUp()

    def test_logged_in_user_id(self):
        logged_in_user_response = self.client.get('/api/v2.0/auth/login/')
        self.assertEqual(logged_in_user_response.data['userId'], self.user.id)
