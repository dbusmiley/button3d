from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.technologies.serializers import TechnologySerializer
from apps.catalogue.models.technology import Technology


class ListTechnologies(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = TechnologySerializer

    def get_queryset(self):
        return Technology.objects.all()
