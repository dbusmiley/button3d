from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from apps.catalogue.models.technology import Technology


class TechnologySerializer(ModelSerializer):
    name = CharField(source='title')

    class Meta:
        model = Technology
        fields = (
            'id',
            'name',
        )
