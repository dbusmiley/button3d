import pytz
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response


class Timezones(ListAPIView):
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        return pytz.all_timezones

    def get(self, request, *args, **kwargs):
        return Response(pytz.all_timezones)
