from rest_framework import serializers
from apps.catalogue.models.color import Color


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = (
            'id',
            'title_en',
            'title_de',
        )
