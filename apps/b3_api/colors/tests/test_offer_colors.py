from apps.b3_tests.factories import PartnerFactory, ProductFactory, \
    StockRecordFactory, create_post_processing
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OfferColorTest(AuthenticatedTestCase):
    def setUp(self):
        super(OfferColorTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = ProductFactory()
        self.stock_record = StockRecordFactory(
            product=self.product, partner=self.partner
        )
        self.post_processing = create_post_processing(
            self.stock_record, has_colors=True
        )
        self.color_ids = [
            color.id for color in self.post_processing.colors.all()
        ]
        self.url = (f'/api/v2.0/'
                    f'services/{self.partner.id}/'
                    f'offers/{self.stock_record.id}/'
                    f'post-processings/{self.post_processing.id}/')

    def test_colors(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            sorted(response.json()['colors']), sorted(self.color_ids)
        )

    def test_no_colors(self):
        post_processing = create_post_processing(
            self.stock_record, has_colors=False
        )
        url = (f'/api/v2.0/'
               f'services/{self.partner.id}/'
               f'offers/{self.stock_record.id}/'
               f'post-processings/{post_processing.id}/')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['colors'], [])
