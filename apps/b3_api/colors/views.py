from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import ListAPIView

from apps.catalogue.models.color import Color

from apps.b3_api.colors.serializers import ColorSerializer


class ListColors(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ColorSerializer

    def get_queryset(self):
        return Color.objects.all()
