from django.conf.urls import url

from apps.b3_api.services.order_pricing_rules.views import \
    OrderPricingRulesView


urlpatterns = [
    url(r'^$', OrderPricingRulesView.as_view()),
]
