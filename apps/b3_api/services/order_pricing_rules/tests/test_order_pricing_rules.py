import json

from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.models import Partner


class ServiceTestCustomPricingRulesTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceTestCustomPricingRulesTest, self).setUp()
        self.site = get_current_site()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()
        self.url = f'/api/v2.0/services/{self.partner.id}/order-pricing-rules/'

    def test_default_custom_pricing_rules(self):
        pricing_rules_response_raw = self.client.get(self.url)
        pricing_rules_response = pricing_rules_response_raw.data
        self.assertEqual(pricing_rules_response['extraFeesPerOrder'], [])
        self.assertEqual(
            pricing_rules_response['minimumOrderPriceValue'], '0.00'
        )
        self.assertEqual(
            pricing_rules_response['minimumOrderPriceEnabled'], False
        )

    def test_create_of_custom_pricing_rules(self):
        payload = {
            "extraFeesPerOrder": [
                {
                    "extraFeeValue": 100.0,
                    "extraFeeName": "Handling Fee"
                },
                {
                    "extraFeeValue": 105.0,
                    "extraFeeName": "Handling Fee 2"
                }
            ],
            "minimumOrderPriceValue": 40.0,
            "minimumOrderPriceEnabled": False
        }
        pricing_rules_response_raw = self.client.put(
            self.url,
            data=json.dumps(payload),
            content_type='application/json'
        )
        partner = Partner.objects.get(id=self.partner.id)
        self.assertEqual(partner.minimum_order_price_enabled, False)
        self.assertEqual(partner.minimum_order_price_value, 40.0)

        extra_fees_in_response = pricing_rules_response_raw.data[
            'extraFeesPerOrder']
        partner_extra_fees = partner.extra_fees.all()
        self.assertEqual(len(partner_extra_fees), 2)

        # This validates the response and also the database table values
        for extra_fee in partner_extra_fees:
            self.assertIn(
                {
                    "extraFeeValue": f'{extra_fee.value:2f}',
                    "extraFeeName": extra_fee.type
                },
                extra_fees_in_response
            )

    def test_invalid_update_of_custom_pricing_rules(self):
        # New payload without extraFees
        payload_modified = {
            "minimumOrderPriceValue": 50.0,
            "minimumOrderPriceEnabled": True
        }
        response = self.client.put(
            self.url,
            data=json.dumps(payload_modified),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 400)

        partner = Partner.objects.get(id=self.partner.id)
        partner_extra_fees = partner.extra_fees.all()
        # If payload is not valid, don't do any changes
        self.assertEqual(len(partner_extra_fees), 0)
        self.assertEqual(partner.minimum_order_price_enabled, False)
        self.assertEqual(partner.minimum_order_price_value, 0)

    def test_custom_pricing_rules_with_larger_payload(self):
        payload_modified_with_fees = {
            "extraFeesPerOrder": [
                {
                    "extraFeeValue": 100.0,
                    "extraFeeName": "Handling Fee"
                },
                {
                    "extraFeeValue": 105.0,
                    "extraFeeName": "Handling Fee 2"
                },
                {
                    "extraFeeValue": 106.0,
                    "extraFeeName": "Handling Fee 4"
                },
                {
                    "extraFeeValue": 107.0,
                    "extraFeeName": "Handling Fee 5"
                }
            ],
            "minimumOrderPriceValue": 60.0,
            "minimumOrderPriceEnabled": True
        }
        self.client.put(
            f'/api/v2.0/services/{self.partner.id}/order-pricing-rules/',
            data=json.dumps(payload_modified_with_fees),
            content_type='application/json'
        )
        partner = Partner.objects.get(id=self.partner.id)
        partner_extra_fees = partner.extra_fees.all()
        self.assertEqual(len(partner_extra_fees), 4)
        self.assertEqual(partner.minimum_order_price_enabled, True)
        self.assertEqual(partner.minimum_order_price_value, 60.0)

        for item in partner_extra_fees:
            self.assertIn(
                {"extraFeeValue": item.value, "extraFeeName":
                    item.type},
                payload_modified_with_fees['extraFeesPerOrder']
            )

    def test_custom_pricing_with_duplicate_keys(self):
        duplicate_payload_modified = {
            "extraFeesPerOrder": [
                {
                    "extraFeeValue": 100.0,
                    "extraFeeName": "Handling Fee DUP"
                },
                {
                    "extraFeeValue": 105.0,
                    "extraFeeName": "Handling Fee DUP"
                }
            ],
            "minimumOrderPriceValue": 60.0,
            "minimumOrderPriceEnabled": True
        }

        duplicate_request_response = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/order-pricing-rules/',
            data=json.dumps(duplicate_payload_modified),
            content_type='application/json'
        )
        self.assertEquals(duplicate_request_response.status_code, 400)

    def test_pricing_rules_validation(self):
        payload = {
            "extraFeesPerOrder": [
                {
                    "extraFeeValue": -100.0,
                    "extraFeeName": "Handling Fee DUP"
                },
            ],
            "minimumOrderPriceValue": -40.0,
            "minimumOrderPriceEnabled": False
        }
        response = self.client.post(
            self.url,
            data=json.dumps(payload),
            content_type='application/json'
        )
        self.assertEqual(response.data['code'],
                         'Partner_VALIDATION_ERROR')
        self.assertEqual(len(response.data['moreInfo']), 2)
