from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.services.order_pricing_rules\
    .serializers.order_pricing_rules import OrderPricingRulesSerializer
from apps.b3_api.services.views import BaseServiceView


class OrderPricingRulesView(BaseServiceView, RetrieveUpdateAPIView):
    serializer_class = OrderPricingRulesSerializer

    def post(self, *args, **kwargs):
        return self.put(*args, **kwargs)
