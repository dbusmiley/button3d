from rest_framework import serializers

from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_order.models import OrderFee


class AppliedOrderFeeSerializer(serializers.ModelSerializer):
    value = CurrencyField(source='excl_tax', coerce_to_string=False)
    type = serializers.CharField(source='name')

    class Meta:
        model = OrderFee
        fields = ('value', 'type')
