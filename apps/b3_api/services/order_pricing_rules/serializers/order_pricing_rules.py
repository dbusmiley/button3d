from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from drf_payload_customizer.mixins import PayloadConverterMixin


from apps.b3_api.services.order_pricing_rules\
    .serializers.extra_fee_per_order import ExtraFeePerOrderSerializer
from apps.partner.models import Partner


class OrderPricingRulesSerializer(PayloadConverterMixin,
                                  serializers.ModelSerializer):
    extra_fees_per_order = ExtraFeePerOrderSerializer(
        many=True, source='extra_fees',
    )

    class Meta:
        model = Partner
        fields = (
            'minimum_order_price_enabled',
            'minimum_order_price_value',
            'extra_fees_per_order',
        )

    def validate_extra_fees_per_order(self, value):
        fee_types = [fee['type'] for fee in value]
        for fee_type in fee_types:
            if fee_types.count(fee_type) > 1:
                raise ValidationError(f'Duplicate extraFeeName: {fee_type}')
        return value

    def update(self, instance, validated_data):
        instance.extra_fees.all().delete()
        extra_fees = validated_data.pop('extra_fees', None)
        if extra_fees is not None:
            for extra_fee in extra_fees:
                instance.extra_fees.create(**extra_fee)

        return super(OrderPricingRulesSerializer, self).update(
            instance, validated_data
        )
