from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.partner.models import ExtraFeePerOrder


class ExtraFeePerOrderSerializer(PayloadConverterMixin,
                                 serializers.ModelSerializer):

    def validate(self, attrs):
        instance = ExtraFeePerOrder(**attrs)
        instance.full_clean(exclude=['partner'])
        return attrs

    class Meta:
        model = ExtraFeePerOrder
        field_mappings = {
            'value': 'extra_fee_value',
            'type': 'extra_fee_name'
        }
        fields = ('value', 'type',)
