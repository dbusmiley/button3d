from apps.b3_api.services.employees.views import EmployeeViewset

from rest_framework import routers

router = routers.SimpleRouter()

router.register(r'', EmployeeViewset, base_name='employees')

urlpatterns = router.urls
