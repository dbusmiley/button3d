from rest_framework import viewsets, mixins

from apps.b3_api.services.views import BaseServiceView
from apps.b3_api.users.serializers.user_part import UserPartSerializer

import button3d.type_declarations as td
import typing as t


class EmployeeViewset(
    BaseServiceView,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    serializer_class = UserPartSerializer

    def get_queryset(self) -> t.Sequence[td.Partner]:
        partner = self.get_partner()
        return partner.users.all()
