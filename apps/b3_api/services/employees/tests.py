import json

from django.urls import reverse
from rest_framework import status

from apps.b3_tests.factories import PartnerFactory, UserFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class EmployeesViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.user.partners.add(self.partner)

        self.users = UserFactory.create_batch(5)
        for user in self.users:
            self.partner.users.add(user)
        self.users.append(self.user)

        UserFactory.create_batch(5)

        # urls & url kwargs
        employee_list_url_kwargs = {'service_id': self.partner.id}
        self.employee_list_url = reverse(
            'api_urls:employees-list', kwargs=employee_list_url_kwargs)

    def test_list(self):
        response = self.client.get(self.employee_list_url)
        response_employees = json.loads(response.content.decode('utf-8'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_employees), len(self.users))
