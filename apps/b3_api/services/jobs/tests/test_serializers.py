from rest_framework.test import APIRequestFactory

from apps.b3_api.services.jobs.serializers import JobCreateSerializer, \
    JobListSerializer
from apps.b3_core.middleware import THREAD_LOCALS
from apps.b3_tests.factories import PartnerFactory, ProductFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory, \
    WorkstationFactory, JobFactory
from apps.b3_order.factories import OrderFactory


class PartSerializerTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        service = PartnerFactory()
        order = OrderFactory(partner=service)
        part = order.lines.first()
        workflow_template = WorkflowTemplateWithStatusesFactory()
        workflow_template.generate_workflow_and_sequences(part.id)
        sequence = order.sequences.all().first()

        workstation = WorkstationFactory(partner=service)
        material = ProductFactory()
        workstation.materials.add(material)

        self.job = JobFactory(
            workstation=workstation,
            material=material,
            created_by=self.user
        )
        self.job.sequences.add(sequence)

        factory = APIRequestFactory()
        self.fake_request = factory.get('/')
        THREAD_LOCALS.current_request = self.fake_request

    def test_create_serializer_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'materialId': 0,
            'workstationId': 0,
            'workstationId': 0,
            'type': 'Printing',
            'sequenceIds': [
                0
            ],
            'processParameters': 'string',
            'materialBatchNumber': 'string',
            'schedule': {
                'startTime': 'string',
                'finishTime': 'string',
            }
        }

        serializer_populated = JobCreateSerializer(
            self.job,
            context={'request': self.fake_request}
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_list_serializder_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'materialId': 0,
            'workstationId': 0,
            'status': 'string',
            'schedule': {
                'startTime': 'string',
                'finishTime': 'string',
            },
        }
        serializer_populated = JobListSerializer(
            self.job
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
