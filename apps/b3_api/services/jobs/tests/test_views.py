import json
import datetime
import pytz
from django.core.files.uploadedfile import SimpleUploadedFile

from django.urls import reverse
from rest_framework import status

from apps.b3_mes.models import Job, Sequence, JobAttachment
from apps.b3_order.factories import OrderFactory
from apps.b3_order.models import OrderLine
from apps.b3_tests.factories import PartnerFactory, ProductFactory, \
    BasketLineFactory
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory, WorkstationFactory, JobFactory, \
    JobAttachmentFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class JobsViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.partner)

        self.num_parts = 5

        self.material = ProductFactory()
        self.workstation = WorkstationFactory(
            partner=self.partner,
            is_printing=True)
        self.workstation.materials.add(self.material)

        for i in range(self.num_parts):
            basket_line = BasketLineFactory(stockrecord__partner=self.partner)
            order = OrderFactory(project=basket_line.basket)
            part = order.lines.first()
            workflow_template = WorkflowTemplateWithStatusesFactory()
            workflow = workflow_template.generate_workflow_and_sequences(
                part.id)

            # set only last status to a printing workstation
            statuses = workflow.get_statuses_ordered()
            for workflow_status in statuses[:-1]:
                workflow_status.workstation = None
                workflow_status.save()

            self.last_status = statuses[-1]
            self.last_status.workstation = self.workstation
            self.last_status.save()

        self.sequence_ids = []
        for part in OrderLine.objects.filter(
            stock_record__partner=self.partner
        ):
            self.sequence_ids += part.get_assignable_sequences(
                self.workstation.id)

        self.name = 'job name'
        self.start_time = pytz.utc.localize(
            datetime.datetime(2018, 12, 12, 20, 9, 30)).isoformat()
        self.finish_time = pytz.utc.localize(
            datetime.datetime(2018, 12, 12, 20, 10, 30)).isoformat()
        self.process_parameters = 'Foo bar'
        self.material_batch_number = 'B-1231'
        self.type = 'printing'
        payload = {
            'workstationId': self.workstation.id,
            'materialId': self.material.id,
            'type': self.type,
            'name': self.name,
            'schedule': {
                'startTime': self.start_time,
                'finishTime': self.finish_time,
            },
            'processParameters': self.process_parameters,
            'materialBatchNumber': self.material_batch_number,
            'sequenceIds': self.sequence_ids
        }
        payload = json.dumps(payload)

        self.job_create_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        self.job = JobFactory(
            name=self.name,
            workstation=self.workstation,
            material=self.material,
            created_by=self.user)

        self.job_attachment = JobAttachmentFactory(
            job=self.job,
            uploader=self.user
        )

        # urls & url kwargs
        job_list_url_kwargs = {
            'service_id': self.partner.id,
        }
        self.job_list_url = reverse(
            'api_urls:jobs-list', kwargs=job_list_url_kwargs)

        job_detail_url_kwargs = {
            'service_id': self.partner.id,
            'job_id': self.job.id
        }
        self.job_detail_url = reverse(
            'api_urls:jobs-detail', kwargs=job_detail_url_kwargs)

        self.job_status_update_url = reverse(
            'api_urls:jobs-update-status', kwargs=job_detail_url_kwargs)

        self.job_sequence_updates_url = reverse(
            'api_urls:jobs-update-sequences', kwargs=job_detail_url_kwargs)

        self.job_reschedule_url = reverse(
            'api_urls:jobs-reschedule', kwargs=job_detail_url_kwargs)
        self.job_instruction_update_url = reverse(
            'api_urls:jobs-update-instructions',
            kwargs=job_detail_url_kwargs)

        # job attachments
        job_attachment_list_url_kwargs = {
            'service_id': self.partner.id,
        }
        self.job_attachment_list_url = reverse(
            'api_urls:job-attachments-list',
            kwargs=job_attachment_list_url_kwargs
        )
        job_attachment_detail_url_kwargs = {
            'service_id': self.partner.id,
            'job_attachment_id': self.job_attachment.id
        }
        self.job_attachment_detail_url = reverse(
            'api_urls:job-attachments-detail',
            kwargs=job_attachment_detail_url_kwargs)

        # Sequence update
        self.sequences = list(
            Sequence.objects.all().values_list('id', flat=True))

    def test_create_job(self):
        job = self._create_job()
        self.assertTrue(
            Job.objects.get(
                id=job.id,
                name=self.name,
                workstation=self.workstation,
                material=self.material,
                type=self.type,
                start_time=self.start_time,
                finish_time=self.finish_time,
                process_parameters=self.process_parameters,
                material_batch_number=self.material_batch_number,
                created_by=self.user
            )
        )

    def test_create_job_sequences(self):
        job = self._create_job()
        self.assertCountEqual(
            job.sequences.all().values_list('id', flat=True),
            self.sequence_ids
        )

    def test_create_job_wrong_material(self):
        self.workstation.materials.remove(self.material)
        response = self.client.post(
            self.job_list_url,
            **self.job_create_post_kwargs,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_job_wrong_sequence(self):
        sequence_id = self.sequence_ids[0]
        sequence = Sequence.objects.get(id=sequence_id)
        sequence.status = self.last_status
        sequence.save()

        response = self.client.post(
            self.job_list_url,
            **self.job_create_post_kwargs,
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def _create_job(self):
        response = self.client.post(
            self.job_list_url,
            **self.job_create_post_kwargs,
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_dict = json.loads(response.content.decode('utf-8'))
        return Job.objects.get(id=response_dict['id'])

    def test_job_list(self):
        Job.objects.all().delete()

        jobs = JobFactory.create_batch(
            5,
            workstation=self.workstation,
            material=self.material,
            created_by=self.user
        )
        response = self.client.get(
            self.job_list_url,
        )
        response_jobs = json.loads(response.content.decode('utf-8'))

        self.assertEqual(len(jobs), len(response_jobs))

    def test_job_list_intervals(self):
        Job.objects.all().delete()

        start_time = '2018-01-01T00:00:00+00:00Z'
        finish_time = '2019-01-01T00:00:00+00:00Z'

        start_time_within_range = pytz.utc.localize(
            datetime.datetime(2018, 6, 1))
        finish_time_within_range = pytz.utc.localize(
            datetime.datetime(2018, 9, 1))

        start_time_outside_range = pytz.utc.localize(
            datetime.datetime(2017, 1, 1))
        finish_time_outside_range = pytz.utc.localize(
            datetime.datetime(2017, 12, 1))

        job_within_range = JobFactory(
            workstation=self.workstation,
            material=self.material,
            created_by=self.user,
            start_time=start_time_within_range,
            finish_time=finish_time_within_range
        )

        # job_outside_range
        JobFactory(
            workstation=self.workstation,
            material=self.material,
            created_by=self.user,
            start_time=start_time_outside_range,
            finish_time=finish_time_outside_range
        )

        response = self.client.get(
            f'{self.job_list_url}'
            f'?start_time={start_time}&finish_time={finish_time}',
        )
        response_jobs = json.loads(response.content.decode('utf-8'))
        response_jobs_ids = [job['id'] for job in response_jobs]

        self.assertCountEqual(response_jobs_ids, [job_within_range.id])

    def test_job_detail(self):
        job = self._create_job()
        job_detail_url_kwargs = {
            'service_id': self.partner.id,
            'job_id': job.id
        }
        self.job_detail_url = reverse(
            'api_urls:jobs-detail', kwargs=job_detail_url_kwargs)
        response = self.client.get(self.job_detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_job_detail_attachments(self):
        response = self.client.get(self.job_detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response_job = json.loads(response.content.decode('utf-8'))
        attachments_actual = [
            attachment['id'] for attachment in response_job['attachments']]
        attachments_expected = self.job.attachments.all().values_list(
            'id', flat=True)
        self.assertCountEqual(attachments_actual, attachments_expected)

    def test_job_detail_sequences_assigned(self):
        parts_qs = self.workstation.get_assignable_parts()
        parts_sequences_dict = {}
        for part in parts_qs:
            parts_sequences_dict[part.id] = self.job.sequences.filter(
                id__in=part.get_assignable_sequences(
                    self.workstation.id))
        response = self.client.get(self.job_detail_url)
        response_job = json.loads(response.content.decode('utf-8'))
        for part in response_job['parts']:
            self.assertCountEqual(
                part['sequencesAssigned'], parts_sequences_dict[part['id']])

    def test_job_detail_all_sequences(self):
        parts_qs = self.workstation.get_assignable_parts()
        parts_sequences_dict = {}
        for part in parts_qs:
            parts_sequences_dict[part.id] = part.get_assignable_sequences(
                self.workstation.id)
        response = self.client.get(self.job_detail_url)
        response_job = json.loads(response.content.decode('utf-8'))
        for part in response_job['parts']:
            self.assertCountEqual(
                part['sequencesAllPossible'], parts_sequences_dict[part['id']])

    def test_job_status_update(self):
        self.job.status = Job.STATUS_SCHEDULED
        self.job.save()
        payload = {
            'status': Job.STATUS_FINISHED
        }
        payload = json.dumps(payload)
        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_status_update_url,
            **update_kwargs
        )
        self.job.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(self.job.status, Job.STATUS_FINISHED)

    def test_job_sequence_update_add(self):
        old_length = self.job.sequences.count()

        payload = {
            'addSequencesIds': [self.sequences[0]],
        }
        payload = json.dumps(payload)
        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_sequence_updates_url,
            **update_kwargs
        )
        self.job.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(self.job.sequences.all()), old_length + 1)

    def test_job_sequence_update_remove(self):
        old_length = self.job.sequences.count()

        payload = {
            'removeSequencesIds': [self.sequences[0]],
        }
        payload = json.dumps(payload)
        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_sequence_updates_url,
            **update_kwargs
        )
        self.job.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(self.job.sequences.all()), old_length)

    def test_job_reschedule(self):
        new_workstation = WorkstationFactory(partner=self.partner)
        new_material = ProductFactory()
        new_workstation.materials.add(new_material)
        new_workstation.save()
        new_start_time = pytz.utc.localize(
            datetime.datetime(2019, 12, 1)).isoformat()
        new_finish_time = pytz.utc.localize(
            datetime.datetime(2019, 12, 15)).isoformat()

        payload = {
            'workstationId': new_workstation.id,
            'materialId': new_material.id,
            'type': Job.TYPE_MAINTENANCE,
            'schedule': {
                'startTime': new_start_time,
                'finishTime': new_finish_time,
            },
        }
        payload = json.dumps(payload)

        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_reschedule_url,
            **update_kwargs
        )
        self.job.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(
            Job.objects.filter(
                id=self.job.id,
                workstation=new_workstation,
                material=new_material,
                start_time=new_start_time,
                finish_time=new_finish_time,
            ).exists()
        )

    def test_job_reschedule_wrong_material(self):
        new_workstation = WorkstationFactory(partner=self.partner)
        new_material = ProductFactory()
        new_start_time = pytz.utc.localize(
            datetime.datetime(2019, 12, 1)).isoformat()
        new_finish_time = pytz.utc.localize(
            datetime.datetime(2019, 12, 15)).isoformat()

        payload = {
            'workstationId': new_workstation.id,
            'materialId': new_material.id,
            'type': Job.TYPE_MAINTENANCE,
            'schedule': {
                'startTime': new_start_time,
                'finishTime': new_finish_time,
            },
        }
        payload = json.dumps(payload)

        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_reschedule_url,
            **update_kwargs
        )
        self.job.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_job_instructions_update(self):
        instructions = 'foo-bar'
        payload = {
            'instructions': instructions
        }
        payload = json.dumps(payload)
        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        response = self.client.put(
            self.job_instruction_update_url,
            **update_kwargs
        )
        self.job.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.job.instructions, instructions)

    def test_delete_job_attachment(self):
        response = self.client.delete(self.job_attachment_detail_url)

        self.assertTrue(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            JobAttachment.objects.filter(id=self.job_attachment.id).exists()
        )

    def test_create_job_attachment(self):
        filename = 'foo-bar'
        uploaded_file = SimpleUploadedFile(
            filename, b'Hello world',
            content_type='multipart/form-data')
        create_job_attachment_post_kwargs = {
            'file': uploaded_file,
            'jobId': self.job.id
        }
        response = self.client.post(
            self.job_attachment_list_url,
            create_job_attachment_post_kwargs,
            format='multipart'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
