from django.db.models import QuerySet
from rest_framework import serializers

from apps.b3_api.attachment.attachment_base_serializer import \
    AttachmentBaseSerializer
from apps.b3_api.services.jobs.fields import (
    WorkstationPrimaryKeyRelatedField, SequencePrimaryKeyRelatedField,
    JobPrimaryKeyRelatedField,)
from apps.b3_api.services.parts.serializers.part_job_serializer import \
    PartJobSerializer
from apps.b3_api.utils.mixins import OldPayloadConverterMixin
from apps.b3_api.utils.serializers import NestingMixin
from apps.b3_mes.models import Job
from apps.b3_mes.models.job_attachment import JobAttachment
from apps.catalogue.models.product import Product
from apps.partner.models import Partner
import button3d.type_declarations as td

from drf_payload_customizer.mixins import PayloadConverterMixin

import typing as t


class JobMaterialValidationSerializerMixin:
    def validate(self, data: dict):
        self._validate_workstation_material(data)

        return super().validate(data)

    def _validate_workstation_material(self, data: dict) -> None:
        workstation = data['workstation']
        if self.instance:
            material = data.get('material', self.instance.material)
        else:
            material = data['material']
        if not workstation.can_print_material(material):
            raise serializers.ValidationError(f'Workstation {workstation} '
                                              f'cannot print {material}')


class JobAttachmentSerializer(
    OldPayloadConverterMixin,
    AttachmentBaseSerializer
):
    class Meta:
        model = JobAttachment
        fields = AttachmentBaseSerializer.Meta.fields


class JobAttachmentCreateSerializer(
    OldPayloadConverterMixin,
    serializers.ModelSerializer
):
    uploader = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    job_id = JobPrimaryKeyRelatedField(source='job')

    def create(self, validated_data: dict) -> td.JobAttachment:
        filesize = validated_data.get('file').size
        filename = validated_data.get('file').name
        validated_data['filesize'] = filesize
        validated_data['filename'] = filename
        return super().create(validated_data)

    class Meta:
        model = JobAttachment
        fields = (
            'uploader',
            'job_id',
            'file',
        )


class JobBaseSerializer(
    OldPayloadConverterMixin,
    NestingMixin,
    serializers.ModelSerializer
):
    workstation_id = WorkstationPrimaryKeyRelatedField(source='workstation')

    class Meta:
        model = Job
        fields = (
            'id',
            'name',
            'material_id',
            'workstation_id',
            'start_time',
            'finish_time',
        )
        nesting_options = [{
            'name': 'schedule',
            'translations': [
                ('start_time', 'start_time'),
                ('finish_time', 'finish_time'),
            ]
        }]


class JobCreateSerializer(
    JobMaterialValidationSerializerMixin,
    JobBaseSerializer
):
    created_by = serializers.HiddenField(
        default=serializers.CurrentUserDefault())
    material_id = serializers.PrimaryKeyRelatedField(
        source='material',
        queryset=Product.objects.all()
    )
    sequence_ids = SequencePrimaryKeyRelatedField(
        source='sequences',
        many=True,
        allow_empty=False,
    )

    class Meta:
        model = JobBaseSerializer.Meta.model
        fields = JobBaseSerializer.Meta.fields + (
            'created_by',
            'type',
            'sequence_ids',
            'process_parameters',
            'material_batch_number',
        )
        nesting_options = JobBaseSerializer.Meta.nesting_options

    def validate(self, data: dict) -> dict:
        self._validate_sequences(data)

        return super().validate(data)

    def _validate_sequences(self, data: dict) -> None:
        actual_sequences = [s.id for s in data['sequences']]
        partner = Partner.objects.get(id=self.context['service_id'])
        parts = partner.get_order_lines()
        possible_sequences = []
        for part in parts:
            possible_sequences += part.get_assignable_sequences(
                data['workstation'])

        for sequence in actual_sequences:
            if sequence not in possible_sequences:
                raise serializers.ValidationError(
                    f'Sequence {sequence} is not a valid choice for this job')

    def create(self, validated_data: dict) -> td.MESJob:
        sequences = validated_data.pop('sequences')
        job = Job(**validated_data)
        job.save()
        job.sequences.set(sequences)
        return job


class JobListSerializer(
    JobBaseSerializer
):
    class Meta:
        model = JobBaseSerializer.Meta.model
        fields = JobBaseSerializer.Meta.fields + (
            'status',
        )
        nesting_options = JobBaseSerializer.Meta.nesting_options


class JobDetailSerializer(JobBaseSerializer):
    created_by_name = serializers.SerializerMethodField()
    parts = serializers.SerializerMethodField()
    attachments = JobAttachmentSerializer(many=True, allow_empty=True)
    comments = serializers.SerializerMethodField()

    @staticmethod
    def get_created_by_name(obj: td.MESJob) -> str:
        return obj.created_by.userprofile.full_name

    def get_parts(self, job: td.MESJob) -> t.Sequence[dict]:
        parts_qs = job.workstation.get_assignable_parts()
        all_parts_data = []
        for part in parts_qs:
            part_data = self._get_part_serializer_data(job, part)
            if part_data['sequencesAssigned']:
                all_parts_data.append(part_data)
        return all_parts_data

    @staticmethod
    def get_comments(obj: td.MESJob) -> t.List:
        """
        TODO: implement once job_comments are implemented
        :param obj:
        :return:
        """
        return []

    def _get_part_serializer_data(
        self, job: Job, part_job: QuerySet
    ) -> t.Dict:

        serializer = PartJobSerializer(
            part_job, context={'job_id': job.id}
        )
        return serializer.data

    class Meta:
        model = JobBaseSerializer.Meta.model
        fields = JobBaseSerializer.Meta.fields + (
            'created_at',
            'created_by_name',
            'type',
            'status',
            'parts',
            'instructions',
            'attachments',
            'comments',
        )
        nesting_options = JobBaseSerializer.Meta.nesting_options


class JobSequnceUpdateSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer,
):

    add_sequences_ids = SequencePrimaryKeyRelatedField(
        many=True,
        required=False,
        allow_empty=True,
        write_only=True,
    )
    remove_sequences_ids = SequencePrimaryKeyRelatedField(
        many=True,
        required=False,
        allow_empty=True,
        write_only=True,
    )

    class Meta:
        model = Job
        fields = (
            'add_sequences_ids',
            'remove_sequences_ids',
        )

    def update(self, job: td.MESJob, data: dict) -> td.MESJob:
        sequences = list(job.sequences.all())
        for seq in data.get('remove_sequences_ids', []):
            if seq in sequences:
                job.sequences.remove(seq)
        for seq in data.get('add_sequences_ids', []):
            job.sequences.add(seq)
        job.save()
        return job


class JobStatusUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = (
            'status',
        )


class JobUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = ()


class JobRescheduleSerializer(
    JobMaterialValidationSerializerMixin,
    JobBaseSerializer
):
    material_id = serializers.PrimaryKeyRelatedField(
        source='material',
        queryset=Product.objects.all(),
        required=False,
    )

    class Meta:
        model = JobBaseSerializer.Meta.model
        fields = JobBaseSerializer.Meta.fields + (
            'material_id',
            'type',
        )
        nesting_options = JobBaseSerializer.Meta.nesting_options


class JobInstructionsUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Job
        fields = (
            'instructions',
        )
