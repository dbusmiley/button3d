from rest_framework import serializers

from apps.b3_mes.models import Workstation, Sequence, Job

import button3d.type_declarations as td

import typing as t


class WorkstationPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> t.Sequence[td.Workstation]:
        return Workstation.objects.filter(partner=self.context['service_id'])


class SequencePrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> t.Sequence[td.Sequence]:
        return Sequence.objects.filter(
            order__partner=self.context['service_id'])


class JobPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> t.Sequence[td.MESJob]:
        return Job.objects.filter(
            workstation__partner=self.context['service_id'])
