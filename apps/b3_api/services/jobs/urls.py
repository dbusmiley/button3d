from rest_framework import routers

from apps.b3_api.services.jobs.views import JobViewset, JobAttachmentViewset

router = routers.SimpleRouter()

router.register(
    r'attachments', JobAttachmentViewset, base_name='job-attachments')
router.register(r'', JobViewset, base_name='jobs')

urlpatterns = router.urls
