from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework import viewsets, mixins

from apps.b3_api.services.jobs.serializers import JobCreateSerializer, \
    JobListSerializer, JobDetailSerializer, JobStatusUpdateSerializer, \
    JobRescheduleSerializer, \
    JobInstructionsUpdateSerializer, JobSequnceUpdateSerializer, \
    JobUpdateSerializer, JobAttachmentCreateSerializer
from apps.b3_api.services.views import BaseMESView
from apps.b3_mes.models import Job
from apps.b3_mes.models import JobAttachment

import button3d.type_declarations as td

import typing as t

from dateutil import parser


class JobViewset(
    BaseMESView,
    viewsets.ModelViewSet,
):
    lookup_url_kwarg = 'job_id'

    def get_queryset(self) -> t.Sequence[td.MESJob]:
        jobs = Job.objects.filter(workstation__partner=self.get_partner())

        start_time_str = self.request.query_params.get('start_time', None)
        if start_time_str:
            start_time = parser.parse(start_time_str)
            jobs = jobs.filter(start_time__gte=start_time)

        finish_time_str = self.request.query_params.get('finish_time', None)
        if finish_time_str:
            finish_time = parser.parse(finish_time_str)
            jobs = jobs.filter(finish_time__lte=finish_time)

        return jobs

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_service().id
        return context

    def get_serializer_class(self) -> td.Serializer:
        if self.action == 'create':
            return JobCreateSerializer
        elif self.action == 'list':
            return JobListSerializer
        elif self.action == 'retrieve':
            return JobDetailSerializer
        elif self.action == 'update_status':
            return JobStatusUpdateSerializer
        elif self.action == 'update_sequences':
            return JobSequnceUpdateSerializer
        elif self.action == 'reschedule':
            return JobRescheduleSerializer
        elif self.action == 'update_instructions':
            return JobInstructionsUpdateSerializer
        elif self.action == 'update':
            return JobUpdateSerializer
        elif self.action == 'partial_update':
            return JobUpdateSerializer

    def get_object(self) -> td.MESJob:
        queryset = self.filter_queryset(self.get_queryset())

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {'pk': self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        self.check_object_permissions(self.request, obj)

        return obj

    @action(detail=True, methods=['put'])
    def update_status(
        self, request: td.Request, *args, **kwargs
    ) -> td.DjangoResponse:
        """
        Update job status
        """
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['put'])
    def update_sequences(
        self, request: td.Request, *args, **kwargs
    ) -> td.DjangoResponse:
        """
        Update job sequences
        """
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['put'])
    def reschedule(
        self, request: td.Request, *args, **kwargs
    ) -> td.DjangoResponse:
        """
        Reschedule job
        """
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['put'])
    def update_instructions(
        self, request: td.Request, *args, **kwargs
    ) -> td.DjangoResponse:
        """
        Update job instructions
        """
        return super().update(request, *args, **kwargs)


class JobAttachmentViewset(
    BaseMESView,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    lookup_url_kwarg = 'job_attachment_id'
    serializer_class = JobAttachmentCreateSerializer

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_service().id
        return context

    def get_queryset(self) -> t.Sequence[td.JobAttachment]:
        partner = self.get_partner()
        return JobAttachment.objects.filter(job__workstation__partner=partner)

    def get_object(self) -> td.JobAttachment:
        return viewsets.GenericViewSet.get_object(self)
