from apps.b3_api.services.workflows.serializers.\
    workflow_template_list_retrieve_serializer import \
    WorkflowTemplateListRetrieveSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_template_status_create_update_serializer import \
    WorkflowTemplateStatusCreateUpdateSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_template_status_list_retrieve_serializer import \
    WorkflowTemplateStatusListRetrieveSerializer
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory,
)
from apps.b3_api.services.workflows.serializers.\
    workflow_template_create_update_serializer import \
    WorkflowTemplateCreateUpdateSerializer


class WorkflowTemplateSerializersTests(TestCase):
    def setUp(self):
        super().setUp()

        self.workflow_template = WorkflowTemplateWithStatusesFactory()

    def test_create_update_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'description': 'string',
            'statuses': [{'id': 0, 'name': 'string', 'workstationId': 0}],
        }
        serializer_populated = WorkflowTemplateCreateUpdateSerializer(
            self.workflow_template
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_list_retrieve_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'description': 'string',
            'statuses': [
                {
                    'id': 0,
                    'name': 'string',
                    'nextId': 0,
                    'workstation': {'id': 0, 'name': 'string'},
                }
            ],
        }
        serializer_populated = WorkflowTemplateListRetrieveSerializer(
            self.workflow_template
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_status_create_update_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'workstationId': 0,
        }
        serializer_populated = WorkflowTemplateStatusCreateUpdateSerializer(
            self.workflow_template.statuses.first()
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_status_list_retrieve_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'nextId': 0,
            'workstation': {'id': 0, 'name': 'string'},
        }
        serializer_populated = WorkflowTemplateStatusListRetrieveSerializer(
            self.workflow_template.statuses.first()
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
