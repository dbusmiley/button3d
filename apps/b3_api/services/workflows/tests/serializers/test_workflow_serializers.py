from apps.b3_api.services.workflows.serializers.\
    workflow_list_retrieve_serializer import WorkflowListRetrieveSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_status_list_retrieve_serializer import \
    WorkflowStatusListRetrieveSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_status_update_serializer import WorkflowStatusUpdateSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_update_serializer import WorkflowUpdateSerializer
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_api.services.workflows.serializers.\
    workflow_create_serializer import WorkflowCreateSerializer
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory


class WorkflowSerializersTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        self.quantity = 5

        self.service = PartnerFactory()
        self.user.partners.add(self.service)

        self.order = OrderFactory(partner=self.service)
        self.part = self.order.lines.first()
        self.part.quantity = self.quantity
        self.part.save()
        self.workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)
        self.create_serializer_data = {
            'partId': self.part.id,
            'workflowTemplateId': self.workflow_template.id,
        }
        serializer_populated = WorkflowCreateSerializer(
            data=self.create_serializer_data)
        serializer_populated.is_valid()
        self.workflow = serializer_populated.save()

    def test_workflow_create_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'description': 'string',
        }
        serializer_populated = WorkflowCreateSerializer(
            data=self.create_serializer_data)
        serializer_populated.is_valid()
        serializer_populated.save()
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_workflow_create_without_existing_workflow(self):
        workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)

        order = OrderFactory(partner=self.service)
        part = order.lines.first()
        part.quantity = self.quantity
        part.save()
        data = {
            'part_id': part.id,
            'workflow_template_id': workflow_template.id,
        }
        serializer = WorkflowCreateSerializer(data=data)
        serializer.is_valid()
        instance = serializer.save()

        self.assertEqual(self.quantity, instance.part.sequences.all().count())

    def test_workflow_create_with_existing_workflow(self):
        part = self.workflow.part
        data = {
            'part_id': part.id,
            'workflow_template_id': self.workflow_template.id,
        }
        serializer = WorkflowCreateSerializer(data=data)
        serializer.is_valid()
        instance = serializer.save()

        self.assertEqual(self.quantity, instance.part.sequences.all().count())

    def test_list_retrieve_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'description': 'string',
            'statuses': [
                {
                    'id': 0,
                    'name': 'string',
                    'nextId': 0,
                    'workstation': {'id': 0, 'name': 'string'},
                }
            ],
        }
        serializer_populated = WorkflowListRetrieveSerializer(self.workflow)
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_workflow_status_list_retrieve_structure(self):
        status = self.part.workflow.statuses.first()
        serializer_populated = WorkflowStatusListRetrieveSerializer(
            instance=status
        )
        structure_expected = {
            'id': 0,
            'name': 'string',
            'nextId': 0,
            'workstation': {'id': 0, 'name': 'string'},
        }
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_workflow_status_update_structure(self):
        status = self.part.workflow.statuses.first()
        serializer_populated = WorkflowStatusUpdateSerializer(
            instance=status
        )
        structure_expected = {
            'id': 0,
            'name': 'string',
            'workstationId': 0,
        }
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_workflow_update_structure(self):
        serializer_populated = WorkflowUpdateSerializer(
            instance=self.part.workflow
        )
        structure_expected = {
            'id': 0,
            'name': 'string',
            'description': 'string',
            'statuses': [{'id': 0, 'name': 'string', 'workstationId': 0}],
        }
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
