import json


def assert_statuses_order(test_client, response):
    statuses = json.loads(response.content).get('statuses')
    previous_status = statuses.pop(0)
    for current_status in statuses:
        test_client.assertEqual(
            previous_status.get('nextId'), current_status.get('id'))
        previous_status = current_status
