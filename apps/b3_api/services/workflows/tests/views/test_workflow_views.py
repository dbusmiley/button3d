import json

from django.urls import reverse

from apps.b3_api.services.workflows.serializers.\
    workflow_update_serializer import WorkflowUpdateSerializer
from apps.b3_api.services.workflows.tests.utils import assert_statuses_order
from apps.b3_mes.models import Workflow
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory,
    WorkstationFactory)
from apps.b3_tests.factories import PartnerFactory

from rest_framework import status


class WorkflowViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()

        self.service = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.service)

        # setUp for create
        self.order = OrderFactory(partner=self.service)
        self.part = self.order.lines.first()
        self.workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)

        create_payload = {
            'workflowTemplateId': self.workflow_template.id,
            'partId': self.part.id,
        }
        create_payload = json.dumps(create_payload)

        self.create_kwargs = {
            'data': create_payload,
            'content_type': 'application/json',
        }

        self.list_url = reverse(
            'api_urls:workflow-list', kwargs={'service_id': self.service.id}
        )

        # setUp for update
        workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)
        self.workflow_existing = workflow_template.\
            generate_workflow_and_sequences(self.part.id)
        workstation = WorkstationFactory(
            partner=self.workflow_existing.partner)

        statuses = [
            {
                'name': 'new status 1',
                'workstationId': workstation.id,
            },
            {
                'name': 'new status 2',
                'workstationId': workstation.id,
            },
        ]
        self.update_payload = WorkflowUpdateSerializer(
            self.workflow_existing).data
        self.update_payload['statuses'] = statuses
        self.update_payload['name'] = 'new name'
        self.update_payload['description'] = 'new description'

        self.detail_url = reverse(
            'api_urls:workflow-detail',
            kwargs={
                'service_id': self.service.id,
                'workflow_id': self.workflow_existing.id,
            },
        )

        update_payload_all_new_statuses_dumped = json.dumps(
            self.update_payload)
        self.update_kwargs_all_new_statuses = {
            'data': update_payload_all_new_statuses_dumped,
            'content_type': 'application/json',
        }

        old_status_dict = {
            'id': self.workflow_existing.statuses.first().id,
            'name': 'updated status',
            'workstationId': workstation.id,
        }
        statuses.append(old_status_dict)
        self.update_payload['statuses'] = statuses
        update_payload_dumped = json.dumps(self.update_payload)
        self.update_kwargs = {
            'data': update_payload_dumped,
            'content_type': 'application/json',
        }

    def test_create(self):
        response = self.client.post(self.list_url, **self.create_kwargs)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        workflow_dict = {
            'partner_id': self.workflow_template.partner.id,
            'name': self.workflow_template.name,
            'description': self.workflow_template.description,
        }
        self.assertTrue(Workflow.objects.get(**workflow_dict))

    def test_create_replace_existing_workflow(self):
        workflow_template = WorkflowTemplateWithStatusesFactory()
        workflow_template.generate_workflow_and_sequences(self.part.id)

        response = self.client.post(self.list_url, **self.create_kwargs)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_statuses_order(self):
        response = self.client.post(self.list_url, **self.create_kwargs)
        assert_statuses_order(self, response)

    def test_update(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        workflow_dict = {
            'id': self.workflow_existing.id,
            'name': self.update_payload['name'],
            'description': self.update_payload['description'],
        }
        self.assertTrue(Workflow.objects.get(**workflow_dict))

    def test_update_all_new_statuses(self):
        response = self.client.put(
            self.detail_url, **self.update_kwargs_all_new_statuses)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        workflow_dict = {
            'id': self.workflow_existing.id,
            'name': self.update_payload['name'],
            'description': self.update_payload['description'],
        }
        self.assertTrue(Workflow.objects.get(**workflow_dict))

    def test_update_statuses_order(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        assert_statuses_order(self, response)
