import json

from django.urls import reverse

from apps.b3_api.services.workflows.serializers.\
    workflow_template_create_update_serializer import \
    WorkflowTemplateCreateUpdateSerializer
from apps.b3_api.services.workflows.tests.utils import assert_statuses_order
from apps.b3_mes.models import WorkflowTemplate, WorkflowTemplateStatus
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateFactory,
    WorkstationFactory,
    WorkflowTemplateStatusFactory, WorkflowTemplateWithStatusesFactory)
from apps.b3_tests.factories.partner import PartnerFactory

from rest_framework import status
import factory


class WorkflowTemplateViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.service = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.service)

        # setUp for update
        self.workflow_template_existing = WorkflowTemplateFactory(
            partner=self.service)

        status_3 = WorkflowTemplateStatusFactory(
            workflow=self.workflow_template_existing,
            name='status 3', next=None
        )

        status_2 = WorkflowTemplateStatusFactory(
            workflow=self.workflow_template_existing,
            name='status 2', next=status_3
        )

        WorkflowTemplateStatusFactory(
            workflow=self.workflow_template_existing,
            name='status 1', next=status_2
        )

        workstation = WorkstationFactory(partner=self.service)

        status_3_dict = {
            'id': status_3.id,
            'name': status_3.name,
            'workstationId': workstation.id,
        }

        status_2_dict = {
            'id': status_2.id,
            'name': status_2.name,
            'workstationId': workstation.id,
        }

        status_4_dict = {'name': 'new status', 'workstationId': workstation.id}

        statuses = [status_2_dict, status_4_dict, status_3_dict]

        self.update_payload = WorkflowTemplateCreateUpdateSerializer(
            self.workflow_template_existing).data
        self.update_payload['statuses'] = statuses
        self.update_payload['name'] = 'new name'
        self.update_payload['description'] = 'new description'
        update_payload = json.dumps(self.update_payload)

        self.update_kwargs = {
            'data': update_payload,
            'content_type': 'application/json',
        }

        self.detail_url = reverse(
            'api_urls:workflow-template-detail',
            kwargs={
                'service_id': self.service.id,
                'workflow_id': self.workflow_template_existing.id},
        )

        # setUp for create/list
        self.workflow_template = factory.build(
            dict, FACTORY_CLASS=WorkflowTemplateFactory)
        self.workflow_template.pop('partner')

        workstation = WorkstationFactory(partner=self.service)
        create_payload = self.workflow_template
        create_payload['statuses'] = [
            {'name': 'Data prep', 'workstationId': workstation.id},
            {'name': 'Printing', 'workstationId': workstation.id},
            {'name': 'Finished', 'workstationId': None},
        ]
        create_payload = json.dumps(create_payload)
        self.create_kwargs = {
            'data': create_payload,
            'content_type': 'application/json',
        }

        self.list_url = reverse(
            'api_urls:workflow-template-list',
            kwargs={'service_id': self.service.pk},
        )
        WorkflowTemplateWithStatusesFactory.create_batch(
            5, partner=self.service)
        self.num_workflows = WorkflowTemplate.objects.filter(
            partner=self.service).count()

    def test_create(self):
        [s.delete() for s in WorkflowTemplateStatus.objects.all()]
        [workflow.delete() for workflow in WorkflowTemplate.objects.all()]

        response = self.client.post(self.list_url, **self.create_kwargs)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.workflow_template.pop('statuses')
        self.assertTrue(WorkflowTemplate.objects.get(**self.workflow_template))

    def test_create_statuses_order(self):
        response = self.client.post(self.list_url, **self.create_kwargs)
        assert_statuses_order(self, response)

    def test_update(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        workflow = WorkflowTemplate.objects.get(
            id=self.workflow_template_existing.id)
        self.assertEqual(self.update_payload['name'], workflow.name)
        self.assertEqual(
            self.update_payload['description'], workflow.description)

    def test_update_statuses(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        workflow = WorkflowTemplate.objects.get(
            id=self.workflow_template_existing.id)
        statuses_names_expected = [
            s['name'] for s in self.update_payload['statuses']]
        statuses_names_actual = workflow.statuses.all().values_list(
            'name', flat=True)
        self.assertCountEqual(statuses_names_expected, statuses_names_actual)

    def test_update_statuses_order(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        assert_statuses_order(self, response)

    def test_list(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.num_workflows)

    def test_delete(self):
        response = self.client.delete(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(
            WorkflowTemplate.objects.filter(
                id=self.workflow_template_existing.id).exists())
