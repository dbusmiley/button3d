from django.db import transaction

from apps.b3_api.services.views import BaseMESView
from apps.b3_api.services.workflows.serializers.\
    workflow_template_create_update_serializer import \
    WorkflowTemplateCreateUpdateSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_template_list_retrieve_serializer import \
    WorkflowTemplateListRetrieveSerializer
from apps.b3_mes.models.workflow_template import WorkflowTemplate
from apps.b3_api.utils.mixins import DifferentOutInViewsetSerializerMixin
import button3d.type_declarations as td

from rest_framework import viewsets, mixins
import typing as t


class WorkflowTemplateViewset(
    DifferentOutInViewsetSerializerMixin,
    BaseMESView,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    lookup_url_kwarg = 'workflow_id'

    serializer_class = WorkflowTemplateListRetrieveSerializer

    request_serializer_update = WorkflowTemplateCreateUpdateSerializer
    response_serializer_update = WorkflowTemplateListRetrieveSerializer

    request_serializer_create = WorkflowTemplateCreateUpdateSerializer
    response_serializer_create = WorkflowTemplateListRetrieveSerializer

    def get_object(self) -> td.OrderLine:
        return super(viewsets.GenericViewSet, self).get_object()

    def get_queryset(self) -> t.Sequence[td.WorkflowTemplate]:
        partner = self.get_partner()
        return WorkflowTemplate.objects.filter(partner=partner)

    @transaction.atomic
    def perform_destroy(self, instance: td.WorkflowTemplate) -> None:
        """
        Override `perform_destroy()` to prevent bulk delete of related statuses
        and make the transaction atomic
        """
        for status in instance.statuses.all():
            status.delete()
        instance.delete()

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_partner().pk
        return context
