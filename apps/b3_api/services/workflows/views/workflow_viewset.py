from apps.b3_api.services.views import BaseMESView
from apps.b3_api.services.workflows.serializers.\
    workflow_create_serializer import \
    WorkflowCreateSerializer
from apps.b3_api.services.workflows.serializers.\
    workflow_update_serializer import \
    WorkflowUpdateSerializer
from apps.b3_mes.models.workflow import Workflow
from apps.b3_api.utils.mixins import DifferentOutInViewsetSerializerMixin
from apps.b3_api.services.workflows.serializers.\
    workflow_list_retrieve_serializer import \
    WorkflowListRetrieveSerializer
import button3d.type_declarations as td

import typing as t

from rest_framework import mixins, viewsets


class WorkflowViewset(
    DifferentOutInViewsetSerializerMixin,
    BaseMESView,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
):
    lookup_url_kwarg = 'workflow_id'

    request_serializer_update = WorkflowUpdateSerializer
    response_serializer_update = WorkflowListRetrieveSerializer

    request_serializer_create = WorkflowCreateSerializer
    response_serializer_create = WorkflowListRetrieveSerializer

    def get_object(self) -> td.OrderLine:
        return super(viewsets.GenericViewSet, self).get_object()

    def get_queryset(self) -> t.Sequence[td.Workflow]:
        partner = self.get_partner()
        return Workflow.objects.filter(partner=partner)

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_partner().pk
        return context
