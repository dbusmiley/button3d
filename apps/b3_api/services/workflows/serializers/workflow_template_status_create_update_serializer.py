from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models import WorkflowTemplateStatus
from apps.b3_api.services.workstations.fields import (
    WorkstationPrimaryKeyRelatedField
)


class WorkflowTemplateStatusCreateUpdateSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    workstation_id = WorkstationPrimaryKeyRelatedField(
        source='workstation', allow_null=True)

    class Meta:
        model = WorkflowTemplateStatus
        fields = ('id', 'name', 'workstation_id')
