from apps.b3_api.services.workflows.serializers.\
    workflow_status_update_serializer import \
    WorkflowStatusUpdateSerializer
from apps.b3_api.services.workflows.serializers.\
    base_workflow_create_update_serializer import \
    BaseWorkflowCreateUpdateSerializer
from apps.b3_mes.models.workflow import Workflow
from apps.b3_mes.models.workflow_status import WorkflowStatus


class WorkflowUpdateSerializer(BaseWorkflowCreateUpdateSerializer):
    statuses = WorkflowStatusUpdateSerializer(many=True)

    workflow_model = Workflow
    workflow_status_model = WorkflowStatus

    class Meta:
        model = Workflow
        fields = BaseWorkflowCreateUpdateSerializer.Meta.fields
