from apps.b3_mes.models.workflow_template_status import WorkflowTemplateStatus
from apps.b3_api.services.workflows.serializers.\
    base_workflow_status_list_retrieve_serializer import \
    BaseWorkflowStatusListRetrieveSerializer


class WorkflowTemplateStatusListRetrieveSerializer(
    BaseWorkflowStatusListRetrieveSerializer
):
    class Meta:
        model = WorkflowTemplateStatus
        fields = BaseWorkflowStatusListRetrieveSerializer.Meta.fields
