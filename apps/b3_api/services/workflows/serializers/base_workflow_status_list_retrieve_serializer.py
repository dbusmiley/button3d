from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.workstations.serializers.\
    workstation_name_serializer import \
    WorkstationNameSerializer


class BaseWorkflowStatusListRetrieveSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    workstation = WorkstationNameSerializer()

    class Meta:
        fields = ('id', 'name', 'next_id', 'workstation')
        read_only_fields = ('id', 'name', 'next_id', 'workstation')
