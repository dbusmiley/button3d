from django.db import transaction

from apps.partner.models.partner import Partner
from apps.b3_api.utils.helpers import update_model_using_dict
import button3d.type_declarations as td

import typing as t
from rest_framework import serializers
from drf_payload_customizer.mixins.\
    payload_no_null_or_none_mixin import PayloadNoNullOrNoneMixin


class BaseWorkflowCreateUpdateSerializer(
    PayloadNoNullOrNoneMixin,
    serializers.ModelSerializer
):
    workflow_model = None
    workflow_status_model = None

    class Meta:
        fields = ('id', 'name', 'description', 'statuses')

    @transaction.atomic
    def create(self, validated_data: dict) -> td.AbstractWorkflow:
        """
        Override parent function to allow for creating nested serializer
        instances
        """
        partner = Partner.objects.get(id=self.context['service_id'])
        statuses = validated_data.pop('statuses')

        workflow = self.workflow_model(partner=partner, **validated_data)
        workflow.save()

        self.instance = workflow
        self._create_and_update_statuses(statuses)

        return workflow

    @transaction.atomic
    def update(
        self, instance: td.AbstractWorkflow, validated_data: dict
    ) -> td.AbstractWorkflow:
        """
        Override parent function to allow for updating nested serializer.

        This handles for the nested serializer:
            1. adding new instances
            2. removing existing instances
            3. updating existing instances
        """
        # Using `initial_data` alongside `validated_data` to be able to access
        # read_only field: `id` of statuses in order to distinguish between
        # deleted/new/remaining statuses
        statuses_validated = validated_data.pop('statuses')
        statuses_with_ids = self.initial_data.get('statuses')
        statuses = []
        for status_validated, status_with_id in zip(
            statuses_validated, statuses_with_ids
        ):
            status_validated['id'] = status_with_id.get('id', None)
            statuses.append(status_validated)
        statuses_remaining = self._get_statuses_remaining_ids(statuses)
        if statuses_remaining:
            # if at least one status remains, then we can safely delete the
            # other statuses since any Sequence set
            # to them would be redirected to the remaining status
            statuses_deleted = self._get_statuses_deleted(statuses)
            self._perform_delete_statuses(statuses_deleted)
            self._create_and_update_statuses(statuses)
        else:
            # since no statuses remain from the old statuses, must first create
            #  new statuses so any Sequence set to
            # deleted statuses would be redirected to a newly created status
            # -- must force eval query in next line, otherwise, new statuses
            # are create before the query evaluates
            statuses_deleted = list(self._get_statuses_deleted(statuses))
            first_status = self._create_and_update_statuses(statuses)
            self._update_sequences_related_to_statuses_deleted(
                statuses_deleted, first_status)
            self._perform_delete_statuses(statuses_deleted)

        update_model_using_dict(instance, validated_data)

        return instance

    @staticmethod
    def _get_statuses_remaining_ids(
        statuses: t.Sequence[dict]
    ) -> t.Sequence[int]:
        return [
            status.get('id') for status in statuses if status.get('id')
        ]

    def _get_statuses_deleted(
        self, statuses: t.Sequence[dict]
    ) -> t.Sequence[td.AbstractWorkflowStatus]:
        statuses_remaining = self._get_statuses_remaining_ids(statuses)
        return self.instance.statuses.exclude(
            id__in=statuses_remaining
        )

    @staticmethod
    def _update_sequences_related_to_statuses_deleted(
        statuses_deleted: t.Sequence[td.AbstractWorkflowStatus],
        first_status: td.AbstractWorkflowStatus
    ) -> None:
        for status in statuses_deleted:
            for seq in status.sequences.all():
                seq.status = first_status
                seq.save()

    @staticmethod
    def _perform_delete_statuses(
        statuses: t.Sequence[td.WorkflowTemplateStatus]
    ) -> None:
        [s.delete() for s in statuses]

    def _create_and_update_statuses(
        self, statuses: t.Sequence[dict]
    ) -> td.AbstractWorkflowStatus:
        """
        Creates and updates statuses for `self.instance`

        1. Nullify all `.next` for `workflow` statuses
        -- This is to prevent unique constraint errors on `.next`
            since it is a `OneToOneField` and in the case of an update
            to statuses where one statuses is deleted and another new one
            replaces it
            e.g.
            Before: status_1 -> status_2 -> status_3
            After:  status_1 -> status_A -> status_3

            Unique constraint validation fails if we try to create
            `status_A` before nullifying `.next` of `status_2`
            since both would have `status_3` as their value for `.next`

        2. Reverse statuses to create/update them from end to start and thus
            always have the value for `.next` while iterating over the statuses

        3. Create/update statuses
        :param statuses: list of dicts of statuses
        :return first status:
        """
        for status in self.instance.statuses.all():
            status.next = None
            status.save()

        statuses.reverse()

        next_id = None

        for status_dict in statuses:
            id = status_dict.pop('id', None)
            status_dict.pop('next', None)
            if next_id:
                next = self.workflow_status_model.objects.get(pk=next_id)
            else:
                next = None
            status_dict.update({'next': next, 'workflow_id': self.instance.id})

            if id:
                status = self.workflow_status_model.objects.get(id=id)
                update_model_using_dict(status, status_dict)
            else:
                status = self.workflow_status_model(**status_dict)

            status.save()
            next_id = status.id
        return status
