from collections import OrderedDict
from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models import WorkflowTemplate
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td


class WorkflowCreateSerializer(
    PayloadConverterMixin, serializers.Serializer
):
    workflow_template_id = serializers.IntegerField()
    part_id = serializers.IntegerField()

    class Meta:
        to_representation_fields = ('id', 'name', 'description')

    @staticmethod
    def validate_workflow_template_id(value: int):
        """
        Asserts that workflow exists in db
        """
        try:
            WorkflowTemplate.objects.get(id=value)
        except WorkflowTemplate.DoesNotExist:
            raise serializers.ValidationError(
                'WorkflowTemplate does not exist'
            )
        return value

    @staticmethod
    def validate_part_id(value: int):
        """
        Asserts that part exists in db
        """
        try:
            OrderLine.objects.get(id=value)
        except OrderLine.DoesNotExist:
            raise serializers.ValidationError(
                'Line does not exist'
            )
        return value

    def create(self, validated_data: dict) -> td.Workflow:
        workflow_template_id = validated_data.get('workflow_template_id')
        workflow_template = WorkflowTemplate.objects.get(
            id=workflow_template_id
        )

        part_id = validated_data.get('part_id')
        part = OrderLine.objects.get(id=part_id)
        part.delete_old_sequences()
        part.delete_old_workflow()

        workflow = workflow_template.generate_workflow_and_sequences(
            part_id=part_id
        )

        return workflow

    def update(
        self, instance: td.Workflow, validated_data: dict
    ) -> None:
        raise NotImplementedError('This serializer is not used for updating')

    def to_representation(self, instance: td.Workflow) -> OrderedDict:
        """
        Object instance -> Dict of primitive datatypes.
        """
        ret = OrderedDict()
        for field_name in self.Meta.to_representation_fields:
            ret[field_name] = getattr(instance, field_name)
        return ret
