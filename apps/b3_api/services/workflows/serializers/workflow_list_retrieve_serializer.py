from apps.b3_mes.models.workflow import Workflow
from apps.b3_api.services.workflows.serializers.\
    workflow_status_list_retrieve_serializer import \
    WorkflowStatusListRetrieveSerializer
import button3d.type_declarations as td

from rest_framework import serializers


class WorkflowListRetrieveSerializer(serializers.ModelSerializer):
    statuses = serializers.SerializerMethodField()

    @staticmethod
    def get_statuses(obj: td.Workflow) -> dict:
        serializer = WorkflowStatusListRetrieveSerializer(
            obj.get_statuses_ordered(), many=True)
        return serializer.data

    class Meta:
        model = Workflow
        fields = ('id', 'name', 'description', 'statuses')
