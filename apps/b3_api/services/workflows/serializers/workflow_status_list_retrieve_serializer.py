from apps.b3_mes.models.workflow_status import WorkflowStatus
from apps.b3_api.services.workflows.serializers.\
    base_workflow_status_list_retrieve_serializer import \
    BaseWorkflowStatusListRetrieveSerializer


class WorkflowStatusListRetrieveSerializer(
    BaseWorkflowStatusListRetrieveSerializer
):
    class Meta:
        model = WorkflowStatus
        fields = BaseWorkflowStatusListRetrieveSerializer.Meta.fields
