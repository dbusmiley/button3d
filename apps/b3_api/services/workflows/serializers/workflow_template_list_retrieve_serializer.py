from apps.b3_mes.models.workflow_template import WorkflowTemplate
from apps.b3_api.services.workflows.serializers.\
    workflow_template_status_list_retrieve_serializer import \
    WorkflowTemplateStatusListRetrieveSerializer
import button3d.type_declarations as td

from rest_framework import serializers


class WorkflowTemplateListRetrieveSerializer(serializers.ModelSerializer):
    statuses = serializers.SerializerMethodField()

    @staticmethod
    def get_statuses(obj: td.WorkflowTemplate) -> dict:
        serializer = WorkflowTemplateStatusListRetrieveSerializer(
            obj.get_statuses_ordered(), many=True)
        return serializer.data

    class Meta:
        model = WorkflowTemplate
        fields = ('id', 'name', 'description', 'statuses')
