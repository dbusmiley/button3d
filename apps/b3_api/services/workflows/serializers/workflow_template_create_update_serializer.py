from apps.b3_api.services.workflows.serializers.\
    base_workflow_create_update_serializer import \
    BaseWorkflowCreateUpdateSerializer
from apps.b3_mes.models.workflow_template import WorkflowTemplate
from apps.b3_mes.models.workflow_template_status import WorkflowTemplateStatus
from apps.b3_api.services.workflows.serializers.\
    workflow_template_status_create_update_serializer import \
    WorkflowTemplateStatusCreateUpdateSerializer


class WorkflowTemplateCreateUpdateSerializer(
    BaseWorkflowCreateUpdateSerializer
):
    statuses = WorkflowTemplateStatusCreateUpdateSerializer(many=True)

    workflow_model = WorkflowTemplate
    workflow_status_model = WorkflowTemplateStatus

    class Meta:
        model = WorkflowTemplate
        fields = BaseWorkflowCreateUpdateSerializer.Meta.fields
