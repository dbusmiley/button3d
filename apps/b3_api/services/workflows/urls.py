from apps.b3_api.services.workflows.views.workflow_template_viewset import (
    WorkflowTemplateViewset
)
from apps.b3_api.services.workflows.views.workflow_viewset import (
    WorkflowViewset
)

from rest_framework import routers

router = routers.SimpleRouter()

router.register(
    r'template', WorkflowTemplateViewset, base_name='workflow-template'
)
router.register(r'', WorkflowViewset, base_name='workflow')


urlpatterns = router.urls
