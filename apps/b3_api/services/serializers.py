from rest_framework import serializers

from apps.partner.models import Partner
from drf_payload_customizer.mixins import PayloadConverterMixin


class ServiceSerializer(PayloadConverterMixin, serializers.ModelSerializer):
    class Meta:
        model = Partner
        field_mappings = {
            'price_currency': 'currency',
            'mes_activated': 'mes_enabled',
        }
        fields = (
            'id',
            'name',
            'logo',
            'price_currency',
            'enabled',
            'mes_activated',
        )
        read_only_fields = ('enabled', 'mes_activated')
