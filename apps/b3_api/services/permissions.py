from rest_framework import permissions

from apps.b3_api.errors.api_responses import error_object_access_denied_user


class IsServiceRelated(permissions.BasePermission):
    message = 'This object is not related to your service'

    def has_permission(self, request, view):
        requesting_partner = view.kwargs['service_id']
        if not request.user.partners.filter(id=requesting_partner).exists():
            # Use our custom message instead
            self.message = error_object_access_denied_user(
                object_name='SERVICE_OBJECT',
                object_id=requesting_partner,
                user_id=request.user.id
            )
            return False

        return True
