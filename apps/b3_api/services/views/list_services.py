from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.services.serializers import ServiceSerializer


class ListServices(ListAPIView):
    permission_classes = (
        IsAuthenticated,
        IsServiceUser,
    )
    serializer_class = ServiceSerializer

    def get_queryset(self):
        return self.request.user.partners.all()
