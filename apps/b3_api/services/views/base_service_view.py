from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from apps.b3_api.permissions import IsServiceUser, IsMESUser
from apps.b3_api.services.permissions import IsServiceRelated
from .mixins import ServiceMixin


class BaseServiceView(APIView, ServiceMixin):
    """
    Base class that can fetch the right Service for
    /services/<service_id>/ functions
    """
    permission_classes = (IsAuthenticated, IsServiceUser, IsServiceRelated)
    lookup_url_kwarg = 'service_id'

    def get_object(self):
        return self.get_service()

    def get_partner(self):
        return self.get_service()


class BaseMESView(BaseServiceView):
    permission_classes = BaseServiceView.permission_classes + (IsMESUser, )
