from apps.partner.models import Partner
from django.http import Http404

from apps.b3_api.errors.api_responses import error_object_not_found


class ServiceMixin:
    def get_service(self):
        try:
            return Partner.objects.get(id=self.kwargs['service_id'])
        except Partner.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='PARTNER', object_id=self.kwargs['service_id']
                )
            )
