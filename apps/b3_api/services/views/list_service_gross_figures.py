from datetime import timedelta, datetime, time

import dateutil.parser
from django.utils import timezone
from rest_framework.response import Response

from apps.b3_api.services.views.base_service_view import BaseServiceView
from apps.b3_api.utils.currency_util import get_currency_or_default, \
    get_total_exclusive_tax
from apps.b3_order.models import Order


class ListServiceGrossFigures(BaseServiceView):
    def get(self, request, *args, **kwargs):
        """
        Fetch total value including tax for orders connected with a given
        partner for all days of past week. This logic is inspired by This
        logic is copied from https://stackoverflow.com/a/18696566 .
        :param request: Request incoming of the endpoint.
        :return: A list of gross volume of the partner over the last seven
        days.
        """

        partner = self.get_object()
        try:
            end_date = dateutil.parser.parse(
                self.request.query_params.get('endDate', None)
            )
        except Exception:
            end_date = timezone.now()

        try:
            start_date = dateutil.parser.parse(
                self.request.query_params.get('startDate', None)
            )
        except Exception:
            start_date = end_date - timedelta(days=30)

        amount_days = (end_date - start_date).days

        currency = get_currency_or_default(self.request.query_params)

        entries = []
        current_timezone_info = timezone.get_current_timezone()
        for x in reversed(list(range(0, amount_days + 1))):
            week_date = (end_date - timedelta(days=x)).date()
            min_datetime = datetime.combine(week_date, time.min).replace(
                tzinfo=current_timezone_info
            )
            max_datetime = datetime.combine(week_date, time.max).replace(
                tzinfo=current_timezone_info
            )

            currency_aggregates_qs = \
                Order.objects.filter(
                    partner=partner,
                    datetime_placed__range=(min_datetime, max_datetime)
                ).values_list('currency', 'total_value')

            total_exclusive_tax = get_total_exclusive_tax(
                currency=currency,
                currency_aggregates_qs=currency_aggregates_qs)

            entries.append(
                {
                    'day': week_date,
                    'value': total_exclusive_tax
                }
            )

        return Response(entries)
