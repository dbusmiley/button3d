from decimal import Decimal as D

from rest_framework.exceptions import ValidationError

from apps.b3_api.services.offers.validations import validate_price_formula
from apps.b3_api.utils.generate_model_field import get_model_field
from apps.b3_api.utils.serializers import NestingSerializer
from apps.b3_core.utils import eval_price_support_custom, \
    eval_price_orientation_custom, PriceEvaluationError
from apps.partner.models import StockRecord


class ServiceOfferPriceDetailSerializer(NestingSerializer):
    priceFormula = get_model_field(StockRecord, 'price_custom')

    def validate_priceFormula(self, formula):
        validated, exception = validate_price_formula(formula)
        if not validated:
            raise ValidationError(exception)

        return formula

    def validate(self, attrs):
        errors = {}
        price_support = attrs['price_support_custom']
        price_orientation = attrs['price_orientation_custom']
        support_enabled = attrs['support_enabled']
        support_offset = attrs['support_offset']
        support_angle = attrs['support_angle']

        if support_enabled:
            # validate support structure parameters
            if support_angle > 90 or support_angle < 0:
                errors['support_angle'] = \
                    'Angle must be between 0 and 90'
            if support_offset < 0:
                errors['support_offset'] = 'Offset must be positive'
            # validate price_support_custom
            errors.update(self._validate_price_support(price_support))
            # validate price_orientation_custom
            errors.update(self._validate_price_orientation(price_orientation))

        if errors:
            raise ValidationError(errors)
        return attrs

    @staticmethod
    def _validate_price_support(formula):
        err = None
        if not formula:
            return {'price_support_custom': 'This field is required.'}

        try:
            result = eval_price_support_custom(
                price_support_custom=formula,
                quantity=1,
                support_volume=D('2328.60'),
                support_area=D('1767.29'),
                width=D('75'),
                height=D('75'),
                depth=D('75')
            )
            if result <= 0:
                err = (
                    f'Test result (volume=2328.60; area=1767.29; '
                    f'width=75; height=75; depth=75): {result}'
                )
        except PriceEvaluationError as e:
            err = str(e)

        return {'price_support_custom': err} if err else {}

    @staticmethod
    def _validate_price_orientation(formula):
        err = None
        if not formula:
            return {}

        try:
            result = eval_price_orientation_custom(
                price_orientation_custom=formula,
                model_costs={"h-": 352.36},
                support_costs={"h-": 173.73},
            )
            if result <= 0:
                err = (
                    f'Test result: (model_cost1=173.73; '
                    f'support_cost1=352.36): {result}'
                )
        except PriceEvaluationError as e:
            err = str(e)

        return {'price_orientation_custom': err} if err else {}

    def to_representation(self, instance):
        serializer_out = super(ServiceOfferPriceDetailSerializer,
                               self).to_representation(instance)
        # 'support_enabled' is represented inplicitly by the presence of the
        # 'supportStructure' object
        if not instance.support_enabled:
            serializer_out['supportStructure'] = None
        del serializer_out['support_enabled']

        return serializer_out

    def to_internal_value(self, data):
        if 'supportStructure' not in data:
            raise ValidationError(
                {'supportStructure', 'This field is required.'}
            )

        # If the 'supportStructure' object is not present, that means that
        # 'support_enabled' must be set to false (and default values are stored
        # in the database)
        if data['supportStructure'] is None:
            data['supportStructure'] = {
                'angle': 45.0,
                'offset': 0.0,
                'formula': '',
                'orientationFormula': ''
            }
            data['support_enabled'] = False
        else:
            data['support_enabled'] = True

        return super(ServiceOfferPriceDetailSerializer,
                     self).to_internal_value(data)

    class Meta:
        model = StockRecord
        fields = (
            'priceFormula', 'support_angle', 'support_offset',
            'price_support_custom', 'price_orientation_custom',
            'support_enabled'
        )
        nesting_options = [
            {
                'name':
                'supportStructure',
                'translations': [
                    ('support_angle', 'angle'), ('support_offset', 'offset'),
                    ('price_support_custom', 'formula'),
                    ('price_orientation_custom', 'orientationFormula')
                ]
            }
        ]
