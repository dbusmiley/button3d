from apps.b3_api.services.discounts.serializers import DiscountSerializer
from apps.partner.models import StockRecordRule


class OfferDiscountSerializer(DiscountSerializer):
    class Meta(DiscountSerializer.Meta):
        model = StockRecordRule
