from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.b3_api.services.offers.utils import get_datasheet_data
from apps.b3_api.services.offers.validations import validate_min_max_bounds, \
    validate_production_days
from apps.b3_api.utils.generate_model_field import get_model_field
from apps.b3_api.utils.serializers import NestingSerializer
from apps.partner.models import StockRecord


class DetailServiceOfferSerializer(NestingSerializer):
    multiplePostProcessings = get_model_field(
        StockRecord, 'combinable_post_processings'
    )
    datasheet = serializers.SerializerMethodField()
    unit = serializers.SerializerMethodField()

    def get_unit(self, obj):
        return 'mm'

    def get_datasheet(self, obj):
        return get_datasheet_data(request=self.context.get('request'), obj=obj)

    def validate(self, attrs):
        _, production_errors = self.validate_production_days(
            attrs['min_production_days'], attrs['max_production_days']
        )

        _, bounds_errors = self.validate_bounds(
            attrs['min_x_mm'],
            attrs['max_x_mm'],
            attrs['min_y_mm'],
            attrs['max_y_mm'],
            attrs['min_z_mm'],
            attrs['max_z_mm'],
        )

        errors = {**production_errors, **bounds_errors}
        if errors:
            raise ValidationError(errors)
        return attrs

    @staticmethod
    def validate_production_days(production_days_min, production_days_max):
        errors = validate_production_days(
            production_days_min,
            'min_production_days',
            production_days_max,
            'max_production_days'
        )
        return bool(errors), errors

    @staticmethod
    def validate_bounds(min_x, max_x, min_y, max_y, min_z, max_z):
        x_errors = validate_min_max_bounds(
            min_x, max_x, 'max_x_mm'
        )
        y_errors = validate_min_max_bounds(
            min_y, max_y, 'max_y_mm'
        )
        z_errors = validate_min_max_bounds(
            min_z, max_z, 'max_z_mm'
        )
        errors = {**x_errors, **y_errors, **z_errors}
        return bool(errors), errors

    class Meta:
        model = StockRecord
        fields = (
            'datasheet',
            'min_production_days',
            'max_production_days',
            'min_x_mm',
            'max_x_mm',
            'min_y_mm',
            'max_y_mm',
            'min_z_mm',
            'max_z_mm',
            'unit',
            'multiplePostProcessings'
        )
        nesting_options = [
            {
                'name':
                    'production',
                'translations': [
                    ('min_production_days', 'minDays'),
                    ('max_production_days', 'maxDays')
                ]
            }, {
                'name':
                    'properties',
                'translations': [
                    {
                        'name': 'x',
                        'translations': [
                            ('min_x_mm', 'minimum'),
                            ('max_x_mm', 'maximum')
                        ]
                    },
                    {
                        'name': 'y',
                        'translations': [
                            ('min_y_mm', 'minimum'),
                            ('max_y_mm', 'maximum')
                        ]
                    },
                    {
                        'name': 'z',
                        'translations': [
                            ('min_z_mm', 'minimum'),
                            ('max_z_mm', 'maximum')
                        ]
                    },
                    ('unit', 'unit')
                ]
            }
        ]
