from rest_framework import serializers

from apps.b3_api.services.offers.utils import \
    fetch_value_from_manual_pricing_rules, create_response_from_value
from apps.partner.models import StockRecord


class ServiceOfferManualPricingSerializer(serializers.ModelSerializer):
    """
    """
    itemTotalPrice = serializers.SerializerMethodField()
    totalPrice = serializers.SerializerMethodField()
    totalQuantity = serializers.SerializerMethodField()

    def get_itemTotalPrice(self, obj):
        return create_response_from_value(
            fetch_value_from_manual_pricing_rules(
                rules=obj.get_manual_pricing_rules(),
                condition='PriceGreaterCondition',
                property='max_price'
            )
        )

    def get_totalQuantity(self, obj):
        return create_response_from_value(
            fetch_value_from_manual_pricing_rules(
                rules=obj.get_manual_pricing_rules(),
                condition='QuantityGreaterCondition',
                property='max_quantity'
            )
        )

    def get_totalPrice(self, obj):
        return create_response_from_value(
            fetch_value_from_manual_pricing_rules(
                rules=obj.get_manual_pricing_rules(),
                condition='PriceQuantityProductGreaterCondition',
                property='max_price_quantity_product'
            )
        )

    class Meta:
        model = StockRecord
        fields = (
            'totalPrice',
            'totalQuantity',
            'itemTotalPrice',
        )
