import logging

from rest_framework import serializers

from apps.b3_api.services.offers.post_processings.serializers import \
    PostProcessingSerializer
from apps.b3_api.services.offers.utils import get_datasheet_data, \
    get_production_days
from drf_payload_customizer.mixins import PayloadConverterMixin
from apps.partner.models import StockRecord

logger = logging.getLogger(__name__)


class ListServiceOffersSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    material_name = serializers.SerializerMethodField()
    production = serializers.SerializerMethodField()
    datasheet = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    material_id = serializers.CharField(source='product.id')
    post_processings = PostProcessingSerializer(many=True)
    post_processing_titles = serializers.SerializerMethodField()
    multiple_post_processings = serializers.BooleanField(
        source='combinable_post_processings'
    )
    technology_id = serializers.SerializerMethodField(read_only=True)

    def get_status(self, obj):
        if obj.edit_status == StockRecord.PUBLISHED:
            return 'Published'
        elif obj.edit_status == StockRecord.DRAFT:
            return 'Draft'
        raise ValueError(f'invalid edit_status: {obj.edit_status}')

    def get_datasheet(self, obj):
        return get_datasheet_data(request=self.context.get('request'), obj=obj)

    def get_production(self, obj):
        return get_production_days(obj=obj)

    def get_material_name(self, obj):
        return obj.product.full_product_title

    @staticmethod
    def get_technology_id(obj):
        return obj.product.technology_id

    def get_post_processing_titles(self, obj):
        return [
            post_processing.title
            for post_processing in obj.post_processings.all()
        ]

    class Meta:
        model = StockRecord
        fields = (
            'id',
            'material_name',
            'material_id',
            'production',
            'datasheet',
            'status',
            'post_processings',
            'post_processing_titles',
            'multiple_post_processings',
            'technology_id',
        )
