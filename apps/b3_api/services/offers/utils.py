import os
import json

from rest_framework.fields import FileField
from rest_framework.generics import DestroyAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.parsers import MultiPartParser, FormParser

from apps.partner.models import condition_classes
from rest_framework import status, serializers
from rest_framework.response import Response
from apps.partner.models import Partner
from apps.partner.models import StockRecord
from django.core.exceptions import ObjectDoesNotExist


class DataSheetSerializer(serializers.Serializer):
    file = FileField()


class DataSheetViewMixin(UpdateModelMixin, DestroyAPIView):
    """
    This mixin can be used with a base class that implements
    `get_object()`. The route of this view needs to have a parameter
    named `language` that is passed to the view.
    Depending on the language, the uploaded data sheet is saved
    to the instance either in `datasheed_pdf_de` or `datasheed_pdf_en`
    """
    parser_classes = (MultiPartParser, FormParser, )
    serializer_class = DataSheetSerializer

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        self.update_datasheet(serializer.validated_data['file'])
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update_datasheet(self, file_content):
        instance = self.get_object()
        language = self.kwargs['language']
        if language == "english":
            instance.datasheet_pdf_en = file_content
        elif language == "german":
            instance.datasheet_pdf_de = file_content
        else:
            raise NotImplementedError(
                f'{language} is not a supported language'
            )
        instance.save()

    def perform_destroy(self, instance):
        instance = self.get_object()
        language = self.kwargs['language']
        if language == "english":
            instance.datasheet_pdf_en = None
        elif language == "german":
            instance.datasheet_pdf_de = None
        instance.save()


def get_datasheet_data(request, obj):
    return {
        "english": {
            "name":
            os.path.basename(obj.datasheet_pdf_en.name)
            if obj.datasheet_pdf_en else None,
            "url":
            request.build_absolute_uri(obj.datasheet_pdf_en.url)
            if obj.datasheet_pdf_en else None
        },
        "german": {
            "name":
            os.path.basename(obj.datasheet_pdf_de.name)
            if obj.datasheet_pdf_de else None,
            "url":
            request.build_absolute_uri(obj.datasheet_pdf_de.url)
            if obj.datasheet_pdf_de else None
        },
    }


def get_production_days(obj):
    return {
        "minimumDays": obj.min_production_days,
        "maximumDays": obj.max_production_days
    }


def fetch_value_from_manual_pricing_rules(rules, condition, property):
    for rule in rules:
        conditions = json.loads(rule.conditions)
        for cls, args in conditions:
            if condition_classes[condition] == condition_classes[cls]:
                if property in args:
                    return args[property]


def create_response_from_value(value):
    if not value and value != 0:
        return {'enabled': False, 'value': None}
    else:
        return {'enabled': True, 'value': value}


def create_no_entity_found_error(key, label):
    code = f'OFFER_DISCOUNTS_NO_{key}_FOUND'
    message = f'No {label} could be found for the ids provided in the ' \
        f'URL parameters.'
    return Response(
        {
            'code': code,
            'message': message,
            'moreInfo': None,
        },
        status=status.HTTP_422_UNPROCESSABLE_ENTITY
    )


def get_stock_record_by_url_parameters(arguments):
    """
    :param data: request data
    :return: (validated Boolean, validated_data stock_record_rule,
    error_message String
    """
    validated_data = None
    try:
        partner = Partner.objects.get(id=arguments['service_id'])
    except ObjectDoesNotExist:
        return False, validated_data, create_no_entity_found_error(
            'PARTNER', 'partner'
        )

    try:
        stock_record = StockRecord.objects.get(
            id=arguments['offer_id'],
            partner=partner,
        )
    except ObjectDoesNotExist:
        return False, validated_data, create_no_entity_found_error(
            'OFFER', 'offer'
        )

    try:
        validated_data = stock_record.rules.get(
            conclusion='discount', id=arguments['discount_id']
        )
    except ObjectDoesNotExist:
        return False, validated_data, create_no_entity_found_error(
            'OFFER_RULE', 'offer rule'
        )

    return True, validated_data, None


def fetch_production_days(production_data):
    try:
        minimum_days = int(production_data['minimumDays'])
        maximum_days = int(production_data['maximumDays'])
        if not minimum_days <= maximum_days:
            return False
        else:
            return minimum_days, maximum_days
    except TypeError:
        return False
