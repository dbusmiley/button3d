from django.conf.urls import url, include

from apps.b3_api.services.offers.views import ListServiceOffers, \
    UpdateFetchServiceOfferStatus, ServiceOfferDetail, \
    ServiceOfferPriceDetail, UpdateFetchServiceOfferManualPricing, \
    OfferDataSheetUpdate, TestOfferCustomPricing, \
    ListCreateServiceOfferDiscounts, UpdateDeleteServiceDiscount

urlpatterns = [
    url(r'^$', ListServiceOffers.as_view()),
    url(r'^(?P<offer_id>[0-9]+)/$', ServiceOfferDetail.as_view()),
    url(
        r'^(?P<offer_id>[0-9]+)/test-pricing-formula/$',
        TestOfferCustomPricing.as_view()
    ),
    url(
        r'^(?P<offer_id>[0-9]+)/status/$',
        UpdateFetchServiceOfferStatus.as_view()
    ),
    url(
        r'^(?P<offer_id>[0-9]+)/datasheet/'
        r'(?P<language>german|english)/$',
        OfferDataSheetUpdate.as_view()
    ),
    url(r'^(?P<offer_id>[0-9]+)/price/$', ServiceOfferPriceDetail.as_view()),
    url(
        r'^(?P<offer_id>[0-9]+)/discounts/$',
        ListCreateServiceOfferDiscounts.as_view()
    ),
    url(
        r'^(?P<offer_id>[0-9]+)/discounts/(?P<discount_id>[0-9]+)/$',
        UpdateDeleteServiceDiscount.as_view()
    ),
    url(
        r'^(?P<offer_id>[0-9]+)/manual-price/$',
        UpdateFetchServiceOfferManualPricing.as_view()
    ),
    url(
        r'^(?P<offer_id>[0-9]+)/post-processings/',
        include('apps.b3_api.services.offers.post_processings.urls')
    ),

]
