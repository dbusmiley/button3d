from django.utils import timezone
from rest_framework import status
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.offers.serializers \
    import DetailServiceOfferSerializer
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView


class ServiceOfferDetail(BaseStockRecordView, RetrieveUpdateDestroyAPIView):
    serializer_class = DetailServiceOfferSerializer

    @staticmethod
    def _handle_invalid_production_days(offer_id):
        return Response(
            error_object_update_failed(
                object_name='Offer',
                object_id=offer_id,
                message='Minimum production days should be less than the '
                'maximum production days',
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def perform_destroy(self, instance):
        instance.deleted_date = timezone.now()
        instance.save()
