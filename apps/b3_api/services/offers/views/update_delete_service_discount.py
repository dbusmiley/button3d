from django.http import Http404
from rest_framework.generics import RetrieveUpdateDestroyAPIView

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.offers.serializers import OfferDiscountSerializer
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView
from apps.partner.models import StockRecordRule


class UpdateDeleteServiceDiscount(
    BaseStockRecordView, RetrieveUpdateDestroyAPIView
):
    serializer_class = OfferDiscountSerializer

    def get_object(self):
        try:
            return StockRecordRule.objects.get(
                pk=self.kwargs['discount_id'],
                stockrecord__id=self.kwargs['offer_id'],
                stockrecord__partner__id=self.kwargs['service_id'],
                conclusion="discount"
            )
        except StockRecordRule.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='StockRecordRule',
                    object_id=self.kwargs['discount_id']
                )
            )
