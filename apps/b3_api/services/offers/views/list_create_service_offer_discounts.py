from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response

from apps.b3_api.services.offers.serializers import OfferDiscountSerializer
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView
from apps.partner.models import StockRecordRule


class ListCreateServiceOfferDiscounts(BaseStockRecordView, ListCreateAPIView):
    serializer_class = OfferDiscountSerializer

    def get_queryset(self):
        return StockRecordRule.objects.filter(
            stockrecord=self.get_object(), conclusion="discount"
        )

    def perform_create(self, serializer):
        serializer.save(stockrecord=self.get_object(), conclusion="discount")

    def delete(self, request, *args, **kwargs):
        stockrecord = self.get_object()
        stockrecord.clear_discount_rules()
        return Response(status=status.HTTP_204_NO_CONTENT)
