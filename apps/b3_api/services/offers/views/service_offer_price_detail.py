from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.services.offers.serializers \
    import ServiceOfferPriceDetailSerializer
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView


class ServiceOfferPriceDetail(BaseStockRecordView, RetrieveUpdateAPIView):
    serializer_class = ServiceOfferPriceDetailSerializer
