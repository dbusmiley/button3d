from apps.partner.models import StockRecord
from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.offers.validations import validate_production_days, \
    validate_min_max_bounds, validate_price_formula
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView
from apps.b3_api.utils.helpers import underscore_to_camelcase


class UpdateFetchServiceOfferStatus(BaseStockRecordView):
    def get(self, request, *args, **kwargs):
        offer_edit_status = self.get_object().edit_status
        return Response(
            {
                "published":
                True if offer_edit_status == StockRecord.PUBLISHED else False
            }
        )

    def post(self, request, *args, **kwargs):
        offer = self.get_object()

        if not request.data['published']:
            offer.edit_status = StockRecord.DRAFT
            offer.save()
            return Response(status=status.HTTP_202_ACCEPTED)

        errors = {}
        # make sure offer has enough attributes before you set it to published
        errors.update(
            validate_production_days(
                offer.min_production_days,
                underscore_to_camelcase('min_production_days'),
                offer.max_production_days,
                underscore_to_camelcase('max_production_days')
            )
        )
        # validate max/min bounds
        for dim in ['x', 'y', 'z']:
            min_dimension = f'min_{dim}_mm'
            max_dimension = f'max_{dim}_mm'
            errors.update(
                validate_min_max_bounds(
                    getattr(offer, min_dimension),
                    getattr(offer, max_dimension),
                    underscore_to_camelcase(max_dimension)
                )
            )

        # validate pricing formula
        validated, exception = validate_price_formula(offer.price_custom)
        if not validated:
            errors.update({'priceFormula': exception})

        if errors:
            return Response(
                error_object_update_failed(
                    object_name='Offer',
                    object_id=offer.id,
                    message='Required fields are set or contain errors',
                    exception=errors
                ),
                status=status.HTTP_400_BAD_REQUEST
            )

        offer.edit_status = StockRecord.PUBLISHED
        offer.save()

        return Response(status=status.HTTP_202_ACCEPTED)
