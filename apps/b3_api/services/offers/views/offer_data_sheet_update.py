from apps.b3_api.services.offers.utils import DataSheetViewMixin
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView


class OfferDataSheetUpdate(BaseStockRecordView, DataSheetViewMixin):
    pass
