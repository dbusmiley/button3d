from rest_framework import status
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.offers.serializers \
    import ServiceOfferManualPricingSerializer
from apps.b3_api.services.offers.validations \
    import validate_manual_pricing_conditions
from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView
from apps.partner.conditions import PriceGreaterCondition, \
    PriceQuantityProductGreaterCondition, QuantityGreaterCondition


class UpdateFetchServiceOfferManualPricing(
    BaseStockRecordView, RetrieveUpdateAPIView
):
    serializer_class = ServiceOfferManualPricingSerializer

    @staticmethod
    def _extract_data_from_payload(data):
        return data['itemTotalPrice'], data['totalPrice'], \
            data['totalQuantity']

    @staticmethod
    def _handle_negative_manual_pricing(request):
        return Response(
            error_object_update_failed(
                object_name='Manual Price',
                message='Totals / prices must be greater than zero.'
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def post(self, request, *args, **kwargs):
        stock_record = self.get_object()

        (item_total_price, total_price, total_quantity) = self. \
            _extract_data_from_payload(request.data)

        item_total_price_enabled = item_total_price['enabled']
        total_price_enabled = total_price['enabled']
        total_quantity_enabled = total_quantity['enabled']

        if not validate_manual_pricing_conditions(
            item_total_price, total_price, total_quantity
        ):
            return self._handle_negative_manual_pricing(request)

        stock_record.clear_manual_pricing_rules()

        if not any(
            [
                item_total_price_enabled, total_price_enabled,
                total_quantity_enabled
            ]
        ):
            return Response(status=status.HTTP_200_OK)

        if item_total_price and item_total_price_enabled:
            condition = PriceGreaterCondition(
                max_price=int(item_total_price['value'])
            )
            stock_record.add_manual_pricing_rule(condition)
        if total_price and total_price_enabled:
            condition = PriceQuantityProductGreaterCondition(
                max_price_quantity_product=int(total_price['value'])
            )
            stock_record.add_manual_pricing_rule(condition)
        if total_quantity and total_quantity_enabled:
            condition = QuantityGreaterCondition(
                max_quantity=int(total_quantity['value'])
            )
            stock_record.add_manual_pricing_rule(condition)

        return Response(status=status.HTTP_200_OK)
