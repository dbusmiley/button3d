from django.http import Http404
from apps.partner.models import StockRecord

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.views import BaseServiceView


class BaseStockRecordView(BaseServiceView):
    """
    Base class to handle common operations on service-offer edits
    """

    def get_object(self):
        try:
            return StockRecord.objects.get(
                partner__id=self.kwargs['service_id'],
                pk=self.kwargs['offer_id']
            )
        except StockRecord.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='StockRecord',
                    object_id=self.kwargs['offer_id']
                )
            )

    def get_partner(self):
        return super(BaseStockRecordView, self).get_object()
