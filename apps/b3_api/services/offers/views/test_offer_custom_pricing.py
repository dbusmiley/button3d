from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from apps.b3_api.services.offers.views.base_stock_record_view \
    import BaseStockRecordView
from apps.b3_api.utils.helpers import underscore_dict_to_camelcase
from apps.b3_compare.utils import StockRecordPriceTester, \
    AnalyzePendingException
from apps.b3_core.models import StlFile
from apps.b3_core.utils import PriceEvaluationError, \
    ManualPricingRequiredException, NoPricingPossibleException
from apps.partner.models import PostProcessing
from apps.b3_core.factories import ErrorResponseFactory

errorresponse_factory = ErrorResponseFactory('DJANGO_API')


class TestOfferCustomPricing(BaseStockRecordView):
    @staticmethod
    def _convert_to_output_format(price_info):
        price_info = underscore_dict_to_camelcase(price_info)

        price_info['price']['exclusiveTax'] = \
            price_info['price'].pop('exclTax')

        price_info['price']['inclusiveTax'] \
            = price_info['price'].pop('inclTax')

        return price_info

    def post(self, request, *args, **kwargs):
        """
        Provide custom pricing formulae for a given pricing formula and offer
        """
        stlfile = get_object_or_404(
            StlFile, uuid=request.data.get('uuid', None)
        )
        price_tester = StockRecordPriceTester(self.get_object())
        price_tester.set_custom_values(
            price_formula=request.data.get('priceFormula', None),
            support_enabled=request.data.get('supportEnabled', None),
            support_formula=request.data.get('supportFormula', None),
            support_angle=request.data.get('supportAngle', None),
            support_offset=request.data.get('supportOffset', None),
            orientation_formula=request.data.get('orientationFormula', None)
        )

        post_processing_ids = request.data.get('selectedPostProcessings', [])

        post_processings = PostProcessing.objects.filter(
            id__in=post_processing_ids
        )

        try:
            price_info = price_tester.get_price(
                stlfile,
                post_processings=post_processings,
                quantity=request.data.get('quantity', 1),
                scale=request.data.get('scale', 1.0),
            )
            return Response(self._convert_to_output_format(price_info))
        except PriceEvaluationError as e:
            return errorresponse_factory.make_badrequest(
                error_code='PRICE_FORMULA_INVALID',
                message='Error while evaluating the price. '
                'Make sure the formula is correct.',
                error_message=str(e),
                formula=e.formula_name
            )
        except AnalyzePendingException:
            return errorresponse_factory.make_badrequest(
                error_code='ANALYZE_PENDING',
                message='The file is still being analyzed. '
                'Please wait a moment and retry'
            )
        except ManualPricingRequiredException:
            return errorresponse_factory.make_badrequest(
                error_code='MANUAL_PRICING_REQUIRED',
                message='Manual Pricing is required for this item'
            )
        except NoPricingPossibleException:
            return errorresponse_factory.make_badrequest(
                error_code='PRICE_EVALUATION_ERROR',
                message='An unexpected error ocurred '
                'while evaluating the price.'
            )
