from apps.catalogue.models.product import Product
from apps.partner.models import StockRecord
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response

from apps.b3_api.services.offers.serializers import ListServiceOffersSerializer
from apps.b3_api.services.views import BaseServiceView


class ListServiceOffers(BaseServiceView, ListCreateAPIView):
    serializer_class = ListServiceOffersSerializer

    def get_queryset(self):
        return StockRecord.objects.filter(
            partner__id=self.kwargs['service_id']
        )

    def post(self, request, *args, **kwargs):
        try:
            product_id = request.data['finishing']
            product = Product.objects.get(
                id=product_id
            )
        except KeyError:
            raise ValidationError({
                'finishing': 'This field is required.'
            })
        except Product.DoesNotExist:
            raise ValidationError({
                'finishing': 'Invalid finishing id.'
            })

        stock_record, created = StockRecord.objects.get_or_create(
            partner=self.get_object(),
            product=product
        )
        return Response(
            {
                'id': stock_record.id
            }, status=status.HTTP_201_CREATED
        )
