import json

from apps.b3_tests.factories import create_product,\
    create_stockrecord, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.conclusionactions import FixedDiscountAction
from apps.partner.conditions import QuantityGreaterCondition


class OfferDiscountsTest(AuthenticatedTestCase):
    """
    This test will check the API for offer discounts. By default in the set up
    method one stock record and one discount will be created which should be
    respected for the _tests.
    """

    def setUp(self):
        super(OfferDiscountsTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=2)
        )

    def get_discount_response(self):
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/discounts/'
        return self.client.get(url)

    def create_default_discount(self):
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/discounts/'
        new_discount_dict = {
            "conditions": [
                {
                    "type": "price-in-range",
                    "arguments": {
                        "maxPrice": 55,
                        "minPrice": 22
                    }
                }
            ],
            "action": {
                "type": "fixed",
                "value": 55
            }
        }
        return self.client.post(
            url,
            data=json.dumps(new_discount_dict),
            content_type="application/json"
        )

    def edit_default_stock_record_discount(self):
        discount_rule = self.stock_record.get_discount_rules()[0]
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}' \
            f'/discounts/{discount_rule.id}/'
        new_discount_dict = {
            "conditions": [
                {
                    "type": "price-in-range",
                    "arguments": {
                        "maxPrice": 55,
                        "minPrice": 22
                    }
                }
            ],
            "action": {
                "type": "fixed",
                "value": 55
            }
        }
        return self.client.put(
            url,
            data=json.dumps(new_discount_dict),
            content_type="application/json"
        )

    def delete_default_stock_record_discount(self):
        discount_rule = self.stock_record.get_discount_rules()[0]
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}' \
            f'/discounts/{discount_rule.id}/'
        return self.client.delete(url, )

    def test_getting_default_discounts(self):
        response = self.get_discount_response()
        self.assertEqual(len(response.data), 1)

    def test_default_value(self):
        response = self.get_discount_response()
        self.assertEqual(response.data[0]['action']['value'], 10)

    def test_default_action(self):
        response = self.get_discount_response()
        self.assertEqual(response.data[0]['action']['type'], 'fixed')

    def test_default_type(self):
        response = self.get_discount_response()
        self.assertEqual(
            response.data[0]['conditions'][0]['type'], 'quantity-greater'
        )

    def test_default_discount_type(self):
        response = self.get_discount_response()
        self.assertEqual(response.data[0]['action']['type'], 'fixed')

    def test_default_arguments(self):
        response = self.get_discount_response()
        self.assertEqual(
            len(response.data[0]['conditions'][0]['arguments']), 1
        )
        self.assertEqual(
            response.data[0]['conditions'][0]['arguments']['maxQuantity'], 2
        )

    def test_creating_discount_offer_status_code(self):
        response = self.create_default_discount()
        self.assertEqual(response.status_code, 201)

    def test_creating_discount_offer_amount(self):
        self.create_default_discount()
        discount_rules = self.stock_record.get_discount_rules()
        self.assertEqual(len(discount_rules), 2)
        self.create_default_discount()
        self.create_default_discount()
        discount_rules = self.stock_record.get_discount_rules()
        self.assertEqual(len(discount_rules), 4)

    def test_editing_discount_offer_status_code(self):
        response = self.edit_default_stock_record_discount()
        self.assertEqual(response.status_code, 200)

    def test_editing_discount_offer_amount_should_not_change(self):
        self.edit_default_stock_record_discount()
        discount_rules = self.stock_record.get_discount_rules()
        self.assertEqual(len(discount_rules), 1)

    def test_editing_discount_offer_conclusion_actions(self):
        self.edit_default_stock_record_discount()
        discount_rule_conclusion_actions = json.loads(
            self.stock_record.get_discount_rules()[0].conclusion_actions
        )
        action_name, action_args = discount_rule_conclusion_actions[0]
        self.assertEqual(action_name, 'FixedDiscountAction')
        self.assertEqual(action_args, {'amount': 55})

    def test_editing_discount_offer_conditions(self):
        self.edit_default_stock_record_discount()
        discount_rule_condition = json.loads(
            self.stock_record.get_discount_rules()[0].conditions
        )
        self.assertEqual(len(discount_rule_condition), 1)
        condition_name, condition_args = discount_rule_condition[0]
        self.assertEqual(condition_name, 'PriceInRangeCondition')
        self.assertEqual(condition_args, {'min_price': 22, 'max_price': 55})

    def test_deleting_discount_offer_status_code(self):
        response = self.delete_default_stock_record_discount()
        self.assertEqual(response.status_code, 204)

    def test_deleting_discount_offer_amount(self):
        self.delete_default_stock_record_discount()
        discount_rules = self.stock_record.get_discount_rules()
        self.assertEqual(len(discount_rules), 0)
