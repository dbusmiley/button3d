import json

from apps.b3_tests.factories import create_product, \
    create_stockrecord, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OfferManualPricingTest(AuthenticatedTestCase):
    """
    This test will check the API for offer discounts. By default in the set up
    method one stock record and one discount will be created which should be
    respected for the _tests.
    """

    def setUp(self):
        super(OfferManualPricingTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

        self.url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/manual-price/'

    def generate_mock_pricing_data(self, is_rule_enabled):
        return {
            'totalPrice': {
                'enabled': True,
                'value': 40
            },
            'itemTotalPrice': {
                'enabled': is_rule_enabled,
                'value': -34
            },
            'totalQuantity': {
                'enabled': False,
                'value': 0
            }
        }

    def test_invalid_and_disabled_manual_pricing(self):
        manual_pricing_args = self.generate_mock_pricing_data(False)
        response = self.client.post(
            self.url,
            data=json.dumps(manual_pricing_args),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)

    def test_invalid_and_enabled_manual_pricing(self):
        manual_pricing_args = self.generate_mock_pricing_data(True)
        response = self.client.post(
            self.url,
            data=json.dumps(manual_pricing_args),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 400)

    def test_disabled_manual_pricing(self):
        manual_pricing_args = {
            'totalPrice': {
                'enabled': False,
                'value': None
            },
            'itemTotalPrice': {
                'enabled': False,
                'value': None
            },
            'totalQuantity': {
                'enabled': False,
                'value': None
            }
        }
        response = self.client.post(
            self.url,
            data=json.dumps(manual_pricing_args),
            content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
