import json

from apps.b3_tests.factories import create_product, \
    create_stockrecord, PartnerFactory, StlFileFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.partner.conclusionactions import FixedDiscountAction
from apps.partner.conditions import QuantityGreaterCondition


class OfferCustomPricingTest(AuthenticatedTestCase):
    """
    This test will check the API for offer discounts. By default in the set up
    method one stock record and one discount will be created which should be
    respected for the _tests.
    """

    def setUp(self):
        super(OfferCustomPricingTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

        self.stock_record.add_discount_rule(
            FixedDiscountAction(amount=10),
            QuantityGreaterCondition(max_quantity=2)
        )
        self.demo_file = StlFileFactory(
            uuid='00000000-0000-0000-0000-000000000000', showname='demo'
        )
        self.url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/test-pricing-formula/'

    def test_custom_pricing(self):
        """
        Logic copied from apps.b3_compare._tests.test_api.CustomPriceTest
        :return:
        """
        custom_pricing_args = {
            'uuid': self.demo_file.uuid,
            'priceFormula': '1.25+root(volume)+pow(box, 0.33)'
        }
        response = self.client.post(
            self.url,
            data=json.dumps(custom_pricing_args),
            content_type="application/json"
        )

        self.assertEqual(response.status_code, 200)
        response_json = response.json()
        self.assertEqual(response_json['filename'], 'demo')
        price = response_json['price']
        self.assertEqual(price['exclusiveTax'], 225.73)
        self.assertEqual(price['tax'], 42.89)
        self.assertEqual(price['inclusiveTax'], 268.62)

    def test_custom_price_eval_return_error(self):
        """
        Logic copied from apps.b3_compare._tests.test_api.CustomPriceTest
        :return:
        """
        custom_pricing_args = {
            'uuid': self.demo_file.uuid,
            'priceFormula': '(bla ?./',
        }

        response = self.client.post(self.url, data=custom_pricing_args)

        self.assertEqual(response.status_code, 400)
        response_json = response.json()
        self.assertEqual(
            response_json['code'],
            'DJANGO_API_PRICE_FORMULA_INVALID'
        )
