import json

from apps.b3_tests.factories import create_product, \
    create_stockrecord, PartnerFactory, StlFileFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OfferCustomPricingDetailTest(AuthenticatedTestCase):
    """
    This test will check the API for offer discounts. By default in the set up
    method one stock record and one discount will be created which should be
    respected for the _tests.
    """

    def setUp(self):
        super(OfferCustomPricingDetailTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )
        self.demo_file = StlFileFactory(
            uuid='00000000-0000-0000-0000-000000000000', showname='demo'
        )

    def test_pricing_detail_for_pricing_disabled(self):
        self.stock_record.support_enabled = False
        self.stock_record.save()
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/price/'
        self.assertEqual(
            {
                "priceFormula": self.stock_record.price_custom,
                "supportStructure": None
            },
            self.client.get(url).data
        )

    def test_pricing_detail_for_pricing_enabled(self):
        self.stock_record.support_angle = 46.00
        self.stock_record.support_offset = 2.00
        self.stock_record.price_support_custom = \
            '0.50 * 0.20 * volume + 0.30 * area'
        self.stock_record.price_orientation_custom = 5
        self.stock_record.support_enabled = True
        self.stock_record.save()

        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/price/'
        response_data = self.client.get(url).data

        self.assertEqual(response_data['supportStructure']['angle'], '46.0000')
        self.assertEqual(response_data['supportStructure']['offset'], '2.00')
        self.assertEqual(
            response_data['supportStructure']['formula'],
            '0.50 * 0.20 * volume + 0.30 * area'
        )
        self.assertEqual(
            response_data['supportStructure']['orientationFormula'], '5'
        )

    def test_post_pricing_details(self):
        self.stock_record.support_enabled = False
        self.stock_record.save()

        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/offers/{self.stock_record.id}/price/'
        response_data = self.client.get(url).data
        self.assertIsNone(response_data['supportStructure'])

        put_data = {
            'priceFormula': self.stock_record.price_custom,
            'supportStructure': {
                'angle': 90.0,
                'offset': 10.0,
                'formula': 'w+h+d',
                'orientationFormula': ''
            }
        }

        put_response_raw = self.client.put(
            url,
            data=json.dumps(put_data),
            content_type="application/json",
        )
        put_response = json.loads(put_response_raw.content)

        self.assertEqual(put_response['supportStructure']['angle'], '90.0000')
        self.assertEqual(put_response['supportStructure']['offset'], '10.00')
        self.assertEqual(put_response['supportStructure']['formula'], 'w+h+d')

        invalid_put_data = {
            'priceFormula': self.stock_record.price_custom,
            'supportStructure': {
                'angle': 91.0,
                'offset': -10.0,
                'formula': 'a+b+c',
                'orientationFormula': 'x+y+z'
            }
        }

        invalid_put_response_raw = self.client.put(
            url,
            data=json.dumps(invalid_put_data),
            content_type="application/json",
        )
        invalid_put_response = json.loads(
            invalid_put_response_raw.content
        )

        self.assertEqual(
            invalid_put_response['code'], 'StockRecord_VALIDATION_ERROR'
        )

        invalid_fields = sorted(
            [x['field'] for x in invalid_put_response['moreInfo']]
        )
        expected_invalid_fields = sorted(
            [
                'supportStructure.formula', 'supportStructure.angle',
                'supportStructure.offset',
                'supportStructure.orientationFormula'
            ]
        )

        self.assertEqual(expected_invalid_fields, invalid_fields)
