from apps.b3_tests.factories import create_product, \
    create_stockrecord, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OfferStatusUpdateTest(AuthenticatedTestCase):
    """
    This test will check the API for offer discounts. By default in the set up
    method one stock record and one discount will be created which should be
    respected for the _tests.
    """

    def setUp(self):
        super(OfferStatusUpdateTest, self).setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

    def test_invalid_offer_settings_validations(self):
        self.stock_record.min_production_days = 10
        self.stock_record.max_production_days = 1
        self.stock_record.save()
        offer_status_update = self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/offers/{self.stock_record.id}/status/',
            data={
                "published": True
            }
        )
        self.assertEqual(offer_status_update.status_code, 400)
        self.assertEqual(
            offer_status_update.data['code'], 'OFFER_UPDATE_FAILED'
        )
        self.assertEqual(
            offer_status_update.data['message'],
            'Required fields are set or contain errors'
        )
        self.assertIn(
            offer_status_update.data['moreInfo']['exception']
            ['maxProductionDays'], 'Maximum must be bigger than minimum.'
        )

    def test_invalid_price_formula_validations(self):
        self.stock_record.min_production_days = 1
        self.stock_record.max_production_days = 10
        self.stock_record.max_x_mm = 10
        self.stock_record.max_y_mm = 10
        self.stock_record.max_z_mm = 10
        self.stock_record.min_x_mm = 1
        self.stock_record.min_y_mm = 1
        self.stock_record.min_z_mm = 1

        self.stock_record.price_custom = 'foo'
        self.stock_record.save()
        offer_status_update = self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/offers/{self.stock_record.id}/status/',
            data={
                "published": True
            }
        )
        self.assertIn(
            offer_status_update.data['moreInfo']['exception']['priceFormula'],
            'Undefined variable \'foo\''
        )

        self.stock_record.price_custom = ''
        self.stock_record.save()

        offer_status_update = self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/offers/{self.stock_record.id}/status/',
            data={
                "published": True
            }
        )
        self.assertIn(
            offer_status_update.data['moreInfo']['exception']['priceFormula'],
            'This field is required.'
        )

    def test_valid_offer_settings_validations(self):
        self.stock_record.min_production_days = 1
        self.stock_record.max_production_days = 10
        self.stock_record.max_x_mm = 10
        self.stock_record.max_y_mm = 10
        self.stock_record.max_z_mm = 10
        self.stock_record.min_x_mm = 1
        self.stock_record.min_y_mm = 1
        self.stock_record.min_z_mm = 1
        self.stock_record.save()

        offer_status_update = self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/offers/{self.stock_record.id}/status/',
            data={
                "published": True
            }
        )
        self.assertEqual(offer_status_update.status_code, 202)
