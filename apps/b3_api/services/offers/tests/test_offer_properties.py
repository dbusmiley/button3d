from apps.b3_tests.factories import (
    PartnerFactory,
    create_parent_product,
    create_child_product,
    create_stockrecord,
    TechnologyFactory,
)
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OfferTechnologyPropertyTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(name='Meltwerk')
        self.partner.users.add(self.user)
        self.partner.save()

    def test_invalid_offer_settings_validations(self):
        technology_a = TechnologyFactory(title='Technology A')
        TechnologyFactory(title='Technology B')

        product = create_parent_product(technology=technology_a)
        finish = create_child_product(
            parent_product=product, is_colorable=True, technology=technology_a
        )
        stock_record = create_stockrecord(
            child_product=finish, partner=self.partner, is_default=True
        )
        stock_record.min_production_days = 10
        stock_record.max_production_days = 1
        stock_record.save()
        offer_status_update = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/offers/'
        )
        self.assertEqual(
            technology_a.id, offer_status_update.data[0]["technologyId"]
        )
