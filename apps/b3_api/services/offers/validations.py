from decimal import Decimal as D

from apps.b3_core.utils import eval_price_custom


def validate_production_days(
    min_production_days, min_production_days_key, max_production_days,
    max_production_days_key
):
    errors = {}
    required_error = 'This field is required.'
    min_max_error = 'Maximum must be bigger than minimum.'

    if min_production_days is None:
        errors[min_production_days_key] = required_error
    # validate max_production_days
    if max_production_days is None:
        errors[max_production_days_key] = required_error

    # validate min_production_days < max_production_days
    if min_production_days is not None \
        and max_production_days is not None \
            and min_production_days > max_production_days:
        errors[max_production_days_key] = min_max_error

    return errors


def validate_min_max_bounds(min_dimension, max_dimension, max_dimension_key):
    errors = {}
    required_error = 'This field is required.'
    min_max_error = 'Maximum must be bigger than minimum.'
    if max_dimension is None:
        errors[max_dimension_key] = required_error
    if max_dimension is not None and min_dimension is not None \
            and min_dimension > max_dimension:
        errors[max_dimension_key] = min_max_error

    return errors


def validate_price_formula(price_formula):
    if not price_formula:
        return False, 'This field is required.'

    try:
        result = eval_price_custom(
            price_custom=price_formula,
            quantity=1,
            model_volume=D('23286.07'),
            box_volume=D('422773.03'),
            shells_number=D('1'),
            area=D('17672.93'),
            width=D('75'),
            height=D('75'),
            depth=D('75'),
            machine_volume=D('23286.07')
        )
        if result <= 0:
            return False, \
                f'Test result (volume=23286.07; box=422773.03; ' \
                f'shells=1; area=17672.93; width=75; height=75; ' \
                f'depth=75; machine_volume=23286.07): {result}'
        return True, None
    except BaseException as e:
        return False, str(e)


def validate_manual_pricing_conditions(
    item_total_price, total_price, total_quantity
):
    return not any(
        [
            item_total_price['enabled'] and
            (item_total_price['value'] < 0), total_price['enabled'] and
            (total_price['value'] < 0), total_quantity['enabled'] and
            (total_quantity['value'] < 0)
        ]
    )
