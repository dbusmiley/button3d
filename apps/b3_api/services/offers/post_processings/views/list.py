from rest_framework.generics import ListCreateAPIView

from apps.b3_api.services.offers.post_processings.serializers import \
    PostProcessingSerializer
from apps.b3_api.services.offers.views import BaseStockRecordView


class PostProcessingListView(BaseStockRecordView, ListCreateAPIView):
    serializer_class = PostProcessingSerializer

    def get_queryset(self):
        return self.get_object().post_processings.all()

    def perform_create(self, serializer):
        serializer.save(stock_record=self.get_object())
