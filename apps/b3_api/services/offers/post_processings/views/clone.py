import typing as t
import button3d.type_declarations as td
from django.db import IntegrityError
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response

from apps.b3_api.services.offers.post_processings.serializers import \
    PostProcessingCloneSerializer, PostProcessingSerializer
from apps.b3_api.services.offers.views import BaseStockRecordView
from apps.partner.models import PostProcessing


class PostProcessingCloneView(BaseStockRecordView, CreateAPIView):
    serializer_class = PostProcessingCloneSerializer
    queryset = []

    def create(self, request, *args, **kwargs) -> Response:
        clone_serializer = self.get_serializer(
            data=request.data, partner=self.get_partner()
        )
        clone_serializer.is_valid(raise_exception=True)

        cloned_post_processings, already_existing_post_processing_names = \
            self.clone_post_processings(
                clone_serializer.validated_data['postProcessingIds']
            )

        post_processing_serializer = PostProcessingSerializer(
            cloned_post_processings,
            many=True,
            context=self.get_serializer_context(),
        )

        headers = self.get_success_headers(post_processing_serializer.data)

        response_data = {
            'cloned': post_processing_serializer.data,
            'errors': already_existing_post_processing_names
        }

        return Response(
            response_data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def clone_post_processings(self, post_processing_ids: t.Sequence[int]) -> \
            t.Tuple[td.PostProcessings, t.Sequence[str]]:
        """
        Clones passed post-processing objects.

        Returns tuple of cloned post-processing objects and names of already
        existing post-processings
        """

        stock_record = self.get_object()
        original_post_processings = PostProcessing.objects.filter(
            id__in=post_processing_ids,
            stock_record__partner=stock_record.partner
        )
        cloned_post_processings = []
        already_existing_post_processing_names = []
        for post_processing in original_post_processings:
            try:
                cloned_post_processings.append(self.create_clone(
                    post_processing,
                    stock_record
                ))
            except IntegrityError:
                already_existing_post_processing_names.append(
                    post_processing.title
                )

        return cloned_post_processings, already_existing_post_processing_names

    @staticmethod
    def create_clone(
            post_processing: td.PostProcessing,
            stock_record: td.StockRecord,
    ) -> td.PostProcessing:
        """
        Clone post_processing as a post-processing for this StockRecord
        """

        colors = [color for color in post_processing.colors.all()]
        post_processing.pk = None
        post_processing.stock_record = stock_record
        post_processing.published = False
        post_processing.save()
        post_processing.colors.add(*colors)
        return post_processing
