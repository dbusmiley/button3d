from django.http import Http404

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.offers.views import BaseStockRecordView
from apps.partner.models import PostProcessing


class BasePostProcessingView(BaseStockRecordView):
    def get_object(self):
        try:
            return PostProcessing.objects.get(
                id=self.kwargs['post_processing_id'],
                stock_record_id=self.kwargs['offer_id'],
                stock_record__partner_id=self.kwargs['service_id'],
            )
        except PostProcessing.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='PostProcessing',
                    object_id=self.kwargs['post_processing_id']
                )
            )
