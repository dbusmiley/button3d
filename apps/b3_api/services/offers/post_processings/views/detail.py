from rest_framework.generics import RetrieveUpdateDestroyAPIView

from apps.b3_api.services.offers.post_processings.serializers import \
    PostProcessingSerializer
from apps.b3_api.services.offers.post_processings.views.base import \
    BasePostProcessingView
from apps.b3_api.services.offers.utils import DataSheetViewMixin


class PostProcessingDetailView(
    BasePostProcessingView,
    RetrieveUpdateDestroyAPIView
):
    serializer_class = PostProcessingSerializer


class PostProcessingDataSheetView(BasePostProcessingView, DataSheetViewMixin):
    pass
