from django.conf.urls import url

from apps.b3_api.services.offers.post_processings.views import \
    PostProcessingListView, PostProcessingDetailView, \
    PostProcessingCloneView, PostProcessingDataSheetView

urlpatterns = [
    url(r'^$', PostProcessingListView.as_view()),
    url(
        r'^(?P<post_processing_id>[0-9]+)/$',
        PostProcessingDetailView.as_view()
    ),
    url(
        r'^(?P<post_processing_id>[0-9]+)/datasheet/'
        r'(?P<language>german|english)/$',
        PostProcessingDataSheetView.as_view()
    ),
    url(r'^clone/$', PostProcessingCloneView.as_view()),
]
