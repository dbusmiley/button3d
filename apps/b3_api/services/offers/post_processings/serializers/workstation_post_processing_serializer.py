from apps.partner.models import PostProcessing

from rest_framework import serializers


class WorkstationPostProcessingSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostProcessing
        fields = (
            'id',
            'title'
        )
