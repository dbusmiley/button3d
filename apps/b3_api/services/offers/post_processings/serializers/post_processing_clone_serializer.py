from rest_framework.fields import MultipleChoiceField
from rest_framework.serializers import Serializer

from apps.partner.models import PostProcessing


class PostProcessingCloneSerializer(Serializer):
    postProcessingIds = MultipleChoiceField(choices=[])

    def __init__(self, *args, **kwargs):
        partner = kwargs.pop('partner')
        super().__init__(*args, **kwargs)
        self.fields['postProcessingIds'].choices = [
            (post_processing.id, post_processing.id)
            for post_processing
            in PostProcessing.objects.filter(stock_record__partner=partner)
        ]
