from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.b3_api.services.offers.utils import get_datasheet_data
from apps.b3_api.services.offers.validations \
    import validate_production_days, validate_min_max_bounds
from apps.b3_api.utils.generate_model_field import get_model_field
from apps.b3_api.utils.serializers import NestingSerializer
from apps.b3_core.utils import eval_price_custom, PriceEvaluationError
from apps.partner.models import PostProcessing


class PostProcessingSerializer(NestingSerializer):
    manuallyPriced = get_model_field(PostProcessing, 'always_priced_manually')
    priceFormula = get_model_field(PostProcessing, 'price_formula')
    datasheet = serializers.SerializerMethodField()
    unit = serializers.SerializerMethodField()

    def get_unit(self, obj):
        return 'mm'

    def get_datasheet(self, obj):
        return get_datasheet_data(request=self.context.get('request'), obj=obj)

    def validate(self, attrs):
        if attrs['published'] is False:
            # If PostProcessing is unpublished, it doesn't need to be valid
            return attrs

        _, production_errors = self.validate_production_days(
            attrs['production_days_min'], attrs['production_days_max']
        )

        _, bounds_errors = self.validate_bounds(
            attrs['bounds_x_min'],
            attrs['bounds_x_max'],
            attrs['bounds_y_min'],
            attrs['bounds_y_max'],
            attrs['bounds_z_min'],
            attrs['bounds_z_max'],
        )

        errors = {**production_errors, **bounds_errors}

        try:
            eval_price_custom(attrs['price_formula'], *[1] * 9)
        except PriceEvaluationError as e:
            errors['price_formula'] = f'Formula is invalid: "{str(e)}"'

        if errors:
            raise ValidationError(errors)
        return attrs

    @staticmethod
    def validate_production_days(production_days_min, production_days_max):
        errors = validate_production_days(
            production_days_min,
            'production_days_min',
            production_days_max,
            'production_days_max'
        )
        return bool(errors), errors

    @staticmethod
    def validate_bounds(min_x, max_x, min_y, max_y, min_z, max_z):
        x_errors = validate_min_max_bounds(
            min_x, max_x, 'bounds_x_max'
        )
        y_errors = validate_min_max_bounds(
            min_y, max_y, 'bounds_y_max'
        )
        z_errors = validate_min_max_bounds(
            min_z, max_z, 'bounds_z_max'
        )
        errors = {**x_errors, **y_errors, **z_errors}
        return bool(errors), errors

    class Meta:
        model = PostProcessing
        fields = (
            'id',
            'published',
            'title',
            'description',
            'production_days_min',
            'production_days_max',
            'manuallyPriced',
            'priceFormula',
            'datasheet',
            'colors',
            'bounds_x_min',
            'bounds_x_max',
            'bounds_y_min',
            'bounds_y_max',
            'bounds_z_min',
            'bounds_z_max',
            'unit'
        )
        nesting_options = [
            {
                'name': 'production',
                'translations': [
                    ('production_days_min', 'minDays'),
                    ('production_days_max', 'maxDays')
                ]
            },
            {
                'name': 'properties',
                'translations': [
                    {
                        'name': 'x',
                        'translations': [
                            ('bounds_x_min', 'minimum'),
                            ('bounds_x_max', 'maximum'),
                        ]
                    },
                    {
                        'name': 'y',
                        'translations': [
                            ('bounds_y_min', 'minimum'),
                            ('bounds_y_max', 'maximum'),
                        ]
                    },
                    {
                        'name': 'z',
                        'translations': [
                            ('bounds_z_min', 'minimum'),
                            ('bounds_z_max', 'maximum'),
                        ]
                    },
                    ('unit', 'unit')
                ]
            }

        ]
