from rest_framework import serializers

from apps.partner.models import PostProcessing


class PartPostProcessingSerializer(serializers.ModelSerializer):

    class Meta:
        model = PostProcessing
        fields = (
            'id',
            'title',
        )
