from apps.b3_api.services.offers.post_processings.\
    serializers.workstation_post_processing_serializer import \
    WorkstationPostProcessingSerializer
from apps.b3_tests.factories import PostProcessingFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase


class WorkstationPostProcessingSerializerTest(TestCase):
    def setUp(self):
        super().setUp()

        post_processing = PostProcessingFactory()
        self.serializer_populated = WorkstationPostProcessingSerializer(
            instance=post_processing)

        self.structure_expected = {'id': 0, 'title': ''}

    def test_structure(self):
        assert_serializer_structure(
            self, self.serializer_populated, self.structure_expected
        )
