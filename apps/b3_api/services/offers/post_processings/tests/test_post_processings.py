import json
import os

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test.client import MULTIPART_CONTENT
from rest_framework import status

from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.clients import ApiKeyAuthorizedClient
from apps.b3_tests.factories import PartnerFactory, ProductFactory, \
    StockRecordFactory, PostProcessingFactory, create_post_processing, \
    ColorFactory, PostProcessing, StlFileFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class PostProcessingTest(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory()
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = ProductFactory()
        self.stock_record = StockRecordFactory(
            product=self.product, partner=self.partner
        )

    def test_list(self):
        post_processings = [
            PostProcessingFactory(stock_record=self.stock_record)
            for i in range(5)
        ]
        post_processing_ids = sorted(
            post_processing.id for post_processing in post_processings
        )

        list_url = (f'/api/v2.0/'
                    f'services/{self.partner.id}/'
                    f'offers/{self.stock_record.id}/'
                    f'post-processings/')
        list_response = self.client.get(list_url)
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)

        list_json = list_response.json()
        retrieved_ids = sorted(
            post_processing['id'] for post_processing in list_json
        )
        self.assertEqual(retrieved_ids, post_processing_ids)

    def test_detail(self):
        post_processing = create_post_processing(
            self.stock_record, has_colors=True
        )

        detail_url = (f'/api/v2.0/'
                      f'services/{self.partner.id}/'
                      f'offers/{self.stock_record.id}/'
                      f'post-processings/{post_processing.id}/')
        detail_response = self.client.get(detail_url)
        self.assertEqual(detail_response.status_code, status.HTTP_200_OK)

        self.assertEqual(
            detail_response.json(), self.get_representation(post_processing)
        )

    def test_create(self):
        color = ColorFactory()
        create_json = {
            'title': 'Test',
            'description': 'Test post-processing',
            'published': True,
            'production': {
                'minDays': 1,
                'maxDays': 2
            },
            'properties': {
                'x': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'y': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'z': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'unit': 'mm'
            },
            'manuallyPriced': False,
            'priceFormula': '123',
            'colors': [color.id]
        }

        list_url = (f'/api/v2.0/'
                    f'services/{self.partner.id}/'
                    f'offers/{self.stock_record.id}/'
                    f'post-processings/')
        create_response = self.client.post(
            list_url,
            data=json.dumps(create_json),
            content_type='application/json'
        )
        self.assertEqual(create_response.status_code, status.HTTP_201_CREATED)

        create_response_json = create_response.json()
        post_processing = PostProcessing.objects.get(
            id=create_response_json['id']
        )

        self.assertEqual(post_processing.stock_record, self.stock_record)
        self.assertEqual(
            create_response_json, self.get_representation(post_processing)
        )
        self.check_values_updated(post_processing, create_json)

    def test_delete(self):
        post_processing = create_post_processing(self.stock_record)
        detail_url = (f'/api/v2.0/'
                      f'services/{self.partner.id}/'
                      f'offers/{self.stock_record.id}/'
                      f'post-processings/{post_processing.id}/')
        delete_response = self.client.delete(detail_url)
        self.assertEqual(
            delete_response.status_code, status.HTTP_204_NO_CONTENT
        )

        with self.assertRaises(PostProcessing.DoesNotExist):
            PostProcessing.objects.get(id=post_processing.id)
        # Make sure PostProcessing was not actually deleted
        PostProcessing.all_objects.get(id=post_processing.id)

    def test_update(self):
        color = ColorFactory()
        post_processing = create_post_processing(self.stock_record)
        update_json = {
            'title': 'Test',
            'published': True,
            'description': 'Test post-processing',
            'production': {
                'minDays': 10,
                'maxDays': 20
            },
            'properties': {
                'x': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'y': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'z': {
                    'minimum': 5,
                    'maximum': 1000
                },
                'unit': 'mm'
            },
            'manuallyPriced': True,
            'priceFormula': '123',
            'colors': [color.id]
        }

        detail_url = (f'/api/v2.0/'
                      f'services/{self.partner.id}/'
                      f'offers/{self.stock_record.id}/'
                      f'post-processings/{post_processing.id}/')
        update_response = self.client.put(
            detail_url,
            data=json.dumps(update_json),
            content_type='application/json'
        )
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        post_processing.refresh_from_db()
        self.assertEqual(
            update_response.json(), self.get_representation(post_processing)
        )
        self.check_values_updated(post_processing, update_json)

    def test_datasheet_upload(self):
        post_processing = create_post_processing(self.stock_record)
        datasheet_fname = 'datasheet.pdf'
        datasheet_content = 'SomePdfContent'
        datasheet_file = SimpleUploadedFile(
            datasheet_fname,
            datasheet_content.encode('utf-8'),
            content_type='application/pdf'
        )

        datasheet_url = (f'/api/v2.0/'
                         f'services/{self.partner.id}/'
                         f'offers/{self.stock_record.id}/'
                         f'post-processings/{post_processing.id}/'
                         f'datasheet/english/')
        datasheet_upload_response = self.client.post(
            datasheet_url,
            data={'file': datasheet_file},
            content_type=MULTIPART_CONTENT,
            return_json=True,
        )
        self.assertEqual(
            datasheet_upload_response.status_code,
            status.HTTP_204_NO_CONTENT
        )

        post_processing.refresh_from_db()
        with open(post_processing.datasheet_pdf_en.path) as f:
            self.assertEqual(f.read(), datasheet_content)

        detail_url = (f'/api/v2.0/'
                      f'services/{self.partner.id}/'
                      f'offers/{self.stock_record.id}/'
                      f'post-processings/{post_processing.id}/')
        detail_response = self.client.get(detail_url)
        self.assertEqual(detail_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            detail_response.json(), self.get_representation(post_processing)
        )

    def test_datasheet_delete(self):
        post_processing = create_post_processing(self.stock_record)
        post_processing.datasheet_pdf_de.save(
            'test-file', ContentFile('test-content')
        )
        post_processing.save()

        datasheet_url = (f'/api/v2.0/'
                         f'services/{self.partner.id}/'
                         f'offers/{self.stock_record.id}/'
                         f'post-processings/{post_processing.id}/'
                         f'datasheet/german/')
        datasheet_delete_response = self.client.delete(datasheet_url)
        self.assertEqual(
            datasheet_delete_response.status_code, status.HTTP_204_NO_CONTENT
        )

        post_processing.refresh_from_db()
        self.assertFalse(post_processing.datasheet_pdf_de)

    def test_clone(self):
        other_product = ProductFactory()
        other_stock_record = StockRecordFactory(
            product=other_product, partner=self.partner
        )
        post_processings = [
            PostProcessingFactory(stock_record=self.stock_record)
            for i in range(5)
        ]

        clone_json = {
            'postProcessingIds': [
                post_processing.id for post_processing in post_processings
            ]
        }
        clone_url = (f'/api/v2.0/'
                     f'services/{self.partner.id}/'
                     f'offers/{other_stock_record.id}/'
                     f'post-processings/clone/')
        clone_response = self.client.post(
            clone_url,
            data=json.dumps(clone_json),
            content_type='application/json'
        )
        self.assertEqual(
            len(other_stock_record.post_processings.all()),
            len(self.stock_record.post_processings.all())
        )
        response_json = clone_response.json()
        cloned_post_processings = response_json['cloned']
        errors = response_json['errors']

        self.assertEqual([], errors)
        for post_processing in other_stock_record.post_processings.all():
            self.assertFalse(post_processing.published)
            self.assertIn(
                self.get_representation(post_processing),
                cloned_post_processings
            )

    def test_unpublished(self):
        stl_uuid = '00000000-0000-0000-0000-00000000000x'
        StlFileFactory(
            uuid=stl_uuid,
            showname='demo'
        )
        ShippingMethodFactory(
            partner=self.partner,
        )
        post_processing = create_post_processing(
            self.stock_record, published=False
        )
        query_params = {
            'uuid': stl_uuid,
            'country': 'DE',
            'currency': 'EUR',
            'scale': 1.0,
            'material': self.product.slug,
            'supplier': self.partner.code
        }
        api_client = ApiKeyAuthorizedClient(self.organization.private_api_key)
        response = api_client.get('/v1/finishes-prices/', query_params)
        # Only one finishing (raw) should be visible
        self.assertEqual(len(response.json()['results']), 1)
        post_processing.published = True
        post_processing.save()
        prices_response = api_client.get('/v1/finishes-prices/', query_params)
        results = prices_response.json()['results']
        # Now the post-processing should be visible too
        self.assertEqual(len(results), 2)
        self.assertEqual(results[1]['slug'], post_processing.full_slug)

    @staticmethod
    def get_representation(post_processing):
        datasheet_en = {'name': None, 'url': None}
        datasheet_de = {'name': None, 'url': None}
        if post_processing.datasheet_pdf_en:
            datasheet_en['name'] = os.path.basename(
                post_processing.datasheet_pdf_en.name
            )
            datasheet_en['url'] = (f'http://{settings.TEST_BASE_DOMAIN}'
                                   f'{post_processing.datasheet_pdf_en.url}')

        if post_processing.datasheet_pdf_de:
            datasheet_de['name'] = os.path.basename(
                post_processing.datasheet_pdf_de.name
            )
            datasheet_de['url'] = (f'http://{settings.TEST_BASE_DOMAIN}'
                                   f'{post_processing.datasheet_pdf_de.url}')

        return {
            'id': post_processing.id,
            'published': post_processing.published,
            'title': post_processing.title,
            'description': post_processing.description,
            'production': {
                'minDays': post_processing.production_days_min,
                'maxDays': post_processing.production_days_max
            },
            'properties': {
                'x': {
                    'minimum': f'{post_processing.bounds_x_min:.4f}'
                    if post_processing.bounds_x_min is not None else None,
                    'maximum': f'{post_processing.bounds_x_max:.4f}'
                },
                'y': {
                    'minimum': f'{post_processing.bounds_y_min:.4f}'
                    if post_processing.bounds_y_min is not None else None,
                    'maximum': f'{post_processing.bounds_y_max:.4f}'
                },
                'z': {
                    'minimum': f'{post_processing.bounds_z_min:.4f}'
                    if post_processing.bounds_z_min is not None else None,
                    'maximum': f'{post_processing.bounds_z_max:.4f}'
                },

                'unit': 'mm'
            },
            'manuallyPriced': post_processing.always_priced_manually,
            'datasheet': {
                'english': datasheet_en,
                'german': datasheet_de
            },
            'priceFormula': post_processing.price_formula,
            'colors': [color.id for color in post_processing.colors.all()]
        }

    def check_values_updated(self, post_processing, update_json):
        self.assertEqual(post_processing.title, update_json['title'])
        self.assertEqual(post_processing.published, update_json['published'])
        self.assertEqual(
            post_processing.description, update_json['description']
        )
        self.assertEqual(
            post_processing.production_days_min,
            update_json['production']['minDays']
        )
        self.assertEqual(
            post_processing.production_days_max,
            update_json['production']['maxDays']
        )
        self.assertEqual(
            post_processing.bounds_x_min,
            update_json['properties']['x']['minimum']
        )
        self.assertEqual(
            post_processing.bounds_x_max,
            update_json['properties']['x']['maximum']
        )
        self.assertEqual(
            post_processing.bounds_y_min,
            update_json['properties']['y']['minimum']
        )
        self.assertEqual(
            post_processing.bounds_y_max,
            update_json['properties']['y']['maximum']
        )
        self.assertEqual(
            post_processing.bounds_z_min,
            update_json['properties']['z']['minimum']
        )
        self.assertEqual(
            post_processing.bounds_z_max,
            update_json['properties']['z']['maximum']
        )
        self.assertEqual(
            post_processing.always_priced_manually,
            update_json['manuallyPriced']
        )
        self.assertEqual(
            post_processing.price_formula, update_json['priceFormula']
        )
        self.assertEqual(
            sorted([color.id for color in post_processing.colors.all()]),
            sorted(update_json['colors'])
        )
