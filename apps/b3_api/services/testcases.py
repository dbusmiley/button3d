from apps.b3_order.factories import OrderFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory
from apps.b3_organization.utils import get_current_site
from apps.basket.models import Basket


class ServiceAPITestCase(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceAPITestCase, self).setUp()
        self.site = get_current_site()

        self.partner = self.add_partner()

        self.basket = self.create_basket()

        self.stock_record = StockRecordFactory(partner=self.partner)

        self.line = self.add_line_to_basket(self.basket)

    def add_partner(self, accessible_by_client=True):
        partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        partner.site = self.site
        if accessible_by_client:
            partner.users.add(self.user)
        partner.save()
        return partner

    def create_basket(self):
        basket = BasketFactory(
            owner=self.user,
            status=Basket.EDITABLE
        )
        basket.site = self.site
        basket.save()
        return basket

    def order_basket(self, basket):
        order = OrderFactory(
            site=self.site,
            project=basket,
            purchased_by=self.user,
        )
        order.save()
        return order

    def add_line_to_basket(self, basket):
        stl_file = StlFileFactory()
        line, created = basket.add_product(
            product=self.stock_record.product,
            stockrecord=self.stock_record,
            stlfile=stl_file
        )
        line.save()
        return line
