import json

from apps.b3_organization.utils import get_current_site
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceTestOrderNotesSettingsTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceTestOrderNotesSettingsTest, self).setUp()
        self.site = get_current_site()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

    def test_default_configurations_for_order_notes(self):
        order_notes_settings_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/order-notes-settings/'
        )
        order_notes_settings = order_notes_settings_response_raw.data

        # Assure defaults for couple of settings
        self.assertEqual(order_notes_settings['orderConfirmationPrefix'],
                         'OC-')
        self.assertIsNone(order_notes_settings['generalSalesConditions'])

    def test_update_order_notes_settings_api(self):
        updated_order_notes_config = {
            'orderConfirmationPrefix': 'OCC-',
            'deliveryNotePrefix': 'DL-',
            'invoicePrefix': 'IN-',
            'generalSalesConditions': None,
            'generalShippingConditions': None,
            'footerCompanyInformation': None
        }
        order_notes_settings_response_raw = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/order-notes-settings/',
            data=json.dumps(updated_order_notes_config),
            content_type='application/json'
        )
        order_notes_settings = order_notes_settings_response_raw.data

        # Assure defaults for couple of settings
        self.assertEqual(order_notes_settings['orderConfirmationPrefix'],
                         updated_order_notes_config['orderConfirmationPrefix'])

        self.partner.refresh_from_db()
        self.assertEqual(self.partner.order_confirmation_prefix, 'OCC-')
