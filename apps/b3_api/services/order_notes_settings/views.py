from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.services.views import BaseServiceView
from apps.b3_api.services.order_notes_settings.serializers import \
    OrderNotesSettingsSerializer


class OrderNotesSettingsView(BaseServiceView, RetrieveUpdateAPIView):
    serializer_class = OrderNotesSettingsSerializer
