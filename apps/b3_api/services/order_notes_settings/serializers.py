from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.partner.models import Partner


class OrderNotesSettingsSerializer(PayloadConverterMixin,
                                   serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = ('order_confirmation_prefix', 'delivery_note_prefix',
                  'invoice_prefix', 'general_sales_conditions',
                  'general_shipping_conditions',
                  'footer_company_information',)
