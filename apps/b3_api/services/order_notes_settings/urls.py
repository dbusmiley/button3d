from django.conf.urls import url

from apps.b3_api.services.order_notes_settings.views import \
    OrderNotesSettingsView


urlpatterns = [
    url(r'^$', OrderNotesSettingsView.as_view()),
]
