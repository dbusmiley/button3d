from django.conf.urls import url, include

from apps.b3_api.services.views import ListServices, ListServiceGrossFigures
from apps.b3_api.services.settings.views import ListServiceSettings, \
    ServiceLogoDetail

urlpatterns = [
    url(r'^$', ListServices.as_view()),
    url(
        r'^(?P<service_id>[0-9]+)/figures/net-volume/$',
        ListServiceGrossFigures.as_view()
    ),
    url(r'^(?P<service_id>[0-9]+)/settings/$', ListServiceSettings.as_view()),
    url(r'^(?P<service_id>[0-9]+)/logo/$', ServiceLogoDetail.as_view()),
    url(
        r'^(?P<service_id>[0-9]+)/discounts/',
        include('apps.b3_api.services.discounts.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/offers/',
        include('apps.b3_api.services.offers.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/projects/',
        include('apps.b3_api.services.projects.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/orders/',
        include('apps.b3_api.services.orders.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/order-pricing-rules/',
        include('apps.b3_api.services.order_pricing_rules.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/order-notes-settings/',
        include('apps.b3_api.services.order_notes_settings.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/parts/',
        include('apps.b3_api.services.parts.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/workflows/',
        include('apps.b3_api.services.workflows.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/sequences/',
        include('apps.b3_api.services.sequences.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/workstations/',
        include('apps.b3_api.services.workstations.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/jobs/',
        include('apps.b3_api.services.jobs.urls')
    ),
    url(
        r'^(?P<service_id>[0-9]+)/employees/',
        include('apps.b3_api.services.employees.urls'),
    ),
]
