from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models import WorkflowTemplate
from apps.b3_order.models import OrderLine

import button3d.type_declarations as td


class PartAssignWorkflowTemplateSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    workflow_template_id = serializers.IntegerField(write_only=True)

    def update(
        self, instance: td.OrderLine, validated_data: dict
    ) -> td.OrderLine:
        workflow_template_id = self.initial_data.get('workflowTemplateId')
        workflow_template = WorkflowTemplate.objects.get(
            id=workflow_template_id
        )

        instance.delete_old_sequences()
        instance.delete_old_workflow()

        workflow_template.generate_workflow_and_sequences(
            part_id=instance.id
        )
        return instance

    def validate(self, attrs: dict) -> dict:
        if WorkflowTemplate.objects.filter(
                id=self.initial_data['workflowTemplateId']).exists():
            return attrs

        raise serializers.ValidationError('Workflow template does not exist')

    class Meta:
        fields = ('id', 'workflow_template_id')
        model = OrderLine
