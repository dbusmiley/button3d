from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models import PartAttachment
from apps.b3_order.models import OrderLine

import button3d.type_declarations as td


class PartAttachmentCreateSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    uploader = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )
    part_id = serializers.PrimaryKeyRelatedField(
        source='part', queryset=OrderLine.objects.all())

    def create(self, validated_data: dict) -> td.PartAttachment:
        filesize = validated_data.get('file').size
        filename = validated_data.get('file').name
        validated_data['filesize'] = filesize
        validated_data['filename'] = filename
        return super().create(validated_data)

    class Meta:
        model = PartAttachment
        fields = (
            'uploader',
            'part_id',
            'file',
        )
