from rest_framework import serializers

from apps.b3_order.models import OrderLine
import button3d.type_declarations as td


class PartMoveToNextStepSerializer(serializers.ModelSerializer):

    def update(
        self, instance: td.OrderLine, validated_data: dict
    ) -> td.OrderLine:
        instance.move_all_sequences_to_next_status()
        return instance

    def validate(self, attrs: dict) -> dict:
        if self.instance.check_all_sequences_at_same_status():
            return attrs
        raise serializers.ValidationError('Not all sequences are at the same '
                                          'status, cannot move to next status')

    class Meta:
        fields = ('id',)
        model = OrderLine
