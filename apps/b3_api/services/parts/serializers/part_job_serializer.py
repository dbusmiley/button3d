from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.catalogue.serializers import ProductSerializer
from apps.b3_api.services.offers.post_processings.serializers.\
    part_post_processing_serializer import \
    PartPostProcessingSerializer
from apps.b3_mes.models import Job
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td

import typing as t


class PartJobSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    material = ProductSerializer(source='stock_record.product')
    post_processings = serializers.SerializerMethodField()
    order_date = serializers.DateTimeField(source='order.datetime_placed')
    sequences_all_possible = serializers.SerializerMethodField()
    sequences_assigned = serializers.SerializerMethodField()
    sequences_unassigned = serializers.SerializerMethodField()

    @staticmethod
    def get_post_processings(obj: td.OrderLine) -> t.Sequence[td.StrKeyDict]:
        return [
            PartPostProcessingSerializer(post_processing).data
            for post_processing in obj.post_processings
        ]

    def get_sequences_assigned(
        self, obj: td.OrderLine
    ) -> t.Sequence[td.Sequence]:
        job = self._get_job()
        possible_sequence_ids = obj.sequences.values_list('id', flat=True)
        return job.sequences.filter(id__in=possible_sequence_ids).values_list(
            'id', flat=True)

    def get_sequences_unassigned(self, obj):
        job = self._get_job()
        return obj.get_assignable_sequences(job.workstation.id)

    @staticmethod
    def get_sequences_all_possible(obj):
        return obj.sequences.values_list('id', flat=True)

    def _get_job(self) -> td.MESJob:
        job_id = self.context['job_id']
        return Job.objects.get(id=job_id)

    class Meta:
        model = OrderLine
        fields = (
            'thumbnail_url',
            'id',
            'name',
            'material',
            'order_date',
            'status',
            'post_processings',
            'sequences_unassigned',
            'sequences_assigned',
            'sequences_all_possible'
        )
