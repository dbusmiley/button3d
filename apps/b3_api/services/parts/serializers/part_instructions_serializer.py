from rest_framework import serializers

from apps.b3_order.models import OrderLine


class PartInstructionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = (
            'id',
            'instructions',
        )
