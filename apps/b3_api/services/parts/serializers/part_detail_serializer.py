from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.catalogue.serializers import ProductWithTechnologySerializer
from apps.b3_api.services.parts.serializers.part_attachment_serializer import \
    PartAttachmentSerializer
from apps.b3_api.users.serializers.user_part import UserPartSerializer
from apps.b3_core.serializers import PartStlFileSerializer
from apps.b3_order.models import OrderLine
from apps.b3_api.services.projects.attachments.serializers.\
    order_attachment_serializer import OrderAttachmentSerializer
from apps.b3_api.services.sequences.serializers.sequence_serializer import (
    SequenceSerializer
)
from apps.b3_api.services.workflows.serializers. \
    workflow_list_retrieve_serializer import \
    WorkflowListRetrieveSerializer
from apps.b3_api.services.offers.post_processings.serializers.\
    part_post_processing_serializer import PartPostProcessingSerializer
from apps.b3_api.services.orders.serializers.part_order_serializer import \
    PartOrderSerializer

import button3d.type_declarations as td

import typing as t


class PartDetailSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    file = PartStlFileSerializer(source='stl_file')
    material = serializers.SerializerMethodField()
    post_processings = serializers.SerializerMethodField()
    order = PartOrderSerializer()
    workflow = WorkflowListRetrieveSerializer()
    order_line_attachments = OrderAttachmentSerializer(
        many=True, source='attachments')
    part_attachments = PartAttachmentSerializer(many=True)
    sequences = SequenceSerializer(many=True)
    dimensions = serializers.SerializerMethodField()
    unit = serializers.CharField(source='configuration.unit')
    project_id = serializers.IntegerField(source='order.project.id')
    employees = UserPartSerializer(many=True)
    total_sequences = serializers.IntegerField(source='quantity')

    @staticmethod
    def get_material(obj: td.OrderLine) -> td.ProductWithTechnologySerializer:
        return ProductWithTechnologySerializer(obj.stock_record.product).data

    @staticmethod
    def get_dimensions(obj: td.OrderLine) -> dict:
        return {
            'x': obj.dimensions['w'],
            'y': obj.dimensions['h'],
            'z': obj.dimensions['d'],
        }

    @staticmethod
    def get_post_processings(obj: td.OrderLine) -> t.List:
        return [
            PartPostProcessingSerializer(post_processing).data
            for post_processing in obj.post_processings
        ]

    class Meta:
        model = OrderLine
        fields = (
            'id',
            'name',
            'file',
            'material',
            'post_processings',
            'order',
            'workflow',
            'order_line_attachments',
            'part_attachments',
            'sequences',
            'dimensions',
            'unit',
            'project_id',
            'status',
            'instructions',
            'employees',
            'total_sequences',
        )
        read_only_fields = fields
