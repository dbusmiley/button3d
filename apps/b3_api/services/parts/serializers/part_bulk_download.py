from django.conf import settings
from drf_payload_customizer.mixins import PayloadConverterMixin
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.b3_order.models import OrderLine

import jwt
from apps.b3_organization.utils import get_base_url
import datetime
from django.utils.http import urlencode


class PartBulkDownloadSerializer(
    PayloadConverterMixin,
    serializers.Serializer,
):
    url = serializers.CharField(read_only=True)
    jwt = serializers.JSONField(read_only=True)
    part_ids = serializers.ListField(
        child=serializers.IntegerField(), write_only=True)

    class Meta:
        fields = ('url', 'jwt', 'part_ids')

    def validate(self, attrs: dict) -> dict:
        service_id = self.context['service_id']
        parts_qs = OrderLine.objects.filter(order__partner__id=service_id)
        for part_id in self.initial_data['partIds']:
            if not parts_qs.filter(id=part_id).exists():
                raise ValidationError(
                    f'Part with id {part_id} does not exists or you do not '
                    f'have permissions to access it')
        return attrs

    def update(self, instance: None, validated_data: None) -> None:
        raise ValidationError('An update cannot be performed as this '
                              'is not a ModelSerializer')

    def create(self, validated_data: dict) -> dict:

        exp = getattr(settings, 'JSON_WEB_TOKEN_EXPIRATION_SECONDS', 30)
        payload = {
            'exp':
                datetime.datetime.utcnow() + datetime.timedelta(seconds=exp),
            'parts': [],
        }

        for part in OrderLine.objects.filter(
            id__in=self.initial_data['partIds']
        ):
            payload['parts'].append({
                'uuid': part.stl_file.uuid,
                'name': part.name
            })
        token = jwt.encode(payload, settings.SECRET_KEY)
        url_token = urlencode({'token': token})
        base_url = get_base_url()
        self.validated_data['url'] = f'{base_url}/bulk-download?{url_token}'
        self.validated_data['jwt'] = token
        return self.data
