from rest_framework import serializers

from apps.b3_api.services.parts.fields import EmployeePrimaryKeyRelatedField
from apps.b3_order.models import OrderLine

import button3d.type_declarations as td


class PartAddEmployeesSerializer(serializers.ModelSerializer):
    employees = EmployeePrimaryKeyRelatedField(many=True)

    def update(
        self, instance: td.OrderLine, validated_data: dict
    ) -> td.OrderLine:
        employees = validated_data.pop('employees')
        instance.employees.add(*employees)
        return super().update(instance, validated_data)

    class Meta:
        fields = ('id', 'employees',)
        model = OrderLine
