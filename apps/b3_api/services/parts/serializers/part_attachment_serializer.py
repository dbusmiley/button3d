from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.attachment.attachment_base_serializer import \
    AttachmentBaseSerializer
from apps.b3_mes.models import PartAttachment


class PartAttachmentSerializer(
    PayloadConverterMixin,
    AttachmentBaseSerializer
):

    class Meta:
        model = PartAttachment
        fields = AttachmentBaseSerializer.Meta.fields
