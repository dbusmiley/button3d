import typing as t
from rest_framework import serializers

from apps.b3_api.catalogue.serializers import ProductWithTechnologySerializer
from apps.b3_api.services.offers.post_processings.serializers.\
    part_post_processing_serializer import \
    PartPostProcessingSerializer
from apps.b3_api.users.serializers.user_part import UserPartSerializer
from apps.b3_order.models import OrderLine
from apps.b3_api.utils.mixins import \
    OldPayloadConverterMixin
from apps.b3_shipping.utils.get_delivery_range import get_delivery_range
import button3d.type_declarations as td


class PartListSerializer(
    OldPayloadConverterMixin,
    serializers.ModelSerializer
):
    material = serializers.SerializerMethodField()
    post_processings = serializers.SerializerMethodField()
    order_id = serializers.IntegerField(source='order.id')
    project_id = serializers.IntegerField(source='order.project.id')
    order_number = serializers.CharField(source='order.number')
    order_date = serializers.DateTimeField(source='order.datetime_placed')
    user_id = serializers.IntegerField(source='order.purchased_by.id')
    billing_address_id = serializers.IntegerField(
        source='order.billing_address.id')
    shipping_address_id = serializers.IntegerField(
        source='order.shipping_address.id'
    )
    company = serializers.CharField(
        source='order.billing_address.company_name')
    delivery_date = serializers.SerializerMethodField()
    workflow_name = serializers.CharField(source='workflow.name')
    employees = UserPartSerializer(read_only=True, many=True)

    @staticmethod
    def get_material(obj: td.OrderLine) -> td.ProductWithTechnologySerializer:
        return ProductWithTechnologySerializer(obj.stock_record.product).data

    @staticmethod
    def get_post_processings(
        obj: td.OrderLine
    ) -> t.Sequence[td.StringKeyDict]:
        return [
            PartPostProcessingSerializer(post_processing).data
            for post_processing in obj.post_processings
        ]

    @staticmethod
    def get_delivery_date(obj: td.OrderLine) -> t.Dict[str, str]:
        min_delivery_date, max_delivery_date = get_delivery_range(
            obj.order.lines.all(), obj.order.shipping_method
        )
        return {
            'minimum': f'{min_delivery_date} days',
            'maximum': f'{max_delivery_date} days',
        }

    class Meta:
        model = OrderLine
        fields = (
            'id',
            'name',
            'thumbnail_url',
            'material',
            'post_processings',
            'quantity',
            'order_id',
            'order_number',
            'order_date',
            'project_id',
            'user_id',
            'billing_address_id',
            'shipping_address_id',
            'status',
            'next_status',
            'company',
            'delivery_date',
            'workflow_name',
            'employees',
        )
        read_only_fields = fields
