from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models import WorkflowStatus
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td


class PartStatusUpdateSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    status_id = serializers.IntegerField()

    def validate(self, attrs: dict) -> dict:
        status_id = attrs.get('status_id')
        try:
            self.instance.workflow.statuses.get(id=status_id)
        except WorkflowStatus.DoesNotExist:
            raise ValidationError('This status does not belong '
                                  'to the workflow set for this part')
        return attrs

    def update(
        self, instance: td.OrderLine, validated_data: dict
    ) -> td.OrderLine:
        status_id = validated_data.get('status_id')
        status = WorkflowStatus.objects.get(id=status_id)
        for sequence in instance.sequences.all():
            sequence.status = status
            sequence.save()
        return instance

    def create(self, validated_data: dict) -> None:
        raise Exception('Cannot create part status since '
                        'it is a computed non-nullable value')

    class Meta:
        model = OrderLine
        fields = (
            'status_id',
        )
