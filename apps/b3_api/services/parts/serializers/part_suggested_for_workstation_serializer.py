from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.catalogue.serializers import ProductSerializer
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td

import typing as t


class PartSuggestedForWorkstationSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    material = ProductSerializer(source='stock_record.product')
    post_processings = serializers.SerializerMethodField()
    sequences_unassigned = serializers.SerializerMethodField()
    order_number = serializers.CharField(source='order.number')
    order_date = serializers.DateTimeField(source='order.datetime_placed')

    @staticmethod
    def get_post_processings(obj: td.OrderLine) -> t.Sequence[str]:
        return [
            post_processing.title
            for post_processing in obj.post_processings
        ]

    def get_sequences_unassigned(self, obj: td.OrderLine) -> t.Sequence[int]:
        workstation_id = self.context['workstation_id']
        return obj.get_assignable_sequences(workstation_id)

    class Meta:
        model = OrderLine
        fields = (
            'id',
            'name',
            'order_number',
            'material',
            'post_processings',
            'order_date',
            'sequences_unassigned',
            'thumbnail_url',
        )
