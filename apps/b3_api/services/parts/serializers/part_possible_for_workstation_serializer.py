from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin


from apps.b3_api.catalogue.serializers import ProductSerializer
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td

import typing as t


class PartPossibleForWorkstationSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    material = ProductSerializer(source='stock_record.product')
    post_processings = serializers.SerializerMethodField()
    sequences_unassigned = serializers.SerializerMethodField()
    sequences_suggested = serializers.SerializerMethodField()
    order_number = serializers.CharField(source='order.number')
    order_date = serializers.DateTimeField(source='order.datetime_placed')

    @staticmethod
    def get_post_processings(obj: td.OrderLine):
        return [
            post_processing.title
            for post_processing in obj.post_processings
        ]

    def get_sequences_unassigned(self, obj: td.OrderLine) -> t.Sequence[int]:
        workstation_id = self.context['workstation_id']
        return obj.get_assignable_sequences(workstation_id)

    @staticmethod
    def get_sequences_suggested(obj: td.OrderLine) -> t.List:
        return []

    class Meta:
        model = OrderLine
        fields = (
            'id',
            'name',
            'order_number',
            'material',
            'post_processings',
            'order_date',
            'sequences_unassigned',
            'sequences_suggested',
            'thumbnail_url',
        )
