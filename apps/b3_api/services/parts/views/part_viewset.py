from rest_framework.decorators import action
from rest_framework import viewsets, mixins

import typing as t
import button3d.type_declarations as td
from apps.b3_api.services.parts.serializers import PartListSerializer, \
    PartDetailSerializer
from apps.b3_api.services.parts.serializers.part_add_employees_serializer \
    import PartAddEmployeesSerializer
from apps.b3_api.services.parts.serializers.\
    part_assign_workflow_template_serializer import \
    PartAssignWorkflowTemplateSerializer
from apps.b3_api.services.parts.serializers.part_attachment_serializer \
    import PartAttachmentSerializer
from apps.b3_api.services.parts.serializers.part_bulk_download \
    import PartBulkDownloadSerializer
from apps.b3_api.services.parts.serializers.part_employees_serializer \
    import PartUpdateEmployeesSerializer
from apps.b3_api.services.parts.serializers.part_move_to_next_step_serializer \
    import PartMoveToNextStepSerializer
from apps.b3_api.services.parts.serializers.part_status_update_serializer \
    import PartStatusUpdateSerializer
from apps.b3_api.services.parts.serializers.part_instructions_serializer \
    import PartInstructionsSerializer
from apps.b3_api.services.views import BaseMESView
from apps.b3_order.models import OrderLine
from apps.b3_api.utils.mixins import BulkUpdateFailAllModelMixin


class PartViewset(
    BaseMESView,
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet,
    BulkUpdateFailAllModelMixin,
):
    lookup_url_kwarg = 'part_id'

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_partner().id
        return context

    def get_serializer_class(self) -> td.SerializerClass:
        if self.action == 'list':
            return PartListSerializer
        elif self.action == 'retrieve':
            return PartDetailSerializer
        elif self.action == 'update_status':
            return PartStatusUpdateSerializer
        elif self.action == 'update_instructions':
            return PartInstructionsSerializer
        elif self.action == 'update_employees':
            return PartUpdateEmployeesSerializer
        elif self.action == 'create_attachment':
            return PartAttachmentSerializer
        elif self.action == 'move_to_next_status':
            return PartMoveToNextStepSerializer
        elif self.action == 'assign_workflow_template':
            return PartAssignWorkflowTemplateSerializer
        elif self.action == 'bulk_add_employees':
            return PartAddEmployeesSerializer
        elif self.action == 'bulk_download':
            return PartBulkDownloadSerializer

    def get_queryset(self) -> t.Sequence[td.OrderLine]:
        return OrderLine.objects.select_related(
            'stock_record', 'order',
            'workflow').filter(
                order__partner_id=self.kwargs['service_id']
        )

    def get_object(self) -> td.OrderLine:
        return super(viewsets.GenericViewSet, self).get_object()

    @action(detail=True, methods=['put'])
    def update_status(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['put'])
    def update_instructions(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().update(request, *args, **kwargs)

    @action(detail=True, methods=['put'])
    def update_employees(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().update(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def create_attachment(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().create(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def move_to_next_status(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().bulk_update(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def assign_workflow_template(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().bulk_update(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def bulk_add_employees(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        return super().bulk_update(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def bulk_download(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)
