from apps.b3_api.services.parts.serializers.part_attachment_create_serializer \
    import PartAttachmentCreateSerializer
from apps.b3_api.services.views import BaseMESView
from apps.b3_mes.models import PartAttachment
from apps.b3_order.models import OrderLine
import button3d.type_declarations as td

from rest_framework import viewsets, mixins
import typing as t


class PartAttachmentViewset(
    BaseMESView,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    lookup_url_kwarg = 'part_attachment_id'
    serializer_class = PartAttachmentCreateSerializer

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.get_partner().id
        return context

    def get_queryset(self) -> t.Sequence[td.PartAttachment]:
        parts_ids = OrderLine.objects.filter(
            order__partner_id=self.kwargs['service_id']).values_list(
            'id', flat=True)
        return PartAttachment.objects.filter(part_id__in=parts_ids)

    def get_object(self) -> td.PartAttachment:
        return super(viewsets.GenericViewSet, self).get_object()
