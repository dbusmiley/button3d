from apps.b3_api.services.parts.serializers import PartDetailSerializer, \
    PartListSerializer
from apps.b3_api.services.parts.serializers.\
    part_assign_workflow_template_serializer import \
    PartAssignWorkflowTemplateSerializer
from apps.b3_api.services.parts.serializers.part_bulk_download import \
    PartBulkDownloadSerializer
from apps.b3_api.services.parts.serializers.part_employees_serializer import \
    PartUpdateEmployeesSerializer
from apps.b3_api.services.parts.serializers.part_instructions_serializer \
    import PartInstructionsSerializer
from apps.b3_api.services.parts.serializers.part_move_to_next_step_serializer \
    import PartMoveToNextStepSerializer
from apps.b3_api.services.parts.serializers.part_status_update_serializer \
    import PartStatusUpdateSerializer
from apps.b3_api.services.workstations.serializers. \
    workstation_possible_sequences_serializer import \
    WorkstationPossibleSequencesSerializer
from apps.b3_core.serializers import PartStlFileSerializer
from apps.b3_order.models import OrderLinePostProcessingOption
from apps.b3_tests.factories import PartnerFactory, UserFactory, \
    PostProcessingFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_core.middleware import THREAD_LOCALS
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory, PartAttachmentFactory, \
    WorkstationFactory
from apps.b3_order.factories import OrderFactory

from rest_framework.test import APIRequestFactory


class PartSerializerTests(TestCase):

    def setUp(self):
        super().setUp()
        self.service = PartnerFactory()
        user = UserFactory(username='user-0')
        for _ in range(3):
            self.order = OrderFactory(partner=self.service)
            self.part = self.order.lines.first()
            self.part.quantity = 3
            self.part.save()

            self.stl_file = self.part.stl_file

            PartAttachmentFactory(part=self.part, uploader=user)
            self.part.employees.add(user)

            workflow_template = WorkflowTemplateWithStatusesFactory(
                partner=self.service)
            workflow = workflow_template. \
                generate_workflow_and_sequences(self.part.id)

            self.workstation = WorkstationFactory(
                partner=self.service, is_printing=True)

            last_status = workflow.statuses.get(next=None)
            last_status.workstation = self.workstation
            last_status.save()

            status = workflow.get_first_status()
            status.workstation = self.workstation
            status.save()

        factory = APIRequestFactory()
        self.fake_request = factory.get('/')
        THREAD_LOCALS.current_request = self.fake_request

    def test_detail_serializer_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'file': {
                'name': 'string',
                'created': 'string',
                'thumbnailUrl': 'string',
                'uuid': 'string',
                'downloadLinks': {
                    'optimized': 'string',
                    'original': 'string',
                    'optimizedCtm': 'string',
                    'originalCtm': 'string',
                    'wta': 'string',
                    'supportStructure': 'string',
                    'texture': 'string',
                },
            },
            'material': {
                'id': 0,
                'title': 'string',
                'technology': {
                    'id': 0,
                    'name': 'string'
                }
            },
            'postProcessings': [0],
            'order': {'id': 0, 'number': 0},
            'workflow': {
                'id': 0,
                'name': 'string',
                'description': 'string',
                'statuses': [
                    {
                        'id': 0,
                        'name': 'string',
                        'nextId': 0,
                        'workstation': {'id': 0, 'name': 'string'},
                    }
                ],
            },
            'orderLineAttachments': [
                {
                    'id': 0,
                    'name': 'string',
                    'added': 'string',
                    'url': 'string',
                    'uploader': {
                        'id': 0,
                        'name': 'string',
                        'firstName': 'string',
                        'lastName': 'string',
                        'email': 'string',
                        'company': 'string',
                        'customerNumber': 'string',
                    },
                }
            ],
            'partAttachments': [
                {
                    'id': 0,
                    'name': 'string',
                    'added': 'string',
                    'url': 'string',
                    'uploader': {
                        'id': 0,
                        'name': 'string',
                        'firstName': 'string',
                        'lastName': 'string',
                        'email': 'string',
                        'company': 'string',
                        'customerNumber': 'string',
                    },
                }
            ],
            'sequences': [
                {
                    'id': 0,
                    'projectId': 0,
                    'orderNumber': 'string',
                    'orderId': 0,
                    'partId': 0,
                    'jobId': 0,
                    'status': 'string',
                    'nextStatus': 'string',
                    'lastUpdated': 'string',
                    'jobStatus': 'string',
                    'jobNumber': 'string',
                }
            ],
            'dimensions': {'x': 'string', 'y': 'string', 'z': 'string'},
            'unit': 'string',
            'projectId': 0,
            'status': 'string',
            'instructions': 'string',
            'employees': [
                {
                    'id': 'integer',
                    'name': 'string',
                    'firstName': 'string',
                    'lastName': 'string',
                    'email': 'string',
                    'avatarUrl': 'string',
                }
            ],
            'totalSequences': 0,
        }
        serializer_populated = PartDetailSerializer(
            self.part, context={'request': self.fake_request}
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_list_serializer_structure(self):
        structure_expected = {
            'id': 0,
            'name': 'string',
            'thumbnailUrl': 'string',
            'material': {
                'id': 0,
                'title': 'string',
                'technology': {
                    'id': 0,
                    'name': 'string'
                }
            },
            'postProcessings': [0],
            'quantity': 0,
            'orderId': 0,
            'orderNumber': 0,
            'orderDate': 'string',
            'projectId': 0,
            'userId': 0,
            'billingAddressId': 0,
            'shippingAddressId': 0,
            'status': 'string',
            'nextStatus': 'string',
            'company': 'string',
            'deliveryDate': {'minimum': 'string', 'maximum': 'string'},
            'workflowName': 'string',
            'employees': [{
                'id': 0,
                'name': 'string',
                'firstName': 'string',
                'lastName': 'string',
                'email': 'string',
                'avatarUrl': 'string',
            }, ],
        }
        serializer_populated = PartListSerializer(self.part)
        serializer_data = serializer_populated.data
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
        # This method always returns the correct keys, but may not return
        # the correct data. We usually get the number 6, but this depends on
        # factory defaults, so less than 20 is good enough. We really want
        # to see if this is an integer, since the method can return a tuple
        # of None's.
        tmp, __ = serializer_data['deliveryDate']['minimum'].split(' ')
        n_days = int(tmp, 10)
        self.assertLess(n_days, 20)

    def test_part_stl_file_structure(self):
        structure_expected = {
            'name': 'string',
            'created': 'string',
            'thumbnailUrl': 'string',
            'uuid': 'string',
            'downloadLinks': {
                'optimized': 'string',
                'original': 'string',
                'optimizedCtm': 'string',
                'originalCtm': 'string',
                'wta': 'string',
                'supportStructure': 'string',
                'texture': 'string',
            },
        }
        serializer_populated = PartStlFileSerializer(
            self.stl_file, context={'request': self.fake_request}
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_part_instructions_structure(self):
        structure_expected = {
            'id': 0,
            'instructions': 'string'
        }
        serializer_populated = PartInstructionsSerializer(
            self.part, context={'request': self.fake_request}
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_part_employees_structure(self):
        structure_expected = {
            'employees': [0]
        }
        serializer_populated = PartUpdateEmployeesSerializer(
            instance=self.part)
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_part_possible_for_workstation_structure(self):
        post_processing = PostProcessingFactory()
        OrderLinePostProcessingOption(
            post_processing=post_processing,
            line=self.part
        ).save()
        structure_expected = {
            'id': 0,
            'parts': [
                {
                    'id': 0,
                    'name': 'string',
                    'orderNumber': 0,
                    'material': 'string',
                    'postProcessings': ['string'],
                    'orderDate': 'string',
                    'sequencesUnassigned': [0],
                    'sequencesSuggested': [],
                    'thumbnailUrl': 'string',
                }
            ],
            'suggestedParts': [
                {
                    'id': 0,
                    'name': 'string',
                    'orderNumber': 0,
                    'material': 'string',
                    'postProcessings': ['string'],
                    'orderDate': 'string',
                    'sequencesUnassigned': [0],
                    'sequencesSuggested': [0],
                    'thumbnailUrl': 'string',
                }
            ],
        }
        serializer_populated = WorkstationPossibleSequencesSerializer(
            self.workstation,
            context={'service_id': self.service.id}
        )
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_part_move_to_next_step_structure(self):
        serializer_populated = PartMoveToNextStepSerializer(
            instance=self.part)
        structure_expected = {'id': 0}
        assert_serializer_structure(
            self,
            serializer_populated,
            structure_expected
        )

    def test_part_assign_workflow_template_structure(self):
        serializer_populated = PartAssignWorkflowTemplateSerializer(
            instance=self.part)
        structure_expected = {'id': 0}
        assert_serializer_structure(
            self,
            serializer_populated,
            structure_expected
        )

    def test_part_status_update_structure(self):
        serializer_populated = PartStatusUpdateSerializer(instance=self.part)
        structure_expected = {'statusId': 0}
        assert_serializer_structure(
            self,
            serializer_populated,
            structure_expected
        )

    def test_part_bulk_download_structure(self):
        serializer_populated = PartBulkDownloadSerializer(
            data={'partIds': [self.part.id]},
            context={'service_id': self.service.id}
        )
        serializer_populated.is_valid()
        serializer_populated.save()

        structure_expected = {
            'url': 'string',
            'jwt': 'string',
        }
        assert_serializer_structure(
            self,
            serializer_populated,
            structure_expected
        )
