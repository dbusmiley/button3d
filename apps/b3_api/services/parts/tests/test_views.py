import json

import jwt
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework import status

from apps.b3_mes.models import PartAttachment
from apps.b3_order.factories import OrderFactory
from apps.b3_order.models import OrderLine, Order
from apps.b3_tests.factories import PartnerFactory, UserFactory
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory, PartAttachmentFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class PartViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.service = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.service)

        self.num_parts = 5

        for i in range(self.num_parts):
            order = OrderFactory(partner=self.service)
            part = order.lines.first()
            part.quantity = 5
            part.save()
            workflow_template = WorkflowTemplateWithStatusesFactory(
                partner=self.service)
            workflow_template. \
                generate_workflow_and_sequences(part.id)

            user = UserFactory(username=f'user-{i}')
            PartAttachmentFactory(part=part, uploader=user)
            part.employees.add(user)

        self.order = Order.objects.first()
        self.part = self.order.lines.first()
        self.part_attachment = self.part.part_attachments.first()

        # urls & url kwargs
        self.part_list_url_kwargs = {'service_id': self.service.id}
        self.part_list_url = reverse(
            'api_urls:parts-list', kwargs=self.part_list_url_kwargs)

        self.part_detail_url_kwargs = {
            'service_id': self.service.id, 'part_id': self.part.id
        }
        self.parts_detail_url = reverse(
            'api_urls:parts-detail', kwargs=self.part_detail_url_kwargs)

        part_attachment_list_url_kwargs = {'service_id': self.service.id}
        self.part_attachment_list_url = reverse(
            'api_urls:part-attachments-list',
            kwargs=part_attachment_list_url_kwargs)

        self.part_attachment_detail_url_kwargs = {
            'service_id': self.service.id,
            'part_attachment_id': self.part_attachment.id
        }
        self.part_attachment_detail_url = reverse(
            'api_urls:part-attachments-detail',
            kwargs=self.part_attachment_detail_url_kwargs)

        self._setUp_move_to_next_status()

        self._setUp_update_instructions()

        self._setUp_status_update()

        self._setUp_update_employees()

        self._setUp_assign_workflow_template()

        self._setUp_bulk_add_employees()

        self._setUp_bulk_download()

    def _setUp_move_to_next_status(self):
        self.part_move_to_next_status_url = reverse(
            'api_urls:parts-move-to-next-status',
            kwargs=self.part_list_url_kwargs
        )
        payload = list(OrderLine.objects.all().values('id'))
        payload = json.dumps(payload)
        self.move_to_next_status_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

    def _setUp_update_instructions(self):
        self.new_instructions = 'New instructions'
        self.update_instructions_url = reverse(
            'api_urls:parts-update-instructions',
            kwargs=self.part_detail_url_kwargs)
        payload = {'instructions': self.new_instructions, }
        payload = json.dumps(payload)
        self.update_instructions_update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

    def _setUp_status_update(self):
        workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)
        workflow_template.generate_workflow_and_sequences(
            part_id=self.part.id)
        self.new_status = self.part.workflow.statuses.get(next=None)
        status_update_payload = {'statusId': self.new_status.id, }
        status_update_payload = json.dumps(status_update_payload)
        self.status_update_post_kwargs = {
            'data': status_update_payload,
            'content_type': 'application/json',
        }
        self.status_update_url = reverse(
            'api_urls:parts-update-status',
            kwargs=self.part_detail_url_kwargs)

    def _setUp_update_employees(self):
        self.updated_employees = []
        for i in range(5):
            user = UserFactory(username=f'user_for_update_employees_{i}')
            self.service.users.add(user)
            self.updated_employees.append(user.id)

        payload = {
            'employees': self.updated_employees,
        }
        payload = json.dumps(payload)
        self.update_employees_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        self.update_employees_url = reverse(
            'api_urls:parts-update-employees',
            kwargs=self.part_detail_url_kwargs)

    def _setUp_assign_workflow_template(self):
        self.part_assign_workflow_template_url = reverse(
            'api_urls:parts-assign-workflow-template',
            kwargs=self.part_list_url_kwargs
        )
        self.bulk_assign_workflow_template_id = \
            WorkflowTemplateWithStatusesFactory().id
        payload = [
            {
                'id': part_id,
                'workflowTemplateId': self.bulk_assign_workflow_template_id
            }
            for part_id in OrderLine.objects.all().values_list('id', flat=True)
        ]
        payload = json.dumps(payload)
        self.assign_workflow_template_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

    def _setUp_bulk_add_employees(self):
        self.new_employees = []
        for i in range(5):
            user = UserFactory(username=f'user_for_add_new_employees_{i}')
            self.service.users.add(user)
            self.new_employees.append(user.id)

        payload = [
            {
                'id': part.id,
                'employees': self.new_employees,
            }
            for part in OrderLine.objects.all()
        ]
        payload = json.dumps(payload)
        self.bulk_add_employees_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        self.bulk_add_employees_url = reverse(
            'api_urls:parts-bulk-add-employees',
            kwargs=self.part_list_url_kwargs)

    def _setUp_bulk_download(self):
        self.bulk_download_part_ids = list(OrderLine.objects.filter(
            order__partner__id=self.service.id).values_list('id', flat=True))
        payload = {'partIds': self.bulk_download_part_ids}
        payload = json.dumps(payload)
        self.bulk_download_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        self.bulk_download_url = reverse(
            'api_urls:parts-bulk-download',
            kwargs=self.part_list_url_kwargs
        )

    def test_retrieve(self):
        response = self.client.get(self.parts_detail_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        part = json.loads(response.content.decode('utf-8'))
        self.assertEqual(part.get('id'), self.part.id)

    def test_list(self):
        response = self.client.get(self.part_list_url)
        response_parts = json.loads(response.content.decode('utf-8'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response_parts), self.num_parts)

    def test_status_update(self):
        self.assertTrue(
            self.part.sequences.exclude(status=self.new_status).exists())
        response = self.client.put(
            self.status_update_url, **self.status_update_post_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(
            self.part.sequences.exclude(status=self.new_status).exists())

    def test_update_instructions(self):
        response = self.client.put(
            self.update_instructions_url,
            **self.update_instructions_update_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        part = OrderLine.objects.get(id=self.part.id)
        self.assertEqual(self.new_instructions, part.instructions)

    def test_update_employees(self):
        response = self.client.put(
            self.update_employees_url,
            **self.update_employees_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        employees = self.part.employees.all().values_list('id', flat=True)
        self.assertCountEqual(
            employees,
            self.updated_employees)

    def test_update_employees_empty_list(self):
        payload = {
            'employees': [],
        }
        payload = json.dumps(payload)
        self.update_employees_post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }
        response = self.client.put(
            self.update_employees_url,
            **self.update_employees_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(self.part.employees.all().exists())

    def test_delete_part_attachment(self):
        response = self.client.delete(self.part_attachment_detail_url)
        self.assertTrue(response.status_code, status.HTTP_204_NO_CONTENT)
        # The method will return 204 even if no attachment is found, but it
        # will put a message into the reponse body.
        self.assertEqual(response.content, b'')
        self.assertFalse(
            PartAttachment.objects.filter(id=self.part_attachment.id).exists()
        )

    def test_delete_part_attachment_invalid_id(self):
        """This is probably wrong and should be fixed + logic reversed."""
        kwargs = self.part_attachment_detail_url_kwargs.copy()
        kwargs['part_attachment_id'] = 9999999
        part_attachment_detail_url = reverse(
            'api_urls:part-attachments-detail', kwargs=kwargs
        )
        response = self.client.delete(part_attachment_detail_url)
        self.assertTrue(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            response.content, b'"No PartAttachment matches the given query."'
        )

    def test_create_part_attachment(self):
        filename = 'foo-bar'
        uploaded_file = SimpleUploadedFile(
            filename, b'Hello world',
            content_type='multipart/form-data')
        create_part_attachment_post_kwargs = {
            'file': uploaded_file,
            'partId': self.part.id
        }
        response = self.client.post(
            self.part_attachment_list_url,
            create_part_attachment_post_kwargs,
            format='multipart'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_move_to_next_status(self):
        response = self.client.post(
            self.part_move_to_next_status_url,
            **self.move_to_next_status_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_move_to_next_status_different_statuses(self):
        part = OrderLine.objects.first()
        sequence = part.sequences.first()
        sequence.status = sequence.status.next
        sequence.save()

        response = self.client.post(
            self.part_move_to_next_status_url,
            **self.move_to_next_status_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_assign_workflow_template(self):
        initial_workflow_id = OrderLine.objects.first().workflow.id
        response = self.client.post(
            self.part_assign_workflow_template_url,
            **self.assign_workflow_template_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(
            initial_workflow_id, OrderLine.objects.first().workflow.id)

    def test_bulk_add_employees(self):
        response = self.client.post(
            self.bulk_add_employees_url,
            **self.bulk_add_employees_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        new = set(self.new_employees)
        for part in OrderLine.objects.all():
            actual = set(part.employees.all().values_list('id', flat=True))
            self.assertTrue(new < actual)

    def test_bulk_download(self):
        response = self.client.post(
            self.bulk_download_url,
            **self.bulk_download_post_kwargs
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        decoded_response = jwt.decode(
            response.data['jwt'], settings.SECRET_KEY)

        parts_qs = OrderLine.objects.filter(id__in=self.bulk_download_part_ids)
        expected_response = {
            'exp': 1543933000,
            'parts': [
                {'uuid': part.stl_file.uuid, 'name': part.name}
                for part in parts_qs
            ]
        }

        self.assertEqual(
            decoded_response['parts'],
            expected_response['parts']
        )
        self.assertTrue('exp' in expected_response)
