from rest_framework import serializers

from apps.partner.models import Partner
from apps.b3_mes.models import Workstation

import button3d.type_declarations as td
import typing as t


class EmployeePrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> t.Sequence[td.Partner]:
        partner = Partner.objects.get(id=self.context['service_id'])
        return partner.users.all()


class WorkstationPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return Workstation.objects.filter(partner=self.context['service_id'])
