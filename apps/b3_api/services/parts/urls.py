from apps.b3_api.services.parts.views.part_viewset import PartViewset
from apps.b3_api.services.parts.views.part_attachment_viewset import \
    PartAttachmentViewset

from rest_framework import routers

router = routers.SimpleRouter()

router.register(
    r'part-attachments', PartAttachmentViewset, base_name='part-attachments')
router.register(r'', PartViewset, base_name='parts')

urlpatterns = router.urls
