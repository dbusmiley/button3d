import json

from apps.b3_tests.factories import PartnerFactory, SiteFactory, \
    OrganizationFactory, CountryFactory

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from unittest import expectedFailure


class ServiceTestSettingsUpdate(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceTestSettingsUpdate, self).setUp()
        self.site = SiteFactory()
        self.site.save()
        self.country = CountryFactory(
            alpha2='DE'
        )
        self.country.save()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.organization = OrganizationFactory(site=self.site)
        self.organization.save()

    def test_settings_has_address(self):
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/settings/',
        )

        project_response = json.loads(projects_response_raw.content)
        self.assertNotEqual(len(project_response['address']), 0)

    def test_settings_update_with_address(self):
        """Update printing service settings, including address"""
        post_data = {
            'id': self.partner.id,
            'name': self.partner.name,
            'logo': None,
            'website': 'http://test.example.com/',
            'description': self.partner.description,
            'address': {
                'firstName': 'First',
                'lastName': 'Last',
                'line1': 'Line1',
                'line2': 'Line2',
                'company': 'Company',
                'city': 'Berlin',
                'zipCode': '16453',
                'state': 'Berlin',
                'phoneNumber': '+493055578748',
                'country': 'DE'
            },
            'vatId': 'VAT',
            'taxType': 'Sales_Tax',
            'taxRate': 50.0,
            'currency': 'EUR',
            'additionalSupportedCurrencies': []
        }
        service_settings_update = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/settings/',
            data=json.dumps(post_data),
            content_type='application/json'
        )
        self.assertEqual(service_settings_update.status_code, 200)
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/settings/',
        )
        project_response = json.loads(projects_response_raw.content)
        self.assertEqual(project_response['address']['city'], 'Berlin')

    def test_settings_update_bad_post_code(self):
        """Do not accept a bad post code"""
        post_data = {
            'id': self.partner.id,
            'name': self.partner.name,
            'logo': None,
            'website': 'http://test.example.com/',
            'description': self.partner.description,
            'address': {
                'firstName': 'First',
                'lastName': 'Last',
                'line1': 'Line1',
                'line2': 'Line2',
                'company': 'Company',
                'city': 'Berlin',
                'postCode': '16453123123123',
                'state': 'Berlin',
                'phoneNumber': '+493055578748',
                'country': 'DE'
            },
            'vatId': 'VAT',
            'taxType': 'Sales_Tax',
            'taxRate': 50.0,
            'currency': 'EUR',
            'additionalSupportedCurrencies': []
        }
        expected_zip_code = self.partner.address.zip_code
        service_settings_update = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/settings/',
            data=json.dumps(post_data),
            content_type='application/json'
        )
        self.assertEqual(service_settings_update.status_code, 400)

        # TODO(felix) Check if the post code was changed. FE Change needed.
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/settings/',
        )
        project_response = json.loads(projects_response_raw.content)
        actual_postcode = project_response['address']['zipCode']
        self.assertEqual(expected_zip_code, actual_postcode)

    @expectedFailure
    def test_settings_update_empty_state(self):
        """Test says empty city but blanks state, then fails modified check"""
        # Now try with an empty city
        post_data = {
            'id': self.partner.id,
            'name': self.partner.name,
            'logo': None,
            'website': 'http://test.example.com/',
            'description': self.partner.description,
            'address': {
                'firstName': 'First',
                'lastName': 'Last',
                'line1': 'Line1',
                'line2': 'Line2',
                'company': 'Company',
                'city': 'Berlin',
                'postCode': '16453123123123',
                'state': None,
                'phoneNumber': '+493055578748',
                'country': 'DE'
            },
            'vatId': 'VAT',
            'taxType': 'Sales_Tax',
            'taxRate': 50.0,
            'currency': 'EUR',
            'additionalSupportedCurrencies': []
        }
        service_settings_update = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/settings/',
            data=json.dumps(post_data),
            content_type='application/json'
        )
        self.assertEqual(service_settings_update.status_code, 400)

        # Check if the post code was changed
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/settings/',
        )
        project_response = json.loads(projects_response_raw.content)
        self.assertEqual(project_response['address']['state'], 'Berlin')

    def test_settings_update_clear_description(self):
        """Clear a description by using empty field"""
        post_data = {
            'id': self.partner.id,
            'name': self.partner.name,
            'logo': None,
            'website': 'http://test.example.com/',
            'description': '''''',
            'address': {
                'firstName': 'First',
                'lastName': 'Last',
                'line1': 'Line1',
                'line2': 'Line2',
                'company': 'Company',
                'city': 'Berlin',
                'zipCode': '16453',
                'state': 'Berlin',
                'phoneNumber': '+493055578748',
                'country': 'DE'
            },
            'vatId': 'VAT',
            'taxType': 'Sales_Tax',
            'taxRate': 50.0,
            'currency': 'EUR',
            'additionalSupportedCurrencies': []
        }
        service_settings_update = self.client.put(
            f'/api/v2.0/services/{self.partner.id}/settings/',
            data=json.dumps(post_data),
            content_type='application/json'
        )
        self.assertEqual(service_settings_update.status_code, 200)
        # Check if the description was changed
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/settings/',
        )
        project_response = json.loads(projects_response_raw.content)
        self.partner.refresh_from_db()
        self.assertEqual('', self.partner.description)
        self.assertIsNone(project_response['description'])
