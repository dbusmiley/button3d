from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.permissions import IsServiceUser
from apps.b3_api.services.settings.serializers import ServiceSettingsSerializer
from apps.partner.models import Partner


class ListServiceSettings(RetrieveUpdateAPIView):
    serializer_class = ServiceSettingsSerializer
    permission_classes = (IsAuthenticated, IsServiceUser)
    lookup_url_kwarg = 'service_id'

    def get_queryset(self):
        return Partner.objects.all()
