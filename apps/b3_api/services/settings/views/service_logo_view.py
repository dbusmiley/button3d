from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.services.settings.serializers \
    import ServiceLogoDetailSerializer

from apps.b3_api.services.views.base_service_view import BaseServiceView


class ServiceLogoDetail(BaseServiceView, RetrieveUpdateAPIView):
    serializer_class = ServiceLogoDetailSerializer
