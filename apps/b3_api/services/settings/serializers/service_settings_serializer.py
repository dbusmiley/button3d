from apps.b3_address.serializers import AddressSerializer
from apps.partner.models import Partner
from rest_framework.exceptions import ValidationError
from rest_framework.fields import ListField

from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin


class ServiceSettingsSerializer(PayloadConverterMixin,
                                serializers.ModelSerializer):
    address = AddressSerializer(required=False)
    additional_supported_currencies = ListField()

    def validate(self, attrs):
        main_currency = attrs['price_currency']
        additional_currencies = attrs['additional_supported_currencies']
        if main_currency in additional_currencies:
            raise ValidationError({
                'currency': 'The main currency must not be part of the '
                            'additional supported currencies.'
            })

        return attrs

    def update(self, instance, validated_data):
        validated_address = validated_data.pop('address')
        validated_address['user'] = None
        AddressSerializer(
            data=instance.address, partial=True,
        ).update(
            instance=instance.address, validated_data=validated_address,
        )

        return super(ServiceSettingsSerializer,
                     self).update(instance, validated_data)

    class Meta:
        model = Partner
        field_mappings = {
            'vat_rate': 'tax_rate',
            'price_currency': 'currency',
        }
        fields = (
            'id', 'name', 'logo', 'website', 'address', 'vat_id', 'tax_type',
            'vat_rate', 'price_currency', 'additional_supported_currencies',
            'description'
        )
        read_only_fields = ('logo', )
