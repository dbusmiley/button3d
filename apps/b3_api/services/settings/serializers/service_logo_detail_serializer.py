from apps.partner.models import Partner
from rest_framework import serializers


class ServiceLogoDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Partner
        fields = ('logo', )
