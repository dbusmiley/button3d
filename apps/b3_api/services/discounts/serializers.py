from collections import OrderedDict
from numbers import Number

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.b3_api.utils.helpers import underscore_to_camelcase, \
    camelcase_to_underscore
from apps.partner.conclusionactions import PercentageDiscountAction, \
    FixedDiscountAction
from apps.partner.conditions import PriceGreaterCondition, \
    PriceSmallerCondition, PriceInRangeCondition, PriceOutsideRangeCondition, \
    QuantityGreaterCondition, QuantitySmallerCondition, \
    QuantityInRangeCondition, QuantityOutsideRangeCondition, \
    PriceQuantityProductGreaterCondition, \
    PriceQuantityProductSmallerCondition, \
    PriceQuantityProductInRangeCondition, \
    PriceQuantityProductOutsideRangeCondition
from apps.partner.models import AbstractRule
from apps.partner.models import PartnerRule

condition_mapping = {
    'price-greater': PriceGreaterCondition,
    'price-smaller': PriceSmallerCondition,
    'price-in-range': PriceInRangeCondition,
    'price-outside-range': PriceOutsideRangeCondition,
    'quantity-greater': QuantityGreaterCondition,
    'quantity-smaller': QuantitySmallerCondition,
    'quantity-in-range': QuantityInRangeCondition,
    'quantity-outside-range': QuantityOutsideRangeCondition,
    'price-quantity-greater': PriceQuantityProductGreaterCondition,
    'price-quantity-smaller': PriceQuantityProductSmallerCondition,
    'price-quantity-in-range': PriceQuantityProductInRangeCondition,
    'price-quantity-outside-range': PriceQuantityProductOutsideRangeCondition,
}

reverse_condition_mapping = {
    cls: name
    for name, cls in condition_mapping.items()
}


class DiscountSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartnerRule
        fields = ('id', 'conditions', 'conclusion_actions')

    def to_representation(self, instance):
        conditions = []
        for condition in instance.get_conditions():
            condition_dict = OrderedDict()
            condition_dict['arguments'] = self._get_condition_args(condition)
            condition_dict['type'] = self._get_condition_type(condition)
            conditions.append(condition_dict)

        action = OrderedDict()
        conclusion_action = instance.get_conclusion_actions()[0]
        action['type'] = self._get_action_type(conclusion_action)
        action['value'] = self._get_action_value(conclusion_action)

        rep = OrderedDict()
        rep['id'] = instance.id
        rep['conditions'] = conditions
        rep['action'] = action
        return rep

    def to_internal_value(self, data):
        validated, errors = self._validate_payload(data)
        if not validated:
            raise ValidationError(errors)

        rule = AbstractRule()
        for condition in data['conditions']:
            cond_type = condition['type']
            cond_args = condition['arguments']
            cond_obj = self._get_condition_object(cond_type, cond_args)
            rule.add_condition(cond_obj, commit=False)

        action_type = data['action']['type']
        action_value = data['action']['value']
        action_obj = self._get_action_object(action_type, action_value)
        rule.add_conclusion_action(action_obj, commit=False)

        value = OrderedDict()
        if 'id' in data:
            value['id'] = data['id']
        value['conditions'] = rule.conditions
        value['conclusion_actions'] = rule.conclusion_actions

        return super(DiscountSerializer, self).to_internal_value(value)

    @staticmethod
    def _get_condition_type(condition):
        return reverse_condition_mapping[condition.__class__]

    @staticmethod
    def _get_condition_args(condition):
        camel_dict = OrderedDict()
        for key, value in condition.args.items():
            camel_dict[underscore_to_camelcase(key)] = value
        return camel_dict

    @staticmethod
    def _get_action_type(action):
        if isinstance(action, PercentageDiscountAction):
            return 'percentage'
        elif isinstance(action, FixedDiscountAction):
            return 'fixed'

    @staticmethod
    def _get_action_value(action):
        if isinstance(action, PercentageDiscountAction):
            return action.args['rate'] * 100.0
        elif isinstance(action, FixedDiscountAction):
            return action.args['amount']

    @staticmethod
    def _get_condition_object(cond_type, args):
        underscore_args = OrderedDict()
        for key, value in args.items():
            underscore_args[camelcase_to_underscore(key)] = value

        for name, cls in condition_mapping.items():
            if cond_type == name:
                return cls(**underscore_args)

    @staticmethod
    def _get_action_object(action_type, value):
        if action_type == 'percentage':
            return PercentageDiscountAction(rate=value / 100.0)
        elif action_type == 'fixed':
            return FixedDiscountAction(amount=value)

    @staticmethod
    def _validate_payload(data):
        errors = {}
        _, condition_errors = DiscountSerializer._validate_conditions(data)
        _, action_errors = DiscountSerializer._validate_action(data)
        errors.update(condition_errors)
        errors.update(action_errors)

        return not errors, errors

    @staticmethod
    def _validate_conditions(data):
        if 'conditions' not in data:
            return False, {'conditions': 'This field is required.'}
        if not isinstance(data['conditions'], list):
            return False, {'conditions': 'value must be a list.'}

        errors = {}
        for i, condition in enumerate(data['conditions']):
            _, cond_errors = DiscountSerializer._validate_condition(condition)
            condition_field = f'conditions[{i}]'
            errors.update(
                {
                    f"{condition_field}.{key}": val
                    for key, val in cond_errors.items()
                }
            )
        return not errors, errors

    @staticmethod
    def _validate_condition(condition):
        if 'type' not in condition:
            return False, {'type': 'This field is required.'}
        cond_type = condition['type']
        if cond_type not in condition_mapping.keys():
            return False, {
                'type': f"'{cond_type}' is not a valid condition type."}
        if 'arguments' not in condition:
            return False, {'arguments': 'This field is required.'}

        cond_args = condition['arguments']
        if not isinstance(cond_args, dict):
            return False, {'arguments': 'value must be a dictionary.'}

        errors = {}
        required_args = condition_mapping[cond_type].required_args
        for under_key in required_args:
            camel_key = underscore_to_camelcase(under_key)
            if camel_key not in cond_args:
                error = f"argument '{camel_key}' is required " \
                        f"for condition type '{cond_type}'"
                errors['arguments'] = error
            value = cond_args[camel_key]

            if not isinstance(value, Number):
                field = f'arguments.{camel_key}'
                errors[field] = f"'{value}' is not a (positive) number."

        return not errors, errors

    @staticmethod
    def _validate_action(data):
        percentage_action_error = 'For a discount of type \'percentage\', ' \
                                  'this value must be between 0 and 100'

        if 'action' not in data:
            return False, {'action': 'This field is required.'}
        elif not isinstance(data['action'], dict):
            return False, {'action': 'value must be a dictionary.'}

        errors = {}
        if 'type' not in data['action']:
            action_type = None
            errors['action.type'] = 'This field is required.'
        else:
            action_type = data['action']['type']
            if action_type not in ['fixed', 'percentage']:
                errors['action.type'] = f"'{action_type}' is not a " \
                                        f"valid action type."

        if 'value' not in data['action']:
            errors['action.value'] = 'This field is required.'
        else:
            action_value = data['action']['value']
            if not isinstance(action_value, Number) or action_value < 0:
                errors['action.value'] = f"'{action_value}' is not " \
                                         f"a (positive) number."
            elif action_type == 'percentage' and action_value > 100:
                return False, {'action.value': percentage_action_error}

        return not errors, errors
