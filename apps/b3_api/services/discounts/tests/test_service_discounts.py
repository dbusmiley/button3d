"""
Test /api/v2.0/services/{serviceId}/discounts endpoints.
Documentation at: http://deepthought.3yourmind.com/button3d/documentation/
features/python/discounts/#adding-a-discount-rule
closely related to apps/basket/_tests/test_basket_discounts.py
@author: tt@3yourmind.com
"""
import json

from apps.b3_address.factories import CountryFactory, AddressFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import StlFileFactory, PartnerFactory, \
    create_product, create_stockrecord, BasketFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.basket.models import Basket


class ServiceDiscountsTest(AuthenticatedTestCase):
    def create_priced_basket(self):
        basket = BasketFactory(status=Basket.OPEN, owner=self.user)
        basket.add_product(
            product=self.product,
            stockrecord=self.stock_record,
            stlfile=self.stl_file,
            quantity=1
        )
        return basket

    def fetch_discount_data(self):
        service_get_discounts = self.client.get(
            self.discount_url
        )
        project_response = json.loads(service_get_discounts.content)

        return project_response

    def setUp(self):
        super(ServiceDiscountsTest, self).setUp()

        self.stl_file = StlFileFactory(showname='The Stl Model')
        self.partner = PartnerFactory(name='The Company')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

        germany = CountryFactory(
            alpha2='DE',
        )
        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
        )
        self.user_address = AddressFactory(
            user=self.user,
            country=germany
        )
        self.offer_id = self.stock_record.id
        self.service_id = self.partner.id
        self.discount_url = f'/api/v2.0/services/{self.service_id}' \
            f'/offers/{self.offer_id}/discounts/'

        self.basket_1 = self.create_priced_basket()

    def test_adding_in_new_discounts(self):
        # First check if rules exist
        discount_resp = self.fetch_discount_data()
        self.assertEqual(discount_resp, [])

        # Now add a new discount
        new_price_dict = {
            "conditions":
                [{
                    "type": 'price-greater',
                    "arguments": {
                        "maxPrice": 15
                    }
                }],
            "action": {
                "type": 'fixed',
                "value": 10}
        }
        service_update_discounts = self.client.post(
            self.discount_url,
            data=json.dumps(new_price_dict),
            content_type='application/json'
        )
        self.assertEqual(
            service_update_discounts.data["conditions"],
            new_price_dict["conditions"]
        )
        self.assertEqual(
            service_update_discounts.data["action"], new_price_dict["action"]
        )
        self.assertEqual(service_update_discounts.status_code, 201)

        # Check if the new rule was applied the default price is 120.86 and
        # this should get applied
        original_price = self.basket_1.total_excl_tax
        self.basket_1.refresh_price()
        self.assertTrue(original_price - self.basket_1.total_excl_tax, 10)

        # test fetching this discount now
        discount_resp = self.fetch_discount_data()
        self.assertEqual(len(discount_resp), 1)
        self.assertEqual(
            discount_resp[0]['conditions'],
            new_price_dict['conditions']
        )
        self.assertEqual(
            discount_resp[0]['action'],
            new_price_dict['action']
        )

    def test_delete_discounts(self):
        # First check if rules exist
        discount_resp = self.fetch_discount_data()
        self.assertEqual(discount_resp, [])

        # Now add a new discount
        new_price_dict = {
            "conditions":
                [{
                    "type": 'price-greater',
                    "arguments": {
                        "maxPrice": 15
                    }
                }],
            "action": {
                "type": 'fixed',
                "value": 10}
        }
        service_update_discounts = self.client.post(
            self.discount_url,
            data=json.dumps(new_price_dict),
            content_type='application/json'
        )
        response_json = json.loads(service_update_discounts.content)
        del response_json['id']
        expected = {"action": {"type": 'fixed', "value": 10},
                    "conditions": [{"arguments": {"maxPrice": 15},
                                    "type": 'price-greater'}]}
        self.assertEqual(response_json, expected)

        # now delete them
        service_delete_discounts = self.client.delete(
            self.discount_url
        )
        self.assertEqual(service_delete_discounts.status_code, 204)

        # at last check if rules exist
        discount_resp = self.fetch_discount_data()
        self.assertEqual(discount_resp, [])
