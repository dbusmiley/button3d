"""
Test /api/v2.0/services/{serviceId}/discounts endpoints.
Documentation at: http://deepthought.3yourmind.com/button3d/documentation/
features/python/discounts/#adding-a-discount-rule
closely related to apps/basket/_tests/test_basket_discounts.py
@author: tt@3yourmind.com
"""
import json

from apps.b3_tests.factories import AddressFactory, BasketFactory
from apps.b3_tests.factories import CountryFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import StlFileFactory, PartnerFactory, \
    create_product, create_stockrecord
from apps.basket.models import Basket
from apps.partner.models import PartnerRule


class ServiceDiscountsTest(AuthenticatedTestCase):
    def create_priced_basket(self):
        basket = BasketFactory(status=Basket.OPEN, owner=self.user)
        basket.add_product(
            product=self.product,
            stockrecord=self.stock_record,
            stlfile=self.stl_file,
            quantity=1
        )
        return basket

    def fetch_discount_detail(self, service_id, rule_id):
        service_get_discounts = self.client.get(
            '/api/v2.0/services/%d/discounts/%d/' % (service_id, rule_id)
        )
        project_response = json.loads(service_get_discounts.content)

        return project_response

    def update_discount_detail(self, service_id, rule_id, data):
        service_update_discount = self.client.put(
            '/api/v2.0/services/%d/discounts/%d/' % (self.partner.id, rule_id),
            data=json.dumps(data),
            content_type="application/json"
        )
        return service_update_discount

    def setUp(self):
        super(ServiceDiscountsTest, self).setUp()

        self.stl_file = StlFileFactory(showname='The Stl Model')
        self.partner = PartnerFactory(name='The Company')
        self.partner.users.add(self.user)
        self.partner.save()
        self.product = create_product()
        self.finish = create_product()
        self.stock_record = create_stockrecord(
            child_product=self.finish, partner=self.partner, is_default=True
        )

        germany = CountryFactory(
            alpha2='DE',
        )
        self.shipping_method = ShippingMethodFactory(
            partner=self.partner,
        )
        self.user_address = AddressFactory(
            user=self.user,
            country=germany
        )

        self.basket_1 = self.create_priced_basket()

        # also create a sample rule here
        # Now add a new discount
        new_discount_dict = {
            "conditions":
            [{
                "type": "price-greater",
                "arguments": {
                    "maxPrice": 15
                }
            }],
            "action": {
                "type": "fixed",
                "value": 10
            }
        }
        self.client.post(
            '/api/v2.0/services/%d/discounts/' % (self.partner.id),
            data=json.dumps(new_discount_dict),
            content_type="application/json"
        )

        self.rule_id = PartnerRule.objects.all()[0].id

    def test_fetching_detail_of_rule(self):
        self.assertNotEqual(
            self.fetch_discount_detail(self.partner.id, self.rule_id), []
        )

    def test_updating_details_of_rule(self):
        updated_rule_json = {
            "conditions":
            [{
                "type": "price-greater",
                "arguments": {
                    "maxPrice": 25
                }
            }],
            "action": {
                "type": "percentage",
                "value": 10
            }
        }

        response = self.update_discount_detail(
            self.partner.id, self.rule_id, updated_rule_json
        )
        self.assertEqual(
            response.data["conditions"], updated_rule_json["conditions"]
        )
        self.assertEqual(response.data["action"], updated_rule_json["action"])

        # Check if data was updated
        rule_data_raw = self.fetch_discount_detail(
            self.partner.id, self.rule_id
        )
        self.assertEqual(
            rule_data_raw['conditions'][0]['arguments']['maxPrice'], 25
        )
