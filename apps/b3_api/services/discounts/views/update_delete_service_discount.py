from django.http import Http404
from rest_framework.generics import RetrieveUpdateDestroyAPIView

from apps.b3_api.services.discounts.serializers import DiscountSerializer
from apps.b3_api.services.views import BaseServiceView
from apps.partner.models import PartnerRule


class UpdateDeleteServiceDiscount(
    BaseServiceView, RetrieveUpdateDestroyAPIView
):
    serializer_class = DiscountSerializer

    def get_object(self):
        try:
            return PartnerRule.objects.get(
                pk=self.kwargs['discount_id'],
                partner__id=self.kwargs['service_id'],
                conclusion="discount"
            )
        except PartnerRule.DoesNotExist:
            raise Http404
