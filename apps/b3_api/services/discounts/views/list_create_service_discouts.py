from rest_framework import status
from rest_framework.generics import ListCreateAPIView
from rest_framework.response import Response

from apps.b3_api.services.discounts.serializers import DiscountSerializer
from apps.b3_api.services.views import BaseServiceView
from apps.partner.models import PartnerRule


class ListCreateServiceDiscounts(BaseServiceView, ListCreateAPIView):
    serializer_class = DiscountSerializer

    def get_queryset(self):
        return PartnerRule.objects.filter(
            partner=self.get_object(), conclusion="discount"
        )

    def perform_create(self, serializer):
        serializer.save(partner=self.get_object(), conclusion="discount")

    def delete(self, request, *args, **kwargs):
        partner = self.get_object()
        partner.clear_discount_rules()
        return Response(status=status.HTTP_204_NO_CONTENT)
