from django.conf.urls import url

from apps.b3_api.services.discounts.views import ListCreateServiceDiscounts,\
    UpdateDeleteServiceDiscount

urlpatterns = [
    url(r'^$', ListCreateServiceDiscounts.as_view()),
    url(r'^(?P<discount_id>[0-9]+)/$', UpdateDeleteServiceDiscount.as_view())
]
