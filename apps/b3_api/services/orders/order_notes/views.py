import json
import logging
import typing as t
from collections import namedtuple
from decimal import Decimal
from pathlib import Path

from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.files import File
from django.http import (HttpResponse, Http404, JsonResponse, HttpRequest)
from django.template.loader import get_template
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views import generic
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveDestroyAPIView
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from apps.b3_api.errors.api_responses import error_got_wrong_value
from apps.b3_api.services.orders.order_notes import serializers
from apps.b3_api.services.orders.serializers.service_order_line_serializer \
    import LineSerializer
from apps.b3_api.services.orders.views.base_order_view import BaseOrderView
from apps.b3_api.services.orders.views.mixins import OrderMixin
from apps.b3_api.services.views.mixins import ServiceMixin
from apps.b3_core.models.base import WrappedTemplate
from apps.b3_core.utils import quantize, build_secure_download_url
from apps.b3_order.models import Order
from apps.b3_order.models import order_notes
from apps.b3_organization.models import Organization
from apps.b3_organization.utils import get_current_org
from apps.b3_pdf.models.templates import OrderNoteTemplate
from apps.b3_pdf.renderer.base import PdfTemplateHandler
from apps.b3_pdf.renderer.base import TemplateInterface
from apps.b3_pdf.renderer.django import DjangoStaticFetcher
from apps.b3_pdf.renderer.weasyprint import PdfService
from apps.catalogue.models.product import Product
from apps.partner.models.partner import Partner
from button3d import type_declarations as td

__all__ = (
    'OrderNoteTemplateResponse',
    'PdfPreviewDownloadView',
    'OrderConfirmationListCreate',
    'OrderConfirmationDetailView',
    'OrderConfirmationPreviewCreateView',
    'InvoiceListCreate',
    'InvoiceDetailView',
    'DeliveryNoteListCreate',
    'DeliveryNoteDetailView',
)

logger = logging.getLogger(__name__)
PriceAttrs = namedtuple('PriceAttrs', ('backend', 'frontend'))
PriceAttrsType = t.NamedTuple('PriceAttrs', backend=str, frontend=str)


class InvalidLineIdError(ValueError):
    def __init__(self, bad_value: t.Any = None, expected: t.Any = 1,
                 *args, **kwargs):
        self.bad_value = bad_value
        self.expected = expected
        super().__init__(*args, **kwargs)


class OrderNoteTemplateResponse(TemplateResponse):
    def __init__(self,
                 organization: t.Optional[Organization] = None,
                 partner: t.Optional[Partner] = None, *args, **kwargs):
        self.organization = organization
        self.partner = partner

        super().__init__(*args, **kwargs)

    @property
    def rendered_content(self):
        filter_kwargs = {}
        if self.organization:
            filter_kwargs['org_id'] = self.organization.pk
        if self.partner:
            filter_kwargs['partner_id'] = self.partner.pk

        template = OrderNoteTemplate.objects.get_template()

        return template.render(self.content)


class DownloadView(generic.View):
    content_type: str = 'application/pdf'
    file_extension: str = 'pdf'
    storage_path: Path = None
    filename_kwarg = 'filename'
    file_open_mode = 'rb'

    def get_file_extension(self):
        return self.file_extension

    def get_filename(self):
        basename = self.kwargs[self.filename_kwarg]
        extension = self.get_file_extension()
        return f'{basename}.{extension}'

    def get_storage_path(self) -> Path:
        return self.storage_path

    def get_file_path(self) -> Path:
        return self.get_storage_path() / self.get_filename()

    def get_file_contents(self) -> t.AnyStr:
        file_path = self.get_file_path()
        with file_path.open(mode=self.file_open_mode) as f:
            content = f.read()

        return content

    def get(self, request, *args, **kwargs) -> td.DjangoResponse:
        return HttpResponse(
            self.get_file_contents(), content_type=self.content_type
        )


# XXX: Needs permission checks
class PdfPreviewDownloadView(DownloadView):
    storage_path: Path = Path(settings.BASE_DIR, 'tmp', 'pdf')

    def get(self, *args, **kwargs):
        file_path = self.get_file_path()
        contents = self.get_file_contents()

        file_path.unlink()
        return HttpResponse(content=contents, content_type=self.content_type)


# XXX: Needs permission checks
class PdfDownloadView(ServiceMixin, DownloadView):
    # permission_classes =
    storage_path: Path = Path(settings.MEDIA_ROOT, 'pdf', 'order_notes')

    def get_storage_path(self) -> Path:
        service: Partner = self.get_service()
        return self.storage_path / service.slug

    def get_document_owner(self):
        return self.get_service()

    def get_file_contents(self):
        filename = self.get_filename()
        with open(filename, 'rb') as pdf:  # type: t.BinaryIO
            contents = pdf.read()
            pdf.close()

        return contents


def calculate_volume(dimensions: t.Sequence[float]) -> Decimal:
    """Calculates the volume and quantizes to 6 decimal precision."""
    width, height, depth = dimensions
    volume = Decimal(width) * Decimal(height) * Decimal(depth)
    return volume.quantize(Decimal('0.000001'))


def calculate_item_weight_from_line(line: td.OrderLine) -> Decimal:
    """
    Calculates the weight of a line item and quantizes to 6 decimal precision.
    This is the weight of the bounding box, which can be siginificantly more
    then what we are shipping. However, it would also account for filling
    materials and the box, so the estimate would even out.

    :raises ValueError:\
        If the material does not have it's maximum density set.
    """
    material: Product = line.product
    density = Decimal(material.attr_density_max).quantize(Decimal('0.000001'))
    volume = calculate_volume(line.dimensions.values())
    # Density is in grams / cm3, while dimensions are in mm
    # We return in grams
    return density * volume / 1000


def get_price_attributes(organization=None) -> PriceAttrsType:
    """
    Returns a named tuple with the attribute names for the backend and
    frontend respectively, that is in accordance with the Organization's
    pricing strategy.
    """
    organization = organization or get_current_org()
    if organization.show_net_prices:
        return PriceAttrs(backend='excl_tax', frontend='exclusiveTax')
    else:
        return PriceAttrs(backend='incl_tax', frontend='inclusiveTax')


LineInfo = t.Tuple[td.StringKeyDict, t.List[td.StringKeyDict]]


def _process_order_lines(order: Order, request: td.HttpRequest,
                         price_attrs: PriceAttrsType) -> LineInfo:
    """
    Process and return order lines and totals from those order lines

    We provide the line data as an array of dictionaries prepared by
    :class:`BasketLineSerializer`.
    In addition to the serializer's work, we:

        - Change 'prices', to align with the price strategy and prevent
          accidentally picking the wrong one.
        - Add weight: calculated weight estimation
        - Add weight_unit: unit of the weight (g, or Kg)
        - Add technology on offer
        - Add measure_unit: unit for dimensions

    :returns: A 2-tuple:

        - A dictionary of totals information with the following keys:
            - weight: total weight of the order
            - weight_unit: unit of the total weight
            - total_weight_decimals: number of decimals to format when
              displaying the weight
        - Array of line data
    """
    order_lines, total_weight, total_weight_unit = [], 0, 'g'
    serializer_context = {'request': request}
    for line in order.lines.all():  # type: td.OrderLine
        # A line is the model instance. The line_data is the serialized
        # dict of that line instance. We mostly want to pass the
        # serialized data to the template, but we do some processing on that
        # line data as outlined above.
        line_data = LineSerializer(line, context=serializer_context).data
        quantity = line.quantity
        weight = calculate_item_weight_from_line(line) * quantity
        total_weight += weight
        unit, weight_decimals = 'g', 0
        if weight > 99.9:
            weight /= Decimal(1000.0)
            unit = 'kg'
            weight_decimals = 1

        unit_price = line_data.pop('prices')[price_attrs.frontend]
        discount_price = line_data.pop('discount')[price_attrs.frontend]
        line_price = quantity * unit_price
        prices = {
            'currency': order.currency,
            'unitPrice': unit_price,
            'discount': discount_price,
            'linePrice': line_price,
        }
        line_data['offer']['technology'] = line.product.technology.title
        backend3d = getattr(settings, 'BACKEND_3D_HOST', None)
        thumbnail = build_secure_download_url(
            line.stl_file.uuid, file='thumbnail.png', base_url=backend3d
        )
        line_data.update(
            weight=weight, weightUnit=unit, weightDecimals=weight_decimals,
            measureUnit=line.configuration.unit, prices=prices,
            thumbnail=thumbnail,
        )
        order_lines.append(line_data)

    total_weight_decimals = 0
    if total_weight > 99.9:
        total_weight /= Decimal(1000)
        total_weight_unit = 'kg'
        total_weight_decimals = 1

    totals = {
        'weight': total_weight,
        'weight_unit': total_weight_unit,
        'total_weight_decimals': total_weight_decimals,
    }

    return totals, order_lines


# noinspection PyAttributeOutsideInit
class PdfFromTemplateMixin(PdfTemplateHandler,
                           generic.base.TemplateResponseMixin,
                           generic.base.ContextMixin, OrderMixin):
    template_name = NotImplemented
    output_path = NotImplemented
    note_prefix_attribute = NotImplemented
    note_model = None
    serializer_class: t.Type[serializers.BaseOrderNoteSerializer] = None
    is_preview = False
    note_name = NotImplemented
    note_pk_kwarg = None
    pdf_download_viewname = None
    document_extension = 'pdf'
    document_path: str = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._order, self._partner, self.note_object = None, None, None
        self.kwargs: dict = {}
        self.request: td.DjangoAuthenticatedRequest = None
        self.note_instance: order_notes.BaseOrderNote = None

    def handle_incoming_data(self, data: td.StringKeyDict):
        pass

    def get_template(self) -> TemplateInterface:
        template_name = self.get_template_names()[0]
        return WrappedTemplate(template_obj=get_template(template_name))

    def get_output_path(self) -> str:
        if self.output_path is None:
            path = Path(settings.BASE_DIR, 'tmp', 'pdf')
            self.output_path = str(path)
            try:
                path.mkdir(parents=True)
            except FileExistsError:
                pass

        return self.output_path

    def get_partner(self) -> Partner:
        if not self._partner:
            try:
                self._partner = Partner.objects.get(
                    pk=self.kwargs['service_id']
                )
            except Partner.DoesNotExist:
                raise PermissionError('No such partner')

        return self._partner

    def get_document_owner(self):
        return self.get_partner()

    def make_document_path(self):
        if not self.document_path:
            return
        path = Path(settings.MEDIA_ROOT, self.document_path)
        try:
            path.mkdir(parents=True)
        except FileExistsError:
            pass

    def get_note_instance(self) -> td.BaseOrderNote:
        note_id = self.kwargs[self.note_pk_kwarg]
        return self.note_model.objects.get(pk=note_id)

    def get_note_prefix(self) -> t.Optional[str]:
        if not self.is_preview:
            return None

        partner = self.get_partner()
        note_prefix = getattr(partner, self.note_prefix_attribute)
        if not note_prefix:
            # noinspection PyProtectedMember
            prefix_field = partner._meta.get_field(
                self.note_prefix_attribute
            )
            note_prefix = prefix_field.default

        return note_prefix

    def get_partner_info(self) -> td.StringKeyDict:
        partner = self.get_partner()
        try:
            logo_url = partner.logo.url
        except ValueError:
            logo_url = static('images/logo3yd.png')

        ps_address = partner.address

        sender_address_line = f'{ps_address.line1}, '
        if ps_address.line2:
            sender_address_line += f'{ps_address.line2}, '

        sender_address_line += f'{ps_address.postcode}, {ps_address.city}'

        page_footer = partner.footer_company_information or ''
        general_sales_conditions = getattr(
            partner, 'general_sales_conditions', ''
        )
        general_shipping_conditions = getattr(
            partner, 'general_shipping_conditions', ''
        )
        return {
            'service': partner,
            'service_address': ps_address,
            'sender_address_line': sender_address_line,
            'logo_url': logo_url,
            'page_footer': page_footer,
            'general_sales_conditions': general_sales_conditions,
            'general_shipping_conditions': general_shipping_conditions,
        }

    def get_context_data(self, **kwargs):
        """
        Collect order information for the template.

        In the new order model we always have a billing address, but not
        always a shipping address. We do however have either a shipping
        address or a pickup location.

        :param kwargs: context data passed in from view
        :return:
        """
        context = super().get_context_data(**kwargs)
        if self.is_preview:
            context.update(note_id='XXXXXX', note_obj=None)
        else:
            context.update(note_id=self.note_instance.pk,
                           note_obj=self.note_instance)

        context.update(self.get_partner_info())
        if 'note_prefix' not in context:
            if not self.is_preview:
                # Normally we pass in note_prefix and already have it if
                # it's not a preview.
                logger.error('No note prefix but is_preview is False!')
                return None
            context.update(note_prefix=self.get_note_prefix())
        order_obj: Order = self.get_order()

        price_attrs = get_price_attributes(order_obj.site.organization)
        totals, order_lines = _process_order_lines(
            order=order_obj, request=self.request, price_attrs=price_attrs
        )

        service_user_name = str(self.request.user)
        order_date = order_obj.datetime_placed.date()
        customer_reference = order_obj.customer_reference or '-'

        subtotal = getattr(order_obj.subtotal, price_attrs.backend)
        fee = quantize(getattr(order_obj.fees_total, price_attrs.backend))
        shipping_price = getattr(order_obj.shipping_price, price_attrs.backend)
        min_diff = getattr(order_obj.minimum_price_fee, price_attrs.backend)

        # Apply variables to template
        context.update({
            'note_name': self.note_name,
            'billing_address': order_obj.billing_address,
            'shipping_address': order_obj.shipping_address,
            'company': order_obj.shipping_address.company_name if
            order_obj.shipping_address else None,
            'is_pickup': order_obj.pickup_location is not None,
            'order_date': order_date,
            'order_obj': order_obj,
            'order_lines': order_lines,
            'delivery_time': order_obj.delivery_days_range_pretty_printed,
            'customer_reference': customer_reference,
            'service_user': self.request.user,
            'service_user_name': service_user_name,
            'total_weight': totals['weight'],
            'total_weight_unit': totals['weight_unit'],
            'total_weight_decimals': totals['total_weight_decimals'],
            'subtotal': subtotal,
            'fee': fee,
            'difference_to_minimum': min_diff,
            'voucher_price': None,
            'shipping_price': shipping_price,
            'total_prices': order_obj.total,
            'tax': order_obj.total.tax,
            'tax_rate': order_obj.total.tax_rate,
            'comments': order_obj.basket.comments.order_by('-created'),
            # Code for future enhancement via service panel
            'theme_font': 'Open Sans',
            'theme_date_format': 'd.m.Y',
        })

        return context

    def _get_pdf_service(self, context: td.StringKeyDict) -> PdfService:
        hostname = self.request.get_host()

        request_uri = self.request.get_full_path()
        base_url = f'{self.request.scheme}://{hostname}{request_uri}'
        return PdfService(
            self, context, base_url=base_url,
            resource_fetcher=DjangoStaticFetcher(),
        )

    def save_pdf(self, context: td.StringKeyDict) -> t.Tuple[str, str]:
        service = self._get_pdf_service(context)
        self.make_document_path()
        return service.save()

    def generate_pdf(self,
                     context: td.StringKeyDict) -> t.Tuple[str, t.BinaryIO]:
        service = self._get_pdf_service(context)
        self.make_document_path()
        return service.generate_pdf_contents()

    def get_pdf_url(self, digest: str) -> str:
        kwargs = self.kwargs
        kwargs.update(filename=digest)
        return reverse(self.pdf_download_viewname, kwargs=kwargs)


class BasePdfPreviewCreateView(PdfFromTemplateMixin, generic.View):
    pdf_download_viewname = 'api_urls:preview-download'

    def get(self, request: HttpRequest, *args, **kwargs):
        return self.http_method_not_allowed(request, *args, **kwargs)

    def post(self, *args, **kwargs) -> HttpResponse:
        """
        Handles incoming post data, initializing the serializer. It kind of
        mimics the behavior of a DRF view, but is a generic view.
        """
        raw_data = self.request.body
        logger.info(f'Data from frontend: {raw_data}')
        if self.serializer_class is None:
            raise NotImplementedError('serializer_class must be set.')

        serializer = self.serializer_class(
            data=json.loads(raw_data), context={'request': self.request}
        )
        if not serializer.is_valid(raise_exception=True):
            return JsonResponse(status=status.HTTP_400_BAD_REQUEST,
                                data=serializer.initial_data)

        logger.info(f'Serializer data: {serializer.validated_data}')
        self.handle_incoming_data(serializer.validated_data)
        try:
            context = self.get_context_data(
                note_data=serializer.validated_data
            )
        except PermissionError:
            return HttpResponse(status=status.HTTP_403_FORBIDDEN)
        except Http404 as e:
            data = json.loads(e.args[0])
            return JsonResponse(status=status.HTTP_404_NOT_FOUND, data=data)
        except InvalidLineIdError as e:
            data = error_got_wrong_value('line_id', e.bad_value, e.expected,
                                         message=e.args[0])
            return JsonResponse(status=status.HTTP_400_BAD_REQUEST, data=data)

        logger.debug(context)
        return self.render_to_response(context)

    def render_to_response(self, context, **response_kwargs):
        if not self.is_preview:
            return JsonResponse(status=400, data={})

        path, digest = self.save_pdf(context)
        url = self.get_pdf_url(digest)
        return JsonResponse({'url': url})


class OrderConfirmationPreviewCreateView(BasePdfPreviewCreateView):
    template_name = 'order_notes/confirmation_note.html'
    output_path = None
    note_prefix_attribute = 'order_confirmation_prefix'
    note_name = _('Order Confirmation')
    is_preview = True
    note_model = order_notes.OrderConfirmationNote
    serializer_class = serializers.OrderConfirmationSerializer


class InvoicePreviewCreateView(BasePdfPreviewCreateView):
    template_name = 'order_notes/invoice.html'
    output_path = None
    note_prefix_attribute = 'invoice_prefix'
    note_name = _('Invoice')
    is_preview = True
    note_model = order_notes.InvoiceNote
    serializer_class = serializers.InvoiceSerializer

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        model = order_notes.OrderConfirmationNote
        confirmation_note = model.objects.filter(
            order=self.get_order()
        ).order_by('-created_at').first()
        if confirmation_note is None:
            confirmation_note = {'pk': 'XXXXXX'}

        context['confirmation_note'] = confirmation_note
        context['note_id'] = \
            order_notes.InvoiceSequence.objects.get_next(
                partner=self.get_partner(), update=False
        )

        return context


def get_line_by_line_id(line_id: int,
                        lines: t.Sequence[td.StringKeyDict]):
    for line in lines:
        if line['id'] == line_id:
            return line

    return None


class DeliveryNotePreviewCreateView(BasePdfPreviewCreateView):
    template_name = 'order_notes/delivery_note.html'
    output_path = None
    note_prefix_attribute = 'delivery_note_prefix'
    note_name = _('Delivery Note')
    is_preview = True
    note_model = order_notes.DeliveryNote
    serializer_class = serializers.DeliveryNoteSerializer
    document_path = 'pdf/order_notes'

    def get_latest_confirmation_note(self) -> \
            t.Optional[order_notes.OrderConfirmationNote]:
        manager = order_notes.OrderConfirmationNote.objects
        try:
            return manager.filter(order=self.get_order()).latest('created_at')
        except order_notes.OrderConfirmationNote.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fallback = {'order_confirmation_number': 'XXXXXX'}
        confirmation_note = self.get_latest_confirmation_note()

        context['confirmation_note'] = confirmation_note or fallback

        # Handle actually delivered items
        note_data = context.get('note_data')
        logger.debug('Note data keys: {}'.format('\n'.join(note_data.keys())))
        order_lines = context.pop('order_lines')

        logger.info(order_lines)
        valid_line_ids = [line['id'] for line in order_lines]
        delivered_items = []
        processed_lines: t.List[td.StringKeyDict] = note_data.get(
            'processed_lines'
        )
        for processed_line in processed_lines:
            delivered = processed_line['delivered']
            if delivered == 0:
                continue
            line_id = processed_line['order_line_id']
            line = get_line_by_line_id(line_id, order_lines)
            if not line:
                raise InvalidLineIdError(
                    f'Line ID not found in order: {line_id}',
                    bad_value=line_id, expected=valid_line_ids)
            delivered_items.append({
                'order_line': line,
                'delivered': delivered,
            })
        context.update({
            'delivered_items': delivered_items,
            'show_thumbnails': note_data.get('show_thumbnails', False),
        })
        return context


class OrderConfirmationListCreate(BaseOrderView, PdfFromTemplateMixin,
                                  ListCreateAPIView):
    serializer_class = serializers.OrderConfirmationSerializer
    output_path = None
    note_name = _('Order Confirmation')
    is_preview = False
    note_model = order_notes.OrderConfirmationNote
    template_name = 'order_notes/confirmation_note.html'
    pdf_download_viewname = 'api_urls:preview-download'
    document_path = 'pdf/order_notes'

    def get_template(self):
        return super().get_template()

    def perform_create(self,
                       serializer: serializers.OrderConfirmationSerializer):
        self.note_instance: order_notes.BaseOrderNote = serializer.save(
            order=self.get_order(), created_by=self.request.user
        )
        service = self.get_service()
        self.note_instance.set_note_prefix(prefix_provider=service)
        self.note_instance.save()
        self.note_instance.refresh_from_db()  # To get the pk
        context = self.get_context_data(
            note_data=serializer.validated_data,
            note_prefix=self.note_instance.note_prefix,
            note_id=self.note_instance.pk,
        )
        digest, file_obj = self.generate_pdf(context)
        filename = f'{digest}.{self.document_extension}'
        self.note_instance.document.save(filename, File(file_obj))
        self.note_instance.save()

    def get_queryset(self):
        return order_notes.OrderConfirmationNote.objects.filter(
            order=self.get_order()
        )


class OrderConfirmationDetailView(BaseOrderView, RetrieveDestroyAPIView):
    serializer_class = serializers.OrderConfirmationSerializer
    pk_kwarg = 'confirmation_note_id'

    def get_object(self):
        try:
            return order_notes.OrderConfirmationNote.objects.get(
                pk=self.kwargs[self.pk_kwarg]
            )
        except order_notes.OrderConfirmationNote.DoesNotExist:
            raise Http404


class InvoiceListCreate(BaseOrderView, PdfFromTemplateMixin,
                        ListCreateAPIView):
    serializer_class = serializers.InvoiceSerializer
    document_path = 'pdf/order_notes'
    output_path = None
    note_name = _('Invoice')
    is_preview = False
    note_model = order_notes.InvoiceNote
    template_name = 'order_notes/invoice.html'

    def perform_create(self, serializer: serializers.InvoiceSerializer):
        order: Order = self.get_order()
        try:
            order_notes.can_create_invoice(order=order)
        except DjangoValidationError as e:
            raise ValidationError(detail=e.message, code=e.code)

        model = order_notes.OrderConfirmationNote
        confirmation_note = model.objects.filter(
            order=self.get_order()
        ).order_by('-created_at').first()
        if confirmation_note is None:
            confirmation_note = {'pk': ''}

        invoice_number = order_notes.InvoiceSequence.objects.get_next(
            partner=order.partner, update=True
        )
        self.note_instance: order_notes.InvoiceNote = serializer.save(
            order=self.get_object(), created_by=self.request.user
        )
        service = self.get_service()
        self.note_instance.set_note_prefix(prefix_provider=service)
        self.note_instance.invoice_number = invoice_number
        self.note_instance.save()
        context = self.get_context_data(
            note_data=serializer.validated_data,
            note_prefix=self.note_instance.note_prefix,
            note_id=self.note_instance.invoice_number,
            confirmation_note=confirmation_note,
        )
        digest, file_obj = self.generate_pdf(context)
        filename = f'{digest}.{self.document_extension}'
        self.note_instance.document.save(filename, File(file_obj))
        self.note_instance.save()

    def get_queryset(self):
        return order_notes.InvoiceNote.objects.filter(order=self.get_order())


class InvoiceDetailView(BaseOrderView, RetrieveDestroyAPIView):
    serializer_class = serializers.InvoiceSerializer

    def get_object(self):
        try:
            return order_notes.InvoiceNote.objects.get(
                id=self.kwargs['invoice_id']
            )
        except order_notes.InvoiceNote.DoesNotExist:
            raise Http404

    def perform_destroy(self, instance):
        instance.cancelled_by = self.request.user
        instance.cancelled_at = str(timezone.now().date())
        instance.is_cancelled = True
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DeliveryNoteListCreate(BaseOrderView, PdfFromTemplateMixin,
                             ListCreateAPIView):
    serializer_class = serializers.DeliveryNoteSerializer
    document_path = 'pdf/order_notes'
    output_path = None
    note_name = _('Delivery Note')
    is_preview = False
    note_model = order_notes.DeliveryNote
    template_name = 'order_notes/delivery_note.html'

    def get_latest_confirmation_note(self) -> \
            t.Optional[order_notes.OrderConfirmationNote]:
        manager = order_notes.OrderConfirmationNote.objects
        try:
            return manager.filter(order=self.get_order()).latest('created_at')
        except order_notes.OrderConfirmationNote.DoesNotExist:
            return None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        fallback = {'confirmation_note_number': ''}
        confirmation_note = self.get_latest_confirmation_note()

        context['confirmation_note'] = confirmation_note or fallback

        # Serializer should save to self.note_instance before this is called.
        assert self.note_instance is not None  # nosec
        delivered_lines: t.Sequence[td.StringKeyDict] = \
            self.note_instance.processed_lines.values(
            'order_line', 'delivered'
        )
        order_lines = context.pop('order_lines')
        delivered_items: t.List[td.StringKeyDict] = []
        for item in delivered_lines:
            delivered_items.append({
                'order_line': get_line_by_line_id(
                    item['order_line'], order_lines
                ),
                'delivered': item['delivered']
            })
        logger.info(f'Delivered items: {delivered_items}')
        show_thumbnails = context['note_data'].get('show_thumbnails', False)
        context.update({
            'delivered_items': delivered_items,
            'show_thumbnails': show_thumbnails,
        })
        return context

    def perform_create(self, serializer):
        order = self.get_order()
        self.note_instance: order_notes.DeliveryNote = serializer.save(
            order=order, created_by=self.request.user
        )
        self.note_instance.set_note_prefix(prefix_provider=self.get_partner())
        self.note_instance.save()
        self.note_instance.refresh_from_db()  # To get the pk
        context = self.get_context_data(
            note_data=serializer.validated_data,
            note_prefix=self.note_instance.note_prefix,
            note_id=self.note_instance.pk,
        )
        digest, file_obj = self.generate_pdf(context)
        filename = f'{digest}.{self.document_extension}'
        self.note_instance.document.save(filename, File(file_obj))
        self.note_instance.save()

    def get_queryset(self):
        return order_notes.DeliveryNote.objects.filter(order=self.get_object())


class DeliveryNoteDetailView(BaseOrderView, RetrieveDestroyAPIView):
    serializer_class = serializers.DeliveryNoteSerializer

    def get_object(self):
        try:
            return order_notes.DeliveryNote.objects.get(
                id=self.kwargs['delivery_note_id']
            )
        except order_notes.DeliveryNote.DoesNotExist:
            raise Http404
