import typing as t

from django.db.models import F
from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.users.serializers.user import UserSerializer
from apps.b3_order import models
from button3d import type_declarations as td


# noinspection PyAbstractClass
class BaseOrderNoteSerializer(PayloadConverterMixin,
                              serializers.ModelSerializer):
    created_by = UserSerializer(read_only=True)
    resource_url = serializers.SerializerMethodField()

    # noinspection PyMethodMayBeStatic
    def get_resource_url(
        self, obj: t.Union[td.StringKeyDict, models.BaseOrderNote]
    ) -> t.Optional[str]:
        try:
            return obj.document.url
        except (AttributeError, ValueError):
            return None

    class Meta:
        model = None
        read_only_fields = ('created_at', 'created_by', 'resource_url',
                            'note_prefix')
        fields = (
            'id', 'created_at', 'created_by', 'issue_date', 'shipping_date',
            'comment', 'note_prefix', 'notify_customer', 'resource_url'
        )


class DeliveryNoteLineSerializer(PayloadConverterMixin,
                                 serializers.ModelSerializer):
    order_line_id = serializers.IntegerField()

    class Meta:
        model = models.DeliveryNoteLine
        fields = ('order_line_id', 'delivered',)


class OrderConfirmationSerializer(BaseOrderNoteSerializer):
    class Meta(BaseOrderNoteSerializer.Meta):
        model = models.OrderConfirmationNote


class InvoiceSerializer(BaseOrderNoteSerializer):
    class Meta(BaseOrderNoteSerializer.Meta):
        model = models.InvoiceNote
        read_only_fields = BaseOrderNoteSerializer.Meta.read_only_fields + (
            'is_cancelled', 'invoice_number'
        )
        fields = BaseOrderNoteSerializer.Meta.fields + (
            'is_cancelled', 'invoice_number'
        )


class DeliveryNoteSerializer(BaseOrderNoteSerializer):
    is_partial_delivery = serializers.SerializerMethodField()
    processed_lines = DeliveryNoteLineSerializer(many=True)

    def create(self, validated_data: td.StringKeyDict) -> models.DeliveryNote:
        processed_lines: t.Sequence[t.Dict] = validated_data.pop(
            'processed_lines'
        )
        delivery_note = models.DeliveryNote.objects.create(**validated_data)

        for line in processed_lines:
            if line.get('delivered', 0) == 0:
                continue
            models.DeliveryNoteLine.objects.create(
                delivery_note=delivery_note, **line
            )

        return delivery_note

    # noinspection PyMethodMayBeStatic
    def get_is_partial_delivery(self,
                                obj: t.Union[models.DeliveryNote,
                                             td.StringKeyDict]) -> bool:
        """
        A partial delivery can only be determined if the object is saved and
        we're working with a model.
        Since it's a computed property, we simply return False if the
        serializer is called with data instead of a model.
        """
        if not isinstance(obj, models.DeliveryNote):
            return False
        have_partial_lines = obj.processed_lines.filter(
            order_line__quantity__gt=F('delivered')
        ).exists()
        processed_all_lines = bool(
            obj.processed_lines.count() == obj.order.lines.count()
        )
        return have_partial_lines or not processed_all_lines

    class Meta(BaseOrderNoteSerializer.Meta):
        model = models.DeliveryNote
        fields = BaseOrderNoteSerializer.Meta.fields + (
            'is_partial_delivery', 'processed_lines', 'show_thumbnails',
        )
