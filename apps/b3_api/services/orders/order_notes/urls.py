from django.conf.urls import url

from apps.b3_api.services.orders.order_notes import views


urlpatterns = [
    url(r'^delivery-notes/$', views.DeliveryNoteListCreate.as_view()),
    url(r'^delivery-notes/(?P<delivery_note_id>\d+)/$',
        views.DeliveryNoteDetailView.as_view()),
    url(r'^delivery-notes/preview/',
        views.DeliveryNotePreviewCreateView.as_view()),
    url(r'^invoice-notes/$', views.InvoiceListCreate.as_view()),
    url(r'^invoice-notes/(?P<invoice_id>\d+)/$',
        views.InvoiceDetailView.as_view()),
    url(r'^invoice-notes/preview/', views.InvoicePreviewCreateView.as_view()),
    url(r'^confirmation-notes/$', views.OrderConfirmationListCreate.as_view()),
    url(r'^confirmation-notes/(?P<confirmation_note_id>\d+)/$',
        views.OrderConfirmationDetailView.as_view(),
        name='confirmation-note-detail',
        ),
    url(r'^confirmation-notes/preview/$',
        views.OrderConfirmationPreviewCreateView.as_view(),
        name='oc-create-preview'),
    url(r'^pdf/(?P<filename>[a-f0-9]+)/$',
        views.PdfPreviewDownloadView.as_view(), name='preview-download'),
]
