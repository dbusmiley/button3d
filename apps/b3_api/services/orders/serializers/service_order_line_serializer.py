from decimal import Decimal as D

from rest_framework import serializers

from apps.b3_api.services.projects.lines.utils import \
    get_printability_status_or_error, fetch_download_links_stl_file
from apps.b3_checkout.serializers.projects.price import FloatPriceSerializer
from apps.b3_core.utils import build_secure_download_url
from apps.b3_order.models import OrderLine


class LineSerializer(serializers.ModelSerializer):
    discount = FloatPriceSerializer(source='item_discount')
    prices = serializers.SerializerMethodField()
    offer = serializers.SerializerMethodField()
    dimensions = serializers.SerializerMethodField()
    file = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()

    def get_offer(self, obj):
        product = obj.stock_record.product
        post_processing_options = obj.post_processing_options.all()

        return {
            'material': product.full_product_title,
            'minimumWallThickness': product.attr_wall_min,
            'optimalWallThickness': product.attr_wall_opt,
            'postProcessings': [{
                'name': option.post_processing.title,
                'color': option.color.id if option.color else None
            } for option in post_processing_options]
        }

    def get_model(self, obj):
        thumbnail_url = self.context.get('request').build_absolute_uri(
            obj.thumbnail_url
        )
        response_dict = {
            'uuid': obj.stl_file.uuid if obj.stl_file else None,
            'printability': {
                'status': 'not-printable',  # default as not-printable
                'error': None,
            },
            'thumbnail': thumbnail_url,
            'area': None,
            'volume': None,
            'faces': None,
            'shells': 1,
            'holes': None,
            'boxVolume': None,
        }
        stl_file_parameter = obj.stl_file.parameter
        if stl_file_parameter:
            response_dict.update({
                'area': (
                    f'{D(stl_file_parameter.get_area(obj.scale)):.4f}'
                ),
                'volume': (
                    f'{D(stl_file_parameter.get_volume(obj.scale)):.4f}'
                ),
                'faces': stl_file_parameter.faces,
                'shells': stl_file_parameter.shells,
                'holes': stl_file_parameter.holes,
                'boxVolume': (
                    f'{D(stl_file_parameter.get_bb_volume(obj.scale)):.4f}'
                ),
            })

        (printable, status, error) = get_printability_status_or_error(obj)
        if not printable:
            response_dict['printability']['error'] = error
        else:
            response_dict['printability']['status'] = status['printability']

        return response_dict

    def get_prices(self, obj):
        price_object = obj.unit_price
        if not price_object:
            return None

        return price_object.as_camel_case_dict()

    def get_dimensions(self, obj):
        return obj.dimensions

    def get_file(self, obj):
        non_secure_downloads = fetch_download_links_stl_file(
            self.context.get('request'), obj.stl_file)

        return {
            **non_secure_downloads,
            "optimizedCtm": build_secure_download_url(
                uuid=obj.stl_file.uuid,
                file='repaired_simple.ctm',
                expiration=60*20),
            "originalCtm": build_secure_download_url(
                uuid=obj.stl_file.uuid,
                file='repaired.ctm',
                expiration=60 * 20),
            "wta": build_secure_download_url(
                uuid=obj.stl_file.uuid,
                file='wta.json',
                expiration=60 * 20),
            "supportStructure": build_secure_download_url(
                uuid=obj.stl_file.uuid,
                file='support.json',
                expiration=60 * 20),
            "texture": build_secure_download_url(
                uuid=obj.stl_file.uuid,
                file='texture.png',
                expiration=60 * 20)
        }

    class Meta:
        model = OrderLine
        fields = (
            'id',
            'name',
            'quantity',
            'prices',
            'discount',
            'offer',
            'dimensions',
            'file',
            'model',
            'scale',
            'unit',
        )
