from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.order_pricing_rules.serializers.applied_order_fees \
    import AppliedOrderFeeSerializer
from apps.b3_api.services.orders.serializers.service_order_billing_serializer \
    import ServiceOrderBillingSerializer
from apps.b3_api.services.orders.serializers.service_order_line_serializer \
    import LineSerializer
from apps.b3_api.services.orders.serializers. \
    service_order_shipping_serializer import ServiceOrderShippingSerializer
from apps.b3_api.users.serializers.user import UserSerializer
from apps.b3_checkout.serializers.projects.price import FloatPriceSerializer
from apps.b3_order.models import Order


class ServiceOrderDetailSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    prices = serializers.SerializerMethodField()
    shipping = ServiceOrderShippingSerializer(source='*')
    language = serializers.CharField(source='customer_language')
    billing = ServiceOrderBillingSerializer(source='*')
    date = serializers.DateTimeField(source='datetime_placed')
    fees = AppliedOrderFeeSerializer(source='applied_fees', many=True)
    minimum_price_fee = FloatPriceSerializer()
    purchased_by = UserSerializer()
    lines = LineSerializer(many=True)
    site = serializers.SerializerMethodField()
    voucher_discount = FloatPriceSerializer()
    voucher_code = serializers.CharField(source='voucher.code', default='')
    instruction = serializers.CharField(source='delivery_instructions')
    company = serializers.SerializerMethodField()
    status = serializers.CharField(source='status.type')

    def get_prices(self, obj):
        prices = obj.total_without_shipping.as_camel_case_dict()
        if not prices:
            return None
        prices["subTotal"] = obj.subtotal.as_camel_case_dict()
        return prices

    def get_site(self, obj):
        return {
            'domain': obj.site.domain,
        }

    def get_company(self, obj):
        company, tax = '', ''
        if obj.billing_address:
            company = obj.billing_address.company_name
            tax = obj.billing_address.vat_id
        return {
            "companyName": company,
            "taxNumber": tax
        }

    class Meta:
        model = Order
        fields = (
            'number',
            'status',
            'project',
            'purchased_by',
            'date',
            'prices',
            'fees',
            'minimum_price_fee',
            'shipping',
            'billing',
            'lines',
            'site',
            'language',
            'instruction',
            'company',
            'voucher_discount',
            'voucher_code'
        )
        depth = 2
