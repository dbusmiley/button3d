from rest_framework import serializers

from apps.b3_order.models import Order


class ServiceOrderTrackingSerializer(serializers.ModelSerializer):
    info = serializers.CharField(source='tracking_information')

    class Meta:
        model = Order
        fields = ('info', )
