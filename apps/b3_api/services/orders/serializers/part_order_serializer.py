from rest_framework import serializers

from apps.b3_order.models import Order


class PartOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = (
            'id',
            'number',
        )
