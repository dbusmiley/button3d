from rest_framework import serializers

from apps.b3_checkout.models import PaymentAttachment


class ServiceOrderAttachmentSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='filename')
    url = serializers.URLField(source='get_absolute_url')

    class Meta:
        model = PaymentAttachment
        fields = (
            'name',
            'url'
        )
