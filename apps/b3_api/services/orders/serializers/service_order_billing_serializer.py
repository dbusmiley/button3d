from rest_framework import serializers

from apps.b3_api.services.orders.serializers.service_order_address_serializer \
    import ServiceOrderAddressSerializer
from apps.b3_api.services.orders.serializers.\
    service_order_attachment_serializer import ServiceOrderAttachmentSerializer
from apps.b3_order.models import Order


class ServiceOrderBillingSerializer(serializers.ModelSerializer):
    payment = serializers.CharField(source='payment.payment_method.name')
    address = ServiceOrderAddressSerializer(
        source='billing_address', many=False
    )
    attachments = ServiceOrderAttachmentSerializer(
        source='payment.payment_attachments', many=True
    )

    class Meta:
        model = Order
        fields = (
            'payment',
            'address',
            'attachments'
        )
