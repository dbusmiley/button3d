from django.utils.dateformat import format
from rest_framework import serializers

from apps.b3_order.models import Order


class ProjectOrderSerializer(serializers.ModelSerializer):
    date = serializers.SerializerMethodField()

    def get_date(self, obj):
        return format(obj.datetime_placed, 'U')

    class Meta:
        model = Order
        fields = ('date', 'id')
