from django.utils.dateformat import format
from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.orders.serializers.service_order_billing_serializer \
    import ServiceOrderBillingSerializer
from apps.b3_api.services.orders.serializers. \
    service_order_shipping_serializer import ServiceOrderShippingSerializer
from apps.b3_api.users.serializers.user import UserSerializer
from apps.b3_checkout.serializers.projects.price import FloatPriceSerializer
from apps.b3_order.models import Order


class ProjectOrderMinimalDetailsSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    order_id = serializers.IntegerField(source='id')
    order_date = serializers.SerializerMethodField()
    shipping = ServiceOrderShippingSerializer(source='*')
    billing = ServiceOrderBillingSerializer(source='*')
    discounts = FloatPriceSerializer(source='voucher_discount')
    purchased_by = UserSerializer()

    def get_order_date(self, obj):
        return int(format(obj.datetime_placed, 'U'))

    class Meta:
        model = Order
        fields = (
            'order_id',
            'order_date',
            'number',
            'billing',
            'shipping',
            'discounts',
            'purchased_by'
        )
