from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_address.models import Address


class ServiceOrderAddressSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    class Meta:
        model = Address
        field_mappings = {
            'company_name': 'company',
            'zip_code': 'post_code'
        }
        fields = (
            'title',
            'first_name',
            'last_name',
            'line1',
            'line2',
            'company_name',
            'department',
            'city',
            'zip_code',
            'state',
            'phone_number',
            'country',
        )
