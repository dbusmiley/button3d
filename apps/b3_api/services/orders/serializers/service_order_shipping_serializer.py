from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.orders.serializers.service_order_address_serializer \
    import ServiceOrderAddressSerializer
from apps.b3_api.utils.currency_field_serializer import CurrencyField
from apps.b3_order.models import Order
from apps.b3_shipping.models import ShippingMethod


class ServiceOrderShippingDaysSerializer(serializers.ModelSerializer):
    minimum = serializers.IntegerField(source='shipping_days_min')
    maximum = serializers.IntegerField(source='shipping_days_max')

    class Meta:
        model = ShippingMethod
        fields = (
            'maximum',
            'minimum',
        )


class ServiceOrderShippingSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer,
):
    exclusive_tax = CurrencyField(
        source='shipping_price.excl_tax',
        coerce_to_string=False
    )
    inclusive_tax = CurrencyField(
        source='shipping_price.incl_tax',
        coerce_to_string=False
    )
    tax = CurrencyField(
        source='shipping_price.tax',
        coerce_to_string=False
    )
    method = serializers.CharField(source='shipping_method')
    delivery = ServiceOrderShippingDaysSerializer(source='shipping_method')
    address = ServiceOrderAddressSerializer(
        source='shipping_address', many=False
    )

    class Meta:
        model = Order
        fields = (
            'exclusive_tax',
            'inclusive_tax',
            'tax',
            'method',
            'pickup_location',
            'delivery',
            'address'
        )
