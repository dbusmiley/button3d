import json
from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceOrderLanguageTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceOrderLanguageTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)

        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line2.save()
        self.basket.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        self.shipping_method = ShippingMethodFactory(partner=self.partner)
        self.shipping_method.save()
        # need an order associated with this.

    def test_order_language(self):
        order = OrderFactory(
            project=self.basket,
            purchased_by=self.user,
            site=self.site,
            shipping_method=self.shipping_method,
        )
        order.save()
        order_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/{order.id}/'
        )
        order_response = json.loads(order_response_raw.content)
        self.assertEqual(order.customer_language, order_response["language"])
