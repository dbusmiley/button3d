import io
import csv
from decimal import Decimal

from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_order.models import Order
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceOrderExportsTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceOrderExportsTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory()
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)

        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line2.save()
        self.basket.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        self.order = OrderFactory(
            project=self.basket,
            partner=self.partner,
            shipping_method=shipping_method,
            purchased_by=self.user,
        )
        self.order.save()

    def test_export_empty_csv_file(self):
        project_response_csv_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/export/'
        )
        csv_rows = list(csv.DictReader(
            io.StringIO(str(project_response_csv_raw.content))
        ))
        self.assertNotEqual(project_response_csv_raw.content, None)
        self.assertEqual(len(csv_rows), 0)

    def test_export_csv_file(self):
        project_response_csv_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/export/'
        )
        project_response_content = project_response_csv_raw.content.decode()
        csv_rows = list(csv.DictReader(io.StringIO(project_response_content)))
        order_obj = Order.objects.get(id=self.order.id)
        self.assertEqual(len(csv_rows), self.order.lines.count())
        project_row = csv_rows[0]

        self.assertEqual(
            Decimal(project_row['Total price, EUR']),
            order_obj.total.excl_tax
        )
        self.assertEqual(
            Decimal(project_row['Voucher discount amount, EUR']),
            order_obj.voucher_discount.excl_tax
        )
