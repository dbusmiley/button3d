import json
from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceOrderPartnerAccessTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceOrderPartnerAccessTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)

        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line2.save()
        self.basket.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        self.order = OrderFactory(
            project=self.basket,
            purchased_by=self.user,
            site=self.site,
            shipping_method=shipping_method,
        )
        self.order.save()

    def test_order_detail_view(self):
        """
        Try to fetch the created dummy order, accessed with the right partner
        """
        service_order_detail_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/{self.order.id}/'
        )
        self.assertEqual(
            json.loads(
                service_order_detail_response_raw.content)['number'],
            self.order.number
        )

    def test_order_detail_with_multiple_orders_partners(self):
        """
        Add an additional order, with a different partner and make sure that
        only the right ones are returned on a call
        """
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site
        basket1.save()

        partner1 = PartnerFactory(name='Meltwerk1', logo='meltwerk-logo.png')
        partner1.site = self.site
        partner1.users.add(self.user)
        partner1.save()

        stockrecord1 = StockRecordFactory(partner=partner1)

        line1, created = basket1.add_product(
            product=stockrecord1.product,
            stockrecord=stockrecord1,
            stlfile=StlFileFactory()
        )
        line1.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        shipping_method1 = ShippingMethodFactory(partner=partner1)
        shipping_method1.save()

        order1 = OrderFactory(
            project=basket1,
            purchased_by=self.user,
            site=self.site,
            shipping_method=shipping_method1,
        )
        order1.save()

        service_order_detail_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/{self.order.id}/'
        )
        self.assertEqual(
            json.loads(
                service_order_detail_response_raw.content
            )['number'],
            self.order.number
        )
