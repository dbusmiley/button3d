import json
from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceOrderLanguageTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceOrderLanguageTest, self).setUp()
        self.site = get_current_site()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()
        self.stockrecord = StockRecordFactory(partner=self.partner)
        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

    def create_order(self, instruction=None):
        line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line2.save()
        self.basket.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        order = OrderFactory(
            project=self.basket,
            purchased_by=self.user,
            site=self.site,
            shipping_method=shipping_method,
        )
        if instruction:
            order.delivery_instructions = instruction
            order.save()
        return order

    def test_no_instruction_order(self):
        order = self.create_order()
        order_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/{order.id}/'
        )
        order_response = json.loads(order_response_raw.content)
        self.assertEqual(None, order_response['instruction'])

    def test_instruction_order(self):
        order = self.create_order(instruction='This is a cool instruction')
        order_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/orders/{order.id}/'
        )
        order_response = json.loads(order_response_raw.content)
        self.assertEqual('This is a cool instruction',
                         order_response['instruction'])
