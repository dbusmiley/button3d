from django.utils import timezone
from apps.b3_order.factories import OrderFactory
from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import SiteFactory, OrganizationFactory, \
    UserFactory, BasketFactory, StockRecordFactory, \
    get_current_site, PartnerFactory, BasketLineFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase


class OrderDetailsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(OrderDetailsTest, self).setUp()

    def test_cross_organization_order_detail(self):
        partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        partner.site = get_current_site()
        partner.users.add(self.user)
        partner.save()

        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.save()
        site = SiteFactory(
            domain='alternative.my.3yd',
            name='Alternative Organization',
        )
        new_org = OrganizationFactory(site=site)
        new_org.partners_enabled.add(partner)
        set_current_site(site)

        new_user = UserFactory(password=self.test_password)

        basket = BasketFactory(site_id=site.id, owner=new_user)
        BasketLineFactory(
            basket=basket,
            stockrecord=stockrecord,
        )
        BasketFactory.save_basket(basket)

        order = OrderFactory(
            project=basket,
            site_id=site.id,
            purchased_by=new_user,
            datetime_placed=timezone.now()
        )
        order.save()

        url = f'/api/v2.0/services/{partner.id}/orders/{order.id}/'
        order_response = self.client.get(url)
        self.assertEqual(order_response['status'], 'Pending')

    def test_company_information_on_response(self):
        site = get_current_site()
        partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        partner.site = site
        partner.users.add(self.user)
        partner.save()

        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.save()

        basket = BasketFactory(site_id=site.id, owner=self.user)
        BasketLineFactory(
            basket=basket,
            stockrecord=stockrecord,
        )
        BasketFactory.save_basket(basket)

        order = OrderFactory(
            project=basket,
            site_id=site.id,
            purchased_by=self.user,
            datetime_placed=timezone.now())
        order.save()

        url = f'/api/v2.0/services/{partner.id}/orders/{order.id}/'
        order_response = self.client.get(url)
        self.assertEqual(
            order_response['company'],
            {'companyName': '3YOURMIND', 'taxNumber': 'AS-123-R'}
        )

        # Now test for presence of details
        order.billing_address.company_name = '3yourminD'
        order.billing_address.vat_id = 'DE123456789'
        order.billing_address.save()
        order.save()

        order_response = self.client.get(url)
        self.assertEqual(
            order_response['company'],
            {'companyName': '3yourminD', 'taxNumber': 'DE123456789'}
        )
