from json import dumps

from apps.b3_order.factories import OrderFactory
from apps.b3_organization.utils import set_current_site
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import SiteFactory, OrganizationFactory, \
    UserFactory, BasketFactory, StlFileFactory, \
    StockRecordFactory, get_current_site, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.basket.models import Basket


class OrderTrackingDetailsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(OrderTrackingDetailsTest, self).setUp()

    def test_get_and_update_order_detail(self):
        partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        partner.site = get_current_site()
        partner.users.add(self.user)
        partner.save()
        shipping_method = ShippingMethodFactory(partner=partner)

        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.save()
        site = SiteFactory(
            domain='alternative.my.3yd',
            name='Alternative Organization',
        )
        new_org = OrganizationFactory(site=site)
        new_org.partners_enabled.add(partner)
        set_current_site(site)

        new_user = UserFactory(password=self.test_password)

        new_basket = BasketFactory(site_id=site.id, owner=new_user)
        new_basket.save()
        new_line1, new_created = new_basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        new_line1.save()

        new_basket.pricing_status = Basket.NO_REQUEST
        new_basket.status = Basket.SUBMITTED
        new_basket.save()

        new_order = OrderFactory(
            project=new_basket,
            site=site,
            purchased_by=new_user,
            shipping_method=shipping_method,
        )
        new_order.tracking_information = '#8743942834234'
        new_order.save()

        url = f'/api/v2.0/services/{partner.id}' \
            f'/orders/{new_order.id}/tracking/'
        order_response = self.client.get(url)
        expected = {"info": "#8743942834234"}
        self.assertEqual(order_response, expected)

        payload = dumps({"info": "012345"})
        order_response = self.client.put(
            url, data=payload, content_type='application/json')
        expected = {"info": "012345"}
        self.assertEqual(order_response, expected)

        # POST does the same as PUT
        payload = dumps({"info": "000000"})
        order_response = self.client.post(
            url, data=payload, content_type='application/json')
        expected = {"info": "000000"}
        self.assertEqual(order_response, expected)
