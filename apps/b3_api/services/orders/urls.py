from django.conf.urls import url, include

from apps.b3_api.services.orders.views import ServiceOrderDetail, \
    ServiceOrderExport, ServiceMultipleOrderExport, ServiceOrderTracking


urlpatterns = [
    url(r'^(?P<order_id>\d+)/$', ServiceOrderDetail.as_view()),
    url(r'^(?P<order_id>\d+)/export/$', ServiceOrderExport.as_view()),
    url(r'^(?P<order_id>\d+)/order-notes/',
        include('apps.b3_api.services.orders.order_notes.urls')),
    url(r'^export/$', ServiceMultipleOrderExport.as_view()),
    url(r'^(?P<order_id>\d+)/tracking/', ServiceOrderTracking.as_view())
]
