from rest_framework.generics import RetrieveAPIView

from apps.b3_api.services.orders.views.base_order_view import BaseOrderView
from apps.b3_api.services.orders.serializers.service_order_detail_serializer \
    import ServiceOrderDetailSerializer


class ServiceOrderDetail(BaseOrderView, RetrieveAPIView):
    def get_serializer_class(self):
        return ServiceOrderDetailSerializer
