from rest_framework.generics import RetrieveUpdateAPIView

from apps.b3_api.services.orders.views.base_order_view import BaseOrderView
from apps.b3_api.services.orders.serializers.service_order_tracking_serializer\
    import ServiceOrderTrackingSerializer


class ServiceOrderTracking(BaseOrderView, RetrieveUpdateAPIView):
    def get_serializer_class(self):
        return ServiceOrderTrackingSerializer

    def post(self, *args, **kwargs):
        return self.put(*args, **kwargs)
