from apps.b3_api.services.views import BaseServiceView
from .mixins import OrderMixin


class BaseOrderView(BaseServiceView, OrderMixin):
    def get_object(self):
        return self.get_order()
