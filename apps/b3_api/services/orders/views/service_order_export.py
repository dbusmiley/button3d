from django.http import HttpResponse

from apps.b3_api.services.views import BaseServiceView
from apps.b3_order.models import Order
from apps.b3_order.order_csv_export import OrderCSVExport


class ServiceOrderExport(BaseServiceView):
    def get(self, request, order_id, service_id, *args, **kwargs):
        """
        Call internal CSV generation API and create CSV file
        """

        orders = Order.objects.filter(
            id=order_id,
            partner_id=service_id,
        )

        csv_exporter = OrderCSVExport(orders, request)

        response = HttpResponse(
            csv_exporter.as_raw_csv(),
            content_type='text/csv',
        )

        order_number: str = orders[0].number
        file_name: str = f'order-{order_number}.csv'

        response['Content-Disposition'] = \
            f'attachment; filename="{file_name}"'
        return response
