from django.http import Http404

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_order.models import Order


class OrderMixin:
    def get_order(self) -> Order:
        try:
            return Order.objects.with_partner(
                partner_id=self.kwargs.get('service_id')
            ).get(pk=self.kwargs['order_id'])
        except Order.DoesNotExist:
            raise Http404(
                error_object_not_found(
                    object_name='Order', object_id=self.kwargs['order_id']
                )
            )
