from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_bad_db_operation
from apps.b3_api.services.views import BaseServiceView
from apps.b3_order.models import Order as OrderNew
from apps.b3_order.order_csv_export import OrderCSVExport
from apps.b3_order.serializers.order_csv_export_filter_serializer import \
    OrderCSVExportFilterSerializer


class ServiceMultipleOrderExport(BaseServiceView):
    @staticmethod
    def _handle_bad_db_operation(exception):
        return Response(
            error_bad_db_operation(object_name='Order', exception=exception),
            status=status.HTTP_400_BAD_REQUEST
        )

    def get(self, request, *args, **kwargs):
        """
        Endpoint to get the csv of multiple projects.
        """
        partner = self.get_object()

        query_serializer = OrderCSVExportFilterSerializer(
            data=request.query_params
        )
        query_serializer.is_valid(raise_exception=True)
        query = query_serializer.save()

        partner_orders = OrderNew.all_objects \
            .distinct()\
            .filter(project__lines__stockrecord__partner=partner)\
            .order_by('id')

        partner_orders = partner_orders.filter(query)

        csv_exporter = OrderCSVExport(
            partner_orders,
            request,
        )

        response = HttpResponse(
            csv_exporter.as_raw_csv(),
            content_type='text/csv',
        )
        filter_query_pretty_printed = \
            query_serializer.filter_query_pretty_printed()
        file_name = f'orders {filter_query_pretty_printed}.csv'

        response['Content-Disposition'] = \
            f'attachment; filename="{file_name}"'
        return response
