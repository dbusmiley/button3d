from apps.b3_api.services.projects.lines.utils import (
    fetch_download_links_stl_file
)
from apps.b3_core.utils import build_secure_download_url


def generate_file_urls(request, stl_file):
    non_secure_downloads = fetch_download_links_stl_file(request, stl_file)

    return {
        **non_secure_downloads,
        "optimizedCtm": build_secure_download_url(
            uuid=stl_file.uuid, file='repaired_simple.ctm', expiration=60 * 20
        ),
        "originalCtm": build_secure_download_url(
            uuid=stl_file.uuid, file='repaired.ctm', expiration=60 * 20
        ),
        "wta": build_secure_download_url(
            uuid=stl_file.uuid, file='wta.json', expiration=60 * 20
        ),
        "supportStructure": build_secure_download_url(
            uuid=stl_file.uuid, file='support.json', expiration=60 * 20
        ),
        "texture": build_secure_download_url(
            uuid=stl_file.uuid, file='texture.png', expiration=60 * 20
        ),
    }
