from apps.b3_api.services.workstations.serializers. \
    workstation_possible_sequences_serializer import \
    WorkstationPossibleSequencesSerializer
from apps.b3_order.factories import OrderFactory
from apps.b3_order.models import OrderLinePostProcessingOption
from apps.b3_tests.factories import PartnerFactory, PostProcessingFactory
from apps.b3_tests.factories.services import WorkstationFactory, \
    WorkflowTemplateWithStatusesFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase


class WorkstationPossibleSequencesSerializerTest(TestCase):
    def setUp(self):
        super().setUp()

        partner = PartnerFactory()
        order = OrderFactory(partner=partner)
        part = order.lines.first()
        part.quantity = 3
        part.save()

        post_processing = PostProcessingFactory()
        OrderLinePostProcessingOption(
            post_processing=post_processing,
            line=part
        ).save()

        workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=partner)

        workflow = workflow_template. \
            generate_workflow_and_sequences(part.id)

        workstation = WorkstationFactory(
            partner=partner, is_printing=True)

        last_status = workflow.statuses.get(next=None)
        last_status.workstation = workstation
        last_status.save()

        status = workflow.get_first_status()
        status.workstation = workstation
        status.save()

        self.serializer_populated = WorkstationPossibleSequencesSerializer(
            workstation,
            context={'service_id': partner.id}
        )

        self.structure_expected = {
            'id': 0,
            'parts': [
                {
                    'id': 0,
                    'name': 'string',
                    'orderNumber': 0,
                    'material': {
                        'id': 0,
                        'title': 'string',
                    },
                    'postProcessings': ['string'],
                    'orderDate': 'string',
                    'sequencesUnassigned': [0],
                    'sequencesSuggested': [],
                    'thumbnailUrl': 'string',
                }
            ],
            'suggestedParts': [
                {
                    'id': 0,
                    'name': 'string',
                    'orderNumber': 0,
                    'material': {
                        'id': 0,
                        'title': 'string',
                    },
                    'postProcessings': ['string'],
                    'orderDate': 'string',
                    'sequencesUnassigned': [0],
                    'sequencesSuggested': [0],
                    'thumbnailUrl': 'string',
                }
            ]

        }

    def test_structure(self):
        assert_serializer_structure(
            self, self.serializer_populated, self.structure_expected
        )
