from apps.b3_api.services.workstations.\
    serializers.workstation_create_update_serializer import \
    WorkstationCreateUpdateSerializer
from apps.b3_tests.factories.services import WorkstationFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase


class WorkstationCreateUpdateSerializerTest(TestCase):
    def setUp(self):
        super().setUp()

        workstation = WorkstationFactory()
        self.serializer_populated = WorkstationCreateUpdateSerializer(
            workstation
        )

        self.structure_expected = {
            'id': 0,
            'name': 'string',
            'order': 0,
            'materials': [0],
            'postProcessings': [0],
            'description': '',
            'boundingBox3d': 'true',
            'loadFactor': 0.0,
            'dimensions': {
                'x': 0,
                'y': 0,
                'z': 0
            },
        }

    def test_structure(self):
        assert_serializer_structure(
            self, self.serializer_populated, self.structure_expected
        )
