import json

from django.urls import reverse

from apps.b3_mes.models import Workstation
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories.services import WorkstationFactory, \
    WorkflowTemplateWithStatusesFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories.partner import PartnerFactory, \
    PostProcessingFactory, StockRecordFactory
from apps.b3_tests.factories import ProductFactory, BasketLineFactory

from rest_framework import status


class WorkstationViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.partner = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.partner)
        product = ProductFactory()
        stock_record = StockRecordFactory(
            partner=self.partner,
            product=product
        )
        post_processing = PostProcessingFactory(stock_record=stock_record)

        # setUp for create/list
        self.payload_create = {
            'name': 'EOS 201',
            'materials': [product.id],
            'postProcessings': [post_processing.id],
            'description': '',
            'boundingBox3d': True,
            'loadFactor': 29.2,
            'dimensions': {
                'x': 100,
                'y': 10,
                'z': 10
            },
        }
        self.payload_create = json.dumps(self.payload_create)
        self.create_kwargs = {
            'data': self.payload_create,
            'content_type': 'application/json',
        }

        self.list_url = reverse(
            'api_urls:workstation-list', kwargs={'service_id': self.partner.pk}
        )

        WorkstationFactory.create_batch(
            5,
            partner=self.partner,
            is_printing=False)

        # setUp for update/delete
        self.workstation_existing = Workstation.objects.filter(
            partner=self.partner).first()
        self.workstation_existing.is_printing = True
        self.workstation_existing.save()
        current_products = ProductFactory.create_batch(2)
        self.workstation_existing.materials.set(current_products)
        current_post_processings = PostProcessingFactory.create_batch(2)
        self.workstation_existing.post_processings.set(
            current_post_processings)

        current_products.pop()
        current_post_processings.pop()

        new_products = current_products + [ProductFactory()]
        new_post_processings = current_post_processings + [
            PostProcessingFactory()
        ]

        self.new_product_ids = [p.id for p in new_products]
        self.new_post_processings_ids = [pp.id for pp in new_post_processings]

        self.payload_update = {
            'name': 'EOS 201',
            'materials': self.new_product_ids,
            'postProcessings': self.new_post_processings_ids,
            'description': '',
            'boundingBox3d': False,
            'loadFactor': 20.4,
            'dimensions': {
                'x': 50,
                'y': 20,
                'z': 30
            },
        }
        self.payload_update = json.dumps(self.payload_update)

        self.update_kwargs = {
            'data': self.payload_update,
            'content_type': 'application/json',
        }
        self.detail_url = reverse(
            'api_urls:workstation-detail',
            kwargs={
                'service_id': self.partner.pk,
                'workstation_id': self.workstation_existing.id,
            },
        )

        # setUp for possible_sequences
        project_line = BasketLineFactory(
            stockrecord=stock_record,
            quantity=4,
            configuration__scale=1,
            stl_file__parameter__h=10,
            stl_file__parameter__d=10,
            stl_file__parameter__w=70
        )
        project_line.post_processing_options.create(
            post_processing=post_processing
        )

        order = OrderFactory(project=project_line.basket)
        part = order.lines.first()

        workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.partner)

        workflow = workflow_template. \
            generate_workflow_and_sequences(part.id)

        '''

        `status_1`              | `status_2`          | third status
                                |                     |
        `sequences_possible`    | -                   | -
                                |                     |
        `workstation_non_print` | `workstation_print` | `workstation_non_print`

        '''
        status_1 = workflow.get_first_status()
        status_2 = status_1.next
        status_3 = status_2.next

        workstation_print = WorkstationFactory(
            partner=self.partner,
            is_printing=True,
            dimension_x=100,
            dimension_y=10,
            dimension_z=10
        )
        workstation_non_print = WorkstationFactory(
            partner=self.partner, is_printing=False)

        status_1.workstation = workstation_non_print
        status_1.save()
        status_2.workstation = workstation_print
        status_2.save()
        status_3.workstation = workstation_non_print
        status_3.save()

        sequence = status_1.sequences.first()
        sequence.status = status_2
        sequence.save()

        sequence = status_1.sequences.first()
        sequence.status = status_3
        sequence.save()

        self.possible_sequences = list(
            status_1.sequences.all().values_list('id', flat=True))

        self.possible_sequences_url = reverse(
            'api_urls:workstation-possible-sequences',
            kwargs={
                'service_id': self.partner.pk,
                'workstation_id': workstation_print.id,
            },
        )

        self.swap_workstations_url = reverse(
            'api_urls:workstation-swap-workstations',
            kwargs={
                'service_id': self.partner.pk,
            },
        )

    def test_create(self):
        response = self.client.post(self.list_url, **self.create_kwargs)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        workstation_dict = self._update_workstation_dict_for_querying(
            self.payload_create
        )

        self.assertTrue(Workstation.objects.get(**workstation_dict))

    def test_list(self):
        num_workstations = Workstation.objects.filter(
            partner=self.partner).count()

        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), num_workstations)

    def test_delete(self):
        response = self.client.delete(self.detail_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Workstation.objects.filter(
            id=self.workstation_existing.id).exists())

    def test_update(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        workstation_dict = self._update_workstation_dict_for_querying(
            self.payload_update)
        workstation_dict['id'] = self.workstation_existing.id

        self.assertTrue(Workstation.objects.get(**workstation_dict))

    def _update_workstation_dict_for_querying(self, payload):
        workstation_dict = json.loads(payload)
        workstation_dict.pop('postProcessings')
        workstation_dict.pop('materials')

        load_factor = workstation_dict.pop('loadFactor')
        workstation_dict['load_factor'] = load_factor

        bounding_box3d = workstation_dict.pop('boundingBox3d')
        workstation_dict['bounding_box3d'] = bounding_box3d

        dimensions = workstation_dict.pop('dimensions')
        workstation_dict['dimension_x'] = dimensions['x']
        workstation_dict['dimension_y'] = dimensions['y']
        workstation_dict['dimension_z'] = dimensions['z']

        return workstation_dict

    def test_update_manytomany(self):
        response = self.client.put(self.detail_url, **self.update_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        workstation_dict = json.loads(self.payload_update)
        post_processings = workstation_dict.pop('postProcessings')
        materials = workstation_dict.pop('materials')
        id = self.workstation_existing.id

        workstation = Workstation.objects.get(id=id)
        self.assertCountEqual(
            post_processings,
            workstation.post_processings.all().values_list('id', flat=True)
        )
        self.assertCountEqual(
            materials,
            workstation.materials.all().values_list('id', flat=True)
        )

    def test_possible_sequences(self):
        response = self.client.get(self.possible_sequences_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        parts = json.loads(response.content.decode('utf-8'))['parts']
        suggested_parts = json.loads(
            response.content.decode('utf-8'))['suggestedParts']
        self.assertCountEqual(
            parts[0]['sequencesUnassigned'], self.possible_sequences)
        self.assertEqual(len(suggested_parts), 1)

    def test_swap_workstations(self):
        [first, second] = list(
            Workstation.objects.filter(partner=self.partner).all()[:2]
        )

        first_order = first.order
        second_order = second.order

        payload = json.dumps({
            'fromWorkstationId': first.id,
            'toWorkstationId': second.id,
        })
        update_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }
        response = self.client.post(
            self.swap_workstations_url,
            **update_kwargs,
        )

        first.refresh_from_db()
        second.refresh_from_db()

        # should actually be HTTP_204_NO_CONTENT
        # but I failed to convince the seriliazer
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(first_order, second.order)
        self.assertEqual(second_order, first.order)
