from rest_framework.decorators import action

from apps.b3_api.services.views import BaseMESView

from rest_framework import viewsets

from apps.b3_api.services.workstations.serializers.\
    workstation_create_update_serializer import \
    WorkstationCreateUpdateSerializer
from apps.b3_api.services.workstations.serializers.\
    workstation_list_serializer import \
    WorkstationListSerializer
from apps.b3_api.services.workstations.serializers. \
    workstation_possible_sequences_serializer import \
    WorkstationPossibleSequencesSerializer
from apps.b3_api.services.workstations.serializers. \
    workstation_swap_workstations_serializer import \
    WorkstationSwapWorkstationsSerializer
from apps.b3_mes.models import Workstation
import button3d.type_declarations as td

import typing as t


class WorkstationViewset(
    BaseMESView,
    viewsets.ModelViewSet,
):
    lookup_url_kwarg = 'workstation_id'

    def get_serializer_class(self) -> td.SerializerClass:
        if self.action in ['create', 'update']:
            return WorkstationCreateUpdateSerializer
        elif self.action == 'list':
            return WorkstationListSerializer
        elif self.action == 'possible_sequences':
            return WorkstationPossibleSequencesSerializer
        elif self.action == 'swap_workstations':
            return WorkstationSwapWorkstationsSerializer

    def get_serializer_context(self) -> dict:
        context = super().get_serializer_context()
        context['service_id'] = self.kwargs['service_id']
        return context

    def get_queryset(self) -> t.Sequence[td.Workstation]:
        partner_id = self.kwargs['service_id']
        return Workstation.objects\
            .filter(partner__id=partner_id)\
            .order_by('order')

    def get_object(self):
        return super(viewsets.ModelViewSet, self).get_object()

    @action(detail=True, methods=['get'])
    def possible_sequences(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        """
        Retrieves sequences that can be assigned to a job for a workstation
        """
        return super().retrieve(request, *args, **kwargs)

    @action(detail=False, methods=['post'])
    def swap_workstations(self, request, *args, **kwargs):
        """
        Swap the order of 2 workstations
        """
        return super().create(request, *args, **kwargs)
