from apps.b3_api.services.workstations.views import WorkstationViewset

from rest_framework import routers

router = routers.SimpleRouter()

router.register(r'', WorkstationViewset, base_name='workstation')

urlpatterns = router.urls
