from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.utils.serializers import NestingMixin
from apps.b3_mes.models import Workstation


class WorkstationBaseSerializer(
    PayloadConverterMixin,
    NestingMixin,
    serializers.ModelSerializer
):
    class Meta:
        model = Workstation
        fields = (
            'id',
            'name',
            'order',
            'dimension_x',
            'dimension_y',
            'dimension_z',
            'materials',
            'post_processings',
            'description',
            'bounding_box3d',
            'load_factor',
        )
        nesting_options = [{
            'name': 'dimensions',
            'translations': [
                ('dimension_x', 'x'),
                ('dimension_y', 'y'),
                ('dimension_z', 'z'),
            ]
        }]
