from rest_framework import serializers

from apps.b3_api.services.workstations.serializers.workstation_base_serializer\
    import WorkstationBaseSerializer
from apps.b3_mes.models import Workstation
from apps.partner.models import Partner, PostProcessing, Product
from apps.b3_api.utils.helpers import update_model_using_dict
import button3d.type_declarations as td

import typing as t


class WorkstationCreateUpdateSerializer(
    WorkstationBaseSerializer
):
    materials = serializers.PrimaryKeyRelatedField(
        allow_empty=True, many=True, queryset=Product.objects.all()
    )
    post_processings = serializers.PrimaryKeyRelatedField(
        allow_empty=True, many=True, queryset=PostProcessing.objects.all()
    )

    def create(self, validated_data):
        return self._perform_create_update(validated_data)

    def update(self, instance, validated_data):
        return self._perform_create_update(
            validated_data, create=False, instance=instance
        )

    def _perform_create_update(
        self, validated_data: dict, create=True, instance=None
    ) -> td.Workstation:
        materials = validated_data.pop('materials')
        post_processings = validated_data.pop('post_processings')

        if create:
            partner = Partner.objects.get(id=self.context['service_id'])
            instance = Workstation(partner=partner, **validated_data)
            instance.save()
        else:
            update_model_using_dict(instance, validated_data)

        self._set_many_to_many(materials, post_processings, instance)

        return instance

    @staticmethod
    def _set_many_to_many(
        materials: t.Sequence[td.Product],
        post_processings: t.Sequence[td.PostProcessing],
        instance: td.Workstation
    ) -> None:
        instance.materials.set(materials)
        instance.post_processings.set(post_processings)
