from drf_payload_customizer.mixins import PayloadConverterMixin
from rest_framework import serializers

from apps.b3_api.services.parts.serializers. \
    part_possible_for_workstation_serializer import \
    PartPossibleForWorkstationSerializer
from apps.b3_api.services.parts.serializers.\
    part_suggested_for_workstation_serializer import \
    PartSuggestedForWorkstationSerializer
from apps.b3_mes.models import Workstation
import button3d.type_declarations as td

import typing as t


class WorkstationPossibleSequencesSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    parts = serializers.SerializerMethodField()
    suggested_parts = serializers.SerializerMethodField()

    @staticmethod
    def get_parts(obj: td.Workstation) -> dict:
        parts_qs = obj.get_assignable_parts()
        return PartPossibleForWorkstationSerializer(
            parts_qs,
            context={'workstation_id': obj.id},
            many=True
        ).data

    @staticmethod
    def get_suggested_parts(obj: td.Workstation) -> t.Sequence[dict]:
        parts_qs = obj.get_suggested_parts_sequences()
        suggested_parts_sequences = []
        for part, sequences in parts_qs:

            serializer = PartSuggestedForWorkstationSerializer(
                instance=part,
                context={'workstation_id': obj.id},
            )
            data = serializer.data
            data.update({'sequencesSuggested': sequences})
            suggested_parts_sequences.append(data)
        return suggested_parts_sequences

    class Meta:
        model = Workstation
        fields = ('id', 'parts', 'suggested_parts')
