from apps.b3_api.catalogue.serializers import ProductSerializer
from apps.b3_api.services.offers.post_processings.serializers.\
    workstation_post_processing_serializer import \
    WorkstationPostProcessingSerializer
from apps.b3_api.services.workstations.serializers.workstation_base_serializer\
    import WorkstationBaseSerializer


class WorkstationListSerializer(
    WorkstationBaseSerializer
):
    materials = ProductSerializer(many=True, read_only=True)
    post_processings = WorkstationPostProcessingSerializer(
        many=True, read_only=True
    )
