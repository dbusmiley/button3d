from rest_framework import serializers, status
from rest_framework.response import Response

from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.workstations.fields import \
    WorkstationPrimaryKeyRelatedField


from apps.b3_mes.models import Workstation


class WorkstationSwapWorkstationsSerializer(
    PayloadConverterMixin,
    serializers.Serializer,
):
    from_workstation_id = WorkstationPrimaryKeyRelatedField(write_only=True)
    to_workstation_id = WorkstationPrimaryKeyRelatedField(write_only=True)

    class Meta:
        model = Workstation
        fields = ('from_workstation_id', 'to_workstation_id')

    def create(self, validated_data):
        first = validated_data['from_workstation_id']
        second = validated_data['to_workstation_id']

        first.swap(second)

        # this is not working we're returning 201 created
        return Response(validated_data, status=status.HTTP_204_NO_CONTENT)
