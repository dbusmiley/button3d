from rest_framework import serializers

from apps.b3_mes.models import Workstation


class WorkstationNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Workstation
        fields = ('id', 'name')
