from rest_framework import serializers

from apps.partner.models import Partner
import button3d.type_declarations as td

import typing as t


class WorkstationPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self) -> t.Sequence[td.Partner]:
        partner = Partner.objects.get(id=self.context['service_id'])
        return partner.workstations.all()
