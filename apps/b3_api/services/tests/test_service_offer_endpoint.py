import json

from apps.b3_api.services.testcases import ServiceAPITestCase
from apps.b3_tests.factories import ProductFactory

from apps.partner.models import StockRecord


class ServiceOfferTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceOfferTest, self).setUp()
        self.offer_url = f'/api/v2.0/services/{self.partner.id}/offers/'
        self.status_url = f'{self.offer_url}{self.stock_record.id}/status/'

    def add_new_offer(self, **offer_data):
        product_new = ProductFactory(**offer_data)
        product_new.save()
        new_price_dict = {"material": 0, "finishing": product_new.id}
        return self.client.post(
            self.offer_url,
            data=json.dumps(new_price_dict),
            content_type="application/json"
        )

    def update_offer(self, payload):
        return self.client.post(
            self.status_url,
            data=json.dumps(payload),
            content_type="application/json"
        )

    def test_existing_offers(self):
        response = self.client.get(self.offer_url)
        self.assertEqual(response.data[0]['materialName'],
                         self.stock_record.product.title)
        self.assertEqual(len(response.data), 1)

    def test_existing_offer_status(self):
        response = self.client.get(self.status_url)
        self.assertEqual(response.data['published'], True)

    def test_add_new_offer(self):
        response = self.add_new_offer(title='New Material')
        self.assertEqual(response.status_code, 201)

    def test_new_offer_exists(self):
        self.add_new_offer(title='New Material')
        response = self.client.get(self.offer_url)
        self.assertEqual(len(response.data), 2)

    def test_update_offer(self):
        response = self.update_offer({"published": False})

        self.assertEqual(response.status_code, 202)

    def test_updated_offer_status(self):
        self.update_offer({"published": False})
        response = self.client.get(self.status_url)
        self.assertEqual(response.data['published'], False)

    def test_offer_was_updated_in_database(self):
        self.update_offer({"published": False})
        stock_record = StockRecord.objects.get(pk=self.stock_record.id)
        self.assertEqual(stock_record.edit_status, StockRecord.DRAFT)
