from apps.b3_order.factories import OrderFactory
from apps.basket.models import Basket


def create_order_for_the_basket(site, basket):
    """
    Utility function to create an order with the existing basket
    """
    basket.pricing_status = Basket.NO_REQUEST
    basket.status = Basket.SUBMITTED
    # need an order associated with this.
    order = OrderFactory()

    # manually overriding to match the site, basket
    order.site = site
    order.basket = basket
    order.save()
    return basket
