
from apps.b3_api.services.testcases import ServiceAPITestCase
from apps.basket.models import Basket


class ServiceProjectsEndpointsTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceProjectsEndpointsTest, self).setUp()
        self.projects_url = f'/api/v2.0/services/{self.partner.id}/projects/'

    def test_without_ordered_projects(self):
        response = self.client.get(self.projects_url)
        self.assertEqual(len(response.data), 0)

    def test_with_waiting_for_manually_priced_baskets(self):
        # Check manually priced baskets
        self.basket.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        self.basket.save()
        response = self.client.get(self.projects_url)
        self.assertEqual(len(response.data), 1)

    def test_with_manually_priced_baskets(self):
        # now, also check if this is the same for MANUALLY_PRICED
        self.basket.pricing_status = Basket.MANUALLY_PRICED
        self.basket.save()
        response = self.client.get(self.projects_url)
        self.assertEqual(len(response.data), 1)

    def test_with_ordered_baskets(self):
        # now, check on submitted BASKETS with no pricing request
        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED

        # need an order associated with this.
        order = self.order_basket(self.basket)

        # basket status updated after creating a new order
        self.basket.save()
        response = self.client.get(self.projects_url)

        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data[0]['order']['orderId'],
            order.id,
            "Order should be matched correctly"
        )

    def test_with_multiple_lines_project(self):
        # add one more line here
        self.add_line_to_basket(self.basket)
        self.basket.pricing_status = Basket.MANUALLY_PRICED
        self.basket.save()

        response = self.client.get(self.projects_url)
        # The project is submitted at this point, so it should return 1
        self.assertEqual(len(response.data), 1)
