import json

from apps.b3_api.services.testcases import ServiceAPITestCase
from apps.partner.models import Partner


class ServicesAPIEndpointTest(ServiceAPITestCase):
    def setUp(self):
        super(ServicesAPIEndpointTest, self).setUp()
        self.services_url = '/api/v2.0/services/'
        self.partner_settings_url = \
            f'{self.services_url}{self.partner.id}/settings/'

    def update_settings(self, payload):
        return self.client.put(
            self.partner_settings_url,
            data=json.dumps(payload),
            content_type='application/json'
        )

    def test_restricted_access(self):
        self.partner.users.remove(self.user)
        response = self.client.get(self.services_url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.data, {
                'code': 'SERVICE_ACCESS_DENIED',
                'message': 'Access denied for the requesting user',
                'moreInfo': {
                    'userId': str(self.user.id),
                    'objectName': 'SERVICE',
                    'objectId': 'None'
                }
            }
        )

    def test_allowed_access(self):
        response = self.client.get(self.services_url)
        self.assertEqual(len(response.data), 1)

    def test_access_on_added_partner_is_restricted(self):
        self.add_partner(accessible_by_client=False)
        response = self.client.get(self.services_url)
        self.assertEqual(len(response.data), 1)

    def test_access_on_added_partner_for_client_is_allowed(self):
        self.add_partner(accessible_by_client=True)
        response = self.client.get(self.services_url)
        self.assertEqual(len(response.data), 2)

    def test_single_service(self):
        self.partner.price_currency = 'USD'
        self.partner.save()

        response = self.client.get(self.services_url)
        data = response.data
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]['id'], self.partner.id)
        self.assertEqual(data[0]['name'], 'Meltwerk')
        self.assertEqual(
            data[0]['logo'],
            'http://my.3yd/media/meltwerk-logo.png'
        )
        self.assertEqual(data[0]['currency'], 'USD')

    def test_update_settings(self):
        response = self.client.get(self.partner_settings_url)
        current_settings = json.loads(response.content)

        updated_partner_name = 'New Partner'
        current_settings['name'] = updated_partner_name
        current_settings['address']['postCode'] = 'EC1A 1BB'
        current_settings['address']['company'] = '3YOURMIND'
        current_settings['address']['phoneNumber'] = '+493055578748'

        update_response = self.update_settings(current_settings)

        # Fetch partner again and check if things were updated
        partner = Partner.objects.get(id=self.partner.id)

        self.assertEqual(update_response.status_code, 200)
        self.assertEqual(partner.name, updated_partner_name)
        self.assertEqual(partner.address, self.partner.address)
