from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.org_testcases import \
    AuthenticatedAjaxOrganizationTestCase
import os
from django.conf import settings
from apps.b3_tests.factories import PartnerFactory
from django.test.client import BOUNDARY, MULTIPART_CONTENT, encode_multipart


def get_fixture_dir():
    return os.path.realpath(os.path.join(settings.BASE_DIR, 'apps',
                                         'b3_tests', 'fixtures'))


class ServiceLogoUploadTest(AuthenticatedAjaxOrganizationTestCase):
    def setUp(self):
        super().setUp()
        self.site = get_current_site()
        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

    def upload_file(self, filename) -> dict:
        service_id = self.partner.pk
        self.assertTrue(isinstance(service_id, int))
        request_uri = f'/api/v2.0/services/{service_id}/logo/'
        with open(os.path.join(get_fixture_dir(), filename), 'rb') as file_obj:
            response = self.client.put(
                request_uri, encode_multipart(BOUNDARY, {'logo': file_obj}),
                content_type=MULTIPART_CONTENT
            )

        return response

    def test_upload_png(self):
        response = self.upload_file(filename='SCHUNK_Typenschildlogo_RGB.png')
        self.assertTrue('logo' in response)
        self.assertTrue('status' not in response)

    def test_upload_jpeg(self):
        response = self.upload_file(
            filename='ines-alvarez-fdez-489172-unsplash.jpg'
        )
        # If this test start failing, maybe something changed in the
        # response structure. If so, print it and adjust and think why you
        # needed to change the error structure.
        # The tests done here, make sure we reject jpeg input, as the VUE
        # cropper component SHALL always generate PNG output.
        # The backend is configured in such a way that it only accepts PNG,
        # so the fronent MUST always send PNG.
        # What is said here, also applies the SVG test
        # print(response)

        self.assertTrue('logo' not in response)
        self.assertEqual(response['code'], 'Partner_VALIDATION_ERROR')
        self.assertTrue('moreInfo' in response and
                        'field' in response['moreInfo'][0])

    def test_upload_svg(self):
        response = self.upload_file(filename='3yourmind-logo-white.svg')
        self.assertTrue('logo' not in response)
        self.assertEqual(response['code'], 'Partner_VALIDATION_ERROR')
        self.assertTrue('moreInfo' in response and
                        'field' in response['moreInfo'][0])
