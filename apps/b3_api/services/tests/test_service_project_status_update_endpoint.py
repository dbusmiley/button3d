import json

from apps.b3_api.services.testcases import ServiceAPITestCase
from apps.b3_order.models import Order


class ServiceProjectStatusUpdateTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceProjectStatusUpdateTest, self).setUp()
        self.order_basket(self.basket)
        self.projects_status_url = f'/api/v2.0/services/{self.partner.id}'\
                                   f'/projects/{self.basket.id}/status/'

    def change_status(self, payload):
        return self.client.post(
            self.projects_status_url,
            data=json.dumps(payload),
            content_type='application/json'
        )

    @staticmethod
    def create_status_update(new_status, message="Test"):
        return {
            'content': f'<html><body><p>{message}</p></body></html>',
            'status': new_status
        }

    def test_user_with_no_pricing_perm_trying_to_price(self):
        self.partner.users.remove(self.user)
        status_cancel = self.create_status_update('CANCELLED')

        response = self.change_status(status_cancel)
        self.assertEqual(response.status_code, 403)

    def test_status_to_cancelled(self):
        status_cancel = self.create_status_update('CANCELLED')
        response = self.change_status(status_cancel)
        order = Order.objects.get(project=self.basket)
        self.assertEqual(response.data['success'], True)
        self.assertEqual(order.status.type, 'Cancelled')

    def test_basket_status_with_one_project(self):
        status_printing = self.create_status_update('PRINTING')
        response = self.change_status(status_printing)
        order = Order.objects.get(project=self.basket)

        self.assertEqual(response.data['success'], True)
        self.assertEqual(order.status.type, 'Printing')

    def test_email_body_with_nonascii_characters(self):
        status_printing = self.create_status_update('PRINTING', "ÜÖÄßßöds")
        response = self.change_status(status_printing)
        order = Order.objects.get(project=self.basket)
        self.assertEqual(response.data['success'], True)
        self.assertEqual(order.status.type, 'Printing')
