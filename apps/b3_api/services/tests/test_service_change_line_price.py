import json
from decimal import Decimal

from apps.basket.models import Basket
from apps.b3_api.services.testcases import ServiceAPITestCase
from apps.basket.models import Line


class ServiceUpdateLinePricingTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceUpdateLinePricingTest, self).setUp()
        self.line.is_manual_pricing_required = True
        self.line.save()

        self.basket.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        self.basket.save()
        self.order = self.order_basket(self.basket)
        self.line_url = f'/api/v2.0/services/{self.partner.id}'\
                        f'/projects/{self.basket.id}/lines/{self.line.id}'

    def test_updating_line_price(self):
        new_price_dict = {"price": 40.53}
        response = self.client.post(
            self.line_url,
            data=json.dumps(new_price_dict),
            content_type="application/json"
        )
        line_obj = Line.objects.get(id=self.line.id)
        data = json.loads(response.content)

        self.assertEqual(data['prices']['exclusiveTax'], Decimal(40.53))
        self.assertAlmostEqual(line_obj.price_excl_tax, Decimal(40.53))
