"""
Test illegal access to a project from an unauthorized partner
"""

from apps.b3_tests.factories import PartnerFactory
from apps.b3_api.services.testcases import ServiceAPITestCase


class ServiceTestRelatedObjectsTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceTestRelatedObjectsTest, self).setUp()
        self.partner_b = PartnerFactory(
            name='Meltwerk',
            logo='meltwerk-logo.png',
            site=self.site,
        )
        self.partner_b.save()

    def test_illegal_project_access(self):
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner_b.id}'
            f'/projects/{self.basket.id}/'
        )
        self.assertEqual(projects_response_raw.status_code, 403)
        self.assertEqual(
            projects_response_raw.data, {
                'code': 'SERVICE_OBJECT_ACCESS_DENIED',
                'message': 'Access denied for the requesting user',
                'moreInfo': {
                    'userId': str(self.user.id),
                    'objectName': 'SERVICE_OBJECT',
                    'objectId': str(self.partner_b.id)
                }
            }
        )
