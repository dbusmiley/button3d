
from apps.b3_api.services.testcases import ServiceAPITestCase


class ServiceProjectDetailsEndpointsTest(ServiceAPITestCase):
    def setUp(self):
        super(ServiceProjectDetailsEndpointsTest, self).setUp()
        self.project_url = f'/api/v2.0/services/{self.partner.id}'\
            f'/projects/{self.basket.id}/'

    def test_access_on_nonexistent_basket(self):
        response = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/0/'

        )
        self.assertEqual(response.status_code, 404)
        self.assertDictEqual(
            response.data, {
                'code': 'BASKET_NOT_FOUND',
                'message': 'BASKET not found for the given id',
                'moreInfo': {
                    'objectName': 'BASKET',
                    'id': '0'
                }
            }
        )

    def test_restricted_access(self):
        self.partner.users.remove(self.user)
        response = self.client.get(self.project_url)
        self.assertEqual(response.status_code, 403)
        self.assertDictEqual(
            response.data, {
                'code': 'SERVICE_ACCESS_DENIED',
                'message': 'Access denied for the requesting user',
                'moreInfo': {
                    'userId': f'{self.user.id}',
                    'objectName': 'SERVICE',
                    'objectId': f'{self.partner.id}'
                }
            }
        )

    def test_project_details_for_onordered_basket_with_a_line(self):
        response = self.client.get(self.project_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.basket.id)
        self.assertEqual(response.data['status'], 'No Request')
        self.assertEqual(response.data['lines'][0]['name'], self.line.name)

    def test_project_details_for_ordered_with_a_line(self):
        self.order_basket(self.basket)
        response = self.client.get(self.project_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['id'], self.basket.id)
        self.assertEqual(response.data['status'], 'Pending')
        self.assertEqual(response.data['lines'][0]['name'], self.line.name)
