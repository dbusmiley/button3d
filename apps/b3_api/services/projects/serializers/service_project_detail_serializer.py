from django.utils.dateformat import format
from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.orders.serializers import ProjectOrderSerializer
from apps.b3_api.services.projects.attachments\
    .serializers.order_attachment_serializer import OrderAttachmentSerializer
from apps.b3_api.services.projects.attachments \
    .serializers.project_attachment_serializer import \
    ProjectAttachmentSerializer
from apps.b3_api.services.projects.lines.serializers import \
    LineDetailsWithAttachmentSerializer
from apps.b3_api.services.projects.lines.serializers \
    .basket_line_details_with_attachment_serializer import \
    BasketLineDetailsWithAttachmentSerializer
from apps.b3_order.models import OrderAttachment
from apps.basket.models import BasketAttachment, Basket
from apps.basket.utils import generate_basket_user_permissions


class ServiceProjectDetailSerializer(
    PayloadConverterMixin,
    serializers.ModelSerializer
):
    order_details = ProjectOrderSerializer(source='order')
    lines = serializers.SerializerMethodField()
    attachments = serializers.SerializerMethodField()
    created = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    users_with_permissions = serializers.SerializerMethodField()

    def get_users_with_permissions(self, obj):
        return generate_basket_user_permissions(obj)

    def get_lines(self, obj):
        context = {'request': self.context.get('request')}
        if hasattr(obj, 'order'):
            return LineDetailsWithAttachmentSerializer(
                obj.order.lines.all(), many=True, context=context
            ).data
        else:
            return BasketLineDetailsWithAttachmentSerializer(
                obj.lines.all(), many=True, context=context
            ).data

    def get_attachments(self, obj):
        if hasattr(obj, 'order'):
            query = OrderAttachment.objects.filter(
                order=obj.order, order_line=None)
            attachment_serializer = OrderAttachmentSerializer
        else:
            query = BasketAttachment.objects.filter(
                basket=obj, basket_line=None)
            attachment_serializer = ProjectAttachmentSerializer
        return attachment_serializer(
            instance=query,
            many=True,
            context={
                'request': self.context.get('request'),
            }
        ).data

    def get_status(self, obj):
        if hasattr(obj, 'order'):
            return obj.order.status.type
        return obj.pricing_status

    def get_created(self, obj):
        return int(format(obj.date_created, 'U'))

    class Meta:
        model = Basket
        fields = (
            'id',
            'title',
            'created',
            'users_with_permissions',
            'status',
            'order_details',
            'lines',
            'attachments',
        )
