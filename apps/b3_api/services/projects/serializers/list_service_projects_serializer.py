from django.utils.dateformat import format
from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_api.services.orders.serializers \
    import ProjectOrderMinimalDetailsSerializer
from apps.b3_api.users.serializers.user import UserSerializer
from apps.basket.models import Basket


class ListServiceProjectsSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    owner = UserSerializer()
    prices = serializers.SerializerMethodField()
    comments = serializers.IntegerField(
        source='comments.count', read_only=True
    )
    status = serializers.SerializerMethodField()
    created = serializers.SerializerMethodField()
    order = ProjectOrderMinimalDetailsSerializer()

    def get_prices(self, obj):
        if hasattr(obj, 'order'):
            return obj.order.total_without_shipping.as_camel_case_dict()
        return None

    def get_created(self, obj):
        return int(format(obj.date_created, 'U'))

    def get_status(self, obj):
        if hasattr(obj, 'order'):
            return obj.order.status.type
        return 'Not ordered'

    class Meta:
        model = Basket
        fields = (
            'id',
            'title',
            'owner',
            'created',
            'prices',
            'comments',
            'status',
            'pricing_status',
            'order',
        )
