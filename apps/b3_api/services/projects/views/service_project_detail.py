from rest_framework.generics import RetrieveAPIView

from apps.b3_api.services.projects.serializers \
    .service_project_detail_serializer import ServiceProjectDetailSerializer
from apps.b3_api.services.projects.views.base_project_view \
    import BaseProjectView


class ServiceProjectDetail(BaseProjectView, RetrieveAPIView):
    def get_serializer_class(self):
        return ServiceProjectDetailSerializer
