from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.projects.permissions import IsUserWithProjectRW
from apps.b3_api.services.projects.views.base_project_view \
    import BaseProjectView
from apps.basket.models.basket import NotAllLinesPricedException


class ServiceProjectPriceConfirm(BaseProjectView, APIView):
    permission_classes = BaseProjectView.permission_classes + (
        IsUserWithProjectRW,
    )

    @staticmethod
    def _handle_pricing_update_failed(project_id):
        return Response(
            error_object_update_failed(
                object_name='PROJECT',
                object_id=project_id,
                message='Not all lines are priced yet.',
                exception=None,
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def post(self, request, *args, **kwargs):
        project = self.get_object()
        try:
            project.set_as_manually_priced()
        except NotAllLinesPricedException:
            return self._handle_pricing_update_failed(project.id)

        return Response({"success": True}, status=status.HTTP_200_OK)
