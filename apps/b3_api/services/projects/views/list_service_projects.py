from django.db.models import Q, Prefetch
from rest_framework.generics import ListAPIView

from apps.b3_api.services.projects.serializers \
    .list_service_projects_serializer import \
    ListServiceProjectsSerializer
from apps.b3_api.services.views.base_service_view import BaseServiceView
from apps.b3_order.models import Order
from apps.basket.models import Basket


class ListServiceProjects(BaseServiceView, ListAPIView):
    serializer_class = ListServiceProjectsSerializer

    def get_queryset(self):
        all_baskets_by_service = Basket.all_objects.filter(
            lines__stockrecord__partner__id=self.kwargs['service_id'],
            lines__deleted_date__isnull=True,
        ).order_by('date_submitted').distinct().prefetch_related(
            'lines__stockrecord',
            'lines__product',
            'owner',
            'comments',
        )
        ordered_baskets_without_pricing = all_baskets_by_service.filter(
            pricing_status=Basket.NO_REQUEST, status=Basket.SUBMITTED
        )

        manually_priced_baskets = all_baskets_by_service.filter(
            Q(pricing_status=Basket.WAITING_FOR_MANUAL_PRICING) |
            Q(pricing_status=Basket.MANUALLY_PRICED)
        )

        all_orders = Order.all_objects.filter(
            project_id__in=all_baskets_by_service.values_list(
                'id', flat=True
            )
        )
        qs = manually_priced_baskets | ordered_baskets_without_pricing
        return qs.prefetch_related(
            Prefetch('order', queryset=all_orders),
            'order__lines',
            'order__billing_address',
            'order__shipping_address'
        )
