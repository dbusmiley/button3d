from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.views.base_service_view import BaseServiceView
from apps.basket.models import Basket


class BaseProjectView(BaseServiceView):
    """
    Base class that can fetch the right Service for
    /services/<id>/projects/ functions
    """

    @staticmethod
    def _handle_related_order_not_found(order_id):
        return Response(
            error_object_not_found(
                object_name='Order',
                object_id=order_id,
            ),
            status=status.HTTP_404_NOT_FOUND
        )

    def get_object(self):
        basket = Basket.all_objects.filter(
            pk=self.kwargs['project_id'],
            lines__stockrecord__partner__id=self.kwargs['service_id']
        ).first()
        if not basket:
            raise Http404(
                error_object_not_found(
                    object_name='BASKET', object_id=self.kwargs['project_id']
                )
            )
        return basket

    def get_partner(self):
        return super(BaseProjectView, self).get_object()
