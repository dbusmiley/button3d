import html
import logging
import typing as t

from rest_framework import status
from rest_framework.response import Response

import button3d.type_declarations as td
from apps.b3_api.errors.api_responses import error_object_update_failed, \
    error_got_wrong_value
from apps.b3_api.services.projects.permissions import IsUserWithProjectRW
from apps.b3_api.services.projects.views.base_project_view \
    import BaseProjectView


class ChangeServiceProjectsStatus(BaseProjectView):
    permission_classes = BaseProjectView.permission_classes + (
        IsUserWithProjectRW,
    )

    @classmethod
    def update_basket_with_new_status(
            cls, basket: td.Project, status: str) -> bool:
        basket.pricing_status = status
        basket.save()
        return True

    @staticmethod
    def _handle_invalid_cancel_params(project_id: int) -> Response:
        return Response(
            error_object_update_failed(
                object_name='PROJECT',
                object_id=project_id,
                message='Missing required email content value'
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    @staticmethod
    def _handle_invalid_project_status(new_status: str) -> Response:
        return Response(
            error_got_wrong_value(
                key='status',
                value=new_status,
                value_expected='(PRINTING, CANCELLED, SHIPPED)',
                message='Got wrong project status'
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def post(self, request: td.DRFRequest, *args, **kwargs) -> Response:
        """
        This will change the state of an order. It will just change the state
        for one order at a time. It will handle a JSON with two keys 'status'
        and 'content'. The 'status' key will describe the new status. It can
        be printing, cancelled and shipped.
        """
        project = self.get_object()

        new_status = request.data.get('status', None)
        email_message = request.data.get('content', None)

        encoded_email_message = html.escape(email_message)

        if new_status not in ['PRINTING', 'CANCELLED', 'SHIPPED']:
            return self._handle_invalid_project_status(new_status)

        if new_status == 'CANCELLED' and not encoded_email_message:
            return self._handle_invalid_cancel_params(project.id)

        order: t.Optional[td.OrderNew] = project.order
        if not order:
            logging.warning(
                f'New Order not found for Project: {project.id}')
            return self._handle_related_order_not_found(project.id)

        order.set_status_with_email_message(
            new_status.title(),
            encoded_email_message
        )
        return Response({'success': True})
