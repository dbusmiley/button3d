from rest_framework import permissions


class IsUserWithProjectRW(permissions.BasePermission):
    message = 'You do not have access to modify this basket'

    def has_permission(self, request, view):
        project = view.get_object()

        if not project.can_user_edit(user=request.user, request=request):
            return False

        return True


class IsUserWithProjectPrice(permissions.BasePermission):
    message = 'You do not have access to price this basket'

    def has_permission(self, request, view):
        project = view.get_project()

        if not project.can_user_price(user=request.user):
            return False

        return True
