from django.urls import reverse
from rest_framework import serializers

from apps.b3_api.services.orders.serializers.service_order_line_serializer \
    import LineSerializer
from apps.b3_api.services.projects.attachments.serializers \
    .order_attachment_serializer import OrderAttachmentSerializer


class LineDetailsWithAttachmentSerializer(LineSerializer):
    attachments = OrderAttachmentSerializer(many=True)
    delivery = serializers.SerializerMethodField()

    def get_model(self, obj):
        response_dict = super().get_model(obj)
        request = self.context.get('request')

        response_dict['viewer'] = request.build_absolute_uri(
            reverse(
                'b3_user_panel:project-line-viewer',
                args=(obj.order.basket.id, obj.id, obj.stl_file.uuid)
            )
        )
        return response_dict

    def get_delivery(self, obj):
        stock_record = obj.stock_record
        partner = stock_record.partner
        post_processings = obj.post_processings
        fastest_shipping_method = partner.get_fastest_shipping_method()
        if not fastest_shipping_method:
            min_days, max_days = None, None
        else:
            min_days, max_days = fastest_shipping_method.delivery_time(
                stock_record, post_processings
            )
        return {'minimum': min_days, 'maximum': max_days}

    class Meta(LineSerializer.Meta):
        fields = LineSerializer.Meta.fields + ('attachments', 'delivery')
