from django.urls import reverse
from rest_framework import serializers

from apps.b3_api.services.projects.attachments.serializers\
    .project_attachment_serializer import \
    ProjectAttachmentSerializer
from apps.b3_api.services.projects.lines.serializers.basket_line_serializer \
    import BasketLineSerializer


class BasketLineDetailsWithAttachmentSerializer(BasketLineSerializer):
    attachments = ProjectAttachmentSerializer(many=True)
    delivery = serializers.SerializerMethodField()

    def get_model(self, obj):
        response_dict = super().get_model(obj)
        request = self.context.get('request')

        response_dict['viewer'] = request.build_absolute_uri(
            reverse(
                'b3_user_panel:project-line-viewer',
                args=(obj.basket_id, obj.id, obj.stl_file.uuid)
            )
        )
        return response_dict

    def get_delivery(self, obj):
        stock_record = obj.stockrecord
        partner = stock_record.partner
        post_processings = obj.post_processings
        fastest_shipping_method = partner.get_fastest_shipping_method()
        if not fastest_shipping_method:
            min_days, max_days = None, None
        else:
            min_days, max_days = fastest_shipping_method.delivery_time(
                stock_record, post_processings
            )
        return {'minimum': min_days, 'maximum': max_days}

    class Meta(BasketLineSerializer.Meta):
        fields = BasketLineSerializer.Meta.fields + ('attachments', 'delivery')
