import logging

logger = logging.getLogger(__name__)


def get_printability_status_or_error(object):
    """
    Returns printable Boolean, printability_status dict, problem
    :param object:
    :return:
    """
    printability_status = {
        'status': 'error',
        'error': None,
        'printability': 'unknown'
    }

    if not object.product.attr_wall_min:
        return False, printability_status, 'E_UNKOWN_WALL_THICKNESS'
    else:
        printability_status = object.stl_file.get_printability_status(
            object.product.attr_wall_min,
            object.scale,
        )

        if 'printability_problem' in printability_status:
            return False, printability_status, printability_status[
                'printability_problem'
            ]

        return True, printability_status, None


def fetch_download_links_stl_file(request, stl_file):
    return {
        "optimized":
        request.build_absolute_uri(stl_file.download_optimized_url),
        "original": request.build_absolute_uri(stl_file.download_original_url)
    }
