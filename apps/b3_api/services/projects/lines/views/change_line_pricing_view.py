from decimal import Decimal

from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.projects.lines.views.base_line_view \
    import BaseLineView
from apps.b3_api.services.projects.permissions import IsUserWithProjectPrice


class ChangeLinePricingView(BaseLineView):
    permission_classes = BaseLineView.permission_classes + (
        IsUserWithProjectPrice,
    )

    @staticmethod
    def _handle_no_manual_pricing_required(line_id):
        return Response(
            error_object_update_failed(
                object_name='PROJECT',
                object_id=line_id,
                message='Line does not need manual pricing.',
                exception=None,
            ),
            status=status.HTTP_403_FORBIDDEN
        )

    @staticmethod
    def _handle_object_update_failed(line_id, exception):
        return Response(
            error_object_update_failed(
                object_name='LINE',
                object_id=line_id,
                message="Pricing update failed",
                exception=exception
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def post(self, request, *args, **kwargs):
        line = self.get_object()

        if not line:
            return self._handle_invalid_line_response(kwargs['line_id'])

        if not line.is_manual_pricing_required:
            return self._handle_no_manual_pricing_required(line.id)
        try:
            price_excl_tax = Decimal(request.data['price'])
            if price_excl_tax < 0:
                raise ValueError('Cannot set a negative price')
            line.set_manual_price(price_excl_tax)
            return Response(
                {
                    "lineId": line.id,
                    "prices": line.price_object.as_camel_case_dict(),
                    "status": "updated"
                },
                status=status.HTTP_200_OK
            )
        except Exception as e:
            return self._handle_object_update_failed(
                line_id=line.id, exception=str(e)
            )
