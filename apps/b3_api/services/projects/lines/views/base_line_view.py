from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_not_found
from apps.b3_api.services.projects.views.base_project_view \
    import BaseProjectView
from apps.basket.models import Line


class BaseLineView(BaseProjectView):
    @staticmethod
    def _handle_invalid_line_response(line_id):
        return Response(
            error_object_not_found(object_name='LINE', object_id=line_id),
            status=status.HTTP_400_BAD_REQUEST
        )

    def get_partner(self):
        return super(BaseLineView, self).get_partner()

    def get_project(self):
        return super(BaseLineView, self).get_object()

    def get_object(self):
        return Line.objects.filter(id=self.kwargs['line_id']).first()
