from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.b3_api.errors.api_responses import error_object_update_failed
from apps.b3_api.services.projects.views import BaseProjectView


class ChangeProjectAttachmentView(BaseProjectView, APIView):
    @staticmethod
    def _handle_delete_failed_attachment(attachment_id):
        return Response(
            error_object_update_failed(
                object_name='BASKET_ATTACHMENTS',
                object_id=attachment_id,
                message="Couldnt delete the attachment"
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def delete(self, request, *args, **kwargs):
        object = self.get_object()

        attachment_id = self.kwargs.get('attachment_id', None)
        test_attachment = object.attachments.filter(id=attachment_id).first()

        if not test_attachment:
            return self._handle_delete_failed_attachment(attachment_id)

        test_attachment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self):
        basket = super().get_object()
        if hasattr(basket, 'order'):
            return basket.order
        return basket
