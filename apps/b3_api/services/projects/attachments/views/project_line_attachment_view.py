from rest_framework import status
from rest_framework.response import Response

from apps.b3_api.errors.api_responses import error_object_not_found, \
    error_object_update_failed
from apps.b3_api.services.projects.views import BaseProjectView
from apps.b3_order.models import OrderAttachment, Order
from apps.basket.models import BasketAttachment


class ProjectLineAttachmentView(BaseProjectView):
    @staticmethod
    def _handle_invalid_line_response(line_id):
        return Response(
            error_object_not_found(
                object_name='BASKET_LINE', object_id=line_id
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    @staticmethod
    def _handle_invalid_attachment_response(line_id):
        return Response(
            error_object_update_failed(
                object_name='BASKET_LINE',
                object_id=line_id,
                message="Attachment not found"
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    @staticmethod
    def _handle_update_failed(line_id, exception):
        return Response(
            error_object_update_failed(
                object_name='BASKET_LINE',
                object_id=line_id,
                message="Could not update the line",
                exception=exception
            ),
            status=status.HTTP_400_BAD_REQUEST
        )

    def post(self, request, *args, **kwargs):
        instance = self.get_object()
        instance_line = instance.lines.filter(id=kwargs['line_id']).first()

        if not instance_line:
            return self._handle_invalid_line_response(
                line_id=self.kwargs['line_id']
            )

        file_data = self.request.data.get('file', None)
        if not file_data:
            return self._handle_invalid_attachment_response(
                line_id=self.kwargs['line_id']
            )

        try:
            if isinstance(instance, Order):
                attachment_model = OrderAttachment(
                    file=file_data,
                    filename=file_data.name,
                    filesize=file_data.size,
                    uploader=request.user
                    if request.user.is_authenticated else None,
                    order=instance,
                    order_line=instance_line
                )
            else:
                attachment_model = BasketAttachment(
                    file=file_data,
                    filename=file_data.name,
                    filesize=file_data.size,
                    uploader=request.user
                    if request.user.is_authenticated else None,
                    basket=instance,
                    basket_line=instance_line
                )
            attachment_model.save()
            instance_line.attachments.add(attachment_model)
        except Exception as e:
            return self._handle_update_failed(self.kwargs['line_id'], str(e))

        return Response(status=status.HTTP_200_OK)

    def get_object(self):
        basket = super().get_object()
        if hasattr(basket, 'order'):
            return basket.order
        return basket
