from apps.b3_api.attachment.attachment_base_serializer import \
    AttachmentBaseSerializer
from apps.b3_order.models import OrderAttachment


class OrderAttachmentSerializer(AttachmentBaseSerializer):

    class Meta:
        model = OrderAttachment
        fields = AttachmentBaseSerializer.Meta.fields
