from django.conf.urls import url

from apps.b3_api.services.projects.attachments.views \
    import ChangeProjectAttachmentView, ProjectLineAttachmentView
from apps.b3_api.services.projects.views import ListServiceProjects, \
    ChangeServiceProjectsStatus, ServiceProjectDetail, \
    ServiceProjectPriceConfirm
from apps.b3_api.services.projects.lines.views import ChangeLinePricingView

urlpatterns = [
    url(r'^$', ListServiceProjects.as_view()),
    url(r'^(?P<project_id>\d+)/$', ServiceProjectDetail.as_view()),
    url(r'^(?P<project_id>\d+)/price/$', ServiceProjectPriceConfirm.as_view()),
    url(
        r'^(?P<project_id>\d+)/lines/(?P<line_id>\d+)$',
        ChangeLinePricingView.as_view()
    ),
    url(
        r'^(?P<project_id>\d+)/lines/(?P<line_id>\d+)/attachments/$',
        ProjectLineAttachmentView.as_view()
    ),
    url(
        r'^(?P<project_id>\d+)/attachments/(?P<attachment_id>\d+)$',
        ChangeProjectAttachmentView.as_view()
    ),
    url(
        r'^(?P<project_id>\d+)/status/$', ChangeServiceProjectsStatus.as_view()
    ),
]
