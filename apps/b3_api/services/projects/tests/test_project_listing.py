from django.utils import timezone

from apps.b3_api.utils.tests.cross_organization_util import \
    CrossOrganizationUtil
from apps.b3_order.factories import OrderFactory
from apps.basket.models import Basket
from apps.b3_tests.factories import PartnerFactory, get_current_site, \
    SiteFactory, OrganizationFactory, UserFactory, BasketFactory, \
    StockRecordFactory, StlFileFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase
from apps.b3_organization.utils import set_current_site


class ProjectListingTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(ProjectListingTest, self).setUp()
        self.partner = PartnerFactory()
        self.partner.site = get_current_site()
        self.partner.users.add(self.user)
        self.partner.save()

    def create_basket_for_date(self, created_date, submitted_date):
        basket = BasketFactory(site_id=self.site.id, owner=self.user)
        basket.created_date = created_date
        basket.date_submitted = submitted_date
        basket.save()

        stockrecord = StockRecordFactory(partner=self.partner)
        line1, created = basket.add_product(
            product=stockrecord.product, stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        line1.save()
        return basket

    def create_order_for_basket(self, basket, order_date):
        order = OrderFactory(
            project=basket,
        )
        order.save()
        return order

    def test_ordering_of_projects_and_orders_listing(self):
        date_now = timezone.now()

        # Create two orders 5 days ago
        date_a = date_now + timezone.timedelta(days=5)
        basket_1 = self.create_basket_for_date(
            created_date=date_a, submitted_date=date_a)
        basket_1.pricing_status = Basket.NO_REQUEST
        basket_1.status = Basket.SUBMITTED
        basket_1.save()
        self.create_order_for_basket(basket_1, order_date=date_a)

        basket_2 = self.create_basket_for_date(
            created_date=date_a, submitted_date=date_a)
        basket_2.pricing_status = Basket.NO_REQUEST
        basket_2.status = Basket.SUBMITTED
        basket_2.save()
        self.create_order_for_basket(basket_2, order_date=date_a)

        # Now create a manually priced basket
        date_b = date_now + timezone.timedelta(days=4)
        basket_3 = self.create_basket_for_date(
            created_date=date_b, submitted_date=date_b)
        basket_3.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        basket_3.status = Basket.EDITABLE
        basket_3.save()

        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )

        # I should have the manually priced basket on top
        self.assertEqual(projects_response_raw[0]['id'], basket_3.id)

        # Now order this manually priced basket later
        date_c = date_now + timezone.timedelta(days=3)
        basket_3.status = Basket.SUBMITTED
        basket_3.pricing_status = Basket.MANUALLY_PRICED
        self.create_order_for_basket(basket_3, order_date=date_c)

        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertEqual(projects_response_raw[0]['id'], basket_3.id)

        # Now a normal order is placed at a later date, and this should be on
        # top
        date_d = date_now + timezone.timedelta(days=2)
        basket_4 = self.create_basket_for_date(
            created_date=date_d, submitted_date=date_d)
        basket_4.pricing_status = Basket.NO_REQUEST
        basket_4.status = Basket.SUBMITTED
        basket_4.save()
        self.create_order_for_basket(basket_4, order_date=date_d)

        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertEqual(projects_response_raw[0]['id'], basket_4.id)

    def test_cross_projects_listed_and_correctly_displayed(self):
        """
        If an order is made on a site 'site1.3yourmind.com' by a supplier
        belonging to 'site2.3yourmind.com' the order should always show
        correctly on the service panel project listing for the supplier
        """
        # Set up partner
        partner = self.partner
        stockrecord = StockRecordFactory(partner=partner)
        stockrecord.save()

        # Enable partner on another site
        new_site = SiteFactory(
            domain='alternative.my.3yd',
            name='Alternative Organization',
        )
        new_org = OrganizationFactory(site=new_site)
        new_org.partners_enabled.add(partner)
        set_current_site(new_site)

        # Create a user
        user = UserFactory(password=self.test_password)

        # Order on site as normal
        CrossOrganizationUtil.create_basket_and_order(
            site=partner.site,
            user=user, partner=partner)

        # Order from same supplier on another site
        CrossOrganizationUtil.create_basket_and_order(
            site=new_site,
            user=user, partner=partner)

        # Test Orders are present on
        # Reponse self.assertEqual(2, len(project_response))
        url = f'/api/v2.0/services/{partner.id}/projects/'
        project_response = self.client.get(url)
        for project in project_response:
            self.assertIsNotNone(project['order'])
