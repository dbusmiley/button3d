import json
from django.utils import timezone

from apps.b3_address.models import Address
from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory, CountryFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceProjectOwnerListingTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectOwnerListingTest, self).setUp()
        site = get_current_site()
        basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket.site = self.site
        basket.save()

        self.partner = PartnerFactory()
        self.partner.site = site
        self.partner.users.add(self.user)
        self.partner.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        self.basket = BasketFactory(site_id=self.site.id, owner=self.user)
        self.basket.save()
        self.line1, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line2.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()
        self.date_time_of_order = timezone.now()

        self.order = OrderFactory(
            project=self.basket,
            site=self.site,
            purchased_by=self.user,
            shipping_method=shipping_method,
        )
        self.order.save()

    def test_user_address_line3(self):
        user_addr = Address.objects.create(
            user=self.user,
            country=CountryFactory(),
            company_name='TestCompany'
        )
        projects_net_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertEqual(
            json.loads(projects_net_response_raw.content
                       )[0]['owner']['company'], 'TestCompany'
        )
        user_addr.company_name = '3yourmind'
        user_addr.save()
        projects_net_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertEqual(
            json.loads(projects_net_response_raw.content
                       )[0]['owner']['company'], '3yourmind'
        )
