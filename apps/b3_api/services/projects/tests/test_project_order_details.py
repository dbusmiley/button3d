from django.utils import timezone
from django.utils.dateformat import format

from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory
from apps.b3_organization.utils import get_current_site

from apps.basket.models import Basket
from apps.b3_order.models import Order


class ServiceProjectOrdersListDetails(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectOrdersListDetails, self).setUp()

        self.partner = PartnerFactory()
        self.partner.site = get_current_site()
        self.partner.users.add(self.user)
        self.partner.save()

        self.shipping_method = ShippingMethodFactory(partner=self.partner)
        self.shipping_method.save()
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        self.basket = BasketFactory(owner=self.user)
        self.basket.save()
        self.line1, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line1.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

    def test_service_project_before_order_creation_time(self):
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertIsNone(projects_response_raw.data[0]['order'])

    def test_service_project_after_order_creation_time(self):
        # Check if the right time was retrieved on the projects API
        self.date_time_of_order = timezone.now()
        order = OrderFactory(
            project=self.basket,
            site=self.site,
            purchased_by=self.user,
        )
        order.save()
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        order = Order.objects.get(id=order.id)
        self.assertEqual(
            projects_response_raw.data[0]['order']['orderDate'],
            int(format(order.datetime_placed, 'U'))
        )
        self.assertEqual(
            projects_response_raw.data[0]['order']['orderId'], order.id
        )
        self.assertEqual(
            projects_response_raw.data[0]['order']['number'], order.number
        )

    def test_service_project_order_shipping_details(self):
        order = OrderFactory(
            project=self.basket,
            site=self.site,
            purchased_by=self.user,
            shipping_method=self.shipping_method,
        )
        order.save()
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        order = Order.all_objects.get(id=order.id)
        self.assertEqual(
            projects_response_raw.data[0]['order']['shipping']['exclusiveTax'],
            order.shipping_price.excl_tax
        )
        self.assertEqual(
            projects_response_raw.data[0]['order']['shipping']['inclusiveTax'],
            order.shipping_price.incl_tax
        )

    def test_service_project_with_no_order_shipping_details(self):
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/'
        )
        self.assertIsNone(projects_response_raw.data[0]['order'])
