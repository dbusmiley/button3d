import json
from decimal import Decimal
from django.core import mail

from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceProjectConfirmPricingEndpointsTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectConfirmPricingEndpointsTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.users.add(self.user)

        self.partner.save()
        self.manual_price_rule = {
            "totalPrice": {
                "enabled": True,
                "value": 1
            },
            "totalQuantity": {
                "enabled": False,
                "value": None
            },
            "itemTotalPrice": {
                "enabled": False,
                "value": None
            }
        }

    def create_manual_pricing_rule(self, stockrecord, manual_price_rule):
        self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/offers/{stockrecord.id}/manual-price/',
            data=json.dumps(manual_price_rule),
            content_type="application/json"
        )

    def test_project_details_with_a_line(self):
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        stockrecord.clear_manual_pricing_rules()
        stockrecord.save()

        self.assertEqual(len(stockrecord.get_manual_pricing_rules()), 0)
        self.create_manual_pricing_rule(
            stockrecord=stockrecord, manual_price_rule=self.manual_price_rule)

        self.assertNotEqual(len(stockrecord.get_manual_pricing_rules()), 0)

        self.line, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory()
        )
        self.line.save()
        self.basket.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        self.basket.save()

        pricing = {"price": 123}

        self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}'
            f'/lines/{self.line.id}',
            data=json.dumps(pricing),
            content_type="application/json"
        )
        self.assertEqual(
            self.basket.price_object.as_dict()['excl_tax'], Decimal('123.00')
        )

        self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}/price/',
            data={},
            content_type="application/json"
        )

        self.assertEqual(self.basket.first_line.can_be_ordered, True)

    def test_email_sent_on_manual_pricing_confirm(self):
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        stockrecord.clear_manual_pricing_rules()
        stockrecord.save()
        self.create_manual_pricing_rule(
            stockrecord=stockrecord, manual_price_rule=self.manual_price_rule)

        self.line, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory()
        )
        self.line.save()
        self.basket.pricing_status = Basket.WAITING_FOR_MANUAL_PRICING
        self.basket.save()

        pricing = {"price": 123}

        # set price
        self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}'
            f'/lines/{self.line.id}',
            data=json.dumps(pricing),
            content_type="application/json"
        )

        # confirm price
        self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}/price/',
            data={},
            content_type="application/json"
        )

        # Confirm a mail was sent
        expected_mail_body = 'Hello,Your 3D project has been manually priced.'
        self.assertIn(expected_mail_body, mail.outbox[0].body)

        # Final sanity check
        self.assertEqual(self.basket.first_line.can_be_ordered, True)
