from django.core.files.uploadedfile import SimpleUploadedFile
from django.test.client import MULTIPART_CONTENT

from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory
from apps.b3_organization.utils import get_current_site

from apps.basket.models import Basket


class ServiceProjectOrdersListDetails(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectOrdersListDetails, self).setUp()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = get_current_site()
        self.partner.users.add(self.user)
        self.partner.save()

        self.shipping_method = ShippingMethodFactory(partner=self.partner)
        self.shipping_method.save()
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        self.basket = BasketFactory(site_id=self.site.id,
                                    owner=self.user,
                                    status=Basket.EDITABLE)
        self.basket.save()
        self.line1, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line1.save()

        self.basket.save()

    def test_attachment_upload(self):
        # Upload a file to the BASKET
        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/projects/{self.basket.id}' \
            f'/lines/{self.line1.id}/attachments/'

        attachment = SimpleUploadedFile(
            'attachment.pdf', 'SomePdfContent'.encode('utf-8'),
            content_type='application/pdf')

        data = {
            'file': attachment,
            'basket_id': self.basket.pk
        }

        json_response = self.client.post(
            url, data, return_json=True, content_type=MULTIPART_CONTENT)

        self.assertEqual(json_response.status_code, 200)

        url = f'/api/v2.0/services/{self.partner.id}' \
            f'/projects/{self.basket.id}/'
        json_response = self.client.get(url)

        self.assertEqual(json_response.status_code, 200)

        self.assertEqual(
            json_response.data["lines"][0]["attachments"][0]["name"],
            "attachment.pdf"
        )
