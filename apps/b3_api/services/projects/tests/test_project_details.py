from apps.b3_api.utils.tests.cross_organization_util import \
    CrossOrganizationUtil
from apps.b3_organization.utils import set_current_site
from apps.b3_tests.factories import SiteFactory, OrganizationFactory, \
    UserFactory, get_current_site, PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedAjaxTestCase


class ProjectDetailsTest(AuthenticatedAjaxTestCase):
    def setUp(self):
        super(ProjectDetailsTest, self).setUp()

    def test_cross_organization_project_detail(self):
        partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        partner.site = get_current_site()
        partner.users.add(self.user)
        partner.save()

        site = SiteFactory(
            domain='alternative.my.3yd',
            name='Alternative Organization',
        )
        new_org = OrganizationFactory(site=site)
        new_org.partners_enabled.add(partner)
        set_current_site(site)

        new_user = UserFactory(password=self.test_password)
        new_basket, _ = CrossOrganizationUtil.create_basket_and_order(
            site=new_org.site,
            user=new_user,
            partner=partner)

        url = f'/api/v2.0/services/{partner.id}/projects/{new_basket.id}/'
        project_response = self.client.get(url)
        self.assertEqual(project_response['status'], 'Pending')
