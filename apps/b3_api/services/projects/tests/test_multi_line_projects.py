import json
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceProjectMultiLineTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectMultiLineTest, self).setUp()
        self.site = get_current_site()

        self.basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        self.basket.site = self.site
        self.basket.save()

        self.partner = PartnerFactory()
        self.partner.site = self.site
        self.partner.users.add(self.user)
        self.partner.save()

        self.stockrecord = StockRecordFactory(partner=self.partner)

        self.line1, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        self.line2.save()
        self.basket.save()

    @staticmethod
    def parse_result_to_get_tax_info(response):
        order_discounts = response[0]['order']['discounts']
        return order_discounts['exclusiveTax'], order_discounts['inclusiveTax']

    def test_multi_line_order_change(self):
        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        # need an order associated with this.
        order = OrderFactory(
            project=self.basket,
            purchased_by=self.user,
            site=self.site
        )
        order.save()
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/{self.basket.id}/'
        )
        self.assertEqual(projects_response_raw.data['status'], 'Pending')

        # Now update the status
        status_printing = {
            "content": '<html><body><p>Test</p></body></html>',
            "status": 'PRINTING'
        }
        projects_response_raw = self.client.post(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}/status/',
            data=json.dumps(status_printing),
            content_type='application/json'
        )

        project_response = json.loads(projects_response_raw.content)

        self.assertEqual(project_response['success'], True)

        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}'
            f'/projects/{self.basket.id}/'
        )
        self.assertEqual(projects_response_raw.data['status'], 'Printing')

    def test_model_configurations_are_included(self):
        def get_line_data(container, line_id):
            return next(line_data for line_data in container
                        if int(line_data['name'][-1]) == line_id)

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()

        # need an order associated with this.
        order = OrderFactory(
            project=self.basket,
            purchased_by=self.user,
            site=self.site
        )
        order.save()
        projects_response_raw = self.client.get(
            f'/api/v2.0/services/{self.partner.id}/projects/{self.basket.id}/'
        )
        project_data = projects_response_raw.data
        lines_data = project_data['lines']
        line_1_data = get_line_data(lines_data, 1)
        self.assertEquals(
            f'{self.line1.stl_file.parameter.volume:.4f}',
            line_1_data['model']['volume']
        )
        self.assertEquals(
            f'{self.line1.stl_file.parameter.area:.4f}',
            line_1_data['model']['area']
        )
        self.assertEquals(
            self.line1.stl_file.parameter.faces, line_1_data['model']['faces']
        )
        self.assertEquals(
            self.line1.stl_file.parameter.shells,
            line_1_data['model']['shells']
        )
        self.assertEquals(
            self.line1.stl_file.parameter.holes, line_1_data['model']['holes']
        )
        self.assertEquals(
            self.line1.configuration.unit, line_1_data['unit']
        )
