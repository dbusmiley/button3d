import json
from django.utils import timezone

from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket
from apps.b3_organization.utils import get_current_site
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class ServiceProjectMultiLineNetFiguresTest(AuthenticatedTestCase):
    def setUp(self):
        super(ServiceProjectMultiLineNetFiguresTest, self).setUp()
        site = get_current_site()
        basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket.site = self.site
        basket.save()

        self.partner = PartnerFactory()
        self.partner.site = site
        self.partner.users.add(self.user)
        self.partner.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        stockrecord = StockRecordFactory(partner=self.partner)
        stockrecord.save()
        self.basket = BasketFactory(site_id=self.site.id, owner=self.user)
        self.basket.save()
        self.line1, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line1.save()

        self.line2, created = self.basket.add_product(
            product=stockrecord.product,
            stockrecord=stockrecord,
            stlfile=StlFileFactory(),
        )
        self.line2.save()

        self.basket.pricing_status = Basket.NO_REQUEST
        self.basket.status = Basket.SUBMITTED
        self.basket.save()
        self.date_time_of_order = timezone.now()

        self.order = OrderFactory(
            project=self.basket,
            site=self.site,
            purchased_by=self.user,
            shipping_method=shipping_method,
            datetime_placed=self.date_time_of_order,
            voucher=None
        )
        self.order.save()

    def test_multi_line_order_change(self):
        projects_net_response_raw = self.client.get(
            '/api/v2.0/services/%d/figures/net-volume/' % (self.partner.id)
        )
        dir(self.line1)
        self.assertIn(
            {
                "day":
                str(self.date_time_of_order.date()),
                "value":
                float(
                    (self.line1.price_excl_tax + self.line2.price_excl_tax)
                )
            }, json.loads(projects_net_response_raw.content)
        )
