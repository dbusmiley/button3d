from django.shortcuts import get_object_or_404

from apps.b3_api.services.sequences.serializers.\
    sequence_status_update_serializer import \
    SequenceStatusUpdateSerializer
from apps.b3_mes.models.sequence import Sequence
from apps.b3_api.services.views import BaseMESView
import button3d.type_declarations as td

import typing as t

from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response


# TODO: can possibly use BulkUpdateFailAllMixin
class SequenceBulkUpdateView(BaseMESView, UpdateAPIView):
    serializer_class = SequenceStatusUpdateSerializer

    def update(
        self, request: td.DjangoAuthenticatedRequest, *args, **kwargs
    ) -> td.DjangoResponse:
        instances = self.get_objects()
        serializers = []
        for instance in instances:
            serializer = self.get_serializer(instance, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializers.append(serializer)

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need
                # to forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

        response_data = []
        for serializer in serializers:
            self.perform_update(serializer)
            response_data.append(serializer.data)

        return Response(response_data)

    def get_objects(self) -> t.Sequence[td.Sequence]:
        """
        Returns the objects the view is updating.
        """
        objs = []
        ids = self.request.data.get('ids')
        queryset = self.get_queryset()
        for id in ids:
            filter_kwargs = {'id': id}
            obj = get_object_or_404(queryset, **filter_kwargs)

            # May raise a permission denied
            self.check_object_permissions(self.request, obj)

            objs.append(obj)

        return objs

    def get_queryset(self) -> t.Sequence[td.Sequence]:
        """
        Get the list of items for this view.
        This must be an iterable, and may be a queryset.
        """
        partner = self.get_partner()
        queryset = Sequence.objects.filter(order__partner=partner)
        return queryset
