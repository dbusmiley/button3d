from rest_framework import serializers
from drf_payload_customizer.mixins import PayloadConverterMixin

from apps.b3_mes.models.sequence import Sequence
import button3d.type_declarations as td


class SequenceSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    project_id = serializers.IntegerField(source='basket.id')
    order_number = serializers.CharField(source='order.number')
    order_id = serializers.PrimaryKeyRelatedField(
        source='order', read_only=True
    )
    part_id = serializers.PrimaryKeyRelatedField(source='part', read_only=True)
    job_id = serializers.IntegerField(source='job_number')
    status = serializers.SerializerMethodField()
    next_status = serializers.SerializerMethodField()
    last_updated = serializers.DateTimeField(source='modified')
    job_status = serializers.CharField()
    job_number = serializers.CharField()

    class Meta:
        model = Sequence
        fields = (
            'id',
            'project_id',
            'order_number',
            'order_id',
            'part_id',
            'job_id',
            'status',
            'next_status',
            'last_updated',
            'job_status',
            'job_number',
        )
        read_only_fields = (
            'id',
            'project_id',
            'order_number',
            'order_id',
            'part_id',
            'job_id',
            'status',
            'next_status',
            'last_updated',
            'job_status',
            'job_number',
        )

    @staticmethod
    def get_status(obj: td.Sequence) -> str:
        return obj.status.name if obj.status else ''

    @staticmethod
    def get_next_status(obj: td.Sequence) -> str:
        return obj.next_status.name if obj.next_status else ''
