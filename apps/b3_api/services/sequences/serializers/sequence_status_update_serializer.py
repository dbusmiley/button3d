from apps.b3_mes.models.sequence import Sequence

from rest_framework import serializers


class SequenceStatusUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sequence
        fields = ('id', 'status')
