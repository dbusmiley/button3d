from django.conf.urls import url

from apps.b3_api.services.sequences.views import SequenceBulkUpdateView

urlpatterns = [
    url(r"^$", SequenceBulkUpdateView.as_view(), name='sequence-bulk-update')
]
