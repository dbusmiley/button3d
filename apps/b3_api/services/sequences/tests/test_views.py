import json

from django.urls import reverse

from apps.b3_order.factories import OrderFactory
from apps.b3_tests.factories import PartnerFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories.services import (
    WorkflowTemplateWithStatusesFactory)

from rest_framework import status


class SequenceViewsTests(AuthenticatedTestCase):
    def setUp(self):
        super().setUp()
        self.service = PartnerFactory(mes_activated=True)
        self.user.partners.add(self.service)
        self.order = OrderFactory(partner=self.service)
        self.part = self.order.lines.first()
        self.workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)
        self.workflow_template.generate_workflow_and_sequences(self.part.id)

        self.sequence_ids = list(
            self.order.sequences.all().values_list('id', flat=True)
        )

        new_status = self.order.sequences.first().status.next
        payload = {
            'ids': self.sequence_ids,
            'status': new_status.id,
        }
        payload = json.dumps(payload)
        self.post_kwargs = {
            'data': payload,
            'content_type': 'application/json',
        }

        url_kwargs = {'service_id': self.service.pk}
        self.url = reverse('api_urls:sequence-bulk-update', kwargs=url_kwargs)

    def test_bulk_update(self):
        response = self.client.put(self.url, **self.post_kwargs)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
