from apps.b3_api.services.sequences.serializers.sequence_serializer import \
    SequenceSerializer
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.utils import assert_serializer_structure
from apps.b3_tests.testcases.common_testcases import TestCase
from apps.b3_tests.factories.services import \
    WorkflowTemplateWithStatusesFactory
from apps.b3_api.services.sequences.serializers.\
    sequence_status_update_serializer import \
    SequenceStatusUpdateSerializer
from apps.b3_tests.factories import PartnerFactory


class SequenceStatusUpdateSerializerTests(TestCase):
    def setUp(self):
        super().setUp()

        self.service = PartnerFactory()
        self.order = OrderFactory(partner=self.service)
        self.part = self.order.lines.first()
        self.workflow_template = WorkflowTemplateWithStatusesFactory(
            partner=self.service)
        self.workflow_template.generate_workflow_and_sequences(self.part.id)

        self.sequence = self.part.sequences.first()

    def test_structure(self):
        serializer_populated = SequenceStatusUpdateSerializer(
            self.sequence
        )
        structure_expected = {'id': 0, 'status': 0}
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )

    def test_sequence_serializer_structure(self):
        serializer_populated = SequenceSerializer(self.sequence)
        structure_expected = {
            'id': 0,
            'projectId': 0,
            'orderNumber': 0,
            'orderId': 0,
            'partId': 0,
            'jobId': 0,
            'status': 'string',
            'nextStatus': 'string',
            'lastUpdated': 'string',
            'jobStatus': 'string',
            'jobNumber': 'string',
        }
        assert_serializer_structure(
            self, serializer_populated, structure_expected
        )
