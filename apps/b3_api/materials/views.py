from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.materials.serializers import MaterialSerializer
from apps.catalogue.models.product import Product


class ListMaterials(ListAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = MaterialSerializer
    not_offered_by_service_query_param = 'notOfferedByService'

    def get_queryset(self):
        exclude_by = {}
        service_id_to_exclude = self.request.query_params.get(
            self.not_offered_by_service_query_param, None
        )
        if service_id_to_exclude:
            try:
                exclude_by = {
                    'stockrecords__partner__id': int(service_id_to_exclude)
                }
            except ValueError:
                pass
        return Product.objects.all().exclude(**exclude_by)
