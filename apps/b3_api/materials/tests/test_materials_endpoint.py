import json

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    ProductFactory


class MaterialsEndpointTest(AuthenticatedTestCase):
    def test_query_params_for_exclusion(self):
        product = ProductFactory()
        response = self.client.get('/api/v2.0/materials/')
        self.assertEquals(len(json.loads(response.content)), 1)

        partner = PartnerFactory()
        StockRecordFactory(partner=partner, product=product)
        response = self.client.get(
            f'/api/v2.0/materials/?notOfferedByService={partner.id}'
        )

        self.assertEquals(len(json.loads(response.content)), 0)
