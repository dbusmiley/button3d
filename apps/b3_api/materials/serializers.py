from rest_framework import serializers

from apps.catalogue.models.product import Product


class MaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'id',
            'title',
        )
