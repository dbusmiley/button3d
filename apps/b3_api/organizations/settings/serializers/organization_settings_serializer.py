from django.conf import settings
from django.template.defaultfilters import title
from rest_framework import serializers

from drf_payload_customizer.mixins import PayloadConverterMixin
from apps.b3_migration.models import Switch
from apps.b3_organization.models import Organization, OrganizationTheme


class OrganizationThemeSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    class Meta:
        model = OrganizationTheme
        fields = '__all__'


class OrganizationSettingsSerializer(
    PayloadConverterMixin, serializers.ModelSerializer
):
    showname = serializers.SerializerMethodField()
    is_single_supplier_account = serializers.BooleanField()
    primary_theme_color = serializers.CharField(
        source='theme.primary_bkg_color', default=''
    )
    theme = OrganizationThemeSerializer()
    use_new_user_panel = serializers.SerializerMethodField()
    use_left_navigation = serializers.SerializerMethodField()
    show_net_prices = serializers.BooleanField()
    vat_id_label = serializers.SerializerMethodField()
    stripe_publishable_key = serializers.SerializerMethodField()

    def get_showname(self, obj):
        return title(obj.showname)

    def get_use_new_user_panel(self, obj):
        return Switch.is_active(Switch.NEW_USER_PANEL)

    def get_use_left_navigation(self, obj):
        return Switch.is_active(Switch.LEFT_NAVIGATION)

    def get_vat_id_label(self, obj):
        return {
            'en': obj.vat_id_label_en,
            'de': obj.vat_id_label_de
        }

    def get_stripe_publishable_key(self, obj):
        return settings.STRIPE_PUBLISHABLE_KEY

    class Meta:
        model = Organization
        fields = (
            'showname',
            'is_single_supplier_account',
            'company_information_visibility',
            'vat_id_label',
            'primary_theme_color',
            'theme',
            'show_net_prices',
            'use_new_user_panel',
            'use_left_navigation',
            'stripe_publishable_key'
        )
