from rest_framework import permissions
from rest_framework.generics import RetrieveAPIView

from apps.b3_api.organizations.settings.serializers\
    .organization_settings_serializer import \
    OrganizationSettingsSerializer
from apps.b3_organization.utils import get_current_org


class OrganizationSettingsView(RetrieveAPIView):
    """
    A response to show what toggles/settings are currently active for the
    organization/site.
    """
    permission_classes = (permissions.AllowAny,)

    serializer_class = OrganizationSettingsSerializer

    def get_object(self):
        obj = get_current_org()
        return obj
