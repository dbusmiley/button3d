from apps.b3_organization.utils import get_current_org
from apps.b3_tests.factories import PartnerFactory, SiteFactory, \
    OrganizationFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase


class OrganizationServiceListTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationServiceListTest, self).setUp()
        other_site = SiteFactory(
            domain='other.domain',
            name='other'
        )
        other_org = OrganizationFactory(site=other_site)
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.our_partner = PartnerFactory(
            name='OurPartner', logo='test-logo.png'
        )
        self.our_partner.site = self.org.site
        self.our_partner.save()

        self.other_partner = PartnerFactory(
            name='OtherPartner', logo='test-logo.png'
        )
        self.other_partner.site = other_org.site
        self.other_partner.save()

    def test_only_services_from_organization_listed(self):
        organization_services_response = self.client.get(
            '/api/v2.0/organization-panel/services/'
        )
        service_ids = [
            service['id']
            for service in organization_services_response.data
        ]
        self.assertEqual([self.our_partner.id], service_ids)
