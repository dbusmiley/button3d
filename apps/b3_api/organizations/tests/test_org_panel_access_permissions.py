from datetime import timedelta
from django.utils import timezone

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory

from apps.b3_organization.utils import get_current_site, get_current_org


class OrganizationAPIAccessPermissionsTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationAPIAccessPermissionsTest, self).setUp()
        self.site = get_current_site()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.root_url = '/api/v2.0/organization-panel/metrics/'

    def test_api_access_without_permission(self):
        users_kpi_response = self.client.get(
            '{0}users/active/amount/?dateFrom={1}&dateTo={2}'.
            format(
                self.root_url,
                (timezone.now() - timedelta(days=5)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                timezone.now().isoformat().replace('+00:00', 'Z')
            )
        )
        self.assertEqual(users_kpi_response.status_code, 403)
        self.assertEqual(
            users_kpi_response.data['code'],
            'ORGANIZATION_OBJECT_ACCESS_DENIED'
        )

    def test_api_access_with_permission(self):
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()
        users_kpi_response = self.client.get(
            '{0}users/active/amount/?dateFrom={1}&dateTo={2}'.
            format(
                self.root_url,
                (timezone.now() - timedelta(days=5)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                timezone.now().isoformat().replace('+00:00', 'Z')
            )
        )
        self.assertEqual(users_kpi_response.status_code, 200)
