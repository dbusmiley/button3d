from rest_framework import permissions

from apps.b3_api.errors.api_responses import error_object_access_denied_user
from apps.b3_organization.utils import get_current_org


class IsCurrentOrganizationAdmin(permissions.BasePermission):
    message = 'You do not have access to view this object'

    def has_permission(self, request, view):
        current_org = get_current_org()
        if request.user not in current_org.organization_panel_admins.all():
            # Use our custom message instead
            self.message = error_object_access_denied_user(
                object_name='ORGANIZATION_OBJECT',
                object_id='ORGANIZATION PANEL',
                user_id=request.user.id
            )
            return False
        return True
