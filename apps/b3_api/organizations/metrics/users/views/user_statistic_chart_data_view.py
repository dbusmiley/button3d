from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.metrics.users.serializers \
    import UserStatisticsChartDataSerializer
from apps.b3_api.organizations.utils import ChartDataView
from apps.b3_organization.tracking.trackers import UserInteractionTracker, \
    RegistrationStateTracker
from apps.b3_organization.tracking.utils import REGISTRATION
from apps.b3_organization.utils import get_current_site


class UserStatisticsChartDataView(ChartDataView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin
    )
    statistics_serializer_class = UserStatisticsChartDataSerializer

    def get_statistics(self, start, end, aggregation_type, **kwargs):
        organization = get_current_site().organization
        user_interaction_tracker = UserInteractionTracker(organization)
        registration_state_tracker = RegistrationStateTracker(organization)

        unique_interaction_stats = user_interaction_tracker.get_aggregated(
            start, end, aggregation_type, unique_actors=True
        )
        registration_state_stats = registration_state_tracker.get_aggregated(
            start, end, aggregation_type
        )

        def merge_statistics(interactions, registration_states):
            interval, active = interactions
            interval, state_dict = registration_states
            return {
                'interval': interval,
                'active': len(active),
                'firstStep': len(state_dict[REGISTRATION.FIRST_STEP]),
                'registered': len(state_dict[REGISTRATION.REGISTERED])
            }

        return list(map(
            merge_statistics,
            unique_interaction_stats,
            registration_state_stats
        ))
