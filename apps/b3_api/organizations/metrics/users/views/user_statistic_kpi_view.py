from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.utils import KPIView
from apps.b3_organization.tracking.trackers import UserInteractionTracker
from apps.b3_organization.utils import get_current_org


class UserStatisticsKPIView(KPIView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )

    def get_kpi_for_time_frame(self, start, end):
        user_interaction_tracker = UserInteractionTracker(
            organization=get_current_org()
        )
        _, active_users = next(
            user_interaction_tracker.get_aggregated(start, end)
        )
        return len(active_users)
