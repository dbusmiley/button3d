# flake8: noqa
from .user_statistic_chart_data_view import UserStatisticsChartDataView
from .user_statistic_kpi_view import UserStatisticsKPIView
