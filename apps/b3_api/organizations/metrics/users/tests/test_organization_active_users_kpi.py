from datetime import timedelta
from decimal import Decimal

from django.utils import timezone

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, UserFactory, \
    UserInteractionFactory

from apps.b3_organization.utils import get_current_site, get_current_org


class OrganizationUsersKPITest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationUsersKPITest, self).setUp()
        self.site = get_current_site()
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.user.save()
        self.now = timezone.now()

    def test_users_kpi_response_with_no_previous_period(self):
        UserInteractionFactory(
            user=self.user,
            timestamp=self.now - timedelta(days=2)
        )

        users_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'users/active/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (self.now - timedelta(days=3)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                (self.now - timedelta(days=1)).isoformat().replace(
                    '+00:00', 'Z'
                )
            )
        )
        self.assertEqual(users_kpi_response.data['value'], 1)
        # Here, there wont be any users in the previous timeframe, so it
        # would be null
        self.assertEqual(users_kpi_response.data['changePercentage'], None)

    def test_users_kpi_response_with_multiple_users_no_previous_period(self):
        # Add two users in the current period
        user1 = UserFactory()
        user1.userprofile.site = self.site
        user1.save()
        UserInteractionFactory(
            user=user1,
            timestamp=self.now - timedelta(days=2)
        )

        user2 = UserFactory()
        user2.userprofile.site = self.site
        user2.save()
        UserInteractionFactory(
            user=user2,
            timestamp=self.now - timedelta(days=3)
        )

        users_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'users/active/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (self.now - timedelta(days=4)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                (self.now - timedelta(days=1)).isoformat().replace(
                    '+00:00', 'Z'
                )
            )
        )
        self.assertEqual(users_kpi_response.data['value'], 2)
        # Here, there wont be any users in the previous timeframe, so it
        # would be null
        self.assertEqual(users_kpi_response.data['changePercentage'], None)

    def test_users_kpi_response_with_multiple_users_previous_period(self):
        # Add two users in the current period
        user1 = UserFactory()
        user1.userprofile.site = self.site
        user1.save()
        UserInteractionFactory(
            user=user1,
            timestamp=self.now - timedelta(days=2)
        )

        user2 = UserFactory()
        user2.userprofile.site = self.site
        user2.save()
        UserInteractionFactory(
            user=user2,
            timestamp=self.now - timedelta(days=3)
        )

        # Add 3 users in the last period
        user3 = UserFactory()
        user3.userprofile.site = self.site
        user3.save()
        UserInteractionFactory(
            user=user3,
            timestamp=self.now - timedelta(days=5)
        )

        user4 = UserFactory()
        user4.userprofile.site = self.site
        user4.save()
        UserInteractionFactory(
            user=user4,
            timestamp=self.now - timedelta(days=6)
        )

        user5 = UserFactory()
        user5.userprofile.site = self.site
        user5.save()
        UserInteractionFactory(
            user=user5,
            timestamp=self.now - timedelta(days=6)
        )

        users_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'users/active/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (timezone.now() - timedelta(days=4)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                (timezone.now() - timedelta(days=1)).isoformat().replace(
                    '+00:00', 'Z'
                ),
            )
        )
        self.assertEqual(users_kpi_response.data['value'], 2)
        # The manually calculated change percentage
        change_percentage = Decimal(((2 - 3) / 3.0) * 100)
        self.assertAlmostEqual(
            users_kpi_response.data['changePercentage'], change_percentage
        )
