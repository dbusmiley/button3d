from datetime import timedelta, datetime
from actstream.models import timezone
from dateutil.relativedelta import relativedelta
from django.utils.http import urlencode

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import UserFactory, UserInteractionFactory, \
    SignupFactory, SignupFirstStepStateChangeFactory, \
    SignupRegisteredStateChangeFactory
from apps.b3_organization.utils import get_current_org


class OrganizationUserChartDataTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationUserChartDataTest, self).setUp()
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.now = datetime(
            year=1990, month=1, day=1, tzinfo=timezone.now().tzinfo
        )

        # Create user interactions
        users = [UserFactory() for _ in range(10)]
        for i, user in enumerate(users):
            user.userprofile.site = self.site
            user.save()
            for j in range(i + 1):
                UserInteractionFactory(
                    user=user,
                    timestamp=self.now - timedelta(days=j)
                )

        # Create signup state changes
        signups = [SignupFactory() for _ in range(10)]
        for i, signup in enumerate(signups):
            for j in range(i + 1):
                SignupFirstStepStateChangeFactory(
                    signup=signup,
                    timestamp=self.now - timedelta(days=j)
                )
                if i % 2:
                    SignupRegisteredStateChangeFactory(
                        signup=signup,
                        timestamp=self.now - timedelta(days=j, seconds=-1)
                    )

    def test_user_chart_data_daily(self):
        # If dateFrom is omitted, the last 30 days are used as default
        # The default timeAggregationType is 'daily'
        params = urlencode({
            'dateTo': (self.now + timedelta(days=1)).isoformat(),
            'timeAggregationType': 'daily'
        })
        user_char_data_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/users/chart-data/?{params}'
        )
        for i, interval in enumerate(user_char_data_response.data[:-11]):
            self.assertEqual(interval['active'], 0)
            self.assertEqual(interval['firstStep'], 0)
            self.assertEqual(interval['registered'], 0)

        for i, interval in enumerate(user_char_data_response.data[-11:]):
            self.assertEqual(interval['active'], i)
            self.assertEqual(interval['firstStep'], i//2)
            self.assertEqual(interval['registered'], i//2 + i % 2)

    def test_user_chart_data_weekly(self):
        # If dateFrom is omitted, the last 30 days are used as default
        params = urlencode({
            'dateTo': (self.now + timedelta(days=1)).isoformat(),
            'timeAggregationType': 'weekly'
        })
        user_char_data_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/users/chart-data/?{params}'
        )

        last_week = user_char_data_response.data[-1]
        week_before_last_week = user_char_data_response.data[-2]
        all_weeks_before = user_char_data_response.data[:-2]

        self.assertEqual(last_week['active'], 10)
        self.assertEqual(last_week['firstStep'], 5)
        self.assertEqual(last_week['registered'], 5)

        self.assertEqual(week_before_last_week['active'], 3)
        self.assertEqual(week_before_last_week['firstStep'], 1)
        self.assertEqual(week_before_last_week['registered'], 2)

        for interval in all_weeks_before:
            self.assertEqual(interval['active'], 0)
            self.assertEqual(interval['firstStep'], 0)
            self.assertEqual(interval['registered'], 0)

    def test_user_chart_data_monthly(self):
        params = urlencode({
            'dateFrom': (
                self.now + timedelta(days=1) - relativedelta(months=2)
            ).isoformat(),
            'dateTo': (self.now + timedelta(days=1)).isoformat(),
            'timeAggregationType': 'monthly'
        })
        user_char_data_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/users/chart-data/?{params}'
        )

        last_month = user_char_data_response.data[-1]
        month_before_last_month = user_char_data_response.data[-2]

        self.assertEqual(last_month['active'], 10)
        self.assertEqual(last_month['firstStep'], 5)
        self.assertEqual(last_month['registered'], 5)

        self.assertEqual(month_before_last_month['active'], 0)
        self.assertEqual(month_before_last_month['firstStep'], 0)
        self.assertEqual(month_before_last_month['registered'], 0)

    def test_user_chart_data_yearly(self):
        params = urlencode({
            'dateFrom': (
                self.now + timedelta(days=1) - relativedelta(years=2)
            ).isoformat(),
            'dateTo': (self.now + timedelta(days=1)).isoformat(),
            'timeAggregationType': 'monthly'
        })
        user_char_data_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/users/chart-data/?{params}'
        )

        last_year = user_char_data_response.data[-1]
        year_before_last_year = user_char_data_response.data[-2]

        self.assertEqual(last_year['active'], 10)
        self.assertEqual(last_year['firstStep'], 5)
        self.assertEqual(last_year['registered'], 5)

        self.assertEqual(year_before_last_year['active'], 0)
        self.assertEqual(year_before_last_year['firstStep'], 0)
        self.assertEqual(year_before_last_year['registered'], 0)
