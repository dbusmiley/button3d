from django.conf.urls import url

from apps.b3_api.organizations.metrics.users.views \
    import UserStatisticsChartDataView, UserStatisticsKPIView

urlpatterns = [
    url(r'^active/amount/', UserStatisticsKPIView.as_view()),
    url(r'^chart-data/', UserStatisticsChartDataView.as_view()),
]
