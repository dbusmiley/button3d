from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import Serializer, IntegerField


class UserStatisticsChartDataSerializer(Serializer):
    start = SerializerMethodField()
    end = SerializerMethodField()
    firstStep = IntegerField(read_only=True)
    active = IntegerField(read_only=True)
    registered = IntegerField(read_only=True)

    def get_start(self, instance):
        return instance['interval'].start.isoformat()

    def get_end(self, instance):
        return instance['interval'].end.isoformat()
