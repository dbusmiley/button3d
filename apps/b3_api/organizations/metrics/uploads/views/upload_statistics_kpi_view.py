from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.utils import KPIView
from apps.b3_core.models import StlFile
from apps.b3_organization.utils import get_current_org


class UploadStatisticsKPIView(KPIView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )

    def get_kpi_for_time_frame(self, start, end):
        return StlFile.objects.filter(
            site__organization=get_current_org(),
            creation_date__gte=start,
            creation_date__lte=end
        ).count()
