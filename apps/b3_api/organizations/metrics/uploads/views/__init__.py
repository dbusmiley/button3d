# flake8: noqa
from .upload_project_statistics_chart_data_view \
    import UploadProjectStatisticsChartDataView
from .upload_statistics_kpi_view import UploadStatisticsKPIView
