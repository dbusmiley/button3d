from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.metrics.uploads.serializers \
    import UploadStatisticsChartDataSerializer
from apps.b3_api.organizations.utils import ChartDataView
from apps.b3_core.models import StlFile
from apps.b3_organization.tracking.utils import get_time_intervals
from apps.b3_organization.utils import get_current_org
from apps.basket.models import Basket


class UploadProjectStatisticsChartDataView(ChartDataView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )
    statistics_serializer_class = UploadStatisticsChartDataSerializer

    @staticmethod
    def build_aggregation_buckets(start, end, aggregation_type):
        intervals = get_time_intervals(start, end, aggregation_type)
        return [
            {
                "interval": interval,
                "files": 0,
                "projects": 0
            } for interval in intervals
        ]

    @staticmethod
    def distribute_projects_among_buckets(baskets, aggregation_buckets):
        for basket in baskets:
            buckets = iter(aggregation_buckets)
            current_bucket = next(buckets)

            while basket.date_created >= current_bucket['interval'].end:
                try:
                    current_bucket = next(buckets)
                except StopIteration:
                    break

            current_bucket['projects'] += 1

    @staticmethod
    def distribute_files_among_buckets(stl_files, aggregation_buckets):
        for stl_file in stl_files:
            buckets = iter(aggregation_buckets)
            current_bucket = next(buckets)

            while stl_file.creation_date >= current_bucket['interval'].end:
                try:
                    current_bucket = next(buckets)
                except StopIteration:
                    break

            current_bucket['files'] += 1

    def get_statistics(self, start, end, aggregation_type, **kwargs):
        current_org = get_current_org()
        baskets = Basket.objects.filter(
            site__organization=current_org,
            date_created__gte=start,
            date_created__lte=end
        ).order_by('date_created')

        files = StlFile.objects.filter(
            site__organization=current_org,
            creation_date__gte=start,
            creation_date__lte=end
        ).order_by('creation_date')

        aggregation_buckets = self.build_aggregation_buckets(
            start, end, aggregation_type
        )
        self.distribute_projects_among_buckets(baskets, aggregation_buckets)
        self.distribute_files_among_buckets(files, aggregation_buckets)

        return aggregation_buckets
