from datetime import timedelta

from django.utils import timezone

from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    StlFileFactory, BasketFactory

from apps.basket.models import Basket
from dateutil import parser

from apps.b3_organization.utils import get_current_site, get_current_org


class OrganizationUploadStatisticsTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationUploadStatisticsTest, self).setUp()
        self.site = get_current_site()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.partner.site = self.site
        self.partner.save()

        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()
        self.user.save()
        self.now = timezone.now()

    def create_bulk_baskets(self, count, date, partner, site):
        baskets = []
        for i in range(count):
            basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
            basket.site = site
            basket.save()

            stlfile1 = StlFileFactory()
            stlfile1.creation_date = date
            stlfile1.save()
            stlfile2 = StlFileFactory()
            stlfile2.creation_date = date
            stlfile2.save()

            stockrecord = StockRecordFactory(partner=partner)
            line1, created = basket.add_product(
                product=stockrecord.product,
                stockrecord=stockrecord,
                stlfile=stlfile1
            )
            line1.date_created = date
            line1.save()
            line2, created = basket.add_product(
                product=stockrecord.product,
                stockrecord=stockrecord,
                stlfile=stlfile2
            )
            line2.date_created = date
            line2.save()
            shipping_method = ShippingMethodFactory(partner=partner)
            shipping_method.save()

            basket.date_created = date
            basket.save()
            baskets.append(basket)

        return baskets

    @staticmethod
    def get_items_for_date(date):
        return list(Basket.objects.filter(
            date_created=date
        ).distinct())

    @staticmethod
    def get_total_number_of_files_in_basket(baskets_list):
        return sum([len(basket.all_lines()) for basket in baskets_list])

    def get_result_for_date(self, results, date):
        for result in results:
            if parser.parse(result['end']) > date:
                return result

    def test_uploads_daily_statistics(self):
        date_a = self.now - timedelta(days=1)
        date_b = self.now - timedelta(days=2)
        date_c = self.now - timedelta(days=3)

        self.create_bulk_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        date_from = (timezone.now() - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        uploads_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        # since 5 days
        self.assertEqual(len(uploads_stats_response.data), 5)

        # Collect orders for dates
        date_a_baskets = self.get_items_for_date(date_a)

        # now make sure if the values makes sense
        date_a_results = self.get_result_for_date(
            uploads_stats_response.data, date_a
        )
        self.assertEqual(date_a_results['projects'], 3)

        self.assertEqual(
            date_a_results['files'],
            self.get_total_number_of_files_in_basket(date_a_baskets)
        )

        date_b_baskets = self.get_items_for_date(date_b)

        # now make sure if the values makes sense
        date_b_results = self.get_result_for_date(
            uploads_stats_response.data, date_b
        )
        self.assertEqual(date_b_results['projects'], 2)

        self.assertEqual(
            date_b_results['files'],
            self.get_total_number_of_files_in_basket(date_b_baskets)
        )

        date_c_baskets = self.get_items_for_date(date_c)

        # now make sure if the values makes sense
        date_c_results = self.get_result_for_date(
            uploads_stats_response.data, date_c
        )
        self.assertEqual(date_c_results['projects'], 4)

        self.assertEqual(
            date_c_results['files'],
            self.get_total_number_of_files_in_basket(date_c_baskets)
        )

    def test_uploads_weekly_statistics(self):
        # week 52
        date_a = parser.parse('2017-12-25T02:00:00.000Z')
        # week 52
        date_b = parser.parse('2017-12-26T02:00:00.000Z')
        # week 1
        date_c = parser.parse('2018-01-01T02:00:00.000Z')

        self.create_bulk_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        uploads_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-12-22T02:00:00.000Z'
            f'&dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=weekly'
        )

        # since there are 2 weeks here
        self.assertEqual(len(uploads_stats_response.data), 2)

        # Collect baskets for dates
        date_a_uploads = self.get_items_for_date(date_a)
        date_b_uploads = self.get_items_for_date(date_b)

        date_a_uploads_result = self.get_result_for_date(
            uploads_stats_response.data,
            date_a
        )

        # now make sure if the values makes sense
        self.assertEqual(date_a_uploads_result['projects'], 5)

        self.assertEqual(
            date_a_uploads_result['files'],
            self.get_total_number_of_files_in_basket(
                date_a_uploads + date_b_uploads
            )
        )

        date_c_uploads = self.get_items_for_date(date_c)
        # now make sure if the values makes sense
        date_c_uploads_results = self.get_result_for_date(
            uploads_stats_response.data,
            date_c
        )

        # now make sure if the values makes sense
        self.assertEqual(date_c_uploads_results['projects'], 4)

        self.assertEqual(
            date_c_uploads_results['files'],
            self.get_total_number_of_files_in_basket(date_c_uploads)
        )

    def test_uploads_monthly_statistics(self):
        """
        We are expecting the results to be for 2017-10-22T02:00:00+00:00,
        2017-11-22T02:00:00+00:00, 2017-12-22T02:00:00+00:00 here as the
        dateFrom and dateTo is adjusted accordingly
        """
        # month 11, 2017 (and see its 11-04, so will go to the result from
        # 2017-11-05T00:00:00+00:00)
        date_a = parser.parse('2017-11-04T02:00:00.000Z')
        # month 12, 2017
        date_b = parser.parse('2017-12-25T02:00:00.000Z')
        # month 12, 2017
        date_c = parser.parse('2017-12-26T02:00:00.000Z')

        self.create_bulk_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        uploads_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-10-22T02:00:00.000Z'
            f'&dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=monthly'
        )

        self.assertEqual(len(uploads_stats_response.data), 3)
        # Collect orders for dates
        date_a_uploads = self.get_items_for_date(date_a)

        date_a_uploads_results = self.get_result_for_date(
            uploads_stats_response.data,
            date_a
        )

        self.assertEqual(date_a_uploads_results['projects'], 3)

        # now make sure if the values makes sense
        self.assertEqual(
            date_a_uploads_results['files'],
            self.get_total_number_of_files_in_basket(date_a_uploads)
        )

        date_b_baskets = self.get_items_for_date(date_b)
        date_c_baskets = self.get_items_for_date(date_c)

        date_b_c_uploads_results = self.get_result_for_date(
            uploads_stats_response.data,
            date_c
        )

        self.assertEqual(date_b_c_uploads_results['projects'], 6)

        # now make sure if the values makes sense
        self.assertEqual(
            date_b_c_uploads_results['files'],
            self.get_total_number_of_files_in_basket(date_b_baskets) +
            self.get_total_number_of_files_in_basket(date_c_baskets)
        )

    def test_baskets_quarterly_statistics(self):
        """
        We are expecting the results to be for 2017-06-22T02:00:00+00:00,
        2017-09-22T02:00:00+00:00, 2017-12-22T02:00:00+00:00 here as the
        dateFrom and dateTo is adjusted accordingly
        """
        # month 11, 2017 - quarter (10 2017 to 01 2018)
        date_a = parser.parse('2017-11-04T02:00:00.000Z')
        # month 12, 2017 - quarter (10 2017 to 01 2018)
        date_b = parser.parse('2017-12-25T02:00:00.000Z')
        # month 09, 2017 - quarter (08 2017 to 10 2017)
        date_c = parser.parse('2017-09-15T02:00:00.000Z')

        self.create_bulk_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        uploads_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-06-22T02:00:00.000Z'
            f'&dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=quarterly'
        )

        self.assertEqual(len(uploads_stats_response.data), 3)

        # Collect orders for dates
        date_a_uploads = self.get_items_for_date(date_a)
        date_b_uploads = self.get_items_for_date(date_b)

        date_a_b_uploads_results = self.get_result_for_date(
            uploads_stats_response.data,
            parser.parse('2017-12-22T02:00:00+00:00')
        )

        # now make sure if the values makes sense
        self.assertEqual(date_a_b_uploads_results['projects'], 5)
        self.assertEqual(
            date_a_b_uploads_results['files'],
            self.get_total_number_of_files_in_basket(date_a_uploads) +
            self.get_total_number_of_files_in_basket(date_b_uploads)
        )
        date_c_uploads = self.get_items_for_date(date_c)

        date_c_uploads_results = self.get_result_for_date(
            uploads_stats_response.data,
            parser.parse('2017-09-22T02:00:00+00:00')
        )

        # now make sure if the values makes sense
        self.assertEqual(date_c_uploads_results['projects'], 4)
        self.assertEqual(
            date_c_uploads_results['files'],
            self.get_total_number_of_files_in_basket(date_c_uploads)
        )

    def test_baskets_yearly_statistics(self):
        """
        We are expecting the results to be for 2016-01-04T02:00:00+00:00,
        2017-01-04T02:00:00+00:00, 2018-01-04T02:00:00+00:00 here as the
        dateFrom and dateTo is adjusted accordingly
        """
        # month 11, 2017
        date_a = parser.parse('2017-11-04T02:00:00.000Z')
        # month 12, 2017 - quarter (10 2017 to 01 2018)
        date_b = parser.parse('2017-12-25T02:00:00.000Z')
        # month 09, 2016 - quarter (08 2017 to 10 2017)
        date_c = parser.parse('2016-09-26T02:00:00.000Z')

        self.create_bulk_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self.create_bulk_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        uploads_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2016-01-04T02:00:00.000Z'
            f'&dateTo=2018-01-05T02:00:00.000Z'
            f'&timeAggregationType=yearly'
        )

        self.assertEqual(len(uploads_stats_response.data), 3)
        # Collect orders for dates
        date_a_uploads = self.get_items_for_date(date_a)
        date_b_uploads = self.get_items_for_date(date_b)

        date_a_b_uploads_result = self.get_result_for_date(
            uploads_stats_response.data,
            parser.parse('2018-01-04T02:00:00+00:00')
        )

        self.assertEqual(date_a_b_uploads_result['projects'], 5)
        self.assertEqual(
            date_a_b_uploads_result['files'],
            self.get_total_number_of_files_in_basket(date_a_uploads) +
            self.get_total_number_of_files_in_basket(date_b_uploads)
        )

        date_c_uploads = self.get_items_for_date(date_c)
        date_c_uploads_result = self.get_result_for_date(
            uploads_stats_response.data,
            parser.parse('2017-01-04T02:00:00+00:00')
        )

        self.assertEqual(date_c_uploads_result['projects'], 4)
        self.assertEqual(
            date_c_uploads_result['files'],
            self.get_total_number_of_files_in_basket(date_c_uploads)
        )
