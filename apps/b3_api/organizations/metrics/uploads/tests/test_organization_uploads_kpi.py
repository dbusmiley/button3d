from datetime import timedelta
from decimal import Decimal

from django.utils import timezone

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory

from apps.basket.models import Basket

from apps.b3_organization.utils import get_current_site, get_current_org


class OrganizationUploadsKPITest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationUploadsKPITest, self).setUp()
        self.site = get_current_site()
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.stockrecord = StockRecordFactory(partner=self.partner)
        self.now = timezone.now()

    def test_files_kpi_response_with_no_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=1)
            )
        )
        line1.date_created = self.now - timedelta(days=1)
        line1.save()
        basket1.save()

        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        files_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        self.assertEqual(files_kpi_response.data['value'], 1)
        # Here, there wont be any files in the previous timeframe, so it
        # would be null
        self.assertEqual(files_kpi_response.data['changePercentage'], None)

    def test_files_kpi_response_with_multiple_files_no_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=2)
            )
        )
        line1.date_created = self.now - timedelta(days=2)
        line1.save()
        basket1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.site = self.site
        line2, created = basket2.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=3)
            )
        )
        line2.date_created = self.now - timedelta(days=3)
        line2.save()
        basket2.save()

        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        files_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        self.assertEqual(files_kpi_response.data['value'], 2)
        # Here, there wont be any files in the previous timeframe, so it
        # would be null
        self.assertEqual(files_kpi_response.data['changePercentage'], None)

    def test_projects_kpi_response_with_multiple_files_previous_period(self):
        # 2 in current period
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=2)
            )
        )
        line1.date_created = self.now - timedelta(days=2)
        line1.save()
        basket1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.site = self.site

        line2, created = basket2.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=3)
            )
        )
        line2.date_created = self.now - timedelta(days=3)
        line2.save()
        basket2.save()

        # now add 3 in the previous period as well
        basket3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket3.site = self.site

        line3, created = basket3.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=10)
            )
        )
        line3.date_created = self.now - timedelta(days=10)
        line3.save()
        basket3.save()

        basket4 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket4.site = self.site

        line4, created = basket4.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=11)
            )
        )
        line4.date_created = self.now - timedelta(days=11)
        line4.save()
        basket4.save()

        basket5 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket5.site = self.site

        line5, created = basket5.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory(
                creation_date=self.now - timedelta(days=11)
            )
        )
        line5.date_created = self.now - timedelta(days=11)
        line5.save()
        basket5.save()

        date_from = (self.now - timedelta(days=7)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        files_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/uploads/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        self.assertEqual(files_kpi_response.data['value'], 2)

        # The manually calculated change percentage
        change_percentage = Decimal(((2 - 3) / 3.0) * 100)
        self.assertAlmostEqual(
            files_kpi_response.data['changePercentage'], change_percentage
        )
