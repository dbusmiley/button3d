from django.conf.urls import url

from apps.b3_api.organizations.metrics.uploads.views \
    import UploadProjectStatisticsChartDataView, UploadStatisticsKPIView

urlpatterns = [
    url(r'^amount/', UploadStatisticsKPIView.as_view()),
    url(r'^chart-data/', UploadProjectStatisticsChartDataView.as_view())
]
