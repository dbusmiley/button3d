from rest_framework import serializers
from rest_framework.serializers import Serializer


class UploadStatisticsChartDataSerializer(Serializer):
    start = serializers.SerializerMethodField()
    end = serializers.SerializerMethodField()
    files = serializers.IntegerField(read_only=True)
    projects = serializers.IntegerField(read_only=True)

    def get_start(self, instance):
        return instance['interval'].start.isoformat()

    def get_end(self, instance):
        return instance['interval'].end.isoformat()
