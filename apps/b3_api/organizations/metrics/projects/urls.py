from django.conf.urls import url

from apps.b3_api.organizations.metrics.projects.views import \
    ProjectStatisticsKPIView

urlpatterns = [
    url(r'^amount/', ProjectStatisticsKPIView.as_view()),
]
