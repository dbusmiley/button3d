from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_organization.utils import get_current_org
from apps.basket.models import Basket
from apps.b3_api.organizations.utils import KPIView


class ProjectStatisticsKPIView(KPIView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )

    def get_kpi_for_time_frame(self, start, end):
        return Basket.objects.filter(
            site__organization=get_current_org(),
            date_created__gte=start,
            date_created__lte=end
        ).count()
