from datetime import timedelta
from decimal import Decimal

from django.utils import timezone

from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory

from apps.basket.models import Basket

from apps.b3_organization.utils import get_current_site, get_current_org


class OrganizationProjectsKPITest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationProjectsKPITest, self).setUp()
        self.site = get_current_site()
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.stockrecord = StockRecordFactory(partner=self.partner)

    def test_projects_kpi_response_with_no_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.date_created = timezone.now() - timedelta(days=1)
        basket1.site = self.site
        basket1.save()

        projects_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'projects/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (timezone.now() -
                 timezone.timedelta(days=5)).isoformat().replace(
                     '+00:00', 'Z'
                ),
                timezone.now().isoformat().replace('+00:00', 'Z')
            )
        )
        self.assertEqual(projects_kpi_response.data['value'], 1)
        # Here, there wont be any projects in the previous timeframe, so it
        # would be null
        self.assertEqual(projects_kpi_response.data['changePercentage'], None)

    def test_projects_kpi_response_with_multiple_projects_no_previous_period(
        self
    ):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.date_created = timezone.now() - timedelta(days=2)
        basket1.site = self.site
        basket1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.date_created = timezone.now() - timedelta(days=3)
        basket2.site = self.site
        basket2.save()

        projects_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'projects/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (timezone.now() - timedelta(days=5)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                timezone.now().isoformat().replace('+00:00', 'Z')
            )
        )
        self.assertEqual(projects_kpi_response.data['value'], 2)
        # Here, there wont be any projects in the previous timeframe, so it
        # would be null
        self.assertEqual(projects_kpi_response.data['changePercentage'], None)

    def test_projects_kpi_response_with_multiple_projects_previous_period(
        self
    ):
        # 2 in current period
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.date_created = timezone.now() - timedelta(days=2)
        basket1.site = self.site
        basket1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.date_created = timezone.now() - timedelta(days=3)
        basket2.site = self.site
        basket2.save()

        # now add 3 in the previous period as well
        basket3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket3.date_created = timezone.now() - timedelta(days=10)
        basket3.site = self.site
        basket3.save()

        basket4 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket4.date_created = timezone.now() - timedelta(days=11)
        basket4.site = self.site
        basket4.save()

        basket5 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket5.date_created = timezone.now() - timedelta(days=11)
        basket5.site = self.site
        basket5.save()

        projects_kpi_response = self.client.get(
            '/api/v2.0/organization-panel/metrics/' +
            'projects/amount/?dateFrom={0}&dateTo={1}'.
            format(
                (timezone.now() - timedelta(days=7)).isoformat().replace(
                    '+00:00', 'Z'
                ),
                timezone.now().isoformat().replace('+00:00', 'Z')
            )
        )
        self.assertEqual(projects_kpi_response.data['value'], 2)

        # The manually calculated change percentage
        change_percentage = Decimal(((2.0 - 3.0) / 3.0) * 100)
        self.assertAlmostEqual(
            projects_kpi_response.data['changePercentage'], change_percentage
        )
