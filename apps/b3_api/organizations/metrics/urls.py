from django.conf.urls import url, include

from apps.b3_api.organizations.views import ListOrganizationServices

urlpatterns = [
    url(r'^orders/', include('apps.b3_api.organizations.metrics.orders.urls')),
    url(r'^uploads/',
        include('apps.b3_api.organizations.metrics.uploads.urls')),
    url(r'^users/',
        include('apps.b3_api.organizations.metrics.users.urls')),
    url(r'^projects/',
        include('apps.b3_api.organizations.metrics.projects.urls')),
    url(r'^services/', ListOrganizationServices.as_view()),
]
