from decimal import Decimal

from django.conf import settings
from django.http import Http404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.b3_api.organizations.metrics.orders.serializers \
    import OrderStatisticsChartDataSerializer
from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.utils import ChartDataView
from apps.b3_core.utils import get_exchange_rate
from apps.b3_organization.tracking.trackers import OrderStateTracker
from apps.b3_organization.tracking.utils import ORDER
from apps.b3_organization.utils import get_current_site
from apps.partner.models import Partner


class OrderStatisticsChartDataView(ChartDataView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )
    statistics_serializer_class = OrderStatisticsChartDataSerializer

    def get(self, *args, **kwargs):
        query_params = self.parse_query_params()
        statistics = self.get_statistics(**query_params)
        currency = settings.DEFAULT_CURRENCY
        try:
            currency = self.request.query_params['currency']
        except KeyError:
            pass

        exchange_rates = {}
        for currency_iterate in settings.SUPPORTED_CURRENCY_SET:
            exchange_rates[currency_iterate] = Decimal(get_exchange_rate(
                from_c=currency_iterate, to_c=currency))

        return Response(
            self.statistics_serializer_class(
                statistics,
                many=True,
                context={
                    'exchange_rates': exchange_rates,
                }
            ).data
        )

    def parse_query_params(self):
        query_params = super(
            OrderStatisticsChartDataView, self
        ).parse_query_params()

        service_id = self.request.query_params.get('serviceId', None)
        printing_service = Partner.objects.filter(id=service_id).first()
        if service_id and printing_service is None:
            raise Http404('Invalid serviceId')

        query_params['printing_service'] = printing_service
        return query_params

    def get_statistics(self, start, end, aggregation_type, **kwargs):
        partner = kwargs['printing_service']
        organization = get_current_site().organization
        order_state_tracker = OrderStateTracker(
            organization=organization,
            partner=partner
        )

        order_state_stats = order_state_tracker.get_aggregated(
            start, end, aggregation_type
        )

        return [
            {
                'interval': interval,
                'ordered': state_dict[ORDER.PENDING],
                'printing': state_dict[ORDER.PRINTING],
                'delivered': state_dict[ORDER.SHIPPED],
                'cancelled': state_dict[ORDER.CANCELLED]
            }
            for interval, state_dict in order_state_stats
        ]
