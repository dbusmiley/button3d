import button3d.type_declarations as td
from datetime import datetime
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.organizations.utils import KPIView
from apps.b3_api.utils.currency_util import get_currency_or_default, \
    get_total_exclusive_tax
from apps.b3_order.models import Order
from apps.b3_organization.utils import get_current_org


class OrderStatisticsKPIView(KPIView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )

    def get_kpi_for_time_frame(self,
                               start: datetime, end: datetime) -> td.Decimal:
        currency = get_currency_or_default(self.request.query_params)

        orders = Order.objects.filter(
            site__organization=get_current_org(),
            datetime_placed__gte=start,
            datetime_placed__lte=end
        )
        currency_aggregates_qs = orders.values_list(
            'currency', 'total_value', 'shipping_value'
        )

        total_exclusive_tax = get_total_exclusive_tax(
            currency=currency,
            currency_aggregates_qs=currency_aggregates_qs
        )
        return total_exclusive_tax
