from django.conf.urls import url
from apps.b3_api.organizations.metrics.orders.views \
    import OrderStatisticsChartDataView, OrderStatisticsKPIView

urlpatterns = [
    url(r'^amount/', OrderStatisticsKPIView.as_view()),
    url(r'^chart-data/', OrderStatisticsChartDataView.as_view()),
]
