from datetime import timedelta
import datetime
from decimal import Decimal
from unittest import skipIf

from actstream.models import actor_stream
from django.utils import timezone
from django.db import connection

from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory
from apps.basket.models import Basket

from apps.b3_order.models import Order
from dateutil import parser

from apps.b3_organization.utils import get_current_site, get_current_org


@skipIf(
    connection.vendor == 'microsoft',
    'Feature does not work with MS-SQL'
)
class OrganizationOrdersStatisticsTest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationOrdersStatisticsTest, self).setUp()
        self.site = get_current_site()

        self.partner = PartnerFactory()
        self.partner.site = self.site
        self.partner.save()

        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()
        self.user.save()
        self.now = timezone.now()

    def get_items_for_date(self, date, service_id):
        return list(Order.objects.filter(
            partner_id=int(service_id),
            datetime_placed__range=(
                datetime.datetime.combine(date, datetime.time.min).replace(
                    tzinfo=timezone.get_current_timezone()
                ),
                datetime.datetime.combine(date, datetime.time.max).replace(
                    tzinfo=timezone.get_current_timezone()
                )
            )
        ).distinct())

    def get_total_value_of_orders(self, orders_list):
        total_excl_tax = 0
        for order in orders_list:
            total_excl_tax += order.total_without_shipping.excl_tax

        return Decimal(total_excl_tax)

    def get_result_for_date(self, results, date):
        for result in results:
            if parser.parse(result['end']) > date:
                return result

    def test_orders_daily_statistics(self):
        date_a = self.now - timedelta(days=3)
        date_b = self.now - timedelta(days=2)
        date_c = self.now - timedelta(days=1)

        self._create_bulk_orders_and_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        date_from = (datetime.datetime.now() -
                     timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = datetime.datetime.now().isoformat().replace('+00:00', 'Z')
        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        # since 5 days
        self.assertEqual(len(order_stats_response.data), 6)

        # Collect orders for dates
        date_a_orders = self.get_items_for_date(
            date_a.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_a
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders), 2)
            }
        )

        date_b_orders = self.get_items_for_date(
            date_b.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_b
            )['ordered'],
            {
                'amount': 5,
                'value': self.get_total_value_of_orders(
                    date_a_orders + date_b_orders
                )
            }
        )

        date_c_orders = self.get_items_for_date(
            date_c.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_c
            )['ordered'],
            {
                'amount': 9,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders + date_b_orders + date_c_orders
                ), 2)
            }
        )

    def test_with_multiple_partners(self):
        date = self.now - timedelta(days=1)

        partner_a = PartnerFactory()
        partner_a.site = self.site
        partner_a.save()

        self._create_bulk_orders_and_baskets(
            3, date, partner=self.partner, site=self.site
        )

        self._create_bulk_orders_and_baskets(
            2, date, partner=partner_a, site=self.site
        )
        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        # since 5 days
        self.assertEqual(len(order_stats_response.data), 5)

        # Collect orders for dates
        orders_partner_original = self.get_items_for_date(
            date.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    orders_partner_original
                ), 2)
            }
        )

        # Now get the order stats for other partner
        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={partner_a.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        orders_partner_a = self.get_items_for_date(
            date.date(), service_id=partner_a.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date
            )['ordered'],
            {
                'amount': 2,
                'value':
                round(self.get_total_value_of_orders(orders_partner_a), 2)
            }
        )

    def test_with_cancelled_orders(self):
        """
        What happens when an order is cancelled ? Will it show up ?
        :return:
        """
        date = self.now - timedelta(days=1)

        orders = self._create_bulk_orders_and_baskets(
            3, date, partner=self.partner, site=self.site
        )

        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')

        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        # since 5 days
        self.assertEqual(len(order_stats_response.data), 5)

        # Collect orders for dates
        orders_partner_original = self.get_items_for_date(
            date.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    orders_partner_original
                ), 2)
            }
        )
        # Now cancel an order from date_a
        self.populate_current_request()
        order_a_first_order = orders[0]
        order_a_first_order.set_status('Cancelled')
        # Set the timestamp of the cancel action directly after it was ordered
        cancelled_action = actor_stream(order_a_first_order).first()
        cancelled_action.timestamp = date + timedelta(seconds=1)
        cancelled_action.save()

        date_from = (self.now - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = self.now.isoformat().replace('+00:00', 'Z')
        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom={date_from}&dateTo={date_to}'
            f'&timeAggregationType=daily'
        )

        orders_partner_original = self.get_items_for_date(
            date.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date
            )['ordered'],
            {
                'amount': 2,
                'value': round(self.get_total_value_of_orders([
                    order for order in orders_partner_original
                    if order.status.type == 'Pending'
                ]), 2)
            }
        )

    def test_orders_weekly_statistics(self):
        # week 52
        date_a = parser.parse('2017-12-25T02:00:00.000Z')
        # week 52
        date_b = parser.parse('2017-12-26T02:00:00.000Z')
        # week 1
        date_c = parser.parse('2018-01-01T02:00:00.000Z')

        self._create_bulk_orders_and_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-12-22T02:00:00.000Z'
            f'&dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=weekly'
        )

        # The timespan between the two dates are exactly 2 weeks
        self.assertEqual(len(order_stats_response.data), 2)

        # Collect orders for dates
        date_a_orders = self.get_items_for_date(
            date_a.date(), service_id=self.partner.id
        )
        date_b_orders = self.get_items_for_date(
            date_b.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_a
            )['ordered'],
            {
                'amount': 5,
                'value': self.get_total_value_of_orders(
                    date_a_orders + date_b_orders
                )
            }
        )

        date_c_orders = self.get_items_for_date(
            date_c.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_c
            )['ordered'],
            {
                'amount': 9,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders + date_b_orders + date_c_orders
                ), 2)
            }
        )

    def test_orders_monthly_statistics(self):
        # month 11, 2017 (and see its 11-04, so will go to the result from
        # 2017-11-05T00:00:00+00:00)
        date_a = parser.parse('2017-11-04T02:00:00.000Z')
        # month 12, 2017
        date_b = parser.parse('2017-12-25T02:00:00.000Z')
        # month 12, 2017
        date_c = parser.parse('2017-12-26T02:00:00.000Z')

        self._create_bulk_orders_and_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-10-22T02:00:00.000Z&'
            f'dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=monthly'
        )

        self.assertEqual(len(order_stats_response.data), 3)
        # Collect orders for dates
        date_a_orders = self.get_items_for_date(
            date_a.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_a
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders), 2)
            }
        )

        date_b_orders = self.get_items_for_date(
            date_b.date(), service_id=self.partner.id
        )
        date_c_orders = self.get_items_for_date(
            date_c.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_b
            )['ordered'],
            {
                'amount': 9,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders + date_b_orders + date_c_orders
                ), 2)
            }
        )

    def test_orders_quarterly_statistics(self):
        # month 10, 2017 - quarter (08 2017 to 10 2017)
        date_a = parser.parse('2017-09-26T02:00:00.000Z')
        # month 11, 2017 - quarter (10 2017 to 01 2018)
        date_b = parser.parse('2017-11-04T02:00:00.000Z')
        # month 12, 2017 - quarter (10 2017 to 01 2018)
        date_c = parser.parse('2017-12-25T02:00:00.000Z')

        self._create_bulk_orders_and_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2017-06-22T02:00:00.000Z'
            f'&dateTo=2018-1-05T02:00:00.000Z'
            f'&timeAggregationType=quarterly'
        )

        self.assertEqual(len(order_stats_response.data), 3)
        # Collect orders for dates
        date_a_orders = self.get_items_for_date(
            date_a.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_a
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders), 2)
            }
        )
        date_b_orders = self.get_items_for_date(
            date_b.date(), service_id=self.partner.id
        )
        date_c_orders = self.get_items_for_date(
            date_c.date(), service_id=self.partner.id
        )

        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_b
            )['ordered'],
            {
                'amount': 9,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders + date_b_orders + date_c_orders
                ), 2)
            }
        )

    def test_orders_yearly_statistics(self):
        date_a = parser.parse('2016-09-26T02:00:00.000Z')
        date_b = parser.parse('2017-11-04T02:00:00.000Z')
        date_c = parser.parse('2017-12-25T02:00:00.000Z')

        self._create_bulk_orders_and_baskets(
            3, date_a, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            2, date_b, partner=self.partner, site=self.site
        )
        self._create_bulk_orders_and_baskets(
            4, date_c, partner=self.partner, site=self.site
        )

        order_stats_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/chart-data/'
            f'?serviceId={self.partner.id}'
            f'&dateFrom=2016-01-04T02:00:00.000Z'
            f'&dateTo=2018-01-05T02:00:00.000Z'
            f'&timeAggregationType=yearly'
        )

        self.assertEqual(len(order_stats_response.data), 3)

        # Collect orders for dates
        date_a_orders = self.get_items_for_date(
            date_a.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_a
            )['ordered'],
            {
                'amount': 3,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders), 2)
            }
        )

        date_b_orders = self.get_items_for_date(
            date_b.date(), service_id=self.partner.id
        )
        date_c_orders = self.get_items_for_date(
            date_c.date(), service_id=self.partner.id
        )
        # now make sure if the values makes sense
        self.assertEqual(
            self.get_result_for_date(
                order_stats_response.data, date_b
            )['ordered'],
            {
                'amount': 9,
                'value': round(self.get_total_value_of_orders(
                    date_a_orders + date_b_orders + date_c_orders
                ), 2)
            }
        )

    def _create_bulk_orders_and_baskets(self, count, date, partner, site):
        orders = []
        for i in range(count):
            basket = BasketFactory(owner=self.user, status=Basket.EDITABLE)
            basket.site = site
            basket.save()

            stockrecord = StockRecordFactory(partner=partner)
            line1, created = basket.add_product(
                product=stockrecord.product,
                stockrecord=stockrecord,
                stlfile=StlFileFactory()
            )
            line1.save()
            line2, created = basket.add_product(
                product=stockrecord.product,
                stockrecord=stockrecord,
                stlfile=StlFileFactory()
            )
            line2.save()
            shipping_method = ShippingMethodFactory(partner=partner)
            shipping_method.save()
            # need an order associated with this.
            order = OrderFactory(
                project=basket,
                purchased_by=self.user,
                shipping_method=shipping_method,
                datetime_placed=date,
            )
            order.site = site
            order.save()
            orders.append(order)
            for action in actor_stream(order):
                action.timestamp = date
                action.save()
        return orders
