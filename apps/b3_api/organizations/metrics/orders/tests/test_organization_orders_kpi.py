from datetime import timedelta

from django.utils import timezone

from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_order.factories import OrderFactory
from apps.b3_tests.testcases.common_testcases import AuthenticatedTestCase
from apps.b3_organization.utils import get_current_site, get_current_org
from apps.b3_tests.factories import PartnerFactory, StockRecordFactory, \
    BasketFactory, StlFileFactory
from apps.basket.models import Basket


class OrganizationOrdersKPITest(AuthenticatedTestCase):
    def setUp(self):
        super(OrganizationOrdersKPITest, self).setUp()
        self.site = get_current_site()
        self.org = get_current_org()
        self.org.organization_panel_admins.add(self.user)
        self.org.save()

        self.partner = PartnerFactory(
            name='Meltwerk', logo='meltwerk-logo.png'
        )
        self.stockrecord = StockRecordFactory(partner=self.partner)

    def test_orders_kpi_response_with_no_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site
        basket1.save()

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        order1 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=2)
        )
        order1.save()

        date_from = (timezone.now() - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = timezone.now().isoformat().replace('+00:00', 'Z')
        order_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        self.assertEqual(
            order_kpi_response.data['value'],
            order1.total_without_shipping.excl_tax
        )
        # Here, there wont be any orders in the previous timeframe, so it
        # would be null
        self.assertEqual(order_kpi_response.data['changePercentage'], None)

    def test_orders_kpi_response_with_multiple_orders_no_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site
        basket1.save()

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        order1 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=2)
        )
        order1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.site = self.site
        basket2.save()

        line1, created = basket2.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        order2 = OrderFactory(
            project=basket2,
            purchased_by=self.user,
            site=self.site,
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=3)
        )
        order2.save()

        date_from = (timezone.now() - timedelta(days=5)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = timezone.now().isoformat().replace('+00:00', 'Z')
        order_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        self.assertEqual(
            order_kpi_response.data['value'],
            order1.total_without_shipping.excl_tax
            + order2.total_without_shipping.excl_tax
        )
        # Here, there wont be any orders in the previous timeframe, so it
        # would be null
        self.assertEqual(order_kpi_response.data['changePercentage'], None)

    def test_orders_kpi_response_with_multiple_orders_previous_period(self):
        basket1 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket1.site = self.site
        basket1.save()

        line1, created = basket1.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        order1 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=2)
        )
        order1.save()

        basket2 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket2.site = self.site
        basket2.save()

        line1, created = basket2.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line1.save()

        order2 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=3)
        )
        order2.save()

        # now add something in the pevious period as well
        basket3 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket3.site = self.site
        basket3.save()

        line3, created = basket3.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line3.save()

        shipping_method = ShippingMethodFactory(partner=self.partner)
        shipping_method.save()
        # need an order associated with this.
        order3 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=10)
        )
        order3.save()

        basket4 = BasketFactory(owner=self.user, status=Basket.EDITABLE)
        basket4.site = self.site
        basket4.save()

        line4, created = basket4.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line4.save()
        line5, created = basket4.add_product(
            product=self.stockrecord.product,
            stockrecord=self.stockrecord,
            stlfile=StlFileFactory()
        )
        line5.save()

        order4 = OrderFactory(
            shipping_method=shipping_method,
            datetime_placed=timezone.now() - timedelta(days=11)
        )
        order4.save()

        date_from = (timezone.now() - timedelta(days=7)).isoformat().replace(
            '+00:00', 'Z'
        )
        date_to = timezone.now().isoformat().replace('+00:00', 'Z')
        order_kpi_response = self.client.get(
            f'/api/v2.0/organization-panel/metrics/orders/amount/'
            f'?dateFrom={date_from}&dateTo={date_to}'
        )
        current_period_total = order1.total_without_shipping.excl_tax \
            + order2.total_without_shipping.excl_tax
        self.assertEqual(
            order_kpi_response.data['value'],
            current_period_total
        )

        last_period_total = order3.total_without_shipping.excl_tax \
            + order4.total_without_shipping.excl_tax
        change_percentage = (
            (current_period_total - last_period_total) / last_period_total
        ) * 100
        self.assertAlmostEqual(
            order_kpi_response.data['changePercentage'], change_percentage
        )
