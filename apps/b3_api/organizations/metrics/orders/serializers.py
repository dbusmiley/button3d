from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import Serializer

from apps.b3_core.utils import quantize


class OrderStatisticsChartDataSerializer(Serializer):
    start = SerializerMethodField()
    end = SerializerMethodField()
    ordered = SerializerMethodField()
    printing = SerializerMethodField()
    delivered = SerializerMethodField()
    cancelled = SerializerMethodField()

    def get_start(self, instance):
        return instance['interval'].start.isoformat()

    def get_end(self, instance):
        return instance['interval'].end.isoformat()

    def get_ordered(self, instance):
        return self.get_value_amount_dict(instance['ordered'])

    def get_printing(self, instance):
        return self.get_value_amount_dict(instance['printing'])

    def get_delivered(self, instance):
        return self.get_value_amount_dict(instance['delivered'])

    def get_cancelled(self, instance):
        return self.get_value_amount_dict(instance['cancelled'])

    def get_value_amount_dict(self, orders):
        exchange_rates = self.context['exchange_rates']
        return {
            'value': quantize(
                sum(
                    exchange_rates[order.currency]
                    * (order.total_value - order.shipping_value)
                    for order in orders
                )
            ),
            'amount': len(orders)
        }
