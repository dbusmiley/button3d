from django.conf.urls import url, include

from apps.b3_api.organizations.views import ListOrganizationServices

urlpatterns = [
    url(r'^services/', ListOrganizationServices.as_view()),
    url(r'^metrics/',
        include('apps.b3_api.organizations.metrics.urls'),
        name="metrics"
        ),
]
