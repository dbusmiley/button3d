from decimal import Decimal

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from pytz import UTC
from rest_framework.response import Response
from rest_framework.views import APIView


class ChartDataView(APIView):
    statistics_serializer_class = None

    def parse_query_params(self):
        start, end = parse_start_end(self.request.query_params)

        aggregation_type = self.request.query_params.get(
            'timeAggregationType',
            None
        )

        return {
            'start': start,
            'end': end,
            'aggregation_type': aggregation_type
        }

    def get(self, *args, **kwargs):
        query_params = self.parse_query_params()
        statistics = self.get_statistics(**query_params)
        return Response(
            self.statistics_serializer_class(
                statistics,
                many=True
            ).data
        )

    def get_statistics(self, start, end, aggregation_type, **kwargs):
        raise NotImplemented('Child classes must implement get_statistics()')


class KPIView(APIView):
    def get(self, request, *args, **kwargs):
        start, end = parse_start_end(self.request.query_params)

        current_period_kpi = self.get_kpi_for_time_frame(start, end)
        last_period_start_date = start - (end - start)
        last_period_kpi = self.get_kpi_for_time_frame(
            last_period_start_date, start
        )

        return Response(
            {
                'value': current_period_kpi,
                'changePercentage': self.get_change_percentage(
                    current_period_kpi, last_period_kpi
                )
            }
        )

    @staticmethod
    def get_change_percentage(current_period_value, last_period_value):
        if last_period_value == 0:
            return None

        absolute_change = current_period_value - last_period_value
        relative_chage = absolute_change / Decimal(last_period_value)
        return relative_chage * 100

    def get_kpi_for_time_frame(self, start, end):
        raise NotImplemented(
            'Child classes must implement get_kpi_for_time_frame()'
        )


def parse_start_end(query_params):
    if 'dateTo' in query_params:
        end = parse(query_params['dateTo']).replace(tzinfo=UTC)
    else:
        end = timezone.now()

    if 'dateFrom' in query_params:
        start = parse(query_params['dateFrom']).replace(tzinfo=UTC)
    else:
        start = end - relativedelta(months=1)

    return start, end
