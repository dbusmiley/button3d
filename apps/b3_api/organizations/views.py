from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from apps.b3_api.organizations.permissions import IsCurrentOrganizationAdmin
from apps.b3_api.services.serializers import ServiceSerializer
from apps.b3_organization.utils import get_current_org
from apps.partner.models import Partner


class ListOrganizationServices(ListAPIView):
    permission_classes = (
        IsAuthenticated,
        IsCurrentOrganizationAdmin,
    )
    serializer_class = ServiceSerializer

    def get_queryset(self):
        return Partner.objects.filter(site__organization=get_current_org())
