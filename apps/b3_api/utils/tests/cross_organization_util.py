from django.utils import timezone
from apps.b3_order.factories import OrderFactory
from apps.b3_shipping.factories.shipping_method import ShippingMethodFactory
from apps.b3_tests.factories import BasketFactory, \
    StlFileFactory, StockRecordFactory
from apps.basket.models import Basket


class CrossOrganizationUtil:
    def __init__(self):
        pass

    @staticmethod
    def create_basket_and_order(site, user, partner):
        new_basket = BasketFactory(site_id=site.id,
                                   owner=user)
        new_basket.save()
        stock_record = StockRecordFactory(partner=partner)
        stock_record.save()
        new_line1, new_created = new_basket.add_product(
            product=stock_record.product,
            stockrecord=stock_record,
            stlfile=StlFileFactory())
        new_line1.save()

        new_basket.pricing_status = Basket.NO_REQUEST
        new_basket.status = Basket.SUBMITTED
        new_basket.save()

        shipping_method = ShippingMethodFactory(partner=partner)
        new_order = OrderFactory(
            project=new_basket,
            site_id=site.id,
            shipping_method=shipping_method,
            purchased_by=user,
            datetime_placed=timezone.now())
        new_order.save()
        return new_basket, new_order
