from apps.b3_api.utils.openapi import OpenApiSpecPatternResolver
from django.test import SimpleTestCase
from django.conf import settings
from unittest import expectedFailure


def get_resolver():
    return OpenApiSpecPatternResolver(
        spec_path=settings.OPENAPI_SPEC_FILE
    )


class OpenapiSpecPatternResolverTest(SimpleTestCase):

    @expectedFailure
    def test_init(self):
        resolver = get_resolver()
        self.assertIsNotNone(resolver.spec)
        self.assertEqual(resolver.spec_path, settings.OPENAPI_SPEC_FILE)
        self.assertEqual(
            resolver.spec_params['service_id'],
            '(?P<service_id>[0-9]+)'
        )

    @expectedFailure
    def test_replace_single(self):
        resolver = get_resolver()
        pattern = '/api/v2.0/services/(?P<service_id>[0-9]+)/payment-methods/'
        actual = resolver.transform_path_pattern(pattern)
        expected = '/api/v2.0/services/{service_id}/payment-methods/'
        self.assertEqual(actual, expected)

    @expectedFailure
    def test_replace_multi(self):
        resolver = get_resolver()
        pattern = r'/api/v2.0/service-panel/services/(?P<service_id>[0-9]+)/' \
                  r'payment-methods/(?P<payment_method_id>[0-9]+)/'
        actual = resolver.transform_path_pattern(pattern)
        expected = '/api/v2.0/service-panel/services/{service_id}/' \
                   'payment-methods/{payment_method_id}/'
        self.assertEqual(actual, expected)
