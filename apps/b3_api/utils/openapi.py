import typing as t
import os.path
import json
import logging

from openapi_spec_validator.schemas import read_yaml_file
from openapi_core.schema.specs.models import Spec
from openapi_core.shortcuts import create_spec
from datetime import datetime
from .urlpatterns import PatternCache

__all__ = (
    'generate_openapi_spec',
    'OpenApiSpecPatternResolver',
    'ReloadableSpecMixin',
)

logger = logging.getLogger(__name__)


def generate_openapi_spec(path) -> t.Tuple[Spec, datetime]:
    fname, ext = os.path.splitext(path)
    if ext in ('.yaml', '.yml'):
        spec_dict = read_yaml_file(path)
    else:
        stream = open(path, 'rt', encoding='utf-8')
        spec_dict = json.load(stream)

    spec = create_spec(spec_dict)
    last_mod = datetime.fromtimestamp(os.stat(path).st_mtime)
    return spec, last_mod


class ReloadableSpecMixin(object):
    def __init__(self, spec_path: str=None, *args, **kwargs):
        self.spec_path = spec_path
        self.spec, self.spec_updated = None, None

    def regenerate_spec_if_updated(self) -> bool:
        last_mod = datetime.fromtimestamp(os.stat(self.spec_path).st_mtime)
        if self.spec_updated is None or self.spec_updated < last_mod:
            logger.info('Specification outdated: Updating.')
            self.spec, self.spec_updated = generate_openapi_spec(
                self.spec_path)
            return True

        return False


class OpenApiSpecPatternResolver(ReloadableSpecMixin, PatternCache):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.spec_params = {}  # type: t.Dict[str, str]
        self.regenerate_spec_if_updated()
        self.build_parameter_dict()

    def get_pattern(self,
                    request_uri: str, trim_caret: bool = True,
                    trim_dollar: bool = True):
        """
        Get a pattern for a given uri.

        Looks for a match, then returns the path pattern as an "OpenAPI
        friendly" pattern. So the regex parts are turned into "path
        parameters".

        :param request_uri: the uri to match
        :param trim_caret: when a match is found, trim the leading caret
        :param trim_dollar: when a match is found, trim the trailing dollar
        :return: ``None`` or the matched pattern
        """
        self.regenerate_spec_if_updated()
        match = super().get_pattern(request_uri, trim_caret=trim_caret,
                                    trim_dollar=trim_dollar)

        # We have a match, if any regexes are in there, we need to replace
        # them with curly-braced path patterns.
        if match is not None:
            match = self.transform_path_pattern(match)

        return match

    def regenerate_spec_if_updated(self):
        updated = super().regenerate_spec_if_updated()
        if updated:
            self.build_parameter_dict()

    def transform_path_pattern(self, pattern: str) -> str:
        for name, regex in self.spec_params.items():
            if regex in pattern:
                pattern = pattern.replace(regex, '{' + name + '}')

        return pattern

    def build_parameter_dict(self):
        for ref, component in self.spec.components.parameters.items():
            self.spec_params[ref] = component['x-django-regex']
