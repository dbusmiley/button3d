import inflection

from rest_framework import fields, status
from rest_framework.response import Response
from rest_framework.utils.serializer_helpers import ReturnDict

__all__ = (
    'OldPayloadConverterMixin',
    'BulkUpdateFailAllModelMixin',
)


class DifferentOutInViewsetSerializerMixin:
    """
    Allows the use of different serializers

    For a given combination of request/response with the action, we can specify
    our serializer of choice.
    """
    request_serializer_update = None
    response_serializer_update = None

    request_serializer_create = None
    response_serializer_create = None

    def get_serializer_class(self):
        if self.action == 'update':
            return self.request_serializer_update
        elif self.action == 'create':
            return self.request_serializer_create
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        response_serializer = self.response_serializer_create(
            instance=serializer.instance
        )

        headers = self.get_success_headers(response_serializer.data)
        return Response(
            response_serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        response_serializer = self.response_serializer_update(
            instance=serializer.instance
        )
        return Response(response_serializer.data)


# noinspection PyUnresolvedReferences
class OldPayloadConverterMixin:

    """
    The version of PayloadConverterMixin that actually works with NestingMixin
    instead of introducing logic where a nested field does not get converted
    """

    def to_internal_value(self, camel_cased):
        blankified_dictionary = {}

        for key, value in camel_cased.items():
            is_character_field = isinstance(
                self.fields.get(inflection.underscore(key)),
                fields.CharField
            )
            blankified_dictionary[key] = (
                self._blankify(value) if is_character_field else value
            )

        return super().to_internal_value(
            self._to_snake_dict(blankified_dictionary)
        )

    def to_representation(self, instance):
        snake_cased = super().to_representation(instance)
        return self._to_camel_dict(self._nullify_dict(snake_cased))

    def _to_camel_dict(self, snake_cased):
        camel_cased = {}

        for snake_key, value in snake_cased.items():
            camel_key = inflection.camelize(
                str(snake_key),
                uppercase_first_letter=False
            )

            is_dict = isinstance(value, dict)
            camel_cased[camel_key] = \
                self._to_camel_dict(value) if is_dict else value

        return camel_cased

    def _to_snake_dict(self, camel_cased):
        snake_cased = {}

        for camel_key, value in camel_cased.items():
            snake_key = inflection.underscore(str(camel_key))

            is_dict = isinstance(value, dict)
            snake_cased[snake_key] = \
                self._to_snake_dict(value) if is_dict else value

        return snake_cased

    def _nullify_dict(self, dictionary):
        nullified_dict = {}

        for key, value in dictionary.items():
            is_dict = isinstance(value, dict)
            nullified_dict[key] = \
                self._nullify_dict(value) if is_dict else self._nullify(value)

        return nullified_dict

    @staticmethod
    def _blankify(value):
        return "" if value is None else value

    @staticmethod
    def _nullify(value):
        return None if value == "" else value

    @property
    def errors(self):
        ret = self._to_camel_dict(super().errors)
        return ReturnDict(ret, serializer=self)


class BulkUpdateFailAllModelMixin:
    """
    Bulk update model instances. If one update fails, all updates are not
    executed.

    `request.data` -> [{'id': 1, 'field': 'value'...}, ]

    Set `self.instance_identifier` to the identifier of the
    object, defaults to 'id'
    """
    instance_identifier = 'id'

    def bulk_update(self, request, *args, **kwargs):
        validated_serializers = []
        invalid_response_data = []
        for instance_dict in request.data:
            partial = kwargs.pop('partial', False)

            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

            self.kwargs[lookup_url_kwarg] = instance_dict[
                self.instance_identifier]

            instance = self.get_object()
            serializer = self.get_serializer(
                instance, data=instance_dict, partial=partial)

            if serializer.is_valid():
                self.perform_update(serializer)
                validated_serializers.append(serializer)
            else:
                invalid_response_data.append(
                    {
                        'id': instance.id,
                        'errors': serializer.errors
                    })

            if getattr(instance, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need
                # to forcibly invalidate the prefetch cache on the instance.
                instance._prefetched_objects_cache = {}

        if invalid_response_data:
            return Response(
                invalid_response_data,
                status=status.HTTP_400_BAD_REQUEST
            )

        [serializer.save() for serializer in validated_serializers]
        return Response(
            [serializer.data for serializer in validated_serializers])
