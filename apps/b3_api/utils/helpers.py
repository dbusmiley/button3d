from button3d import type_declarations as td
from django.db import models


def underscore_to_camelcase(string: str) -> str:
    string_parts = string.split("_")
    return string_parts[0] + "".join(x.title() for x in string_parts[1:])


def camelcase_to_underscore(string: str)-> str:
    if not string:
        return string
    underscore = string[0]
    for item in string[1:]:
        if item.isupper():
            underscore += "_" + item.lower()
        else:
            underscore += item
    return underscore


def underscore_dict_to_camelcase(
    dictionary: td.StringKeyDict
) -> td.StringKeyDict:
    return {
        underscore_to_camelcase(key):
            underscore_dict_to_camelcase(val)
            if isinstance(val, dict)
            else val
        for key, val in dictionary.items()
    }


def camelcase_dict_to_underscore(
    dictionary: td.StringKeyDict
) -> td.StringKeyDict:
    return {
        camelcase_to_underscore(key):
            camelcase_dict_to_underscore(val)
            if isinstance(val, dict)
            else val
        for key, val in dictionary.items()
    }


def update_model_using_dict(
    instance: models.Model,
    update_dict: td.StringKeyDict
) -> None:
    """
    Update :model instance: `instance` based on :dict: `update_dict`
    :param instance: model instance
    :param update_dict: dictionary of field names and their new values
    """
    for attr, value in update_dict.items():
        setattr(instance, attr, value)
    instance.save()


def dict_from_class(cls) -> td.StringKeyDict:
    """
    Create a dictionary from a class instance
    """
    return dict(
        (key, value)
        for (key, value) in cls.__dict__.items()
    )
