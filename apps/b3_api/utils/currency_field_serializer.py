from django.conf import settings
from rest_framework import serializers


class CurrencyField(serializers.DecimalField):
    """Serializer to be used in conjunction with CurrencyField model field"""

    def __init__(
        self,
        max_digits=settings.CURRENCY_FIELD_DIGITS,
        decimal_places=settings.CURRENCY_FIELD_DECIMAL_PLACES,
        coerce_to_string=None,
        max_value=None,
        min_value=None,
        localize=False,
        rounding=None,
        **kwargs
    ):

        super(CurrencyField, self).__init__(
            max_digits,
            decimal_places,
            coerce_to_string,
            max_value,
            min_value,
            localize,
            rounding,
            **kwargs
        )
