from collections import OrderedDict
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer


class NestingMixin:
    """
    This helper can be used to return a nested representations of a Model.
    To use the feature, a sublass has to specify the nesting_options Meta
    attribute. The attribute should have the following structure:

    class Meta:
        nesting_options =  [
              {
                'name': name of the nested object,
                'translations': [
                    (field name of model, field name in nested representation),
                    ...
                ]
            },
            { ... }
        ]

    Example:
        Standard representation:
            {
                'field1': 'x',
                'field2': 'y',
                'field3': 'z'
                'field4': 'a'
            }

        Nested representation:
            {
                'field1': 'x',
                'firstNestedFields': {
                    'nestedField2': 'y'
                },
                'secondNestedFields': {
                    'nestedField3': 'z',
                    'nestedNestedField': {
                        'nestedField4': 'w'
                    }
                }
            }

        Required Options:
            class Meta:
                nesting_options = [
                    {
                        'name': 'firstNestedFields',
                        'translations': [
                            ('field2', 'nestedField2')
                        ]
                    },
                    {
                        'name': 'secondNestedFields',
                        'translations': [
                            ('field3', 'nestedField3'),
                            {
                                'name': 'nestedNestedField',
                                'translations': [
                                    ('field4', 'nestedField4')
                                ]
                            }
                        ]
                    }
                ]
    """

    class Meta:
        nesting_options = []

    def to_internal_value(self, data):
        errors = self._validate_input(data)
        if errors:
            raise ValidationError(errors)

        for option in self.Meta.nesting_options:
            data.update(self._to_internal_value_recursive(option, data))
        return super().to_internal_value(data)

    def _to_internal_value_recursive(self, option, data):
        output = {}
        nested_obj = data.pop(option['name'])
        for item in option['translations']:
            if isinstance(item, dict):
                output.update(
                    self._to_internal_value_recursive(item, nested_obj)
                )
            else:
                flat, nested = item
                output[flat] = nested_obj[nested]
        return output

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        for option in self.Meta.nesting_options:
            rep.update(self._to_respresentation_recursive(option, rep))
        return rep

    def _to_respresentation_recursive(self, option, rep):
        nested_obj = OrderedDict()
        for item in option['translations']:
            if isinstance(item, dict):
                nested_obj.update(
                    self._to_respresentation_recursive(item, rep)
                )
            else:
                flat, nested = item
                nested_obj[nested] = rep.pop(flat)

        return {option['name']: nested_obj}

    def _validate_input(self, data):
        errors = {}
        for option in self.Meta.nesting_options:
            errors.update(self._validate_input_recursive(option, data))
        return errors

    def _validate_input_recursive(self, option, data):
        required_error = 'This field is required.'
        if option['name'] not in data:
            return {option['name']: required_error}

        errors = {}
        nested_obj = data[option['name']]
        for item in option['translations']:
            if isinstance(item, dict):
                nested_errors = self._validate_input_recursive(
                    item, nested_obj
                )
                option_name = option["name"]
                errors.update(
                    {
                        f'{option_name}.{error_field}': val
                        for error_field, val in nested_errors.items()
                    }
                )
            else:
                flat, nested = item
                if nested not in nested_obj:
                    option_name = option['name']
                    field = f'{option_name}.{nested}'
                    errors[field] = required_error

        return errors

    @property
    def errors(self):
        errors = super().errors
        translated_errors = {
            self._get_nested_fieldname(key): val
            for key, val in errors.items()
        }
        return translated_errors

    def _get_nested_fieldname(self, key):
        for option in self.Meta.nesting_options:
            name = self._get_nested_fieldname_recursive(option, key)
            if name:
                return name
        return key

    def _get_nested_fieldname_recursive(self, option, key):
        for item in option['translations']:
            if isinstance(item, dict):
                name = self._get_nested_fieldname_recursive(item, key)
                if name:
                    option_name = option['name']
                    return f'{option_name}.{name}'
            else:
                field_key, name = item
                if field_key == key:
                    option_name = option['name']
                    return f'{option_name}.{name}'
        return None


class NestingSerializer(NestingMixin, ModelSerializer):
    pass
