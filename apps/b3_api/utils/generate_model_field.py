from rest_framework import serializers


def get_model_field(class_name, field_name, read_only=False, validators=None):
    """
    This method is deprecated. Use .mixins.PayloadConverterMixin instead.
    """
    return serializers.ModelField(
        model_field=class_name()._meta.get_field(field_name),
        source=field_name,
        read_only=read_only,
        validators=validators
    )
