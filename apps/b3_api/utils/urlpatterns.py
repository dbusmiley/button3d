import re
import typing as t
import logging

__all__ = (
    'trim_leading_caret',
    'trim_trailing_dollar',
    'PatternCache',
)


# Based on code from:
# https://stackoverflow.com/questions/18653278/django-get-url-regex-by-name

logger = logging.getLogger(__name__)


def trim_leading_caret(s: str) -> str:
    """
    Trims a leading caret from the given string

    :param s: pattern to remove the caret from
    :return: the pattern without a leading caret
    """
    return s[1:] if s.startswith('^') else s


def trim_trailing_dollar(s: str) -> str:
    return s[:-1] if s.endswith('$') else s


def recursive_parse(urlpatterns: t.Any, paths: list, prefix: str=None,
                    base: str=None) -> None:
    """
    Recursively parse url patterns

    Given a start point, will recursively parse all url patterns in a Django
    installation and return the patterns as a list.
    Optionally filters the patterns by a base path.

    .. Note::
       We cannot use viewnames to create an easy to index map, because DRF's
       routers do not create url names.

    :param urlpatterns: the current pattern(s) to extract the path pattern
                        from.
    :param paths: Current list of paths
    :param prefix: the prefix for the current set of urlpatterns, in the
                   case of include() usage.
    :param base: filter on path prefix
    :return: List of full path patterns
    """
    try:
        iterator = iter(urlpatterns)
    except TypeError:
        iterator = iter([urlpatterns])

    for pattern in iterator:
        path = (prefix or '') + trim_leading_caret(pattern.regex.pattern)
        if hasattr(pattern, "url_patterns"):
            recursive_parse(pattern.url_patterns, paths, path, base)
        elif not base or path.startswith(base):
            paths.append('^' + path)


class PatternCache(object):
    def __init__(self, base: str=None, manual_populate: bool=False):
        """
        A URL pattern cache

        :param base: filter on path prefix
        :param manual_populate: Do not populate on instance creation but do
        it manually.
        """
        self.base = base
        """ current path prefix filter """
        self.patterns = []  # type: t.List[str]
        if not manual_populate:
            self.populate_patterns()

    def populate_patterns(self) -> None:
        from django.core.urlresolvers import get_resolver
        recursive_parse(get_resolver(None), self.patterns, base=self.base)

    def get_pattern(self,
                    request_uri: str, trim_caret: bool=True,
                    trim_dollar: bool=True) -> t.Optional[str]:
        """
        Get a pattern for a given uri

        Checks if the given request uri ("absolute URI" in the strict sense,
        so the full path component of a URL) matches a known pattern.

        .. Note::
            This code will need some work if Django is upgraded to 2.x.
            Hopefully we can then upgrade to a version where the pattern is
            available on ``request.resolver_match``.
            See: https://code.djangoproject.com/ticket/28766

        :param request_uri: the uri to match
        :param trim_caret: when a match is found, trim the leading caret
        :param trim_dollar: when a match is found, trim the trailing dollar
        :return: ``None`` or the matched pattern
        """
        for pattern in self.patterns:
            if re.search(pattern, request_uri):
                if trim_caret:
                    pattern = trim_leading_caret(pattern)
                if trim_dollar:
                    pattern = trim_trailing_dollar(pattern)

                logger.debug('Pattern matched: {}'.format(pattern))
                return pattern

            logger.debug('{:s}: No pattern matched'.format(request_uri))
        return None
