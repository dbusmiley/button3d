import typing as t
from decimal import Decimal

from django.conf import settings
from django.db.models import Sum

from apps.b3_core.utils import get_exchange_rate
from button3d import type_declarations as td


def get_currency_or_default(
    query_params: td.StringKeyDict
) -> str:
    parsed_currency = parse_currency(query_params)
    if not parsed_currency:
        return settings.DEFAULT_CURRENCY
    return parsed_currency


def parse_currency(query_params: td.StringKeyDict) -> \
        t.Optional[str]:
    if 'currency' in query_params:
        queried_currency = query_params['currency']
        currencies = settings.SUPPORTED_CURRENCY_SET
        if queried_currency in currencies:
            return queried_currency
    return None


def get_total_exclusive_tax(
    currency: str,
    currency_aggregates_qs: str
) -> td.Decimal:
    currency_aggregates = get_currency_aggregates(
        currency=currency,
        currency_aggregates_qs=currency_aggregates_qs)

    return calculate_sum(currency_aggregates=currency_aggregates)


def get_currency_aggregates(
    currency: str,
    currency_aggregates_qs: t.Sequence[t.Tuple[str, td.Decimal]]
) -> td.StringKeyDict:
    currency_aggregates = {}
    for _currency in settings.SUPPORTED_CURRENCY_SET:
        exchange_rate = get_exchange_rate(from_c=_currency, to_c=currency)
        value = (
            (Sum('total_value') - Sum('shipping_value'))
            * exchange_rate
        )

        currency_aggregates[_currency] = currency_aggregates_qs.filter(
            currency=_currency).values('currency').aggregate(
            value=value)
    return currency_aggregates


def calculate_sum(currency_aggregates: td.StringKeyDict) -> td.Decimal:
    result = Decimal('0.0')
    for currency_aggregate in currency_aggregates.keys():
        value = currency_aggregates[currency_aggregate]['value']
        result += value or Decimal('0.00')
    return result
