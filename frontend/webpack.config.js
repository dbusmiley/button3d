const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: {
		commentSystem: path.resolve(__dirname, './src/commentSystem.js'),
		checkoutEN: path.resolve(__dirname, './checkout/checkout-en.js'),
		checkoutDE: path.resolve(__dirname, './checkout/checkout-de.js'),
		// AppendEntries //
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			'@': path.resolve(__dirname, './frontend/src'),
		},
	},
	module: {
		rules: [
			{
				test: /\.(js|vue)$/,
				loader: 'eslint-loader',
				enforce: 'pre',
				exclude: [path.resolve(__dirname, './node_modules')],
				include: [path.resolve(__dirname, './src')],
				options: {
					formatter: require('eslint-friendly-formatter'),
				},
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						scss: 'vue-style-loader!css-loader!sass-loader',
						sass:
							'vue-style-loader!css-loader!sass-loader?indentedSyntax',
						i18n: '@kazupon/vue-i18n-loader',
					},
					postcss: [require('postcss-cssnext')()],
				},
			},
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [
					{
						loader: 'babel-loader',
						options: { presets: ['es2015'] },
					},
				],
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|woff|woff2|eot|ttf|svg)$/,
				loader: 'url-loader?limit=100000',
			},
		],
	},
	output: {
		path: path.resolve(__dirname, '../apps/b3_core/static/js/dist/'),
		filename: '[name].[chunkhash].bundle.js',
	},
	plugins: [
		new HtmlWebpackPlugin({
			hash: true,
			title: 'Comment System',
			scriptName: 'commentSystem.bundle.js',
			commonFileName: 'common',
			chunks: ['commentSystem'],
			inject: false,
			template: path.resolve(
				__dirname,
				'./templates/project_comments.html',
			),
			filename: path.resolve(
				__dirname,
				'../apps/b3_user_panel/templates/dist/project_comments.html',
			),
		}),
		new HtmlWebpackPlugin({
			hash: true,
			title: 'Checkout',
			scriptName: 'checkout.bundle.js',
			commonFileName: 'common',
			chunks: ['checkoutEN'],
			inject: false,
			template: path.resolve(__dirname, './templates/checkout.html'),
			filename: path.resolve(
				__dirname,
				'../apps/b3_user_panel/templates/dist/checkoutEN.html',
			),
		}),
		new HtmlWebpackPlugin({
			hash: true,
			title: 'Checkout',
			scriptName: 'checkout.bundle.js',
			commonFileName: 'common',
			chunks: ['checkoutDE'],
			inject: false,
			template: path.resolve(__dirname, './templates/checkout.html'),
			filename: path.resolve(
				__dirname,
				'../apps/b3_user_panel/templates/dist/checkoutDE.html',
			),
		}),
		// AppendHtmlWebpackPlugin //
	],
};
