var baseWebpackConfig = require('./webpack.config');
var merge = require('webpack-merge');

module.exports = merge(baseWebpackConfig, {
    devtool: 'inline-source-map'
});
