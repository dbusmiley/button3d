module.exports = {
  extends: [
    './best-practices',
    './error',
    './es6',
    './imports',
    './node',
    './style',
    './variables',
  ].map(require.resolve),
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
  },
  plugins: ['vue', 'html'],
  rules: {
    strict: 'error',
  },
};
