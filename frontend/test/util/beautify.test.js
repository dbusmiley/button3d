import beautify from '../../src/util/beautify';

describe('Beautify test for ES Module', () => {
  it('Test just now with one second', () => {
    const result = beautify.prettyDate(1500368000, 1500368001);
    expect(result).toEqual({ amount: null, type: 'TIME_NOW' });
  });

  it('Test Just Now with five seconds', () => {
    const result = beautify.prettyDate(1500368000, 1500368005);
    expect(result).toEqual({ amount: null, type: 'TIME_NOW' });
  });

  it('Test 15 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368015);
    expect(result).toEqual({ amount: 15, type: 'SECOND' });
  });

  it('Test 45 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368045);
    expect(result).toEqual({ amount: 45, type: 'SECOND' });
  });

  it('Test 1 minute and 30 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368090);
    expect(result).toEqual({ amount: null, type: 'MINUTE_EXACT' });
  });

  it('Test 1 minute and 45 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368105);
    expect(result).toEqual({ amount: null, type: 'MINUTE_EXACT' });
  });

  it('Test 3 minutes and 33 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368213);
    expect(result).toEqual({ amount: 3, type: 'MINUTE' });
  });

  it('Test 16 minutes and 20 seconds ago', () => {
    const result = beautify.prettyDate(1500368000, 1500368980);
    expect(result).toEqual({ amount: 16, type: 'MINUTE' });
  });

  it('Test 1 hour and 10 minutes ago', () => {
    const result = beautify.prettyDate(1500000000, 1500004200);
    expect(result).toEqual({ amount: null, type: 'HOUR_EXACT' });
  });

  it('Test 1 hour and 20 minutes ago', () => {
    const result = beautify.prettyDate(1500000000, 1500004800);
    expect(result).toEqual({ amount: null, type: 'HOUR_EXACT' });
  });

  it('Test 2 hours and 1 second ago', () => {
    const result = beautify.prettyDate(1500000000, 1500007201);
    expect(result).toEqual({ amount: 2, type: 'HOUR' });
  });

  it('Test 6 hours and 6 minutes ago', () => {
    const result = beautify.prettyDate(1500000000, 1500021960);
    expect(result).toEqual({ amount: 6, type: 'HOUR' });
  });

  it('Test 1 day and 2 hours ago', () => {
    const result = beautify.prettyDate(1500000000, 1500093600);
    expect(result).toEqual({ amount: null, type: 'DAY_EXACT' });
  });

  it('Test 1 day and 6 hours ago', () => {
    const result = beautify.prettyDate(1500000000, 1500108000);
    expect(result).toEqual({ amount: null, type: 'DAY_EXACT' });
  });

  it('Test 2 days and 6 hours ago', () => {
    const result = beautify.prettyDate(1500000000, 1500194400);
    expect(result).toEqual({ amount: 2, type: 'DAY' });
  });

  it('Test 4 days and 6 hours ago', () => {
    const result = beautify.prettyDate(1500000000, 1500367200);
    expect(result).toEqual({ amount: 4, type: 'DAY' });
  });

  it('Test 7 days and 1 hour ago', () => {
    const result = beautify.prettyDate(1500000000, 1500608400);
    expect(result).toEqual({ amount: null, type: 'WEEK_EXACT' });
  });

  it('Test 8 days and 6 hour ago', () => {
    const result = beautify.prettyDate(1500000000, 1500608400);
    expect(result).toEqual({ amount: null, type: 'WEEK_EXACT' });
  });

  it('Test 2 weeks and 6 hours ago', () => {
    const result = beautify.prettyDate(1500000000, 1501231200);
    expect(result).toEqual({ amount: 2, type: 'WEEK' });
  });

  it('Test 3 weeks and 1 day ago', () => {
    const result = beautify.prettyDate(1500000000, 1501900800);
    expect(result).toEqual({ amount: 3, type: 'WEEK' });
  });

  it('Test 1 month 1 hour ago', () => {
    const result = beautify.prettyDate(1500000000, 1502682000);
    expect(result).toEqual({ amount: null, type: 'MONTH_EXACT' });
  });

  it('Test 1 month and 5 day ago', () => {
    const result = beautify.prettyDate(1500000000, 1502851200);
    expect(result).toEqual({ amount: null, type: 'MONTH_EXACT' });
  });

  it('Test 2 month 1 hour ago', () => {
    const result = beautify.prettyDate(1500000000, 1505360400);
    expect(result).toEqual({ amount: 2, type: 'MONTH' });
  });

  it('Test 1 year 1 hour ago', () => {
    const result = beautify.prettyDate(1500000000, 1531539600);
    expect(result).toEqual({ amount: null, type: 'YEAR_EXACT' });
  });

  it('Test 2 years 4 days ago', () => {
    const result = beautify.prettyDate(1500000000, 1563417600);
    expect(result).toEqual({ amount: 2, type: 'YEAR' });
  });
});
