global.Button3d = {};
global.Button3d.common = {};

global.gettext = stringToTranslate => stringToTranslate;

require('../../../apps/b3_core/static/js/button3d/common/beautify');

describe('Beautify Test', () => {
  it('Test Just Now with one second', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368001);
    expect(result).toEqual('just now');
  });

  it('Test Just Now with five seconds', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368005);
    expect(result).toEqual('just now');
  });

  it('15 seconds ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368015);
    expect(result).toEqual('15 seconds ago');
  });

  it('45 seconds ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368045);
    expect(result).toEqual('45 seconds ago');
  });

  it('A minute ago - 1min 30sec', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368090);
    expect(result).toEqual('a minute ago');
  });

  it('A minute ago - 1min 45sec', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368105);
    expect(result).toEqual('a minute ago');
  });

  it('Test 3min 33sec', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368213);
    expect(result).toEqual('3 minutes ago');
  });

  it('Test 16min 20sec', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500368000, 1500368980);
    expect(result).toEqual('16 minutes ago');
  });

  it('Test 1hr 10 min ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500004200);
    expect(result).toEqual('an hour ago', result);
  });

  it('Test 1hr 20 min ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500004800);
    expect(result).toEqual('an hour ago', result);
  });

  it('Test 2hr 01 sec ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500007201);
    expect(result).toEqual('2 hours ago', result);
  });

  it('Test 6hr 6 minutes ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500021960);
    expect(result).toEqual('6 hours ago', result);
  });

  it('Test 1 day and 2 hours', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500093600);
    expect(result).toEqual('yesterday', result);
  });

  it('Test 1 day and 6 hours', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500108000);
    expect(result).toEqual('yesterday', result);
  });

  it('Test 2 days and 6 hours', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500194400);
    expect(result).toEqual('2 days ago', result);
  });

  it('Test 4 days and 6 hours', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500367200);
    expect(result).toEqual('4 days ago', result);
  });

  it('Test 7 days and 1 hour', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500608400);
    expect(result).toEqual('a week ago', result);
  });

  it('Test 8 days and 6 hour', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1500608400);
    expect(result).toEqual('a week ago', result);
  });

  it('Test 2 weeks and 6 hour', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1501231200);
    expect(result).toEqual('2 weeks ago', result);
  });

  it('Test 3 weeks and 1 day ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1501900800);
    expect(result).toEqual('3 weeks ago', result);
  });

  it('Test 1 month 1 hour ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1502682000);
    expect(result).toEqual('a month ago', result);
  });

  it('Test 1 month and 5 day ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1502851200);
    expect(result).toEqual('a month ago', result);
  });

  it('Test 2 month 1 hour ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1505360400);
    expect(result).toEqual('2 months ago', result);
  });

  it('Test 1 year 1 hour ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1531539600);
    expect(result).toEqual('a year ago', result);
  });

  it('Test 2 years 4 days ago', () => {
    const result = global.Button3d.common.beautify.prettyDate(1500000000, 1563417600);
    expect(result).toEqual('2 years ago', result);
  });
});
