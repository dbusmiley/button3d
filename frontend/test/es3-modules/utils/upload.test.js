import jsdom from 'jsdom';

global.Button3d = {};
global.Button3d.common = {};
global.gettext = stringToTranslate => stringToTranslate;

require('../../../../apps/b3_core/static/js/button3d/common/fileExtensions');
const uploadUtility = require('../../../../apps/b3_core/static/js/utils/upload');

describe('Upload utility get paragraphs test', () => {
  const singeInvalidFileList = [
    {
      name: 'turbine.stl',
      size: 77277233223,
    },
  ];

  const invalidFileList = [
    {
      name: 'turbine.stl',
      size: 77277233223,
    },
    {
      name: 'turbine.3df',
      size: 321414245,
    },
  ];

  it('Test first paragraph creation for single file list', () => {
    const paragraphs = uploadUtility.getErrorParagraphTexts(
      singeInvalidFileList.length,
    );
    expect(paragraphs.first).toEqual('A 3D model is currently too large to be processed:');
  });

  it('Test first paragraph creation for multiple file list', () => {
    const paragraphs = uploadUtility.getErrorParagraphTexts(
      invalidFileList.length,
    );
    expect(paragraphs.first).toEqual('Some 3D models are currently too large to be processed:');
  });
});

describe('Upload utility - slugify', () => {
  it('Test slugify with Capital Case file name', () => {
    const slugifiedFileName = uploadUtility.slugify('Test Name.stl');
    expect(slugifiedFileName).toEqual('test-namestl');
  });

  it('Test slugify with weird filename', () => {
    const slugifiedFileName = uploadUtility.slugify('Test awJawd a / ame.stl');
    expect(slugifiedFileName).toEqual('test-awjawd-a-amestl');
  });
});

describe('Upload utility - createFileProgressBar', () => {
  it('test dae filename containing in progress bar', () => {
    const config = {
      filename: 'awdawawd.dae',
      type: 'warning',
      progress: 100,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const heading = dom.window.document.querySelector('h5');
    expect(heading.textContent).toContain('awdawawd.dae');
  });

  it('test stl filename containing in progress bar', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'warning',
      progress: 100,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const heading = dom.window.document.querySelector('h5');
    expect(heading.textContent).toContain('turbine raw € ÄA.stl');
  });

  it('test warning color', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'warning',
      progress: 100,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const progressBar = dom.window.document.querySelector('.progress-bar');
    expect(progressBar.classList).toContain('progress-bar-warning');
  });

  it('test success color', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'success',
      progress: 100,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const progressBar = dom.window.document.querySelector('.progress-bar');
    expect(progressBar.classList).toContain('progress-bar-success');
  });

  it('check 0 percent percentage', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'success',
      progress: 0,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const progressBar = dom.window.document.querySelector('.progress-bar');
    expect(progressBar.style.width).toEqual('0%');
  });

  it('check 20 percent percentage', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'success',
      progress: 0,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const progressBar = dom.window.document.querySelector('.progress-bar');
    expect(progressBar.style.width).toEqual('0%');
  });

  it('check 100 percent percentage', () => {
    const config = {
      filename: 'turbine raw € ÄA.stl',
      type: 'success',
      progress: 100,
    };
    const html = uploadUtility.createFileProgressBar(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const progressBar = dom.window.document.querySelector('.progress-bar');
    expect(progressBar.style.width).toEqual('100%');
  });
});

describe('Upload utility - createLoadingContainer', () => {
  it('Test loading message', () => {
    const config = {
      loadingMessage: 'This is now loading',
    };
    const html = uploadUtility.createLoadingContainer(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const heading = dom.window.document.querySelector('h4');
    expect(heading.textContent).toEqual('This is now loading');
  });

  it('Test loading message alternative', () => {
    const config = {
      loadingMessage: 'This is now loading really fast',
    };
    const html = uploadUtility.createLoadingContainer(config);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html>${html}`);
    const heading = dom.window.document.querySelector('h4');
    expect(heading.textContent).toEqual('This is now loading really fast');
  });
});

describe('Upload utility - mapInvalidFilesToHtml', () => {
  it('Test single valid file', () => {
    const files = [
      {
        name: 'turbine.stl',
        size: 67108,
      },
    ];
    const result = uploadUtility.mapInvalidFilesToHtml(files);
    expect(result).toHaveLength(0);
  });

  it('Test single invalid file - file name', () => {
    const files = [
      {
        name: 'turbine.stl',
        size: 137217728,
      },
    ];
    const result = uploadUtility.mapInvalidFilesToHtml(files);
    expect(result).toContain('turbine.stl');
    expect(result).toContain('128 MB');
  });

  it('Test single invalid file - list items length', () => {
    const files = [
      {
        name: 'turbine.stl',
        size: 137217728,
      },
    ];
    const html = uploadUtility.mapInvalidFilesToHtml(files);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html><ul>${html}</ul>`);
    const listElementsLength = dom.window.document.querySelector('ul')
      .childNodes.length;
    expect(listElementsLength).toBe(1);
  });

  it('Test multiple files - file names', () => {
    const files = [
      {
        name: 'turbine.stl',
        size: 139217728,
      },
      {
        name: 'turbine.catpart',
        size: 69108864,
      },
      {
        name: 'turbine-small.catpart',
        size: 60108864,
      },
    ];
    const result = uploadUtility.mapInvalidFilesToHtml(files);
    expect(result).toContain('turbine.stl');
    expect(result).toContain('turbine.catpart');
    expect(result).toContain('128 MB');
    expect(result).toContain('64 MB');
  });

  it('Test multiple files - list items length', () => {
    const files = [
      {
        name: 'turbine.stl',
        size: 139217728,
      },
      {
        name: 'turbine.catpart',
        size: 69108864,
      },
      {
        name: 'turbine-small.catpart',
        size: 60108864,
      },
    ];
    const html = uploadUtility.mapInvalidFilesToHtml(files);
    const dom = new jsdom.JSDOM(`<!DOCTYPE html><ul>${html}</ul>`);
    const listElementsLength = dom.window.document.querySelector('ul')
      .childNodes.length;
    expect(listElementsLength).toBe(2);
  });
});
