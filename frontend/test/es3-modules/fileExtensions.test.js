global.Button3d = {};
global.Button3d.common = {};
global.gettext = text => text;

require('../../../apps/b3_core/static/js/button3d/common/fileExtensions');

describe('File extensions test', () => {
  it('Test if stl file is supported', () => {
    const supported3dFiles = global.Button3d.common.fileExtensions.get3dFileExtensions();
    expect(supported3dFiles).toContain('stl');
  });

  it('Test if .stl is inside readable file extensions', () => {
    const readableFileExtensions = global.Button3d.common.fileExtensions.getReadableExtensions();
    expect(readableFileExtensions).toContain('.stl');
  });

  it('Test comma seperated list of file extensions', () => {
    const commaSeperatedFileExtensions = global.Button3d.common.fileExtensions.getCommaSeperated();
    expect(commaSeperatedFileExtensions).toContain('.stl');
  });

  it('Test alternative readable extension representation', () => {
    const alternativeFileExtensions = global.Button3d.common.fileExtensions.getReadableExtensionsAlternative();
    expect(alternativeFileExtensions).toContain('.stl');
  });

  it('Test filename testFile.stl', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'turbine.stl',
    );
    expect(is3dFile).toBeTruthy();
  });

  it('Test filename testFile.obj', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'turbine.obj',
    );
    expect(is3dFile).toBeTruthy();
  });

  it('Test filename testFile.amf', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'turbine.amf',
    );
    expect(is3dFile).toBeFalsy();
  });

  it('Test unicode filename for 3d file', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile('ṀɎȵȌ.stl');
    expect(is3dFile).toBeTruthy();
  });

  it('Test mp3 is no 3d file', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'turbine.mp3',
    );
    expect(is3dFile).toBeFalsy();
  });

  it('Test pdf is no 3d file', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'quotation.pdf',
    );
    expect(is3dFile).toBeFalsy();
  });

  it('Test to get stl extension', () => {
    const extension = global.Button3d.common.fileExtensions.getExtension(
      'turbine.stl',
    );
    expect(extension).toEqual('stl');
  });

  it('Test to get pdf extension', () => {
    const extension = global.Button3d.common.fileExtensions.getExtension(
      'turbine.pdf',
    );
    expect(extension).toEqual('pdf');
  });

  it('Test if .test.stl is 3d file', () => {
    const is3dFile = global.Button3d.common.fileExtensions.is3DFile(
      'turbine.test.stl',
    );
    expect(is3dFile).toBeTruthy();
  });

  it('Test stl filesize', () => {
    const stlFileName = 'turbine.stl';
    var filesize = global.Button3d.common.fileExtensions.getMaximumFilesize(
      stlFileName,
    );
    expect(filesize).toEqual(128);
  });

  it('Test ctm filesize', () => {
    const stlFileName = 'turbine.ctm';
    var filesize = global.Button3d.common.fileExtensions.getMaximumFilesize(
      stlFileName,
    );
    expect(filesize).toEqual(128);
  });

  it('Test 3dm filesize', () => {
    const stlFileName = 'turbine.3dm';
    const filesize = global.Button3d.common.fileExtensions.getMaximumFilesize(
      stlFileName,
    );
    expect(filesize).toEqual(64);
  });

  it('Test jt filesize', () => {
    const stlFileName = 'turbine.jt';
    const filesize = global.Button3d.common.fileExtensions.getMaximumFilesize(
      stlFileName,
    );
    expect(filesize).toEqual(64);
  });

  //
  // 134217728

  it('Test error for stl file', () => {
    const tooBigStlFile = {
      name: 'turbine.stl',
      size: 145217728,
    };
    const errorMessage = global.Button3d.common.fileExtensions.getErrorMessage(
      tooBigStlFile,
    );
    expect(errorMessage).toEqual('.stl files can be uploaded up to 128 MB');
  });

  it('Test no error for stl file', () => {
    const stlFile = {
      name: 'turbine.stl',
      size: 67108864,
    };
    const errorMessage = global.Button3d.common.fileExtensions.getErrorMessage(
      stlFile,
    );
    expect(errorMessage).toBeNull();
  });

  it('Test error for catpart file', () => {
    const tooBigCatpartFile = {
      name: 'turbine.catpart',
      size: 69108864,
    };
    const errorMessage = global.Button3d.common.fileExtensions.getErrorMessage(
      tooBigCatpartFile,
    );
    expect(errorMessage).toEqual('.catpart files can be uploaded up to 64 MB');
  });

  it('Test no error for catpart file', () => {
    const catpartFile = {
      name: 'turbine.catpart',
      size: 63108864,
    };
    const errorMessage = global.Button3d.common.fileExtensions.getErrorMessage(
      catpartFile,
    );
    expect(errorMessage).toBeNull();
  });
});
