import axios from 'axios';
import cookie from 'cookie';

const instance = axios.create({
  timeout: 5000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRFToken': cookie.parse(document.cookie).csrftoken,
  },
});

const actions = {
  fetchComments: projectId =>
    instance.get(`/basket-ajax/${projectId}/comments/`),
  addComment: (projectId, parentId, text) =>
    instance.put(`/basket-ajax/${projectId}/comments/`, {
      parent: parentId,
      text,
    }),
  deleteComment: (projectId, commentId) =>
    instance.delete(`/basket-ajax/${projectId}/comments/`, {
      params: {
        id: commentId,
      },
    }),
  updateComment: (projectId, id, text) => {
    const payload = {
      id,
      text,
    };
    return instance.post(`/basket-ajax/${projectId}/comments/`, payload);
  },
};

export default actions;
