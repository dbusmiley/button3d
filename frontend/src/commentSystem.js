import 'es6-promise/auto';
import 'jspolyfill-array.prototype.find';
import 'es6-object-assign/auto';
import 'string.prototype.includes';
import findIndex from 'array.prototype.findindex';
import '@3yourmind/vue-comments/dist/VueComments.css';
import Vue from 'vue';
import Vuex from 'vuex';
import VueAutosize from 'vue-autosize';
import GetTextPlugin from 'vue-gettext';
import VueCommentSystemPlugin from '@3yourmind/vue-comments';

import CommentSystem from './CommentSystem.vue';
import translations from './translations.json';

findIndex.shim();

Vue.use(Vuex);
Vue.use(VueAutosize);
Vue.use(VueCommentSystemPlugin);
Vue.use(GetTextPlugin, {
  translations,
  defaultLanguage: 'en_GB',
  silent: true,
});

if (window.location.href.includes('/en/')) {
  Vue.config.language = 'en_GB';
}
if (window.location.href.includes('/de/')) {
  Vue.config.language = 'de_DE';
}

// eslint-disable-next-line
new Vue({
  el: '#comment-system',
  render: h => h(CommentSystem),
  store: new Vuex.Store({}),
});
