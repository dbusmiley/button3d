/**
 *
 * Method to beautify unix timestamps.
 *
 * @param {Number} time Time which should be prettified.
 * @param {Number} currentTime Time to base on. I include this here for better testability.
 * @returns {String} The prettified string of the time parameter.
 */
function prettyDate(time, currentTime) {
  const differenceInSeconds = currentTime - time;
  if (differenceInSeconds < 10) {
    return { amount: null, type: 'TIME_NOW' };
  }
  if (differenceInSeconds < 60) {
    return { amount: differenceInSeconds, type: 'SECOND' };
  }
  if (differenceInSeconds < 120) {
    return { amount: null, type: 'MINUTE_EXACT' };
  }
  if (differenceInSeconds < 3600) {
    return { amount: parseInt(differenceInSeconds / 60, 10), type: 'MINUTE' };
  }
  if (differenceInSeconds < 7200) {
    return { amount: null, type: 'HOUR_EXACT' };
  }
  if (differenceInSeconds < 86400) {
    return { amount: parseInt(differenceInSeconds / 3600, 10), type: 'HOUR' };
  }
  if (differenceInSeconds < 172800) {
    return { amount: null, type: 'DAY_EXACT' };
  }
  if (differenceInSeconds < 86400 * 7) {
    return { amount: parseInt(differenceInSeconds / 86400, 10), type: 'DAY' };
  }
  if (differenceInSeconds < 604800 * 2) {
    return { amount: null, type: 'WEEK_EXACT' };
  }
  if (differenceInSeconds < 31 * 86400) {
    return {
      amount: parseInt(differenceInSeconds / (7 * 86400), 10),
      type: 'WEEK',
    };
  }
  if (differenceInSeconds < 2 * 31 * 86400) {
    return { amount: null, type: 'MONTH_EXACT' };
  }
  if (differenceInSeconds < 365 * 86400) {
    return {
      amount: parseInt(differenceInSeconds / (31 * 86400), 10),
      type: 'MONTH',
    };
  }

  if (differenceInSeconds < 2 * 365 * 86400) {
    return { amount: null, type: 'YEAR_EXACT' };
  }
  return {
    amount: parseInt(differenceInSeconds / (365 * 86400), 10),
    type: 'YEAR',
  };
}

function prettyByCurrentDate(date) {
  return prettyDate(date, Math.round(new Date().getTime() / 1000));
}

export default {
  prettyByCurrentDate,
  prettyDate,
};
