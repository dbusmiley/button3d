var fs = require('fs');

console.log('Integrating Crowdin in-context tranlations...');

fs.readFile('./frontend/templates/checkout.html', 'utf8', function(err, data) {
	if (err) throw err;
	var scriptText = `    <script type="text/javascript">
        var _jipt = [];
        _jipt.push(['project', '3yourmind']);
        var pseudoLang = 'ach';
    </script>
    <script type="text/javascript" src="https://cdn.crowdin.com/jipt/jipt.js"></script>`;
	const newData = data.replace(
		/<!--BEGIN:CROWDIN-TRANSLATION-BLOCK-->(\n|.)*<!--END:CROWDIN-TRANSLATION-BLOCK-->/g,
		`<!--BEGIN:CROWDIN-TRANSLATION-BLOCK-->\n${scriptText}\n<!--END:CROWDIN-TRANSLATION-BLOCK-->`,
	);
	fs.writeFile('./frontend/templates/checkout.html', newData, function(err) {
		if (err) throw err;
	});
});

function overrideLocalizedApps(languageCodes) {
	languageCodes.forEach(function(languageCode) {
		fs.readFile('./frontend/checkout/Checkout.vue', 'utf8', function(
			err,
			unlocalizedApp,
		) {
			if (err) console.log(err);
			const localizedApp = unlocalizedApp.replace(
				'<i18n src="./i18n/locales.json"></i18n>',
				'<i18n src="./i18n/locales-ach.json"></i18n>',
			);
			fs.writeFileSync(
				`./frontend/checkout/Checkout-${languageCode}.vue`,
				localizedApp,
			);
		});
		fs.readFile('./frontend/checkout/checkout.js', 'utf8', function(
			err,
			unlocalizedApp,
		) {
			if (err) console.log(err);
			const localizedApp = unlocalizedApp.replace(
				"import Checkout from './Checkout.vue';",
				`import Checkout from './Checkout-${languageCode}.vue';`,
			);
			fs.writeFileSync(
				`./frontend/checkout/checkout-${languageCode}.js`,
				localizedApp,
			);
		});
	});
}

overrideLocalizedApps(['de', 'en']);
