var fs = require('fs');

fs.readFile('./frontend/templates/checkout.html', 'utf8', function(err, data) {
	if (err) throw err;
	const newData = data.replace(
		/<!--BEGIN:CROWDIN-TRANSLATION-BLOCK-->[\s\S]*<!--END:CROWDIN-TRANSLATION-BLOCK-->/g,
		`<!--BEGIN:CROWDIN-TRANSLATION-BLOCK-->\n<!--END:CROWDIN-TRANSLATION-BLOCK-->`,
	);
	fs.writeFile('./frontend/templates/checkout.html', newData, function(err) {
		if (err) throw err;
	});
});
