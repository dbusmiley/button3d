const fs = require('fs');
const { execSync } = require('child_process');

function buildTranslations() {
	function correctTranslations(code) {
		const translationFile = fs.readFileSync(
			`./frontend/checkout/i18n/locales-${code}.json`,
		);

		const translationObject = {};
		const translations = JSON.parse(translationFile);
		translationObject[code] = translations.en;

		fs.writeFileSync(
			`./frontend/checkout/i18n/locales-${code}.json`,
			JSON.stringify(translationObject),
		);
	}

	const languageCodes = ['en', 'de', 'ach'];

	languageCodes.forEach((code) => {
		correctTranslations(code);
	});
}

const createLocalizedApps = (languageCodes, isDev) => {
	languageCodes.forEach((languageCode) => {
		fs.readFile(
			'./frontend/checkout/Checkout.vue',
			'utf8',
			(err, unlocalizedApp) => {
				if (err) console.error(err);
				const localizedApp = isDev
					? unlocalizedApp
					: unlocalizedApp.replace(
							'<i18n src="./i18n/locales.json"></i18n>',
							`<i18n src="./i18n/locales-${languageCode}.json"></i18n>`,
					  );
				fs.writeFileSync(
					`./frontend/checkout/Checkout-${languageCode}.vue`,
					localizedApp,
				);
			},
		);
		fs.readFile(
			'./frontend/checkout/checkout.js',
			'utf8',
			(err, unlocalizedApp) => {
				if (err) console.error(err);
				const localizedApp = isDev
					? unlocalizedApp
					: unlocalizedApp.replace(
							"import Checkout from './Checkout.vue';",
							`import Checkout from './Checkout-${languageCode}.vue';`,
					  );
				fs.writeFileSync(
					`./frontend/checkout/checkout-${languageCode}.js`,
					localizedApp,
				);
			},
		);
	});
};

if (process.argv.includes('--fake')) {
	createLocalizedApps(['de', 'en'], true);
} else {
	execSync('sh ./frontend/checkout/tasks/downloadCrowdin.sh', {
		stdio: [0, 1, 2],
	});
	execSync('sh ./frontend/checkout/tasks/downloadTranslations.sh', {
		stdio: [0, 1, 2],
	});

	buildTranslations();
	createLocalizedApps(['de', 'en'], false);
}
