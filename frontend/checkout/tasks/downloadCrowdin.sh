rm -rf ./frontend/checkout/bin/**/*.*
curl -O https://downloads.crowdin.com/cli/v2/crowdin-cli.zip
mkdir ./frontend/checkout/bin
mv ./crowdin-cli.zip ./frontend/checkout/bin
unzip -o ./frontend/checkout/bin/crowdin-cli.zip -d ./frontend/checkout/bin/
mv ./frontend/checkout/bin/**/crowdin-cli.jar ./frontend/checkout/bin
rm -rf ./frontend/checkout/bin/crowdin-cli.zip
rm -rf ./frontend/checkout/bin/*/
