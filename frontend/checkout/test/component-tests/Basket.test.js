import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';

import Basket from '../../components/Basket.vue';
import StoreFactory from '../mocks/StoreFactory';

import { MockProject, MockProjectPrice } from '../mocks/Basket';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

const i18n = new VueI18n({
	locale: 'en',
	messages: {},
	silentTranslationWarn: true,
});

let store;

const actions = {
	getProject: jest.fn(),
	getProjectPrices: jest.fn(),
	getAllColors: jest.fn(),
};

describe('Basket.vue', () => {
	beforeEach(() => {
		store = StoreFactory(
			{
				basket: {},
				order: {},
				service: {},
				user: {},
			},
			actions,
		);
	});
	it('hide service info for single supplier orgs', () => {
		store = StoreFactory(
			{
				basket: {
					currentProject: MockProject,
					currentProjectPrice: MockProjectPrice,
					userCurrencyPrice: MockProjectPrice,
				},
				order: {},
				organization: {
					settings: {
						isSingleSupplierAccount: true,
					},
				},
				service: {
					currentService: {},
				},
				user: {},
			},
			actions,
		);
		const wrapper = mount(Basket, {
			store,
			localVue,
			i18n,
		});
		expect(
			wrapper.find('[data-test="checkout-service-overview"]').exists(),
		).toBe(false);
	});
});
