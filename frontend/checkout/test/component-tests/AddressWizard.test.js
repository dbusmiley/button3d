import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';

import AddressWizard from '../../components/AddressWizard.vue';

import { MockUserAddresses } from '../mocks/User';
import StoreFactory from '../mocks/StoreFactory';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

const i18n = new VueI18n({
	locale: 'en',
	messages: {},
	silentTranslationWarn: true,
});

let store;

describe('AddressWizard.vue', () => {
	beforeEach(() => {
		store = StoreFactory({
			basket: {},
			order: {},
			service: {},
			user: {},
		});
	});
	it('should represent addresses correctly', () => {
		store = StoreFactory({
			basket: {},
			order: {},
			service: {
				currentServiceId: 1,
			},
			user: {},
		});
		const wrapper = shallowMount(AddressWizard, {
			store,
			localVue,
			i18n,
			propsData: {
				addressType: 'SHIPPING',
			},
		});
		expect(wrapper.vm.addressRepresentation(MockUserAddresses[0])).toEqual(
			'Nigel Berkins, Bismarckstraẞe 9 bei Mettman, 10987 Berlin, Germany',
		);
	});
});
