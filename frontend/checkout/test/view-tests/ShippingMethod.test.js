import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';

import ShippingMethodView from '../../views/order/ShippingMethod.vue';

import {
	MockAdditionalShippingInformation,
	MockSelectedShippingMethod,
} from '../mocks/Order';
import { MockProjectPrice } from '../mocks/Basket';
import StoreFactory from '../mocks/StoreFactory';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(VueI18n);

const i18n = new VueI18n({
	locale: 'en',
	messages: {},
	silentTranslationWarn: true,
});

let store;

const actions = {
	getProjectPrices: jest.fn(),
	getShippingMethods: jest.fn(),
	saveDeliveryInstructions: jest.fn(),
	selectShippingMethod: jest.fn(),
};

describe('ShippingMethod.vue', () => {
	beforeEach(() => {
		store = new Vuex.Store({});
	});
	it('should preselect shipping methods, regardless of index', () => {
		store = StoreFactory(
			{
				basket: {},
				order: {
					additionalShippingInformation: MockAdditionalShippingInformation,
					selectedShippingMethod: MockSelectedShippingMethod,
				},
				service: {
					currentServiceId: 1,
				},
			},
			actions,
		);
		const wrapper = shallowMount(ShippingMethodView, {
			store,
			localVue,
			i18n,
		});
		expect(
			wrapper.vm.isPreSelectedShippingMethod(MockSelectedShippingMethod, 0),
		).toEqual(true);
	});
	it('should preselect shipping methods, regardless of index', () => {
		store = StoreFactory(
			{
				basket: {},
				order: {
					additionalShippingInformation: MockAdditionalShippingInformation,
					selectedShippingMethod: MockSelectedShippingMethod,
				},
				service: {
					currentServiceId: 1,
				},
			},
			actions,
		);
		const wrapper = shallowMount(ShippingMethodView, {
			store,
			localVue,
			i18n,
		});
		expect(
			wrapper.vm.isPreSelectedShippingMethod(MockSelectedShippingMethod, 1),
		).toEqual(true);
	});
	it('should not preselect unselected shipping methods', () => {
		store = StoreFactory(
			{
				basket: {
					currentProjectPrice: MockProjectPrice,
				},
				order: {
					additionalShippingInformation: MockAdditionalShippingInformation,
					selectedShippingMethod: MockSelectedShippingMethod,
				},
				service: {
					currentServiceId: 1,
				},
			},
			actions,
		);
		const wrapper = shallowMount(ShippingMethodView, {
			store,
			localVue,
			i18n,
		});
		const otherShippingMethod = {
			id: 1,
			name: 'Super Premium',
			description: 'Padded 4D Delivery',
			deliveryDays: {
				minimum: 0,
				maximum: 1,
			},
			currency: 'EUR',
			price: {
				inclusiveTax: 0.0,
				exclusiveTax: 0.0,
			},
		};
		expect(
			wrapper.vm.isPreSelectedShippingMethod(otherShippingMethod, 1),
		).toEqual(false);
	});
});
