import Vue from 'vue';
import Vuex from 'vuex';

import MockCountries from './MockCountries.json';

Vue.use(Vuex);

export default (getters, actions) =>
	new Vuex.Store({
		getters: {
			pricePayload: () => getters.pricePayload || {},
		},
		modules: {
			basket: {
				namespaced: true,
				actions,
				getters: {
					projectId: () => getters.basket.projectId || null,
					currentProject: () => getters.basket.currentProject || null,
					currentProjectPrice: () => getters.basket.currentProjectPrice || {},
					userCurrencyPrice: () => getters.basket.userCurrencyPrice || {},
				},
			},
			color: {
				namespaced: true,
				actions,
				getters: {
					colors: () => getters.color.colors || [],
				},
			},
			countries: {
				namespaced: true,
				actions,
				getters: {
					countries: () => MockCountries,
				},
			},
			order: {
				namespaced: true,
				actions,
				getters: {
					additionalPaymentInformation: () =>
						getters.order.additionalPaymentInformation || {},
					additionalShippingInformation: () =>
						getters.order.additionalShippingInformation || {},
					deliveryInstructions: () => getters.order.deliveryInstructions || '',
					selectedBillingAddress: () =>
						getters.order.selectedBillingAddress || null,
					selectedPickupLocation: () =>
						getters.order.selectedPickupLocation || null,
					selectedPaymentMethod: () =>
						getters.order.selectedPaymentMethod || null,
					selectedShippingAddress: () =>
						getters.order.selectedShippingAddress || null,
					selectedShippingMethod: () =>
						getters.order.selectedShippingMethod || null,
					voucherCode: () => getters.order.voucherCode || '',
				},
			},
			organization: {
				namespaced: true,
				actions,
				getters: {
					settings: () => getters.organization.settings || {},
					taxMode: () => getters.organization.taxMode || 'inclusiveTax',
				},
			},
			service: {
				namespaced: true,
				actions,
				getters: {
					currentService: () => getters.service.currentService || null,
					currentServiceId: () => getters.service.currentServiceId || null,
					paymentMethods: () => getters.service.paymentMethods || [],
					pickupLocations: () => getters.service.pickupLocations || [],
					shippingMethods: () => getters.service.shippingMethods || [],
				},
			},
			user: {
				namespaced: true,
				actions,
				getters: {
					userAddresses: () => getters.user.userAddress || [],
					userCurrency: () => getters.user.userCurrency || 'EUR',
				},
			},
		},
	});
