export const MockPickupOrderPayload = {
	projectId: 4,
	billingAddressId: 0,
	pickupLocationId: 0,
	shipping: null,
	voucherCode: '',
	payment: {
		methodId: 0,
		details: {},
		authorizedAmount: '19.03',
	},
	additionalInformation: {
		reference: 'fake reference',
		comment: 'fake comments',
	},
};

export const MockShippedOrderPayload = {
	projectId: 4,
	billingAddressId: 0,
	pickupLocationId: null,
	voucherCode: '',
	shipping: {
		methodId: 0,
		addressId: 1,
		deliveryInstructions: 'fake delivery instructions',
	},
	payment: {
		methodId: 0,
		details: {},
		authorizedAmount: '19.03',
	},
	additionalInformation: {
		reference: 'fake reference',
		comment: 'fake comments',
	},
};
