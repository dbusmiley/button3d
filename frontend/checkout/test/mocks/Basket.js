export const MockProject = {
	id: 4,
	lines: [
		{
			id: 3,
			quantity: 1,
			name: '100004-Brake_caliper-1',
			thumbnailUrl:
				'/en/u/bf1eea79-83d2-4249-8fff-43abac823a83/download_thumbnail/',
			materialName: { en: 'Alumide', de: 'Alumide' },
			postProcessings: [],
		},
	],
};

export const MockProjectPrice = {
	currency: 'EUR',
	taxRate: '19.00000',
	taxType: 'VAT',
	lines: [
		{
			lineId: 3,
			quantity: 1,
			price: {
				unitPrice: { exclusiveTax: '15.99', inclusiveTax: '19.03' },
				itemDiscount: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
				itemTotal: { exclusiveTax: '15.99', inclusiveTax: '19.03' },
			},
		},
	],
	fees: [],
	minimumPrice: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
	priceSummary: {
		subTotal: { exclusiveTax: '15.99', inclusiveTax: '19.03' },
		shippingCost: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
		voucherDiscount: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
		fees: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
		minimumPriceDifference: { exclusiveTax: '0.00', inclusiveTax: '0.00' },
		total: { exclusiveTax: '15.99', inclusiveTax: '19.03' },
	},
};
