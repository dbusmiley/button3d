export const MockPaymentMethods = [
	{
		id: 0,
		type: 'Invoice',
		additionalInformation: {},
		isDefault: true,
	},
	{
		id: 1,
		type: 'Credit Card',
		additionalInformation: {},
		isDefault: false,
	},
];

export const MockPickupLocations = [
	{
		id: 0,
		location: '4D Headquarters, 9 Bismarckstraẞe, Berlin 10625',
		instructions: 'Beware of the fog',
	},
	{
		id: 1,
		location: '4D Boutique, 32 DanzigStraẞe, Berlin 10987',
		instructions: 'Open from 12pm—2pm on Thursdays',
	},
];

export const MockService = {
	id: 23,
	name: '4D Solutions Berlin',
	logo: 'https://image.ibb.co/c1ApFJ/approve_black_and_white_hand_8252.jpg',
	website: 'http://4d.solutions.berlin',
	email: 'infos@4dsolutions.de',
	city: 'Berlin',
	country: 'Germany',
	description:
		'A multinational conglomerate offering the latest in bleeding edge #blockchain-based 4D printing solutions.',
};
