export const MockAdditionalShippingInformation = {
	comment: 'fake comments',
	reference: 'fake reference',
};

export const MockDeliveryInstructions = 'fake delivery instructions';

export const MockSelectedBillingAddress = {
	id: 0,
	title: 'Mr',
	firstName: 'Nigel',
	lastName: 'Berkins',
	company: {
		name: '4d Solutions',
		department: 'Accounts',
		vatId: 'DE123456789',
	},
	line1: 'Bismarckstraẞe 9',
	line2: 'bei Mettman',
	zipCode: '10987',
	city: 'Berlin',
	state: 'Berlin',
	country: 'DE',
	phoneNumber: '+4915226784567',
	isDefault: true,
	verificationStatus: 'unverified',
};

export const MockSelectedPaymentMethod = {
	additionalInformation: {},
	id: 0,
	isDefault: true,
	type: 'Invoice',
};

export const MockSelectedPickupLocation = {
	id: 0,
	location: '4D Headquarters, 9 Bismarckstraẞe, Berlin 10625',
	instructions: 'Beware of the fog',
};

export const MockSelectedShippingAddress = {
	id: 1,
	title: 'Mr',
	firstName: 'Nigel',
	lastName: 'Berkins',
	company: {
		name: '4d Solutions',
		department: 'Accounts',
		vatId: 'DE123456789',
	},
	line1: 'Bismarckstraẞe 9',
	line2: 'bei Mettman',
	zipCode: '10987',
	city: 'Berlin',
	state: 'Berlin',
	country: 'DE',
	phoneNumber: '+4915226784567',
	isDefault: true,
	verificationStatus: 'unverified',
};

export const MockSelectedShippingMethod = {
	id: 0,
	name: 'Super Premium',
	description: 'Padded 4D Delivery',
	deliveryDays: {
		minimum: 0,
		maximum: 1,
	},
	currency: 'EUR',
	price: {
		inclusiveTax: 0.0,
		exclusiveTax: 0.0,
	},
};
