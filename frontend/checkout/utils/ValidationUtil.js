export const validateVolkswagenPayment = (formData) => {
	const errors = {};
	const requiredFields = [
		'creatorName',
		'creatorEmail',
		'provider',
		'proposalNumber',
		'proposal',
		'description',
		'deliveryDate',
		'chargeFrom',
		'chargeTo',
	];

	requiredFields.forEach((fieldName) => {
		if (!formData[fieldName]) {
			errors[fieldName] = ' ';
		}
	});

	return errors;
};

export const validatePOUploadPayment = (formData) => {
	if (!formData.attachments || !formData.attachments.length) {
		return { attachments: ' ' };
	}
	return {};
};

export const validatePaymentMethodConfig = (
	selectedPaymentMethod,
	additionalPaymentInformation,
) => {
	const typeName = selectedPaymentMethod.type;

	const paymentMethodValidators = {
		volkswagen: validateVolkswagenPayment,
		'po-upload': validatePOUploadPayment,
	};
	const validator = paymentMethodValidators[typeName];
	if (validator) {
		const errors = validator(additionalPaymentInformation);
		if (Object.keys(errors).length) {
			console.warn('Payment validation failed', errors);
			return false;
		}
	}

	return true;
};
