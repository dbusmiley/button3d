const colors = {
	red: [0xff0000, 16],
	green: [0x00ff00, 8],
	blue: [0x0000ff, 0],
};

/* eslint-disable-next-line */
const getColorFrom = (hex) => ([mask, offset]) => (hex & mask) >> offset;

export default (rgb, alpha) => {
	const hex = parseInt(rgb.substring(1), 16);

	const channels = [colors.red, colors.green, colors.blue]
		.map(getColorFrom(hex))
		.concat(alpha);

	return `rgba(${channels.join(', ')})`;
};
