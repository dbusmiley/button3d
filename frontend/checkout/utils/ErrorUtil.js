export const translateErrors = (errorMessage) => {
	const translationDict = {
		'Enter a valid phone number.': 'errors.phoneNumberInvalid',
	};
	return translationDict[errorMessage];
};

export const translateErrorCodes = (errorCode) => {
	const errorCodeDict = {
		ERROR_NO_SERVICE: 'errors.noService',
		ERROR_SERVICE_DELETED: 'errors.serviceDeleted',
		ERROR_PROJECT_DELETED: 'errors.projectDeleted',
		ERROR_MULTIPLE_CURRENCIES: 'errors.multipleCurrencies',
		ERROR_NO_STOCK_RECORD: 'errors.noStockRecord',
		ERROR_STOCK_RECORD_DELETED: 'errors.stockRecordDeleted',
		ERROR_STOCK_RECORD_WRONG_SERVICE: 'errors.stockRecordWrongService',
		ERROR_NO_STL_FILE: 'errors.noStlFile',
		ERROR_STL_FILE_DELETED: 'errors.stlFileDeleted',
		ERROR_STL_FILE_ANALYZE_PENDING: 'errors.stlFileAnalysePending',
		ERROR_STL_FILE_NO_PARAMETER: 'errors.stlFileNoParameter',
		ERROR_NO_CONFIGURATION: 'errors.noConfiguration',
		ERROR_POST_PROCESSING_DELETED: 'errors: postProcessingDeleted',
		ERROR_POST_PROCESSING_INVALID_COLOR: 'errors.postProcessingInvalidColor',
		ERROR_POST_PROCESSING_WRONG_SERVICE: 'errors.postProcessingWrongService',
		ERROR_NO_PARTS: 'errors.noParts',
		ERROR_NO_PRICING_POSSIBLE: 'errors.noPricingPossible',
		ERROR_MANUAL_PRICING_REQUIRED: 'errors.manualPricingRequired',
		ERROR_EITHER_SHIPPING_OR_PICKUP: 'errors.eitherShippingOrPickup',
		ERROR_PICKUP_DELETED: 'errors.pickupDeleted',
		ERROR_PICKUP_WRONG_SERVICE: 'errors.pickupWrongService',
		ERROR_SHIPPING_METHOD_DELETED: 'errors.shippingMethodDeleted',
		ERROR_SHIPPING_METHOD_WRONG_SERVICE: 'errors.shippingMethodWrongService',
		ERROR_SHIPPING_METHOD_WRONG_COUNTRY: 'errors.shippingMethodWrongCountry',
		ERROR_NO_SHIPPING_ADDRESS: 'errors.noShippingAddress',
		ERROR_SHIPPING_ADDRESS_INVALID: 'errors.shippingAddressInvalid',
		ERROR_NO_BILLING_ADDRESS: 'errors.noBillingAddress',
		ERROR_BILLING_ADDRESS_INVALID: 'errors.billingAddressInvalid',
		ERROR_VOUCHER_INVALID: 'errors.voucherInvalid',
		ERROR_NO_PAYMENT_METHOD: 'errors.noPaymentMethod',
		ERROR_PAYMENT_METHOD_DELETED: 'errors.paymentMethodDeleted',
		ERROR_PAYMENT_METHOD_WRONG_SERVICE: 'errors.paymentMethodWrongService',
		ERROR_PAYMENT_WRONG_PRICE: 'errors.paymentWrongPrice',
		ERROR_PAYMENT_FAILED: 'errors.paymentFailed',
		ERROR_PAYMENT_NOT_AUTHORIZED: 'errors.paymentNotAuthorized',
		ERROR_ORDER_PLACEMENT_FAILED: 'errors.orderPlacementFailed',
	};
	return errorCodeDict[errorCode] || 'errors.projectCouldNotBeOrdered';
};
