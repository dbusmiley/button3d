import rgba from './colors/rgba';
import shade from './colors/shade';

const renderedCss = (accentColor) =>
	`a {
	color: ${accentColor} !important;
}
a:focus, a:hover {
	color: ${shade(accentColor, -0.2)} !important;
	text-decoration: none;
}
.kotti-button {
	color: ${shade(accentColor, -0.1)} !important;
}
.kotti-button:hover {
	color: #FFFFFF !important;
	background: ${shade(accentColor, -0.1)};
}
.kotti-button.primary {
	color: #FFFFFF !important;
	background: ${accentColor} !important;
}
.kotti-button.primary:hover {
	background: ${shade(accentColor, -0.1)} !important;
}
.kotti-button.secondary {
	color: ${accentColor};
}
.kotti-button.secondary:hover {
	color: #FFFFFF;
	background: ${shade(accentColor, 0.1)} !important;
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}

.kotti-button.text {
	color: ${accentColor};
}

.kotti-button.danger {
	color: #D91919 !important;
}

.kotti-button.danger:hover {
	color: #FFFFFF !important;
}

.kotti-form-input:focus {
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}
.kotti-form-select:focus {
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}
.kotti-form-switch.form-icon input:focus {
	border-color: ${accentColor};
}
.kotti-form-switch.form-icon input:checked {
	background: ${accentColor};
	border-color: ${accentColor};
}


.kotti-form-radio > input:focus + .form-icon {
	background: ${accentColor};
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}

.kotti-form-radio > input:checked + .form-icon {
	background: ${accentColor};
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}

.kotti-form-checkbox > input:focus + .form-icon {
	background: ${accentColor};
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}

.kotti-form-checkbox > input:checked + .form-icon {
	background: ${accentColor};
	border-color: ${accentColor};
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)};
}

.kotti-loading::after {
	border: $border-width-lg solid ${accentColor};
}

.kotti-h3 {
	color: ${accentColor} !important;
}

.kotti-accent-color {
	color: ${accentColor} !important;
}

.kotti-accent-background {
	background: ${accentColor} !important;
}

.multiselect--active {
	border: 1px solid ${accentColor} !important;
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)} !important;
}

.multiselect__option--highlight,
.multiselect__option--selected {
	background: ${accentColor} !important;
	color: #fff !important;
}

.StripeElement--focus {
	box-shadow: 0 0 0 0.1rem ${rgba(accentColor, 0.64)} !important;
}

`;

export default (settings) => {
	if (!settings.primaryThemeColor) {
		return;
	}
	const element = window.document.createElement('style');
	element.innerHTML = renderedCss(settings.primaryThemeColor);
	window.document.head.appendChild(element);
};
