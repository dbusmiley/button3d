import { translateAlphaTwo } from './CountryUtil.js';

export function prettyPrintAddress(address, locale) {
	const addressLine = address.line2 ? `${address.line1} ${address.line2}` : address.line1;
	const fullName = `${address.firstName} ${address.lastName}`;
	const countryLocalized = translateAlphaTwo(address.country, locale);

	return `${fullName}, ${addressLine}, ${address.zipCode} ${address.city}, ${countryLocalized}`;
}
