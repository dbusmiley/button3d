import Countries from 'i18n-iso-countries';
import CountriesDE from 'i18n-iso-countries/langs/de.json';
import CountriesEN from 'i18n-iso-countries/langs/en.json';

Countries.registerLocale(CountriesDE);
Countries.registerLocale(CountriesEN);

export const translateAlphaTwo = (alphaTwoCode, targetLanguage) => {
	return Countries.getName(alphaTwoCode, targetLanguage);
};
