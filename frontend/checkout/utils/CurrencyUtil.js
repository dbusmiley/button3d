export const localizedCurrencyString = (locale, value, currency) =>
	parseFloat(value).toLocaleString(locale, {
		style: 'currency',
		currency,
	});
