### Installation

`yarn install`

### Dev Server

Seeing as this checkout is embedded in the old frontend, there is unfortunately a little bit of setup to do. You'll first need to create a project which is orderable (i.e. has a supplier selected and is not manually priced.) Once this is done, make a note of the project ID and also the supplier ID. You can find ID of your selected supplier by querying the `http://multi.my.3yd/v1/suppliers/?lang=en&country=DE&domain=multi.my.3yd` endpoint.

Now, you need to add these IDs to `frontend/checkout/index.html`. In there, you will find a `projectCheckoutBridge` object. Change the project and service IDs and you're good to go!

Run `yarn run checkout:dev` to get a hot reloading dev server, with Vue devtools available. The project will be available at `http://multi.my.3yd:8456/`.

#### For devs not using docker-compose

You'll need to change the webpack config in the `frontend/checkout` folder to point api calls to wherever your backend is hosted (you'll probably just need to change the port number).

### Building

To test the checkout locally, you'll probably want to skip downloading translations. You can do this by running `yan run build:local`. Make sure the checkout is activated (you can do this in the Django admin panel; search for `Switches`) and try ordering a project!

### Translations

To test whether translations are working, run `yarn run containers:build`.
This is also what gets run inside the docker-compose setup, but you shouldn't have to worry about it.

You will need the crowdin cli installed to upload translations. Information about this can be found here:
https://support.crowdin.com/cli-tool/

Once that's done, you can just `yarn run checkout:uploadTranslations`

To build for in-context translation, run `yarn run containers:buildCrowdin`.
This can also be done in on the staging servers. You should run a customised
build and set `CHECKOUT_INTERACTIVE_TRANSLATIONS` to `true`. Talk to Mattu about this if you want to know more.

### Testing

Run `yarn test`, or `yarn test --watch` to automatically run tests which have changed.
You can also debug tests by running `node --inspect-brk node_modules/.bin/jest --runInBand` and visiting `chrome://inspect`. Use normal debugger statements to trigger breakpoints.
