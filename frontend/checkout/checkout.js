import 'es6-promise/auto';

import axios from 'axios';
import cookie from 'cookie';
import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';
import Vue from 'vue';
import Vuex from 'vuex';
import VueI18n from 'vue-i18n';
import Checkout from './Checkout.vue';
import store from './vuex';
import router from './router';

Vue.use(Vuex);
Vue.use(VueI18n);

const { csrftoken } = cookie.parse(document.cookie);

axios.interceptors.request.use(async (config) => {
	config.headers['X-CSRFToken'] = csrftoken;
	return config;
});

if (window.projectCheckoutBridge.sentryUrl) {
	Raven.config(window.projectCheckoutBridge.sentryUrl)
		.addPlugin(RavenVue, Vue)
		.install();
	axios.interceptors.response.use(
		(response) => response,
		(error) => {
			if (error.response.status >= 500) {
				Raven.captureException(error);
			}
			return Promise.reject(error);
		},
	);
}

const createApp = () => {
	/* eslint-disable-next-line */
	new Vue({
		el: '#checkout',
		store,
		router,
		render: (h) => h(Checkout),
	});
};

// check if the checkout is embedded inside button3d
const projectOrderButton = document.querySelector('#new-checkout-link');
if (projectOrderButton) {
	document.addEventListener('createCheckout', () => {
		document.querySelector('#b3-checkout-mask').style.display = 'block';
		document.querySelector('html').style['overflow-y'] = 'hidden';
		createApp();
	});
} else {
	createApp();
}
