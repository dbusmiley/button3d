const pricePayload = (state) => ({
	currency: state.basket.currentProjectPrice
		? state.basket.currentProjectPrice.currency
		: null,
	projectId: state.basket.projectId,
	shippingAddressId: state.order.selectedShippingAddress
		? state.order.selectedShippingAddress.id
		: null,
	billingAddressId: state.order.selectedBillingAddress
		? state.order.selectedBillingAddress.id
		: null,
	shippingMethodId: state.order.selectedShippingMethod
		? state.order.selectedShippingMethod.id
		: null,
	voucherCode: state.order.voucherCode || null,
});

export default {
	pricePayload,
};
