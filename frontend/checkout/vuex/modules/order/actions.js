import axios from 'axios';

const headers = {
	'Content-Type': 'application/json',
};

const handleStripeToken = (context, token) => {
	context.commit('SET_ADDITIONAL_PAYMENT_INFORMATION', { token });
};

const initializeNetsPayment = (context, payload) => {
	const { serviceId, ...orderPayload } = payload;
	return axios
		.post(
			`/api/v2.0/user-panel/services/${serviceId}/payment/initialize/`,
			orderPayload,
			{ headers },
		)
		.then((response) => response)
		.catch((error) => {
			console.error(error, 'Error creating order.');
			throw error;
		});
};

const placeOrder = (context, payload) =>
	axios
		.post(
			`/api/v2.0/user-panel/projects/${payload.projectId}/orders/`,
			payload.orderPayload,
			{
				headers,
			},
		)
		.then((response) => response)
		.catch((error) => {
			console.error(error, 'Error creating order.');
			throw error;
		});

const saveAdditionalShippingInformation = (context, shippingInformation) => {
	context.commit('SAVE_ADDITIONAL_SHIPPING_INFORMATION', shippingInformation);
};

const saveDeliveryInstructions = (context, deliveryInstructions) => {
	context.commit('SAVE_DELIVERY_INSTRUCTIONS', deliveryInstructions);
};

const selectPickupLocation = (context, pickupLocation) => {
	context.commit('SELECT_PICKUP_LOCATION', pickupLocation);
	context.commit('SELECT_SHIPPING_ADDRESS', null);
	context.commit('RESET_SHIPPING_METHOD');
};

const selectShippingAddress = (context, shippingAddress) => {
	context.commit('SELECT_SHIPPING_ADDRESS', shippingAddress);
	context.commit('SELECT_PICKUP_LOCATION', null);
	context.commit('RESET_SHIPPING_METHOD');
};

const selectPaymentMethod = (context, paymentMethod) => {
	context.commit('SELECT_PAYMENT_METHOD', paymentMethod);
};

const selectShippingMethod = (context, shippingMethod) => {
	context.commit('SELECT_SHIPPING_METHOD', shippingMethod);
};

const selectBillingAddress = (context, billingAddress) => {
	context.commit('SELECT_BILLING_ADDRESS', billingAddress);
};

const setVoucherCode = (context, voucherCode) => {
	context.commit('SET_VOUCHER_CODE', voucherCode);
};

const setAdditionalPaymentInformation = (context, paymentInformation) => {
	context.commit('SET_ADDITIONAL_PAYMENT_INFORMATION', paymentInformation);
};

const uploadAttachment = (context, payload) => {
	const attachmentHeaders = {
		Accept: 'application/json',
		'Content-type': 'multipart/form-data',
	};
	return axios
		.post(
			`/api/v2.0/user-panel/services/${
				payload.serviceId
			}/payment/attachments/`,
			payload.formData,
			{
				headers: attachmentHeaders,
			},
		)
		.then((response) => {
			context.commit('SET_ADDITIONAL_PAYMENT_INFORMATION', {
				attachments: [response.data.id],
			});
		})
		.catch((error) => {
			console.error(error, 'Error uploading attachment.');
			throw error;
		});
};

export default {
	handleStripeToken,
	initializeNetsPayment,
	placeOrder,
	saveAdditionalShippingInformation,
	saveDeliveryInstructions,
	selectPickupLocation,
	selectShippingAddress,
	selectPaymentMethod,
	selectBillingAddress,
	selectShippingMethod,
	setAdditionalPaymentInformation,
	setVoucherCode,
	uploadAttachment,
};
