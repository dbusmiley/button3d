const SAVE_ADDITIONAL_SHIPPING_INFORMATION = (state, shippingInformation) => {
	state.additionalShippingInformation = shippingInformation;
};

const SAVE_DELIVERY_INSTRUCTIONS = (state, deliveryInstructions) => {
	state.deliveryInstructions = deliveryInstructions;
};
const SELECT_SHIPPING_ADDRESS = (state, shippingAddress) => {
	state.selectedShippingAddress = shippingAddress;
};

const SELECT_PICKUP_LOCATION = (state, pickupLocation) => {
	state.selectedPickupLocation = pickupLocation;
};

const SELECT_SHIPPING_METHOD = (state, shippingMethod) => {
	state.selectedShippingMethod = shippingMethod;
};

const RESET_SHIPPING_METHOD = (state) => {
	state.selectedShippingMethod = null;
};

const SELECT_PAYMENT_METHOD = (state, paymentMethod) => {
	state.selectedPaymentMethod = paymentMethod;
};

const SELECT_BILLING_ADDRESS = (state, billingAddress) => {
	state.selectedBillingAddress = billingAddress;
};

const SET_VOUCHER_CODE = (state, voucherCode) => {
	state.voucherCode = voucherCode;
};

const SET_ADDITIONAL_PAYMENT_INFORMATION = (
	state,
	additionalPaymentInformation,
) => {
	state.additionalPaymentInformation = additionalPaymentInformation;
};

export default {
	SAVE_ADDITIONAL_SHIPPING_INFORMATION,
	SAVE_DELIVERY_INSTRUCTIONS,
	SET_ADDITIONAL_PAYMENT_INFORMATION,
	SELECT_SHIPPING_ADDRESS,
	SELECT_PICKUP_LOCATION,
	SELECT_SHIPPING_METHOD,
	SELECT_PAYMENT_METHOD,
	SELECT_BILLING_ADDRESS,
	SET_VOUCHER_CODE,
	RESET_SHIPPING_METHOD,
};
