const additionalPaymentInformation = (state) =>
	state.additionalPaymentInformation;
const additionalShippingInformation = (state) =>
	state.additionalShippingInformation;
const deliveryInstructions = (state) => state.deliveryInstructions;
const selectedShippingAddress = (state) => state.selectedShippingAddress;
const selectedPickupLocation = (state) => state.selectedPickupLocation;
const selectedShippingMethod = (state) => state.selectedShippingMethod;
const selectedPaymentMethod = (state) => state.selectedPaymentMethod;
const selectedBillingAddress = (state) => state.selectedBillingAddress;
const voucherCode = (state) => state.voucherCode;

export default {
	additionalPaymentInformation,
	additionalShippingInformation,
	deliveryInstructions,
	selectedShippingAddress,
	selectedPickupLocation,
	selectedShippingMethod,
	selectedPaymentMethod,
	selectedBillingAddress,
	voucherCode,
};
