import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const defaultState = {
	additionalPaymentInformation: {},
	additionalShippingInformation: {
		reference: '',
	},
	deliveryInstructions: '',
	selectedShippingAddress: null,
	selectedPickupLocation: null,
	selectedShippingMethod: null,
	selectedPaymentMethod: null,
	selectedBillingAddress: null,
	voucherCode: null,
};

export default {
	namespaced: true,
	state: defaultState,
	actions,
	getters,
	mutations,
};
