const settings = (state) => state.settings;
const taxMode = (state) => {
	return state.settings.showNetPrices ? 'exclusiveTax' : 'inclusiveTax';
};
const stripePublishableKey = (state) => {
	return state.settings.stripePublishableKey;
};
const isNetPricesOn = (state) => {
	return state.settings.showNetPrices;
};

export default {
	settings,
	taxMode,
	isNetPricesOn,
	stripePublishableKey,
};
