const UPDATE_CURRENT_ORG_SETTINGS = (state, settings) => {
	state.settings = settings;
};
export default {
	UPDATE_CURRENT_ORG_SETTINGS,
};
