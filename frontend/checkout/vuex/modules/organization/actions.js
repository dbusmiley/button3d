import axios from 'axios';

const headers = {
	Accept: 'application/json',
};

const getSettings = (context) =>
	axios
		.get('/api/v2.0/settings/', { headers })
		.then((response) => {
			context.commit('UPDATE_CURRENT_ORG_SETTINGS', response.data);
			return response;
		})
		.catch((error) => {
			console.error(error, 'Error fetching organization settings.');
			throw error;
		});

export default {
	getSettings,
};
