import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
	userAddresses: [],
	userCurrency: '',
	userPreferences: {},
	addressErrors: [],
};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
