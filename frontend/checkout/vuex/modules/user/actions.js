import axios from 'axios';

const headers = {
	Accept: 'application/json',
	'Content-type': 'application/json',
};

const getUserAddresses = (context) =>
	axios
		.get('/api/v2.0/user-panel/addresses/', { headers })
		.then((response) => {
			context.commit('UPDATE_USER_ADDRESSES', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching user addresses.');
			throw error;
		});

const updateAddress = (context, payload) => {
	const { id, ...rest } = payload;
	const url = id ?
		`/api/v2.0/user-panel/addresses/${id}/` :
		'/api/v2.0/user-panel/addresses/';
	const method = id ? axios.put : axios.post;

	return method(url, rest, { headers })
		.then((response) => {
			context.dispatch('getUserAddresses');
			return response;
		})
		.catch((error) => {
			console.error(error, 'Error updating address.');
			throw error;
		});
};

const setUserCurrency = (context, payload) => {
	context.commit('UPDATE_USER_CURRENCY', payload);
};

const getUserPreferences = (context) =>
	axios
		.get(`/api/v2.0/users/preference/`, { headers })
		.then((response) => {
			context.commit('UPDATE_USER_PREFERENCES', response.data);
			return response;
		})
		.catch((error) => {
			console.error(error, 'Error fetching User settings.');
			throw error;
		});


export default {
	getUserAddresses,
	setUserCurrency,
	getUserPreferences,
	updateAddress,
};
