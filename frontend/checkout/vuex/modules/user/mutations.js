const UPDATE_USER_ADDRESSES = (state, userAddresses) => {
	state.userAddresses = userAddresses;
};

const UPDATE_USER_CURRENCY = (state, userCurrency) => {
	state.userCurrency = userCurrency;
};

const UPDATE_USER_PREFERENCES = (state, userPreferences) => {
	state.userPreferences = userPreferences;
};

export default {
	UPDATE_USER_ADDRESSES,
	UPDATE_USER_CURRENCY,
	UPDATE_USER_PREFERENCES,
};
