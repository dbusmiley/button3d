const userAddresses = (state) => state.userAddresses;
const userCurrency = (state) => state.userCurrency;
const userPreferences = (state) => state.userPreferences;

export default { userAddresses, userCurrency, userPreferences };
