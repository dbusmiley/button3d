const UPDATE_PROJECT_ID = (state, projectId) => {
	state.projectId = projectId;
};

const UPDATE_PROJECT = (state, project) => {
	state.currentProject = project;
};
const UPDATE_PROJECT_PRICE = (state, projectPrice) => {
	state.currentProjectPrice = projectPrice;
};

const UPDATE_USER_CURRENCY_PRICE = (state, projectPrice) => {
	state.userCurrencyPrice = projectPrice;
};

export default {
	UPDATE_PROJECT_ID,
	UPDATE_PROJECT,
	UPDATE_PROJECT_PRICE,
	UPDATE_USER_CURRENCY_PRICE,
};
