import axios from 'axios';

const headers = {
	Accept: 'application/json',
};

const setProjectId = (context, projectId) => {
	context.commit('UPDATE_PROJECT_ID', projectId);
};

const getProject = (context, projectId) =>
	axios
		.get(`/api/v2.0/user-panel/projects/${projectId}/`, { headers })
		.then((response) => {
			context.commit('UPDATE_PROJECT', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching project.');
			throw error;
		});

const getProjectPrices = (
	context,
	{
		billingAddressId,
		currency,
		projectId,
		shippingAddressId,
		shippingMethodId,
		voucherCode,
	},
) =>
	axios
		.get(`/api/v2.0/user-panel/projects/${projectId}/price/`, {
			headers,
			params: {
				billingAddressId,
				currency,
				shippingAddressId,
				shippingMethodId,
				voucherCode,
			},
		})
		.then((response) => {
			context.commit('UPDATE_PROJECT_PRICE', response.data);
			return response;
		})
		.catch((error) => {
			console.error(error, 'Error fetching project price.');
			throw error;
		});

const getUserCurrencyPrices = (
	context,
	{
		billingAddressId,
		currency,
		projectId,
		shippingAddressId,
		shippingMethodId,
		voucherCode,
	},
) =>
	axios
		.get(`/api/v2.0/user-panel/projects/${projectId}/price/`, {
			headers,
			params: {
				billingAddressId,
				currency,
				shippingAddressId,
				shippingMethodId,
				voucherCode,
			},
		})
		.then((response) => {
			context.commit('UPDATE_USER_CURRENCY_PRICE', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching project price.');
			throw error;
		});

export default {
	setProjectId,
	getProject,
	getProjectPrices,
	getUserCurrencyPrices,
};
