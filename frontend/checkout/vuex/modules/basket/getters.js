const currentProject = (state) => state.currentProject;
const currentProjectPrice = (state) => state.currentProjectPrice;
const projectId = (state) => state.projectId;
const userCurrencyPrice = (state) => state.userCurrencyPrice;

export default {
	projectId,
	currentProject,
	currentProjectPrice,
	userCurrencyPrice,
};
