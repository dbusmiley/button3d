import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const defaultState = {
	projectId: null,
	currentProject: null,
	currentProjectPrice: null,
	userCurrencyPrice: null,
};

export default {
	namespaced: true,
	state: defaultState,
	actions,
	getters,
	mutations,
};
