const countries = (state) => state.countries;

export default {
	countries,
};
