import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const defaultState = {
	countries: [],
};

export default {
	namespaced: true,
	state: defaultState,
	actions,
	getters,
	mutations,
};
