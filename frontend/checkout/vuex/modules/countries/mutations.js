const UPDATE_COUNTRIES = (state, countries) => {
	state.countries = countries;
};

export default {
	UPDATE_COUNTRIES,
};
