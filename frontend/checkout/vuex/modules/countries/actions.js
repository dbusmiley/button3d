import axios from 'axios';

const headers = {
	'Content-Type': 'application/json',
};

const getAllCountries = (context) =>
	axios
		.get('/api/v2.0/countries/', { headers })
		.then((response) => {
			context.commit('UPDATE_COUNTRIES', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching countries.');
			throw error;
		});

export default {
	getAllCountries,
};
