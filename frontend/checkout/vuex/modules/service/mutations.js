const SET_SERVICE_ID = (context, serviceId) => {
	context.currentServiceId = serviceId;
};

const UPDATE_CURRENT_SERVICE = (context, service) => {
	context.currentService = service;
};

const UPDATE_PICKUP_LOCATIONS = (context, pickupLocations) => {
	context.pickupLocations = pickupLocations;
};

const UPDATE_SHIPPING_METHODS = (context, shippingMethods) => {
	context.shippingMethods = shippingMethods;
};

const UPDATE_PAYMENT_METHODS = (context, paymentMethods) => {
	context.paymentMethods = paymentMethods;
};

export default {
	SET_SERVICE_ID,
	UPDATE_CURRENT_SERVICE,
	UPDATE_PICKUP_LOCATIONS,
	UPDATE_SHIPPING_METHODS,
	UPDATE_PAYMENT_METHODS,
};
