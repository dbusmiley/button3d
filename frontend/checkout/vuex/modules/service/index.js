import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const defaultState = {
	currentService: null,
	currentServiceId: null,
	pickupLocations: [],
	shippingMethods: [],
	paymentMethods: [],
};

export default {
	namespaced: true,
	state: defaultState,
	actions,
	getters,
	mutations,
};
