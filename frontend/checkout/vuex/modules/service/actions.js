import axios from 'axios';

const headers = {
	Accept: 'application/json',
};

const getCurrentService = (context, serviceId) =>
	axios
		.get(`/api/v2.0/user-panel/services/${serviceId}/`, { headers })
		.then((response) => {
			context.commit('UPDATE_CURRENT_SERVICE', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching printing service.');
			throw error;
		});

const getPickupLocations = (context, serviceId) =>
	axios
		.get(`/api/v2.0/user-panel/services/${serviceId}/pickup-locations/`, {
			headers,
		})
		.then((response) => {
			context.commit('UPDATE_PICKUP_LOCATIONS', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching printing service.');
			throw error;
		});

const getShippingMethods = (context, payload) =>
	axios
		.get(
			`/api/v2.0/user-panel/services/${payload.serviceId}/shipping-methods/`,
			{
				headers,
				params: {
					shippingAddressId: payload.shippingAddress.id,
					projectId: payload.projectId,
				},
			},
		)
		.then((response) => {
			context.commit('UPDATE_SHIPPING_METHODS', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching shipping methods.');
			throw error;
		});

const getPaymentMethods = (context, serviceId) =>
	axios
		.get(`/api/v2.0/user-panel/services/${serviceId}/payment-methods/`, {
			headers,
		})
		.then((response) => {
			context.commit('UPDATE_PAYMENT_METHODS', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching payment methods.');
			throw error;
		});

const setServiceId = (context, serviceId) =>
	context.commit('SET_SERVICE_ID', serviceId);

export default {
	getCurrentService,
	getPickupLocations,
	getShippingMethods,
	getPaymentMethods,
	setServiceId,
};
