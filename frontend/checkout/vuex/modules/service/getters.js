const currentService = (state) => state.currentService;
const currentServiceId = (state) => state.currentServiceId;
const pickupLocations = (state) => state.pickupLocations;
const shippingMethods = (state) => state.shippingMethods;
const paymentMethods = (state) => state.paymentMethods;

export default {
	currentService,
	currentServiceId,
	pickupLocations,
	shippingMethods,
	paymentMethods,
};
