import axios from 'axios';

const headers = {
	'Content-Type': 'application/json',
};

const getAllColors = (context) =>
	axios
		.get('/api/v2.0/colors/', { headers })
		.then((response) => {
			context.commit('UPDATE_COLORS', response.data);
		})
		.catch((error) => {
			console.error(error, 'Error fetching colors.');
			throw error;
		});

export default {
	getAllColors,
};
