const UPDATE_COLORS = (state, colors) => {
	state.colors = colors;
};

export default {
	UPDATE_COLORS,
};
