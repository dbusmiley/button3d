import Vue from 'vue';
import Vuex from 'vuex';

import getters from './getters';

import BasketModule from './modules/basket';
import ColorModule from './modules/color';
import CountriesModule from './modules/countries';
import OrderModule from './modules/order';
import OrganizationModule from './modules/organization';
import ServiceModule from './modules/service';
import UserModule from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		basket: BasketModule,
		color: ColorModule,
		countries: CountriesModule,
		order: OrderModule,
		organization: OrganizationModule,
		service: ServiceModule,
		user: UserModule,
	},
	getters,
});
