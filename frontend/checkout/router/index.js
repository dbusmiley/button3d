import Vue from 'vue';
import Router from 'vue-router';
import ShippingAddress from '../views/order/ShippingAddress.vue';
import ShippingMethod from '../views/order/ShippingMethod.vue';
import PaymentInformation from '../views/order/PaymentInformation.vue';

Vue.use(Router);

const routes = [
	{
		path: '/',
		redirect: '/shipping-address',
	},
	{
		path: '/shipping-address',
		name: 'shipping-address',
		component: ShippingAddress,
	},
	{
		path: '/shipping-method',
		name: 'shipping-method',
		component: ShippingMethod,
	},
	{
		path: '/payment-information',
		name: 'payment-information',
		component: PaymentInformation,
	},
];

const router = new Router({
	routes,
	mode: 'hash',
});

export default router;
