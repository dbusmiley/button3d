#!/bin/bash
# vim: ts=4 sw=4 et ai fo=croqn tw=78

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OPENAPI_SPECS=(openapi.yaml user_panel.yaml mes.yaml service_panel.yaml)
npx=$(which npx)
SWAGGER_CLI_VERS=$(${npx} swagger-cli --version)

cd $SCRIPT_DIR
[ ! -d test-reports ] && mkdir test-reports
pycodestyle --exclude=migrations,south_migrations,parsetab.py,node_modules,settings_build_docker.py ../
cd ../
ORIGINAL_BRANCH=$(git rev-parse --abbrev-ref HEAD)
git fetch origin development && git checkout development && git checkout $ORIGINAL_BRANCH
CHANGED_FILES=$(git diff --name-only development...HEAD| grep -v "/migrations/" | grep -e "\.py$")
if [[ ! -z "$CHANGED_FILES" ]]; then
    printf "Changed files: %s" "${CHANGED_FILES}"
    flake8  $CHANGED_FILES --exclude "*build_docker.py" --output-file ./devops/test-reports/flake8.txt
    flake8_junit ./devops/test-reports/flake8.txt ./devops/test-reports/flake8-junit.xml
else
    printf "WARNING: No changed files detected" >&2
fi

printf "Validating API specs using swagger-cli %s\n" ${SWAGGER_CLI_VERS}

for spec in ${OPENAPI_SPECS[*]}
do
    ${npx} swagger-cli validate apidocs/openapi/${spec}
    if [ $? -eq 0 ]
    then
        printf "OK: %s\n" ${spec}
    else
        printf "ERROR: apidocs/openapi/%s\n" ${spec} >&2
    fi
done
