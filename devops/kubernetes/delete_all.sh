#!/bin/bash

export name=$1
export b3tag=latest

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $SCRIPT_DIR
mkdir tmp
j2 ./db-deployment.yaml.j2 > ./tmp/dep.yml
j2 ./backend-deployment.yaml.j2 >> ./tmp/dep.yml 
j2 ./button3d-deployment.yaml.j2 >> ./tmp/dep.yml
j2 ./yoda-deployment.yaml.j2 >> ./tmp/dep.yml
j2 ./nginx-deployment.yaml.j2 >> ./tmp/dep.yml

kubectl delete -f $SCRIPT_DIR/tmp/dep.yml

#kubectl delete -f $SCRIPT_DIR/db-deployment.yaml
#kubectl delete -f $SCRIPT_DIR/backend-deployment.yaml
#kubectl delete -f $SCRIPT_DIR/button3d-deployment.yaml
#kubectl delete -f $SCRIPT_DIR/yoda-deployment.yaml
#kubectl delete -f $SCRIPT_DIR/nginx-deployment.yaml
