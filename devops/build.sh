#!/bin/bash -e

# This script Builds the 3YOURMIND Backend Container.

# Usage:
#     build.sh <IMAGE> <TAG> <CROWDIN_API_KEY>
#     build.sh <-h|--help>
# Parameter description:
#     IMAGE:          The name of the B3 Docker image
#     TAG:            The Tag of the b3-docker image to test

# From the IMAGE and TAG parameter, this scripts builds the full name of
# the image in the following way:
# <IMAGE>:<TAG>

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $SCRIPT_DIR

IMAGE=$1
TAG=$2

API_KEY="$bamboo_SECRET_CROWDIN_API_KEY"
PROJECT_ID="$bamboo_SECRET_CROWDIN_PROJECT_ID"
CHECKOUT_INTERACTIVE_TRANSLATIONS="$bamboo_CHECKOUT_INTERACTIVE_TRANSLATIONS"

if [[ "$bamboo_SECRET_CROWDIN_API_KEY" = "" ]] || \
  [[ "$bamboo_SECRET_CROWDIN_PROJECT_ID" = "" ]] || \
  [[ "$bamboo_CHECKOUT_INTERACTIVE_TRANSLATIONS" = "" ]]; then
  API_KEY=$CROWDIN_API_KEY
  PROJECT_ID=$CROWDIN_PROJECT_ID
  CHECKOUT_INTERACTIVE_TRANSLATIONS=$INTERACTIVE_TRANSLATIONS
fi

docker build \
  --build-arg CROWDIN_API_KEY=$API_KEY \
  --build-arg CROWDIN_PROJECT_ID=$PROJECT_ID \
  --build-arg CHECKOUT_INTERACTIVE_TRANSLATIONS=$CHECKOUT_INTERACTIVE_TRANSLATIONS \
  -f ../Dockerfile \
  -t $IMAGE:$TAG \
  ..

docker save $IMAGE:$TAG -o ../3yd-django.tgz
