#!/bin/bash 

set -uo pipefail


echo -e "\n##########################################"
echo "## code health: sanity check "
echo "##########################################"
#
#  checking the existance of the declared targets
#
TARGETS=( "/apps/" "/button3d/" "/manage.py" )
HERE="$(dirname $0)/.."
PATHS=()

for target in "${TARGETS[@]}"
do
  rel_path="${HERE}${target}"
  if test -r "$rel_path"; then
    echo "examining: ${rel_path}"
    PATHS+=(\
      "$(realpath ${rel_path})"\
      )
  else
    echo "codehealth: target $target is not a valid file, cant be read or is not existing!!!"
    exit 66
  fi
done
echo "OK"

echo -e "\n##########################################"
echo "## code health: bandit security linter "
echo "##########################################"
#
# linting all targets and saving output as html
#

bandit \
  --exclude node_modules/ \
  -r "${PATHS[@]}"
if test ! $? -eq 0; then
    echo "Bandit found some security issues. Please try to find a more secure solution"
    echo "You can run 'bandit --exclude node_modules/ -r .' locally to see the issues."
    echo "If you are really, really sure what you are doing, "
    echo "you can use '# nosec' in your code, to ignore a line."
  exit 42
fi

echo -e "\n##########################################"
echo "## code health: xenon: complexity check"
echo "##########################################"
#
# checking the code complexity with xenon which is based on radon
# the --max-* {A..F} args are setting the complexity limits. Be aware 
# that I will set this to fail the pipelines if having a bad day.
# read this: http://radon.readthedocs.io/en/latest/intro.html
#
XENON_RESULTS="${HERE}/xenon.log"
rm -f "${XENON_RESULTS}"
for target in "${PATHS[@]}"
do
  xenon \
    --max-average A \
    --max-modules A \
    --max-absolute A \
    "${target}" 2>&1 \
  | sort --reverse --key 7 \
      >> "${XENON_RESULTS}" 2>&1 \
  || echo "$target not OK!" 
done
echo "OK!"


if test "$(git rev-parse --abbrev-ref HEAD)"="development"; then
  sonar-scanner \
    -Dsonar.projectKey=button3d \
    -Dsonar.sources="$PWD" \
    -Dsonar.host.url=https://sonar.internal.3yourmind.com \
    -Dsonar.login=${bamboo_SONARQUBE_TOKEN_PASSWORD} \
    -Dsonar.analysis.scmRevision="$(git rev-parse HEAD)" \
    -Dsonar.projectVersion="$(cat version)"
else
  echo "sonar-scanner not running for branch $(git rev-parse --abbrev-ref HEAD)"
fi
