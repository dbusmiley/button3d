#!/bin/sh -ex
SPEC_FILES=$(cat <<'EOF'
mes.yaml
service_panel.yaml
user_panel.yaml
EOF
)
SRC_DIR=/data/openapi
BUILD_DIR=/data/build

for spec in ${SPEC_FILES}
do
	_tmp=${spec%.yaml}
	path=$(echo ${_tmp}|tr _ -)
	mkdir -p ${BUILD_DIR}/${path}
	printf "Building %s into %s\n" ${spec} ${path}
	redoc-cli bundle -o ${BUILD_DIR}/${path}/index.html ${SRC_DIR}/${spec}
done
