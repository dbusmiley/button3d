#!/bin/sh
redoc-cli serve --ssr --watch /data/openapi/${REDOC_DYNAMIC_SPEC} &
exec nginx -g "daemon off;"
