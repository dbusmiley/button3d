Price:
  type: object
  properties:
    inclusiveTax:
      type: string
      pattern: 'd{1,10}.d{2}'
    exclusiveTax:
      type: string
      pattern: 'd{1,10}.d{2}'

Currency:
  type: string
  enum:
    - EUR
    - USD
    - GBP

CurrencyReadonly:
  type: string
  readOnly: true
  enum:
    - EUR
    - USD
    - GBP

PostProcessing:
  type: object
  properties:
    name:
      type: string
    colorId:
      type: integer

TranslatedName:
  type: object
  properties:
    en:
      type: string
    de:
      type: string

PickupLocation:
  type: object
  properties:
    id:
      type: integer
    location:
      readOnly: true
      type: string
    instructions:
      type: string
    example:
      id: 12
      location: 'Bogenstraße 4, 10448 Berlin'
      instructions: 'Knock twice! Bitte zweimal klopfen!'

PermissionDenied:
  type: object
  properties:
    code:
      type: string
    message:
      type: string
    # Authentication missing provides these fields
    detail:
      type: string
    status_code:
      type: integer
    moreInfo:
      type: object
      properties:
        objectName:
          type: string
        objectId:
          type: string
        userId:
          type: string

PartnerName:
  type: string
  maxLength: 254
  description: |
    Name of the printing service partner

PaymentMethodName:
  type: string
  description: |
    Name of the payment method used to process the order.

DeliveryDays:
  type: array
  nullable: true
  minItems: 2
  maxItems: 2
  example: [2, 10]
  items:
    type: integer
    minimum: 0

OrderStatusName:
  type: string
  enum:
    - pending
    - printing
    - shipped
    - cancelled
  default: pending

OrderStatus:
  type: object
  description: |
    Historic statuses of the order. For orders placed before the new
    checkout was activated, the order's `datetime_placed` and the status'
    `created` will be identical and there will only be one status.
    The natural order of status changes is:
    1. pending
    2. printing
    3. shipped

    The `cancelled` status can replace the last 2. In the case of "old checkout"
    orders, it is possible that the order only has a cancelled state.
  properties:
    created:
      type: string
      format: datetime
    type:
      $ref: '#/OrderStatusName'

BaseOrder:
  type: object
  description: |
    Base information for an order.
  properties:
    id:
      type: integer
      readOnly: true
      example: 42
    number:
      type: string
      readOnly: true
      example: '#20180000293'
    datetimePlaced:
      type: string
      format: date-time
      description: |
        The date and time when the order was placed.
    customerReference:
      type: string
      maxLength: 254
      description: |
        Customer supplied reference. Should be searchable
      example: surv-drone-mockup-3-20181128
    currency:
      type: string
      maxLength: 12
      description: |
        Currency the order was placed in.
      example: EUR
    totalValue:
      type: string
      pattern: 'd{1,10}.d{2}'
      description: |
        Total value of the order, excluding tax.
      example: 207.41
    totalTax:
      type: string
      pattern: 'd{1,10}.d{2}'
      description: |
        Total tax amount of the order. This added to total_value makes the
        grand total including tax.
      example: 42.59
    status:
      $ref: '#/OrderStatus'
    paymentMethodName:
      $ref: '#/PaymentMethodName'
    deliveryDays:
      $ref: '#/DeliveryDays'

OrderInHistoryList:
  type: object
  description: |
    An order with a service partner that has been placed.
    This describes the current state of the order, the items associated with it
    and financial documents.
  allOf:
    - type: object
      properties:
        partnerName:
          $ref: '#/PartnerName'
        numberOfLines:
          type: integer
          description: |
            The number of lines in the order. This is the sum of all 3D designs
            ordered and the document information accompanying those designs if
            any.
        numberOfComments:
          type: integer
          minimum: 0
          description: |
            NOTE: NOT IMPLEMENTED
            Number of comments on the order

    - $ref: '#/BaseOrder'

OrderDetail:
  type: object
  description: |
    Detail information for an order. All financial ammounts, are encoded as
    strings, which represent fixed decimal numbers with 2 decimal places.
    The tax rate percentage has 5 decimal places.
  allOf:
    - type: object
      properties:
        taxRate:
          type: string
          pattern: '\d{1,2}\.\d{5}'
          description: |
            Tax rate percentage as a decimal (20.5 = 20.5%).
          example: '19.0000'
        minPriceDiffValue:
          type: string
          pattern: '\d{1,10}\.\d{2}'
          description: |
            Difference to minimum order amount, excluding tax. If the order
            excluding tax is less then this, then a fee is added.
          example: '5.45'
        minPriceDiffTax:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Tax component of the minimum price difference.
        minPriceValue:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Minimum order amount, excluding tax at the time of ordering.
          example: '50.00'
        minPriceTax:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Tax component of the minimum order amount.
          example: '9.50'
        feesValue:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Additional fees applied to the order, excluding tax.
          example: '2.00'
        feesTax:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Tax component of additional fees.
          example: '0.38'
        voucherDiscountValue:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Voucher discount excluding tax.
        voucherDiscountTax:
          type: string
          pattern: 'd{1,10}.d{2}'
          description: |
            Tax component for voucher discount.
        trackingInformation:
          type: string
          maxLength: 128
          description: |
            Free form tracking information. This may be a URL.
        deliveryInstructions:
          type: string
          description: |
            Instructions for package delivery, entered by the customer.
        project:
          type: integer
          minimum: 1
          description: |
            Primary key of the basket the order was created from.
        billingAddress:
          type: object
          minimum: 1
          nullable: true
          description: |
            Billing address. If this field null, then the payment method used to
            pay for this order, did not require a billing address.
            This usually means that the ordering party and the receiving party
            are part of the same entity.
          $ref: '../addresses/schemas.yaml#/AddressRead'
        shippingAddress:
          type: object
          minimum: 1
          nullable: true
          description: |
            Shipping address. If none was provided, then a pickup location
            should be set.
          $ref: '../addresses/schemas.yaml#/AddressRead'
        partner:
          type: object
          description: |
            Information about the service partner.
          properties:
            id:
              type: integer
              format: int32
            name:
              type: string
              description: |
                Name of the service partner.
            logo:
              type: string
              description: |
                Url to the logo for the given service partner.
            website:
              type: string
              description: |
                Fully qualified URL to the website of the service partner.
            email:
              type: string
              description: |
                Email address for general queries.
            address:
              $ref: '../addresses/schemas.yaml#/AddressRead'
            taxType:
              type: string
              description: |
                Name for the value added tax, according to the country of the
                service partner.
        shippingMethodName:
          type: string
          minimum: 1
          nullable: true
          description: |
            Primary key of the shipping method. If this is null, then the
            pick-up location must be present.
        pickupLocation:
          type: object
          properties:
            id:
              type: integer
              format: int32
            location:
              type: string
              description: |
                Location where the order should be picked up.
            instructions:
              type: string
              nullable: true
              description: |
                Instructions for pickup.
          nullable: true
          description: |
            Primary key of the pick-up location. If this is null, then the
            shipping method and shipping address must be present.
        invoicePrefix:
          type: string
          nullable: true
          description: |
            Prefix for the invoice. This is partner specific and can be changed
            at will. Therefore it is saved on the invoice itself and the full
            invoice number needs to be constructed by prepending the prefix.
        invoiceNumber:
          type: integer
          format: int64
          nullable: true
          description: |
            The invoice number.
        invoiceDocument:
          type: string
          nullable: true
          description: |
            Url to the PDF version of the invoice.
        isLegacyOrder:
          type: boolean
          default: false
          description: |
            Whether this order was placed with the old checkout. If this is the
            case, there's no status history.
        statuses:
          type: array
          items:
            $ref: '#/OrderStatus'

    - $ref: '#/BaseOrder'
