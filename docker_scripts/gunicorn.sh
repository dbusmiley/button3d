#!/bin/bash
echo "starting gunicorn"

exec python -W ignore /usr/local/bin/gunicorn button3d.wsgi:application \
    --bind 0.0.0.0:8080 \
    --workers 3 \
    --chdir /var/www/django/ \
    --reload
