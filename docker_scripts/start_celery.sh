#!/bin/bash -e

if [[ "${USE_CELERY,,}" = "true" ]]; then
  mkdir -p /var/www/mails
  cd /var/www/mails/
  python3.6 -m http.server 7777 &
fi

cd /var/www/django

exec celery worker -A button3d --loglevel info -E
