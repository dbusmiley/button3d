#!/bin/bash -e
export django_root=/var/www/django
export B3_VERSION=$(cat ${django_root}/version)

j2 $django_root/docker_scripts/server.conf.j2 > /etc/nginx/conf.d/server.conf

if [[ "${S3_SYNC_STATIC,,}" == "true" ]] && [[ "${STATICFILE_STORAGE}" = "s3" ]]; then
  /bin/sync_static.sh
fi

python wait_for_db.py

if [[ "${MIGRATE_DB,,}" == "true" ]]; then
  python manage.py migrate
fi

if [[ "${LOAD_SAMPLE_DATA,,}" == "true" ]]; then
  /bin/prepare_dev.sh
fi

if [ "${LOAD_DEMO_USERS,,}" == "true" ]; then
  python manage.py load_staging_users "${LOAD_DEMO_USERS_DOMAIN}"
fi

if [[ $DEBUG == "true" ]]; then
  exec python -B -W ignore $django_root/manage.py runserver 0.0.0.0:8000
else
  if [[ "${ENABLE_NEWRELIC,,}" == "true" ]]; then
    printf "Starting Django with Newrelic\n"
    export NEW_RELIC_CONFIG_FILE=$django_root/newrelic.conf
    j2 $django_root/newrelic.conf.j2 > $django_root/newrelic.conf
    py_exec="/usr/local/bin/newrelic-admin run-program python -W ignore"
  else
    printf "Starting Djang without Newrelic\n"
    py_exec="python -W ignore"
  fi
  echo "starting gunicorn and nginx"
  $py_exec /usr/local/bin/gunicorn button3d.wsgi:application \
      --bind 0.0.0.0:8080 \
      --workers 3 \
      --chdir $django_root/ \
      --reload &
  exec nginx -g "pid /tmp/nginx.pid; daemon off;"
fi

