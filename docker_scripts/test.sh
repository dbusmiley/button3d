#!/bin/bash
cd /var/www/django
if [[ $DEBUG = True ]];
then
    npm run build
fi
echo "127.0.0.1       my.3yd \n127.0.0.1       org.my.3yd" >> /etc/hosts
mkdir /var/www/django/test-reports/html/

# Wait for the Connection to the DB, exit if > 60 seconds
WAIT_TIMEOUT=60
SECONDS_WAITED=0
until nc -z $DB_HOST $DB_PORT 
do
  sleep 1
  echo "Waited $SECONDS_WAITED seconds for SQL Server to come up"
  SECONDS_WAITED="$(($SECONDS_WAITED+1))"
  if test $SECONDS_WAITED -gt $WAIT_TIMEOUT; then
    echo "Wait Timeout Exceeded, Database didn't start..."
    exit 1
  fi
done

python -W ignore manage.py migrate
if [ $? -ne 0 ]; then exit $?; fi
echo "Testing Load Sample Data"
python manage.py flush --no-input 
python manage.py load_sample_data -M multi-bumble.3yourmind.com -s single-bumble.3yourmind.com -p 3yourminD -m test+admin@3yd.de
if [ $? -ne 0 ]; then exit $?; fi

python -W ignore /usr/local/bin/coverage run --source='.' --omit "**/migrations/**" manage.py test --testrunner="xmlrunner.extra.djangotestrunner.XMLTestRunner" -nk -v 2
returncode=$?

coverage xml
coverage html -d /var/www/django/test-reports/html/
coverage2clover < coverage.xml > ./test-reports/clover.xml
rm coverage.xml
#./node_modules/.bin/jest --ci --testResultsProcessor="jest-bamboo-formatter"
touch ./jest.json # modify the timestamp of this
mv ./jest.json ./test-reports/jest.json
chmod -R 777 /var/www/django/test-reports
exit $returncode
