#!/bin/sh

[ $# -eq 0 ] && {
	echo "Usage: $0 <lib> [lib [lib [...]]]"; exit 64
}

for libpath in $*
do
	target=${libpath%.*}
	source=${libpath##*/}
	ln -s ${source} ${target}
done
