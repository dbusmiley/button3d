#!/bin/bash
set -eo pipefail

DJANGO_ROOT=${DJANGO_ROOT:-"/var/www/django"}
STATIC_ROOT=${STATIC_ROOT:-"/var/www/assets"}

if [ -z "${S3_STATIC_SECRET_KEY}" -o -z "${S3_STATIC_ACCESS_KEY}" ]; then
  printf "S3_STATIC_ACCESS_KEY and/or S3_STATIC_SECRET_KEY not set\n"
  exit 1
fi

if [ -z "${S3_STATIC_BUCKET}" ]; then
  printf "S3_STATIC_BUCKET is not set\n"
  exit 1
fi

if [ -z "${S3_STATIC_REGION}" ]; then
  printf "S3_STATIC_REGION is not set\n"
  exit 1
fi

export AWS_ACCESS_KEY_ID=${S3_STATIC_ACCESS_KEY}
export AWS_SECRET_ACCESS_KEY=${S3_STATIC_SECRET_KEY}
export AWS_DEFAULT_REGION=${S3_STATIC_REGION}

version=$(cat ${DJANGO_ROOT}/version)
aws s3 cp ${STATIC_ROOT} s3://${S3_STATIC_BUCKET}/static/ --recursive --acl public-read
