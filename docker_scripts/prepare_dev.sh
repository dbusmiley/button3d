#!/bin/bash
cd /var/www/django

until nc -z $DB_HOST $DB_PORT
do
  echo "Waiting for SQL Server to come up"
  sleep 1
done
MULTI_DOMAIN=multi.my.3yd
SINGLE_DOMAIN=single.my.3yd
if [[ -n $SAMPLE_DATA_MULTI_DOMAIN ]]; then
MULTI_DOMAIN=$SAMPLE_DATA_MULTI_DOMAIN
fi
if [[ -n $SAMPLE_DATA_SINGLE_DOMAIN ]]; then
SINGLE_DOMAIN=$SAMPLE_DATA_SINGLE_DOMAIN
fi
python manage.py load_sample_data -M $MULTI_DOMAIN -s $SINGLE_DOMAIN -p 3yourminD -m test+admin@3yd.de --once
