Django models
=============
Core models not tied to a specific part of the application.

.. py:currentmodule:: apps.b3_core.models.base

Utilities
---------

.. autoclass:: WrappedTemplate
    :members:

Abstract models
---------------

.. autoclass:: BaseEditableTemplate
    :members:

.. autoclass:: BaseEncryptionSupport
   :members:

Managers
--------

.. autoclass:: EditableTemplateManager
    :members:

Concrete models
---------------

.. autoclass:: TemplateVarDescription
    :members:

.. py:currentmodule:: apps.b3_core.models.templates

.. autoclass:: OrderNoteTemplate
   :members:


