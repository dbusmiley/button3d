.. py:currentmodule:: apps.b3_core.encryption

Encryption Service
==================

Configuration
-------------

.. autoclass:: EncryptionConfig
    :members:

Symmetric File Encryption
-------------------------

Two concrete classes are provided. One standard and one extra secure. The
difference is not just in security, but also in performance.

The default shall be evaluated periodically to conform to industry standards and
in the event the cipher or hashing algorithm is broken.

.. note::

    When the default is changed, the extra secure
    version must also change and care must be taken to migrate keys, either in
    a single migration or on an event base.
    This is not an easy task, since the servie and it's consumers are decoupled
    and it is therefore the consumer's responsibility to know which documents
    are encrypted with which keys.

.. autoclass:: SymmetricFileEncryptionService
    :members:

.. autoclass:: ExtraSecureFileEncryptionService
    :members:
