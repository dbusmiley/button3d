.. py:currentmodule:: apps.b3_pdf.renderer.base

PDF rendering service
=====================

Base classes
------------

.. autoclass:: ResourceFetcher
   :members: __call__

.. autoclass:: TemplateInterface
   :members:

.. autoclass:: PdfTemplateHandler
   :members:

Weasyprint implementation
-------------------------

.. py:currentmodule:: apps.b3_pdf.renderer.weasyprint

.. autoclass:: WeasyResourceFetcher
   :members: __call__

.. autoclass:: PdfService
   :members:

Django specific
^^^^^^^^^^^^^^^

.. py:currentmodule:: apps.b3_pdf.renderer.django

.. autoclass:: DjangoStaticFetcher
   :members:
   :special-members: __call__

PDF templates
-------------

.. py:currentmodule:: apps.b3_pdf.models.templates

.. autoclass:: OrderNoteTemplate
   :members:
