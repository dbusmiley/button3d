.. Button3D Python Documentation documentation master file, created by
   sphinx-quickstart on Thu Jul 26 19:41:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Button3D Python Documentation's documentation!
=========================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   b3_core/index
   b3_pdf


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
